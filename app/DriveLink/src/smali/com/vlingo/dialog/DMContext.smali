.class public Lcom/vlingo/dialog/DMContext;
.super Ljava/lang/Object;
.source "DMContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/DMContext$HistoryPredicate;
    }
.end annotation


# static fields
.field private static final CODE_NOTHING_RECOGNIZED:Ljava/lang/String; = "NothingRecognized"

.field private static final HEX_CHARS:[C

.field private static final KEY_CONTACT_CLEAR_CACHE:Ljava/lang/String; = "contact_clear_cache"

.field private static final KEY_DATE_TIME_CACHE:Ljava/lang/String; = "date_time_cache"

.field private static final KEY_LIST_POSITION:Ljava/lang/String; = "list_position"

.field private static final KEY_PROMPT:Ljava/lang/String; = "prompt"

.field public static final MAX_CACHED_TIME_DIFFERENCE_MINUTES:I = 0x5

.field private static final NODE_CODE:Ljava/lang/String; = "Code"

.field private static final NODE_PG:Ljava/lang/String; = "pg"

.field private static final NODE_UL:Ljava/lang/String; = "UL"

.field private static final NODE_WARNING:Ljava/lang/String; = "Warning"

.field private static final STATE_VERSION:[B

.field private static final logger:Lcom/vlingo/common/log4j/VLogger;

.field private static metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;


# instance fields
.field private activeParse:Lcom/vlingo/dialog/util/Parse;

.field private addedListPositionGoal:Z

.field private annotateContacts:Z

.field private autoListen:Z

.field private cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

.field private capabilityMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private clientDateTime:Lcom/vlingo/dialog/util/DateTime;

.field private closed:Z

.field private config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

.field private dialogMode:Ljava/lang/String;

.field private disableIncompleteWidget:Z

.field private fieldId:Ljava/lang/String;

.field private goalList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            ">;"
        }
    .end annotation
.end field

.field private goalsFinished:Z

.field private history:Lcom/vlingo/dialog/state/model/History;

.field private incomingFieldId:Ljava/lang/String;

.field private isDialogManagerFlow:Z

.field private isPassthrough:Z

.field private lastPrompt:Ljava/lang/String;

.field private listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

.field private listSize:Ljava/lang/Integer;

.field private needRecognition:Z

.field private parses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/util/Parse;",
            ">;"
        }
    .end annotation
.end field

.field private recognitionNotProcessed:Z

.field private softwareVersion:Ljava/lang/String;

.field private state:Lcom/vlingo/dialog/state/model/State;

.field private stateBytes:[B

.field private stateWasReset:Z

.field private wasResetToTop:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 71
    const-class v0, Lcom/vlingo/dialog/DMContext;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    .line 73
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vlingo/dialog/DMContext;->STATE_VERSION:[B

    .line 391
    new-instance v0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/vlingo/mda/model/PackageMeta;

    const/4 v2, 0x0

    invoke-static {}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/mda/model/ClassMeta;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;-><init>([Lcom/vlingo/mda/model/PackageMeta;)V

    sput-object v0, Lcom/vlingo/dialog/DMContext;->metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    .line 1080
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lcom/vlingo/dialog/DMContext;->HEX_CHARS:[C

    return-void

    .line 73
    nop

    :array_0
    .array-data 1
        0x44t
        0x4dt
        0x0t
        0x3t
    .end array-data

    .line 1080
    :array_1
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public constructor <init>(Lcom/vlingo/dialog/manager/model/DialogManagerConfig;[BLjava/lang/String;Lcom/vlingo/dialog/util/DateTime;Ljava/util/Map;)V
    .locals 5
    .param p1, "config"    # Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    .param p2, "dialogStateBytes"    # [B
    .param p3, "incomingFieldId"    # Ljava/lang/String;
    .param p4, "clientDateTime"    # Lcom/vlingo/dialog/util/DateTime;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/manager/model/DialogManagerConfig;",
            "[B",
            "Ljava/lang/String;",
            "Lcom/vlingo/dialog/util/DateTime;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p5, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->closed:Z

    .line 89
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    .line 90
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->goalsFinished:Z

    .line 93
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->autoListen:Z

    .line 97
    iput-boolean v2, p0, Lcom/vlingo/dialog/DMContext;->needRecognition:Z

    .line 102
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->stateWasReset:Z

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    .line 111
    iput-boolean v2, p0, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    .line 112
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->isPassthrough:Z

    .line 113
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->recognitionNotProcessed:Z

    .line 125
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->annotateContacts:Z

    .line 127
    iput-object v3, p0, Lcom/vlingo/dialog/DMContext;->softwareVersion:Ljava/lang/String;

    .line 129
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->wasResetToTop:Z

    .line 131
    iput-object v3, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    .line 135
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->addedListPositionGoal:Z

    .line 137
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->disableIncompleteWidget:Z

    .line 141
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .line 142
    iput-object p2, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    .line 143
    iput-object p3, p0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    .line 144
    iput-object p4, p0, Lcom/vlingo/dialog/DMContext;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    .line 146
    iput-object p5, p0, Lcom/vlingo/dialog/DMContext;->capabilityMap:Ljava/util/Map;

    .line 147
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "capability map = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 149
    :cond_0
    if-eqz p1, :cond_3

    .line 150
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->unpackHistory()Z

    move-result v0

    .line 151
    .local v0, "loadedHistory":Z
    if-nez v0, :cond_1

    .line 153
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v2, "reset dialog state"

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 155
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->init()V

    .line 156
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->initCachedDateTime()V

    .line 157
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "prompt"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->lastPrompt:Ljava/lang/String;

    .line 158
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "prompt"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 159
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 160
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 161
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pending queries: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->getPendingQueries()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 167
    .end local v0    # "loadedHistory":Z
    :cond_2
    :goto_0
    return-void

    .line 164
    :cond_3
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    .line 165
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v2, "no config: non-DM flow"

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private addNlgGoal(Lcom/vlingo/dialog/goal/model/NlgGoal;Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Z)V
    .locals 3
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/NlgGoal;
    .param p2, "template"    # Ljava/lang/String;
    .param p3, "templateForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p4, "autoListen"    # Z

    .prologue
    .line 732
    if-nez p2, :cond_0

    .line 733
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "template == null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 735
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getDialogSuffix()Ljava/lang/String;

    move-result-object v0

    .line 736
    .local v0, "suffix":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 737
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 739
    :cond_1
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/goal/model/NlgGoal;->setTemplate(Ljava/lang/String;)V

    .line 740
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/vlingo/dialog/goal/model/NlgGoal;->setTemplateForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 741
    iget-boolean v1, p0, Lcom/vlingo/dialog/DMContext;->autoListen:Z

    or-int/2addr v1, p4

    iput-boolean v1, p0, Lcom/vlingo/dialog/DMContext;->autoListen:Z

    .line 742
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 743
    return-void
.end method

.method private addPendingQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)V
    .locals 1
    .param p1, "queryGoal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 790
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/state/model/History;->addPendingQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 791
    return-void
.end method

.method private addToHistory(Lcom/vlingo/dialog/state/model/HistoryItem;)V
    .locals 1
    .param p1, "item"    # Lcom/vlingo/dialog/state/model/HistoryItem;

    .prologue
    .line 602
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 603
    return-void
.end method

.method private addUtteranceGoal(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 639
    new-instance v0, Lcom/vlingo/dialog/goal/model/UtteranceGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/UtteranceGoal;-><init>()V

    .line 640
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/UtteranceGoal;
    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/goal/model/UtteranceGoal;->setText(Ljava/lang/String;)V

    .line 641
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 642
    return-void
.end method

.method private static anyCompatibleFieldId(Lcom/vlingo/dialog/manager/model/IFormManager;Ljava/lang/String;)Z
    .locals 4
    .param p0, "manager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p1, "incomingFieldId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 550
    invoke-interface {p0, p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->compatibleFieldId(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 558
    :goto_0
    return v2

    .line 553
    :cond_0
    invoke-interface {p0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 554
    .local v1, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-static {v1, p1}, Lcom/vlingo/dialog/DMContext;->anyCompatibleFieldId(Lcom/vlingo/dialog/manager/model/IFormManager;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 558
    .end local v1    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static appendSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "suffix"    # Ljava/lang/String;

    .prologue
    .line 199
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 202
    .end local p0    # "string":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .restart local p0    # "string":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private assertNotClosed()V
    .locals 2

    .prologue
    .line 281
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->closed:Z

    if-eqz v0, :cond_0

    .line 282
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284
    :cond_0
    return-void
.end method

.method private extractUtterance(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;
    .locals 4
    .param p1, "root"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 323
    const-string/jumbo v3, "UL"

    invoke-virtual {p1, v3}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 324
    .local v2, "ulList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 325
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/common/message/MNode;

    .line 326
    .local v1, "ul":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v3, "c"

    invoke-virtual {v1, v3}, Lcom/vlingo/common/message/MNode;->getChildWithName(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v0

    .line 327
    .local v0, "c":Lcom/vlingo/common/message/MNode;
    if-eqz v0, :cond_0

    .line 328
    invoke-static {v0}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->assembleStringFromWords(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v3

    .line 331
    .end local v0    # "c":Lcom/vlingo/common/message/MNode;
    .end local v1    # "ul":Lcom/vlingo/common/message/MNode;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private extractUtterance(Lcom/vlingo/voicepad/tagtoaction/model/Action;)Ljava/lang/String;
    .locals 4
    .param p1, "nluAction"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .prologue
    .line 335
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getParams()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;

    .line 336
    .local v1, "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    const-string/jumbo v2, "utterance"

    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 337
    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 340
    .end local v1    # "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private filterHistory(Lcom/vlingo/dialog/DMContext$HistoryPredicate;)V
    .locals 3
    .param p1, "predicate"    # Lcom/vlingo/dialog/DMContext$HistoryPredicate;

    .prologue
    .line 953
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/state/model/HistoryItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 954
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/state/model/HistoryItem;

    .line 955
    .local v1, "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    invoke-interface {p1, v1}, Lcom/vlingo/dialog/DMContext$HistoryPredicate;->apply(Lcom/vlingo/dialog/state/model/HistoryItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 956
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 959
    .end local v1    # "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    :cond_1
    return-void
.end method

.method private getDialogSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "mode"    # Ljava/lang/String;
    .param p2, "modeLess"    # Ljava/lang/String;

    .prologue
    .line 191
    if-nez p1, :cond_0

    .end local p2    # "modeLess":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "modeLess":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method private static getMode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "taskName"    # Ljava/lang/String;

    .prologue
    .line 379
    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 380
    .local v0, "i":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "taskName"    # Ljava/lang/String;

    .prologue
    .line 374
    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 375
    .local v0, "i":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .end local p0    # "taskName":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "taskName":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private getPendingQueries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 798
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->getPendingQueries()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static getTaskName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 369
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 370
    .local v0, "i":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .end local p0    # "path":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "path":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private hijack(Lcom/vlingo/dialog/manager/model/TopFormManager;)Z
    .locals 1
    .param p1, "topManager"    # Lcom/vlingo/dialog/manager/model/TopFormManager;

    .prologue
    .line 535
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-virtual {p1, p0, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v0

    return v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 818
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    if-nez v1, :cond_1

    .line 819
    new-instance v1, Lcom/vlingo/dialog/state/model/History;

    invoke-direct {v1}, Lcom/vlingo/dialog/state/model/History;-><init>()V

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    .line 826
    :goto_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 827
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v2, "DMContext received with invalid activePath component."

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 828
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    .line 831
    :cond_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    if-nez v1, :cond_2

    .line 832
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/dialog/DMContext;->stateWasReset:Z

    .line 833
    new-instance v1, Lcom/vlingo/dialog/state/model/State;

    invoke-direct {v1}, Lcom/vlingo/dialog/state/model/State;-><init>()V

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    .line 834
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->createForm()Lcom/vlingo/dialog/model/Form;

    move-result-object v0

    .line 835
    .local v0, "topForm":Lcom/vlingo/dialog/model/IForm;
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1, v0}, Lcom/vlingo/dialog/state/model/State;->setTop(Lcom/vlingo/dialog/model/IForm;)V

    .line 836
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1, v0}, Lcom/vlingo/dialog/state/model/State;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 837
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->preSerialize()V

    .line 843
    .end local v0    # "topForm":Lcom/vlingo/dialog/model/IForm;
    :goto_1
    return-void

    .line 821
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->fetchState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    goto :goto_0

    .line 840
    :cond_2
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->copy()Lcom/vlingo/dialog/state/model/State;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    .line 841
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/State;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/manager/model/TopFormManager;->unprune(Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_1
.end method

.method private initCachedDateTime()V
    .locals 4

    .prologue
    .line 170
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v3, "date_time_cache"

    invoke-virtual {v2, v3}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "cachedDateTimeString":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 172
    invoke-static {v1}, Lcom/vlingo/dialog/util/DateTime;->fromClientTime(Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    .line 173
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    invoke-virtual {v2, v3}, Lcom/vlingo/dialog/util/DateTime;->minutesUntil(Lcom/vlingo/dialog/util/DateTime;)I

    move-result v0

    .line 174
    .local v0, "ageMinutes":I
    const/4 v2, 0x5

    if-le v0, v2, :cond_0

    .line 175
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    iput-object v2, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    .line 180
    .end local v0    # "ageMinutes":I
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    iput-object v2, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    goto :goto_0
.end method

.method public static logBytes([B)V
    .locals 8
    .param p0, "bytes"    # [B

    .prologue
    const/16 v7, 0x40

    const/4 v6, 0x0

    .line 1056
    const/16 v0, 0x40

    .line 1057
    .local v0, "limit":I
    const/16 v1, 0x10

    .line 1059
    .local v1, "limitEnd":I
    if-nez p0, :cond_0

    .line 1060
    sget-object v3, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v4, "dialog state bytes = null"

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 1069
    :goto_0
    return-void

    .line 1061
    :cond_0
    array-length v3, p0

    if-gt v3, v7, :cond_1

    .line 1062
    sget-object v3, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "dialog state bytes["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p0

    invoke-static {p0, v6, v5}, Lcom/vlingo/dialog/DMContext;->toHex([BII)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    goto :goto_0

    .line 1064
    :cond_1
    const/16 v3, 0x10

    array-length v4, p0

    add-int/lit8 v4, v4, -0x40

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1065
    .local v2, "nEnd":I
    sget-object v3, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "dialog state bytes["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p0, v6, v7}, Lcom/vlingo/dialog/DMContext;->toHex([BII)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p0

    sub-int/2addr v5, v2

    array-length v6, p0

    invoke-static {p0, v5, v6}, Lcom/vlingo/dialog/DMContext;->toHex([BII)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private maybeSendIncompleteWidget()V
    .locals 4

    .prologue
    .line 710
    iget-boolean v3, p0, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->getPendingQueries()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/vlingo/dialog/DMContext;->recognitionNotProcessed:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/vlingo/dialog/DMContext;->disableIncompleteWidget:Z

    if-nez v3, :cond_0

    .line 715
    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v3}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 716
    .local v0, "active":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 717
    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v2

    .line 718
    .local v2, "topManager":Lcom/vlingo/dialog/manager/model/TopFormManager;
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    .line 719
    .local v1, "activeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v1, p0, v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->maybeSendIncompleteWidget(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 722
    .end local v0    # "active":Lcom/vlingo/dialog/model/IForm;
    .end local v1    # "activeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    .end local v2    # "topManager":Lcom/vlingo/dialog/manager/model/TopFormManager;
    :cond_0
    return-void
.end method

.method private modeConversion()V
    .locals 11

    .prologue
    .line 344
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v9}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v2

    .line 345
    .local v2, "activePath":Ljava/lang/String;
    const-string/jumbo v9, ""

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    invoke-static {v2}, Lcom/vlingo/dialog/DMContext;->getTaskName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 347
    .local v3, "activeTask":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/dialog/DMContext;->getMode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 348
    .local v0, "activeMode":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 350
    invoke-static {v3}, Lcom/vlingo/dialog/DMContext;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 351
    .local v1, "activeName":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v9}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/dialog/model/IForm;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    invoke-interface {v9, v3}, Lcom/vlingo/dialog/model/IForm;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/model/Form;

    .line 352
    .local v4, "activeTaskForm":Lcom/vlingo/dialog/model/Form;
    invoke-virtual {v4}, Lcom/vlingo/dialog/model/Form;->getName()Ljava/lang/String;

    move-result-object v8

    .line 353
    .local v8, "origName":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    const-string/jumbo v10, ""

    invoke-direct {p0, v9, v10}, Lcom/vlingo/dialog/DMContext;->getDialogSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 354
    .local v7, "newSuffix":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 355
    .local v6, "newName":Ljava/lang/String;
    invoke-virtual {v4, v6}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 356
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v9}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/dialog/model/IForm;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 358
    .local v5, "newActivePath":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v9}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/vlingo/dialog/manager/model/TopFormManager;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 360
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v9, v5}, Lcom/vlingo/dialog/state/model/State;->setActivePath(Ljava/lang/String;)V

    goto :goto_0

    .line 363
    :cond_2
    invoke-virtual {v4, v8}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private packHistory()[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 942
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 943
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    move-object v2, v0

    .line 944
    .local v2, "os":Ljava/io/OutputStream;
    sget-object v4, Lcom/vlingo/dialog/DMContext;->STATE_VERSION:[B

    invoke-virtual {v2, v4}, Ljava/io/OutputStream;->write([B)V

    .line 945
    new-instance v3, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v3, v2}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 946
    .end local v2    # "os":Ljava/io/OutputStream;
    .local v3, "os":Ljava/io/OutputStream;
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 947
    .local v1, "oos":Ljava/io/ObjectOutputStream;
    iget-object v4, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v1, v4}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 948
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 949
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    return-object v4
.end method

.method private startTaskCleanup(Z)V
    .locals 4
    .param p1, "clearCachedDateTime"    # Z

    .prologue
    .line 608
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v0

    .line 609
    .local v0, "historyItems":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/state/model/HistoryItem;>;"
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 610
    if-eqz p1, :cond_0

    .line 611
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    .line 612
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "date_time_cache"

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->getClientDateTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    :cond_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->setCandidates(Lcom/vlingo/dialog/state/model/CompletedQuery;)V

    .line 615
    return-void
.end method

.method public static final toHex([BII)Ljava/lang/String;
    .locals 6
    .param p0, "bytes"    # [B
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 1072
    sub-int v4, p2, p1

    mul-int/lit8 v4, v4, 0x2

    new-array v0, v4, [C

    .line 1073
    .local v0, "buf":[C
    move v1, p1

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    move v3, v2

    .end local v2    # "j":I
    .local v3, "j":I
    :goto_0
    if-ge v1, p2, :cond_0

    .line 1074
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    sget-object v4, Lcom/vlingo/dialog/DMContext;->HEX_CHARS:[C

    aget-byte v5, p0, v1

    ushr-int/lit8 v5, v5, 0x4

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 1075
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .restart local v3    # "j":I
    sget-object v4, Lcom/vlingo/dialog/DMContext;->HEX_CHARS:[C

    aget-byte v5, p0, v1

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v2

    .line 1073
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1077
    :cond_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    return-object v4
.end method

.method private unpackHistory()Z
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 887
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    .line 889
    iget-object v7, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    array-length v7, v7

    sget-object v9, Lcom/vlingo/dialog/DMContext;->STATE_VERSION:[B

    array-length v9, v9

    if-ge v7, v9, :cond_4

    .line 890
    :cond_0
    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 891
    iget-object v7, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    if-nez v7, :cond_2

    .line 892
    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v9, "dialog state bytes = null"

    invoke-virtual {v7, v9}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    move v7, v8

    .line 937
    :goto_1
    return v7

    .line 894
    :cond_2
    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 895
    iget-object v7, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    invoke-static {v7}, Lcom/vlingo/dialog/DMContext;->logBytes([B)V

    goto :goto_0

    .line 897
    :cond_3
    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "dialog state bytes: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    array-length v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 905
    :cond_4
    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 906
    iget-object v7, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    invoke-static {v7}, Lcom/bzbyte/util/Base64;->encodeBytes([B)Ljava/lang/String;

    move-result-object v6

    .line 907
    .local v6, "stateEncoded":Ljava/lang/String;
    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Inbound DM State: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 910
    .end local v6    # "stateEncoded":Ljava/lang/String;
    :cond_5
    const/4 v4, 0x1

    .line 911
    .local v4, "match":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    sget-object v7, Lcom/vlingo/dialog/DMContext;->STATE_VERSION:[B

    array-length v7, v7

    if-ge v1, v7, :cond_6

    .line 912
    iget-object v7, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    aget-byte v7, v7, v1

    sget-object v9, Lcom/vlingo/dialog/DMContext;->STATE_VERSION:[B

    aget-byte v9, v9, v1

    if-eq v7, v9, :cond_9

    .line 913
    const/4 v4, 0x0

    .line 917
    :cond_6
    if-nez v4, :cond_a

    .line 918
    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 919
    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v9, "dialog state bytes: non-matching version"

    invoke-virtual {v7, v9}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 921
    :cond_7
    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 922
    iget-object v7, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    invoke-static {v7}, Lcom/vlingo/dialog/DMContext;->logBytes([B)V

    :cond_8
    move v7, v8

    .line 924
    goto/16 :goto_1

    .line 911
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 928
    :cond_a
    :try_start_0
    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v7, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    sget-object v9, Lcom/vlingo/dialog/DMContext;->STATE_VERSION:[B

    array-length v9, v9

    iget-object v10, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    array-length v10, v10

    sget-object v11, Lcom/vlingo/dialog/DMContext;->STATE_VERSION:[B

    array-length v11, v11

    sub-int/2addr v10, v11

    invoke-direct {v2, v7, v9, v10}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 929
    .local v2, "is":Ljava/io/InputStream;
    new-instance v3, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v3, v2}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 930
    .end local v2    # "is":Ljava/io/InputStream;
    .local v3, "is":Ljava/io/InputStream;
    new-instance v5, Ljava/io/ObjectInputStream;

    invoke-direct {v5, v3}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 931
    .local v5, "ois":Ljava/io/ObjectInputStream;
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/dialog/state/model/History;

    iput-object v7, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    .line 932
    iget-object v7, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v7}, Lcom/vlingo/dialog/state/model/History;->postDeserialize()V

    .line 933
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 934
    const/4 v7, 0x1

    goto/16 :goto_1

    .line 935
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v5    # "ois":Ljava/io/ObjectInputStream;
    :catch_0
    move-exception v0

    .line 936
    .local v0, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v7

    if-eqz v7, :cond_b

    sget-object v7, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v9, "exception unpacking dialog state"

    invoke-virtual {v7, v9, v0}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    :cond_b
    move v7, v8

    .line 937
    goto/16 :goto_1
.end method


# virtual methods
.method public addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V
    .locals 3
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;

    .prologue
    .line 746
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 747
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 748
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->addedListPositionGoal:Z

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vlingo/dialog/goal/model/ListGoal;

    if-eqz v0, :cond_0

    .line 749
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "list_position"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 751
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "added goal: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 752
    :cond_1
    return-void
.end method

.method public addListPositionGoal(Lcom/vlingo/dialog/goal/model/ListPositionGoal;)V
    .locals 4
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/ListPositionGoal;

    .prologue
    .line 681
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->addedListPositionGoal:Z

    .line 682
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "list_position"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/vlingo/dialog/goal/model/ListPositionGoal;->getStartIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/dialog/goal/model/ListPositionGoal;->getEndIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 684
    return-void
.end method

.method public addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "template"    # Ljava/lang/String;
    .param p2, "templateForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "fieldId"    # Ljava/lang/String;
    .param p4, "autoListen"    # Z

    .prologue
    .line 654
    new-instance v0, Lcom/vlingo/dialog/goal/model/PromptGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/PromptGoal;-><init>()V

    invoke-direct {p0, v0, p1, p2, p4}, Lcom/vlingo/dialog/DMContext;->addNlgGoal(Lcom/vlingo/dialog/goal/model/NlgGoal;Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Z)V

    .line 655
    if-eqz p3, :cond_0

    .line 656
    invoke-virtual {p0, p3}, Lcom/vlingo/dialog/DMContext;->setFieldId(Ljava/lang/String;)V

    .line 658
    :cond_0
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "prompt"

    invoke-virtual {v0, v1, p1}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    return-void
.end method

.method public addQueryGoal(Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V
    .locals 1
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;
    .param p2, "clearCache"    # Z

    .prologue
    .line 662
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/goal/model/QueryGoal;->setClearCache(Z)V

    .line 663
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/DMContext;->addPendingQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 664
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 666
    instance-of v0, p1, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    if-eqz v0, :cond_0

    .line 667
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->setContactClearCache(Z)V

    .line 669
    :cond_0
    return-void
.end method

.method public addTaskGoal(Ljava/lang/String;ZZLcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "confirm"    # Z
    .param p3, "execute"    # Z
    .param p4, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 672
    new-instance v0, Lcom/vlingo/dialog/goal/model/TaskGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/TaskGoal;-><init>()V

    .line 673
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/TaskGoal;
    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/goal/model/TaskGoal;->setName(Ljava/lang/String;)V

    .line 674
    invoke-virtual {v0, p2}, Lcom/vlingo/dialog/goal/model/TaskGoal;->setConfirm(Z)V

    .line 675
    invoke-virtual {v0, p3}, Lcom/vlingo/dialog/goal/model/TaskGoal;->setExecute(Z)V

    .line 676
    invoke-virtual {v0, p4}, Lcom/vlingo/dialog/goal/model/TaskGoal;->setForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 677
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 678
    return-void
.end method

.method public anyListGoals()Z
    .locals 3

    .prologue
    .line 979
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getGoals()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/goal/model/Goal;

    .line 980
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/Goal;
    instance-of v2, v0, Lcom/vlingo/dialog/goal/model/ListGoal;

    if-eqz v2, :cond_0

    .line 981
    const/4 v2, 0x1

    .line 984
    .end local v0    # "goal":Lcom/vlingo/dialog/goal/model/Goal;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public appendDialogSuffix(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/vlingo/dialog/DMContext;->appendSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public appendTemplateSuffix(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/vlingo/dialog/DMContext;->appendSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cancel(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "formManager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 755
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 757
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v1

    .line 758
    .local v1, "topManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->createForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 759
    .local v0, "topForm":Lcom/vlingo/dialog/model/IForm;
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2, v0}, Lcom/vlingo/dialog/state/model/State;->setTop(Lcom/vlingo/dialog/model/IForm;)V

    .line 760
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2, v0}, Lcom/vlingo/dialog/state/model/State;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 761
    invoke-static {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->clearDisambig(Lcom/vlingo/dialog/DMContext;)V

    .line 763
    new-instance v2, Lcom/vlingo/dialog/goal/model/DialogCancelGoal;

    invoke-direct {v2}, Lcom/vlingo/dialog/goal/model/DialogCancelGoal;-><init>()V

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 767
    return-void
.end method

.method public clearConfirm()V
    .locals 2

    .prologue
    .line 770
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->setConfirm(Ljava/lang/String;)V

    .line 771
    return-void
.end method

.method public clearPendingQueries()V
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->clearPendingQueries()V

    .line 807
    return-void
.end method

.method public clearQueries()V
    .locals 1

    .prologue
    .line 814
    const-class v0, Lcom/vlingo/dialog/goal/model/QueryGoal;

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->clearQueriesByType(Ljava/lang/Class;)V

    .line 815
    return-void
.end method

.method public clearQueriesByType(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 810
    .local p1, "queryClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/goal/model/QueryGoal;>;"
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/state/model/History;->clearQueriesByType(Ljava/lang/Class;)V

    .line 811
    return-void
.end method

.method public close()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 288
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    if-eqz v0, :cond_2

    .line 289
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->preSerialize()V

    .line 291
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "next state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 292
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->packHistory()[B

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    .line 293
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "packed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 301
    :cond_1
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->closed:Z

    .line 302
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    return-object v0

    .line 295
    :cond_2
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    if-nez v0, :cond_1

    .line 296
    new-instance v0, Lcom/vlingo/dialog/state/model/History;

    invoke-direct {v0}, Lcom/vlingo/dialog/state/model/History;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    .line 297
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->preSerialize()V

    .line 298
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->packHistory()[B

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    .line 299
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "packed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;
    .locals 1
    .param p1, "queryEvent"    # Lcom/vlingo/dialog/event/model/QueryEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/event/model/QueryEvent;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;)",
            "Lcom/vlingo/dialog/state/model/CompletedQuery;"
        }
    .end annotation

    .prologue
    .line 802
    .local p2, "queryClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/goal/model/QueryGoal;>;"
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/dialog/state/model/History;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    return-object v0
.end method

.method public disableIncompleteWidget()V
    .locals 1

    .prologue
    .line 1052
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->disableIncompleteWidget:Z

    .line 1053
    return-void
.end method

.method public fetchCompletedQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)Lcom/vlingo/dialog/state/model/CompletedQuery;
    .locals 3
    .param p1, "queryGoal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 859
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/History;->getCompletedQueries()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 860
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryGoal()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 864
    .end local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fetchQueryEvent(Ljava/lang/Class;)Lcom/vlingo/dialog/event/model/QueryEvent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/event/model/QueryEvent;",
            ">;)",
            "Lcom/vlingo/dialog/event/model/QueryEvent;"
        }
    .end annotation

    .prologue
    .line 868
    .local p1, "eventClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/event/model/QueryEvent;>;"
    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v3}, Lcom/vlingo/dialog/state/model/History;->getCompletedQueries()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 869
    .local v2, "query":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v0

    .line 870
    .local v0, "event":Lcom/vlingo/dialog/event/model/QueryEvent;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 874
    .end local v0    # "event":Lcom/vlingo/dialog/event/model/QueryEvent;
    .end local v2    # "query":Lcom/vlingo/dialog/state/model/CompletedQuery;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fetchState()Lcom/vlingo/dialog/state/model/State;
    .locals 5

    .prologue
    .line 846
    const/4 v3, 0x0

    .line 847
    .local v3, "result":Lcom/vlingo/dialog/state/model/State;
    iget-object v4, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v4}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v2

    .line 848
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/state/model/HistoryItem;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v2, v4}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .local v0, "i":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/vlingo/dialog/state/model/HistoryItem;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 849
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/state/model/HistoryItem;

    .line 850
    .local v1, "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    instance-of v4, v1, Lcom/vlingo/dialog/state/model/State;

    if-eqz v4, :cond_0

    move-object v3, v1

    .line 851
    check-cast v3, Lcom/vlingo/dialog/state/model/State;

    .line 855
    .end local v1    # "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    :cond_1
    return-object v3
.end method

.method public finishGoals()V
    .locals 3

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 688
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->maybeSendIncompleteWidget()V

    .line 689
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 690
    new-instance v0, Lcom/vlingo/dialog/goal/model/FieldIdGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/FieldIdGoal;-><init>()V

    .line 691
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/FieldIdGoal;
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/goal/model/FieldIdGoal;->setFieldId(Ljava/lang/String;)V

    .line 692
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 694
    .end local v0    # "goal":Lcom/vlingo/dialog/goal/model/FieldIdGoal;
    :cond_0
    iget-boolean v2, p0, Lcom/vlingo/dialog/DMContext;->autoListen:Z

    if-eqz v2, :cond_1

    .line 695
    new-instance v2, Lcom/vlingo/dialog/goal/model/ListenGoal;

    invoke-direct {v2}, Lcom/vlingo/dialog/goal/model/ListenGoal;-><init>()V

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 697
    :cond_1
    iget-boolean v2, p0, Lcom/vlingo/dialog/DMContext;->isPassthrough:Z

    if-eqz v2, :cond_3

    .line 699
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/goal/model/Goal;>;"
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 700
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/goal/model/Goal;

    .line 701
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/Goal;
    instance-of v2, v0, Lcom/vlingo/dialog/goal/model/FieldIdGoal;

    if-nez v2, :cond_2

    instance-of v2, v0, Lcom/vlingo/dialog/goal/model/PassthroughGoal;

    if-nez v2, :cond_2

    .line 702
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 706
    .end local v0    # "goal":Lcom/vlingo/dialog/goal/model/Goal;
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/goal/model/Goal;>;"
    :cond_3
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/dialog/DMContext;->goalsFinished:Z

    .line 707
    return-void
.end method

.method public getActiveForm()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    return-object v0
.end method

.method public getActiveParse()Lcom/vlingo/dialog/util/Parse;
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    return-object v0
.end method

.method public getAnnotateContacts()Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->annotateContacts:Z

    return v0
.end method

.method public getCapabilityValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "capability"    # Ljava/lang/String;

    .prologue
    .line 882
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->capabilityMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getClientDateTime()Lcom/vlingo/dialog/util/DateTime;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    return-object v0
.end method

.method public getContactClearCache()Z
    .locals 3

    .prologue
    .line 970
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "contact_clear_cache"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 971
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDialogMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogSuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/vlingo/dialog/DMContext;->getDialogSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getGoals()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 725
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->goalsFinished:Z

    if-nez v0, :cond_0

    .line 728
    :cond_0
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getIncomingFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getLastPrompt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 975
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->lastPrompt:Ljava/lang/String;

    return-object v0
.end method

.method public getListPosition()Lcom/vlingo/dialog/manager/model/ListPosition;
    .locals 4

    .prologue
    .line 1005
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    if-nez v1, :cond_0

    .line 1006
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "list_position"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/dialog/manager/model/ListPosition;->parse(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v0

    .line 1007
    .local v0, "previous":Lcom/vlingo/dialog/manager/model/ListPosition;
    if-eqz v0, :cond_1

    .line 1008
    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    .line 1009
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1010
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "no list position from client: using previous value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/ListPosition;->getStartIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/ListPosition;->getEndIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 1021
    .end local v0    # "previous":Lcom/vlingo/dialog/manager/model/ListPosition;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    return-object v1

    .line 1014
    .restart local v0    # "previous":Lcom/vlingo/dialog/manager/model/ListPosition;
    :cond_1
    new-instance v1, Lcom/vlingo/dialog/manager/model/ListPosition;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getListSize()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vlingo/dialog/manager/model/ListPosition;-><init>(II)V

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    .line 1015
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1016
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "no list position from client: using default value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/ListPosition;->getStartIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/ListPosition;->getEndIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getListSize()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1030
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getScrollDefaultListSize()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    .line 1031
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 1032
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    .line 1034
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "no client list size: using default "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 1036
    :cond_1
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    return-object v0
.end method

.method public getNeedRecognition()Z
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->needRecognition:Z

    return v0
.end method

.method public getParses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/util/Parse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 585
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    return-object v0
.end method

.method public getScrollAutoListen()Z
    .locals 1

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getScrollAutoListen()Z

    move-result v0

    return v0
.end method

.method public getScrollErrorAutoListen()Z
    .locals 1

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getScrollErrorAutoListen()Z

    move-result v0

    return v0
.end method

.method public getScrollShowUserTurn()Z
    .locals 1

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getScrollShowUserTurn()Z

    move-result v0

    return v0
.end method

.method public getSoftwareVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->softwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Lcom/vlingo/dialog/state/model/State;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    return-object v0
.end method

.method public isConfirmCancel()Z
    .locals 2

    .prologue
    .line 786
    const-string/jumbo v0, "cancel"

    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->getConfirm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isConfirmExecute()Z
    .locals 2

    .prologue
    .line 782
    const-string/jumbo v0, "execute"

    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->getConfirm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isDialogManagerFlow()Z
    .locals 1

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    return v0
.end method

.method public isPassthrough()Z
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->isPassthrough:Z

    return v0
.end method

.method public peekQuery()Lcom/vlingo/dialog/goal/model/QueryGoal;
    .locals 1

    .prologue
    .line 794
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->peekQuery()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v0

    return-object v0
.end method

.method public processEvents(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Event;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 306
    .local p1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Event;>;"
    iget-object v4, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    if-nez v4, :cond_1

    .line 307
    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v5, "no config: ignoring events"

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 320
    :cond_0
    return-void

    .line 310
    :cond_1
    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "processEvents: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 311
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 312
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->modeConversion()V

    .line 313
    iget-object v4, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v4}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 314
    .local v0, "activeForm":Lcom/vlingo/dialog/model/IForm;
    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "active: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v6}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 315
    :cond_3
    iget-object v4, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v4}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v5}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/dialog/manager/model/TopFormManager;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    .line 316
    .local v1, "activeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/event/model/Event;

    .line 317
    .local v2, "event":Lcom/vlingo/dialog/event/model/Event;
    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_4

    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "event: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 318
    :cond_4
    invoke-interface {v1, p0, v0, v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method public processRecognition(Lcom/vlingo/common/message/MNode;)V
    .locals 1
    .param p1, "recognition"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 384
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/DMContext;->processRecognition(Lcom/vlingo/common/message/MNode;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    .line 385
    return-void
.end method

.method public processRecognition(Lcom/vlingo/common/message/MNode;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V
    .locals 19
    .param p1, "recognition"    # Lcom/vlingo/common/message/MNode;
    .param p2, "nluAction"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .prologue
    .line 408
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    .line 409
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v16

    if-eqz v16, :cond_0

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v17, "no recognition MNode"

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 410
    :cond_0
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->recognitionNotProcessed:Z

    .line 532
    :cond_1
    :goto_0
    return-void

    .line 414
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 416
    if-eqz p1, :cond_6

    .line 417
    const-string/jumbo v16, "Warning"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vlingo/common/message/MNode;

    .line 418
    .local v15, "warningNode":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v16, "Code"

    invoke-virtual/range {v15 .. v16}, Lcom/vlingo/common/message/MNode;->getChildWithName(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v6

    .line 419
    .local v6, "codeNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getTextChildNodeValue()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 420
    .local v5, "code":Ljava/lang/String;
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v16

    if-eqz v16, :cond_4

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "rec warning code: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 422
    :cond_4
    const-string/jumbo v16, "NothingRecognized"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 423
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v16

    if-eqz v16, :cond_5

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "handling "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " by generating FieldIdGoal = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 424
    :cond_5
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    .line 425
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->autoListen:Z

    .line 426
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    .line 427
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->recognitionNotProcessed:Z

    goto/16 :goto_0

    .line 433
    .end local v5    # "code":Ljava/lang/String;
    .end local v6    # "codeNode":Lcom/vlingo/common/message/MNode;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v15    # "warningNode":Lcom/vlingo/common/message/MNode;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-object/from16 v16, v0

    if-nez v16, :cond_7

    .line 434
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    goto/16 :goto_0

    .line 438
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v12

    .line 440
    .local v12, "topManager":Lcom/vlingo/dialog/manager/model/TopFormManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    if-eqz v16, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getDmFieldIdPattern()Ljava/util/regex/Pattern;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/util/regex/Matcher;->matches()Z

    move-result v16

    if-eqz v16, :cond_8

    const/4 v8, 0x1

    .line 441
    .local v8, "isDmFieldId":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    if-eqz v16, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->isTopLevelFieldId(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_9

    const/4 v9, 0x1

    .line 443
    .local v9, "isSpecialTopLevelFieldId":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    if-eqz v16, :cond_a

    if-nez v8, :cond_a

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->mightHijack(Ljava/lang/String;)Z

    move-result v10

    .line 445
    .local v10, "mightHijack":Z
    if-nez v9, :cond_a

    if-nez v10, :cond_a

    .line 447
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    goto/16 :goto_0

    .line 440
    .end local v8    # "isDmFieldId":Z
    .end local v9    # "isSpecialTopLevelFieldId":Z
    .end local v10    # "mightHijack":Z
    :cond_8
    const/4 v8, 0x0

    goto :goto_1

    .line 441
    .restart local v8    # "isDmFieldId":Z
    :cond_9
    const/4 v9, 0x0

    goto :goto_2

    .line 454
    .restart local v9    # "isSpecialTopLevelFieldId":Z
    :cond_a
    if-eqz p1, :cond_b

    .line 455
    invoke-direct/range {p0 .. p1}, Lcom/vlingo/dialog/DMContext;->extractUtterance(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v14

    .line 459
    .local v14, "utterance":Ljava/lang/String;
    :goto_3
    if-nez v14, :cond_c

    .line 460
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->recognitionNotProcessed:Z

    goto/16 :goto_0

    .line 457
    .end local v14    # "utterance":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/DMContext;->extractUtterance(Lcom/vlingo/voicepad/tagtoaction/model/Action;)Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "utterance":Ljava/lang/String;
    goto :goto_3

    .line 464
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    .line 465
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    .line 467
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->clearPendingQueries()V

    .line 469
    if-eqz p1, :cond_d

    .line 470
    const-string/jumbo v16, "pg"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .restart local v7    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/vlingo/common/message/MNode;

    .line 471
    .local v11, "pg":Lcom/vlingo/common/message/MNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v17, Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v0, v17

    invoke-direct {v0, v11, v14}, Lcom/vlingo/dialog/util/Parse;-><init>(Lcom/vlingo/common/message/MNode;Ljava/lang/String;)V

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 474
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v11    # "pg":Lcom/vlingo/common/message/MNode;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v17, Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/util/Parse;-><init>(Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 476
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_f

    .line 477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    .line 479
    :cond_f
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v16

    if-eqz v16, :cond_10

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "top parse: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 481
    :cond_10
    const/4 v13, 0x0

    .line 482
    .local v13, "treatFieldIdAsTopLevel":Z
    if-eqz v9, :cond_13

    .line 483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v12, v0, v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->isSkipParseType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_12

    .line 484
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v16

    if-eqz v16, :cond_11

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "skipping DM because of fieldId "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " and parseType "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 485
    :cond_11
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    goto/16 :goto_0

    .line 488
    :cond_12
    const/4 v13, 0x1

    .line 492
    :cond_13
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->doPassthrough(Lcom/vlingo/dialog/DMContext;)Z

    move-result v16

    if-eqz v16, :cond_15

    .line 493
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v16

    if-eqz v16, :cond_14

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "DM passthrough for fieldId="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " and parseType="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 494
    :cond_14
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    .line 495
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isPassthrough:Z

    .line 496
    new-instance v16, Lcom/vlingo/dialog/goal/model/PassthroughGoal;

    invoke-direct/range {v16 .. v16}, Lcom/vlingo/dialog/goal/model/PassthroughGoal;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    goto/16 :goto_0

    .line 500
    :cond_15
    if-nez v8, :cond_16

    if-nez v9, :cond_16

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->willHijackParse(Lcom/vlingo/dialog/DMContext;)Z

    move-result v16

    if-nez v16, :cond_16

    .line 501
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    goto/16 :goto_0

    .line 505
    :cond_16
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/vlingo/dialog/DMContext;->addUtteranceGoal(Ljava/lang/String;)V

    .line 507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    move-object/from16 v16, v0

    if-eqz v16, :cond_1

    .line 508
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->modeConversion()V

    .line 509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v4

    .line 510
    .local v4, "activePath":Ljava/lang/String;
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v16

    if-eqz v16, :cond_17

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "active: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 511
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 512
    .local v2, "active":Lcom/vlingo/dialog/model/IForm;
    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/dialog/DMContext;->hijack(Lcom/vlingo/dialog/manager/model/TopFormManager;)Z

    move-result v16

    if-nez v16, :cond_1

    .line 513
    invoke-virtual {v12, v4}, Lcom/vlingo/dialog/manager/model/TopFormManager;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v3

    .line 514
    .local v3, "activeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    if-nez v13, :cond_18

    invoke-virtual {v12}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1a

    if-eq v3, v12, :cond_1a

    .line 516
    :cond_18
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v16

    if-eqz v16, :cond_19

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "resetting to top level because fieldId "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 517
    :cond_19
    move-object v3, v12

    .line 518
    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 519
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 520
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->getFieldId()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    .line 524
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v16, v0

    if-eqz v16, :cond_1

    .line 527
    move-object/from16 v0, p0

    invoke-interface {v3, v0, v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0

    .line 522
    :cond_1a
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/vlingo/dialog/DMContext;->setSaneFieldId(Lcom/vlingo/dialog/manager/model/IFormManager;)V

    goto :goto_5
.end method

.method public processRecognition(Lcom/vlingo/voicepad/tagtoaction/model/Action;)V
    .locals 1
    .param p1, "nluAction"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .prologue
    .line 388
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/vlingo/dialog/DMContext;->processRecognition(Lcom/vlingo/common/message/MNode;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    .line 389
    return-void
.end method

.method public processRecognition(Ljava/lang/String;)V
    .locals 5
    .param p1, "nluActionString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 396
    invoke-static {}, Lcom/vlingo/common/message/XMLCodec;->getInstance()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v1

    .line 397
    .local v1, "codec":Lcom/vlingo/common/message/Codec;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    const-string/jumbo v4, "UTF-8"

    invoke-virtual {p1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 398
    .local v2, "is":Ljava/io/InputStream;
    invoke-interface {v1, v2}, Lcom/vlingo/common/message/Codec;->decode(Ljava/io/InputStream;)Lcom/vlingo/common/message/MNode;

    move-result-object v3

    .line 399
    .local v3, "root":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 400
    sget-object v4, Lcom/vlingo/dialog/DMContext;->metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    invoke-static {v3, v4}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .line 401
    .local v0, "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->processRecognition(Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    .line 402
    return-void
.end method

.method public removeUtteranceGoal()V
    .locals 3

    .prologue
    .line 645
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/goal/model/Goal;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 646
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/goal/model/Goal;

    .line 647
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/Goal;
    instance-of v2, v0, Lcom/vlingo/dialog/goal/model/UtteranceGoal;

    if-eqz v2, :cond_0

    .line 648
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 651
    .end local v0    # "goal":Lcom/vlingo/dialog/goal/model/Goal;
    :cond_1
    return-void
.end method

.method public resetToTop(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "manager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v3, 0x1

    .line 988
    invoke-interface {p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getTop()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/TopFormManager;

    .line 989
    .local v1, "topManager":Lcom/vlingo/dialog/manager/model/TopFormManager;
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 990
    .local v0, "topForm":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {v1, p0, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->removeTasks(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 991
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 992
    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/DMContext;->setFieldId(Ljava/lang/String;)V

    .line 993
    invoke-direct {p0, v3}, Lcom/vlingo/dialog/DMContext;->startTaskCleanup(Z)V

    .line 994
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/History;->reset()V

    .line 997
    iput-boolean v3, p0, Lcom/vlingo/dialog/DMContext;->wasResetToTop:Z

    .line 998
    return-void
.end method

.method public setActiveForm(Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 563
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->wasResetToTop:Z

    if-nez v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/state/model/State;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 566
    :cond_0
    return-void
.end method

.method public setActiveParse(Lcom/vlingo/dialog/util/Parse;)V
    .locals 0
    .param p1, "activeParse"    # Lcom/vlingo/dialog/util/Parse;

    .prologue
    .line 589
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    .line 590
    return-void
.end method

.method public setAnnotateContacts(Z)V
    .locals 0
    .param p1, "annotateContacts"    # Z

    .prologue
    .line 230
    iput-boolean p1, p0, Lcom/vlingo/dialog/DMContext;->annotateContacts:Z

    .line 231
    return-void
.end method

.method public setConfirmCancel()V
    .locals 2

    .prologue
    .line 778
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "cancel"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->setConfirm(Ljava/lang/String;)V

    .line 779
    return-void
.end method

.method public setConfirmExecute()V
    .locals 2

    .prologue
    .line 774
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "execute"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->setConfirm(Ljava/lang/String;)V

    .line 775
    return-void
.end method

.method public setContactClearCache(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 962
    if-eqz p1, :cond_0

    .line 963
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "contact_clear_cache"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 967
    :goto_0
    return-void

    .line 965
    :cond_0
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "contact_clear_cache"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDialogMode(Ljava/lang/String;)V
    .locals 0
    .param p1, "dialogMode"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    .line 184
    return-void
.end method

.method public setFieldId(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 573
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    .line 574
    return-void
.end method

.method public setListPosition(Lcom/vlingo/dialog/manager/model/ListPosition;)V
    .locals 0
    .param p1, "listPosition"    # Lcom/vlingo/dialog/manager/model/ListPosition;

    .prologue
    .line 1001
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    .line 1002
    return-void
.end method

.method public setListSize(I)V
    .locals 1
    .param p1, "listSize"    # I

    .prologue
    .line 1025
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    .line 1026
    return-void
.end method

.method public setNeedRecognition(Z)V
    .locals 0
    .param p1, "needRecognition"    # Z

    .prologue
    .line 264
    iput-boolean p1, p0, Lcom/vlingo/dialog/DMContext;->needRecognition:Z

    .line 265
    return-void
.end method

.method public setSaneFieldId(Lcom/vlingo/dialog/manager/model/IFormManager;)V
    .locals 5
    .param p1, "manager"    # Lcom/vlingo/dialog/manager/model/IFormManager;

    .prologue
    .line 539
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v0

    .line 540
    .local v0, "incomingFieldId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 541
    invoke-interface {p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v1

    .line 542
    .local v1, "myFieldId":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1, v0}, Lcom/vlingo/dialog/DMContext;->anyCompatibleFieldId(Lcom/vlingo/dialog/manager/model/IFormManager;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 543
    sget-object v2, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "incompatible fieldId ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "): setting to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 544
    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/DMContext;->setFieldId(Ljava/lang/String;)V

    .line 547
    .end local v1    # "myFieldId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setSoftwareVersion(Ljava/lang/String;)V
    .locals 3
    .param p1, "softwareVersion"    # Ljava/lang/String;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->softwareVersion:Ljava/lang/String;

    .line 216
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getContactAnnotateMinSoftwareVersion()Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "contactAnnotateMinSoftwareVersion":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 218
    if-eqz p1, :cond_1

    invoke-static {p1, v0}, Lcom/vlingo/voicepad/workflow/WorkflowManager;->compareVersions(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_1

    const/4 v1, 0x1

    .line 221
    .local v1, "doAnnotateContacts":Z
    :goto_0
    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/DMContext;->setAnnotateContacts(Z)V

    .line 223
    .end local v1    # "doAnnotateContacts":Z
    :cond_0
    return-void

    .line 218
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setupTopLevelState()V
    .locals 3

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->isConfirmCancel()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->isConfirmExecute()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 240
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "clearing extraneous confirm state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/State;->getConfirm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 241
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->clearConfirm()V

    .line 243
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->clearConfirm()V

    .line 244
    return-void
.end method

.method public startOutsideTask(Lcom/vlingo/dialog/util/Parse;)V
    .locals 1
    .param p1, "parse"    # Lcom/vlingo/dialog/util/Parse;

    .prologue
    .line 247
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    .line 248
    invoke-virtual {p1}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->startOutsideTask(Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->goalsFinished:Z

    .line 256
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    .line 257
    return-void
.end method

.method public startOutsideTask(Ljava/lang/String;)V
    .locals 4
    .param p1, "parseType"    # Ljava/lang/String;

    .prologue
    .line 618
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "starting outside task for parse type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 619
    :cond_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/State;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 620
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "bad active form for outside task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v3}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " - resetting to top"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 621
    :cond_1
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/State;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 623
    :cond_2
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/vlingo/dialog/DMContext;->startTaskCleanup(Z)V

    .line 624
    new-instance v0, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;

    invoke-direct {v0}, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;-><init>()V

    .line 625
    .local v0, "item":Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;
    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;->setTaskName(Ljava/lang/String;)V

    .line 626
    invoke-direct {p0, v0}, Lcom/vlingo/dialog/DMContext;->addToHistory(Lcom/vlingo/dialog/state/model/HistoryItem;)V

    .line 627
    return-void
.end method

.method public startTask(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "taskName"    # Ljava/lang/String;
    .param p2, "clearCachedDateTime"    # Z

    .prologue
    .line 630
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "starting task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 631
    :cond_0
    invoke-direct {p0, p2}, Lcom/vlingo/dialog/DMContext;->startTaskCleanup(Z)V

    .line 632
    new-instance v0, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;

    invoke-direct {v0}, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;-><init>()V

    .line 633
    .local v0, "item":Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;
    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;->setTaskName(Ljava/lang/String;)V

    .line 634
    invoke-direct {p0, v0}, Lcom/vlingo/dialog/DMContext;->addToHistory(Lcom/vlingo/dialog/state/model/HistoryItem;)V

    .line 635
    invoke-static {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->clearDisambig(Lcom/vlingo/dialog/DMContext;)V

    .line 636
    return-void
.end method

.method public stateWasReset()Z
    .locals 1

    .prologue
    .line 878
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->stateWasReset:Z

    return v0
.end method
