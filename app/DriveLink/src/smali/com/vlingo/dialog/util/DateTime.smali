.class public Lcom/vlingo/dialog/util/DateTime;
.super Ljava/lang/Object;
.source "DateTime.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/util/DateTime$Range;
    }
.end annotation


# static fields
.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final DATE_TIME_FORMAT:Ljava/text/DateFormat;

.field private static final DAY_FORMAT:Ljava/text/DateFormat;

.field public static final LOCALE:Ljava/util/Locale;

.field private static final MONTH_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TIME_FORMAT:Ljava/text/DateFormat;

.field private static final X_VLDM_CLIENT_TIME_FORMAT:Ljava/text/DateFormat;


# instance fields
.field private calendar:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    .line 32
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss"

    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/util/DateTime;->X_VLDM_CLIENT_TIME_FORMAT:Ljava/text/DateFormat;

    .line 35
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd"

    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/util/DateTime;->DATE_FORMAT:Ljava/text/DateFormat;

    .line 36
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH:mm"

    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/util/DateTime;->TIME_FORMAT:Ljava/text/DateFormat;

    .line 39
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm"

    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/util/DateTime;->DATE_TIME_FORMAT:Ljava/text/DateFormat;

    .line 40
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEE"

    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/util/DateTime;->DAY_FORMAT:Ljava/text/DateFormat;

    .line 42
    new-instance v0, Lcom/google/common/collect/ImmutableMap$Builder;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableMap$Builder;-><init>()V

    const-string/jumbo v1, "january"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "february"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "march"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "april"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "may"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "june"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "july"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "august"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "september"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "october"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "november"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string/jumbo v1, "december"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/util/DateTime;->MONTH_MAP:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "calendar"    # Ljava/util/Calendar;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    .line 83
    return-void
.end method

.method private constructor <init>(Ljava/util/Date;)V
    .locals 1
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    sget-object v0, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    .line 78
    iget-object v0, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 79
    return-void
.end method

.method private static format(Ljava/text/DateFormat;Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .param p0, "format"    # Ljava/text/DateFormat;
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 285
    monitor-enter p0

    .line 286
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 287
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static fromClientTime(Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;
    .locals 2
    .param p0, "clientDateTime"    # Ljava/lang/String;

    .prologue
    .line 60
    sget-object v1, Lcom/vlingo/dialog/util/DateTime;->X_VLDM_CLIENT_TIME_FORMAT:Ljava/text/DateFormat;

    invoke-static {v1, p0}, Lcom/vlingo/dialog/util/DateTime;->parse(Ljava/text/DateFormat;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 61
    .local v0, "d":Ljava/util/Date;
    new-instance v1, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v1, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Date;)V

    return-object v1
.end method

.method public static fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;
    .locals 4
    .param p0, "base"    # Lcom/vlingo/dialog/util/DateTime;
    .param p1, "date"    # Ljava/lang/String;
    .param p2, "time"    # Ljava/lang/String;

    .prologue
    .line 65
    if-nez p1, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object p1

    .line 68
    :cond_0
    if-nez p2, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object p2

    .line 71
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "s":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->DATE_TIME_FORMAT:Ljava/text/DateFormat;

    invoke-static {v2, v1}, Lcom/vlingo/dialog/util/DateTime;->parse(Ljava/text/DateFormat;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 73
    .local v0, "d":Ljava/util/Date;
    new-instance v2, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v2, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Date;)V

    return-object v2
.end method

.method public static main([Ljava/lang/String;)V
    .locals 6
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 305
    const-string/jumbo v1, "2014-03-20 14:52:34"

    invoke-static {v1}, Lcom/vlingo/dialog/util/DateTime;->fromClientTime(Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    .line 306
    .local v0, "now":Lcom/vlingo/dialog/util/DateTime;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v2, "2014"

    invoke-virtual {v0, v2, v4, v3}, Lcom/vlingo/dialog/util/DateTime;->getYear(Ljava/lang/String;IZ)Lcom/vlingo/dialog/util/DateTime$Range;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 307
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v2, "2015"

    invoke-virtual {v0, v2, v4, v3}, Lcom/vlingo/dialog/util/DateTime;->getYear(Ljava/lang/String;IZ)Lcom/vlingo/dialog/util/DateTime$Range;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 308
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, v5, v4, v3}, Lcom/vlingo/dialog/util/DateTime;->getYear(Ljava/lang/String;IZ)Lcom/vlingo/dialog/util/DateTime$Range;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 309
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, v5, v3, v3}, Lcom/vlingo/dialog/util/DateTime;->getYear(Ljava/lang/String;IZ)Lcom/vlingo/dialog/util/DateTime$Range;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 310
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v2, 0x2

    invoke-virtual {v0, v5, v2, v3}, Lcom/vlingo/dialog/util/DateTime;->getYear(Ljava/lang/String;IZ)Lcom/vlingo/dialog/util/DateTime$Range;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 311
    return-void
.end method

.method private static parse(Ljava/text/DateFormat;Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .param p0, "format"    # Ljava/text/DateFormat;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 275
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    :try_start_1
    invoke-virtual {p0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    monitor-exit p0

    .line 280
    :goto_0
    return-object v1

    .line 277
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_0

    .line 278
    :catch_0
    move-exception v0

    .line 279
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    .line 280
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static setEndOfDay(Ljava/util/Calendar;)V
    .locals 3
    .param p0, "c"    # Ljava/util/Calendar;

    .prologue
    const/16 v2, 0x3b

    .line 298
    const/16 v0, 0xb

    const/16 v1, 0x17

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 299
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 300
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 301
    const/16 v0, 0xe

    const/16 v1, 0x3e7

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 302
    return-void
.end method

.method private static setStartOfDay(Ljava/util/Calendar;)V
    .locals 2
    .param p0, "c"    # Ljava/util/Calendar;

    .prologue
    const/4 v1, 0x0

    .line 291
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 292
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 293
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 294
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 295
    return-void
.end method


# virtual methods
.method public addDays(I)Lcom/vlingo/dialog/util/DateTime;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 134
    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 135
    .local v0, "c":Ljava/util/Calendar;
    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->add(II)V

    .line 136
    new-instance v1, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v1, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    return-object v1
.end method

.method public addMinutes(I)Lcom/vlingo/dialog/util/DateTime;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 140
    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 141
    .local v0, "c":Ljava/util/Calendar;
    const/16 v1, 0xc

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->add(II)V

    .line 142
    new-instance v1, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v1, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    return-object v1
.end method

.method public addMonths(I)Lcom/vlingo/dialog/util/DateTime;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 128
    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 129
    .local v0, "c":Ljava/util/Calendar;
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->add(II)V

    .line 130
    new-instance v1, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v1, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    return-object v1
.end method

.method public addYears(I)Lcom/vlingo/dialog/util/DateTime;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 122
    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 123
    .local v0, "c":Ljava/util/Calendar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->add(II)V

    .line 124
    new-instance v1, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v1, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    return-object v1
.end method

.method public endOfDay()Lcom/vlingo/dialog/util/DateTime;
    .locals 2

    .prologue
    .line 112
    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 113
    .local v0, "c":Ljava/util/Calendar;
    invoke-static {v0}, Lcom/vlingo/dialog/util/DateTime;->setEndOfDay(Ljava/util/Calendar;)V

    .line 114
    new-instance v1, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v1, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    return-object v1
.end method

.method public getCalendar()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    return-object v0
.end method

.method public getClientDateTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lcom/vlingo/dialog/util/DateTime;->X_VLDM_CLIENT_TIME_FORMAT:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    sget-object v0, Lcom/vlingo/dialog/util/DateTime;->DATE_FORMAT:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/util/DateTime;->format(Ljava/text/DateFormat;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDateTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    sget-object v0, Lcom/vlingo/dialog/util/DateTime;->DATE_TIME_FORMAT:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDay()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/dialog/util/DateTime;->DAY_FORMAT:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/util/DateTime;->format(Ljava/text/DateFormat;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMonth(Ljava/lang/String;IZ)Lcom/vlingo/dialog/util/DateTime$Range;
    .locals 12
    .param p1, "month"    # Ljava/lang/String;
    .param p2, "m"    # I
    .param p3, "after"    # Z

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x6

    const/4 v9, -0x1

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 211
    const/4 v2, 0x0

    .line 212
    .local v2, "monthIndex":Ljava/lang/Integer;
    if-eqz p1, :cond_0

    .line 213
    sget-object v3, Lcom/vlingo/dialog/util/DateTime;->MONTH_MAP:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "monthIndex":Ljava/lang/Integer;
    check-cast v2, Ljava/lang/Integer;

    .line 214
    .restart local v2    # "monthIndex":Ljava/lang/Integer;
    if-nez v2, :cond_0

    .line 215
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "bad month = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 218
    :cond_0
    iget-object v3, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 219
    .local v0, "begin":Ljava/util/Calendar;
    if-eqz v2, :cond_1

    .line 220
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v8, v3}, Ljava/util/Calendar;->set(II)V

    .line 222
    :cond_1
    invoke-virtual {v0, v11, v7}, Ljava/util/Calendar;->set(II)V

    .line 223
    invoke-static {v0}, Lcom/vlingo/dialog/util/DateTime;->setStartOfDay(Ljava/util/Calendar;)V

    .line 224
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    .line 225
    .local v1, "end":Ljava/util/Calendar;
    invoke-virtual {v1, v8, v7}, Ljava/util/Calendar;->add(II)V

    .line 226
    invoke-virtual {v1, v10, v9}, Ljava/util/Calendar;->add(II)V

    .line 227
    invoke-static {v1}, Lcom/vlingo/dialog/util/DateTime;->setEndOfDay(Ljava/util/Calendar;)V

    .line 228
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    iget-object v5, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_2

    .line 230
    invoke-virtual {v0, v7, v7}, Ljava/util/Calendar;->add(II)V

    .line 231
    invoke-virtual {v1, v7, v7}, Ljava/util/Calendar;->add(II)V

    .line 232
    invoke-virtual {v1, v8, v7}, Ljava/util/Calendar;->add(II)V

    .line 233
    invoke-virtual {v1, v11, v7}, Ljava/util/Calendar;->set(II)V

    .line 234
    invoke-virtual {v1, v10, v9}, Ljava/util/Calendar;->add(II)V

    .line 235
    invoke-static {v1}, Lcom/vlingo/dialog/util/DateTime;->setEndOfDay(Ljava/util/Calendar;)V

    .line 237
    :cond_2
    if-lez p2, :cond_3

    .line 238
    if-eqz v2, :cond_5

    .line 239
    invoke-virtual {v0, v7, p2}, Ljava/util/Calendar;->add(II)V

    .line 243
    :goto_0
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "end":Ljava/util/Calendar;
    check-cast v1, Ljava/util/Calendar;

    .line 244
    .restart local v1    # "end":Ljava/util/Calendar;
    invoke-virtual {v1, v8, v7}, Ljava/util/Calendar;->add(II)V

    .line 245
    invoke-virtual {v1, v10, v9}, Ljava/util/Calendar;->add(II)V

    .line 246
    invoke-static {v1}, Lcom/vlingo/dialog/util/DateTime;->setEndOfDay(Ljava/util/Calendar;)V

    .line 248
    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    iget-object v5, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_4

    .line 249
    iget-object v3, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 251
    :cond_4
    new-instance v3, Lcom/vlingo/dialog/util/DateTime$Range;

    new-instance v4, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v4, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    new-instance v5, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v5, v1}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    invoke-direct {v3, v4, v5}, Lcom/vlingo/dialog/util/DateTime$Range;-><init>(Lcom/vlingo/dialog/util/DateTime;Lcom/vlingo/dialog/util/DateTime;)V

    return-object v3

    .line 241
    :cond_5
    invoke-virtual {v0, v8, p2}, Ljava/util/Calendar;->add(II)V

    goto :goto_0
.end method

.method public getTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    sget-object v0, Lcom/vlingo/dialog/util/DateTime;->TIME_FORMAT:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/util/DateTime;->format(Ljava/text/DateFormat;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWeek(IIZ)Lcom/vlingo/dialog/util/DateTime$Range;
    .locals 7
    .param p1, "firstDayOfWeek"    # I
    .param p2, "n"    # I
    .param p3, "after"    # Z

    .prologue
    const/4 v6, 0x6

    .line 146
    iget-object v4, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 147
    .local v1, "dayOfWeek":I
    sub-int v2, p1, v1

    .line 148
    .local v2, "dayOffset":I
    if-lez v2, :cond_0

    .line 149
    add-int/lit8 v2, v2, -0x7

    .line 151
    :cond_0
    mul-int/lit8 v4, p2, 0x7

    add-int/2addr v2, v4

    .line 152
    iget-object v4, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 153
    .local v0, "begin":Ljava/util/Calendar;
    invoke-static {v0}, Lcom/vlingo/dialog/util/DateTime;->setStartOfDay(Ljava/util/Calendar;)V

    .line 154
    invoke-virtual {v0, v6, v2}, Ljava/util/Calendar;->add(II)V

    .line 155
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Calendar;

    .line 156
    .local v3, "end":Ljava/util/Calendar;
    invoke-static {v3}, Lcom/vlingo/dialog/util/DateTime;->setEndOfDay(Ljava/util/Calendar;)V

    .line 157
    invoke-virtual {v3, v6, v6}, Ljava/util/Calendar;->add(II)V

    .line 158
    if-eqz p3, :cond_1

    if-nez p2, :cond_1

    .line 159
    iget-object v4, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 161
    :cond_1
    new-instance v4, Lcom/vlingo/dialog/util/DateTime$Range;

    new-instance v5, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v5, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    new-instance v6, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v6, v3}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    invoke-direct {v4, v5, v6}, Lcom/vlingo/dialog/util/DateTime$Range;-><init>(Lcom/vlingo/dialog/util/DateTime;Lcom/vlingo/dialog/util/DateTime;)V

    return-object v4
.end method

.method public getWeekend(IIZ)Lcom/vlingo/dialog/util/DateTime$Range;
    .locals 8
    .param p1, "firstDayOfWeekend"    # I
    .param p2, "n"    # I
    .param p3, "after"    # Z

    .prologue
    const/4 v6, 0x6

    .line 165
    iget-object v4, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 166
    .local v1, "dayOfWeek":I
    sub-int v2, p1, v1

    .line 167
    .local v2, "dayOffset":I
    const/4 v4, -0x1

    if-ge v2, v4, :cond_2

    .line 168
    add-int/lit8 v2, v2, 0x7

    .line 172
    :cond_0
    :goto_0
    mul-int/lit8 v4, p2, 0x7

    add-int/2addr v2, v4

    .line 173
    iget-object v4, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 174
    .local v0, "begin":Ljava/util/Calendar;
    invoke-static {v0}, Lcom/vlingo/dialog/util/DateTime;->setStartOfDay(Ljava/util/Calendar;)V

    .line 175
    invoke-virtual {v0, v6, v2}, Ljava/util/Calendar;->add(II)V

    .line 176
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Calendar;

    .line 177
    .local v3, "end":Ljava/util/Calendar;
    invoke-static {v3}, Lcom/vlingo/dialog/util/DateTime;->setEndOfDay(Ljava/util/Calendar;)V

    .line 178
    const/4 v4, 0x1

    invoke-virtual {v3, v6, v4}, Ljava/util/Calendar;->add(II)V

    .line 179
    if-eqz p3, :cond_1

    if-nez p2, :cond_1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    .line 180
    iget-object v4, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 182
    :cond_1
    new-instance v4, Lcom/vlingo/dialog/util/DateTime$Range;

    new-instance v5, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v5, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    new-instance v6, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v6, v3}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    invoke-direct {v4, v5, v6}, Lcom/vlingo/dialog/util/DateTime$Range;-><init>(Lcom/vlingo/dialog/util/DateTime;Lcom/vlingo/dialog/util/DateTime;)V

    return-object v4

    .line 169
    .end local v0    # "begin":Ljava/util/Calendar;
    .end local v3    # "end":Ljava/util/Calendar;
    :cond_2
    const/4 v4, 0x5

    if-le v2, v4, :cond_0

    .line 170
    add-int/lit8 v2, v2, -0x7

    goto :goto_0
.end method

.method public getYear(Ljava/lang/String;IZ)Lcom/vlingo/dialog/util/DateTime$Range;
    .locals 6
    .param p1, "year"    # Ljava/lang/String;
    .param p2, "y"    # I
    .param p3, "after"    # Z

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 189
    iget-object v2, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 190
    .local v0, "begin":Ljava/util/Calendar;
    const/4 v2, 0x0

    invoke-virtual {v0, v4, v2}, Ljava/util/Calendar;->set(II)V

    .line 191
    invoke-virtual {v0, v5, v3}, Ljava/util/Calendar;->set(II)V

    .line 192
    invoke-static {v0}, Lcom/vlingo/dialog/util/DateTime;->setStartOfDay(Ljava/util/Calendar;)V

    .line 193
    if-eqz p1, :cond_1

    .line 194
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 198
    :goto_0
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    .line 199
    .local v1, "end":Ljava/util/Calendar;
    const/16 v2, 0xb

    invoke-virtual {v1, v4, v2}, Ljava/util/Calendar;->set(II)V

    .line 200
    const/16 v2, 0x1f

    invoke-virtual {v1, v5, v2}, Ljava/util/Calendar;->set(II)V

    .line 201
    invoke-static {v1}, Lcom/vlingo/dialog/util/DateTime;->setEndOfDay(Ljava/util/Calendar;)V

    .line 202
    if-eqz p3, :cond_0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 203
    iget-object v2, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 205
    :cond_0
    new-instance v2, Lcom/vlingo/dialog/util/DateTime$Range;

    new-instance v3, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v3, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    new-instance v4, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v4, v1}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    invoke-direct {v2, v3, v4}, Lcom/vlingo/dialog/util/DateTime$Range;-><init>(Lcom/vlingo/dialog/util/DateTime;Lcom/vlingo/dialog/util/DateTime;)V

    return-object v2

    .line 196
    .end local v1    # "end":Ljava/util/Calendar;
    :cond_1
    invoke-virtual {v0, v3, p2}, Ljava/util/Calendar;->add(II)V

    goto :goto_0
.end method

.method public isBefore(Lcom/vlingo/dialog/util/DateTime;)Z
    .locals 4
    .param p1, "other"    # Lcom/vlingo/dialog/util/DateTime;

    .prologue
    .line 261
    iget-object v0, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p1, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public minutesUntil(Lcom/vlingo/dialog/util/DateTime;)I
    .locals 6
    .param p1, "other"    # Lcom/vlingo/dialog/util/DateTime;

    .prologue
    .line 255
    const v0, 0xea60

    .line 256
    .local v0, "MINUTES_PER_MILLISECOND":I
    iget-object v2, p1, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v1, v2

    .line 257
    .local v1, "ms":I
    const v2, 0xea60

    div-int v2, v1, v2

    return v2
.end method

.method public startOfDay()Lcom/vlingo/dialog/util/DateTime;
    .locals 2

    .prologue
    .line 106
    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 107
    .local v0, "c":Ljava/util/Calendar;
    invoke-static {v0}, Lcom/vlingo/dialog/util/DateTime;->setStartOfDay(Ljava/util/Calendar;)V

    .line 108
    new-instance v1, Lcom/vlingo/dialog/util/DateTime;

    invoke-direct {v1, v0}, Lcom/vlingo/dialog/util/DateTime;-><init>(Ljava/util/Calendar;)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tomorrow()Lcom/vlingo/dialog/util/DateTime;
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    return-object v0
.end method
