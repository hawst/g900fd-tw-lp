.class public abstract Lcom/vlingo/dialog/util/EditDistance$Cost;
.super Ljava/lang/Object;
.source "EditDistance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/util/EditDistance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Cost"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static final equalIgnoreCase(CC)Z
    .locals 3
    .param p0, "x"    # C
    .param p1, "y"    # C

    .prologue
    .line 34
    invoke-static {p0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "sx":Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    .line 36
    .local v1, "sy":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    return v2
.end method


# virtual methods
.method public abstract del(C)I
.end method

.method public abstract ins(C)I
.end method

.method public abstract sub(CC)I
.end method
