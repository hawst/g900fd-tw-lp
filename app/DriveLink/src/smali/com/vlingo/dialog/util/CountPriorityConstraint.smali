.class public Lcom/vlingo/dialog/util/CountPriorityConstraint;
.super Ljava/lang/Object;
.source "CountPriorityConstraint.java"


# static fields
.field private static final CHOOSE:[[I

.field private static final logger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 15
    const-class v0, Lcom/vlingo/dialog/util/CountPriorityConstraint;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/util/CountPriorityConstraint;->logger:Lcom/vlingo/common/log4j/VLogger;

    .line 17
    const/16 v0, 0xb

    new-array v0, v0, [[I

    new-array v1, v2, [I

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    new-array v1, v4, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v3

    new-array v1, v5, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v4

    new-array v1, v6, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v5

    const/4 v1, 0x6

    new-array v1, v1, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v6

    const/4 v1, 0x6

    const/4 v2, 0x7

    new-array v2, v2, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x8

    new-array v2, v2, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x9

    new-array v2, v2, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0xa

    new-array v2, v2, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0xb

    new-array v2, v2, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/util/CountPriorityConstraint;->CHOOSE:[[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x1
    .end array-data

    :array_1
    .array-data 4
        0x1
        0x2
        0x1
    .end array-data

    :array_2
    .array-data 4
        0x1
        0x3
        0x3
        0x1
    .end array-data

    :array_3
    .array-data 4
        0x1
        0x4
        0x6
        0x4
        0x1
    .end array-data

    :array_4
    .array-data 4
        0x1
        0x5
        0xa
        0xa
        0x5
        0x1
    .end array-data

    :array_5
    .array-data 4
        0x1
        0x6
        0xf
        0x14
        0xf
        0x6
        0x1
    .end array-data

    :array_6
    .array-data 4
        0x1
        0x7
        0x15
        0x23
        0x23
        0x15
        0x7
        0x1
    .end array-data

    :array_7
    .array-data 4
        0x1
        0x8
        0x1c
        0x38
        0x46
        0x38
        0x1c
        0x8
        0x1
    .end array-data

    :array_8
    .array-data 4
        0x1
        0x9
        0x24
        0x54
        0x7e
        0x7e
        0x54
        0x24
        0x9
        0x1
    .end array-data

    :array_9
    .array-data 4
        0x1
        0xa
        0x2d
        0x78
        0xd2
        0xfc
        0xd2
        0x78
        0x2d
        0xa
        0x1
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static choose(II)I
    .locals 5
    .param p0, "n"    # I
    .param p1, "k"    # I

    .prologue
    .line 141
    if-ltz p0, :cond_0

    if-gez p1, :cond_2

    .line 142
    :cond_0
    const/4 v0, -0x1

    .line 169
    :cond_1
    :goto_0
    return v0

    .line 143
    :cond_2
    if-ge p0, p1, :cond_3

    .line 144
    const/4 v0, 0x0

    goto :goto_0

    .line 145
    :cond_3
    if-eq p0, p1, :cond_4

    if-nez p1, :cond_5

    .line 146
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 149
    :cond_5
    sget-object v4, Lcom/vlingo/dialog/util/CountPriorityConstraint;->CHOOSE:[[I

    array-length v4, v4

    if-ge p0, v4, :cond_6

    .line 150
    sget-object v4, Lcom/vlingo/dialog/util/CountPriorityConstraint;->CHOOSE:[[I

    aget-object v4, v4, p0

    aget v0, v4, p1

    goto :goto_0

    .line 155
    :cond_6
    sub-int v4, p0, p1

    if-ge p1, v4, :cond_7

    .line 156
    sub-int v1, p0, p1

    .line 157
    .local v1, "delta":I
    move v3, p1

    .line 163
    .local v3, "iMax":I
    :goto_1
    add-int/lit8 v0, v1, 0x1

    .line 165
    .local v0, "ans":I
    const/4 v2, 0x2

    .local v2, "i":I
    :goto_2
    if-gt v2, v3, :cond_1

    .line 166
    add-int v4, v1, v2

    mul-int/2addr v4, v0

    div-int v0, v4, v2

    .line 165
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 159
    .end local v0    # "ans":I
    .end local v1    # "delta":I
    .end local v2    # "i":I
    .end local v3    # "iMax":I
    :cond_7
    move v1, p1

    .line 160
    .restart local v1    # "delta":I
    sub-int v3, p0, p1

    .restart local v3    # "iMax":I
    goto :goto_1
.end method

.method private static element(III)[I
    .locals 6
    .param p0, "n"    # I
    .param p1, "k"    # I
    .param p2, "m"    # I

    .prologue
    .line 109
    new-array v1, p1, [I

    .line 111
    .local v1, "ans":[I
    move v0, p0

    .line 112
    .local v0, "a":I
    move v2, p1

    .line 113
    .local v2, "b":I
    invoke-static {p0, p1}, Lcom/vlingo/dialog/util/CountPriorityConstraint;->choose(II)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    sub-int v4, v5, p2

    .line 115
    .local v4, "x":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, p1, :cond_0

    .line 116
    invoke-static {v0, v2, v4}, Lcom/vlingo/dialog/util/CountPriorityConstraint;->largestV(III)I

    move-result v0

    .line 117
    invoke-static {v0, v2}, Lcom/vlingo/dialog/util/CountPriorityConstraint;->choose(II)I

    move-result v5

    sub-int/2addr v4, v5

    .line 118
    add-int/lit8 v2, v2, -0x1

    .line 119
    add-int/lit8 v5, p0, -0x1

    sub-int/2addr v5, v0

    aput v5, v1, v3

    .line 115
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 122
    :cond_0
    return-object v1
.end method

.method public static getNthOrderedSubset(Ljava/util/List;I)Ljava/util/List;
    .locals 12
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "constraints":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v7

    .line 69
    .local v7, "n":I
    if-ltz p1, :cond_0

    const/4 v11, 0x1

    shl-int/2addr v11, v7

    if-lt p1, v11, :cond_2

    .line 70
    :cond_0
    const/4 v9, 0x0

    .line 90
    :cond_1
    return-object v9

    .line 73
    :cond_2
    move v4, v7

    .line 74
    .local v4, "k":I
    const/4 v10, 0x0

    .line 75
    .local v10, "sum":I
    const/4 v8, 0x0

    .line 76
    .local v8, "offset":I
    const/4 v5, 0x1

    .line 77
    .local v5, "lastChoose":I
    :goto_0
    if-le p1, v10, :cond_3

    .line 78
    add-int/2addr v8, v5

    .line 79
    add-int/lit8 v4, v4, -0x1

    invoke-static {v7, v4}, Lcom/vlingo/dialog/util/CountPriorityConstraint;->choose(II)I

    move-result v5

    .line 80
    add-int/2addr v10, v5

    goto :goto_0

    .line 83
    :cond_3
    sub-int v11, p1, v8

    invoke-static {v7, v4, v11}, Lcom/vlingo/dialog/util/CountPriorityConstraint;->element(III)[I

    move-result-object v1

    .line 85
    .local v1, "e":[I
    new-instance v9, Ljava/util/ArrayList;

    array-length v11, v1

    invoke-direct {v9, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 86
    .local v9, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, v1

    .local v0, "arr$":[I
    array-length v6, v0

    .local v6, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v6, :cond_1

    aget v2, v0, v3

    .line 87
    .local v2, "i":I
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private static largestV(III)I
    .locals 2
    .param p0, "a"    # I
    .param p1, "b"    # I
    .param p2, "x"    # I

    .prologue
    .line 129
    add-int/lit8 v0, p0, -0x1

    .line 131
    .local v0, "v":I
    :goto_0
    invoke-static {v0, p1}, Lcom/vlingo/dialog/util/CountPriorityConstraint;->choose(II)I

    move-result v1

    if-le v1, p2, :cond_0

    .line 132
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 134
    :cond_0
    return v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 8
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 55
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "A"

    aput-object v6, v4, v5

    const-string/jumbo v5, "B"

    aput-object v5, v4, v7

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 57
    .local v0, "constraints":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v4, Lcom/vlingo/dialog/util/CountPriorityConstraint;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v5, "start"

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 58
    invoke-static {v0}, Lcom/vlingo/dialog/util/CountPriorityConstraint;->orderedSubsets(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 59
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    sget-object v4, Lcom/vlingo/dialog/util/CountPriorityConstraint;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 61
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    shl-int v2, v7, v4

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 62
    sget-object v4, Lcom/vlingo/dialog/util/CountPriorityConstraint;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-static {v0, v1}, Lcom/vlingo/dialog/util/CountPriorityConstraint;->getNthOrderedSubset(Ljava/util/List;I)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 61
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    :cond_0
    return-void
.end method

.method public static orderedSubsets(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "constraints":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    shl-int v1, v3, v4

    .line 46
    .local v1, "n":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 47
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 48
    invoke-static {p0, v0}, Lcom/vlingo/dialog/util/CountPriorityConstraint;->getNthOrderedSubset(Ljava/util/List;I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_0
    return-object v2
.end method
