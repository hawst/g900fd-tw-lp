.class public Lcom/vlingo/dialog/util/Parse;
.super Ljava/lang/Object;
.source "Parse.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/vlingo/dialog/util/Parse;",
        ">;"
    }
.end annotation


# static fields
.field private static final ATTR_C:Ljava/lang/String; = "c"

.field private static final ATTR_CF:Ljava/lang/String; = "cf"

.field private static final ATTR_CSF:Ljava/lang/String; = "csf"

.field private static final ATTR_NAME:Ljava/lang/String; = "nm"

.field private static final ATTR_T:Ljava/lang/String; = "t"

.field private static final NODE_TAG:Ljava/lang/String; = "tag"

.field private static final NODE_TL:Ljava/lang/String; = "tl"


# instance fields
.field private consumed:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private input:Ljava/lang/String;

.field private score:F

.field private tagMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/common/message/MNode;Ljava/lang/String;)V
    .locals 11
    .param p1, "pgNode"    # Lcom/vlingo/common/message/MNode;
    .param p2, "input"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v8, Ljava/util/LinkedHashMap;

    invoke-direct {v8}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v8, p0, Lcom/vlingo/dialog/util/Parse;->tagMap:Ljava/util/Map;

    .line 35
    new-instance v8, Ljava/util/LinkedHashSet;

    invoke-direct {v8}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v8, p0, Lcom/vlingo/dialog/util/Parse;->consumed:Ljava/util/Set;

    .line 38
    const-string/jumbo v8, "t"

    invoke-virtual {p1, v8}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/dialog/util/Parse;->type:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/vlingo/dialog/util/Parse;->input:Ljava/lang/String;

    .line 41
    const-string/jumbo v8, "c"

    invoke-virtual {p1, v8}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "cValue":Ljava/lang/String;
    if-nez v0, :cond_2

    const/4 v8, 0x0

    :goto_0
    iput v8, p0, Lcom/vlingo/dialog/util/Parse;->score:F

    .line 44
    const-string/jumbo v8, "tag"

    invoke-virtual {p1, v8}, Lcom/vlingo/common/message/MNode;->findChildren(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/common/message/MNode;

    .line 45
    .local v5, "tagNode":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v8, "nm"

    invoke-virtual {v5, v8}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-virtual {v8, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 46
    .local v4, "tagName":Ljava/lang/String;
    const-string/jumbo v8, "tl"

    invoke-virtual {v5, v8}, Lcom/vlingo/common/message/MNode;->findFirstChild(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v7

    .line 47
    .local v7, "tlNode":Lcom/vlingo/common/message/MNode;
    invoke-static {v7}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->assembleStringFromWords(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v6

    .line 48
    .local v6, "tagValue":Ljava/lang/String;
    iget-object v8, p0, Lcom/vlingo/dialog/util/Parse;->tagMap:Ljava/util/Map;

    invoke-interface {v8, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string/jumbo v8, "cf"

    invoke-virtual {v7, v8}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "cfValue":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 51
    iget-object v8, p0, Lcom/vlingo/dialog/util/Parse;->tagMap:Ljava/util/Map;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ":cf"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    :cond_1
    const-string/jumbo v8, "csf"

    invoke-virtual {v7, v8}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "csfValue":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 55
    iget-object v8, p0, Lcom/vlingo/dialog/util/Parse;->tagMap:Ljava/util/Map;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ":csf"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 42
    .end local v1    # "cfValue":Ljava/lang/String;
    .end local v2    # "csfValue":Ljava/lang/String;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "tagName":Ljava/lang/String;
    .end local v5    # "tagNode":Lcom/vlingo/common/message/MNode;
    .end local v6    # "tagValue":Ljava/lang/String;
    .end local v7    # "tlNode":Lcom/vlingo/common/message/MNode;
    :cond_2
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    goto :goto_0

    .line 58
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/voicepad/tagtoaction/model/Action;)V
    .locals 5
    .param p1, "action"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v4, p0, Lcom/vlingo/dialog/util/Parse;->tagMap:Ljava/util/Map;

    .line 35
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v4, p0, Lcom/vlingo/dialog/util/Parse;->consumed:Ljava/util/Set;

    .line 61
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getParams()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;

    .line 62
    .local v2, "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getName()Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 64
    .local v3, "value":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string/jumbo v4, "utterance"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 65
    const-string/jumbo v4, "parse_type"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 66
    iput-object v3, p0, Lcom/vlingo/dialog/util/Parse;->type:Ljava/lang/String;

    goto :goto_0

    .line 68
    :cond_1
    iget-object v4, p0, Lcom/vlingo/dialog/util/Parse;->tagMap:Ljava/util/Map;

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 72
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    .end local v3    # "value":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "parseType"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/util/Parse;->tagMap:Ljava/util/Map;

    .line 35
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/util/Parse;->consumed:Ljava/util/Set;

    .line 75
    iput-object p1, p0, Lcom/vlingo/dialog/util/Parse;->type:Ljava/lang/String;

    .line 76
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/dialog/util/Parse;->input:Ljava/lang/String;

    .line 77
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/vlingo/dialog/util/Parse;)I
    .locals 2
    .param p1, "other"    # Lcom/vlingo/dialog/util/Parse;

    .prologue
    .line 110
    iget v0, p1, Lcom/vlingo/dialog/util/Parse;->score:F

    iget v1, p0, Lcom/vlingo/dialog/util/Parse;->score:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/vlingo/dialog/util/Parse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/util/Parse;->compareTo(Lcom/vlingo/dialog/util/Parse;)I

    move-result v0

    return v0
.end method

.method public consume(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/dialog/util/Parse;->consumed:Ljava/util/Set;

    sget-object v1, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/util/Parse;->get(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "evenIfConsumed"    # Z

    .prologue
    .line 88
    sget-object v0, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 89
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/vlingo/dialog/util/Parse;->consumed:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x0

    .line 92
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/dialog/util/Parse;->tagMap:Ljava/util/Map;

    sget-object v1, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getInput()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/dialog/util/Parse;->input:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vlingo/dialog/util/Parse;->type:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vlingo/dialog/util/Parse;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/dialog/util/Parse;->tagMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/dialog/util/Parse;->consumed:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
