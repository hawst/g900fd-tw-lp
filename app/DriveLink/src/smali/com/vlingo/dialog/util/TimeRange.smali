.class public Lcom/vlingo/dialog/util/TimeRange;
.super Ljava/lang/Object;
.source "TimeRange.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertEndTimeToDuration(Lcom/vlingo/dialog/util/DateTime;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 5
    .param p0, "clientDateTime"    # Lcom/vlingo/dialog/util/DateTime;
    .param p1, "dateSlot"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "timeSlot"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "durationSlot"    # Lcom/vlingo/dialog/model/IForm;
    .param p4, "endTimeSlot"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 10
    invoke-interface {p4}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 11
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v3, v4}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v2

    .line 12
    .local v2, "endDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 14
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 15
    .local v1, "duration":I
    neg-int v3, v1

    invoke-virtual {v2, v3}, Lcom/vlingo/dialog/util/DateTime;->addMinutes(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    .line 16
    .local v0, "beginDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v0}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 28
    .end local v0    # "beginDateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v1    # "duration":I
    :cond_0
    :goto_0
    const/4 v3, 0x0

    invoke-interface {p4, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 30
    .end local v2    # "endDateTime":Lcom/vlingo/dialog/util/DateTime;
    :cond_1
    return-void

    .line 17
    .restart local v2    # "endDateTime":Lcom/vlingo/dialog/util/DateTime;
    :cond_2
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 20
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v3, v4}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    .line 21
    .restart local v0    # "beginDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v2, v0}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 22
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v2

    .line 24
    :cond_3
    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/util/DateTime;->minutesUntil(Lcom/vlingo/dialog/util/DateTime;)I

    move-result v1

    .line 25
    .restart local v1    # "duration":I
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p3, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method
