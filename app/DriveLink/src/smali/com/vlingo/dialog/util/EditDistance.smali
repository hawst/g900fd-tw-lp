.class public Lcom/vlingo/dialog/util/EditDistance;
.super Ljava/lang/Object;
.source "EditDistance.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;,
        Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;,
        Lcom/vlingo/dialog/util/EditDistance$Cost;,
        Lcom/vlingo/dialog/util/EditDistance$Match;
    }
.end annotation


# instance fields
.field private cost:Lcom/vlingo/dialog/util/EditDistance$Cost;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    new-instance v0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;

    invoke-direct {v0}, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/util/EditDistance;->cost:Lcom/vlingo/dialog/util/EditDistance$Cost;

    .line 100
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/dialog/util/EditDistance$Cost;)V
    .locals 0
    .param p1, "cost"    # Lcom/vlingo/dialog/util/EditDistance$Cost;

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/vlingo/dialog/util/EditDistance;->cost:Lcom/vlingo/dialog/util/EditDistance$Cost;

    .line 104
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 16
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 155
    const-string/jumbo v7, "\u0130\u0131"

    .line 156
    .local v7, "s1":Ljava/lang/String;
    const-string/jumbo v8, "iI"

    .line 158
    .local v8, "s2":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "abc"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 159
    .local v6, "ref":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "abc"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 161
    .local v3, "hyp":Ljava/lang/String;
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "ref: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 162
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "hyp: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 164
    new-instance v9, Lcom/vlingo/dialog/util/EditDistance;

    new-instance v11, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;

    const/16 v12, 0x64

    invoke-direct {v11, v12}, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;-><init>(I)V

    invoke-direct {v9, v11}, Lcom/vlingo/dialog/util/EditDistance;-><init>(Lcom/vlingo/dialog/util/EditDistance$Cost;)V

    .line 165
    .local v9, "simpleDistance":Lcom/vlingo/dialog/util/EditDistance;
    new-instance v1, Lcom/vlingo/dialog/util/EditDistance;

    new-instance v11, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;

    const/16 v12, 0x1e

    const/16 v13, 0x64

    const/16 v14, 0x64

    invoke-direct {v11, v12, v13, v14}, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;-><init>(III)V

    invoke-direct {v1, v11}, Lcom/vlingo/dialog/util/EditDistance;-><init>(Lcom/vlingo/dialog/util/EditDistance$Cost;)V

    .line 166
    .local v1, "cheapInsertDistance":Lcom/vlingo/dialog/util/EditDistance;
    new-instance v10, Lcom/vlingo/dialog/util/EditDistance;

    new-instance v11, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;

    const/16 v12, 0x1e

    const/16 v13, 0x64

    const/16 v14, 0x64

    const/16 v15, 0x14

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;-><init>(IIII)V

    invoke-direct {v10, v11}, Lcom/vlingo/dialog/util/EditDistance;-><init>(Lcom/vlingo/dialog/util/EditDistance$Cost;)V

    .line 168
    .local v10, "spaceDistance":Lcom/vlingo/dialog/util/EditDistance;
    const/4 v11, 0x3

    new-array v0, v11, [Lcom/vlingo/dialog/util/EditDistance;

    const/4 v11, 0x0

    aput-object v9, v0, v11

    const/4 v11, 0x1

    aput-object v1, v0, v11

    const/4 v11, 0x2

    aput-object v10, v0, v11

    .local v0, "arr$":[Lcom/vlingo/dialog/util/EditDistance;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 169
    .local v2, "distance":Lcom/vlingo/dialog/util/EditDistance;
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v6, v3}, Lcom/vlingo/dialog/util/EditDistance;->compute(Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(I)V

    .line 168
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 171
    .end local v2    # "distance":Lcom/vlingo/dialog/util/EditDistance;
    :cond_0
    return-void
.end method


# virtual methods
.method public bestMatches([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "refs"    # [Ljava/lang/String;
    .param p2, "hyps"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/util/EditDistance$Match;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 137
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v6, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/util/EditDistance$Match;>;"
    const/4 v2, 0x0

    .local v2, "r":I
    :goto_0
    array-length v0, p1

    if-ge v2, v0, :cond_4

    .line 139
    const/4 v3, 0x0

    .local v3, "h":I
    :goto_1
    array-length v0, p2

    if-ge v3, v0, :cond_3

    .line 140
    aget-object v0, p1, v2

    aget-object v1, p2, v3

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/util/EditDistance;->compute(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 141
    .local v4, "distance":I
    int-to-float v0, v4

    const/4 v1, 0x1

    aget-object v7, p1, v2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    div-float v5, v0, v1

    .line 142
    .local v5, "normalizedDistance":F
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/util/EditDistance$Match;

    iget v0, v0, Lcom/vlingo/dialog/util/EditDistance$Match;->normalizedDistance:F

    cmpl-float v0, v5, v0

    if-nez v0, :cond_2

    .line 143
    :cond_0
    new-instance v0, Lcom/vlingo/dialog/util/EditDistance$Match;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/dialog/util/EditDistance$Match;-><init>(Lcom/vlingo/dialog/util/EditDistance;IIIF)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 144
    :cond_2
    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/util/EditDistance$Match;

    iget v0, v0, Lcom/vlingo/dialog/util/EditDistance$Match;->normalizedDistance:F

    cmpg-float v0, v5, v0

    if-gez v0, :cond_1

    .line 145
    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 146
    new-instance v0, Lcom/vlingo/dialog/util/EditDistance$Match;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/dialog/util/EditDistance$Match;-><init>(Lcom/vlingo/dialog/util/EditDistance;IIIF)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 138
    .end local v4    # "distance":I
    .end local v5    # "normalizedDistance":F
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 150
    .end local v3    # "h":I
    :cond_4
    return-object v6
.end method

.method public compute(Ljava/lang/String;Ljava/lang/String;)I
    .locals 16
    .param p1, "ref"    # Ljava/lang/String;
    .param p2, "hyp"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v9

    .line 110
    .local v9, "nRef":I
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v8

    .line 112
    .local v8, "nHyp":I
    add-int/lit8 v13, v8, 0x1

    new-array v1, v13, [I

    .line 113
    .local v1, "d1":[I
    add-int/lit8 v13, v8, 0x1

    new-array v2, v13, [I

    .line 115
    .local v2, "d2":[I
    const/4 v13, 0x0

    const/4 v14, 0x0

    aput v14, v1, v13

    .line 116
    const/4 v5, 0x1

    .local v5, "h":I
    :goto_0
    if-gt v5, v8, :cond_0

    .line 117
    add-int/lit8 v13, v5, -0x1

    aget v13, v1, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/dialog/util/EditDistance;->cost:Lcom/vlingo/dialog/util/EditDistance$Cost;

    add-int/lit8 v15, v5, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    invoke-virtual {v14, v15}, Lcom/vlingo/dialog/util/EditDistance$Cost;->ins(C)I

    move-result v14

    add-int/2addr v13, v14

    aput v13, v1, v5

    .line 116
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 120
    :cond_0
    const/4 v10, 0x1

    .local v10, "r":I
    :goto_1
    if-gt v10, v9, :cond_2

    .line 121
    add-int/lit8 v13, v10, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->charAt(I)C

    move-result v11

    .line 122
    .local v11, "refChar":C
    const/4 v13, 0x0

    const/4 v14, 0x0

    aget v14, v1, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/dialog/util/EditDistance;->cost:Lcom/vlingo/dialog/util/EditDistance$Cost;

    invoke-virtual {v15, v11}, Lcom/vlingo/dialog/util/EditDistance$Cost;->del(C)I

    move-result v15

    add-int/2addr v14, v15

    aput v14, v2, v13

    .line 123
    const/4 v5, 0x1

    :goto_2
    if-gt v5, v8, :cond_1

    .line 124
    add-int/lit8 v13, v5, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 125
    .local v6, "hypChar":C
    add-int/lit8 v13, v5, -0x1

    aget v13, v2, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/dialog/util/EditDistance;->cost:Lcom/vlingo/dialog/util/EditDistance$Cost;

    invoke-virtual {v14, v6}, Lcom/vlingo/dialog/util/EditDistance$Cost;->ins(C)I

    move-result v14

    add-int v7, v13, v14

    .line 126
    .local v7, "ins":I
    aget v13, v1, v5

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/dialog/util/EditDistance;->cost:Lcom/vlingo/dialog/util/EditDistance$Cost;

    invoke-virtual {v14, v11}, Lcom/vlingo/dialog/util/EditDistance$Cost;->del(C)I

    move-result v14

    add-int v3, v13, v14

    .line 127
    .local v3, "del":I
    add-int/lit8 v13, v5, -0x1

    aget v13, v1, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/dialog/util/EditDistance;->cost:Lcom/vlingo/dialog/util/EditDistance$Cost;

    invoke-virtual {v14, v11, v6}, Lcom/vlingo/dialog/util/EditDistance$Cost;->sub(CC)I

    move-result v14

    add-int v12, v13, v14

    .line 128
    .local v12, "sub":I
    invoke-static {v12, v7}, Ljava/lang/Math;->min(II)I

    move-result v13

    invoke-static {v3, v13}, Ljava/lang/Math;->min(II)I

    move-result v13

    aput v13, v2, v5

    .line 123
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 130
    .end local v3    # "del":I
    .end local v6    # "hypChar":C
    .end local v7    # "ins":I
    .end local v12    # "sub":I
    :cond_1
    move-object v4, v1

    .local v4, "dintemp":[I
    move-object v1, v2

    move-object v2, v4

    .line 120
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 133
    .end local v4    # "dintemp":[I
    .end local v11    # "refChar":C
    :cond_2
    aget v13, v1, v8

    return v13
.end method
