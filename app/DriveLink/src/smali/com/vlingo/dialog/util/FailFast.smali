.class public Lcom/vlingo/dialog/util/FailFast;
.super Ljava/lang/Object;
.source "FailFast.java"


# static fields
.field private static final FAIL_FAST:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    const-string/jumbo v0, "com.vlingo.dialog.FailFast"

    invoke-static {v0}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/vlingo/dialog/util/FailFast;->FAIL_FAST:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static failure()V
    .locals 2

    .prologue
    .line 8
    const-string/jumbo v0, "com.vlingo.dialog.FailFast"

    invoke-static {v0}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    new-instance v0, Ljava/lang/Error;

    const-string/jumbo v1, "FailFast"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 11
    :cond_0
    return-void
.end method
