.class public Lcom/vlingo/dialog/util/DateCanonicalizerUtil;
.super Ljava/lang/Object;
.source "DateCanonicalizerUtil.java"


# static fields
.field private static final BEGINNING:Ljava/lang/String; = "beginning"

.field private static final CLIENT_DATE_FIELD:Ljava/lang/String; = "Date"

.field private static final D:Ljava/lang/String; = "d"

.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final DAY_NUM:Ljava/lang/String; = "daynum"

.field private static final END:Ljava/lang/String; = "end"

.field private static final FIFTH:Ljava/lang/String; = "fifth"

.field private static final FIRST:Ljava/lang/String; = "first"

.field private static final FOURTH:Ljava/lang/String; = "fourth"

.field private static final FRIDAY:Ljava/lang/String; = "friday"

.field private static final LAST:Ljava/lang/String; = "last"

.field private static final M:Ljava/lang/String; = "m"

.field private static final MODIFIER_MONTH:Ljava/lang/String; = "modifiermonth"

.field private static final MODIFIER_WEEK:Ljava/lang/String; = "modifierweek"

.field private static final MONDAY:Ljava/lang/String; = "monday"

.field private static final MONTH:Ljava/lang/String; = "month"

.field private static final SATURDAY:Ljava/lang/String; = "saturday"

.field private static final SECOND:Ljava/lang/String; = "second"

.field private static final SUNDAY:Ljava/lang/String; = "sunday"

.field private static final THIRD:Ljava/lang/String; = "third"

.field private static final THURSDAY:Ljava/lang/String; = "thursday"

.field private static final TUESDAY:Ljava/lang/String; = "tuesday"

.field private static final W:Ljava/lang/String; = "w"

.field private static final WEDNESDAY:Ljava/lang/String; = "wednesday"

.field private static final WEEK_DAY:Ljava/lang/String; = "weekday"

.field private static final X_VL_CLIENT_DATE:Ljava/lang/String; = "x-vlclientdate"

.field private static final Y:Ljava/lang/String; = "y"

.field private static final YEAR:Ljava/lang/String; = "year"

.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 55
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd"

    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->DATE_FORMAT:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createParametersMap(Ljava/lang/String;)Ljava/util/Map;
    .locals 9
    .param p0, "dateModifiers"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 194
    .local v3, "parametersMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 201
    :cond_0
    return-object v3

    .line 196
    :cond_1
    const-string/jumbo v6, "\\s"

    invoke-virtual {p0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v5, v0, v1

    .line 197
    .local v5, "tmp":Ljava/lang/String;
    const-string/jumbo v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 198
    .local v4, "temp":[Ljava/lang/String;
    const/4 v6, 0x0

    aget-object v6, v4, v6

    sget-object v7, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v7, v4, v7

    sget-object v8, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static formatDateToStr(Ljava/util/GregorianCalendar;)Ljava/lang/String;
    .locals 2
    .param p0, "date"    # Ljava/util/GregorianCalendar;

    .prologue
    .line 205
    sget-object v0, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->DATE_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {p0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getCalendarDayOfWeek(Ljava/lang/String;)I
    .locals 3
    .param p0, "dayOfWeek"    # Ljava/lang/String;

    .prologue
    .line 255
    const-string/jumbo v0, "monday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    const/4 v0, 0x2

    .line 268
    :goto_0
    return v0

    .line 257
    :cond_0
    const-string/jumbo v0, "tuesday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    const/4 v0, 0x3

    goto :goto_0

    .line 259
    :cond_1
    const-string/jumbo v0, "wednesday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260
    const/4 v0, 0x4

    goto :goto_0

    .line 261
    :cond_2
    const-string/jumbo v0, "thursday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 262
    const/4 v0, 0x5

    goto :goto_0

    .line 263
    :cond_3
    const-string/jumbo v0, "friday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 264
    const/4 v0, 0x6

    goto :goto_0

    .line 265
    :cond_4
    const-string/jumbo v0, "saturday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 266
    const/4 v0, 0x7

    goto :goto_0

    .line 267
    :cond_5
    const-string/jumbo v0, "sunday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 268
    const/4 v0, 0x1

    goto :goto_0

    .line 270
    :cond_6
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown day of the week: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static getCalendarMonth(Ljava/lang/String;)I
    .locals 3
    .param p0, "month"    # Ljava/lang/String;

    .prologue
    .line 226
    const-string/jumbo v0, "january"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    const/4 v0, 0x0

    .line 249
    :goto_0
    return v0

    .line 228
    :cond_0
    const-string/jumbo v0, "february"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229
    const/4 v0, 0x1

    goto :goto_0

    .line 230
    :cond_1
    const-string/jumbo v0, "march"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    const/4 v0, 0x2

    goto :goto_0

    .line 232
    :cond_2
    const-string/jumbo v0, "april"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 233
    const/4 v0, 0x3

    goto :goto_0

    .line 234
    :cond_3
    const-string/jumbo v0, "may"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 235
    const/4 v0, 0x4

    goto :goto_0

    .line 236
    :cond_4
    const-string/jumbo v0, "june"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 237
    const/4 v0, 0x5

    goto :goto_0

    .line 238
    :cond_5
    const-string/jumbo v0, "july"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 239
    const/4 v0, 0x6

    goto :goto_0

    .line 240
    :cond_6
    const-string/jumbo v0, "august"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 241
    const/4 v0, 0x7

    goto :goto_0

    .line 242
    :cond_7
    const-string/jumbo v0, "september"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 243
    const/16 v0, 0x8

    goto :goto_0

    .line 244
    :cond_8
    const-string/jumbo v0, "october"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 245
    const/16 v0, 0x9

    goto :goto_0

    .line 246
    :cond_9
    const-string/jumbo v0, "november"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 247
    const/16 v0, 0xa

    goto :goto_0

    .line 248
    :cond_a
    const-string/jumbo v0, "december"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 249
    const/16 v0, 0xb

    goto :goto_0

    .line 251
    :cond_b
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown month: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static modifierWeekToInt(Ljava/lang/String;)I
    .locals 3
    .param p0, "modifier"    # Ljava/lang/String;

    .prologue
    .line 209
    const-string/jumbo v0, "last"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const/4 v0, -0x1

    .line 220
    :goto_0
    return v0

    .line 211
    :cond_0
    const-string/jumbo v0, "first"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    const/4 v0, 0x1

    goto :goto_0

    .line 213
    :cond_1
    const-string/jumbo v0, "second"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    const/4 v0, 0x2

    goto :goto_0

    .line 215
    :cond_2
    const-string/jumbo v0, "third"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 216
    const/4 v0, 0x3

    goto :goto_0

    .line 217
    :cond_3
    const-string/jumbo v0, "fourth"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 218
    const/4 v0, 0x4

    goto :goto_0

    .line 219
    :cond_4
    const-string/jumbo v0, "fifth"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 220
    const/4 v0, 0x5

    goto :goto_0

    .line 222
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown week modifier: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static modifyDate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p0, "currDate"    # Ljava/lang/String;
    .param p1, "dateModifier"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-static/range {p1 .. p1}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->createParametersMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v11

    .line 68
    .local v11, "parametersMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v11, :cond_0

    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->size()I

    move-result v17

    if-gtz v17, :cond_1

    .line 188
    .end local p0    # "currDate":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 72
    .restart local p0    # "currDate":Ljava/lang/String;
    :cond_1
    new-instance v9, Ljava/util/GregorianCalendar;

    sget-object v17, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/Locale;)V

    .line 73
    .local v9, "now":Ljava/util/GregorianCalendar;
    const/4 v10, 0x0

    .line 75
    .local v10, "nowRef":Ljava/util/GregorianCalendar;
    if-eqz p0, :cond_3

    .line 76
    const-string/jumbo v17, "-"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 77
    .local v4, "fields":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2

    .line 78
    new-instance v17, Ljava/lang/RuntimeException;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Invalid client date : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 79
    :cond_2
    const/16 v17, 0x1

    const/16 v18, 0x0

    aget-object v18, v4, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 80
    const/16 v17, 0x2

    const/16 v18, 0x1

    aget-object v18, v4, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 81
    const/16 v17, 0x5

    const/16 v18, 0x2

    aget-object v18, v4, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 82
    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    .line 83
    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->clone()Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "nowRef":Ljava/util/GregorianCalendar;
    check-cast v10, Ljava/util/GregorianCalendar;

    .line 85
    .end local v4    # "fields":[Ljava/lang/String;
    .restart local v10    # "nowRef":Ljava/util/GregorianCalendar;
    :cond_3
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/GregorianCalendar;->setLenient(Z)V

    .line 87
    const/4 v13, 0x0

    .line 90
    .local v13, "w":I
    const/4 v3, 0x0

    .line 91
    .local v3, "dayNum":I
    const/4 v14, 0x0

    .line 92
    .local v14, "weekDay":Ljava/lang/String;
    const/4 v8, 0x0

    .line 93
    .local v8, "month":Ljava/lang/String;
    const/16 v16, 0x0

    .line 94
    .local v16, "year":I
    const/4 v7, 0x0

    .line 95
    .local v7, "modifierWeek":Ljava/lang/String;
    const/4 v6, 0x0

    .line 98
    .local v6, "modifierMonth":Ljava/lang/String;
    const-string/jumbo v17, "modifierweek"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 99
    const-string/jumbo v17, "modifierweek"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "modifierWeek":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 100
    .restart local v7    # "modifierWeek":Ljava/lang/String;
    const-string/jumbo v17, "weekday"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_4

    .line 101
    new-instance v17, Ljava/lang/RuntimeException;

    const-string/jumbo v18, "Incorrect utterance, modifierWeek without a weekDay."

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 104
    :cond_4
    const-string/jumbo v17, "modifiermonth"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 105
    const-string/jumbo v17, "modifiermonth"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "modifierMonth":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 106
    .restart local v6    # "modifierMonth":Ljava/lang/String;
    const-string/jumbo v17, "month"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 107
    new-instance v17, Ljava/lang/RuntimeException;

    const-string/jumbo v18, "Incorrect utterance, modifierMonth without a month."

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 110
    :cond_5
    const-string/jumbo v17, "d"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 111
    const-string/jumbo v17, "d"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 112
    .local v2, "d":I
    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-virtual {v9, v0, v2}, Ljava/util/GregorianCalendar;->add(II)V

    .line 113
    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    .line 116
    .end local v2    # "d":I
    :cond_6
    const-string/jumbo v17, "w"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 117
    const-string/jumbo v17, "w"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 118
    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-virtual {v9, v0, v13}, Ljava/util/GregorianCalendar;->add(II)V

    .line 119
    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    .line 122
    :cond_7
    const-string/jumbo v17, "m"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 123
    const-string/jumbo v17, "m"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 124
    .local v5, "m":I
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v9, v0, v5}, Ljava/util/GregorianCalendar;->add(II)V

    .line 125
    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    .line 128
    .end local v5    # "m":I
    :cond_8
    const-string/jumbo v17, "y"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 129
    const-string/jumbo v17, "y"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 130
    .local v15, "y":I
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v9, v0, v15}, Ljava/util/GregorianCalendar;->add(II)V

    .line 131
    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    .line 134
    .end local v15    # "y":I
    :cond_9
    const-string/jumbo v17, "year"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 135
    const-string/jumbo v17, "year"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 136
    const/16 v17, 0x1

    move/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 140
    :cond_a
    const-string/jumbo v17, "month"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 141
    const-string/jumbo v17, "month"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "month":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 142
    .restart local v8    # "month":Ljava/lang/String;
    const/16 v17, 0x2

    invoke-static {v8}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->getCalendarMonth(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 144
    if-eqz v6, :cond_d

    .line 145
    const/16 v17, 0x5

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 146
    const-string/jumbo v17, "end"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 147
    const/16 v17, 0x5

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 148
    const/16 v17, 0x2

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->add(II)V

    .line 149
    const/16 v17, 0x5

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->add(II)V

    .line 153
    :cond_b
    invoke-static {v9}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->formatDateToStr(Ljava/util/GregorianCalendar;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 150
    :cond_c
    const-string/jumbo v17, "beginning"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_b

    .line 151
    new-instance v17, Ljava/lang/RuntimeException;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Invalid modifierMonth: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 157
    :cond_d
    const-string/jumbo v17, "daynum"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 158
    const-string/jumbo v17, "daynum"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 159
    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-virtual {v9, v0, v3}, Ljava/util/GregorianCalendar;->set(II)V

    .line 160
    const-string/jumbo v17, "year"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_e

    const-string/jumbo v17, "month"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    invoke-virtual {v9, v10}, Ljava/util/GregorianCalendar;->compareTo(Ljava/util/Calendar;)I

    move-result v17

    if-gez v17, :cond_e

    .line 161
    const/16 v17, 0x1

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->add(II)V

    .line 162
    :cond_e
    invoke-static {v9}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->formatDateToStr(Ljava/util/GregorianCalendar;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 165
    :cond_f
    const-string/jumbo v17, "weekday"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_13

    .line 166
    const-string/jumbo v17, "weekday"

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "weekDay":Ljava/lang/String;
    check-cast v14, Ljava/lang/String;

    .line 167
    .restart local v14    # "weekDay":Ljava/lang/String;
    if-nez v13, :cond_11

    if-nez v7, :cond_11

    .line 168
    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->clone()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Calendar;

    .line 169
    .local v12, "tmp":Ljava/util/Calendar;
    const/16 v17, 0x7

    invoke-static {v14}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->getCalendarDayOfWeek(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 170
    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    .line 171
    invoke-virtual {v12, v9}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_10

    .line 172
    const/16 v17, 0x5

    const/16 v18, 0x7

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->add(II)V

    .line 173
    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    .line 175
    :cond_10
    const/16 v17, 0x7

    invoke-static {v14}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->getCalendarDayOfWeek(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 177
    invoke-static {v9}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->formatDateToStr(Ljava/util/GregorianCalendar;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 178
    .end local v12    # "tmp":Ljava/util/Calendar;
    :cond_11
    if-eqz v7, :cond_12

    .line 179
    const/16 v17, 0x7

    invoke-static {v14}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->getCalendarDayOfWeek(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 180
    const/16 v17, 0x8

    invoke-static {v7}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->modifierWeekToInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 183
    invoke-static {v9}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->formatDateToStr(Ljava/util/GregorianCalendar;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 185
    :cond_12
    const/16 v17, 0x7

    invoke-static {v14}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->getCalendarDayOfWeek(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 188
    :cond_13
    invoke-static {v9}, Lcom/vlingo/dialog/util/DateCanonicalizerUtil;->formatDateToStr(Ljava/util/GregorianCalendar;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0
.end method
