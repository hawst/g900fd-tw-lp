.class public Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;
.super Lcom/vlingo/dialog/util/EditDistance$Cost;
.source "EditDistance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/util/EditDistance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IgnoreCaseInsDelSubCost"
.end annotation


# instance fields
.field private delCost:I

.field private insCost:I

.field private subCost:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 45
    invoke-direct {p0, v0, v0, v0}, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;-><init>(III)V

    .line 46
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "insDelSubCost"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p1, p1}, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;-><init>(III)V

    .line 50
    return-void
.end method

.method public constructor <init>(III)V
    .locals 0
    .param p1, "insCost"    # I
    .param p2, "delCost"    # I
    .param p3, "subCost"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/vlingo/dialog/util/EditDistance$Cost;-><init>()V

    .line 53
    iput p1, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;->insCost:I

    .line 54
    iput p2, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;->delCost:I

    .line 55
    iput p3, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;->subCost:I

    .line 56
    return-void
.end method


# virtual methods
.method public del(C)I
    .locals 1
    .param p1, "ref"    # C

    .prologue
    .line 63
    iget v0, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;->delCost:I

    return v0
.end method

.method public ins(C)I
    .locals 1
    .param p1, "hyp"    # C

    .prologue
    .line 59
    iget v0, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;->insCost:I

    return v0
.end method

.method public sub(CC)I
    .locals 1
    .param p1, "ref"    # C
    .param p2, "hyp"    # C

    .prologue
    .line 67
    invoke-static {p1, p2}, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;->equalIgnoreCase(CC)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsDelSubCost;->subCost:I

    goto :goto_0
.end method
