.class public Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;
.super Lcom/vlingo/dialog/util/EditDistance$Cost;
.source "EditDistance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/util/EditDistance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IgnoreCaseInsSubDelSpaceCost"
.end annotation


# instance fields
.field private delCost:I

.field private insCost:I

.field private spaceInsDelCost:I

.field private subCost:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "insCost"    # I
    .param p2, "delCost"    # I
    .param p3, "subCost"    # I
    .param p4, "spaceInsDelCost"    # I

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/vlingo/dialog/util/EditDistance$Cost;-><init>()V

    .line 76
    iput p1, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;->insCost:I

    .line 77
    iput p2, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;->delCost:I

    .line 78
    iput p3, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;->subCost:I

    .line 79
    iput p4, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;->spaceInsDelCost:I

    .line 80
    return-void
.end method


# virtual methods
.method public del(C)I
    .locals 1
    .param p1, "ref"    # C

    .prologue
    .line 87
    invoke-static {p1}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;->spaceInsDelCost:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;->delCost:I

    goto :goto_0
.end method

.method public ins(C)I
    .locals 1
    .param p1, "hyp"    # C

    .prologue
    .line 83
    invoke-static {p1}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;->spaceInsDelCost:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;->insCost:I

    goto :goto_0
.end method

.method public sub(CC)I
    .locals 1
    .param p1, "ref"    # C
    .param p2, "hyp"    # C

    .prologue
    .line 91
    invoke-static {p1, p2}, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;->equalIgnoreCase(CC)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/dialog/util/EditDistance$IgnoreCaseInsSubDelSpaceCost;->subCost:I

    goto :goto_0
.end method
