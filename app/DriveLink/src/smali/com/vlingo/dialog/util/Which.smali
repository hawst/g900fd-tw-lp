.class public Lcom/vlingo/dialog/util/Which;
.super Ljava/lang/Object;
.source "Which.java"


# static fields
.field public static final CF_10TH:Ljava/lang/String; = "10th"

.field public static final CF_1ST:Ljava/lang/String; = "1st"

.field public static final CF_2ND:Ljava/lang/String; = "2nd"

.field public static final CF_3RD:Ljava/lang/String; = "3rd"

.field public static final CF_4TH:Ljava/lang/String; = "4th"

.field public static final CF_5TH:Ljava/lang/String; = "5th"

.field public static final CF_6TH:Ljava/lang/String; = "6th"

.field public static final CF_7TH:Ljava/lang/String; = "7th"

.field public static final CF_8TH:Ljava/lang/String; = "8th"

.field public static final CF_9TH:Ljava/lang/String; = "9th"

.field public static final CF_BOTTOM:Ljava/lang/String; = "bottom"

.field public static final CF_FIRST:Ljava/lang/String; = "first"

.field public static final CF_LAST:Ljava/lang/String; = "last"

.field public static final CF_LEFT:Ljava/lang/String; = "left"

.field public static final CF_MIDDLE:Ljava/lang/String; = "middle"

.field public static final CF_RIGHT:Ljava/lang/String; = "right"

.field public static final CF_TOP:Ljava/lang/String; = "top"

.field private static choiceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected static final logger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/vlingo/dialog/util/Which;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/util/Which;->logger:Lcom/vlingo/common/log4j/VLogger;

    .line 31
    new-instance v0, Lcom/vlingo/dialog/util/Which$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/util/Which$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/util/Which;->choiceMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static selectChoice(Lcom/vlingo/dialog/manager/model/ListPosition;IZZLjava/lang/String;)I
    .locals 8
    .param p0, "listPosition"    # Lcom/vlingo/dialog/manager/model/ListPosition;
    .param p1, "listSize"    # I
    .param p2, "useVertical"    # Z
    .param p3, "useHorizontal"    # Z
    .param p4, "choice"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 54
    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/ListPosition;->isValid(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 55
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ListPosition;->getStartIndex()I

    move-result v3

    .line 56
    .local v3, "startIndex":I
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ListPosition;->getEndIndex()I

    move-result v2

    .line 57
    .local v2, "endIndex":I
    sub-int p1, v2, v3

    .line 62
    :goto_0
    sget-object v5, Lcom/vlingo/dialog/util/Which;->choiceMap:Ljava/util/Map;

    invoke-interface {v5, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 63
    .local v0, "choiceMapped":Ljava/lang/Integer;
    const-string/jumbo v5, "top"

    invoke-virtual {v5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "bottom"

    invoke-virtual {v5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    if-nez p2, :cond_2

    .line 64
    const/4 v0, 0x0

    .line 87
    :goto_1
    if-nez v0, :cond_b

    :goto_2
    return v4

    .line 59
    .end local v0    # "choiceMapped":Ljava/lang/Integer;
    .end local v2    # "endIndex":I
    .end local v3    # "startIndex":I
    :cond_1
    const/4 v3, 0x0

    .line 60
    .restart local v3    # "startIndex":I
    move v2, p1

    .restart local v2    # "endIndex":I
    goto :goto_0

    .line 65
    .restart local v0    # "choiceMapped":Ljava/lang/Integer;
    :cond_2
    const-string/jumbo v5, "left"

    invoke-virtual {v5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "right"

    invoke-virtual {v5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    if-nez p3, :cond_4

    .line 66
    const/4 v0, 0x0

    goto :goto_1

    .line 67
    :cond_4
    const-string/jumbo v5, "middle"

    invoke-virtual {v5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 68
    rem-int/lit8 v5, p1, 0x2

    if-nez v5, :cond_5

    .line 69
    const/4 v0, 0x0

    goto :goto_1

    .line 71
    :cond_5
    add-int/lit8 v5, p1, -0x1

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 73
    :cond_6
    if-nez v0, :cond_7

    .line 74
    sget-object v5, Lcom/vlingo/dialog/util/Which;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "unrecognized choice: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 75
    const/4 v0, 0x0

    goto :goto_1

    .line 77
    :cond_7
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 78
    .local v1, "choiceMappedInt":I
    if-ne v1, v4, :cond_8

    .line 80
    add-int/lit8 v5, v2, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 81
    :cond_8
    if-ltz v1, :cond_9

    if-lt v1, v2, :cond_a

    .line 82
    :cond_9
    const/4 v0, 0x0

    goto :goto_1

    .line 84
    :cond_a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v5, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 87
    .end local v1    # "choiceMappedInt":I
    :cond_b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_2
.end method
