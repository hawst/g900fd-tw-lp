.class public interface abstract Lcom/vlingo/dialog/nlg/DMOutputGenerator;
.super Ljava/lang/Object;
.source "DMOutputGenerator.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/nlg/DMOutputGenerator$BadIndexException;
    }
.end annotation


# virtual methods
.method public abstract generate(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Lcom/vlingo/tts/model/NLGResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/dialog/nlg/DMOutputGenerator$BadIndexException;
        }
    .end annotation
.end method

.method public abstract generateAll(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/tts/model/NLGRequest;",
            "Lcom/vlingo/message/model/request/DialogMeta;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/tts/model/NLGResponse;",
            ">;"
        }
    .end annotation
.end method
