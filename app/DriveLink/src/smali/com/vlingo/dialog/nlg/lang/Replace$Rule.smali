.class Lcom/vlingo/dialog/nlg/lang/Replace$Rule;
.super Ljava/lang/Object;
.source "Replace.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/lang/Replace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Rule"
.end annotation


# instance fields
.field pattern:Ljava/util/regex/Pattern;

.field replacement:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "replacement"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/lang/Replace$Rule;->pattern:Ljava/util/regex/Pattern;

    .line 14
    iput-object p2, p0, Lcom/vlingo/dialog/nlg/lang/Replace$Rule;->replacement:Ljava/lang/String;

    .line 15
    return-void
.end method
