.class Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;
.super Ljava/lang/Object;
.source "DMOutputGeneratorImpl3.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CompiledPrompt"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x3b1ba064387dbe26L


# instance fields
.field display:Ljava/lang/String;

.field spoken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/common/message/MNode;)V
    .locals 1
    .param p1, "promptNode"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const-string/jumbo v0, "SpokenDisplay"

    invoke-virtual {p1, v0}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 101
    const-string/jumbo v0, "SpokenDisplay"

    invoke-virtual {p1, v0}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->uniquify(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->access$000(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;->display:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;->spoken:Ljava/lang/String;

    .line 106
    :goto_0
    return-void

    .line 103
    :cond_0
    const-string/jumbo v0, "Spoken"

    invoke-virtual {p1, v0}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->uniquify(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->access$000(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;->spoken:Ljava/lang/String;

    .line 104
    const-string/jumbo v0, "Display"

    invoke-virtual {p1, v0}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->uniquify(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->access$000(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;->display:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public generateDisplay(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;->display:Ljava/lang/String;

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->evaluateSequence(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->access$100(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public generateSpoken(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;->spoken:Ljava/lang/String;

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->evaluateSequence(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->access$100(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
