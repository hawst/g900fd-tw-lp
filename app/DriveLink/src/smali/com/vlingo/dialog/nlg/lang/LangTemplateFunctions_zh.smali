.class public Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
.source "LangTemplateFunctions_zh.java"


# static fields
.field private static final ALL_DAY:Ljava/lang/String; = "\u4e00\u6574\u5929"

.field private static final AM:Ljava/lang/String; = "\u4e0a\u5348"

.field private static final AND:Ljava/lang/String; = "\u548c"

.field private static final AND_FORMAT:[Ljava/lang/String;

.field private static final AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

.field private static final APPOINTMENT:Ljava/lang/String; = "\u7ea6\u4f1a"

.field private static final APPOINTMENT_AND_FORMAT:[Ljava/lang/String;

.field private static final APPOINTMENT_LOCATION:Ljava/lang/String; = "\u5730\u70b9"

.field private static final APPOINTMENT_TITLE:Ljava/lang/String; = "\u6807\u9898"

.field private static final COMMA:Ljava/lang/String; = "\uff0c"

.field private static final DATE_ALT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATE_AT:Ljava/lang/String; = "\u5728"

.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final ENUMERATION_COMMA:Ljava/lang/String; = "\u3001"

.field private static final HOUR:Ljava/lang/String; = "\u70b9"

.field private static final LOCALE:Ljava/util/Locale;

.field private static final MINUTE:Ljava/lang/String; = "\u5206"

.field private static final OR:Ljava/lang/String; = "\u6216"

.field private static final OR_FORMAT:[Ljava/lang/String;

.field private static final PERIOD:Ljava/lang/String; = "\u3002"

.field private static final PM:Ljava/lang/String; = "\u4e0b\u5348"

.field private static final TIME_AT:Ljava/lang/String; = "\u5728"

.field private static final TIME_FORMAT_12H:Ljava/text/DateFormat;

.field private static final TIME_FORMAT_24H:Ljava/text/DateFormat;

.field private static final TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final alarmDaysMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    sget-object v0, Ljava/util/Locale;->TRADITIONAL_CHINESE:Ljava/util/Locale;

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->LOCALE:Ljava/util/Locale;

    .line 22
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->DATE_ALT_MAP:Ljava/util/Map;

    .line 51
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "\u548c"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\u3001\u548c"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    .line 52
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "\u548c"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->AND_FORMAT:[Ljava/lang/String;

    .line 53
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "\u6216"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->OR_FORMAT:[Ljava/lang/String;

    .line 55
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->APPOINTMENT_AND_FORMAT:[Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "M\u6708d\u53f7EEEE"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->DATE_FORMAT:Ljava/text/DateFormat;

    .line 60
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "ah\u70b9m\u5206"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->TIME_FORMAT_12H:Ljava/text/DateFormat;

    .line 61
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "H\u70b9m\u5206"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->TIME_FORMAT_24H:Ljava/text/DateFormat;

    .line 63
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh$2;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh$2;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->TYPE_MAP:Ljava/util/Map;

    .line 151
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh$3;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh$3;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->alarmDaysMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;-><init>()V

    return-void
.end method


# virtual methods
.method protected alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 168
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 170
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "enabled"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 171
    .local v1, "enabled":Z
    if-nez v1, :cond_0

    .line 172
    const-string/jumbo v5, "\u5173\u95ed"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    :cond_0
    const-string/jumbo v5, "repeat"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 176
    .local v2, "repeat":Z
    const-string/jumbo v5, "days"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "days":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->alarmDaysMap:Ljava/util/Map;

    invoke-virtual {p0, v0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->renderDays(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_1

    .line 180
    if-eqz v2, :cond_5

    .line 181
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 182
    const-string/jumbo v5, "\u6bcf\u5929"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    :cond_1
    :goto_0
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 205
    .local v4, "time":Ljava/lang/String;
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,\u3001\uff0c"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 183
    .end local v4    # "time":Ljava/lang/String;
    :cond_2
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 184
    const-string/jumbo v5, "\u6bcf\u661f\u671f\u4e00\u5230\u661f\u671f\u4e94"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 185
    :cond_3
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 186
    const-string/jumbo v5, "\u6bcf\u661f\u671f\u516d\u548c\u661f\u671f\u65e5"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 188
    :cond_4
    const-string/jumbo v5, "\u6bcf"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 192
    :cond_5
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 193
    const-string/jumbo v5, "\u672a\u6765\u4e03\u5929"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 194
    :cond_6
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 195
    const-string/jumbo v5, "\u661f\u671f\u4e00\u5230\u661f\u671f\u4e94"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 196
    :cond_7
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 197
    const-string/jumbo v5, "\u661f\u671f\u516d\u548c\u661f\u671f\u65e5"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 199
    :cond_8
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method protected alarmAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected andFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointmentForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "brief"    # Z

    .prologue
    .line 106
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 108
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "date"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 109
    const-string/jumbo v5, "date_alt"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "dateAlt":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 111
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->DATE_ALT_MAP:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_0
    :goto_0
    const-string/jumbo v5, "time"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->isEmptyOrUnspecifiedAmPm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 118
    const-string/jumbo v5, "all_day"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 119
    const-string/jumbo v5, "\u4e00\u6574\u5929"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_1
    :goto_1
    if-nez p3, :cond_2

    const-string/jumbo v5, "location"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 127
    const-string/jumbo v5, "location"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 129
    const-string/jumbo v5, "\u5730\u70b9"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .end local v1    # "location":Ljava/lang/String;
    :cond_2
    const-string/jumbo v5, "title"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 135
    const-string/jumbo v5, "title"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "title":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 137
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .end local v4    # "title":Ljava/lang/String;
    :cond_3
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,\uff0c"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 113
    .restart local v0    # "dateAlt":Ljava/lang/String;
    :cond_4
    const-string/jumbo v5, "date"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 121
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 122
    .local v3, "time":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 139
    .end local v3    # "time":Ljava/lang/String;
    .restart local v4    # "title":Ljava/lang/String;
    :cond_6
    const-string/jumbo v5, "\u7ea6\u4f1a"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method protected appointmentAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->APPOINTMENT_AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public date(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 80
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->DATE_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->date(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected orFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->OR_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 10
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "task"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const v9, 0xff0c

    .line 217
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v7, "title"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 220
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 221
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 225
    :cond_0
    const-string/jumbo v7, "date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 226
    .local v0, "date":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 227
    const-string/jumbo v7, "\u622a\u6b62\u65e5\uff0c"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 229
    .local v1, "dateAlt":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 230
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    :goto_0
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 237
    .end local v1    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v7, "reminder_date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, "reminderDate":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 239
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "reminder_date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 240
    .local v3, "reminderDateAlt":Ljava/lang/String;
    if-eqz v3, :cond_5

    .line 241
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    :goto_1
    const-string/jumbo v7, "reminder_timer"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 247
    .local v4, "reminderTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 248
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    :cond_2
    const-string/jumbo v7, "\u63d0\u9192\u6211"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    .end local v3    # "reminderDateAlt":Ljava/lang/String;
    .end local v4    # "reminderTime":Ljava/lang/String;
    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, " ,\u3001\uff0c"

    invoke-static {v7, v8}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 232
    .end local v2    # "reminderDate":Ljava/lang/String;
    .restart local v1    # "dateAlt":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 243
    .end local v1    # "dateAlt":Ljava/lang/String;
    .restart local v2    # "reminderDate":Ljava/lang/String;
    .restart local v3    # "reminderDateAlt":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected taskAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public time(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->use24HourTime()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->TIME_FORMAT_24H:Ljava/text/DateFormat;

    .line 86
    .local v0, "format":Ljava/text/DateFormat;
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "\u70b90\u5206"

    const-string/jumbo v3, "\u70b9"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 85
    .end local v0    # "format":Ljava/text/DateFormat;
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->TIME_FORMAT_12H:Ljava/text/DateFormat;

    goto :goto_0
.end method

.method protected typeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_zh;->TYPE_MAP:Ljava/util/Map;

    return-object v0
.end method
