.class Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;
.super Ljava/lang/Object;
.source "DMOutputGeneratorImpl2.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CompiledPrompt"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x7812698815c4e54cL


# instance fields
.field display:Ljava/lang/String;

.field spoken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/dialog/nlg/config/Prompt;)V
    .locals 1
    .param p1, "prompt"    # Lcom/vlingo/dialog/nlg/config/Prompt;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-virtual {p1}, Lcom/vlingo/dialog/nlg/config/Prompt;->getSpokenDisplay()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p1}, Lcom/vlingo/dialog/nlg/config/Prompt;->getSpokenDisplay()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniquify(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->access$000(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;->display:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;->spoken:Ljava/lang/String;

    .line 107
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/dialog/nlg/config/Prompt;->getSpoken()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniquify(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->access$000(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;->spoken:Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Lcom/vlingo/dialog/nlg/config/Prompt;->getDisplay()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniquify(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->access$000(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;->display:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public generateDisplay(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;->display:Ljava/lang/String;

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->evaluateSequence(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->access$100(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public generateSpoken(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;->spoken:Ljava/lang/String;

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->evaluateSequence(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->access$100(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
