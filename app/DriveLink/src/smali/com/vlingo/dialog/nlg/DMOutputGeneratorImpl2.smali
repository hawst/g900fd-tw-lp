.class public Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;
.super Ljava/lang/Object;
.source "DMOutputGeneratorImpl2.java"

# interfaces
.implements Lcom/vlingo/dialog/nlg/DMOutputGenerator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;,
        Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledCondition;,
        Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;
    }
.end annotation


# static fields
.field private static final VARIABLE_PATTERN:Ljava/util/regex/Pattern;

.field private static final engine:Lorg/apache/commons/jexl2/JexlEngine;

.field private static logger:Lcom/vlingo/common/log4j/VLogger; = null

.field private static random:Ljava/util/Random; = null

.field private static final serialVersionUID:J = 0x3950889ad0912809L

.field private static final uniqueMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static uniqueMapCount:I


# instance fields
.field private loadVersion:Ljava/lang/String;

.field private map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 45
    const-class v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->logger:Lcom/vlingo/common/log4j/VLogger;

    .line 47
    const-string/jumbo v0, "\\$\\{([^}]+)\\}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->VARIABLE_PATTERN:Ljava/util/regex/Pattern;

    .line 49
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/JexlEngine;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    .line 51
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    const-string/jumbo v1, "ot"

    const-class v2, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;

    const-string/jumbo v3, "lang"

    const-class v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;

    invoke-static {v1, v2, v3, v4}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlEngine;->setFunctions(Ljava/util/Map;)V

    .line 55
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlEngine;->setCache(I)V

    .line 56
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v0, v5}, Lorg/apache/commons/jexl2/JexlEngine;->setSilent(Z)V

    .line 57
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v0, v5}, Lorg/apache/commons/jexl2/JexlEngine;->setLenient(Z)V

    .line 58
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v1, "DMOutputGenerator: static initializer"

    invoke-virtual {v0, v1}, Lorg/apache/log4j/Logger;->info(Ljava/lang/Object;)V

    .line 61
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->random:Ljava/util/Random;

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniqueMap:Ljava/util/Map;

    .line 67
    sput v5, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniqueMapCount:I

    return-void
.end method

.method private constructor <init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V
    .locals 5
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .param p2, "mainFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->map:Ljava/util/Map;

    .line 224
    if-nez p2, :cond_0

    const-string/jumbo p2, "dm-prompts.xml"

    .line 225
    :cond_0
    const-class v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;

    monitor-enter v2

    .line 226
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->map:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 227
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->getVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->loadVersion:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    .line 230
    invoke-static {}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->cleanUp()V

    .line 231
    sget-object v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Initialized DMOutputGenerator from configuration version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->loadVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 242
    :try_start_2
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V

    .line 244
    :goto_0
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 245
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 235
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "No class meta found for: OutputTemplates"

    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 236
    iget-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->map:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 237
    sget-object v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Cannot load DMOutputGenerator from old format configuration version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->loadVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 242
    :try_start_4
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V

    goto :goto_0

    .line 244
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 239
    .restart local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 242
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v1

    :try_start_6
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method static synthetic access$000(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniquify(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->evaluateSequence(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->VARIABLE_PATTERN:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$300(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->evaluateExpression(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()Ljava/util/Random;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->random:Ljava/util/Random;

    return-object v0
.end method

.method private buildContext(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Lorg/apache/commons/jexl2/JexlContext;
    .locals 5
    .param p1, "request"    # Lcom/vlingo/tts/model/NLGRequest;
    .param p2, "dialogMeta"    # Lcom/vlingo/message/model/request/DialogMeta;

    .prologue
    .line 293
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->getLanguageInstance(Lcom/vlingo/tts/model/NLGRequest;)Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;

    move-result-object v1

    .line 294
    .local v1, "langInstance":Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
    invoke-virtual {p2}, Lcom/vlingo/message/model/request/DialogMeta;->getUse24HourTime()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->use24HourTime(Ljava/lang/Boolean;)V

    .line 295
    new-instance v0, Lorg/apache/commons/jexl2/MapContext;

    const-string/jumbo v2, "lang"

    const-string/jumbo v3, "form"

    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getContext()Lcom/vlingo/mda/util/MDAObject;

    move-result-object v4

    invoke-static {v2, v1, v3, v4}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/apache/commons/jexl2/MapContext;-><init>(Ljava/util/Map;)V

    .line 299
    .local v0, "context":Lorg/apache/commons/jexl2/MapContext;
    return-object v0
.end method

.method private static buildResponse(Lorg/apache/commons/jexl2/JexlContext;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;)Lcom/vlingo/tts/model/NLGResponse;
    .locals 5
    .param p0, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p1, "compiledPrompt"    # Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;

    .prologue
    .line 304
    const-string/jumbo v4, "lang"

    invoke-interface {p0, v4}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;

    .line 306
    .local v1, "langInstance":Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
    invoke-virtual {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->setSpoken()V

    .line 307
    new-instance v3, Lcom/vlingo/tts/model/NLGSegment;

    invoke-direct {v3}, Lcom/vlingo/tts/model/NLGSegment;-><init>()V

    .line 308
    .local v3, "spoken":Lcom/vlingo/tts/model/NLGSegment;
    invoke-virtual {p1, p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;->generateSpoken(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->postProcess(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/nlg/NLGUtil;->separateLargeIntegersIntoWords(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/tts/model/NLGSegment;->setText(Ljava/lang/String;)V

    .line 310
    invoke-virtual {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->setDisplay()V

    .line 311
    new-instance v0, Lcom/vlingo/tts/model/NLGSegment;

    invoke-direct {v0}, Lcom/vlingo/tts/model/NLGSegment;-><init>()V

    .line 312
    .local v0, "display":Lcom/vlingo/tts/model/NLGSegment;
    invoke-virtual {p1, p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;->generateDisplay(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->postProcess(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/vlingo/tts/model/NLGSegment;->setText(Ljava/lang/String;)V

    .line 314
    new-instance v2, Lcom/vlingo/tts/model/NLGResponse;

    invoke-direct {v2}, Lcom/vlingo/tts/model/NLGResponse;-><init>()V

    .line 315
    .local v2, "response":Lcom/vlingo/tts/model/NLGResponse;
    invoke-virtual {v2, v3}, Lcom/vlingo/tts/model/NLGResponse;->setSpokenForm(Lcom/vlingo/tts/model/NLGSegment;)V

    .line 316
    invoke-virtual {v2, v0}, Lcom/vlingo/tts/model/NLGResponse;->setDisplayForm(Lcom/vlingo/tts/model/NLGSegment;)V

    .line 318
    return-object v2
.end method

.method public static cleanUp()V
    .locals 3

    .prologue
    .line 349
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "DMOutputGenerator uniqueMap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniqueMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniqueMapCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 350
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniqueMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 351
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniqueMapCount:I

    .line 352
    return-void
.end method

.method private static evaluateExpression(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 70
    const/4 v2, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "strippedExpression":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v2, v1}, Lorg/apache/commons/jexl2/JexlEngine;->createExpression(Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v0

    .line 72
    .local v0, "expression":Lorg/apache/commons/jexl2/Expression;
    invoke-interface {v0, p0}, Lorg/apache/commons/jexl2/Expression;->evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static evaluateSequence(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 76
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 77
    .local v5, "sb":Ljava/lang/StringBuilder;
    sget-object v6, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->VARIABLE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v6, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 78
    .local v3, "matcher":Ljava/util/regex/Matcher;
    const/4 v0, 0x0

    .line 79
    .local v0, "c":I
    :goto_0
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 80
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    .line 81
    .local v4, "s":I
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v1

    .line 82
    .local v1, "e":I
    if-le v4, v0, :cond_0

    .line 83
    invoke-virtual {p1, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    :cond_0
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->evaluateExpression(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 86
    .local v2, "expansion":Ljava/lang/String;
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    move v0, v1

    .line 88
    goto :goto_0

    .line 89
    .end local v1    # "e":I
    .end local v2    # "expansion":Ljava/lang/String;
    .end local v4    # "s":I
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v0, v6, :cond_2

    .line 90
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private getCompiledTemplate(Lcom/vlingo/tts/model/NLGRequest;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;
    .locals 12
    .param p1, "request"    # Lcom/vlingo/tts/model/NLGRequest;

    .prologue
    .line 265
    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getType()Ljava/lang/String;

    move-result-object v8

    .line 266
    .local v8, "names":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 267
    .local v5, "language":Ljava/lang/String;
    const-string/jumbo v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v6, :cond_2

    aget-object v7, v1, v3

    .line 268
    .local v7, "name":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->map:Ljava/util/Map;

    invoke-interface {v9, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 269
    .local v4, "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;>;"
    if-eqz v4, :cond_1

    .line 270
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;

    .line 271
    .local v2, "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;
    if-nez v2, :cond_0

    const-string/jumbo v9, "-"

    invoke-virtual {v5, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 272
    new-instance v9, Lcom/vlingo/common/util/VLocale;

    invoke-direct {v9, v5}, Lcom/vlingo/common/util/VLocale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/vlingo/common/util/VLocale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 273
    .local v0, "altLanguage":Ljava/lang/String;
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;
    check-cast v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;

    .line 275
    .end local v0    # "altLanguage":Ljava/lang/String;
    .restart local v2    # "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;
    :cond_0
    if-eqz v2, :cond_1

    .line 276
    return-object v2

    .line 267
    .end local v2    # "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 280
    .end local v4    # "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;>;"
    .end local v7    # "name":Ljava/lang/String;
    :cond_2
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "no template found for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getType()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " with language "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9
.end method

.method private getLanguageInstance(Lcom/vlingo/tts/model/NLGRequest;)Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
    .locals 5
    .param p1, "request"    # Lcom/vlingo/tts/model/NLGRequest;

    .prologue
    .line 284
    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 285
    .local v1, "language":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->createInstance(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;

    move-result-object v0

    .line 286
    .local v0, "langInstance":Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
    if-nez v0, :cond_0

    .line 287
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "no LangTemplateFunctions class for language: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 289
    :cond_0
    return-object v0
.end method

.method private initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V
    .locals 9
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .param p2, "file"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    sget-object v6, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "loading "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 250
    invoke-interface {p1, p2}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->load(Ljava/lang/String;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;

    .line 251
    .local v5, "top":Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;
    invoke-virtual {v5}, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;->getImports()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/nlg/config/Import;

    .line 252
    .local v1, "i":Lcom/vlingo/dialog/nlg/config/Import;
    invoke-virtual {v1}, Lcom/vlingo/dialog/nlg/config/Import;->getFile()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v6}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    goto :goto_0

    .line 254
    .end local v1    # "i":Lcom/vlingo/dialog/nlg/config/Import;
    :cond_0
    invoke-virtual {v5}, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;->getTemplates()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/nlg/config/Template;

    .line 255
    .local v4, "template":Lcom/vlingo/dialog/nlg/config/Template;
    new-instance v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;

    invoke-direct {v0, v4}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;-><init>(Lcom/vlingo/dialog/nlg/config/Template;)V

    .line 256
    .local v0, "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;
    iget-object v6, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->map:Ljava/util/Map;

    iget-object v7, v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;->name:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 257
    .local v3, "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;>;"
    if-nez v3, :cond_1

    .line 258
    iget-object v6, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->map:Ljava/util/Map;

    iget-object v7, v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;->name:Ljava/lang/String;

    new-instance v3, Ljava/util/TreeMap;

    .end local v3    # "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;>;"
    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    .restart local v3    # "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;>;"
    invoke-interface {v6, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    :cond_1
    iget-object v6, v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;->language:Ljava/lang/String;

    invoke-interface {v3, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 262
    .end local v0    # "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;
    .end local v3    # "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;>;"
    .end local v4    # "template":Lcom/vlingo/dialog/nlg/config/Template;
    :cond_2
    return-void
.end method

.method public static loadFromDirectory(Ljava/io/File;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;
    .locals 1
    .param p0, "directory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->loadFromDirectory(Ljava/io/File;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;

    move-result-object v0

    return-object v0
.end method

.method public static loadFromDirectory(Ljava/io/File;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;
    .locals 3
    .param p0, "directory"    # Ljava/io/File;
    .param p1, "mainFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    new-instance v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;

    new-instance v1, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;

    const-class v2, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {v0, v1, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    return-object v0
.end method

.method public static loadFromJar(Ljava/io/File;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;
    .locals 1
    .param p0, "jar"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->loadFromJar(Ljava/io/File;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;

    move-result-object v0

    return-object v0
.end method

.method public static loadFromJar(Ljava/io/File;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;
    .locals 3
    .param p0, "jar"    # Ljava/io/File;
    .param p1, "mainFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    new-instance v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;

    new-instance v1, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;

    const-class v2, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {v0, v1, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    return-object v0
.end method

.method private static uniquify(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "in"    # Ljava/lang/String;

    .prologue
    .line 339
    sget v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniqueMapCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniqueMapCount:I

    .line 340
    sget-object v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniqueMap:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 341
    .local v0, "mapped":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 342
    move-object v0, p0

    .line 343
    sget-object v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->uniqueMap:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    :cond_0
    return-object v0
.end method


# virtual methods
.method public generate(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Lcom/vlingo/tts/model/NLGResponse;
    .locals 4
    .param p1, "request"    # Lcom/vlingo/tts/model/NLGRequest;
    .param p2, "dialogMeta"    # Lcom/vlingo/message/model/request/DialogMeta;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/dialog/nlg/DMOutputGenerator$BadIndexException;
        }
    .end annotation

    .prologue
    .line 322
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->buildContext(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Lorg/apache/commons/jexl2/JexlContext;

    move-result-object v2

    .line 323
    .local v2, "context":Lorg/apache/commons/jexl2/JexlContext;
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->getCompiledTemplate(Lcom/vlingo/tts/model/NLGRequest;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;

    move-result-object v1

    .line 324
    .local v1, "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;
    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getIndex()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;->generate(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/Integer;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;

    move-result-object v0

    .line 325
    .local v0, "compiledPrompt":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;
    invoke-static {v2, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->buildResponse(Lorg/apache/commons/jexl2/JexlContext;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;)Lcom/vlingo/tts/model/NLGResponse;

    move-result-object v3

    return-object v3
.end method

.method public generateAll(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Ljava/util/List;
    .locals 6
    .param p1, "request"    # Lcom/vlingo/tts/model/NLGRequest;
    .param p2, "dialogMeta"    # Lcom/vlingo/message/model/request/DialogMeta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/tts/model/NLGRequest;",
            "Lcom/vlingo/message/model/request/DialogMeta;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/tts/model/NLGResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->buildContext(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Lorg/apache/commons/jexl2/JexlContext;

    move-result-object v2

    .line 330
    .local v2, "context":Lorg/apache/commons/jexl2/JexlContext;
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->getCompiledTemplate(Lcom/vlingo/tts/model/NLGRequest;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;

    move-result-object v1

    .line 331
    .local v1, "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 332
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/tts/model/NLGResponse;>;"
    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledTemplate;->generateAll(Lorg/apache/commons/jexl2/JexlContext;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;

    .line 333
    .local v0, "compiledPrompt":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;
    invoke-static {v2, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2;->buildResponse(Lorg/apache/commons/jexl2/JexlContext;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;)Lcom/vlingo/tts/model/NLGResponse;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 335
    .end local v0    # "compiledPrompt":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl2$CompiledPrompt;
    :cond_0
    return-object v4
.end method
