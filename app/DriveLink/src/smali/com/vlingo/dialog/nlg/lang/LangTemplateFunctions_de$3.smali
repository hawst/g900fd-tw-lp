.class final Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;
.super Ljava/util/HashMap;
.source "LangTemplateFunctions_de.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 223
    const-string/jumbo v0, "sun mon tue wed thu fri sat"

    const-string/jumbo v1, "daily"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    const-string/jumbo v0, "mon tue wed thu fri"

    const-string/jumbo v1, "Montag bis Freitag"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    const-string/jumbo v0, "sun sat"

    const-string/jumbo v1, "Samstag und Sonntag"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    const-string/jumbo v0, "sun"

    const-string/jumbo v1, "Sonntag"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    const-string/jumbo v0, "mon"

    const-string/jumbo v1, "Montag"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    const-string/jumbo v0, "tue"

    const-string/jumbo v1, "Dienstag"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    const-string/jumbo v0, "wed"

    const-string/jumbo v1, "Mitwoch"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    const-string/jumbo v0, "thu"

    const-string/jumbo v1, "Donnerstag"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    const-string/jumbo v0, "fri"

    const-string/jumbo v1, "Freitag"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    const-string/jumbo v0, "sat"

    const-string/jumbo v1, "Samstag"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    return-void
.end method
