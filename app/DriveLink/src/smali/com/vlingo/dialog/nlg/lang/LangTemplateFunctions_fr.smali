.class public Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
.source "LangTemplateFunctions_fr.java"


# static fields
.field private static final ALL_DAY:Ljava/lang/String; = "toute la journ\u00e9e"

.field private static final AND_FORMAT:[Ljava/lang/String;

.field private static final AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

.field private static final APPOINTMENT:Ljava/lang/String; = "rendez-vous"

.field private static final AT_TIME:Ljava/lang/String; = "\u00e0"

.field private static final DATE_ALT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final LOCALE:Ljava/util/Locale;

.field private static final LOCATION:Ljava/lang/String; = "lieu"

.field private static final OR_FORMAT:[Ljava/lang/String;

.field private static final PREMIER:Ljava/lang/String; = "1er"

.field private static final SPOKEN_HOURS_12H:[Ljava/lang/String;

.field private static final SPOKEN_HOURS_24H:[Ljava/lang/String;

.field private static final TIME_24H_FORMAT:Ljava/text/DateFormat;

.field private static final TIME_FORMAT:Ljava/text/DateFormat;

.field private static final TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final alarmDaysMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final repeatAlarmDaysMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    sget-object v0, Ljava/util/Locale;->FRENCH:Ljava/util/Locale;

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->LOCALE:Ljava/util/Locale;

    .line 20
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEEE d MMMM"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->DATE_FORMAT:Ljava/text/DateFormat;

    .line 21
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "H\'h\'mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->TIME_FORMAT:Ljava/text/DateFormat;

    .line 22
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH\'h\'mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->TIME_24H_FORMAT:Ljava/text/DateFormat;

    .line 24
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->DATE_ALT_MAP:Ljava/util/Map;

    .line 31
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, " et "

    aput-object v1, v0, v3

    const-string/jumbo v1, ", "

    aput-object v1, v0, v4

    const-string/jumbo v1, " et "

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->AND_FORMAT:[Ljava/lang/String;

    .line 32
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, " et "

    aput-object v1, v0, v3

    const-string/jumbo v1, ", "

    aput-object v1, v0, v4

    const-string/jumbo v1, ", et "

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    .line 33
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, " ou "

    aput-object v1, v0, v3

    const-string/jumbo v1, ", "

    aput-object v1, v0, v4

    const-string/jumbo v1, " ou "

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->OR_FORMAT:[Ljava/lang/String;

    .line 45
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr$2;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr$2;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->TYPE_MAP:Ljava/util/Map;

    .line 68
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "minuit"

    aput-object v1, v0, v3

    const-string/jumbo v1, "1 heure <minutes> du matin"

    aput-object v1, v0, v4

    const-string/jumbo v1, "2 heures du matin"

    aput-object v1, v0, v5

    const-string/jumbo v1, "3 heures du matin"

    aput-object v1, v0, v6

    const-string/jumbo v1, "4 heures du matin"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "5 heures du matin"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "6 heures du matin"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "7 heures du matin"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "8 heures du matin"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "9 heures du matin"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "10 heures du matin"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "11 heures du matin"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "midi"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "1 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "2 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "3 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "4 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "5 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "6 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "7 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "8 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "9 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "10 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "11 da noite"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->SPOKEN_HOURS_12H:[Ljava/lang/String;

    .line 95
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "24 heures"

    aput-object v1, v0, v3

    const-string/jumbo v1, "1 heure"

    aput-object v1, v0, v4

    const-string/jumbo v1, "2 heures"

    aput-object v1, v0, v5

    const-string/jumbo v1, "3 heures"

    aput-object v1, v0, v6

    const-string/jumbo v1, "4 heures"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "5 heures"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "6 heures"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "7 heures"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "8 heures"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "9 heures"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "10 heures"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "11 heures"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "12 heures"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "13 heures"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "14 heures"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "15 heures"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "16 heures"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "17 heures"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "18 heures"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "19 heures"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "20 heures"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "21 heures"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "22 heures"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "23 heures"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->SPOKEN_HOURS_24H:[Ljava/lang/String;

    .line 260
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr$3;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr$3;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->alarmDaysMap:Ljava/util/Map;

    .line 275
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr$4;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr$4;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->repeatAlarmDaysMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;-><init>()V

    return-void
.end method

.method private appendMinutes(Ljava/lang/StringBuilder;I)V
    .locals 1
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "m"    # I

    .prologue
    .line 132
    if-lez p2, :cond_0

    .line 133
    const-string/jumbo v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 136
    :cond_0
    return-void
.end method

.method private timeSpoken(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0xc

    .line 139
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->timeAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 141
    .local v0, "c":Ljava/util/Calendar;
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 142
    .local v1, "h":I
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 143
    .local v2, "m":I
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->use24HourTime()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 144
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->TIME_24H_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v4

    .line 176
    :goto_0
    return-object v4

    .line 146
    :cond_0
    if-nez v1, :cond_1

    .line 147
    const-string/jumbo v4, "minuit"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    invoke-direct {p0, v3, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->appendMinutes(Ljava/lang/StringBuilder;I)V

    .line 176
    :goto_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ":00"

    const-string/jumbo v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 149
    :cond_1
    if-ne v1, v5, :cond_2

    .line 150
    const-string/jumbo v4, "midi"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    invoke-direct {p0, v3, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->appendMinutes(Ljava/lang/StringBuilder;I)V

    goto :goto_1

    .line 152
    :cond_2
    if-ge v1, v5, :cond_4

    .line 153
    if-ne v1, v6, :cond_3

    .line 154
    const-string/jumbo v4, "1 heure"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :goto_2
    invoke-direct {p0, v3, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->appendMinutes(Ljava/lang/StringBuilder;I)V

    .line 160
    const-string/jumbo v4, " du matin"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 156
    :cond_3
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 157
    const-string/jumbo v4, " heures"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 161
    :cond_4
    const/16 v4, 0x12

    if-gt v1, v4, :cond_6

    .line 162
    if-ne v1, v6, :cond_5

    .line 163
    const-string/jumbo v4, "1 heure"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    :goto_3
    invoke-direct {p0, v3, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->appendMinutes(Ljava/lang/StringBuilder;I)V

    .line 169
    const-string/jumbo v4, " da tarde"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 165
    :cond_5
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 166
    const-string/jumbo v4, " heures"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 171
    :cond_6
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 172
    const-string/jumbo v4, " heures"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    invoke-direct {p0, v3, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->appendMinutes(Ljava/lang/StringBuilder;I)V

    .line 174
    const-string/jumbo v4, " da noite"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method protected alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 296
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 298
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 299
    .local v4, "time":Ljava/lang/String;
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    const-string/jumbo v5, "repeat"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 302
    .local v2, "repeat":Z
    const-string/jumbo v5, "days"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, "days":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 305
    if-eqz v2, :cond_5

    .line 306
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->repeatAlarmDaysMap:Ljava/util/Map;

    invoke-virtual {p0, v0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->renderDays(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 307
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 308
    const-string/jumbo v5, " tous les jours, chaque semaine"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :cond_0
    :goto_0
    const-string/jumbo v5, "enabled"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 333
    .local v1, "enabled":Z
    if-nez v1, :cond_1

    .line 334
    const-string/jumbo v5, ", inactive"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 309
    .end local v1    # "enabled":Z
    :cond_2
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 310
    const-string/jumbo v5, " tous les jours en semaine, chaque semaine"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 311
    :cond_3
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 312
    const-string/jumbo v5, " tous les samedis et dimanches"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 314
    :cond_4
    const-string/jumbo v5, " tous les "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 318
    :cond_5
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->alarmDaysMap:Ljava/util/Map;

    invoke-virtual {p0, v0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->renderDays(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 319
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 320
    const-string/jumbo v5, " les sept prochains jours"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 321
    :cond_6
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 322
    const-string/jumbo v5, " en semaine"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 323
    :cond_7
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 324
    const-string/jumbo v5, " le weekend"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 326
    :cond_8
    const-string/jumbo v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method protected alarmAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected andFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointmentForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "brief"    # Z

    .prologue
    const/16 v6, 0x20

    .line 208
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 210
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "title"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 211
    const-string/jumbo v5, "title"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 212
    .local v4, "title":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 213
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :goto_0
    const/16 v5, 0x2c

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 220
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    const-string/jumbo v5, "time"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->isEmptyOrUnspecifiedAmPm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 221
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 222
    const-string/jumbo v5, "all_day"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 223
    const-string/jumbo v5, "toute la journ\u00e9e"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    :cond_1
    :goto_1
    const-string/jumbo v5, "date"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 233
    const-string/jumbo v5, "date_alt"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 234
    .local v0, "dateAlt":Ljava/lang/String;
    const-string/jumbo v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 236
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->DATE_ALT_MAP:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_2
    :goto_2
    if-nez p3, :cond_3

    const-string/jumbo v5, "location"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 243
    const-string/jumbo v5, "location"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 244
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 245
    const-string/jumbo v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    const-string/jumbo v5, "lieu"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 248
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    .end local v1    # "location":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 215
    .restart local v4    # "title":Ljava/lang/String;
    :cond_4
    const-string/jumbo v5, "rendez-vous"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 225
    .end local v4    # "title":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 226
    .local v3, "time":Ljava/lang/String;
    const-string/jumbo v5, "\u00e0"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 228
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 238
    .end local v3    # "time":Ljava/lang/String;
    .restart local v0    # "dateAlt":Ljava/lang/String;
    :cond_6
    const-string/jumbo v5, "date"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method protected appointmentAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public date(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 128
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->DATE_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->date(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected orFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->OR_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 10
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "task"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/16 v9, 0x2c

    .line 347
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 349
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v7, "title"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 350
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 351
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 355
    :cond_0
    const-string/jumbo v7, "date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 356
    .local v0, "date":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 357
    const-string/jumbo v7, " pour "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 359
    .local v1, "dateAlt":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 360
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    :goto_0
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 367
    .end local v1    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v7, "reminder_date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 368
    .local v2, "reminderDate":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 369
    const-string/jumbo v7, " rappel "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "reminder_date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 371
    .local v3, "reminderDateAlt":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 372
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    :goto_1
    const-string/jumbo v7, "reminder_timer"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 378
    .local v4, "reminderTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 379
    const-string/jumbo v7, " \u00e0 "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    .end local v3    # "reminderDateAlt":Ljava/lang/String;
    .end local v4    # "reminderTime":Ljava/lang/String;
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, " ,"

    invoke-static {v7, v8}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 362
    .end local v2    # "reminderDate":Ljava/lang/String;
    .restart local v1    # "dateAlt":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 374
    .end local v1    # "dateAlt":Ljava/lang/String;
    .restart local v2    # "reminderDate":Ljava/lang/String;
    .restart local v3    # "reminderDateAlt":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected taskAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 342
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public time(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->use24HourTime()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->TIME_24H_FORMAT:Ljava/text/DateFormat;

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->TIME_FORMAT:Ljava/text/DateFormat;

    goto :goto_0
.end method

.method protected typeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_fr;->TYPE_MAP:Ljava/util/Map;

    return-object v0
.end method
