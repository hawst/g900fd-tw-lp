.class public Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
.source "LangTemplateFunctions_pt_BR.java"


# static fields
.field private static final ALL_DAY:Ljava/lang/String; = "dia inteiro"

.field private static final AND_FORMAT:[Ljava/lang/String;

.field private static final AND_FORMAT_SERIES_COMMA:[Ljava/lang/String;

.field private static final APPOINTMENT:Ljava/lang/String; = "compromisso"

.field private static final APPOINTMENT_PRE_DATE:Ljava/lang/String; = "na"

.field private static final APPOINTMENT_PRE_LOCATION:Ljava/lang/String; = "localiza\u00e7\u00e3o"

.field private static final APPOINTMENT_PRE_TIME:Ljava/lang/String; = "\u00e0s"

.field private static final DATE_ALT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATE_FORMAT_DISPLAY:Ljava/text/DateFormat;

.field private static final DATE_FORMAT_SPOKEN:Ljava/text/DateFormat;

.field private static final HOUR:Ljava/lang/String; = "hora"

.field private static final HOURS:Ljava/lang/String; = "horas"

.field private static final LOCALE:Ljava/util/Locale;

.field private static final MIDDAY:Ljava/lang/String; = "meio-dia"

.field private static final MIDNIGHT:Ljava/lang/String; = "meia-noite"

.field private static final MINUTE:Ljava/lang/String; = "minuto"

.field private static final MINUTES:Ljava/lang/String; = "minutos"

.field private static final OR_FORMAT:[Ljava/lang/String;

.field private static final POST_PROCESS_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

.field private static final SPOKEN_HOURS_12H:[Ljava/lang/String;

.field private static final SPOKEN_HOURS_24H:[Ljava/lang/String;

.field private static final TIME_FORMAT_DISPLAY_12H:Ljava/text/DateFormat;

.field private static final TIME_FORMAT_DISPLAY_24H:Ljava/text/DateFormat;

.field private static final TIME_FORMAT_SPOKEN_12H:Ljava/text/DateFormat;

.field private static final TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final alarmDaysMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 18
    new-instance v0, Ljava/util/Locale;

    const-string/jumbo v1, "pt"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->LOCALE:Ljava/util/Locale;

    .line 38
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "meia-noite"

    aput-object v1, v0, v5

    const-string/jumbo v1, "1 da manh\u00e3"

    aput-object v1, v0, v6

    const-string/jumbo v1, "2 da manh\u00e3"

    aput-object v1, v0, v4

    const-string/jumbo v1, "3 da manh\u00e3"

    aput-object v1, v0, v7

    const-string/jumbo v1, "4 da manh\u00e3"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string/jumbo v2, "5 da manh\u00e3"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "6 da manh\u00e3"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "7 da manh\u00e3"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "8 da manh\u00e3"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "9 da manh\u00e3"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "10 da manh\u00e3"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "11 da manh\u00e3"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "meio-dia"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "1 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "2 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "3 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "4 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "5 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "6 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "7 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "8 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "9 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "10 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "11 da noite"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->SPOKEN_HOURS_12H:[Ljava/lang/String;

    .line 65
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "24 horas"

    aput-object v1, v0, v5

    const-string/jumbo v1, "1 hora"

    aput-object v1, v0, v6

    const-string/jumbo v1, "2 horas"

    aput-object v1, v0, v4

    const-string/jumbo v1, "3 horas"

    aput-object v1, v0, v7

    const-string/jumbo v1, "4 horas"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string/jumbo v2, "5 horas"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "6 horas"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "7 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "8 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "9 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "10 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "11 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "12 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "13 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "14 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "15 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "16 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "17 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "18 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "19 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "20 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "21 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "22 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "23 horas"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->SPOKEN_HOURS_24H:[Ljava/lang/String;

    .line 122
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEEE, \'dia\' d \'de\' MMMM"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->DATE_FORMAT_DISPLAY:Ljava/text/DateFormat;

    .line 123
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEEE, \'dia\' d \'de\' MMMM"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->DATE_FORMAT_SPOKEN:Ljava/text/DateFormat;

    .line 125
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "h:mm a"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->TIME_FORMAT_DISPLAY_12H:Ljava/text/DateFormat;

    .line 126
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH:mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->TIME_FORMAT_DISPLAY_24H:Ljava/text/DateFormat;

    .line 128
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "h:mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->TIME_FORMAT_SPOKEN_12H:Ljava/text/DateFormat;

    .line 137
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->DATE_ALT_MAP:Ljava/util/Map;

    .line 144
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, " e "

    aput-object v1, v0, v5

    const-string/jumbo v1, ", "

    aput-object v1, v0, v6

    const-string/jumbo v1, " e "

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->AND_FORMAT:[Ljava/lang/String;

    .line 145
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, " e "

    aput-object v1, v0, v5

    const-string/jumbo v1, ", "

    aput-object v1, v0, v6

    const-string/jumbo v1, ", e "

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->AND_FORMAT_SERIES_COMMA:[Ljava/lang/String;

    .line 146
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, " ou "

    aput-object v1, v0, v5

    const-string/jumbo v1, ", "

    aput-object v1, v0, v6

    const-string/jumbo v1, " ou "

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->OR_FORMAT:[Ljava/lang/String;

    .line 165
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR$2;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR$2;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->TYPE_MAP:Ljava/util/Map;

    .line 323
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR$3;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR$3;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->alarmDaysMap:Ljava/util/Map;

    .line 431
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/Replace;

    const/4 v1, 0x6

    new-array v1, v1, [[Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "\\b\u00e0s (?=meia-noite|01:|1 hora|1 da manh\u00e3|1 da tarde)"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u00e0 "

    aput-object v3, v2, v6

    aput-object v2, v1, v5

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "\\b\u00e0s (?=meio-dia)"

    aput-object v3, v2, v5

    const-string/jumbo v3, "ao "

    aput-object v3, v2, v6

    aput-object v2, v1, v6

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "\\bna (?=[Dd]omingo|[Ss]\u00e1bado)"

    aput-object v3, v2, v5

    const-string/jumbo v3, "no "

    aput-object v3, v2, v6

    aput-object v2, v1, v4

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "\\bno (?=[^ ]*-feira)"

    aput-object v3, v2, v5

    const-string/jumbo v3, "na "

    aput-object v3, v2, v6

    aput-object v2, v1, v7

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "\\btoda (?=[Dd]omingo|[Ss]\u00e1bado)"

    aput-object v3, v2, v5

    const-string/jumbo v3, "todo "

    aput-object v3, v2, v6

    aput-object v2, v1, v8

    const/4 v2, 0x5

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v4, "\\btodo (?=[^ ]*-feira)"

    aput-object v4, v3, v5

    const-string/jumbo v4, "toda "

    aput-object v4, v3, v6

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/nlg/lang/Replace;-><init>([[Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->POST_PROCESS_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;-><init>()V

    return-void
.end method

.method private appendMinutes(Ljava/lang/StringBuilder;I)V
    .locals 1
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "m"    # I

    .prologue
    .line 199
    if-lez p2, :cond_0

    .line 200
    const-string/jumbo v0, " e "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 202
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 203
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    const-string/jumbo v0, "minuto"

    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :cond_0
    return-void

    .line 203
    :cond_1
    const-string/jumbo v0, "minutos"

    goto :goto_0
.end method

.method private timeDisplay(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->use24HourTime()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const-string/jumbo v0, "00:"

    const-string/jumbo v1, "24:"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 194
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->TIME_FORMAT_DISPLAY_12H:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private timeSpoken(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0xc

    .line 208
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 209
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->timeAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 210
    .local v0, "c":Ljava/util/Calendar;
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 211
    .local v1, "h":I
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 212
    .local v2, "m":I
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->use24HourTime()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 213
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->SPOKEN_HOURS_24H:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    invoke-direct {p0, v3, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->appendMinutes(Ljava/lang/StringBuilder;I)V

    .line 215
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 233
    :goto_0
    return-object v4

    .line 217
    :cond_0
    if-nez v1, :cond_1

    .line 218
    const-string/jumbo v4, "meia-noite"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    invoke-direct {p0, v3, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->appendMinutes(Ljava/lang/StringBuilder;I)V

    .line 233
    :goto_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ":00"

    const-string/jumbo v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 220
    :cond_1
    if-ne v1, v5, :cond_2

    .line 221
    const-string/jumbo v4, "meio-dia"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    invoke-direct {p0, v3, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->appendMinutes(Ljava/lang/StringBuilder;I)V

    goto :goto_1

    .line 223
    :cond_2
    if-ge v1, v5, :cond_3

    .line 224
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->TIME_FORMAT_SPOKEN_12H:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    const-string/jumbo v4, " da manh\u00e3"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 226
    :cond_3
    const/16 v4, 0x12

    if-gt v1, v4, :cond_4

    .line 227
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->TIME_FORMAT_SPOKEN_12H:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    const-string/jumbo v4, " da tarde"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 230
    :cond_4
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->TIME_FORMAT_SPOKEN_12H:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    const-string/jumbo v4, " da noite"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private timeSpokenDeprecated(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 238
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 239
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->timeAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 240
    .local v0, "c":Ljava/util/Calendar;
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 241
    .local v1, "h":I
    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 242
    .local v2, "m":I
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->use24HourTime()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->SPOKEN_HOURS_24H:[Ljava/lang/String;

    aget-object v4, v4, v1

    :goto_0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    if-lez v2, :cond_0

    .line 244
    const-string/jumbo v4, " e "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 246
    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 247
    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    const-string/jumbo v4, "minuto"

    :goto_1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 242
    :cond_1
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->SPOKEN_HOURS_12H:[Ljava/lang/String;

    aget-object v4, v4, v1

    goto :goto_0

    .line 247
    :cond_2
    const-string/jumbo v4, "minutos"

    goto :goto_1
.end method


# virtual methods
.method protected alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 340
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 342
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 343
    .local v4, "time":Ljava/lang/String;
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    const-string/jumbo v5, "repeat"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 346
    .local v2, "repeat":Z
    const-string/jumbo v5, "days"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 347
    .local v0, "days":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->alarmDaysMap:Ljava/util/Map;

    invoke-virtual {p0, v0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->renderDays(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 349
    if-eqz v0, :cond_0

    .line 350
    if-eqz v2, :cond_5

    .line 351
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 352
    const-string/jumbo v5, " todos os dias"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    :cond_0
    :goto_0
    const-string/jumbo v5, "enabled"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 376
    .local v1, "enabled":Z
    if-nez v1, :cond_1

    .line 377
    const-string/jumbo v5, ", desativado"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 353
    .end local v1    # "enabled":Z
    :cond_2
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 354
    const-string/jumbo v5, " todos os dias da semana"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 355
    :cond_3
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 356
    const-string/jumbo v5, " todo S\u00e1bado e Domingo"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 358
    :cond_4
    const-string/jumbo v5, " toda "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 362
    :cond_5
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 363
    const-string/jumbo v5, " para os pr\u00f3ximos 7 dias"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 364
    :cond_6
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 365
    const-string/jumbo v5, " Segunda a Sexta"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 366
    :cond_7
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 367
    const-string/jumbo v5, " S\u00e1bado e Domingo"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 369
    :cond_8
    const-string/jumbo v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method protected alarmAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->AND_FORMAT_SERIES_COMMA:[Ljava/lang/String;

    return-object v0
.end method

.method protected andFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointmentForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "brief"    # Z

    .prologue
    const/16 v6, 0x20

    .line 269
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 271
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "title"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 272
    const-string/jumbo v5, "title"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 273
    .local v4, "title":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 274
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    :goto_0
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 281
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    const-string/jumbo v5, "date"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 282
    const-string/jumbo v5, "date_alt"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, "dateAlt":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 284
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->DATE_ALT_MAP:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    :goto_1
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 293
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v5, "time"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->isEmptyOrUnspecifiedAmPm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 294
    const-string/jumbo v5, "all_day"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 295
    const-string/jumbo v5, "dia inteiro"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    :goto_2
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 305
    :cond_2
    if-nez p3, :cond_3

    const-string/jumbo v5, "location"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 306
    const-string/jumbo v5, "location"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 307
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 308
    const-string/jumbo v5, "localiza\u00e7\u00e3o"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 310
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 315
    .end local v1    # "location":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 276
    .restart local v4    # "title":Ljava/lang/String;
    :cond_4
    const-string/jumbo v5, "compromisso"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 286
    .end local v4    # "title":Ljava/lang/String;
    .restart local v0    # "dateAlt":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "na"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 288
    const-string/jumbo v5, "date"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 297
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_6
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 298
    .local v3, "time":Ljava/lang/String;
    const-string/jumbo v5, "\u00e0s"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 300
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method protected appointmentAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->AND_FORMAT_SERIES_COMMA:[Ljava/lang/String;

    return-object v0
.end method

.method public date(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->isDisplay()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->DATE_FORMAT_DISPLAY:Ljava/text/DateFormat;

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->date(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->DATE_FORMAT_SPOKEN:Ljava/text/DateFormat;

    goto :goto_0
.end method

.method protected orFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->OR_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public postProcess(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 442
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->POST_PROCESS_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/nlg/lang/Replace;->replace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 10
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "task"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/16 v9, 0x2c

    .line 390
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 392
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v7, "title"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 393
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 394
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 398
    :cond_0
    const-string/jumbo v7, "date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "date":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 401
    const-string/jumbo v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 403
    .local v1, "dateAlt":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 404
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    :goto_0
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 411
    .end local v1    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v7, "reminder_date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 412
    .local v2, "reminderDate":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 413
    const-string/jumbo v7, " lembrete "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "reminder_date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 415
    .local v3, "reminderDateAlt":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 416
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    :goto_1
    const-string/jumbo v7, "reminder_timer"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 422
    .local v4, "reminderTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 423
    const-string/jumbo v7, " \u00e0s "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    .end local v3    # "reminderDateAlt":Ljava/lang/String;
    .end local v4    # "reminderTime":Ljava/lang/String;
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, " ,"

    invoke-static {v7, v8}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 406
    .end local v2    # "reminderDate":Ljava/lang/String;
    .restart local v1    # "dateAlt":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 418
    .end local v1    # "dateAlt":Ljava/lang/String;
    .restart local v2    # "reminderDate":Ljava/lang/String;
    .restart local v3    # "reminderDateAlt":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected taskAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 385
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->AND_FORMAT_SERIES_COMMA:[Ljava/lang/String;

    return-object v0
.end method

.method public time(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->isDisplay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->timeDisplay(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->timeSpoken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected typeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_pt_BR;->TYPE_MAP:Ljava/util/Map;

    return-object v0
.end method
