.class public Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
.source "LangTemplateFunctions_ja.java"


# static fields
.field private static final ALL_DAY:Ljava/lang/String; = "\u7d42\u65e5"

.field private static final AM:Ljava/lang/String; = "\u5348\u524d"

.field private static final AND:Ljava/lang/String; = "\u3068"

.field private static final AND_FORMAT:[Ljava/lang/String;

.field private static final APPOINTMENT_LOCATION:Ljava/lang/String; = "\u5834\u6240:"

.field private static final APPOINTMENT_TITLE:Ljava/lang/String; = "\u30a4\u30d9\u30f3\u30c8\u540d:"

.field private static final COMMA:Ljava/lang/String; = "\u3001"

.field private static final DATE_ALT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final DATE_MARKER:Ljava/lang/String; = "\u65e5"

.field private static final FROM:Ljava/lang/String; = "\u304b\u3089"

.field private static final LOCALE:Ljava/util/Locale;

.field private static final MEETING:Ljava/lang/String; = "\u4f1a\u8b70"

.field private static final MY_EVENT:Ljava/lang/String; = "\u30de\u30a4 \u30a4\u30d9\u30f3\u30c8"

.field private static final OR:Ljava/lang/String; = "\u3084"

.field private static final OR_FORMAT:[Ljava/lang/String;

.field private static final PERIOD:Ljava/lang/String; = "\u3002"

.field private static final PM:Ljava/lang/String; = "\u5348\u5f8c"

.field private static final TIME_FORMAT_12H:Ljava/text/DateFormat;

.field private static final TIME_FORMAT_24H:Ljava/text/DateFormat;

.field private static final TIME_MARKER:Ljava/lang/String; = "\u6642"

.field private static final TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final alarmDaysMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->LOCALE:Ljava/util/Locale;

    .line 19
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->DATE_ALT_MAP:Ljava/util/Map;

    .line 48
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "\u3068"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->AND_FORMAT:[Ljava/lang/String;

    .line 49
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "\u3084"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\u3001"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->OR_FORMAT:[Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "M\u6708d\u65e5 EEEE"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->DATE_FORMAT:Ljava/text/DateFormat;

    .line 52
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "ah\u6642mm\u5206"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->TIME_FORMAT_12H:Ljava/text/DateFormat;

    .line 53
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "H\u6642mm\u5206"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->TIME_FORMAT_24H:Ljava/text/DateFormat;

    .line 55
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja$2;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja$2;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->TYPE_MAP:Ljava/util/Map;

    .line 158
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja$3;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja$3;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->alarmDaysMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;-><init>()V

    return-void
.end method


# virtual methods
.method protected alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 175
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "repeat"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 178
    .local v2, "repeat":Z
    const-string/jumbo v5, "days"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "days":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->alarmDaysMap:Ljava/util/Map;

    invoke-virtual {p0, v0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->renderDays(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_0

    .line 182
    if-eqz v2, :cond_5

    .line 183
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 184
    const-string/jumbo v5, "\u6bce\u65e5"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :goto_0
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 206
    .local v4, "time":Ljava/lang/String;
    const-string/jumbo v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    .end local v4    # "time":Ljava/lang/String;
    :cond_0
    const-string/jumbo v5, "enabled"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 212
    .local v1, "enabled":Z
    if-nez v1, :cond_1

    .line 213
    const-string/jumbo v5, "\u3001\u7121\u52b9"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,\u3001"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 185
    .end local v1    # "enabled":Z
    :cond_2
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 186
    const-string/jumbo v5, "\u6bce\u9031 \u5e73\u65e5"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 187
    :cond_3
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 188
    const-string/jumbo v5, "\u6bce\u9031 \u571f\u66dc\u65e5\u3068\u65e5\u66dc\u65e5"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 190
    :cond_4
    const-string/jumbo v5, "\u6bce\u9031 "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 194
    :cond_5
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 195
    const-string/jumbo v5, "\u6b21\u306e7\u65e5\u9593"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 196
    :cond_6
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 197
    const-string/jumbo v5, "\u6708\u66dc\u65e5\u304b\u3089\u91d1\u66dc\u65e5"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 198
    :cond_7
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 199
    const-string/jumbo v5, "\u571f\u66dc\u65e5\u3068\u65e5\u66dc\u65e5"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 201
    :cond_8
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method protected alarmAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected andFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
    .locals 8
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointmentForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "brief"    # Z

    .prologue
    const/16 v6, 0x20

    .line 107
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "title"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 110
    const-string/jumbo v5, "title"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 111
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 112
    invoke-static {v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 113
    const-string/jumbo v5, "\u30a4\u30d9\u30f3\u30c8\u540d:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    :goto_0
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 121
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    const-string/jumbo v5, "date"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 122
    const-string/jumbo v5, "date_alt"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "dateAlt":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 124
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->DATE_ALT_MAP:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_1
    :goto_1
    const-string/jumbo v5, "time"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->isEmptyOrUnspecifiedAmPm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 131
    const-string/jumbo v5, "all_day"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 132
    const-string/jumbo v5, "\u7d42\u65e5"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :cond_2
    :goto_2
    if-nez p3, :cond_3

    const-string/jumbo v5, "location"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 142
    const-string/jumbo v5, "location"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 143
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 144
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 145
    const-string/jumbo v5, "\u5834\u6240:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .end local v1    # "location":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "  "

    const-string/jumbo v7, " "

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 116
    .restart local v4    # "title":Ljava/lang/String;
    :cond_4
    const-string/jumbo v5, "\u30de\u30a4 \u30a4\u30d9\u30f3\u30c8"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 126
    .end local v4    # "title":Ljava/lang/String;
    .restart local v0    # "dateAlt":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "date"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 134
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_6
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 135
    .local v3, "time":Ljava/lang/String;
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 136
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const-string/jumbo v5, "\u304b\u3089"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method protected appointmentAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public date(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 81
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->DATE_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->date(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "nameAndId"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->name(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "result":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->isSpoken()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\u300c"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\u300d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 71
    .end local v0    # "result":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method protected orFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->OR_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 10
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "task"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/16 v9, 0x3001

    .line 226
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 228
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v7, "title"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 229
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 230
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 234
    :cond_0
    const-string/jumbo v7, "date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "date":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 236
    const-string/jumbo v7, " \u671f\u65e5 "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 238
    .local v1, "dateAlt":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 239
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    :goto_0
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 246
    .end local v1    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v7, "reminder_date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 247
    .local v2, "reminderDate":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 248
    const-string/jumbo v7, " \u30ea\u30de\u30a4\u30f3\u30c0\u30fc"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "reminder_date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 250
    .local v3, "reminderDateAlt":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 251
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    :goto_1
    const-string/jumbo v7, "reminder_timer"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 257
    .local v4, "reminderTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 258
    const-string/jumbo v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    .end local v3    # "reminderDateAlt":Ljava/lang/String;
    .end local v4    # "reminderTime":Ljava/lang/String;
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, " ,\u3001"

    invoke-static {v7, v8}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 241
    .end local v2    # "reminderDate":Ljava/lang/String;
    .restart local v1    # "dateAlt":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 253
    .end local v1    # "dateAlt":Ljava/lang/String;
    .restart local v2    # "reminderDate":Ljava/lang/String;
    .restart local v3    # "reminderDateAlt":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected taskAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public time(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->use24HourTime()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->TIME_FORMAT_24H:Ljava/text/DateFormat;

    .line 87
    .local v0, "format":Ljava/text/DateFormat;
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "00\u5206"

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 86
    .end local v0    # "format":Ljava/text/DateFormat;
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->TIME_FORMAT_12H:Ljava/text/DateFormat;

    goto :goto_0
.end method

.method protected typeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ja;->TYPE_MAP:Ljava/util/Map;

    return-object v0
.end method
