.class public Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;
.super Ljava/lang/Object;
.source "OutputTemplateFunctions.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;

.field public static final kMinsPerDay:I = 0x5a0

.field public static final kMinsPerHour:I = 0x3c


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static daysOfMins(Ljava/lang/String;)I
    .locals 4
    .param p0, "durationMinutesStr"    # Ljava/lang/String;

    .prologue
    .line 21
    sget-object v1, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23
    sget-object v1, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "daysOfMins("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 25
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 27
    .local v0, "durMins":I
    if-gez v0, :cond_1

    .line 29
    neg-int v0, v0

    .line 32
    :cond_1
    div-int/lit16 v1, v0, 0x5a0

    return v1
.end method

.method public static daysOfMinsText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "durationMinutesStr"    # Ljava/lang/String;
    .param p1, "dayLabel"    # Ljava/lang/String;
    .param p2, "daysLabel"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->daysOfMins(Ljava/lang/String;)I

    move-result v0

    .line 38
    .local v0, "dayNum":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .end local p1    # "dayLabel":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .restart local p1    # "dayLabel":Ljava/lang/String;
    :cond_0
    move-object p1, p2

    goto :goto_0
.end method

.method public static hoursOfMins(Ljava/lang/String;)I
    .locals 4
    .param p0, "durationMinutesStr"    # Ljava/lang/String;

    .prologue
    .line 43
    sget-object v1, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    sget-object v1, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "hoursOfMins("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 47
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 49
    .local v0, "durMins":I
    if-gez v0, :cond_1

    .line 51
    neg-int v0, v0

    .line 54
    :cond_1
    rem-int/lit16 v1, v0, 0x5a0

    div-int/lit8 v1, v1, 0x3c

    return v1
.end method

.method public static hoursOfMinsText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "durationMinutesStr"    # Ljava/lang/String;
    .param p1, "hourLabel"    # Ljava/lang/String;
    .param p2, "hoursLabel"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->hoursOfMins(Ljava/lang/String;)I

    move-result v0

    .line 61
    .local v0, "hourNum":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .end local p1    # "hourLabel":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .restart local p1    # "hourLabel":Ljava/lang/String;
    :cond_0
    move-object p1, p2

    goto :goto_0
.end method

.method public static isEmpty(Ljava/lang/String;)Z
    .locals 4
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 111
    if-nez p0, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v1

    .line 112
    :cond_1
    const-string/jumbo v2, "[ \t]+"

    const-string/jumbo v3, ""

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "val":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "x"    # Ljava/lang/Object;
    .param p1, "y"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 121
    if-nez p0, :cond_1

    .line 122
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 124
    :cond_1
    if-eqz p1, :cond_0

    .line 127
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isFuture(Ljava/lang/String;)Z
    .locals 4
    .param p0, "durStr"    # Ljava/lang/String;

    .prologue
    .line 100
    sget-object v1, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    sget-object v1, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "isFuture("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 104
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 106
    .local v0, "dur":I
    if-lez v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isNotEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNotEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "x"    # Ljava/lang/Object;
    .param p1, "y"    # Ljava/lang/Object;

    .prologue
    .line 131
    invoke-static {p0, p1}, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPast(Ljava/lang/String;)Z
    .locals 4
    .param p0, "durStr"    # Ljava/lang/String;

    .prologue
    .line 89
    sget-object v1, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    sget-object v1, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "isPast("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 93
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 95
    .local v0, "dur":I
    if-gez v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static minutesOfMins(Ljava/lang/String;)I
    .locals 4
    .param p0, "durationMinutesStr"    # Ljava/lang/String;

    .prologue
    .line 66
    sget-object v1, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    sget-object v1, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "minutesOfMins("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 70
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 72
    .local v0, "durMins":I
    if-gez v0, :cond_1

    .line 74
    neg-int v0, v0

    .line 77
    :cond_1
    rem-int/lit8 v1, v0, 0x3c

    return v1
.end method

.method public static minutesOfMinsText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "durationMinutesStr"    # Ljava/lang/String;
    .param p1, "minuteLabel"    # Ljava/lang/String;
    .param p2, "minutesLabel"    # Ljava/lang/String;

    .prologue
    .line 82
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;->minutesOfMins(Ljava/lang/String;)I

    move-result v0

    .line 84
    .local v0, "minNum":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .end local p1    # "minuteLabel":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .restart local p1    # "minuteLabel":Ljava/lang/String;
    :cond_0
    move-object p1, p2

    goto :goto_0
.end method
