.class public abstract Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
.super Ljava/lang/Object;
.source "LangTemplateFunctions.java"


# static fields
.field protected static final COMMA_JOINER:Lcom/google/common/base/Joiner;

.field protected static final ID_SEPARATOR:Ljava/lang/String; = "_id_"

.field private static final PHONE_NUMBER_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

.field protected static final PIPE_SPLITTER:Lcom/google/common/base/Splitter;

.field private static final RAW_DATE_FORMAT:Ljava/text/DateFormat;

.field private static final RAW_TIME_FORMAT:Ljava/text/DateFormat;

.field public static final SLOT_ALARM_DAYS:Ljava/lang/String; = "days"

.field public static final SLOT_ALARM_ENABLED:Ljava/lang/String; = "enabled"

.field public static final SLOT_ALARM_LIST:Ljava/lang/String; = "alarm_list"

.field public static final SLOT_ALARM_REPEAT:Ljava/lang/String; = "repeat"

.field public static final SLOT_ALARM_TIME:Ljava/lang/String; = "time"

.field public static final SLOT_TASK_DATE:Ljava/lang/String; = "date"

.field public static final SLOT_TASK_DATE_ALT:Ljava/lang/String; = "date_alt"

.field public static final SLOT_TASK_LIST:Ljava/lang/String; = "task_list"

.field public static final SLOT_TASK_REMINDER_DATE:Ljava/lang/String; = "reminder_date"

.field public static final SLOT_TASK_REMINDER_DATE_ALT:Ljava/lang/String; = "reminder_date_alt"

.field public static final SLOT_TASK_REMINDER_TIME:Ljava/lang/String; = "reminder_timer"

.field public static final SLOT_TASK_TITLE:Ljava/lang/String; = "title"

.field protected static final SPACE_SPLITTER:Lcom/google/common/base/Splitter;

.field protected static final TODAY:Ljava/lang/String; = "today"

.field protected static final TOMORROW:Ljava/lang/String; = "tomorrow"

.field protected static final TYPES:[Ljava/lang/String;

.field protected static final TYPE_HOME:Ljava/lang/String; = "home"

.field protected static final TYPE_MOBILE:Ljava/lang/String; = "mobile"

.field protected static final TYPE_OTHER:Ljava/lang/String; = "other"

.field protected static final TYPE_WORK:Ljava/lang/String; = "work"

.field protected static ivLogger:Lcom/vlingo/common/log4j/VLogger;

.field private static final svLangMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field protected final NUMBER_PATTERN:Ljava/util/regex/Pattern;

.field protected ivIsDisplay:Z

.field protected ivUse24HourTime:Z

.field protected ivUserName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 32
    const-class v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 34
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd"

    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->RAW_DATE_FORMAT:Ljava/text/DateFormat;

    .line 35
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH:mm"

    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->RAW_TIME_FORMAT:Ljava/text/DateFormat;

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "home"

    aput-object v1, v0, v4

    const-string/jumbo v1, "mobile"

    aput-object v1, v0, v5

    const-string/jumbo v1, "work"

    aput-object v1, v0, v3

    const/4 v1, 0x3

    const-string/jumbo v2, "other"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->TYPES:[Ljava/lang/String;

    .line 49
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->svLangMap:Ljava/util/Map;

    .line 99
    const/16 v0, 0x7c

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->trimResults()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->PIPE_SPLITTER:Lcom/google/common/base/Splitter;

    .line 100
    const/16 v0, 0x20

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->trimResults()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->SPACE_SPLITTER:Lcom/google/common/base/Splitter;

    .line 101
    const-string/jumbo v0, ", "

    invoke-static {v0}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->COMMA_JOINER:Lcom/google/common/base/Joiner;

    .line 233
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/Replace;

    new-array v1, v5, [[Ljava/lang/String;

    new-array v2, v3, [Ljava/lang/String;

    const-string/jumbo v3, "(?<=[0-9])[\\s-]*(?=[0-9])"

    aput-object v3, v2, v4

    const-string/jumbo v3, " "

    aput-object v3, v2, v5

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/nlg/lang/Replace;-><init>([[Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->PHONE_NUMBER_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    const-string/jumbo v0, "\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->NUMBER_PATTERN:Ljava/util/regex/Pattern;

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivIsDisplay:Z

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivUse24HourTime:Z

    return-void
.end method

.method protected static badDate(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 536
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static badTime(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    .line 540
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static clone(Ljava/text/DateFormat;)Ljava/text/DateFormat;
    .locals 1
    .param p0, "dateFormat"    # Ljava/text/DateFormat;

    .prologue
    .line 552
    invoke-virtual {p0}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    return-object v0
.end method

.method public static createInstance(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
    .locals 7
    .param p0, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 117
    :try_start_0
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->svLangMap:Ljava/util/Map;

    invoke-interface {v4, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 118
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;>;"
    if-nez v0, :cond_0

    .line 119
    new-instance v4, Lcom/vlingo/common/util/VLocale;

    invoke-direct {v4, p0}, Lcom/vlingo/common/util/VLocale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/vlingo/common/util/VLocale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 120
    .local v3, "language":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->svLangMap:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;>;"
    check-cast v0, Ljava/lang/Class;

    .line 122
    .end local v3    # "language":Ljava/lang/String;
    .restart local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;>;"
    :cond_0
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 123
    if-nez v0, :cond_2

    const-string/jumbo v1, "null"

    .line 124
    .local v1, "className":Ljava/lang/String;
    :goto_0
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "NLG language mapping: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " -> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 126
    .end local v1    # "className":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;

    .line 128
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;>;"
    :goto_1
    return-object v4

    .line 123
    .restart local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;>;"
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 127
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;>;"
    :catch_0
    move-exception v2

    .line 128
    .local v2, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected static dateAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 3
    .param p0, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 430
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->dateAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 431
    .local v1, "date":Ljava/util/Date;
    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 432
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 433
    return-object v0
.end method

.method protected static dateAsDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .param p0, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 423
    :try_start_0
    sget-object v1, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->RAW_DATE_FORMAT:Ljava/text/DateFormat;

    invoke-static {v1, p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->parse(Ljava/text/DateFormat;Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 424
    :catch_0
    move-exception v0

    .line 425
    .local v0, "e":Ljava/text/ParseException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected static format(Ljava/text/DateFormat;Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .param p0, "dateFormat"    # Ljava/text/DateFormat;
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 544
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->clone(Ljava/text/DateFormat;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static formatList(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "itemPipeList"    # Ljava/lang/String;
    .param p1, "format"    # [Ljava/lang/String;

    .prologue
    .line 484
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->toArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static formatList(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "format"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 480
    .local p0, "items":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->toArray(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static formatList([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "items"    # [Ljava/lang/String;
    .param p1, "format"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 459
    array-length v2, p0

    packed-switch v2, :pswitch_data_0

    .line 467
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 468
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    add-int/lit8 v2, v2, -0x2

    if-ge v0, v2, :cond_0

    .line 469
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 470
    aget-object v2, p1, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 461
    .end local v0    # "i":I
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    :pswitch_0
    const-string/jumbo v2, ""

    .line 475
    :goto_1
    return-object v2

    .line 463
    :pswitch_1
    aget-object v2, p0, v4

    goto :goto_1

    .line 465
    :pswitch_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, p0, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p0, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 472
    .restart local v0    # "i":I
    .restart local v1    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    array-length v2, p0

    add-int/lit8 v2, v2, -0x2

    aget-object v2, p0, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    const/4 v2, 0x2

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 474
    array-length v2, p0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, p0, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 459
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected static getId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 202
    const-string/jumbo v1, "_id_"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 203
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 204
    const-string/jumbo v1, "_id_"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 206
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getLanguages()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->svLangMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected static getName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 193
    const-string/jumbo v1, "_id_"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 194
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 195
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 197
    .end local p0    # "name":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method protected static getProperty(Ljava/util/ResourceBundle;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "bundle"    # Ljava/util/ResourceBundle;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 521
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 523
    :goto_0
    return-object v1

    .line 522
    :catch_0
    move-exception v0

    .line 523
    .local v0, "e":Ljava/util/MissingResourceException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static isEmpty(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p0, "formList"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 402
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->sizeOf(Lcom/vlingo/dialog/model/IForm;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static isEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 508
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static isEmptyOrUnspecifiedAmPm(Ljava/lang/String;)Z
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 504
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3f

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static isNotEmpty(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 516
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->isEmpty(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static isNotEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 512
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPlural(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 2
    .param p0, "formList"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v0, 0x1

    .line 406
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->sizeOf(Lcom/vlingo/dialog/model/IForm;)I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPlural(Ljava/lang/String;)Z
    .locals 2
    .param p0, "pipeList"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 394
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->sizeOf(Ljava/lang/String;)I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSingular(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 2
    .param p0, "formList"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v0, 0x1

    .line 189
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->sizeOf(Lcom/vlingo/dialog/model/IForm;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSingular(Ljava/lang/String;)Z
    .locals 2
    .param p0, "pipeList"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 185
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->sizeOf(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static parse(Ljava/text/DateFormat;Ljava/lang/String;)Ljava/util/Date;
    .locals 1
    .param p0, "dateFormat"    # Ljava/text/DateFormat;
    .param p1, "string"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 548
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->clone(Ljava/text/DateFormat;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method protected static putTypeNumbers(Ljava/util/Map;Ljava/lang/String;)V
    .locals 8
    .param p1, "sep"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 556
    .local p0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->TYPES:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v0, v2

    .line 557
    .local v5, "type":Ljava/lang/String;
    invoke-interface {p0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 558
    .local v3, "langType":Ljava/lang/String;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    const/16 v6, 0x9

    if-gt v1, v6, :cond_0

    .line 559
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 558
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 556
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 562
    .end local v1    # "i":I
    .end local v3    # "langType":Ljava/lang/String;
    .end local v5    # "type":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static sizeOf(Lcom/vlingo/dialog/model/IForm;)I
    .locals 1
    .param p0, "formList"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 398
    invoke-interface {p0}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public static sizeOf(Ljava/lang/String;)I
    .locals 1
    .param p0, "pipeList"    # Ljava/lang/String;

    .prologue
    .line 390
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->toArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    goto :goto_0
.end method

.method protected static splitDigitsAndRemoveHyphens(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 238
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->PHONE_NUMBER_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

    invoke-virtual {v0, p0}, Lcom/vlingo/dialog/nlg/lang/Replace;->replace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "in"    # Ljava/lang/String;
    .param p1, "chars"    # Ljava/lang/String;

    .prologue
    .line 587
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "^["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]+|["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]+$"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 588
    .local v0, "regex":Ljava/lang/String;
    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static timeAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 3
    .param p0, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 445
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->timeAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 446
    .local v1, "time":Ljava/util/Date;
    sget-object v2, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 447
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 448
    return-object v0
.end method

.method protected static timeAsDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .param p0, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 438
    :try_start_0
    sget-object v1, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->RAW_TIME_FORMAT:Ljava/text/DateFormat;

    invoke-static {v1, p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->parse(Ljava/text/DateFormat;Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 439
    :catch_0
    move-exception v0

    .line 440
    .local v0, "e":Ljava/text/ParseException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected static toArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p0, "pipeList"    # Ljava/lang/String;

    .prologue
    .line 528
    const-string/jumbo v0, "\\|"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static toArray(Ljava/util/Collection;)[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 532
    .local p0, "list":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p0, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private typeAddDisambiguation([Ljava/lang/String;)V
    .locals 10
    .param p1, "types"    # [Ljava/lang/String;

    .prologue
    .line 269
    new-instance v7, Ljava/util/TreeSet;

    invoke-direct {v7}, Ljava/util/TreeSet;-><init>()V

    .line 270
    .local v7, "seenSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/TreeSet;

    invoke-direct {v6}, Ljava/util/TreeSet;-><init>()V

    .line 271
    .local v6, "pluralSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v8, v0, v3

    .line 272
    .local v8, "type":Ljava/lang/String;
    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 273
    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 276
    .end local v8    # "type":Ljava/lang/String;
    :cond_1
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 277
    .local v5, "plural":Ljava/lang/String;
    const/4 v1, 0x0

    .line 278
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v9, p1

    if-ge v2, v9, :cond_2

    .line 279
    aget-object v8, p1, v2

    .line 280
    .restart local v8    # "type":Ljava/lang/String;
    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 281
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, p1, v2

    .line 278
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 285
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v5    # "plural":Ljava/lang/String;
    .end local v8    # "type":Ljava/lang/String;
    :cond_4
    return-void
.end method


# virtual methods
.method protected abstract alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
.end method

.method protected abstract alarmAndFormat()[Ljava/lang/String;
.end method

.method public alarmList(Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 320
    const-string/jumbo v5, "alarm_list"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 321
    .local v3, "listForm":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v4

    .line 322
    .local v4, "slots":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/model/IForm;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 323
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/model/IForm;

    invoke-virtual {p0, p1, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v5

    .line 329
    :goto_0
    return-object v5

    .line 325
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 326
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/model/IForm;

    .line 327
    .local v0, "a":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 329
    .end local v0    # "a":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->alarmAndFormat()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method protected abstract andFormat()[Ljava/lang/String;
.end method

.method protected annotateNameForOutput(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 211
    if-eqz p2, :cond_0

    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "<Contact id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "</Contact>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 214
    .end local p1    # "name":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method protected abstract appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
.end method

.method protected abstract appointmentAndFormat()[Ljava/lang/String;
.end method

.method public appointmentList(Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 8
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 306
    const-string/jumbo v5, "appointment_list"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 307
    .local v3, "listForm":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v4

    .line 308
    .local v4, "slots":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/model/IForm;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v7, :cond_0

    .line 309
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/model/IForm;

    invoke-virtual {p0, p1, v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;

    move-result-object v5

    .line 315
    :goto_0
    return-object v5

    .line 311
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 312
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/model/IForm;

    .line 313
    .local v0, "a":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p0, p1, v0, v7}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 315
    .end local v0    # "a":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->appointmentAndFormat()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public count(I)Ljava/lang/String;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 181
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public count(Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 1
    .param p1, "formList"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 177
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->sizeOf(Lcom/vlingo/dialog/model/IForm;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->count(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public count(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "pipeList"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->sizeOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->count(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract date(Ljava/lang/String;)Ljava/lang/String;
.end method

.method protected date(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;
    .locals 2
    .param p1, "rawDate"    # Ljava/lang/String;
    .param p2, "dateFormat"    # Ljava/text/DateFormat;

    .prologue
    .line 489
    :try_start_0
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->dateAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->format(Ljava/text/DateFormat;Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 491
    :goto_0
    return-object v1

    .line 490
    :catch_0
    move-exception v0

    .line 491
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->badDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public isDisplay()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivIsDisplay:Z

    return v0
.end method

.method public isSpoken()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivIsDisplay:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public name(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "nameAndId"    # Ljava/lang/String;

    .prologue
    .line 223
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->nameAddHonorific(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 224
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->isSpoken()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->getId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 226
    .local v0, "id":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->splitDigitsAndRemoveHyphens(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->annotateNameForOutput(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 229
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method protected nameAddHonorific(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 219
    return-object p1
.end method

.method public nameAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->andFormat()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public nameAndList(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "pipeList"    # Ljava/lang/String;

    .prologue
    .line 242
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->toArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 243
    .local v1, "names":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 244
    aget-object v2, v1, v0

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->name(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 246
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->nameAndFormat()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public nameOrFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->orFormat()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public nameOrList(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "pipeList"    # Ljava/lang/String;

    .prologue
    .line 250
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->toArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 251
    .local v1, "names":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 252
    aget-object v2, v1, v0

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->name(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->nameOrFormat()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected abstract orFormat()[Ljava/lang/String;
.end method

.method public postProcess(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "in"    # Ljava/lang/String;

    .prologue
    .line 162
    return-object p1
.end method

.method protected renderDays(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 1
    .param p1, "in"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 565
    .local p2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->alarmAndFormat()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->renderDays(Ljava/lang/String;Ljava/util/Map;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected renderDays(Ljava/lang/String;Ljava/util/Map;[Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "in"    # Ljava/lang/String;
    .param p3, "andFormat"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 569
    .local p2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 570
    .local v2, "out":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 571
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "out":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 572
    .restart local v2    # "out":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 573
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 574
    .local v3, "outList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->SPACE_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v4, p1}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 575
    .local v0, "day":Ljava/lang/String;
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "day":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 576
    .restart local v0    # "day":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 577
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 580
    .end local v0    # "day":Ljava/lang/String;
    :cond_1
    invoke-static {v3, p3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 583
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "outList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    return-object v2
.end method

.method public setDisplay()V
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivIsDisplay:Z

    .line 134
    return-void
.end method

.method public setSpoken()V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivIsDisplay:Z

    .line 138
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivUserName:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public smsRecipients(Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
    .locals 9
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "includeSender"    # Z

    .prologue
    .line 348
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 349
    .local v3, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 350
    const-string/jumbo v7, "sender"

    invoke-interface {p1, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 351
    .local v4, "sender":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 352
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->name(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    .end local v4    # "sender":Ljava/lang/String;
    :cond_0
    const-string/jumbo v7, "recipient"

    invoke-interface {p1, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 356
    .local v1, "recipient":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    .line 357
    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->name(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    :cond_1
    const-string/jumbo v7, "recipient_list"

    invoke-interface {p1, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 360
    .local v2, "recipientListForm":Lcom/vlingo/dialog/model/IForm;
    if-eqz v2, :cond_3

    .line 361
    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/model/IForm;

    .line 362
    .local v5, "slot":Lcom/vlingo/dialog/model/IForm;
    iget-object v7, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->NUMBER_PATTERN:Ljava/util/regex/Pattern;

    invoke-interface {v5}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 363
    invoke-interface {v5}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 364
    .local v6, "value":Ljava/lang/String;
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 365
    invoke-virtual {p0, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->name(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 370
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v5    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v6    # "value":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->nameAndFormat()[Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method protected abstract task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
.end method

.method protected abstract taskAndFormat()[Ljava/lang/String;
.end method

.method public taskList(Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 334
    const-string/jumbo v5, "task_list"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 335
    .local v3, "listForm":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v4

    .line 336
    .local v4, "slots":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/model/IForm;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 337
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/model/IForm;

    invoke-virtual {p0, p1, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v5

    .line 343
    :goto_0
    return-object v5

    .line 339
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 340
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/model/IForm;

    .line 341
    .local v0, "a":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 343
    .end local v0    # "a":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->taskAndFormat()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public abstract time(Ljava/lang/String;)Ljava/lang/String;
.end method

.method protected time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;
    .locals 2
    .param p1, "rawTime"    # Ljava/lang/String;
    .param p2, "timeFormat"    # Ljava/text/DateFormat;

    .prologue
    .line 497
    :try_start_0
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->timeAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->format(Ljava/text/DateFormat;Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 499
    :goto_0
    return-object v1

    .line 498
    :catch_0
    move-exception v0

    .line 499
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->badTime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public type(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "canonicalType"    # Ljava/lang/String;

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->typeMap()Ljava/util/Map;

    move-result-object v0

    .line 259
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 260
    .local v1, "type":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 261
    move-object v1, p1

    .line 263
    :cond_0
    return-object v1
.end method

.method public typeAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->andFormat()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public typeAndList(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "pipeList"    # Ljava/lang/String;

    .prologue
    .line 288
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->toArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 289
    .local v1, "types":[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->typeAddDisambiguation([Ljava/lang/String;)V

    .line 290
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 291
    aget-object v2, v1, v0

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->type(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 293
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->typeAndFormat()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected abstract typeMap()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public typeOrFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->orFormat()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public typeOrList(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "pipeList"    # Ljava/lang/String;

    .prologue
    .line 297
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->toArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "types":[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->typeAddDisambiguation([Ljava/lang/String;)V

    .line 299
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 300
    aget-object v2, v1, v0

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->type(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 299
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 302
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->typeOrFormat()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->formatList([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public use24HourTime(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "use24HourTime"    # Ljava/lang/Boolean;

    .prologue
    .line 149
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivUse24HourTime:Z

    .line 150
    return-void

    .line 149
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public use24HourTime()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->ivUse24HourTime:Z

    return v0
.end method
