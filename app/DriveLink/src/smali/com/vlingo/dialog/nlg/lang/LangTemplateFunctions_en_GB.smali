.class public Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;
.source "LangTemplateFunctions_en_GB.java"


# static fields
.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final LOCALE:Ljava/util/Locale;

.field private static final TIME_DISPLAY_12H_FORMAT:Ljava/text/DateFormat;

.field private static final TIME_DISPLAY_24H_FORMAT:Ljava/text/DateFormat;

.field private static final TIME_SPOKEN_12H_FORMAT:Ljava/text/DateFormat;

.field private static final TIME_SPOKEN_24H_FORMAT:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 13
    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->LOCALE:Ljava/util/Locale;

    .line 15
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEEE, d MMMM"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->DATE_FORMAT:Ljava/text/DateFormat;

    .line 17
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "h:mm a"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->TIME_DISPLAY_12H_FORMAT:Ljava/text/DateFormat;

    .line 18
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH:mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->TIME_DISPLAY_24H_FORMAT:Ljava/text/DateFormat;

    .line 19
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "h:mm a"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->TIME_SPOKEN_12H_FORMAT:Ljava/text/DateFormat;

    .line 20
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "h:mm a"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->TIME_SPOKEN_24H_FORMAT:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;-><init>()V

    return-void
.end method


# virtual methods
.method public date(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 24
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->DATE_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->date(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public time(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->isDisplay()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 31
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->use24HourTime()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->TIME_DISPLAY_24H_FORMAT:Ljava/text/DateFormat;

    .line 43
    .local v0, "format":Ljava/text/DateFormat;
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ":00 "

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 34
    .end local v0    # "format":Ljava/text/DateFormat;
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->TIME_DISPLAY_12H_FORMAT:Ljava/text/DateFormat;

    .restart local v0    # "format":Ljava/text/DateFormat;
    goto :goto_0

    .line 37
    .end local v0    # "format":Ljava/text/DateFormat;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->use24HourTime()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 38
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->TIME_SPOKEN_24H_FORMAT:Ljava/text/DateFormat;

    .restart local v0    # "format":Ljava/text/DateFormat;
    goto :goto_0

    .line 40
    .end local v0    # "format":Ljava/text/DateFormat;
    :cond_2
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_GB;->TIME_SPOKEN_12H_FORMAT:Ljava/text/DateFormat;

    .restart local v0    # "format":Ljava/text/DateFormat;
    goto :goto_0
.end method
