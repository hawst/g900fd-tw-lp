.class public Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;
.super Ljava/lang/Object;
.source "DMOutputGeneratorImpl1.java"

# interfaces
.implements Lcom/vlingo/dialog/nlg/DMOutputGenerator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;,
        Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledCondition;,
        Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;,
        Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$SequenceValue;,
        Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;,
        Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$StringValue;,
        Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    }
.end annotation


# static fields
.field private static final VARIABLE_PATTERN:Ljava/util/regex/Pattern;

.field private static final engine:Lorg/apache/commons/jexl2/JexlEngine;

.field private static logger:Lcom/vlingo/common/log4j/VLogger; = null

.field private static random:Ljava/util/Random; = null

.field private static final serialVersionUID:J = 0x3876faed9a840aa9L

.field private static final uniqueMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;",
            ">;"
        }
    .end annotation
.end field

.field private static uniqueMapCount:I


# instance fields
.field private loadVersion:Ljava/lang/String;

.field private map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 42
    const-class v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->logger:Lcom/vlingo/common/log4j/VLogger;

    .line 44
    const-string/jumbo v0, "\\$\\{([^}]+)\\}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->VARIABLE_PATTERN:Ljava/util/regex/Pattern;

    .line 46
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/JexlEngine;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    .line 48
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    const-string/jumbo v1, "ot"

    const-class v2, Lcom/vlingo/dialog/nlg/OutputTemplateFunctions;

    const-string/jumbo v3, "lang"

    const-class v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;

    invoke-static {v1, v2, v3, v4}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlEngine;->setFunctions(Ljava/util/Map;)V

    .line 54
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v0, v5}, Lorg/apache/commons/jexl2/JexlEngine;->setCache(I)V

    .line 55
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v0, v5}, Lorg/apache/commons/jexl2/JexlEngine;->setSilent(Z)V

    .line 56
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v0, v5}, Lorg/apache/commons/jexl2/JexlEngine;->setLenient(Z)V

    .line 57
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v1, "DMOutputGenerator: static initializer"

    invoke-virtual {v0, v1}, Lorg/apache/log4j/Logger;->info(Ljava/lang/Object;)V

    .line 60
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->random:Ljava/util/Random;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMap:Ljava/util/Map;

    .line 66
    sput v5, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMapCount:I

    return-void
.end method

.method private constructor <init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V
    .locals 5
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .param p2, "mainFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->map:Ljava/util/Map;

    .line 268
    if-nez p2, :cond_0

    const-string/jumbo p2, "dm-prompts.xml"

    .line 269
    :cond_0
    const-class v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;

    monitor-enter v2

    .line 270
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->map:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 271
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->getVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->loadVersion:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    .line 274
    invoke-static {}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->cleanUp()V

    .line 275
    sget-object v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Initialized DMOutputGenerator from configuration version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->loadVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 286
    :try_start_2
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V

    .line 288
    :goto_0
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 289
    return-void

    .line 276
    :catch_0
    move-exception v0

    .line 279
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "No class meta found for: OutputTemplates"

    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    iget-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->map:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 281
    sget-object v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Cannot load DMOutputGenerator from old format configuration version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->loadVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 286
    :try_start_4
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V

    goto :goto_0

    .line 288
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 283
    .restart local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 286
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v1

    :try_start_6
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method static synthetic access$000()Lorg/apache/commons/jexl2/JexlEngine;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->VARIABLE_PATTERN:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$200(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->compileString(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-static {p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->compileExpression(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()Ljava/util/Random;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->random:Ljava/util/Random;

    return-object v0
.end method

.method private buildContext(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Lorg/apache/commons/jexl2/JexlContext;
    .locals 5
    .param p1, "request"    # Lcom/vlingo/tts/model/NLGRequest;
    .param p2, "dialogMeta"    # Lcom/vlingo/message/model/request/DialogMeta;

    .prologue
    .line 337
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->getLanguageInstance(Lcom/vlingo/tts/model/NLGRequest;)Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;

    move-result-object v1

    .line 338
    .local v1, "langInstance":Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
    invoke-virtual {p2}, Lcom/vlingo/message/model/request/DialogMeta;->getUse24HourTime()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->use24HourTime(Ljava/lang/Boolean;)V

    .line 339
    new-instance v0, Lorg/apache/commons/jexl2/MapContext;

    const-string/jumbo v2, "lang"

    const-string/jumbo v3, "form"

    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getContext()Lcom/vlingo/mda/util/MDAObject;

    move-result-object v4

    invoke-static {v2, v1, v3, v4}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/apache/commons/jexl2/MapContext;-><init>(Ljava/util/Map;)V

    .line 343
    .local v0, "context":Lorg/apache/commons/jexl2/MapContext;
    return-object v0
.end method

.method private static buildResponse(Lorg/apache/commons/jexl2/JexlContext;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;)Lcom/vlingo/tts/model/NLGResponse;
    .locals 5
    .param p0, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p1, "compiledPrompt"    # Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;

    .prologue
    .line 348
    const-string/jumbo v4, "lang"

    invoke-interface {p0, v4}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;

    .line 350
    .local v1, "langInstance":Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
    invoke-virtual {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->setSpoken()V

    .line 351
    new-instance v3, Lcom/vlingo/tts/model/NLGSegment;

    invoke-direct {v3}, Lcom/vlingo/tts/model/NLGSegment;-><init>()V

    .line 352
    .local v3, "spoken":Lcom/vlingo/tts/model/NLGSegment;
    invoke-virtual {p1, p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;->generateSpoken(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->postProcess(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/dialog/nlg/NLGUtil;->separateLargeIntegersIntoWords(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/tts/model/NLGSegment;->setText(Ljava/lang/String;)V

    .line 354
    invoke-virtual {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->setDisplay()V

    .line 355
    new-instance v0, Lcom/vlingo/tts/model/NLGSegment;

    invoke-direct {v0}, Lcom/vlingo/tts/model/NLGSegment;-><init>()V

    .line 356
    .local v0, "display":Lcom/vlingo/tts/model/NLGSegment;
    invoke-virtual {p1, p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;->generateDisplay(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->postProcess(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/vlingo/tts/model/NLGSegment;->setText(Ljava/lang/String;)V

    .line 358
    new-instance v2, Lcom/vlingo/tts/model/NLGResponse;

    invoke-direct {v2}, Lcom/vlingo/tts/model/NLGResponse;-><init>()V

    .line 359
    .local v2, "response":Lcom/vlingo/tts/model/NLGResponse;
    invoke-virtual {v2, v3}, Lcom/vlingo/tts/model/NLGResponse;->setSpokenForm(Lcom/vlingo/tts/model/NLGSegment;)V

    .line 360
    invoke-virtual {v2, v0}, Lcom/vlingo/tts/model/NLGResponse;->setDisplayForm(Lcom/vlingo/tts/model/NLGSegment;)V

    .line 362
    return-object v2
.end method

.method public static cleanUp()V
    .locals 6

    .prologue
    .line 407
    const/4 v0, 0x0

    .line 408
    .local v0, "expressionCount":I
    sget-object v3, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    .line 409
    .local v2, "value":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    instance-of v3, v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;

    if-eqz v3, :cond_0

    .line 410
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 413
    .end local v2    # "value":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    :cond_1
    sget-object v3, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "DMOutputGenerator uniqueMap: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMapCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 414
    sget-object v3, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 415
    return-void
.end method

.method private static compileExpression(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    .locals 2
    .param p0, "expr"    # Ljava/lang/String;

    .prologue
    .line 398
    sget-object v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMap:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    .line 399
    .local v0, "result":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    sget v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMapCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMapCount:I

    .line 400
    if-nez v0, :cond_0

    .line 401
    sget-object v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMap:Ljava/util/Map;

    new-instance v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;

    .end local v0    # "result":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-direct {v0, p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;-><init>(Ljava/lang/String;)V

    .restart local v0    # "result":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    :cond_0
    return-object v0
.end method

.method private static compileString(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 383
    sget-object v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMap:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    .line 384
    .local v0, "result":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    sget v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMapCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMapCount:I

    .line 385
    if-nez v0, :cond_0

    .line 386
    const-string/jumbo v1, "${"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 387
    new-instance v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$SequenceValue;

    .end local v0    # "result":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-direct {v0, p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$SequenceValue;-><init>(Ljava/lang/String;)V

    .line 392
    .restart local v0    # "result":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    :goto_0
    sget-object v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->uniqueMap:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    :cond_0
    return-object v0

    .line 389
    :cond_1
    new-instance v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$StringValue;

    .end local v0    # "result":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-direct {v0, p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$StringValue;-><init>(Ljava/lang/String;)V

    .restart local v0    # "result":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    goto :goto_0
.end method

.method private getCompiledTemplate(Lcom/vlingo/tts/model/NLGRequest;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;
    .locals 12
    .param p1, "request"    # Lcom/vlingo/tts/model/NLGRequest;

    .prologue
    .line 309
    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getType()Ljava/lang/String;

    move-result-object v8

    .line 310
    .local v8, "names":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 311
    .local v5, "language":Ljava/lang/String;
    const-string/jumbo v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v6, :cond_2

    aget-object v7, v1, v3

    .line 312
    .local v7, "name":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->map:Ljava/util/Map;

    invoke-interface {v9, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 313
    .local v4, "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;>;"
    if-eqz v4, :cond_1

    .line 314
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;

    .line 315
    .local v2, "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;
    if-nez v2, :cond_0

    const-string/jumbo v9, "-"

    invoke-virtual {v5, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 316
    new-instance v9, Lcom/vlingo/common/util/VLocale;

    invoke-direct {v9, v5}, Lcom/vlingo/common/util/VLocale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/vlingo/common/util/VLocale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 317
    .local v0, "altLanguage":Ljava/lang/String;
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;
    check-cast v2, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;

    .line 319
    .end local v0    # "altLanguage":Ljava/lang/String;
    .restart local v2    # "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;
    :cond_0
    if-eqz v2, :cond_1

    .line 320
    return-object v2

    .line 311
    .end local v2    # "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 324
    .end local v4    # "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;>;"
    .end local v7    # "name":Ljava/lang/String;
    :cond_2
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "no template found for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getType()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " with language "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9
.end method

.method private getLanguageInstance(Lcom/vlingo/tts/model/NLGRequest;)Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
    .locals 5
    .param p1, "request"    # Lcom/vlingo/tts/model/NLGRequest;

    .prologue
    .line 328
    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 329
    .local v1, "language":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->createInstance(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;

    move-result-object v0

    .line 330
    .local v0, "langInstance":Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
    if-nez v0, :cond_0

    .line 331
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "no LangTemplateFunctions class for language: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 333
    :cond_0
    return-object v0
.end method

.method private initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V
    .locals 9
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .param p2, "file"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 293
    sget-object v6, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "loading "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 294
    invoke-interface {p1, p2}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->load(Ljava/lang/String;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;

    .line 295
    .local v5, "top":Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;
    invoke-virtual {v5}, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;->getImports()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/nlg/config/Import;

    .line 296
    .local v1, "i":Lcom/vlingo/dialog/nlg/config/Import;
    invoke-virtual {v1}, Lcom/vlingo/dialog/nlg/config/Import;->getFile()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v6}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    .end local v1    # "i":Lcom/vlingo/dialog/nlg/config/Import;
    :cond_0
    invoke-virtual {v5}, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;->getTemplates()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/nlg/config/Template;

    .line 299
    .local v4, "template":Lcom/vlingo/dialog/nlg/config/Template;
    new-instance v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;

    invoke-direct {v0, v4}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;-><init>(Lcom/vlingo/dialog/nlg/config/Template;)V

    .line 300
    .local v0, "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;
    iget-object v6, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->map:Ljava/util/Map;

    iget-object v7, v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;->name:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 301
    .local v3, "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;>;"
    if-nez v3, :cond_1

    .line 302
    iget-object v6, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->map:Ljava/util/Map;

    iget-object v7, v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;->name:Ljava/lang/String;

    new-instance v3, Ljava/util/TreeMap;

    .end local v3    # "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;>;"
    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    .restart local v3    # "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;>;"
    invoke-interface {v6, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    :cond_1
    iget-object v6, v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;->language:Ljava/lang/String;

    invoke-interface {v3, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 306
    .end local v0    # "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;
    .end local v3    # "langMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;>;"
    .end local v4    # "template":Lcom/vlingo/dialog/nlg/config/Template;
    :cond_2
    return-void
.end method

.method public static loadFromDirectory(Ljava/io/File;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;
    .locals 1
    .param p0, "directory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->loadFromDirectory(Ljava/io/File;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;

    move-result-object v0

    return-object v0
.end method

.method public static loadFromDirectory(Ljava/io/File;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;
    .locals 3
    .param p0, "directory"    # Ljava/io/File;
    .param p1, "mainFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    new-instance v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;

    new-instance v1, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;

    const-class v2, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {v0, v1, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    return-object v0
.end method

.method public static loadFromJar(Ljava/io/File;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;
    .locals 1
    .param p0, "jar"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->loadFromJar(Ljava/io/File;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;

    move-result-object v0

    return-object v0
.end method

.method public static loadFromJar(Ljava/io/File;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;
    .locals 3
    .param p0, "jar"    # Ljava/io/File;
    .param p1, "mainFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 257
    new-instance v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;

    new-instance v1, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;

    const-class v2, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {v0, v1, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public generate(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Lcom/vlingo/tts/model/NLGResponse;
    .locals 4
    .param p1, "request"    # Lcom/vlingo/tts/model/NLGRequest;
    .param p2, "dialogMeta"    # Lcom/vlingo/message/model/request/DialogMeta;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/dialog/nlg/DMOutputGenerator$BadIndexException;
        }
    .end annotation

    .prologue
    .line 366
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->buildContext(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Lorg/apache/commons/jexl2/JexlContext;

    move-result-object v2

    .line 367
    .local v2, "context":Lorg/apache/commons/jexl2/JexlContext;
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->getCompiledTemplate(Lcom/vlingo/tts/model/NLGRequest;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;

    move-result-object v1

    .line 368
    .local v1, "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;
    invoke-virtual {p1}, Lcom/vlingo/tts/model/NLGRequest;->getIndex()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;->generate(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/Integer;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;

    move-result-object v0

    .line 369
    .local v0, "compiledPrompt":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;
    invoke-static {v2, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->buildResponse(Lorg/apache/commons/jexl2/JexlContext;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;)Lcom/vlingo/tts/model/NLGResponse;

    move-result-object v3

    return-object v3
.end method

.method public generateAll(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Ljava/util/List;
    .locals 6
    .param p1, "request"    # Lcom/vlingo/tts/model/NLGRequest;
    .param p2, "dialogMeta"    # Lcom/vlingo/message/model/request/DialogMeta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/tts/model/NLGRequest;",
            "Lcom/vlingo/message/model/request/DialogMeta;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/tts/model/NLGResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 373
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->buildContext(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Lorg/apache/commons/jexl2/JexlContext;

    move-result-object v2

    .line 374
    .local v2, "context":Lorg/apache/commons/jexl2/JexlContext;
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->getCompiledTemplate(Lcom/vlingo/tts/model/NLGRequest;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;

    move-result-object v1

    .line 375
    .local v1, "compiledTemplate":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 376
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/tts/model/NLGResponse;>;"
    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledTemplate;->generateAll(Lorg/apache/commons/jexl2/JexlContext;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;

    .line 377
    .local v0, "compiledPrompt":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;
    invoke-static {v2, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->buildResponse(Lorg/apache/commons/jexl2/JexlContext;Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;)Lcom/vlingo/tts/model/NLGResponse;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 379
    .end local v0    # "compiledPrompt":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;
    :cond_0
    return-object v4
.end method
