.class public abstract Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
.source "LangTemplateFunctions_en.java"


# static fields
.field private static final AND_FORMAT:[Ljava/lang/String;

.field private static final AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

.field private static final OR_FORMAT:[Ljava/lang/String;

.field private static final TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final alarmDaysMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, " and "

    aput-object v1, v0, v2

    const-string/jumbo v1, ", "

    aput-object v1, v0, v3

    const-string/jumbo v1, " and "

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->AND_FORMAT:[Ljava/lang/String;

    .line 15
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, " and "

    aput-object v1, v0, v2

    const-string/jumbo v1, ", "

    aput-object v1, v0, v3

    const-string/jumbo v1, ", and "

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    .line 16
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, " or "

    aput-object v1, v0, v2

    const-string/jumbo v1, ", "

    aput-object v1, v0, v3

    const-string/jumbo v1, " or "

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->OR_FORMAT:[Ljava/lang/String;

    .line 18
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->TYPE_MAP:Ljava/util/Map;

    .line 98
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->alarmDaysMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;-><init>()V

    return-void
.end method

.method private static endsWith(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 1
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 117
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 120
    .local v4, "time":Ljava/lang/String;
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string/jumbo v5, "repeat"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 123
    .local v2, "repeat":Z
    const-string/jumbo v5, "days"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "days":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->alarmDaysMap:Ljava/util/Map;

    invoke-virtual {p0, v0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->renderDays(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    .line 127
    if-eqz v2, :cond_4

    .line 128
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 129
    const-string/jumbo v5, " every day"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_0
    :goto_0
    const-string/jumbo v5, "enabled"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 149
    .local v1, "enabled":Z
    if-nez v1, :cond_1

    .line 150
    const-string/jumbo v5, ", disabled"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 130
    .end local v1    # "enabled":Z
    :cond_2
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 131
    const-string/jumbo v5, " every weekday"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 133
    :cond_3
    const-string/jumbo v5, " every "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 137
    :cond_4
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 138
    const-string/jumbo v5, " for the next 7 days"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 139
    :cond_5
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 140
    const-string/jumbo v5, " Monday through Friday"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 142
    :cond_6
    const-string/jumbo v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method protected alarmAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected andFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointmentForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "brief"    # Z

    .prologue
    .line 50
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "title"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 53
    const-string/jumbo v5, "title"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 54
    .local v4, "title":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 55
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    :goto_0
    const-string/jumbo v5, "time"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->isEmptyOrUnspecifiedAmPm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 62
    const-string/jumbo v5, "all_day"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 63
    const-string/jumbo v5, " all day"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    :cond_1
    :goto_1
    const-string/jumbo v5, "date"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 72
    const-string/jumbo v5, "date_alt"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "dateAlt":Ljava/lang/String;
    const/16 v5, 0x20

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 74
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 75
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_2
    :goto_2
    if-nez p3, :cond_3

    const-string/jumbo v5, "location"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 83
    const-string/jumbo v5, "location"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 85
    const-string/jumbo v5, ", location "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .end local v1    # "location":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 57
    .restart local v4    # "title":Ljava/lang/String;
    :cond_4
    const-string/jumbo v5, "appointment"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 65
    .end local v4    # "title":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 66
    .local v3, "time":Ljava/lang/String;
    const-string/jumbo v5, " at "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 77
    .end local v3    # "time":Ljava/lang/String;
    .restart local v0    # "dateAlt":Ljava/lang/String;
    :cond_6
    const-string/jumbo v5, "on "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string/jumbo v5, "date"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method protected appointmentAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected orFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->OR_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 9
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "task"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/16 v8, 0x2c

    .line 163
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v7, "title"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 166
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 167
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    :cond_0
    const-string/jumbo v7, "date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "date":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 173
    const-string/jumbo v7, " due "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const-string/jumbo v7, "date_alt"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 175
    .local v1, "dateAlt":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 176
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    :goto_0
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 183
    .end local v1    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v7, "reminder_date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 184
    .local v2, "reminderDate":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 185
    const-string/jumbo v7, " reminder "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    const-string/jumbo v7, "reminder_date_alt"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 187
    .local v3, "reminderDateAlt":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 188
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    :goto_1
    const-string/jumbo v7, "reminder_timer"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 194
    .local v4, "reminderTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 195
    const-string/jumbo v7, " at "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    .end local v3    # "reminderDateAlt":Ljava/lang/String;
    .end local v4    # "reminderTime":Ljava/lang/String;
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, " ,"

    invoke-static {v7, v8}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 178
    .end local v2    # "reminderDate":Ljava/lang/String;
    .restart local v1    # "dateAlt":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 190
    .end local v1    # "dateAlt":Ljava/lang/String;
    .restart local v2    # "reminderDate":Ljava/lang/String;
    .restart local v3    # "reminderDateAlt":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected taskAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected typeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;->TYPE_MAP:Ljava/util/Map;

    return-object v0
.end method
