.class public Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;
.super Lcom/vlingo/dialog/goal/model/QueryGoal;
.source "ContactQueryGoalBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Capability:Ljava/lang/String; = "Capability"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Limit:Ljava/lang/String; = "Limit"

.field public static final PROP_Name:Ljava/lang/String; = "Name"

.field public static final PROP_Type:Ljava/lang/String; = "Type"


# instance fields
.field private Capability:Ljava/lang/String;

.field private ID:J

.field private Limit:Ljava/lang/Integer;

.field private Name:Ljava/lang/String;

.field private Type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/goal/model/QueryGoal;-><init>()V

    .line 17
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCapability()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;->Capability:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/ContactQueryGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;->ID:J

    return-wide v0
.end method

.method public getLimit()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;->Limit:Ljava/lang/Integer;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;->Type:Ljava/lang/String;

    return-object v0
.end method

.method public setCapability(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;->Capability:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;->ID:J

    .line 51
    return-void
.end method

.method public setLimit(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;->Limit:Ljava/lang/Integer;

    .line 37
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;->Name:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/ContactQueryGoalBase;->Type:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
