.class public Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;
.super Lcom/vlingo/dialog/goal/model/QueryGoal;
.source "AlarmQueryGoalBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Count:Ljava/lang/String; = "Count"

.field public static final PROP_Days:Ljava/lang/String; = "Days"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_RangeEnd:Ljava/lang/String; = "RangeEnd"

.field public static final PROP_RangeStart:Ljava/lang/String; = "RangeStart"


# instance fields
.field private Count:Ljava/lang/Integer;

.field private Days:Ljava/lang/String;

.field private ID:J

.field private RangeEnd:Ljava/lang/String;

.field private RangeStart:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/goal/model/QueryGoal;-><init>()V

    .line 17
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getCount()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;->Count:Ljava/lang/Integer;

    return-object v0
.end method

.method public getDays()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;->Days:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;->ID:J

    return-wide v0
.end method

.method public getRangeEnd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;->RangeEnd:Ljava/lang/String;

    return-object v0
.end method

.method public getRangeStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;->RangeStart:Ljava/lang/String;

    return-object v0
.end method

.method public setCount(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;->Count:Ljava/lang/Integer;

    .line 44
    return-void
.end method

.method public setDays(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;->Days:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;->ID:J

    .line 51
    return-void
.end method

.method public setRangeEnd(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;->RangeEnd:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setRangeStart(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AlarmQueryGoalBase;->RangeStart:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
