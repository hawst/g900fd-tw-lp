.class public abstract Lcom/vlingo/dialog/goal/model/QueryGoalBase;
.super Lcom/vlingo/dialog/goal/model/Goal;
.source "QueryGoalBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Asynchronous:Ljava/lang/String; = "Asynchronous"

.field public static final PROP_ClearCache:Ljava/lang/String; = "ClearCache"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_QueryKey:Ljava/lang/String; = "QueryKey"


# instance fields
.field private Asynchronous:Ljava/lang/Boolean;

.field private ClearCache:Z

.field private ID:J

.field private QueryKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/dialog/goal/model/Goal;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/dialog/goal/model/QueryGoalBase;->ClearCache:Z

    .line 15
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/QueryGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAsynchronous()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/QueryGoalBase;->Asynchronous:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/QueryGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getClearCache()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/vlingo/dialog/goal/model/QueryGoalBase;->ClearCache:Z

    return v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/vlingo/dialog/goal/model/QueryGoalBase;->ID:J

    return-wide v0
.end method

.method public getQueryKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/QueryGoalBase;->QueryKey:Ljava/lang/String;

    return-object v0
.end method

.method public setAsynchronous(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/QueryGoalBase;->Asynchronous:Ljava/lang/Boolean;

    .line 35
    return-void
.end method

.method public setClearCache(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/vlingo/dialog/goal/model/QueryGoalBase;->ClearCache:Z

    .line 28
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/vlingo/dialog/goal/model/QueryGoalBase;->ID:J

    .line 42
    return-void
.end method

.method public setQueryKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/QueryGoalBase;->QueryKey:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
