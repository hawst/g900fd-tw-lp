.class public Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;
.super Lcom/vlingo/dialog/goal/model/QueryGoal;
.source "AppointmentQueryGoalBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Count:Ljava/lang/String; = "Count"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_IncludeAllDay:Ljava/lang/String; = "IncludeAllDay"

.field public static final PROP_Invitee:Ljava/lang/String; = "Invitee"

.field public static final PROP_Location:Ljava/lang/String; = "Location"

.field public static final PROP_MatchType:Ljava/lang/String; = "MatchType"

.field public static final PROP_RangeEnd:Ljava/lang/String; = "RangeEnd"

.field public static final PROP_RangeStart:Ljava/lang/String; = "RangeStart"

.field public static final PROP_Title:Ljava/lang/String; = "Title"


# instance fields
.field private Count:Ljava/lang/Integer;

.field private ID:J

.field private IncludeAllDay:Z

.field private Invitee:Ljava/lang/String;

.field private Location:Ljava/lang/String;

.field private MatchType:Ljava/lang/String;

.field private RangeEnd:Ljava/lang/String;

.field private RangeStart:Ljava/lang/String;

.field private Title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/vlingo/dialog/goal/model/QueryGoal;-><init>()V

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->IncludeAllDay:Z

    .line 25
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 91
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 95
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getCount()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->Count:Ljava/lang/Integer;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->ID:J

    return-wide v0
.end method

.method public getIncludeAllDay()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->IncludeAllDay:Z

    return v0
.end method

.method public getInvitee()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->Invitee:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->Location:Ljava/lang/String;

    return-object v0
.end method

.method public getMatchType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->MatchType:Ljava/lang/String;

    return-object v0
.end method

.method public getRangeEnd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->RangeEnd:Ljava/lang/String;

    return-object v0
.end method

.method public getRangeStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->RangeStart:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->Title:Ljava/lang/String;

    return-object v0
.end method

.method public setCount(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->Count:Ljava/lang/Integer;

    .line 66
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 86
    iput-wide p1, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->ID:J

    .line 87
    return-void
.end method

.method public setIncludeAllDay(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->IncludeAllDay:Z

    .line 80
    return-void
.end method

.method public setInvitee(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->Invitee:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->Location:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setMatchType(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->MatchType:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setRangeEnd(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->RangeEnd:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setRangeStart(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->RangeStart:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;->Title:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
