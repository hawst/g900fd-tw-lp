.class public final Lcom/vlingo/core/facade/CoreManager;
.super Ljava/lang/Object;
.source "CoreManager.java"


# static fields
.field private static final EXPANDED_SW_VERSION_DISAMBIGUATOR:Ljava/lang/String; = "13"

.field private static final EXPANDED_SW_VERSION_SEPARATOR:Ljava/lang/String; = "APP"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method public static applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;
    .locals 1

    .prologue
    .line 143
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    return-object v0
.end method

.method public static audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 212
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    return-object v0
.end method

.method public static destroyCore()V
    .locals 0

    .prologue
    .line 119
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->destroy()V

    .line 120
    return-void
.end method

.method public static dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;
    .locals 1

    .prologue
    .line 127
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    return-object v0
.end method

.method public static endpointManager()Lcom/vlingo/core/facade/endpoints/IEndpointManager;
    .locals 1

    .prologue
    .line 169
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v0

    return-object v0
.end method

.method public static expandSoftwareVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "appVersion"    # Ljava/lang/String;

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "13."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getCoreVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "APP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCoreVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    const-string/jumbo v0, "2.2.0.11867"

    return-object v0
.end method

.method public static getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;
    .locals 1

    .prologue
    .line 216
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->getInstance()Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    move-result-object v0

    return-object v0
.end method

.method public static initCore(Landroid/content/Context;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Lcom/vlingo/core/facade/IClientInfo;Ljava/util/Map;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;
    .param p2, "clientInfo"    # Lcom/vlingo/core/facade/IClientInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;",
            "Lcom/vlingo/core/facade/IClientInfo;",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p3, "fieldIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    const/4 v8, 0x0

    .line 60
    .local v8, "recoMode":Lcom/vlingo/core/facade/RecognitionMode;
    if-nez p2, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "IClientInfo cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    invoke-interface {p2}, Lcom/vlingo/core/facade/IClientInfo;->getRecognitionMode()Lcom/vlingo/core/facade/RecognitionMode;

    move-result-object v8

    .line 64
    if-nez v8, :cond_1

    .line 67
    sget-object v8, Lcom/vlingo/core/facade/RecognitionMode;->CLOUD:Lcom/vlingo/core/facade/RecognitionMode;

    .line 70
    :cond_1
    invoke-interface {p2}, Lcom/vlingo/core/facade/IClientInfo;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    invoke-interface {p2}, Lcom/vlingo/core/facade/IClientInfo;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v2

    invoke-interface {p2}, Lcom/vlingo/core/facade/IClientInfo;->getAppId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, Lcom/vlingo/core/facade/IClientInfo;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2}, Lcom/vlingo/core/facade/IClientInfo;->getAppVersion()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->expandSoftwareVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2}, Lcom/vlingo/core/facade/IClientInfo;->getSalesCode()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p2}, Lcom/vlingo/core/facade/IClientInfo;->getServerDetails()Lcom/vlingo/core/internal/util/CoreServerInfo;

    move-result-object v7

    invoke-interface {p2}, Lcom/vlingo/core/facade/IClientInfo;->isUseEmbeddedDialogManager()Z

    move-result v11

    move-object v0, p0

    move-object v9, p1

    move-object v10, p3

    invoke-static/range {v0 .. v11}, Lcom/vlingo/core/internal/VlingoAndroidCore;->init(Landroid/content/Context;Lcom/vlingo/core/internal/ResourceIdProvider;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/CoreServerInfo;Lcom/vlingo/core/facade/RecognitionMode;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/util/Map;Z)V

    .line 74
    return-void
.end method

.method public static initCore(Landroid/content/Context;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/lang/String;Lcom/vlingo/core/facade/IClientInfo;Ljava/util/Map;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;
    .param p2, "applicationContentProviderBase"    # Ljava/lang/String;
    .param p3, "clientInfo"    # Lcom/vlingo/core/facade/IClientInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/facade/IClientInfo;",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p4, "fieldIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    const/4 v8, 0x0

    .line 87
    .local v8, "recoMode":Lcom/vlingo/core/facade/RecognitionMode;
    if-nez p3, :cond_0

    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "IClientInfo cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    invoke-interface/range {p3 .. p3}, Lcom/vlingo/core/facade/IClientInfo;->getRecognitionMode()Lcom/vlingo/core/facade/RecognitionMode;

    move-result-object v8

    .line 96
    invoke-interface/range {p3 .. p3}, Lcom/vlingo/core/facade/IClientInfo;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    invoke-interface/range {p3 .. p3}, Lcom/vlingo/core/facade/IClientInfo;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v2

    invoke-interface/range {p3 .. p3}, Lcom/vlingo/core/facade/IClientInfo;->getAppId()Ljava/lang/String;

    move-result-object v3

    invoke-interface/range {p3 .. p3}, Lcom/vlingo/core/facade/IClientInfo;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-interface/range {p3 .. p3}, Lcom/vlingo/core/facade/IClientInfo;->getAppVersion()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->expandSoftwareVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface/range {p3 .. p3}, Lcom/vlingo/core/facade/IClientInfo;->getSalesCode()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {p3 .. p3}, Lcom/vlingo/core/facade/IClientInfo;->getServerDetails()Lcom/vlingo/core/internal/util/CoreServerInfo;

    move-result-object v7

    invoke-interface/range {p3 .. p3}, Lcom/vlingo/core/facade/IClientInfo;->isUseEmbeddedDialogManager()Z

    move-result v12

    move-object v0, p0

    move-object v9, p1

    move-object v10, p2

    move-object/from16 v11, p4

    invoke-static/range {v0 .. v12}, Lcom/vlingo/core/internal/VlingoAndroidCore;->init(Landroid/content/Context;Lcom/vlingo/core/internal/ResourceIdProvider;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/CoreServerInfo;Lcom/vlingo/core/facade/RecognitionMode;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 100
    return-void
.end method

.method public static logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/vlingo/core/facade/logging/ILogger;"
        }
    .end annotation

    .prologue
    .line 226
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static logger(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/core/facade/logging/ILogger;
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/vlingo/core/facade/logging/ILogger;"
        }
    .end annotation

    .prologue
    .line 248
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static logger(Ljava/lang/Class;Ljava/lang/String;Z)Lcom/vlingo/core/facade/logging/ILogger;
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/vlingo/core/facade/logging/ILogger;"
        }
    .end annotation

    .prologue
    .line 260
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p0, p1, p2}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;Ljava/lang/String;Z)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static logger(Ljava/lang/Class;Z)Lcom/vlingo/core/facade/logging/ILogger;
    .locals 1
    .param p1, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;Z)",
            "Lcom/vlingo/core/facade/logging/ILogger;"
        }
    .end annotation

    .prologue
    .line 237
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;Z)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;
    .locals 1

    .prologue
    .line 135
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    return-object v0
.end method

.method public static queryManager(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentProviderBase"    # Ljava/lang/String;

    .prologue
    .line 153
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManager;

    move-result-object v0

    return-object v0
.end method

.method public static resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;
    .locals 1

    .prologue
    .line 161
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    return-object v0
.end method

.method public static termsManager()Lcom/vlingo/core/facade/terms/ITermsManager;
    .locals 1

    .prologue
    .line 186
    invoke-static {}, Lcom/vlingo/core/internal/terms/TermsManager;->getInstance()Lcom/vlingo/core/internal/terms/TermsManager;

    move-result-object v0

    return-object v0
.end method

.method public static ttsEngine(Landroid/content/Context;)Lcom/vlingo/core/facade/tts/ITTSEngine;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 178
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/TTSEngine;

    move-result-object v0

    return-object v0
.end method

.method public static userLoggingEngine()Lcom/vlingo/core/facade/logging/IUserLoggingEngine;
    .locals 1

    .prologue
    .line 203
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    return-object v0
.end method
