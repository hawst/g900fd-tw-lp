.class public final Lcom/vlingo/core/facade/CoreRegistrar;
.super Ljava/lang/Object;
.source "CoreRegistrar.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 1
    .param p0, "dmActionType"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .prologue
    .line 68
    invoke-static {p0}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    return-object v0
.end method

.method public static getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 1
    .param p0, "actionType"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .param p3, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 76
    invoke-static {p0, p1, p2, p3}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    return-object v0
.end method

.method public static getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 1
    .param p0, "dmActionType"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/vlingo/core/internal/dialogmanager/DMAction;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/DMActionType;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "classType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    return-object v0
.end method

.method public static getAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;
    .locals 1
    .param p0, "key"    # Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/CoreAdapter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    invoke-static {p0}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public static registerAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V
    .locals 0
    .param p0, "key"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/DMActionType;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/DMAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "action":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/DMAction;>;"
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 60
    return-void
.end method

.method public static registerAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V
    .locals 0
    .param p0, "key"    # Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/CoreAdapter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "adapter":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 86
    return-void
.end method

.method public static registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V
    .locals 0
    .param p0, "key"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "handler":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;>;"
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 36
    return-void
.end method

.method public static setUpStandardActionMappings()V
    .locals 0

    .prologue
    .line 50
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->setupStandardMappings()V

    .line 51
    return-void
.end method

.method public static unregisterAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)V
    .locals 0
    .param p0, "key"    # Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .prologue
    .line 93
    invoke-static {p0}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->unregisterHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)V

    .line 94
    return-void
.end method

.method public static unregisterHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;)V
    .locals 0
    .param p0, "key"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .prologue
    .line 43
    invoke-static {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->unregisterHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;)V

    .line 44
    return-void
.end method
