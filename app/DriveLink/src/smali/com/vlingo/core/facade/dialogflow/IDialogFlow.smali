.class public interface abstract Lcom/vlingo/core/facade/dialogflow/IDialogFlow;
.super Ljava/lang/Object;
.source "IDialogFlow.java"

# interfaces
.implements Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;


# virtual methods
.method public abstract addUserProperties(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract addWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract cancelAudio()V
.end method

.method public abstract cancelDialog()V
.end method

.method public abstract cancelTurn()V
.end method

.method public abstract cancelTurn(Z)V
.end method

.method public abstract clearPendingSafeReaderTurn()V
.end method

.method public abstract endpointReco()V
.end method

.method public abstract finishDialog()V
.end method

.method public abstract getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
.end method

.method public abstract getPreservedState()Ljava/lang/Object;
.end method

.method public abstract getTaskRegulators(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getWidgetSpecificProperties(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract initFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;",
            ")V"
        }
    .end annotation
.end method

.method public abstract isAboutToStartUserFlowWithMic()Z
.end method

.method public abstract isIdle()Z
.end method

.method public abstract isQueuedAudioTask()Z
.end method

.method public abstract manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
.end method

.method public abstract playMedia(I)V
.end method

.method public abstract queuePauseableTask(Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;)V
.end method

.method public abstract registerTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V
.end method

.method public abstract releaseFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;)V
.end method

.method public abstract removeUserProperties(Ljava/lang/String;)V
.end method

.method public abstract removeWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V
.end method

.method public abstract sendPendingEvents()V
.end method

.method public abstract setUserProperties(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setWakeUpContext(Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;)V
.end method

.method public abstract startReplyFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V
.end method

.method public abstract startSafeReaderFlow()V
.end method

.method public abstract startSafeReaderFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V
.end method

.method public abstract startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z
.end method

.method public abstract startUserFlow(Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;Ljava/lang/String;)Z
.end method

.method public abstract startUserFlow(Ljava/lang/String;Ljava/lang/Object;)Z
.end method

.method public abstract stealFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;",
            ")V"
        }
    .end annotation
.end method

.method public abstract tts(Ljava/lang/String;)V
.end method

.method public abstract tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
.end method

.method public abstract unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V
.end method
