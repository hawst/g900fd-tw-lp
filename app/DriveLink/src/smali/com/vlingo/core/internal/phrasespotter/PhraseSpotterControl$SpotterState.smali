.class interface abstract Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
.super Ljava/lang/Object;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "SpotterState"
.end annotation


# virtual methods
.method public abstract is(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Z
.end method

.method public abstract onEnter()V
.end method

.method public abstract onExit()V
.end method

.method public abstract start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
.end method

.method public abstract stop()V
.end method
