.class Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;
.super Ljava/lang/Object;
.source "CMAWeatherLookupHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->showSuccess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 33

    .prologue
    .line 91
    const/16 v25, 0x0

    .line 92
    .local v25, "msg":Ljava/lang/String;
    const/16 v30, 0x0

    .line 93
    .local v30, "spokenMsg":Ljava/lang/String;
    const/4 v4, 0x0

    .line 94
    .local v4, "dateString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getDate()Ljava/lang/String;

    move-result-object v20

    .line 95
    .local v20, "dateForecast":Ljava/lang/String;
    const/16 v24, 0x0

    .line 96
    .local v24, "isToday":Z
    const/16 v23, 0x0

    .line 98
    .local v23, "isDatePlusSeven":Z
    invoke-static/range {v20 .. v20}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 100
    :try_start_0
    new-instance v28, Ljava/text/SimpleDateFormat;

    sget-object v2, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_DATE_FORMAT:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v28

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 101
    .local v28, "sdf":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v19

    .line 103
    .local v19, "date":Ljava/util/Date;
    invoke-virtual/range {v19 .. v19}, Ljava/util/Date;->getDate()I

    move-result v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getDate()I

    move-result v3

    if-ne v2, v3, :cond_1

    const/16 v24, 0x1

    .line 104
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v2

    move/from16 v0, v24

    invoke-virtual {v2, v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->setToday(Z)V

    .line 106
    new-instance v26, Ljava/util/Date;

    invoke-direct/range {v26 .. v26}, Ljava/util/Date;-><init>()V

    .line 107
    .local v26, "newDate":Ljava/util/Date;
    new-instance v18, Ljava/util/Date;

    invoke-direct/range {v18 .. v18}, Ljava/util/Date;-><init>()V

    .line 108
    .local v18, "currentDate":Ljava/util/Date;
    new-instance v17, Ljava/util/GregorianCalendar;

    invoke-direct/range {v17 .. v17}, Ljava/util/GregorianCalendar;-><init>()V

    .line 109
    .local v17, "calendar":Ljava/util/GregorianCalendar;
    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 110
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-result-object v3

    iget-object v3, v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Ljava/util/GregorianCalendar;->add(II)V

    .line 111
    invoke-virtual/range {v17 .. v17}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Ljava/util/Date;->setTime(J)V

    .line 112
    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez v24, :cond_2

    :cond_0
    const/16 v23, 0x1

    .line 113
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v2

    move/from16 v0, v23

    invoke-virtual {v2, v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->setDatePlusSeven(Z)V

    .line 114
    if-eqz v23, :cond_3

    .line 115
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_plus_seven:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$100(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v25

    .line 142
    .end local v17    # "calendar":Ljava/util/GregorianCalendar;
    .end local v18    # "currentDate":Ljava/util/Date;
    .end local v19    # "date":Ljava/util/Date;
    .end local v26    # "newDate":Ljava/util/Date;
    .end local v28    # "sdf":Ljava/text/SimpleDateFormat;
    :goto_2
    invoke-static/range {v30 .. v30}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 143
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$1000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 148
    :goto_3
    invoke-static/range {v20 .. v20}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$1200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCMAWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    invoke-interface {v2, v3, v5, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 153
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->actionSuccess()V

    .line 154
    return-void

    .line 103
    .restart local v19    # "date":Ljava/util/Date;
    .restart local v28    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_1
    const/16 v24, 0x0

    goto/16 :goto_0

    .line 112
    .restart local v17    # "calendar":Ljava/util/GregorianCalendar;
    .restart local v18    # "currentDate":Ljava/util/Date;
    .restart local v26    # "newDate":Ljava/util/Date;
    :cond_2
    const/16 v23, 0x0

    goto :goto_1

    .line 117
    :cond_3
    if-eqz v24, :cond_4

    .line 118
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$200(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 123
    :goto_5
    if-eqz v24, :cond_5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    .line 124
    .local v6, "dateType":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
    :goto_6
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_with_locatin:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationState()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v5

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$300(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 125
    .local v7, "generalMsg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->daysBetween(Ljava/util/Date;Ljava/util/Date;)I
    invoke-static {v2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;Ljava/util/Date;Ljava/util/Date;)I

    move-result v22

    .line 126
    .local v22, "indexDay":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getCurrentTempForToday()Ljava/lang/String;

    move-result-object v14

    .line 127
    .local v14, "currentTemp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v2

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getMaxTempByDate(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v13

    .line 128
    .local v13, "maxTemp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v2

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getWeatherCodeByDate(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v32

    .line 129
    .local v32, "weatherCode":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    const/4 v3, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual/range {v2 .. v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getOneDaySpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 130
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationState()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move-object v10, v4

    move-object/from16 v16, v6

    invoke-virtual/range {v8 .. v16}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 120
    .end local v6    # "dateType":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
    .end local v7    # "generalMsg":Ljava/lang/String;
    .end local v13    # "maxTemp":Ljava/lang/String;
    .end local v14    # "currentTemp":Ljava/lang/String;
    .end local v22    # "indexDay":I
    .end local v32    # "weatherCode":Ljava/lang/Integer;
    :cond_4
    new-instance v29, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "EEEE"

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 121
    .local v29, "sdfWeekday":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    .line 123
    .end local v29    # "sdfWeekday":Ljava/text/SimpleDateFormat;
    :cond_5
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->OTHER:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_6

    .line 132
    .end local v17    # "calendar":Ljava/util/GregorianCalendar;
    .end local v18    # "currentDate":Ljava/util/Date;
    .end local v19    # "date":Ljava/util/Date;
    .end local v26    # "newDate":Ljava/util/Date;
    .end local v28    # "sdf":Ljava/text/SimpleDateFormat;
    :catch_0
    move-exception v21

    .line 133
    .local v21, "e":Ljava/lang/Exception;
    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "DateParseError "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 136
    .end local v21    # "e":Ljava/lang/Exception;
    :cond_6
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$600(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    .line 137
    .local v31, "todayStr":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_with_locatin:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationState()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v5

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 138
    .restart local v7    # "generalMsg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    move-object/from16 v0, v31

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getOneDaySpokenMsgInternal(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;
    invoke-static {v2, v3, v0, v5, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;

    move-result-object v27

    .line 139
    .local v27, "oneDaySpokenMsg":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v27, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v5, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$900(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_7
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    :cond_7
    const-string/jumbo v2, ""

    goto :goto_7

    .line 145
    .end local v7    # "generalMsg":Ljava/lang/String;
    .end local v27    # "oneDaySpokenMsg":Ljava/lang/String;
    .end local v31    # "todayStr":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$1100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-interface {v2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 151
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$1300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCMAWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    invoke-static {v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    invoke-interface {v2, v3, v5, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_4
.end method
