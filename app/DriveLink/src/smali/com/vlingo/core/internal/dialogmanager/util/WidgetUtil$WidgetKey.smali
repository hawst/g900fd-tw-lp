.class public final enum Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
.super Ljava/lang/Enum;
.source "WidgetUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WidgetKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum AddTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum AddressBook:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum AnswerQuestion:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ComposeEmail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ComposeSocialStatus:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ContactDetail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum DeleteAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum DeleteEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum DeleteTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum EditAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum EditEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum EditTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum Map:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum Memo:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum MemoList:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum MessageReadbackBodyHidden:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum MultipleMessageDisplay:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum MultipleMessageShowWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum Notification:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum OpenApp:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum PlayMusic:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ScheduleEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum SetAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowAlarmChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowButton:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowCMAWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowCMAWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowCallWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowChatbotWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowClock:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowContactChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowContactLookup:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowContactTypeChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowEventChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowLocalSearchWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowNavWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowNaverWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowTaskChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowTrueKnowledgeWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowWCISWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowWolframWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum ShowWorldTime:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum SocialNetworkChoice:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum UnsupportedSuggestion:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field public static final enum WebBrowser:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "AddressBook"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddressBook:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 12
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "AddTask"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 13
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "AnswerQuestion"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AnswerQuestion:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ComposeEmail"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeEmail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 15
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ComposeMessage"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 16
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ComposeSocialStatus"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeSocialStatus:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 17
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ContactDetail"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ContactDetail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 18
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "DeleteAlarm"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 19
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "DeleteEvent"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 20
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "DeleteTask"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 21
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "DriveNewsWidget"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 22
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "EditAlarm"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 23
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "EditEvent"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 24
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "EditTask"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 25
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "Map"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Map:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 26
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "Memo"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Memo:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "MemoList"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MemoList:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 28
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "MultipleMessageDisplay"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageDisplay:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 29
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "MultipleMessageShowWidget"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageShowWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 30
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "MultipleMessageReadoutWidget"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 31
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "MultipleSenderMessageReadoutWidget"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 32
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "MusicPlayingWidget"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 33
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "MessageReadback"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 34
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "MessageReadbackBodyHidden"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadbackBodyHidden:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 35
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "Notification"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Notification:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 36
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "OpenApp"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->OpenApp:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 37
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "PlayMusic"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->PlayMusic:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ScheduleEvent"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ScheduleEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 39
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "SetAlarm"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 40
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "SetTimer"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 41
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowAlarmChoices"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowAlarmChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 42
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowButton"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowButton:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 43
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowCallWidget"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCallWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 44
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowChatbotWidget"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowChatbotWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 45
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowClock"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowClock:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 46
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowContactChoices"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 47
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowContactLookup"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactLookup:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 48
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowContactTypeChoices"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactTypeChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 49
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowEvent"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 50
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowEventChoices"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEventChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 51
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowLocalSearchWidget"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowLocalSearchWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 52
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowNavWidget"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowNavWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 53
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowTaskChoices"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowTaskChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 54
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowWCISWidget"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWCISWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 55
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowNaverWidget"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowNaverWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 56
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowWeatherWidget"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 57
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowWeatherDailyWidget"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 58
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowCMAWeatherWidget"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCMAWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 59
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowCMAWeatherDailyWidget"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCMAWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 60
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowWolframWidget"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWolframWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 61
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowWorldTime"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWorldTime:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 62
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "ShowTrueKnowledgeWidget"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowTrueKnowledgeWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 63
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "SocialNetworkChoice"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SocialNetworkChoice:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 64
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "UnsupportedSuggestion"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->UnsupportedSuggestion:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 65
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v1, "WebBrowser"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->WebBrowser:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 10
    const/16 v0, 0x37

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddressBook:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AnswerQuestion:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeEmail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeSocialStatus:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ContactDetail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Map:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Memo:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MemoList:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageDisplay:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageShowWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadbackBodyHidden:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Notification:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->OpenApp:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->PlayMusic:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ScheduleEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowAlarmChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowButton:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCallWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowChatbotWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowClock:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactLookup:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactTypeChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEventChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowLocalSearchWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowNavWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowTaskChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWCISWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowNaverWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCMAWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCMAWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWolframWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWorldTime:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowTrueKnowledgeWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SocialNetworkChoice:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->UnsupportedSuggestion:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->WebBrowser:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    return-object v0
.end method
