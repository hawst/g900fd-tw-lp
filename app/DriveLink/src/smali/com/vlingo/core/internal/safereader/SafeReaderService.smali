.class public Lcom/vlingo/core/internal/safereader/SafeReaderService;
.super Landroid/app/Service;
.source "SafeReaderService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;
    }
.end annotation


# static fields
.field public static final ACTION_SAFEREADER_DEINIT:Ljava/lang/String; = "com.vlingo.client.safereader.ACTION_SAFEREADER_DEINIT"

.field public static final ACTION_SAFEREADER_OFF:Ljava/lang/String; = "com.vlingo.client.safereader.ACTION_SAFEREADER_OFF"

.field public static final ACTION_SAFEREADER_ON:Ljava/lang/String; = "com.vlingo.client.safereader.ACTION_SAFEREADER_ON"

.field public static final ACTION_SAFEREADER_PAUSE:Ljava/lang/String; = "com.vlingo.client.safereader.ACTION_SAFEREADER_PAUSE"

.field public static final ACTION_SAFEREADER_RESUME:Ljava/lang/String; = "com.vlingo.client.safereader.ACTION_SAFEREADER_RESUME"

.field public static final ACTION_SKIP_CURRENT_ITEM:Ljava/lang/String; = "com.vlingo.client.safereader.ACTION_SKIP_CURRENT_ITEM"

.field public static final ACTION_UPDATE_STATUS:Ljava/lang/String; = "com.vlingo.client.safereader.ACTION_UPDATE_STATUS"

.field public static final ACTION_UPDATE_WIDGETS:Ljava/lang/String; = "com.vlingo.client.safereader.ACTION_UPDATE_WIDGETS"


# instance fields
.field private NumClients:I

.field private final intentReceiver:Landroid/content/BroadcastReceiver;

.field private final mBinder:Landroid/os/IBinder;

.field private safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->NumClients:I

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/safereader/SafeReaderService$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService$1;-><init>(Lcom/vlingo/core/internal/safereader/SafeReaderService;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->intentReceiver:Landroid/content/BroadcastReceiver;

    .line 52
    new-instance v0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;-><init>(Lcom/vlingo/core/internal/safereader/SafeReaderService;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->mBinder:Landroid/os/IBinder;

    .line 53
    return-void
.end method

.method private stopServiceIfPossible()V
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->NumClients:I

    if-gtz v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->isSafeReaderOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->stopSelf()V

    .line 63
    :cond_0
    return-void
.end method


# virtual methods
.method public broadcastStatusUpdate()V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_0

    .line 319
    :goto_0
    return-void

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->broadcastStatusUpdate()V

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 126
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 130
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->startSafeReading()V

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 133
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->stopSafeReading()V

    goto :goto_0

    .line 135
    :cond_2
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_DEINIT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 136
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderDeinit()V

    goto :goto_0

    .line 138
    :cond_3
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SKIP_CURRENT_ITEM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 139
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->skipCurrentlyPlayingItem()V

    goto :goto_0

    .line 141
    :cond_4
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_UPDATE_STATUS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 142
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->broadcastStatusUpdate()V

    goto :goto_0

    .line 144
    :cond_5
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_PAUSE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 145
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->pause()V

    goto :goto_0

    .line 147
    :cond_6
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_RESUME"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 148
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->resume()V

    goto :goto_0

    .line 150
    :cond_7
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_UPDATE_WIDGETS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method public isSafeReaderOn()Z
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_0

    .line 174
    const/4 v0, 0x0

    .line 178
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->isSafeReaderOn()Z

    move-result v0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 95
    iget v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->NumClients:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->NumClients:I

    .line 96
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 70
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 71
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 72
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 73
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_DEINIT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SKIP_CURRENT_ITEM"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 75
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_UPDATE_STATUS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 76
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_PAUSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 77
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_RESUME"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->intentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 80
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->intentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 87
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->stopSafeReading()V

    .line 88
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 89
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 103
    iget v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->NumClients:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->NumClients:I

    .line 104
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 119
    if-eqz p1, :cond_0

    .line 120
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->handleIntent(Landroid/content/Context;Landroid/content/Intent;)V

    .line 122
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 110
    iget v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->NumClients:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->NumClients:I

    .line 111
    invoke-direct {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->stopServiceIfPossible()V

    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->isSafeReaderOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->pause()V

    goto :goto_0
.end method

.method public registerListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 286
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->registerAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    goto :goto_0
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->isSafeReaderOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->resume()V

    goto :goto_0
.end method

.method public safeReaderDeinit()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_0

    .line 236
    :goto_0
    return-void

    .line 233
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->stopSafeReading()V

    .line 234
    invoke-direct {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->stopServiceIfPossible()V

    .line 235
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->safeReaderDeinit()V

    goto :goto_0
.end method

.method public safeReaderInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V
    .locals 2
    .param p1, "engine"    # Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    .prologue
    .line 201
    if-eqz p1, :cond_0

    .line 205
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-eqz v0, :cond_2

    .line 206
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    .line 216
    :cond_2
    iput-object p1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    .line 217
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->safeReaderInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V

    goto :goto_0
.end method

.method public skipCurrentlyPlayingItem()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->isSafeReaderOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->skipCurrentlyPlayingItem()V

    goto :goto_0
.end method

.method public startSafeReading()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->isSafeReaderOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->startSafeReading()V

    .line 191
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->doesSupportNotifications()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->getNotificationId()I

    move-result v0

    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v1}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->getNotification()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public stopSafeReading()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 244
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->isSafeReaderOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->stopSafeReading()V

    .line 248
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->doesSupportNotifications()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->stopForeground(Z)V

    goto :goto_0
.end method

.method public unregisterListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_0

    .line 307
    :goto_0
    return-void

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderEngine:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->unregisterAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    goto :goto_0
.end method
