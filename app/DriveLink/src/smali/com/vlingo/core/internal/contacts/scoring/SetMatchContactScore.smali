.class public Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;
.super Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
.source "SetMatchContactScore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$1;,
        Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;
    }
.end annotation


# static fields
.field private static final SPLIT_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private final backOff:I

.field private cachedValues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final score:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string/jumbo v0, "\\s+|\\.|,|-"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->SPLIT_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "score"    # I
    .param p2, "backOff"    # I

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/scoring/ContactScore;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->cachedValues:Ljava/util/Map;

    .line 62
    iput p1, p0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->score:I

    .line 63
    iput p2, p0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->backOff:I

    .line 64
    return-void
.end method

.method private getScore(Ljava/lang/String;Ljava/lang/String;)I
    .locals 30
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "match"    # Ljava/lang/String;

    .prologue
    .line 87
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v29, "++"

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 88
    .local v17, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->cachedValues:Ljava/util/Map;

    move-object/from16 v0, v17

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    .line 89
    .local v14, "cachedValue":Ljava/lang/Integer;
    if-eqz v14, :cond_0

    .line 90
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v27

    .line 182
    :goto_0
    return v27

    .line 92
    :cond_0
    new-instance v23, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;

    const/4 v5, 0x0

    move-object/from16 v0, v23

    invoke-direct {v0, v5}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;-><init>(Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$1;)V

    .line 93
    .local v23, "normalizer":Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;
    sget-object v5, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->SPLIT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v26

    .line 94
    .local v26, "rawQueryWords":[Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->SPLIT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v25

    .line 96
    .local v25, "rawMatchWords":[Ljava/lang/String;
    new-instance v22, Ljava/util/HashMap;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashMap;-><init>()V

    .line 97
    .local v22, "normalQwMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v12, v26

    .local v12, "arr$":[Ljava/lang/String;
    array-length v0, v12

    move/from16 v18, v0

    .local v18, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_1
    move/from16 v0, v18

    if-ge v15, v0, :cond_1

    aget-object v8, v12, v15

    .line 98
    .local v8, "qw":Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 99
    .local v10, "nqw":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-interface {v0, v8, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 102
    .end local v8    # "qw":Ljava/lang/String;
    .end local v10    # "nqw":Ljava/lang/String;
    :cond_1
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    .line 103
    .local v21, "normalMwMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v12, v25

    array-length v0, v12

    move/from16 v18, v0

    const/4 v15, 0x0

    :goto_2
    move/from16 v0, v18

    if-ge v15, v0, :cond_2

    aget-object v9, v12, v15

    .line 104
    .local v9, "mw":Ljava/lang/String;
    invoke-static {v9}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 105
    .local v11, "nmw":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-interface {v0, v9, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 108
    .end local v9    # "mw":Ljava/lang/String;
    .end local v11    # "nmw":Ljava/lang/String;
    :cond_2
    const/16 v20, 0x0

    .line 109
    .local v20, "matches":I
    const/16 v28, 0x0

    .line 111
    .local v28, "scoreWeight":I
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 112
    .local v6, "queryMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 114
    .local v7, "matchMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v12, v26

    array-length v0, v12

    move/from16 v18, v0

    const/4 v15, 0x0

    move/from16 v16, v15

    .end local v12    # "arr$":[Ljava/lang/String;
    .end local v15    # "i$":I
    .end local v18    # "len$":I
    .local v16, "i$":I
    :goto_3
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_5

    aget-object v8, v12, v16

    .line 115
    .restart local v8    # "qw":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 116
    .restart local v10    # "nqw":Ljava/lang/String;
    move-object/from16 v13, v25

    .local v13, "arr$":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v19, v0

    .local v19, "len$":I
    const/4 v15, 0x0

    .end local v16    # "i$":I
    .restart local v15    # "i$":I
    :goto_4
    move/from16 v0, v19

    if-ge v15, v0, :cond_4

    aget-object v9, v13, v15

    .line 117
    .restart local v9    # "mw":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .restart local v11    # "nmw":Ljava/lang/String;
    move-object/from16 v5, p0

    .line 118
    invoke-virtual/range {v5 .. v11}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->firstPassSuits(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 119
    add-int/lit8 v20, v20, 0x1

    .line 120
    add-int/lit8 v28, v28, 0x64

    .line 121
    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 122
    invoke-interface {v7, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 114
    .end local v9    # "mw":Ljava/lang/String;
    .end local v11    # "nmw":Ljava/lang/String;
    :cond_4
    add-int/lit8 v15, v16, 0x1

    move/from16 v16, v15

    .end local v15    # "i$":I
    .restart local v16    # "i$":I
    goto :goto_3

    .line 128
    .end local v8    # "qw":Ljava/lang/String;
    .end local v10    # "nqw":Ljava/lang/String;
    .end local v13    # "arr$":[Ljava/lang/String;
    .end local v19    # "len$":I
    :cond_5
    move-object/from16 v12, v26

    .restart local v12    # "arr$":[Ljava/lang/String;
    array-length v0, v12

    move/from16 v18, v0

    .restart local v18    # "len$":I
    const/4 v15, 0x0

    .end local v16    # "i$":I
    .restart local v15    # "i$":I
    move/from16 v16, v15

    .end local v12    # "arr$":[Ljava/lang/String;
    .end local v15    # "i$":I
    .end local v18    # "len$":I
    .restart local v16    # "i$":I
    :goto_5
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_8

    aget-object v8, v12, v16

    .line 129
    .restart local v8    # "qw":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 130
    .restart local v10    # "nqw":Ljava/lang/String;
    move-object/from16 v13, v25

    .restart local v13    # "arr$":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v19, v0

    .restart local v19    # "len$":I
    const/4 v15, 0x0

    .end local v16    # "i$":I
    .restart local v15    # "i$":I
    :goto_6
    move/from16 v0, v19

    if-ge v15, v0, :cond_7

    aget-object v9, v13, v15

    .line 131
    .restart local v9    # "mw":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .restart local v11    # "nmw":Ljava/lang/String;
    move-object/from16 v5, p0

    .line 132
    invoke-virtual/range {v5 .. v11}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->secondPassSuits(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 133
    add-int/lit8 v20, v20, 0x1

    .line 134
    add-int/lit8 v28, v28, 0x5a

    .line 135
    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-interface {v7, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_6
    add-int/lit8 v15, v15, 0x1

    goto :goto_6

    .line 128
    .end local v9    # "mw":Ljava/lang/String;
    .end local v11    # "nmw":Ljava/lang/String;
    :cond_7
    add-int/lit8 v15, v16, 0x1

    move/from16 v16, v15

    .end local v15    # "i$":I
    .restart local v16    # "i$":I
    goto :goto_5

    .line 142
    .end local v8    # "qw":Ljava/lang/String;
    .end local v10    # "nqw":Ljava/lang/String;
    .end local v13    # "arr$":[Ljava/lang/String;
    .end local v19    # "len$":I
    :cond_8
    move-object/from16 v12, v26

    .restart local v12    # "arr$":[Ljava/lang/String;
    array-length v0, v12

    move/from16 v18, v0

    .restart local v18    # "len$":I
    const/4 v15, 0x0

    .end local v16    # "i$":I
    .restart local v15    # "i$":I
    move/from16 v16, v15

    .end local v12    # "arr$":[Ljava/lang/String;
    .end local v15    # "i$":I
    .end local v18    # "len$":I
    .restart local v16    # "i$":I
    :goto_7
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_b

    aget-object v8, v12, v16

    .line 143
    .restart local v8    # "qw":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 144
    .restart local v10    # "nqw":Ljava/lang/String;
    move-object/from16 v13, v25

    .restart local v13    # "arr$":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v19, v0

    .restart local v19    # "len$":I
    const/4 v15, 0x0

    .end local v16    # "i$":I
    .restart local v15    # "i$":I
    :goto_8
    move/from16 v0, v19

    if-ge v15, v0, :cond_a

    aget-object v9, v13, v15

    .line 145
    .restart local v9    # "mw":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .restart local v11    # "nmw":Ljava/lang/String;
    move-object/from16 v5, p0

    .line 146
    invoke-virtual/range {v5 .. v11}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->thirdPassSuits(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 147
    add-int/lit8 v20, v20, 0x1

    .line 148
    add-int/lit8 v28, v28, 0x28

    .line 149
    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 150
    invoke-interface {v7, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 144
    :cond_9
    add-int/lit8 v15, v15, 0x1

    goto :goto_8

    .line 142
    .end local v9    # "mw":Ljava/lang/String;
    .end local v11    # "nmw":Ljava/lang/String;
    :cond_a
    add-int/lit8 v15, v16, 0x1

    move/from16 v16, v15

    .end local v15    # "i$":I
    .restart local v16    # "i$":I
    goto :goto_7

    .line 156
    .end local v8    # "qw":Ljava/lang/String;
    .end local v10    # "nqw":Ljava/lang/String;
    .end local v13    # "arr$":[Ljava/lang/String;
    .end local v19    # "len$":I
    :cond_b
    move-object/from16 v12, v26

    .restart local v12    # "arr$":[Ljava/lang/String;
    array-length v0, v12

    move/from16 v18, v0

    .restart local v18    # "len$":I
    const/4 v15, 0x0

    .end local v16    # "i$":I
    .restart local v15    # "i$":I
    move/from16 v16, v15

    .end local v12    # "arr$":[Ljava/lang/String;
    .end local v15    # "i$":I
    .end local v18    # "len$":I
    .restart local v16    # "i$":I
    :goto_9
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_e

    aget-object v8, v12, v16

    .line 157
    .restart local v8    # "qw":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 158
    .restart local v10    # "nqw":Ljava/lang/String;
    move-object/from16 v13, v25

    .restart local v13    # "arr$":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v19, v0

    .restart local v19    # "len$":I
    const/4 v15, 0x0

    .end local v16    # "i$":I
    .restart local v15    # "i$":I
    :goto_a
    move/from16 v0, v19

    if-ge v15, v0, :cond_d

    aget-object v9, v13, v15

    .line 159
    .restart local v9    # "mw":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .restart local v11    # "nmw":Ljava/lang/String;
    move-object/from16 v5, p0

    .line 160
    invoke-virtual/range {v5 .. v11}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->fourthPathSuits(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 161
    add-int/lit8 v20, v20, 0x1

    .line 162
    add-int/lit8 v28, v28, 0x1e

    .line 163
    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 164
    invoke-interface {v7, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 158
    :cond_c
    add-int/lit8 v15, v15, 0x1

    goto :goto_a

    .line 156
    .end local v9    # "mw":Ljava/lang/String;
    .end local v11    # "nmw":Ljava/lang/String;
    :cond_d
    add-int/lit8 v15, v16, 0x1

    move/from16 v16, v15

    .end local v15    # "i$":I
    .restart local v16    # "i$":I
    goto :goto_9

    .line 169
    .end local v8    # "qw":Ljava/lang/String;
    .end local v10    # "nqw":Ljava/lang/String;
    .end local v13    # "arr$":[Ljava/lang/String;
    .end local v19    # "len$":I
    :cond_e
    if-nez v20, :cond_f

    .line 170
    const/16 v27, 0x0

    goto/16 :goto_0

    .line 175
    :cond_f
    move/from16 v0, v28

    int-to-float v5, v0

    const/high16 v29, 0x42c80000    # 100.0f

    div-float v24, v5, v29

    .line 176
    .local v24, "partialMatches":F
    const/high16 v5, 0x3f800000    # 1.0f

    cmpg-float v5, v24, v5

    if-gez v5, :cond_10

    .line 177
    mul-float v24, v24, v24

    .line 180
    :cond_10
    move-object/from16 v0, p0

    iget v5, v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->score:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v24

    move-object/from16 v3, v26

    move-object/from16 v4, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->getScoreDown(IF[Ljava/lang/String;[Ljava/lang/String;)I

    move-result v29

    sub-int v27, v5, v29

    .line 181
    .local v27, "retVal":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->cachedValues:Ljava/util/Map;

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    move-object/from16 v0, v17

    move-object/from16 v1, v29

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method


# virtual methods
.method protected firstPassSuits(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p3, "qw"    # Ljava/lang/String;
    .param p4, "mw"    # Ljava/lang/String;
    .param p5, "nqw"    # Ljava/lang/String;
    .param p6, "nmw"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 218
    .local p1, "queryMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p2, "matchMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p2, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p5, p6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p6}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p5}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected fourthPathSuits(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p3, "qw"    # Ljava/lang/String;
    .param p4, "mw"    # Ljava/lang/String;
    .param p5, "nqw"    # Ljava/lang/String;
    .param p6, "nmw"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 200
    .local p1, "queryMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p2, "matchMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p4, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBackOff()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->backOff:I

    return v0
.end method

.method public getScore()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->score:I

    return v0
.end method

.method public getScore(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "match"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 72
    iget-object v5, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-direct {p0, p1, v5}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->getScore(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 74
    .local v4, "result":I
    invoke-virtual {p2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getExtraNames()Ljava/util/List;

    move-result-object v1

    .line 75
    .local v1, "extraNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_1

    .line 76
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 77
    .local v0, "extraName":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->getScore(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 78
    .local v2, "extraScore":I
    if-le v2, v4, :cond_0

    .line 79
    move v4, v2

    goto :goto_0

    .line 83
    .end local v0    # "extraName":Ljava/lang/String;
    .end local v2    # "extraScore":I
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    return v4
.end method

.method protected getScoreDown(IF[Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1, "matches"    # I
    .param p2, "partialMatches"    # F
    .param p3, "rawQueryWords"    # [Ljava/lang/String;
    .param p4, "rawMatchWords"    # [Ljava/lang/String;

    .prologue
    .line 186
    array-length v1, p4

    if-ne p1, v1, :cond_1

    array-length v1, p3

    if-ne p1, v1, :cond_1

    int-to-float v1, p1

    cmpl-float v1, v1, p2

    if-nez v1, :cond_1

    .line 187
    const/4 v0, 0x0

    .line 194
    :cond_0
    :goto_0
    return v0

    .line 189
    :cond_1
    array-length v1, p3

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    sub-float/2addr v1, p2

    iget v2, p0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->backOff:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 191
    .local v0, "scoreDown":I
    array-length v1, p3

    if-ne v1, p1, :cond_0

    .line 192
    add-int/lit8 v0, v0, -0xa

    goto :goto_0
.end method

.method protected secondPassSuits(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p3, "qw"    # Ljava/lang/String;
    .param p4, "mw"    # Ljava/lang/String;
    .param p5, "nqw"    # Ljava/lang/String;
    .param p6, "nmw"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "queryMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p2, "matchMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x1

    .line 212
    invoke-interface {p1, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p2, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p6, p5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected thirdPassSuits(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p3, "qw"    # Ljava/lang/String;
    .param p4, "mw"    # Ljava/lang/String;
    .param p5, "nqw"    # Ljava/lang/String;
    .param p6, "nmw"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 206
    .local p1, "queryMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p2, "matchMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p2, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p4, p3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p6, p5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
