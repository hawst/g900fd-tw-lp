.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "CMAWeatherLookupHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
.implements Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

.field handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    .line 48
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->handler:Landroid/os/Handler;

    .line 50
    new-instance v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-direct {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    .line 51
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->handler:Landroid/os/Handler;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;Ljava/util/Date;Ljava/util/Date;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;
    .param p1, "x1"    # Ljava/util/Date;
    .param p2, "x2"    # Ljava/util/Date;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->daysBetween(Ljava/util/Date;Ljava/util/Date;)I

    move-result v0

    return v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;
    .param p1, "x1"    # Ljava/lang/Integer;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/Integer;
    .param p4, "x4"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getOneDaySpokenMsgInternal(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private daysBetween(Ljava/util/Date;Ljava/util/Date;)I
    .locals 6
    .param p1, "startDate"    # Ljava/util/Date;
    .param p2, "endDate"    # Ljava/util/Date;

    .prologue
    .line 182
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 183
    .local v0, "calendar1":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p1}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 184
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 185
    .local v1, "calendar2":Ljava/util/GregorianCalendar;
    invoke-virtual {v1, p2}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 186
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Calendar;

    .line 187
    .local v2, "date":Ljava/util/Calendar;
    const/4 v3, 0x0

    .line 188
    .local v3, "daysBetween":I
    :goto_0
    invoke-virtual {v2, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 189
    const/4 v4, 0x5

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 190
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 192
    :cond_0
    return v3
.end method

.method private getOneDaySpokenMsgInternal(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;
    .locals 12
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "day"    # Ljava/lang/String;
    .param p3, "indexDay"    # Ljava/lang/Integer;
    .param p4, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 172
    const/4 v10, 0x0

    .line 173
    .local v10, "spokenMsg":Ljava/lang/String;
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getCurrentTempForToday()Ljava/lang/String;

    move-result-object v7

    .line 174
    .local v7, "currentTemp":Ljava/lang/String;
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v0, p3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getMaxTempByDate(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v6

    .line 175
    .local v6, "maxTemp":Ljava/lang/String;
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v0, p3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getWeatherCodeByDate(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v11

    .line 176
    .local v11, "weatherCode":Ljava/lang/Integer;
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v0, p3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getWeatherPhenomenonByDate(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v2

    .line 177
    .local v2, "weatherPhenomenon":Ljava/lang/String;
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationState()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v8

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object/from16 v9, p4

    invoke-virtual/range {v0 .. v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getSpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;

    move-result-object v10

    .line 178
    return-object v10
.end method

.method private showFailure()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$2;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$2;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 204
    return-void
.end method

.method private showSuccess()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v7, 0x0

    .line 60
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 62
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v5

    const-string/jumbo v6, "weather"

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 64
    const-string/jumbo v5, "Location"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "location":Ljava/lang/String;
    const-string/jumbo v5, "Date"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "dateStr":Ljava/lang/String;
    const-string/jumbo v5, "SpokenDate"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 67
    .local v3, "spokenDataStr":Ljava/lang/String;
    const-string/jumbo v5, "time"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 68
    .local v4, "time":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 69
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "yyyy-MM-dd"

    invoke-direct {v2, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 70
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_0
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v5, v0, v3, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const/4 v5, 0x1

    return v5
.end method

.method protected getMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;
    .locals 3
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "date"    # Ljava/lang/String;
    .param p3, "city"    # Ljava/lang/String;
    .param p4, "state"    # Ljava/lang/String;
    .param p5, "maxTemp"    # Ljava/lang/String;
    .param p6, "currentTemp"    # Ljava/lang/String;
    .param p7, "weatherCode"    # I
    .param p8, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    .prologue
    .line 163
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_date_display:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getOneDaySpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "day"    # Ljava/lang/String;
    .param p3, "indexDay"    # Ljava/lang/Integer;
    .param p4, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
    .param p5, "generalMsg"    # Ljava/lang/String;

    .prologue
    .line 167
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getOneDaySpokenMsgInternal(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;
    .locals 3
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "weatherPhenomenon"    # Ljava/lang/String;
    .param p3, "date"    # Ljava/lang/String;
    .param p4, "city"    # Ljava/lang/String;
    .param p5, "state"    # Ljava/lang/String;
    .param p6, "maxTemp"    # Ljava/lang/String;
    .param p7, "currentTemp"    # Ljava/lang/String;
    .param p8, "weatherCode"    # I
    .param p9, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    .prologue
    .line 159
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_current:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p6, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.NoData"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "prompt":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 85
    .end local v0    # "prompt":Ljava/lang/String;
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRequestFailed()V
    .locals 2

    .prologue
    .line 228
    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->showFailure()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 232
    return-void

    .line 230
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v0
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 2
    .param p1, "prompt"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$3;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$3;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 242
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 243
    return-void
.end method

.method public onRequestScheduled()V
    .locals 0

    .prologue
    .line 221
    return-void
.end method

.method public onResponseReceived()V
    .locals 2

    .prologue
    .line 211
    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->showSuccess()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 215
    return-void

    .line 213
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v0
.end method
