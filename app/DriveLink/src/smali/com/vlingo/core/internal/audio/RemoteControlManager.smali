.class public final Lcom/vlingo/core/internal/audio/RemoteControlManager;
.super Ljava/lang/Object;
.source "RemoteControlManager.java"


# static fields
.field public static final PLAYSTATE_PLAYING:I = 0x1

.field public static final PLAYSTATE_STOPPED:I = 0x2

.field private static instance:Lcom/vlingo/core/internal/audio/RemoteControlManager;


# instance fields
.field audioManager:Landroid/media/AudioManager;

.field private eventReceiver:Landroid/content/ComponentName;

.field private mediaPendingEvent:Landroid/app/PendingIntent;

.field private remoteControlClient:Landroid/media/RemoteControlClient;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static close()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->instance:Lcom/vlingo/core/internal/audio/RemoteControlManager;

    .line 75
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/audio/RemoteControlManager;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->instance:Lcom/vlingo/core/internal/audio/RemoteControlManager;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/vlingo/core/internal/audio/RemoteControlManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/audio/RemoteControlManager;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->instance:Lcom/vlingo/core/internal/audio/RemoteControlManager;

    .line 68
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->instance:Lcom/vlingo/core/internal/audio/RemoteControlManager;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->init()V

    .line 70
    sget-object v0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->instance:Lcom/vlingo/core/internal/audio/RemoteControlManager;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 88
    const-string/jumbo v0, "VAC_AVRCP"

    const-string/jumbo v1, "[LatencyCheck] destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->unregister()V

    .line 91
    invoke-static {}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->close()V

    .line 92
    return-void
.end method

.method public init()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 35
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 36
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBVRASupportDevice()Z

    move-result v2

    if-nez v2, :cond_1

    .line 37
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    if-nez v2, :cond_0

    .line 38
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->audioManager:Landroid/media/AudioManager;

    .line 39
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/vlingo/core/internal/audio/MusicIntentReceiver;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->eventReceiver:Landroid/content/ComponentName;

    .line 40
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 41
    .local v1, "mediaButtonIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->eventReceiver:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 42
    invoke-static {v0, v5, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->mediaPendingEvent:Landroid/app/PendingIntent;

    .line 44
    new-instance v2, Landroid/media/RemoteControlClient;

    iget-object v3, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->mediaPendingEvent:Landroid/app/PendingIntent;

    invoke-direct {v2, v3}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v2, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    .line 45
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    const/16 v3, 0x3c

    invoke-virtual {v2, v3}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 51
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->audioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->eventReceiver:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 52
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->audioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 53
    const-string/jumbo v2, "VAC_AVRCP"

    const-string/jumbo v3, "[LatencyCheck] AVRCP params are set in constructor"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    .end local v1    # "mediaButtonIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 55
    :cond_0
    const-string/jumbo v2, "VAC_AVRCP"

    const-string/jumbo v3, "[LatencyCheck] AVRCP params were already set in constructor"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 58
    :cond_1
    const-string/jumbo v2, "VAC_AVRCP"

    const-string/jumbo v3, "[LatencyCheck] BVRA support, do not handle AVRCP & unregister"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->unregister()V

    goto :goto_0
.end method

.method public setPlaybackState(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 95
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBVRASupportDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 101
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_0

    .line 102
    const-string/jumbo v0, "VAC_AVRCP"

    const-string/jumbo v1, "[LatencyCheck] setPlaybackState - playing"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    goto :goto_0

    .line 107
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_0

    .line 108
    const-string/jumbo v0, "VAC_AVRCP"

    const-string/jumbo v1, "[LatencyCheck] setPlaybackState - paused"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 110
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->unregister()V

    goto :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public unregister()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 80
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->eventReceiver:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 81
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/RemoteControlManager;->remoteControlClient:Landroid/media/RemoteControlClient;

    .line 83
    const-string/jumbo v0, "VAC_AVRCP"

    const-string/jumbo v1, "[LatencyCheck] unregister"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_0
    return-void
.end method
