.class public final enum Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
.super Ljava/lang/Enum;
.source "LMTTManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmtt/LMTTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LmttUpdateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

.field public static final enum LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

.field public static final enum LMTT_LANGUAGE_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

.field public static final enum LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;


# instance fields
.field private mTypeString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 57
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    const-string/jumbo v1, "LMTT_MUSIC_UPDATE"

    const-string/jumbo v2, "music"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    .line 58
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    const-string/jumbo v1, "LMTT_CONTACT_UPDATE"

    const-string/jumbo v2, "pim"

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    .line 59
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    const-string/jumbo v1, "LMTT_LANGUAGE_UPDATE"

    const-string/jumbo v2, "language"

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_LANGUAGE_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    .line 56
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_LANGUAGE_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->$VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "typeString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput-object p3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->mTypeString:Ljava/lang/String;

    .line 65
    return-void
.end method

.method static fromTypeString(Ljava/lang/String;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .locals 5
    .param p0, "typeString"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->values()[Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 73
    .local v3, "type":Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    iget-object v4, v3, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->mTypeString:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 77
    .end local v3    # "type":Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    :goto_1
    return-object v3

    .line 72
    .restart local v3    # "type":Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    .end local v3    # "type":Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->$VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    return-object v0
.end method


# virtual methods
.method getTypeString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->mTypeString:Ljava/lang/String;

    return-object v0
.end method
