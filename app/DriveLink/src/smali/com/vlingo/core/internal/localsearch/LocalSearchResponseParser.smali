.class Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;
.super Ljava/lang/Object;
.source "LocalSearchResponseParser.java"

# interfaces
.implements Lcom/vlingo/core/internal/localsearch/LocalSearchParser;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private addFieldToReview(Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;Lorg/w3c/dom/Node;)V
    .locals 3
    .param p1, "rev"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    .param p2, "n"    # Lorg/w3c/dom/Node;

    .prologue
    .line 220
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v0

    .line 221
    .local v0, "key":Ljava/lang/String;
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 223
    const-string/jumbo v2, "Author"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 224
    iput-object v1, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->author:Ljava/lang/String;

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    const-string/jumbo v2, "Body"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 226
    iput-object v1, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->body:Ljava/lang/String;

    goto :goto_0

    .line 227
    :cond_2
    const-string/jumbo v2, "Date"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 228
    iput-object v1, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->date:Ljava/lang/String;

    goto :goto_0

    .line 229
    :cond_3
    const-string/jumbo v2, "Id"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 230
    iput-object v1, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->id:Ljava/lang/String;

    goto :goto_0

    .line 231
    :cond_4
    const-string/jumbo v2, "Rating"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 232
    iput-object v1, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->rating:Ljava/lang/String;

    goto :goto_0

    .line 233
    :cond_5
    const-string/jumbo v2, "Title"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 234
    iput-object v1, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->title:Ljava/lang/String;

    goto :goto_0
.end method

.method private addNodeToBusinessItem(Lorg/w3c/dom/Node;Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;)V
    .locals 2
    .param p1, "n"    # Lorg/w3c/dom/Node;
    .param p2, "bi"    # Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;

    .prologue
    .line 186
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "key":Ljava/lang/String;
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p2, v0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    return-void
.end method

.method private getAttributeValue(Lorg/w3c/dom/NamedNodeMap;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "attrMap"    # Lorg/w3c/dom/NamedNodeMap;
    .param p2, "attrName"    # Ljava/lang/String;

    .prologue
    .line 194
    invoke-interface {p1, p2}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 195
    .local v0, "node":Lorg/w3c/dom/Node;
    if-eqz v0, :cond_0

    .line 196
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    .line 198
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private parseDetailsNodeList(Lorg/w3c/dom/NodeList;Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;)V
    .locals 7
    .param p1, "detailNodeList"    # Lorg/w3c/dom/NodeList;
    .param p2, "bi"    # Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;

    .prologue
    .line 169
    if-eqz p1, :cond_1

    .line 170
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 171
    .local v4, "numDetails":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-ge v2, v4, :cond_1

    .line 172
    invoke-interface {p1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 173
    .local v1, "di":Lorg/w3c/dom/Node;
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v0

    .line 174
    .local v0, "attr":Lorg/w3c/dom/NamedNodeMap;
    const-string/jumbo v6, "Name"

    invoke-interface {v0, v6}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 175
    .local v3, "n":Lorg/w3c/dom/Node;
    if-eqz v3, :cond_0

    .line 176
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v5

    .line 177
    .local v5, "value":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 178
    invoke-virtual {p2, v5}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->markValueAsAvailable(Ljava/lang/String;)V

    .line 171
    .end local v5    # "value":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 183
    .end local v0    # "attr":Lorg/w3c/dom/NamedNodeMap;
    .end local v1    # "di":Lorg/w3c/dom/Node;
    .end local v2    # "j":I
    .end local v3    # "n":Lorg/w3c/dom/Node;
    .end local v4    # "numDetails":I
    :cond_1
    return-void
.end method

.method private parseReview(Lorg/w3c/dom/Node;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    .locals 8
    .param p1, "reviewNode"    # Lorg/w3c/dom/Node;

    .prologue
    .line 202
    new-instance v5, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;

    invoke-direct {v5}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;-><init>()V

    .line 203
    .local v5, "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v6

    .line 204
    .local v6, "revAttr":Lorg/w3c/dom/NamedNodeMap;
    invoke-interface {v6}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v3

    .line 205
    .local v3, "numAttributes":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 206
    invoke-interface {v6, v0}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-direct {p0, v5, v7}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->addFieldToReview(Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;Lorg/w3c/dom/Node;)V

    .line 205
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208
    :cond_0
    invoke-interface {p1}, Lorg/w3c/dom/Node;->hasChildNodes()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 209
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 210
    .local v2, "nl":Lorg/w3c/dom/NodeList;
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 211
    .local v4, "numNodes":I
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_1

    .line 212
    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 213
    .local v1, "n":Lorg/w3c/dom/Node;
    invoke-direct {p0, v5, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->addFieldToReview(Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;Lorg/w3c/dom/Node;)V

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 216
    .end local v1    # "n":Lorg/w3c/dom/Node;
    .end local v2    # "nl":Lorg/w3c/dom/NodeList;
    .end local v4    # "numNodes":I
    :cond_1
    return-object v5
.end method


# virtual methods
.method public parseLocalSearchResponse(Lcom/vlingo/sdk/internal/http/HttpResponse;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)Landroid/util/Pair;
    .locals 42
    .param p1, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;
    .param p2, "existingBusinessItem"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/http/HttpResponse;",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v31, Ljava/util/Vector;

    invoke-direct/range {v31 .. v31}, Ljava/util/Vector;-><init>()V

    .line 34
    .local v31, "results":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;"
    new-instance v29, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-direct/range {v29 .. v29}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;-><init>()V

    .line 39
    .local v29, "requestInfo":Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v10

    .line 40
    .local v10, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v10}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v38

    .line 42
    .local v38, "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getBusinessItemCache()Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;

    move-result-object v16

    .line 44
    .local v16, "listingCache":Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;
    new-instance v12, Ljava/io/ByteArrayInputStream;

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsBytes()[B

    move-result-object v39

    move-object/from16 v0, v39

    invoke-direct {v12, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 46
    .local v12, "inputStream":Ljava/io/ByteArrayInputStream;
    const/4 v6, 0x0

    .line 47
    .local v6, "doc":Lorg/w3c/dom/Document;
    move-object/from16 v0, v38

    invoke-virtual {v0, v12}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v6

    .line 49
    invoke-interface {v6}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v36

    .line 52
    .local v36, "root":Lorg/w3c/dom/Element;
    const-string/jumbo v39, "Error"

    move-object/from16 v0, v36

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 53
    .local v7, "errorNodes":Lorg/w3c/dom/NodeList;
    if-eqz v7, :cond_1

    .line 54
    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v39

    if-lez v39, :cond_0

    .line 56
    sget-object v39, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->TAG:Ljava/lang/String;

    const-string/jumbo v40, "Server error"

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/16 v39, 0x0

    .line 164
    .end local v6    # "doc":Lorg/w3c/dom/Document;
    .end local v7    # "errorNodes":Lorg/w3c/dom/NodeList;
    .end local v10    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v12    # "inputStream":Ljava/io/ByteArrayInputStream;
    .end local v16    # "listingCache":Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;
    .end local v36    # "root":Lorg/w3c/dom/Element;
    .end local v38    # "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    :goto_0
    return-object v39

    .line 60
    .restart local v6    # "doc":Lorg/w3c/dom/Document;
    .restart local v7    # "errorNodes":Lorg/w3c/dom/NodeList;
    .restart local v10    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v12    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v16    # "listingCache":Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;
    .restart local v36    # "root":Lorg/w3c/dom/Element;
    .restart local v38    # "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    :cond_0
    new-instance v37, Lcom/vlingo/core/internal/util/ServerErrorUtil;

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/util/ServerErrorUtil;-><init>(Lorg/w3c/dom/Element;)V

    .line 61
    .local v37, "serverErrorUtil":Lcom/vlingo/core/internal/util/ServerErrorUtil;
    invoke-virtual/range {v37 .. v37}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->hasServerError()Z

    move-result v39

    if-eqz v39, :cond_1

    .line 62
    sget-object v39, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "Server error Code=\'"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v37 .. v37}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->getCode()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string/jumbo v41, "\'"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-virtual/range {v37 .. v37}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->logIt()V

    .line 64
    const/16 v39, 0x0

    goto :goto_0

    .line 69
    .end local v37    # "serverErrorUtil":Lcom/vlingo/core/internal/util/ServerErrorUtil;
    :cond_1
    const-string/jumbo v39, "RequestInfo"

    move-object/from16 v0, v36

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v30

    .line 70
    .local v30, "requestInfoNodes":Lorg/w3c/dom/NodeList;
    if-eqz v30, :cond_2

    invoke-interface/range {v30 .. v30}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v39

    if-lez v39, :cond_2

    .line 71
    const/16 v39, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v39

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v34

    .line 72
    .local v34, "rin":Lorg/w3c/dom/Node;
    if-eqz v34, :cond_2

    .line 73
    invoke-interface/range {v34 .. v34}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v39

    const-string/jumbo v40, "Query"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->getAttributeValue(Lorg/w3c/dom/NamedNodeMap;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v29

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->setQuery(Ljava/lang/String;)V

    .line 74
    invoke-interface/range {v34 .. v34}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v39

    if-eqz v39, :cond_2

    invoke-interface/range {v34 .. v34}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v39

    if-eqz v39, :cond_2

    .line 75
    invoke-interface/range {v34 .. v34}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v39

    const-string/jumbo v40, "City"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->getAttributeValue(Lorg/w3c/dom/NamedNodeMap;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v29

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->setCity(Ljava/lang/String;)V

    .line 76
    invoke-interface/range {v34 .. v34}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v39

    const-string/jumbo v40, "State"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->getAttributeValue(Lorg/w3c/dom/NamedNodeMap;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v29

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->setState(Ljava/lang/String;)V

    .line 77
    invoke-interface/range {v34 .. v34}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v39

    const-string/jumbo v40, "SelectedLocationType"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->getAttributeValue(Lorg/w3c/dom/NamedNodeMap;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v29

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->setSelectedLocationType(Ljava/lang/String;)V

    .line 84
    .end local v34    # "rin":Lorg/w3c/dom/Node;
    :cond_2
    const-string/jumbo v39, "LocalListings"

    move-object/from16 v0, v36

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v20

    .line 86
    .local v20, "localListingNodes":Lorg/w3c/dom/NodeList;
    if-eqz v20, :cond_3

    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v39

    if-gtz v39, :cond_4

    .line 87
    :cond_3
    new-instance v39, Landroid/util/Pair;

    move-object/from16 v0, v39

    move-object/from16 v1, v29

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 161
    .end local v6    # "doc":Lorg/w3c/dom/Document;
    .end local v7    # "errorNodes":Lorg/w3c/dom/NodeList;
    .end local v10    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v12    # "inputStream":Ljava/io/ByteArrayInputStream;
    .end local v16    # "listingCache":Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;
    .end local v20    # "localListingNodes":Lorg/w3c/dom/NodeList;
    .end local v30    # "requestInfoNodes":Lorg/w3c/dom/NodeList;
    .end local v36    # "root":Lorg/w3c/dom/Element;
    .end local v38    # "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    :catch_0
    move-exception v8

    .line 162
    .local v8, "ex":Ljava/lang/Exception;
    sget-object v39, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "Exception parsing local search response: "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    const/16 v39, 0x0

    goto/16 :goto_0

    .line 90
    .end local v8    # "ex":Ljava/lang/Exception;
    .restart local v6    # "doc":Lorg/w3c/dom/Document;
    .restart local v7    # "errorNodes":Lorg/w3c/dom/NodeList;
    .restart local v10    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v12    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v16    # "listingCache":Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;
    .restart local v20    # "localListingNodes":Lorg/w3c/dom/NodeList;
    .restart local v30    # "requestInfoNodes":Lorg/w3c/dom/NodeList;
    .restart local v36    # "root":Lorg/w3c/dom/Element;
    .restart local v38    # "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    :cond_4
    const/16 v39, 0x0

    :try_start_1
    move-object/from16 v0, v20

    move/from16 v1, v39

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v21

    .line 91
    .local v21, "lsn":Lorg/w3c/dom/Node;
    invoke-interface/range {v21 .. v21}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v19

    .line 94
    .local v19, "localListingElements":Lorg/w3c/dom/NodeList;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-interface/range {v19 .. v19}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v39

    move/from16 v0, v39

    if-ge v11, v0, :cond_e

    .line 97
    move-object/from16 v0, v19

    invoke-interface {v0, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v18

    .line 98
    .local v18, "listingNode":Lorg/w3c/dom/Node;
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v15

    .line 100
    .local v15, "listingAttributes":Lorg/w3c/dom/NamedNodeMap;
    const-string/jumbo v39, "ListingID"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v15, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->getAttributeValue(Lorg/w3c/dom/NamedNodeMap;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 101
    .local v17, "listingId":Ljava/lang/String;
    const/4 v3, 0x0

    .line 103
    .local v3, "alreadyReportedOrganic":Z
    const/4 v4, 0x0

    .line 106
    .local v4, "bi":Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;
    if-eqz p2, :cond_5

    .line 107
    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getListingID()Ljava/lang/String;

    move-result-object v9

    .line 108
    .local v9, "existingListingID":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_5

    .line 109
    move-object/from16 v0, p2

    check-cast v0, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;

    move-object v4, v0

    .line 110
    invoke-virtual {v4}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->isOrganic()Z

    move-result v39

    if-eqz v39, :cond_5

    .line 111
    const/4 v3, 0x1

    .line 116
    .end local v9    # "existingListingID":Ljava/lang/String;
    :cond_5
    if-nez v4, :cond_6

    .line 117
    new-instance v4, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;

    .end local v4    # "bi":Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;
    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;-><init>(Ljava/lang/String;)V

    .line 121
    .restart local v4    # "bi":Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;
    :cond_6
    invoke-interface {v15}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v26

    .line 122
    .local v26, "numAttributes":I
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_2
    move/from16 v0, v26

    if-ge v13, v0, :cond_9

    .line 123
    invoke-interface {v15, v13}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v23

    .line 125
    .local v23, "node":Lorg/w3c/dom/Node;
    if-eqz v3, :cond_7

    const-string/jumbo v39, "Type"

    invoke-interface/range {v23 .. v23}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_8

    .line 126
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->addNodeToBusinessItem(Lorg/w3c/dom/Node;Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;)V

    .line 122
    :cond_8
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 131
    .end local v23    # "node":Lorg/w3c/dom/Node;
    :cond_9
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v25

    .line 132
    .local v25, "nodes":Lorg/w3c/dom/NodeList;
    const/4 v5, 0x0

    .line 134
    .local v5, "detailNodeList":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v25 .. v25}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v27

    .line 135
    .local v27, "numNodes":I
    const/4 v13, 0x0

    :goto_3
    move/from16 v0, v27

    if-ge v13, v0, :cond_d

    .line 136
    move-object/from16 v0, v25

    invoke-interface {v0, v13}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    .line 137
    .local v22, "n":Lorg/w3c/dom/Node;
    invoke-interface/range {v22 .. v22}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v24

    .line 138
    .local v24, "nodeName":Ljava/lang/String;
    const-string/jumbo v39, "details"

    move-object/from16 v0, v39

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_b

    .line 139
    invoke-interface/range {v22 .. v22}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 135
    :cond_a
    :goto_4
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 140
    :cond_b
    const-string/jumbo v39, "reviews"

    move-object/from16 v0, v39

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_c

    .line 141
    invoke-interface/range {v22 .. v22}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v33

    .line 142
    .local v33, "reviewNodeList":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v33 .. v33}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v28

    .line 143
    .local v28, "numReviews":I
    const/4 v14, 0x0

    .local v14, "k":I
    :goto_5
    move/from16 v0, v28

    if-ge v14, v0, :cond_a

    .line 144
    move-object/from16 v0, v33

    invoke-interface {v0, v14}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v35

    .line 145
    .local v35, "rn":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->parseReview(Lorg/w3c/dom/Node;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;

    move-result-object v32

    .line 146
    .local v32, "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->addReview(Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;)V

    .line 143
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    .line 148
    .end local v14    # "k":I
    .end local v28    # "numReviews":I
    .end local v32    # "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    .end local v33    # "reviewNodeList":Lorg/w3c/dom/NodeList;
    .end local v35    # "rn":Lorg/w3c/dom/Node;
    :cond_c
    invoke-interface/range {v22 .. v22}, Lorg/w3c/dom/Node;->hasChildNodes()Z

    move-result v39

    if-nez v39, :cond_a

    .line 149
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->addNodeToBusinessItem(Lorg/w3c/dom/Node;Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;)V

    goto :goto_4

    .line 155
    .end local v22    # "n":Lorg/w3c/dom/Node;
    .end local v24    # "nodeName":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;->parseDetailsNodeList(Lorg/w3c/dom/NodeList;Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;)V

    .line 157
    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 158
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;->add(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)V

    .line 94
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 160
    .end local v3    # "alreadyReportedOrganic":Z
    .end local v4    # "bi":Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;
    .end local v5    # "detailNodeList":Lorg/w3c/dom/NodeList;
    .end local v13    # "j":I
    .end local v15    # "listingAttributes":Lorg/w3c/dom/NamedNodeMap;
    .end local v17    # "listingId":Ljava/lang/String;
    .end local v18    # "listingNode":Lorg/w3c/dom/Node;
    .end local v25    # "nodes":Lorg/w3c/dom/NodeList;
    .end local v26    # "numAttributes":I
    .end local v27    # "numNodes":I
    :cond_e
    new-instance v39, Landroid/util/Pair;

    move-object/from16 v0, v39

    move-object/from16 v1, v29

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
