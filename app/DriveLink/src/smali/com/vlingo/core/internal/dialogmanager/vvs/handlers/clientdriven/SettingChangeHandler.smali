.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/SettingChangeHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "SettingChangeHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 10
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 23
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 25
    const-string/jumbo v7, "name"

    invoke-static {p1, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 26
    .local v5, "name":Ljava/lang/String;
    const-string/jumbo v7, "state"

    invoke-static {p1, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 27
    .local v6, "state":Ljava/lang/String;
    const-string/jumbo v7, "confirmOn"

    invoke-static {p1, v7, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 28
    .local v3, "confirmOn":Ljava/lang/String;
    const-string/jumbo v7, "confirmOff"

    invoke-static {p1, v7, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 29
    .local v1, "confirmOff":Ljava/lang/String;
    const-string/jumbo v7, "confirmOnTTS"

    invoke-static {p1, v7, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 30
    .local v4, "confirmOnTTS":Ljava/lang/String;
    const-string/jumbo v7, "confirmOffTTS"

    invoke-static {p1, v7, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 31
    .local v2, "confirmOffTTS":Ljava/lang/String;
    const-string/jumbo v7, "alreadySet"

    invoke-static {p1, v7, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "alreadySet":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v7

    const-string/jumbo v8, "settings-change"

    invoke-virtual {v7, v8}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 35
    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v8, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    invoke-virtual {p0, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/SettingChangeHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    invoke-interface {v7, v5}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v7

    invoke-interface {v7, v6}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->state(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v7

    invoke-interface {v7, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->confirmOn(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v7

    invoke-interface {v7, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->confirmOff(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v7

    invoke-interface {v7, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->confirmOnTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v7

    invoke-interface {v7, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->confirmOffTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v7

    invoke-interface {v7, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->alreadySet(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v7

    invoke-interface {v7, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->vvsListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->queue()V

    .line 46
    return v9
.end method
