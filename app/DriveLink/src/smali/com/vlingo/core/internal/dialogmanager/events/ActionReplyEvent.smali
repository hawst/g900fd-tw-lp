.class public Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;
.super Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
.source "ActionReplyEvent.java"


# instance fields
.field private final FIELDID_FIELD_NAME:Ljava/lang/String;

.field private final ID_FIELD_NAME:Ljava/lang/String;

.field private final NAME:Ljava/lang/String;

.field private final SENDER_NAME_FIELD_NAME:Ljava/lang/String;

.field private final SENDER_PHONE_FIELD_NAME:Ljava/lang/String;

.field private fieldId:Ljava/lang/String;

.field private id:J

.field private senderName:Ljava/lang/String;

.field private senderPhone:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # J
    .param p3, "senderName"    # Ljava/lang/String;
    .param p4, "senderPhone"    # Ljava/lang/String;
    .param p5, "fieldId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;-><init>(ZZZ)V

    .line 13
    const-string/jumbo v0, "action-reply"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->NAME:Ljava/lang/String;

    .line 14
    const-string/jumbo v0, "id"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->ID_FIELD_NAME:Ljava/lang/String;

    .line 15
    const-string/jumbo v0, "sender.name"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->SENDER_NAME_FIELD_NAME:Ljava/lang/String;

    .line 16
    const-string/jumbo v0, "sender.phone"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->SENDER_PHONE_FIELD_NAME:Ljava/lang/String;

    .line 17
    const-string/jumbo v0, "fieldid"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->FIELDID_FIELD_NAME:Ljava/lang/String;

    .line 27
    iput-wide p1, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->id:J

    .line 28
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->senderName:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->senderPhone:Ljava/lang/String;

    .line 30
    iput-object p5, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->fieldId:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string/jumbo v0, "action-reply"

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 4

    .prologue
    .line 40
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v1, "action-reply"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 41
    .local v0, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    const-string/jumbo v1, "id"

    iget-wide v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->id:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 42
    const-string/jumbo v1, "sender.name"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->senderName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 43
    const-string/jumbo v1, "sender.phone"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->senderPhone:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 44
    const-string/jumbo v1, "fieldid"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;->fieldId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 46
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v1

    return-object v1
.end method
