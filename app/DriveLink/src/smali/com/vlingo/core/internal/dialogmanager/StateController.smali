.class public abstract Lcom/vlingo/core/internal/dialogmanager/StateController;
.super Lcom/vlingo/core/internal/dialogmanager/Controller;
.source "StateController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;
    }
.end annotation


# instance fields
.field protected action:Lcom/vlingo/sdk/recognition/VLAction;

.field private allSlotsFilled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/StateController;->allSlotsFilled:Z

    .line 124
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 4
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v3, 0x0

    .line 102
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 104
    if-eqz p1, :cond_0

    .line 105
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/StateController;->extractFieldsFromAction(Lcom/vlingo/sdk/recognition/VLAction;)V

    .line 108
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "LPAction"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 109
    const-string/jumbo v1, "Action"

    invoke-static {p1, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "actionValue":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "cancel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 112
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->reset()V

    .line 121
    .end local v0    # "actionValue":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 115
    .restart local v0    # "actionValue":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/StateController;->handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z

    .line 118
    .end local v0    # "actionValue":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->executeNextRule()Z

    move-result v1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/StateController;->allSlotsFilled:Z

    goto :goto_0
.end method

.method protected executeConfirm()V
    .locals 5

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->getRules()Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 77
    .local v0, "r":Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;
    :goto_0
    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getPrompt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getPrompt()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getFieldId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->SHORT:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/StateController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 79
    :cond_0
    return-void

    .line 76
    .end local v0    # "r":Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->getRules()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "Confirm"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    move-object v0, v1

    goto :goto_0
.end method

.method protected executeNextRule()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 82
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->getRules()Ljava/util/Map;

    move-result-object v3

    if-nez v3, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v2

    .line 87
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->getRules()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    .line 88
    .local v1, "r":Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->isRequired()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 89
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getPrompt()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getPrompt()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getFieldId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/StateController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 90
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected extractFieldsFromAction(Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 9
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v8, 0x0

    .line 38
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->getRules()Ljava/util/Map;

    move-result-object v5

    .line 39
    .local v5, "rules":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;>;"
    if-eqz v5, :cond_0

    if-nez p1, :cond_1

    .line 69
    :cond_0
    return-void

    .line 43
    :cond_1
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 44
    .local v2, "paramName":Ljava/lang/String;
    invoke-static {p1, v2, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 45
    .local v6, "value":Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 46
    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    .line 47
    .local v3, "rule":Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;
    if-eqz v3, :cond_2

    .line 48
    invoke-virtual {v3, v6}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 53
    .end local v2    # "paramName":Ljava/lang/String;
    .end local v3    # "rule":Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;
    .end local v6    # "value":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->getRuleMappings()Ljava/util/Map;

    move-result-object v4

    .line 54
    .local v4, "ruleMappings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v4, :cond_0

    .line 57
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getParameterNames()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 58
    .restart local v2    # "paramName":Ljava/lang/String;
    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 59
    .local v1, "mappedName":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 60
    invoke-static {p1, v2, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 61
    .restart local v6    # "value":Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 62
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    .line 63
    .restart local v3    # "rule":Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;
    if-eqz v3, :cond_4

    .line 64
    invoke-virtual {v3, v6}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->setValue(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public abstract getRuleMappings()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRules()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation
.end method

.method public abstract handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
.end method

.method protected hasConfirm()Z
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->getRules()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->getRules()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "Confirm"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isAllSlotsFilled()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/StateController;->allSlotsFilled:Z

    return v0
.end method

.method protected setAllSlotsFilled(Z)V
    .locals 0
    .param p1, "allSlotsFilled"    # Z

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/StateController;->allSlotsFilled:Z

    .line 158
    return-void
.end method
