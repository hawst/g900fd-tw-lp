.class public Lcom/vlingo/core/internal/util/MusicUtil$PlaylistInfo;
.super Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;
.source "MusicUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/MusicUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlaylistInfo"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;-><init>()V

    .line 145
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/MusicUtil$PlaylistInfo;->type:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    .line 146
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "id"    # I

    .prologue
    .line 149
    const-string/jumbo v0, ""

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/MusicUtil$PlaylistInfo;->type:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    .line 151
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/util/MusicUtil$PlaylistInfo;->setId(I)V

    .line 152
    return-void
.end method
