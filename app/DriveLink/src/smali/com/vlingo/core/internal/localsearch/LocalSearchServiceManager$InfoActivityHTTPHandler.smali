.class Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;
.super Ljava/lang/Object;
.source "LocalSearchServiceManager.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InfoActivityHTTPHandler"
.end annotation


# instance fields
.field m_actionType:Ljava/lang/String;

.field m_bi:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

.field m_retryNum:I

.field final synthetic this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Ljava/lang/String;I)V
    .locals 0
    .param p2, "bi"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .param p3, "actionType"    # Ljava/lang/String;
    .param p4, "retryNum"    # I

    .prologue
    .line 615
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 616
    iput-object p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->m_bi:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .line 617
    iput-object p3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->m_actionType:Ljava/lang/String;

    .line 618
    iput p4, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->m_retryNum:I

    .line 619
    return-void
.end method


# virtual methods
.method public onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 667
    return-void
.end method

.method public onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 661
    return-void
.end method

.method public onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 6
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 621
    const/4 v1, 0x0

    .line 624
    .local v1, "success":Z
    iget v2, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    .line 625
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v0

    .line 626
    .local v0, "res":Ljava/lang/String;
    const-string/jumbo v2, "<InfoActivityResponse Status=\"OK\"/>"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 629
    const/4 v1, 0x1

    .line 632
    .end local v0    # "res":Ljava/lang/String;
    :cond_0
    if-nez v1, :cond_1

    .line 633
    # getter for: Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "InfoActivityRequest failure!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    iget v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->m_retryNum:I

    if-lez v2, :cond_2

    .line 635
    # getter for: Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Retrying, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->m_retryNum:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " retries remain"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    iget v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->m_retryNum:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->m_retryNum:I

    .line 637
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    iget-object v3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->m_bi:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    iget-object v4, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->m_actionType:Ljava/lang/String;

    iget v5, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;->m_retryNum:I

    invoke-virtual {v2, v3, v4, v5}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendInfoActivityRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Ljava/lang/String;I)V

    .line 643
    :cond_1
    :goto_0
    return-void

    .line 640
    :cond_2
    # getter for: Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "NOT retrying, out of retries"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 654
    const/4 v0, 0x0

    return v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 648
    return-void
.end method
