.class public final Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;
.super Ljava/lang/Object;
.source "VLConfigImporter.java"


# static fields
.field private static final DEFAULT_CRITICAL_SETTINGS:Ljava/lang/String; = "shared_critical_settings,lmtt.enable.pim,lmtt.enable.music,lmtt.update.version,lmtt.update.version.pim,lmtt.update.version.music,LMTT_HOST_NAME,lmtt.chunk_delay_ms,lmtt.chunk_retries,lmtt.chunk_retry_delay_ms,lmtt.chunk_size,lmtt.client_shield_duration_mins,lmtt.no_activity_shutdown_period_mins,lmtt.task_retries"

.field private static final TAG:Ljava/lang/String;

.field static configNameMappingTable:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->TAG:Ljava/lang/String;

    .line 33
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->configNameMappingTable:Ljava/util/Hashtable;

    .line 36
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCriticalSettingIndices(Ljava/util/List;Ljava/util/List;)Ljava/util/Set;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "keysIn":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p1, "valuesIn":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v11, -0x1

    .line 176
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 177
    .local v3, "indices":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const-string/jumbo v9, "shared_critical_settings"

    invoke-interface {p0, v9}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 179
    .local v2, "index":I
    if-le v2, v11, :cond_1

    .line 180
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 185
    .local v5, "setting":Ljava/lang/String;
    :goto_0
    const/16 v9, 0x2c

    invoke-static {v5, v9}, Lcom/vlingo/core/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v8

    .line 186
    .local v8, "values":[Ljava/lang/String;
    move-object v0, v8

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v6, v0, v1

    .line 187
    .local v6, "value":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p0, v9}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v7

    .line 188
    .local v7, "valueIndex":I
    if-le v7, v11, :cond_0

    .line 189
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 186
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 182
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "setting":Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    .end local v7    # "valueIndex":I
    .end local v8    # "values":[Ljava/lang/String;
    :cond_1
    const-string/jumbo v9, "shared_critical_settings"

    const-string/jumbo v10, "shared_critical_settings,lmtt.enable.pim,lmtt.enable.music,lmtt.update.version,lmtt.update.version.pim,lmtt.update.version.music,LMTT_HOST_NAME,lmtt.chunk_delay_ms,lmtt.chunk_retries,lmtt.chunk_retry_delay_ms,lmtt.chunk_size,lmtt.client_shield_duration_mins,lmtt.no_activity_shutdown_period_mins,lmtt.task_retries"

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "setting":Ljava/lang/String;
    goto :goto_0

    .line 192
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "i$":I
    .restart local v4    # "len$":I
    .restart local v8    # "values":[Ljava/lang/String;
    :cond_2
    return-object v3
.end method

.method public static handleIntent(Landroid/content/Intent;)V
    .locals 9
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 154
    const-string/jumbo v6, "keys"

    invoke-virtual {p0, v6}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 155
    .local v0, "allKeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v6, "values"

    invoke-virtual {p0, v6}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 156
    .local v2, "allValues":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v6, "types"

    invoke-virtual {p0, v6}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 157
    .local v1, "allTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 159
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    if-nez v3, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->getCriticalSettingIndices(Ljava/util/List;Ljava/util/List;)Ljava/util/Set;

    move-result-object v5

    .line 164
    .local v5, "keysWeWant":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 166
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_3

    .line 167
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 168
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v6, v7, v8, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->updateSetting(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences$Editor;)V

    .line 166
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 171
    :cond_3
    invoke-static {v3}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public static importSettings(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 1
    .param p0, "vlConfigXML"    # Ljava/lang/String;
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 47
    new-instance v0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;

    invoke-direct {v0}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;-><init>()V

    invoke-static {p0, v0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->importSettings(Ljava/lang/String;Lcom/vlingo/core/internal/vlservice/VLConfigParser;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 48
    return-void
.end method

.method public static importSettings(Ljava/lang/String;Lcom/vlingo/core/internal/vlservice/VLConfigParser;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 19
    .param p0, "vlConfigXML"    # Ljava/lang/String;
    .param p1, "configParser"    # Lcom/vlingo/core/internal/vlservice/VLConfigParser;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 53
    :try_start_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->parseXML(Ljava/lang/String;)V

    .line 55
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->getSettings()Ljava/util/Hashtable;

    move-result-object v11

    .line 56
    .local v11, "settings":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-virtual {v11}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v5

    .line 57
    .local v5, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 59
    .local v6, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v2, "arrayKeys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 61
    .local v4, "arrayValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .local v3, "arrayTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    :try_start_1
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 65
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 66
    .local v9, "key":Ljava/lang/String;
    sget-object v14, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->configNameMappingTable:Ljava/util/Hashtable;

    invoke-virtual {v14, v9}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 67
    sget-object v14, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->configNameMappingTable:Ljava/util/Hashtable;

    invoke-virtual {v14, v9}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "key":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 68
    .restart local v9    # "key":Ljava/lang/String;
    :cond_0
    invoke-virtual {v11, v9}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/util/Pair;

    .line 69
    .local v10, "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v13, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v13, Ljava/lang/String;

    .line 70
    .local v13, "value":Ljava/lang/String;
    iget-object v12, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v12, Ljava/lang/String;

    .line 71
    .local v12, "type":Ljava/lang/String;
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-virtual {v4, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    invoke-virtual {v3, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    invoke-static {v9, v13, v12, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->updateSetting(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences$Editor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 80
    .end local v9    # "key":Ljava/lang/String;
    .end local v10    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12    # "type":Ljava/lang/String;
    .end local v13    # "value":Ljava/lang/String;
    :catchall_0
    move-exception v14

    :try_start_2
    invoke-static {v6}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    throw v14
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 95
    .end local v2    # "arrayKeys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "arrayTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "arrayValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .end local v6    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v11    # "settings":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :catch_0
    move-exception v7

    .line 96
    .local v7, "ex":Ljava/lang/Exception;
    sget-object v14, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "SET1 Error importing settings: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    .end local v7    # "ex":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-void

    .line 80
    .restart local v2    # "arrayKeys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v3    # "arrayTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v4    # "arrayValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .restart local v6    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v11    # "settings":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_2
    :try_start_3
    invoke-static {v6}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 82
    const-string/jumbo v14, "appstate.config_last_update"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    invoke-static/range {v14 .. v16}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 83
    const-string/jumbo v14, "appstate.config_update_count"

    const-string/jumbo v15, "appstate.config_update_count"

    const-wide/16 v16, 0x0

    invoke-static/range {v15 .. v17}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v15

    const-wide/16 v17, 0x1

    add-long v15, v15, v17

    invoke-static/range {v14 .. v16}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 85
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 86
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 87
    .local v8, "intent":Landroid/content/Intent;
    const-string/jumbo v14, "com.vlingo.midas.hello_update"

    invoke-virtual {v8, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    const-string/jumbo v14, "keys"

    invoke-virtual {v8, v14, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 89
    const-string/jumbo v14, "values"

    invoke-virtual {v8, v14, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 90
    const-string/jumbo v14, "types"

    invoke-virtual {v8, v14, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 91
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1
.end method

.method public static updateSetting(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences$Editor;)V
    .locals 9
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 115
    const-string/jumbo v6, "string"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 116
    invoke-static {p3, p0, p1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    const-string/jumbo v6, "boolean"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 119
    const-string/jumbo v6, "true"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 120
    .local v0, "boolVal":Z
    invoke-static {p3, p0, v0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Z)V

    goto :goto_0

    .line 122
    .end local v0    # "boolVal":Z
    :cond_2
    const-string/jumbo v6, "long"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 124
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 125
    .local v4, "longVal":J
    invoke-static {p3, p0, v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 127
    .end local v4    # "longVal":J
    :catch_0
    move-exception v1

    .line 128
    .local v1, "ex":Ljava/lang/NumberFormatException;
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "SET3 unable to parse long value: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 131
    .end local v1    # "ex":Ljava/lang/NumberFormatException;
    :cond_3
    const-string/jumbo v6, "integer"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 133
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 134
    .local v3, "intVal":I
    invoke-static {p3, p0, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 136
    .end local v3    # "intVal":I
    :catch_1
    move-exception v1

    .line 137
    .restart local v1    # "ex":Ljava/lang/NumberFormatException;
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "SET3 + unable to parse int value: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 140
    .end local v1    # "ex":Ljava/lang/NumberFormatException;
    :cond_4
    const-string/jumbo v6, "float"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 142
    :try_start_2
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 143
    .local v2, "floatVal":F
    invoke-static {p3, p0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setFloat(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;F)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 145
    .end local v2    # "floatVal":F
    :catch_2
    move-exception v1

    .line 146
    .restart local v1    # "ex":Ljava/lang/NumberFormatException;
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "SET3unable to parse long value: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
