.class public final Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;
.super Ljava/lang/Object;
.source "PhoneticEncodingUtils.java"


# static fields
.field private static final PHONETIC_ENCODERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    const-class v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->TAG:Ljava/lang/String;

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->PHONETIC_ENCODERS:Ljava/util/List;

    .line 30
    sget-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->PHONETIC_ENCODERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    sget-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->PHONETIC_ENCODERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static applyOperations(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 7
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "authority"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 206
    .local p2, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/16 v2, 0x12c

    .line 208
    .local v2, "maxSizeOfOperations":I
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    div-int v3, v5, v2

    .line 209
    .local v3, "numberOfSlides":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-gt v1, v3, :cond_1

    .line 210
    add-int/lit8 v5, v1, 0x1

    mul-int/lit16 v5, v5, 0x12c

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v5, v6, :cond_0

    add-int/lit8 v5, v1, 0x1

    mul-int/lit16 v0, v5, 0x12c

    .line 211
    .local v0, "end":I
    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    mul-int/lit16 v5, v1, 0x12c

    invoke-virtual {p2, v5, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 212
    .local v4, "subList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {p0, p1, v4}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 210
    .end local v0    # "end":I
    .end local v4    # "subList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_1

    .line 215
    :cond_1
    return-void
.end method

.method public static clearEncodersCachedValues()V
    .locals 3

    .prologue
    .line 133
    sget-object v2, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->PHONETIC_ENCODERS:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;

    .line 134
    .local v0, "encoder":Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;->clearCachedValues()V

    goto :goto_0

    .line 136
    .end local v0    # "encoder":Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
    :cond_0
    return-void
.end method

.method public static encodeContactData(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/util/List;)V
    .locals 22
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "origUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p2, "rawContactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const-string/jumbo v3, ","

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/StringUtils;->join(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 71
    .local v15, "joinedIds":Ljava/lang/String;
    const/16 v3, 0x11

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "mimetype"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "contact_id"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "raw_contact_id"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "lookup"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "display_name"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string/jumbo v4, "starred"

    aput-object v4, v5, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "data1"

    aput-object v4, v5, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "data2"

    aput-object v4, v5, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "data3"

    aput-object v4, v5, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "data4"

    aput-object v4, v5, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "data5"

    aput-object v4, v5, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "data6"

    aput-object v4, v5, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "data7"

    aput-object v4, v5, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "data8"

    aput-object v4, v5, v3

    const/16 v3, 0xe

    const-string/jumbo v4, "data9"

    aput-object v4, v5, v3

    const/16 v3, 0xf

    const-string/jumbo v4, "times_contacted"

    aput-object v4, v5, v3

    const/16 v3, 0x10

    const-string/jumbo v4, "phonetic_name"

    aput-object v4, v5, v3

    .line 73
    .local v5, "projection":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 75
    .local v9, "c":Landroid/database/Cursor;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v16, "mimetypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v3, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->PHONETIC_ENCODERS:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;

    .line 77
    .local v12, "encoder":Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
    invoke-virtual {v12}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;->getMimetypeList()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 84
    .end local v12    # "encoder":Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
    :cond_0
    :try_start_0
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    .local v17, "mimetypes":Ljava/lang/StringBuilder;
    const/4 v13, 0x1

    .line 86
    .local v13, "first":Z
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 87
    .local v19, "type":Ljava/lang/String;
    if-eqz v13, :cond_2

    .line 88
    const/4 v13, 0x0

    .line 92
    :goto_2
    const-string/jumbo v3, "\'"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 119
    .end local v13    # "first":Z
    .end local v17    # "mimetypes":Ljava/lang/StringBuilder;
    .end local v19    # "type":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 120
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "encodeContactData: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v11}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122
    if-eqz v9, :cond_1

    .line 124
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 130
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_3
    return-void

    .line 90
    .restart local v13    # "first":Z
    .restart local v17    # "mimetypes":Ljava/lang/StringBuilder;
    .restart local v19    # "type":Ljava/lang/String;
    :cond_2
    :try_start_3
    const-string/jumbo v3, ", "

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 122
    .end local v13    # "first":Z
    .end local v17    # "mimetypes":Ljava/lang/StringBuilder;
    .end local v19    # "type":Ljava/lang/String;
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_3

    .line 124
    :try_start_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 127
    :cond_3
    :goto_4
    throw v3

    .line 95
    .restart local v13    # "first":Z
    .restart local v17    # "mimetypes":Ljava/lang/StringBuilder;
    :cond_4
    :try_start_5
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "caller_is_syncadapter"

    const-string/jumbo v6, "true"

    invoke-virtual {v3, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v20

    .line 98
    .local v20, "uri":Landroid/net/Uri;
    const-string/jumbo v3, "%s IN (%s) AND %s in (%s)"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "raw_contact_id"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    aput-object v15, v4, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "mimetype"

    aput-object v7, v4, v6

    const/4 v6, 0x3

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    .line 99
    .local v21, "where":Ljava/lang/String;
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 108
    .local v10, "deletedItems":I
    const-string/jumbo v3, "%s IN (?,?,?) AND %s IN (%s)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "mimetype"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "raw_contact_id"

    aput-object v7, v4, v6

    const/4 v6, 0x2

    aput-object v15, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x3

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "vnd.android.cursor.item/name"

    aput-object v4, v7, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "vnd.android.cursor.item/organization"

    aput-object v4, v7, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "vnd.android.cursor.item/nickname"

    aput-object v4, v7, v3

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 116
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->getOperations(Landroid/net/Uri;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v18

    .line 118
    .local v18, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->applyOperations(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 122
    if-eqz v9, :cond_1

    .line 124
    :try_start_6
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_3

    .line 125
    :catch_1
    move-exception v11

    .line 126
    .restart local v11    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "encodeContactData: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v11}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 125
    .end local v10    # "deletedItems":I
    .end local v13    # "first":Z
    .end local v17    # "mimetypes":Ljava/lang/StringBuilder;
    .end local v18    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v20    # "uri":Landroid/net/Uri;
    .end local v21    # "where":Ljava/lang/String;
    :catch_2
    move-exception v11

    .line 126
    sget-object v3, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "encodeContactData: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v11}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 125
    .end local v11    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v11

    .line 126
    .restart local v11    # "e":Ljava/lang/Exception;
    sget-object v4, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "encodeContactData: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v11}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4
.end method

.method private static getNextOperation(Landroid/net/Uri;Landroid/database/Cursor;Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;Ljava/util/List;Ljava/util/List;)Landroid/content/ContentProviderOperation;
    .locals 16
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "encoder"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/database/Cursor;",
            "Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/ContentProviderOperation;"
        }
    .end annotation

    .prologue
    .line 161
    .local p3, "prevPhoneticEncoding":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "phoneticEncoding":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;->getDataColumns()[Ljava/lang/String;

    move-result-object v3

    .line 162
    .local v3, "dataColumnNames":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v13

    const-string/jumbo v14, "caller_is_syncadapter"

    const-string/jumbo v15, "true"

    invoke-virtual {v13, v14, v15}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v13

    invoke-virtual {v13}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    .line 163
    invoke-static/range {p0 .. p0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    .line 164
    .local v12, "operationBuilder":Landroid/content/ContentProviderOperation$Builder;
    const/4 v5, 0x0

    .line 165
    .local v5, "foundEncodedData":Z
    const/4 v6, 0x0

    .line 166
    .local v6, "i":I
    move-object v1, v3

    .local v1, "arr$":[Ljava/lang/String;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_5

    aget-object v2, v1, v7

    .line 167
    .local v2, "columnName":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 168
    .local v10, "name":Ljava/lang/String;
    invoke-static {v10}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_4

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;->canEncode(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 169
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 170
    .local v4, "encodedName":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    const-string/jumbo v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 166
    .end local v4    # "encodedName":Ljava/lang/String;
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 173
    .restart local v4    # "encodedName":Ljava/lang/String;
    :cond_0
    if-eqz p3, :cond_1

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v13

    array-length v14, v3

    if-ne v13, v14, :cond_1

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_2

    :cond_1
    const/4 v11, 0x1

    .line 174
    .local v11, "notEqualToPrevEncoding":Z
    :goto_2
    const-string/jumbo v13, ""

    invoke-virtual {v4, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    if-eqz v11, :cond_3

    .line 177
    invoke-virtual {v12, v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 178
    move-object/from16 v0, p4

    invoke-interface {v0, v6, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 179
    const/4 v5, 0x1

    .line 188
    .end local v4    # "encodedName":Ljava/lang/String;
    .end local v11    # "notEqualToPrevEncoding":Z
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 173
    .restart local v4    # "encodedName":Ljava/lang/String;
    :cond_2
    const/4 v11, 0x0

    goto :goto_2

    .line 183
    .restart local v11    # "notEqualToPrevEncoding":Z
    :cond_3
    const-string/jumbo v13, ""

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_3

    .line 186
    .end local v4    # "encodedName":Ljava/lang/String;
    .end local v11    # "notEqualToPrevEncoding":Z
    :cond_4
    const-string/jumbo v13, ""

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_3

    .line 190
    .end local v2    # "columnName":Ljava/lang/String;
    .end local v10    # "name":Ljava/lang/String;
    :cond_5
    if-eqz v5, :cond_6

    .line 191
    const-string/jumbo v13, "raw_contact_id"

    const-string/jumbo v14, "raw_contact_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 192
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "mimetype"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;->getMimeType()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 193
    .local v9, "mimeType":Ljava/lang/String;
    const-string/jumbo v13, "mimetype"

    invoke-virtual {v12, v13, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 194
    const-string/jumbo v13, "contact_id"

    const-string/jumbo v14, "contact_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 195
    const-string/jumbo v13, "lookup"

    const-string/jumbo v14, "lookup"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 196
    const-string/jumbo v13, "display_name"

    const-string/jumbo v14, "display_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 197
    const-string/jumbo v13, "starred"

    const-string/jumbo v14, "starred"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 198
    const-string/jumbo v13, "times_contacted"

    const-string/jumbo v14, "times_contacted"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 199
    const-string/jumbo v13, "phonetic_name"

    const-string/jumbo v14, "phonetic_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 200
    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v13

    .line 202
    .end local v9    # "mimeType":Ljava/lang/String;
    :goto_4
    return-object v13

    :cond_6
    const/4 v13, 0x0

    goto :goto_4
.end method

.method private static getOperations(Landroid/net/Uri;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 140
    .local v3, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 142
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v5, "prevPhoneticEncoding":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v6, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->PHONETIC_ENCODERS:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;

    .line 145
    .local v0, "encoder":Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v4, "phoneticEncoding":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p0, p1, v0, v5, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->getNextOperation(Landroid/net/Uri;Landroid/database/Cursor;Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;Ljava/util/List;Ljava/util/List;)Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 147
    .local v2, "operation":Landroid/content/ContentProviderOperation;
    move-object v5, v4

    .line 148
    if-eqz v2, :cond_1

    .line 149
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 152
    .end local v0    # "encoder":Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
    .end local v2    # "operation":Landroid/content/ContentProviderOperation;
    .end local v4    # "phoneticEncoding":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 157
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "prevPhoneticEncoding":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    return-object v3
.end method

.method public static varargs queryToWhereClauseWithEncoding(Ljava/util/List;Ljava/util/List;[Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p2, "fields"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;",
            ">;>;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p0, "query":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p1, "encoderClasses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Class<+Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;>;>;"
    const/16 v11, 0x27

    .line 37
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .local v8, "whereClause":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 40
    .local v0, "counter":I
    sget-object v9, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->PHONETIC_ENCODERS:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;

    .line 41
    .local v1, "encoder":Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-interface {p1, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 42
    if-eqz v0, :cond_1

    .line 43
    const-string/jumbo v9, " OR "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v2, "extendedMimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;->getMimetypeList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 48
    .local v7, "mimetype":Ljava/lang/String;
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 50
    .end local v7    # "mimetype":Ljava/lang/String;
    :cond_2
    const-string/jumbo v9, "( mimetype IN ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    const/4 v3, 0x1

    .line 52
    .local v3, "first":Z
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 53
    .local v6, "mimeType":Ljava/lang/String;
    if-nez v3, :cond_3

    .line 54
    const-string/jumbo v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    :cond_3
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 57
    const/4 v3, 0x0

    goto :goto_2

    .line 59
    .end local v6    # "mimeType":Ljava/lang/String;
    :cond_4
    const-string/jumbo v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    const-string/jumbo v9, " AND "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {v1, p0, p2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;->getNameQueryPart(Ljava/util/List;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 65
    .end local v1    # "encoder":Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
    .end local v2    # "extendedMimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "first":Z
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method
