.class Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;
.super Ljava/lang/Object;
.source "DialogTurn.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

.field final synthetic val$event:Lcom/vlingo/core/internal/dialogmanager/DialogEvent;

.field final synthetic val$turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/core/internal/dialogmanager/DialogEvent;)V
    .locals 0

    .prologue
    .line 541
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;->val$turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;->val$event:Lcom/vlingo/core/internal/dialogmanager/DialogEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 543
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;->val$turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-nez v0, :cond_0

    .line 544
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Received a queue event before our first turn"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;->val$turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getTurnNumber()I

    move-result v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iget-object v1, v1, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getTurnNumber()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 547
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;->val$event:Lcom/vlingo/core/internal/dialogmanager/DialogEvent;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->shouldStopTtsReco()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 548
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->interruptTurn()V

    .line 551
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;->val$event:Lcom/vlingo/core/internal/dialogmanager/DialogEvent;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->addEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;)V

    .line 552
    return-void
.end method
