.class Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;
.super Ljava/lang/Object;
.source "DMServerTask.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/TonePlayer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

.field private usingMediaSyncEvent:Z


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;)V
    .locals 1

    .prologue
    .line 295
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->usingMediaSyncEvent:Z

    return-void
.end method


# virtual methods
.method public onStarted(I)V
    .locals 3
    .param p1, "audioSessionId"    # I

    .prologue
    .line 313
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->useMediaSyncApproach:Z
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$800(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 314
    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[LatencyCheck] TonePlayer.onStarted: Starting recording for:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->usingMediaSyncEvent:Z

    .line 316
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->DataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;->onDataNotReady()V

    .line 317
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    iget-object v1, v1, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->DataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    move-result-object v1

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performReco(Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;I)V
    invoke-static {v0, v1, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$900(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;I)V

    .line 319
    :cond_0
    return-void
.end method

.method public onStopped()V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$500(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$500(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onRecoToneStopped(Z)V

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->DataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;->onDataReady()V

    .line 305
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->usingMediaSyncEvent:Z

    if-nez v0, :cond_1

    .line 306
    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onStopped: Starting recording"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    iget-object v1, v1, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->DataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    move-result-object v1

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performReco(Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$700(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V

    .line 309
    :cond_1
    return-void
.end method
