.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "WeatherLookupHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
.implements Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;
    }
.end annotation


# static fields
.field private static final SDF_2:Ljava/text/SimpleDateFormat;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

.field private forecastDay:Ljava/lang/String;

.field handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 53
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->TAG:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->SDF_2:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    .line 55
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->forecastDay:Ljava/lang/String;

    .line 60
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->handler:Landroid/os/Handler;

    .line 62
    new-instance v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-direct {v0}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    .line 63
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 64
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->handler:Landroid/os/Handler;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 44
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->strDatetoDOW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 44
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 44
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 44
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 44
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;
    .param p1, "x1"    # Ljava/lang/Integer;
    .param p2, "x2"    # Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    .prologue
    .line 44
    invoke-direct/range {p0 .. p5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getOneDaySpokenMsgInternal(Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 44
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 44
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 44
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getForecastDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 414
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getForecastDescriptionByAdaptorDate()Ljava/lang/String;

    move-result-object v0

    .line 415
    .local v0, "forecastDesc":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 416
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getForecastDescriptionFromFirstForecast()Ljava/lang/String;

    move-result-object v0

    .line 418
    :cond_0
    return-object v0
.end method

.method private getForecastDescription(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 422
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getForecastDescriptionByDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 423
    .local v0, "forecastDesc":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 424
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getForecastDescriptionFromFirstForecast()Ljava/lang/String;

    move-result-object v0

    .line 426
    :cond_0
    if-nez v0, :cond_1

    .line 427
    invoke-static {p1}, Lcom/vlingo/core/internal/schedule/DateUtil;->diffInDays(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getForecastDescriptionFromSpecificForecast(I)Ljava/lang/String;

    move-result-object v0

    .line 429
    :cond_1
    return-object v0
.end method

.method private getForecastDescriptionByAdaptorDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getDate()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getForecastDescriptionByDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getForecastDescriptionByDate(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 319
    const/4 v1, 0x0

    .line 321
    .local v1, "forecastDesc":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "Forecasts"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "ForecastDate"

    invoke-virtual {v3, v4, p1}, Lcom/vlingo/core/internal/weather/WeatherElement;->findChildWithAttr(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "DaytimeForecast"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "DayNighttimeForecast"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v3

    const-string/jumbo v4, "ShortText"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_0
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    :goto_0
    if-nez v1, :cond_0

    .line 332
    :try_start_1
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "Forecasts"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "ForecastDate"

    invoke-virtual {v3, v4, p1}, Lcom/vlingo/core/internal/weather/WeatherElement;->findChildWithAttr(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "DaytimeForecast"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "DayNighttimeForecast"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v3

    const-string/jumbo v4, "LongText"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_1
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_1 .. :try_end_1} :catch_1

    .line 342
    :cond_0
    :goto_1
    return-object v1

    .line 327
    :catch_0
    move-exception v2

    .line 328
    .local v2, "wnf":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "caught exception looking for SHORT_TEXT #1 \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 338
    .end local v2    # "wnf":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    :catch_1
    move-exception v2

    .line 339
    .restart local v2    # "wnf":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "caught exception looking for LONG_TEXT #1 \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getForecastDescriptionFromFirstForecast()Ljava/lang/String;
    .locals 6

    .prologue
    .line 354
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isToday()Z

    move-result v3

    if-nez v3, :cond_0

    .line 355
    const/4 v1, 0x0

    .line 378
    :goto_0
    return-object v1

    .line 357
    :cond_0
    const/4 v1, 0x0

    .line 359
    .local v1, "forecastDesc":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "Forecasts"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "Forecast"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "DaytimeForecast"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "DayNighttimeForecast"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v3

    const-string/jumbo v4, "ShortText"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_0
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_0 .. :try_end_0} :catch_0

    .line 369
    :goto_1
    :try_start_1
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "Forecasts"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "Forecast"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "DaytimeForecast"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "DayNighttimeForecast"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v3

    const-string/jumbo v4, "LongText"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_1
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 365
    :catch_0
    move-exception v2

    .line 366
    .local v2, "wnf":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "caught exception looking for SHORT_TEXT #2 \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 375
    .end local v2    # "wnf":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    :catch_1
    move-exception v2

    .line 376
    .restart local v2    # "wnf":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "caught exception looking for LONG_TEXT #2 \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private getForecastDescriptionFromSpecificForecast(I)Ljava/lang/String;
    .locals 9
    .param p1, "day"    # I

    .prologue
    .line 390
    const/4 v0, 0x0

    .line 391
    .local v0, "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    const/4 v1, 0x0

    .line 393
    .local v1, "forecastAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v6

    const-string/jumbo v7, "Forecasts"

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    .line 394
    .local v3, "parentForecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    if-lez p1, :cond_0

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v6

    if-ge p1, v6, :cond_0

    .line 395
    invoke-virtual {v3, p1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v0

    .line 396
    const-string/jumbo v6, "DaytimeForecast"

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v6

    const-string/jumbo v7, "DayNighttimeForecast"

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;
    :try_end_0
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 404
    .end local v3    # "parentForecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_3

    .line 405
    :cond_1
    const/4 v2, 0x0

    .line 410
    :cond_2
    :goto_1
    return-object v2

    .line 401
    :catch_0
    move-exception v5

    .line 402
    .local v5, "wnf":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "caught exception looking for FORECASTS #3 \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 407
    .end local v5    # "wnf":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    :cond_3
    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v6

    const-string/jumbo v7, "ForecastDate"

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->forecastDay:Ljava/lang/String;

    .line 408
    const-string/jumbo v6, "ShortText"

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 409
    .local v4, "shortDesc":Ljava/lang/String;
    const-string/jumbo v6, "LongText"

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 410
    .local v2, "longDesc":Ljava/lang/String;
    if-eqz v4, :cond_2

    move-object v2, v4

    goto :goto_1
.end method

.method private getOneDaySpokenMsgInternal(Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;
    .locals 14
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "weatherInfoUtil"    # Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
    .param p3, "day"    # Ljava/lang/String;
    .param p4, "date"    # Ljava/lang/String;
    .param p5, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    .prologue
    .line 268
    const/4 v12, 0x0

    .line 269
    .local v12, "spokenMsg":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getCurrentTempForTTS()Ljava/lang/String;

    move-result-object v8

    .line 270
    .local v8, "currentTempForTTS":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getMaximumTempOneDayWeather()Ljava/lang/String;

    move-result-object v7

    .line 271
    .local v7, "temp":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getCurrentWeatherCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 272
    .local v13, "weatherCode":Ljava/lang/Integer;
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getForecastDescription(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 273
    .local v3, "forecast":Ljava/lang/String;
    if-nez v3, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getProvider()Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-result-object v2

    if-ne v1, v2, :cond_3

    .line 275
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->forecastDay:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 276
    :goto_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationState()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v9

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v4, p3

    move-object/from16 v10, p5

    invoke-virtual/range {v1 .. v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getSpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;

    move-result-object v12

    .line 287
    :cond_1
    :goto_1
    return-object v12

    .line 275
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->forecastDay:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->strDatetoDOW(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    goto :goto_0

    .line 277
    :catch_0
    move-exception v11

    .line 279
    .local v11, "e":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    invoke-virtual {v11}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->printStackTrace()V

    goto :goto_1

    .line 281
    .end local v11    # "e":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isToday()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 282
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_general:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    goto :goto_1
.end method

.method private showSuccess()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 238
    return-void
.end method

.method private strDatetoDOW(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "dateStr"    # Ljava/lang/String;

    .prologue
    .line 241
    const/4 v0, 0x0

    .line 244
    .local v0, "ret":Ljava/lang/String;
    :try_start_0
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->SDF_2:Ljava/text/SimpleDateFormat;

    invoke-virtual {v4, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 246
    .local v3, "wDate":Ljava/util/Date;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "ru-RU"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 247
    new-instance v1, Ljava/text/DateFormatSymbols;

    invoke-direct {v1}, Ljava/text/DateFormatSymbols;-><init>()V

    .line 248
    .local v1, "russianDateFormatSymbols":Ljava/text/DateFormatSymbols;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isBMode()Z

    move-result v4

    if-nez v4, :cond_0

    .line 249
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_russian_weekdays:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$array;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/DateFormatSymbols;->setWeekdays([Ljava/lang/String;)V

    .line 253
    :goto_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v4, "EEEE"

    invoke-direct {v2, v4, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/text/DateFormatSymbols;)V

    .line 258
    .end local v1    # "russianDateFormatSymbols":Ljava/text/DateFormatSymbols;
    .local v2, "sdfWeekday":Ljava/text/SimpleDateFormat;
    :goto_1
    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 260
    .end local v2    # "sdfWeekday":Ljava/text/SimpleDateFormat;
    .end local v3    # "wDate":Ljava/util/Date;
    :goto_2
    return-object v0

    .line 252
    .restart local v1    # "russianDateFormatSymbols":Ljava/text/DateFormatSymbols;
    .restart local v3    # "wDate":Ljava/util/Date;
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_russian_weekdays:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/DateFormatSymbols;->setWeekdays([Ljava/lang/String;)V

    goto :goto_0

    .line 259
    .end local v1    # "russianDateFormatSymbols":Ljava/text/DateFormatSymbols;
    .end local v3    # "wDate":Ljava/util/Date;
    :catch_0
    move-exception v4

    goto :goto_2

    .line 255
    .restart local v3    # "wDate":Ljava/util/Date;
    :cond_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v4, "EEEE"

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 256
    .restart local v2    # "sdfWeekday":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2}, Ljava/text/SimpleDateFormat;->toLocalizedPattern()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->applyLocalizedPattern(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v7, 0x0

    .line 72
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 74
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v5

    const-string/jumbo v6, "weather"

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 76
    const-string/jumbo v5, "Location"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "location":Ljava/lang/String;
    const-string/jumbo v5, "Date"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "dateStr":Ljava/lang/String;
    const-string/jumbo v5, "SpokenDate"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 79
    .local v3, "spokenDataStr":Ljava/lang/String;
    const-string/jumbo v5, "time"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 80
    .local v4, "time":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 81
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "yyyy-MM-dd"

    invoke-direct {v2, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 82
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 84
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_0
    const-string/jumbo v5, "location_enabled"

    invoke-static {v5, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_1

    if-eqz v1, :cond_2

    .line 85
    :cond_1
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v5, v0, v3, v1}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :goto_0
    const/4 v5, 0x1

    return v5

    .line 87
    :cond_2
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0
.end method

.method public getAdaptor()Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    return-object v0
.end method

.method protected getMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;
    .locals 3
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "date"    # Ljava/lang/String;
    .param p3, "city"    # Ljava/lang/String;
    .param p4, "state"    # Ljava/lang/String;
    .param p5, "highTemp"    # Ljava/lang/String;
    .param p6, "curTemp"    # Ljava/lang/String;
    .param p7, "weatherCode"    # I
    .param p8, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    .prologue
    .line 306
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_date_display:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getOneDaySpokenMsg(Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "weatherInfoUtil"    # Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
    .param p3, "day"    # Ljava/lang/String;
    .param p4, "date"    # Ljava/lang/String;
    .param p5, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;
    .param p6, "generalMsg"    # Ljava/lang/String;

    .prologue
    .line 264
    invoke-direct/range {p0 .. p5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getOneDaySpokenMsgInternal(Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;
    .locals 3
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "forecast"    # Ljava/lang/String;
    .param p3, "date"    # Ljava/lang/String;
    .param p4, "city"    # Ljava/lang/String;
    .param p5, "state"    # Ljava/lang/String;
    .param p6, "highTemp"    # Ljava/lang/String;
    .param p7, "curTemp"    # Ljava/lang/String;
    .param p8, "weatherCode"    # I
    .param p9, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    .prologue
    .line 302
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_current:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p7, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.NoData"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "prompt":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 102
    .end local v0    # "prompt":Ljava/lang/String;
    :goto_0
    return-void

    .line 99
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRequestFailed()V
    .locals 2

    .prologue
    .line 137
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->showFailure()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 141
    return-void

    .line 139
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v0
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 2
    .param p1, "prompt"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 434
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$3;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$3;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 440
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 441
    return-void
.end method

.method public onRequestScheduled()V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public onResponseReceived()V
    .locals 3

    .prologue
    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v0

    .line 111
    .local v0, "weatherDetails":Lcom/vlingo/core/internal/weather/WeatherElement;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->showFailure()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 123
    return-void

    .line 118
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->showSuccess()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 121
    .end local v0    # "weatherDetails":Lcom/vlingo/core/internal/weather/WeatherElement;
    :catchall_0
    move-exception v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v1
.end method

.method protected showFailure()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$2;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$2;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 299
    return-void
.end method
