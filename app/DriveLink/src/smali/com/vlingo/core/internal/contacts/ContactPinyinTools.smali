.class public Lcom/vlingo/core/internal/contacts/ContactPinyinTools;
.super Ljava/lang/Object;
.source "ContactPinyinTools.java"


# static fields
.field private static final PINYIN_DATA_FILE:Ljava/lang/String; = "ChinesePinyinTableWithTones.txt"


# instance fields
.field private context:Landroid/content/Context;

.field private pinyinTranscriptionList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Character;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->context:Landroid/content/Context;

    .line 31
    return-void
.end method

.method private initPinyinTranscriptionList()V
    .locals 10

    .prologue
    .line 34
    const/4 v4, 0x0

    .line 36
    .local v4, "is":Ljava/io/InputStream;
    :try_start_0
    iget-object v8, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v8

    const-string/jumbo v9, "ChinesePinyinTableWithTones.txt"

    invoke-virtual {v8, v9}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 41
    :goto_0
    if-eqz v4, :cond_2

    .line 42
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    iput-object v8, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionList:Ljava/util/List;

    .line 43
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-direct {v8, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 46
    .local v1, "br":Ljava/io/BufferedReader;
    :goto_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .local v6, "line":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 47
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 48
    .local v3, "insideList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Character;>;"
    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v5, :cond_1

    aget-char v7, v0, v2

    .line 49
    .local v7, "symbol":C
    const/16 v8, 0xa

    if-eq v8, v7, :cond_0

    const/16 v8, 0xd

    if-eq v8, v7, :cond_0

    if-eqz v7, :cond_0

    .line 50
    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 48
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 53
    .end local v7    # "symbol":C
    :cond_1
    iget-object v8, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionList:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 55
    .end local v0    # "arr$":[C
    .end local v2    # "i$":I
    .end local v3    # "insideList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Character;>;"
    .end local v5    # "len$":I
    .end local v6    # "line":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 63
    .end local v1    # "br":Ljava/io/BufferedReader;
    :cond_2
    return-void

    .line 37
    :catch_1
    move-exception v8

    goto :goto_0
.end method


# virtual methods
.method public destroyPinyinTranscriptionList()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionList:Ljava/util/List;

    .line 89
    return-void
.end method

.method public findNormalizedCharacterList(C)Ljava/util/List;
    .locals 4
    .param p1, "symbol"    # C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionList:Ljava/util/List;

    if-nez v3, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->initPinyinTranscriptionList()V

    .line 73
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v2, "samePhoneticsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionList:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 75
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 76
    .local v1, "insideList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "insideList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 82
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_3
    return-object v2
.end method
