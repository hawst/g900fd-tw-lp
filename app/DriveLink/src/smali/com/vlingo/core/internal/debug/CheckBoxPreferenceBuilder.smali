.class public Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;
.super Lcom/vlingo/core/internal/debug/PreferenceBuilder;
.source "CheckBoxPreferenceBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
        "<",
        "Landroid/preference/CheckBoxPreference;",
        ">;"
    }
.end annotation


# static fields
.field private static showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder$1;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder$1;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;-><init>()V

    .line 10
    sget-object v0, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->setShowAsSummary(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V

    .line 11
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "setting"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;-><init>(Ljava/lang/String;)V

    .line 15
    sget-object v0, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->setShowAsSummary(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V

    .line 16
    return-void
.end method


# virtual methods
.method public register(Landroid/preference/PreferenceActivity;)Landroid/preference/CheckBoxPreference;
    .locals 3
    .param p1, "activity"    # Landroid/preference/PreferenceActivity;

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 23
    .local v0, "pref":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->getDefault()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->getDefault()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 26
    :cond_0
    return-object v0
.end method

.method public bridge synthetic register(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;
    .locals 1
    .param p1, "x0"    # Landroid/preference/PreferenceActivity;

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    return-object v0
.end method
