.class public Lcom/vlingo/core/internal/logging/EventLog;
.super Ljava/lang/Object;
.source "EventLog.java"


# instance fields
.field private actionList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;"
        }
    .end annotation
.end field

.field private actionsLogMessage:Ljava/lang/String;

.field private asrLogMessage:Ljava/lang/String;

.field private dialogState:[B

.field private nluLogMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V
    .locals 1
    .param p1, "result"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getResultString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/logging/EventLog;->asrLogMessage:Ljava/lang/String;

    .line 33
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getActions()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/logging/EventLog;->actionList:Ljava/util/List;

    .line 34
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getDialogState()[B

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/logging/EventLog;->dialogState:[B

    .line 35
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/logging/EventLog;->buildNluLogMessages(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    .line 36
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/logging/EventLog;->buildActionsLogMessage(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    .line 37
    return-void
.end method

.method private buildActionsLogMessage(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V
    .locals 9
    .param p1, "result"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .prologue
    .line 40
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 41
    .local v5, "sb":Ljava/lang/StringBuilder;
    if-nez p1, :cond_0

    .line 44
    const-string/jumbo v7, "no result"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/vlingo/core/internal/logging/EventLog;->nluLogMessage:Ljava/lang/String;

    .line 61
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getActions()Ljava/util/List;

    move-result-object v2

    .line 49
    .local v2, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 50
    .local v1, "actionIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/sdk/recognition/VLAction;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 51
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLAction;

    .line 52
    .local v0, "action":Lcom/vlingo/sdk/recognition/VLAction;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLAction;->getParameterNames()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 54
    .local v4, "paramName":Ljava/lang/String;
    const/4 v7, 0x0

    invoke-static {v0, v4, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 55
    .local v6, "value":Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 56
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 60
    .end local v0    # "action":Lcom/vlingo/sdk/recognition/VLAction;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "paramName":Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/vlingo/core/internal/logging/EventLog;->actionsLogMessage:Ljava/lang/String;

    goto :goto_0
.end method

.method private buildNluLogMessages(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V
    .locals 8
    .param p1, "result"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .prologue
    .line 64
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .local v3, "sb":Ljava/lang/StringBuilder;
    if-nez p1, :cond_0

    .line 68
    const-string/jumbo v6, "no result"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/logging/EventLog;->nluLogMessage:Ljava/lang/String;

    .line 121
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getParseGroup()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    move-result-object v1

    .line 74
    .local v1, "parseGroup":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    if-nez v1, :cond_1

    .line 77
    const-string/jumbo v6, "no parseGroup"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/logging/EventLog;->nluLogMessage:Ljava/lang/String;

    goto :goto_0

    .line 82
    :cond_1
    const-string/jumbo v6, "Parse type: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->getParseType()Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "parseType":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 85
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->getParseType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :goto_1
    const-string/jumbo v6, ", Slots:"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->getTags()Ljava/util/Vector;

    move-result-object v4

    .line 95
    .local v4, "tags":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    if-nez v4, :cond_3

    .line 98
    const-string/jumbo v6, "no slots"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/logging/EventLog;->nluLogMessage:Ljava/lang/String;

    goto :goto_0

    .line 89
    .end local v4    # "tags":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    :cond_2
    const-string/jumbo v6, "no parseType"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 103
    .restart local v4    # "tags":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    :cond_3
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_2
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v6

    if-ge v0, v6, :cond_6

    .line 104
    invoke-virtual {v4, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    .line 105
    .local v5, "tempTag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " Slot "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    if-eqz v5, :cond_5

    .line 107
    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    const-string/jumbo v6, " value : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->getRecResults()Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 110
    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->getRecResults()Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 103
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 112
    :cond_4
    const-string/jumbo v6, " no value"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 117
    :cond_5
    const-string/jumbo v6, "no slot"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 120
    .end local v5    # "tempTag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/logging/EventLog;->nluLogMessage:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static getLog()Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    .line 147
    .local v0, "toReturn":Lcom/vlingo/core/internal/logging/Logger;
    return-object v0
.end method


# virtual methods
.method public getActionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vlingo/core/internal/logging/EventLog;->actionList:Ljava/util/List;

    return-object v0
.end method

.method public getActionsLogMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vlingo/core/internal/logging/EventLog;->actionsLogMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getAsrLogMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/core/internal/logging/EventLog;->asrLogMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogState()[B
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/vlingo/core/internal/logging/EventLog;->dialogState:[B

    return-object v0
.end method

.method public getNluLogMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/vlingo/core/internal/logging/EventLog;->nluLogMessage:Ljava/lang/String;

    return-object v0
.end method
