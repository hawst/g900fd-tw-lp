.class public abstract Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;
.super Ljava/lang/Object;
.source "SafeReaderServiceProxyBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;,
        Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;
    }
.end annotation


# static fields
.field protected static final IDLE_DELAY:I = 0x5

.field private static final IDLE_DELAY_INTERVAL:I = 0x7530

.field protected static final MSG_BASE:I = 0x14

.field protected static final RUN_QUEUE:I = 0x4

.field protected static final SERVICE_CONNECTED:I = 0x1

.field protected static final SERVICE_DISCONNECTED:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected final context:Landroid/content/Context;

.field private final handler:Landroid/os/Handler;

.field private isConnected:Z

.field private isConnecting:Z

.field private lastCommandTime:J

.field private pendingRequestQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private sb:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;

.field private service:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

.field private serviceToken:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-boolean v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnecting:Z

    .line 44
    iput-boolean v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnected:Z

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->lastCommandTime:J

    .line 49
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    .line 50
    iput-object p1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->context:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$1;-><init>(Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;
    .locals 4
    .param p1, "contextParam"    # Landroid/content/Context;
    .param p2, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    .line 236
    new-instance v0, Landroid/content/ContextWrapper;

    invoke-direct {v0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 237
    .local v0, "cw":Landroid/content/ContextWrapper;
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->getSafeReaderServiceClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 238
    new-instance v1, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;

    invoke-direct {v1, p2}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;-><init>(Landroid/content/ServiceConnection;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->sb:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;

    .line 239
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->getSafeReaderServiceClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->sb:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    new-instance v1, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;

    invoke-direct {v1, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;-><init>(Landroid/content/ContextWrapper;)V

    .line 246
    :goto_0
    return-object v1

    .line 245
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Failed to bind to service"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private declared-synchronized queue(IIILjava/lang/Object;)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->queue(Landroid/os/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    monitor-exit p0

    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized queue(Landroid/os/Message;)V
    .locals 1
    .param p1, "m"    # Landroid/os/Message;

    .prologue
    .line 96
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :cond_0
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected allowsDelayDisconnect()Z
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized connect()V
    .locals 2

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->service:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnecting:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnected:Z

    if-nez v0, :cond_0

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnecting:Z

    .line 106
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->context:Landroid/content/Context;

    new-instance v1, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$2;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$2;-><init>(Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;)V

    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :cond_0
    monitor-exit p0

    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized execute(I)V
    .locals 3
    .param p1, "what"    # I

    .prologue
    .line 69
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->execute(IIILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    monitor-exit p0

    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized execute(IIILjava/lang/Object;)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnected:Z

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 82
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnecting:Z

    if-nez v0, :cond_2

    .line 83
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->connect()V

    .line 86
    :cond_2
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 87
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->queue(IIILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized execute(ILjava/lang/Object;)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 73
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->execute(IIILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized executeQueue()V
    .locals 3

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->service:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 138
    iget-object v2, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    .line 139
    .local v1, "queuedMessage":Landroid/os/Message;
    iget-object v2, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 137
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "queuedMessage":Landroid/os/Message;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 141
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method protected abstract getMessageName(Landroid/os/Message;)Ljava/lang/String;
.end method

.method protected getMessageNameBase(Landroid/os/Message;)Ljava/lang/String;
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 146
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 156
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->getMessageName(Landroid/os/Message;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 148
    :pswitch_1
    const-string/jumbo v0, "SERVICE_CONNECTED"

    goto :goto_0

    .line 150
    :pswitch_2
    const-string/jumbo v0, "SERVICE_DISCONNECTED"

    goto :goto_0

    .line 152
    :pswitch_3
    const-string/jumbo v0, "RUN_QUEUE"

    goto :goto_0

    .line 154
    :pswitch_4
    const-string/jumbo v0, "IDLE_DELAY"

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected getSafeReaderServiceClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    const-class v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    return-object v0
.end method

.method protected declared-synchronized handleBase(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v6, 0x7530

    const/4 v5, 0x5

    .line 164
    monitor-enter p0

    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 202
    :pswitch_0
    iget-boolean v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 204
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->service:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->service:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-virtual {p0, p1, v1}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handleMessageForService(Landroid/os/Message;Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224
    :cond_0
    :goto_0
    :try_start_2
    iget-boolean v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnected:Z

    if-eqz v1, :cond_1

    iget v1, p1, Landroid/os/Message;->what:I

    if-eq v1, v5, :cond_1

    .line 225
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->lastCommandTime:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 230
    :cond_1
    monitor-exit p0

    return-void

    .line 169
    :pswitch_1
    :try_start_3
    iget-boolean v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnecting:Z

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    iput-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->service:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    .line 171
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnected:Z

    .line 172
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnecting:Z

    .line 173
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->executeQueue()V

    .line 174
    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;

    const/4 v2, 0x5

    const-wide/16 v3, 0x7530

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 181
    :pswitch_2
    :try_start_4
    iget-boolean v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnected:Z

    if-eqz v1, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->executeQueue()V

    goto :goto_0

    .line 188
    :pswitch_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->release()V

    goto :goto_0

    .line 191
    :pswitch_4
    iget-boolean v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnected:Z

    if-eqz v1, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->allowsDelayDisconnect()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->lastCommandTime:J

    sub-long/2addr v1, v3

    cmp-long v1, v1, v6

    if-lez v1, :cond_2

    .line 195
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->release()V

    goto :goto_0

    .line 197
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;

    const/4 v2, 0x5

    const-wide/16 v3, 0x7530

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 164
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method protected abstract handleMessageForService(Landroid/os/Message;Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V
.end method

.method public declared-synchronized release()V
    .locals 2

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;

    if-eqz v0, :cond_1

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnected:Z

    .line 124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->isConnecting:Z

    .line 125
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 128
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 129
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->unbindFromService(Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;)V

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->service:Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    .line 132
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->lastCommandTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :cond_1
    monitor-exit p0

    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public unbindFromService(Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;)V
    .locals 3
    .param p1, "token"    # Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;

    .prologue
    .line 250
    if-nez p1, :cond_0

    .line 251
    sget-object v1, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Trying to unbind with null token"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v0, p1, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceToken;->mWrappedContext:Landroid/content/ContextWrapper;

    .line 258
    .local v0, "cw":Landroid/content/ContextWrapper;
    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->sb:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;

    if-nez v1, :cond_1

    .line 259
    sget-object v1, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Trying to unbind for unknown Context"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 262
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->sb:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    .line 263
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->sb:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;

    goto :goto_0
.end method
