.class public Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
.super Ljava/lang/Object;
.source "MessageReadoutType.java"


# instance fields
.field private address:Ljava/lang/String;

.field private displayName:Ljava/lang/String;

.field private messagesFromSender:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/LinkedList;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "messagesFromSender":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->address:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    .line 33
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 93
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 97
    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 98
    .local v0, "readoutObj":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->address:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->address:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 101
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->displayName:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->displayName:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 104
    :cond_3
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    if-nez v2, :cond_7

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    if-nez v2, :cond_0

    .line 107
    :cond_4
    const/4 v1, 0x1

    goto :goto_0

    .line 98
    :cond_5
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->address:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->address:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 101
    :cond_6
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->displayName:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->displayName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    .line 104
    :cond_7
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayNameOrAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->displayName:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->address:Ljava/lang/String;

    .line 87
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->displayName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getMessageCount()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    .line 43
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMessagesFromSender()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getMostRecentTimestamp()J
    .locals 6

    .prologue
    .line 48
    const-wide/16 v2, 0x0

    .line 49
    .local v2, "mostRecent":J
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    if-eqz v4, :cond_1

    .line 50
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 51
    .local v1, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getTimeStamp()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    .line 52
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getTimeStamp()J

    move-result-wide v2

    goto :goto_0

    .line 56
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_1
    return-wide v2
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 112
    const/4 v0, 0x1

    .line 113
    .local v0, "hash":I
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->address:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->address:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit8 v0, v1, 0x11

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->displayName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 117
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->displayName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 119
    :cond_1
    return v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->address:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->displayName:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setMessagesFromSender(Ljava/util/LinkedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "messagesFromSender":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->messagesFromSender:Ljava/util/LinkedList;

    .line 77
    return-void
.end method
