.class public Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;
.super Ljava/util/Timer;
.source "TimerSingleton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/TimerSingleton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimerSingletonTimer"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 33
    const-string/jumbo v0, "TimerSingleton"

    invoke-direct {p0, v0}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    .line 34
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method protected cancelTimer()V
    .locals 0

    .prologue
    .line 44
    invoke-super {p0}, Ljava/util/Timer;->cancel()V

    .line 45
    return-void
.end method
