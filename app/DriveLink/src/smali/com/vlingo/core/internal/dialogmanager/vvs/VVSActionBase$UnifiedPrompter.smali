.class public Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
.super Ljava/lang/Object;
.source "VVSActionBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "UnifiedPrompter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;


# direct methods
.method protected constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 1
    .param p1, "id"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 127
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 1
    .param p1, "id"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p2, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 135
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 136
    return-void
.end method

.method public showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 1
    .param p1, "id"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p2, "autoListen"    # Z
    .param p3, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 147
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 148
    return-void
.end method

.method public showSystemTurn(Ljava/lang/String;)V
    .locals 2
    .param p1, "prompt"    # Ljava/lang/String;

    .prologue
    .line 131
    const/4 v1, 0x0

    const/4 v0, 0x0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    invoke-virtual {p0, p1, v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 132
    return-void
.end method

.method public showSystemTurn(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 2
    .param p1, "prompt"    # Ljava/lang/String;
    .param p2, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p1, v1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 140
    return-void
.end method

.method public showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "speech"    # Ljava/lang/String;
    .param p3, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 144
    return-void
.end method

.method public showSystemTurn(Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 1
    .param p1, "prompt"    # Ljava/lang/String;
    .param p2, "autoListen"    # Z
    .param p3, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;

    invoke-virtual {v0, p1, p1, p2, p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 152
    return-void
.end method
