.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhraseSpotter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PSBatteryChangedReceiver"
.end annotation


# instance fields
.field private isRegistered:Z

.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V
    .locals 1

    .prologue
    .line 375
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 376
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->isRegistered:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p2, "x1"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;

    .prologue
    .line 375
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 408
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 410
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 411
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging(Landroid/content/Intent;)Z
    invoke-static {v2, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Landroid/content/Intent;)Z

    move-result v1

    .line 412
    .local v1, "charging":Z
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging:Z
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z

    move-result v2

    if-eq v2, v1, :cond_0

    .line 415
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging:Z
    invoke-static {v2, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$402(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z

    .line 420
    .end local v1    # "charging":Z
    :cond_0
    return-void
.end method

.method register(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 379
    iget-boolean v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->isRegistered:Z

    if-eqz v1, :cond_0

    .line 382
    const/4 v0, 0x0

    .line 387
    :goto_0
    return-object v0

    .line 385
    :cond_0
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->BATTERY_CHANGED_FILTER:Landroid/content/IntentFilter;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1000()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p1, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 386
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->isRegistered:Z

    goto :goto_0
.end method

.method unregister(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 391
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->isRegistered:Z

    if-nez v0, :cond_0

    .line 404
    :goto_0
    return-void

    .line 397
    :cond_0
    :try_start_0
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 398
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->isRegistered:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 400
    :catch_0
    move-exception v0

    goto :goto_0
.end method
