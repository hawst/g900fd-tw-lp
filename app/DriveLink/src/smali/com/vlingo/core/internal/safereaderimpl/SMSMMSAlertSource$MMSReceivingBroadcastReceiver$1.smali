.class Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver$1;
.super Ljava/lang/Object;
.source "SMSMMSAlertSource.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 190
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getNewMMSSafereaderAlerts(I)Ljava/util/LinkedList;

    move-result-object v0

    .line 192
    .local v0, "mmsAlerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 193
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 197
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 198
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 201
    :cond_0
    return-void
.end method
