.class public Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;
.super Ljava/lang/Object;
.source "SMSUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/SMSUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TextMessage"
.end annotation


# instance fields
.field public final address:Ljava/lang/String;

.field public final body:Ljava/lang/String;

.field public final contactId:J

.field public final date:J

.field public final id:J

.field public final lookupKey:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final subject:Ljava/lang/String;

.field public final type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "body"    # Ljava/lang/String;
    .param p4, "subject"    # Ljava/lang/String;
    .param p5, "lookupKey"    # Ljava/lang/String;
    .param p6, "id"    # J
    .param p8, "contactId"    # J
    .param p10, "date"    # J
    .param p12, "type"    # Ljava/lang/String;

    .prologue
    .line 596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 597
    iput-object p1, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->address:Ljava/lang/String;

    .line 598
    iput-object p2, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->name:Ljava/lang/String;

    .line 599
    iput-object p3, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->body:Ljava/lang/String;

    .line 600
    iput-object p4, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->subject:Ljava/lang/String;

    .line 601
    iput-object p5, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->lookupKey:Ljava/lang/String;

    .line 602
    iput-wide p6, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->id:J

    .line 603
    iput-wide p8, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->contactId:J

    .line 604
    iput-wide p10, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->date:J

    .line 605
    iput-object p12, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->type:Ljava/lang/String;

    .line 606
    return-void
.end method
