.class public Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;
.super Ljava/lang/Object;
.source "ContactDBUtilManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;
    .locals 2

    .prologue
    .line 20
    const/4 v0, 0x0

    .line 21
    .local v0, "contactDBUtil":Lcom/vlingo/core/internal/contacts/IContactDBUtil;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->usingNuanceNormalizationTable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 22
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getInstance()Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    move-result-object v0

    .line 26
    :goto_0
    return-object v0

    .line 24
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getInstance()Lcom/vlingo/core/internal/contacts/OldContactDBUtil;

    move-result-object v0

    goto :goto_0
.end method

.method public static getRemoteContentProviderVersion()I
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 55
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 56
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getVersionContentUri()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 57
    .local v8, "versionCursor":Landroid/database/Cursor;
    const/4 v7, -0x1

    .line 58
    .local v7, "version":I
    if-eqz v8, :cond_0

    .line 59
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 61
    :cond_0
    return v7
.end method

.method public static usingNuanceNormalizationTable()Z
    .locals 6

    .prologue
    .line 30
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 31
    .local v1, "context":Landroid/content/Context;
    const/4 v3, 0x1

    .line 32
    .local v3, "useCustomTable":Z
    if-eqz v1, :cond_1

    .line 33
    const/4 v0, 0x0

    .line 35
    .local v0, "client":Landroid/content/ContentProviderClient;
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 36
    if-nez v0, :cond_0

    .line 37
    const/4 v3, 0x0

    .line 43
    :cond_0
    if-eqz v0, :cond_1

    .line 44
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 48
    .end local v0    # "client":Landroid/content/ContentProviderClient;
    :cond_1
    :goto_0
    return v3

    .line 39
    .restart local v0    # "client":Landroid/content/ContentProviderClient;
    :catch_0
    move-exception v2

    .line 40
    .local v2, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    .line 41
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_0

    .line 43
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_2

    .line 44
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_2
    throw v4
.end method
