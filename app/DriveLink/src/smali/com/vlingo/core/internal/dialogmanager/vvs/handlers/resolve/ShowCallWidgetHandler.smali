.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowCallWidgetHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private allMatches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field protected forwardRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

.field protected messages:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private placeCall(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;Z)V
    .locals 4
    .param p1, "recipient"    # Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    .param p2, "videoCall"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 131
    if-nez p1, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->turnOnSpeakerIfRequired()V

    .line 136
    move-object v0, p0

    .line 139
    .local v0, "dma":Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-static {v1, v2, v0, v3, p2}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Z)V

    .line 140
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 141
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->DIAL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->sendAcceptedText(Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V

    goto :goto_0
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 157
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 158
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 159
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 160
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 150
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 151
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 152
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 153
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 21
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 53
    invoke-super/range {p0 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 55
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v18

    const-string/jumbo v19, "voicedial"

    invoke-virtual/range {v18 .. v19}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 57
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v18

    sget-object v19, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface/range {v18 .. v19}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/List;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->allMatches:Ljava/util/List;

    .line 59
    const-string/jumbo v18, "recipient"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v16

    .line 61
    .local v16, "recipients":Ljava/lang/String;
    const/4 v15, 0x0

    .line 62
    .local v15, "recipientContactData":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v12, 0x0

    .line 64
    .local v12, "rawPhoneNumbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v13, 0x0

    .line 65
    .local v13, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-static/range {v16 .. v16}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_0

    const-string/jumbo v18, "{}"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 66
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->allMatches:Ljava/util/List;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->allMatches:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_2

    .line 68
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->allMatches:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRecipientContacts(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v15

    .line 69
    invoke-static/range {v16 .. v16}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 83
    :cond_0
    :goto_0
    if-eqz v15, :cond_3

    .line 84
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 85
    .local v14, "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v14, :cond_1

    .line 86
    iget-object v4, v14, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 87
    .local v4, "address":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 88
    new-instance v13, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .end local v13    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    iget-object v0, v14, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    move-object/from16 v18, v0

    iget-object v0, v14, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v13, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v13    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    goto :goto_1

    .line 70
    .end local v4    # "address":Ljava/lang/String;
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v14    # "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :catch_0
    move-exception v5

    .line 71
    .local v5, "e":Lorg/json/JSONException;
    sget-object v18, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v19, "Error extracting json contacts and numbers from recipients parameter"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/16 v18, 0x0

    .line 123
    .end local v5    # "e":Lorg/json/JSONException;
    :goto_2
    return v18

    .line 76
    :cond_2
    :try_start_1
    invoke-static/range {v16 .. v16}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v12

    goto :goto_0

    .line 77
    :catch_1
    move-exception v5

    .line 78
    .restart local v5    # "e":Lorg/json/JSONException;
    sget-object v18, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v19, "Error extracting json contacts and numbers from recipients parameter"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/16 v18, 0x0

    goto :goto_2

    .line 93
    .end local v5    # "e":Lorg/json/JSONException;
    :cond_3
    if-eqz v12, :cond_4

    .line 94
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .restart local v6    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 95
    .local v11, "number":Ljava/lang/String;
    new-instance v13, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .end local v13    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    const-string/jumbo v18, ""

    move-object/from16 v0, v18

    invoke-direct {v13, v0, v11}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v13    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    goto :goto_3

    .line 98
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v11    # "number":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->forwardRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 99
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->forwardRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 102
    :cond_5
    const-string/jumbo v18, "mode"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v10

    .line 103
    .local v10, "mode":Ljava/lang/String;
    invoke-static {v10}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_8

    const-string/jumbo v18, "videophone"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_8

    const/16 v17, 0x1

    .line 105
    .local v17, "videoCall":Z
    :goto_4
    const-string/jumbo v18, "confirm"

    const/16 v19, 0x1

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v7

    .line 106
    .local v7, "isConfirm":Z
    const-string/jumbo v18, "execute"

    const/16 v19, 0x1

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v8

    .line 107
    .local v8, "isExecute":Z
    if-nez v7, :cond_9

    if-eqz v8, :cond_9

    const/4 v9, 0x1

    .line 109
    .local v9, "isExecuteStep":Z
    :goto_5
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v18

    if-eqz v18, :cond_6

    .line 110
    sget-object v18, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCallWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-interface {v0, v1, v2, v13, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 113
    :cond_6
    const-string/jumbo v18, "execute"

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 117
    :try_start_2
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v13, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->placeCall(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;Z)V

    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 123
    :cond_7
    :goto_6
    const/16 v18, 0x0

    goto/16 :goto_2

    .line 103
    .end local v7    # "isConfirm":Z
    .end local v8    # "isExecute":Z
    .end local v9    # "isExecuteStep":Z
    .end local v17    # "videoCall":Z
    :cond_8
    const/16 v17, 0x0

    goto :goto_4

    .line 107
    .restart local v7    # "isConfirm":Z
    .restart local v8    # "isExecute":Z
    .restart local v17    # "videoCall":Z
    :cond_9
    const/4 v9, 0x0

    goto :goto_5

    .line 119
    .restart local v9    # "isExecuteStep":Z
    :catch_2
    move-exception v5

    .line 120
    .restart local v5    # "e":Lorg/json/JSONException;
    sget-object v18, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->TAG:Ljava/lang/String;

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method

.method protected turnOnSpeakerIfRequired()V
    .locals 1

    .prologue
    .line 127
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/PhoneUtil;->turnOnSpeakerphoneIfRequired(Landroid/content/Context;)V

    .line 128
    return-void
.end method
