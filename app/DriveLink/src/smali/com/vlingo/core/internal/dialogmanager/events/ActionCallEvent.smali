.class public Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;
.super Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
.source "ActionCallEvent.java"


# instance fields
.field private final FIELDID_FIELD_NAME:Ljava/lang/String;

.field private final NAME:Ljava/lang/String;

.field private final SENDER_NAME_FIELD_NAME:Ljava/lang/String;

.field private final SENDER_PHONE_FIELD_NAME:Ljava/lang/String;

.field private fieldId:Ljava/lang/String;

.field private senderName:Ljava/lang/String;

.field private senderPhone:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "senderName"    # Ljava/lang/String;
    .param p2, "senderPhone"    # Ljava/lang/String;
    .param p3, "fieldId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;-><init>(ZZZ)V

    .line 12
    const-string/jumbo v0, "action-call"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;->NAME:Ljava/lang/String;

    .line 13
    const-string/jumbo v0, "sender.name"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;->SENDER_NAME_FIELD_NAME:Ljava/lang/String;

    .line 14
    const-string/jumbo v0, "sender.phone"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;->SENDER_PHONE_FIELD_NAME:Ljava/lang/String;

    .line 15
    const-string/jumbo v0, "fieldid"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;->FIELDID_FIELD_NAME:Ljava/lang/String;

    .line 24
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;->senderName:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;->senderPhone:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;->fieldId:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "action-call"

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 3

    .prologue
    .line 36
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v1, "action-call"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 37
    .local v0, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    const-string/jumbo v1, "sender.name"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;->senderName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 38
    const-string/jumbo v1, "sender.phone"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;->senderPhone:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 39
    const-string/jumbo v1, "fieldid"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;->fieldId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 41
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v1

    return-object v1
.end method
