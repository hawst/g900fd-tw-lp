.class public Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;
.super Ljava/lang/Object;
.source "VVSActionBase.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    }
.end annotation


# instance fields
.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field protected turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    return-void
.end method

.method protected static varargs getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 230
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public actionAbort()V
    .locals 0

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->reset()V

    .line 96
    return-void
.end method

.method public actionFail(Ljava/lang/String;)V
    .locals 0
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->reset()V

    .line 88
    return-void
.end method

.method public actionSuccess()V
    .locals 0

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->reset()V

    .line 80
    return-void
.end method

.method public clone()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->clone()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;

    move-result-object v0

    return-object v0
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 112
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 113
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->resetEmbeddedNews()V

    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method protected getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;
    .locals 1
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/DMActionType;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 247
    .local p2, "aClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p1, p2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    return-object v0
.end method

.method protected getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;
    .locals 2
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .param p3, "actionListener"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/DMActionType;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 266
    .local p2, "aClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-static {p1, v0, p3, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    return-object v0
.end method

.method protected getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    return-object v0
.end method

.method protected getNBestData()Lcom/vlingo/sdk/recognition/NBestData;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getNBestData()Lcom/vlingo/sdk/recognition/NBestData;

    move-result-object v0

    return-object v0
.end method

.method protected reset()V
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 104
    return-void
.end method

.method protected resetEmbeddedNews()V
    .locals 2

    .prologue
    .line 308
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 309
    .local v0, "df":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getWidgetSpecificProperties(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 310
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V

    .line 312
    :cond_0
    return-void
.end method

.method protected sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V
    .locals 1
    .param p1, "acceptedText"    # Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getGUttId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->setGUttId(Ljava/lang/String;)V

    .line 290
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->setDialogGuid(Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getTurnNumber()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->setDialogTurn(I)V

    .line 292
    invoke-static {}, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;->getInstance()Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 294
    :cond_0
    return-void
.end method

.method protected sendAcceptedText(Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V
    .locals 7
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    .prologue
    .line 278
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_0

    .line 279
    invoke-static {}, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;->getInstance()Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;

    move-result-object v6

    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getGUID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getTurnNumber()I

    move-result v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getGUttId()Ljava/lang/String;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 281
    :cond_0
    return-void
.end method

.method protected sendAcceptedText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "acceptedType"    # Ljava/lang/String;
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_0

    .line 303
    invoke-static {}, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;->getInstance()Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;

    move-result-object v7

    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getGUID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getTurnNumber()I

    move-result v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getGUttId()Ljava/lang/String;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 305
    :cond_0
    return-void
.end method

.method public setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 70
    return-void
.end method

.method public setTurn(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 0
    .param p1, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 65
    return-void
.end method

.method protected showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;Lcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 4
    .param p1, "vlingoText"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p2, "spokenText"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    const/4 v3, 0x0

    .line 184
    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p2, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v3, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 185
    return-void
.end method

.method protected showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;Lcom/vlingo/core/internal/ResourceIdProvider$string;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 3
    .param p1, "vlingoText"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p2, "spokenText"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p3, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    const/4 v1, 0x0

    .line 188
    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2, p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 189
    return-void
.end method

.method protected showSystemTurn(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "vlingoText"    # Ljava/lang/String;
    .param p2, "spokenText"    # Ljava/lang/String;

    .prologue
    .line 194
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 195
    return-void
.end method

.method protected showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 1
    .param p1, "vlingoText"    # Ljava/lang/String;
    .param p2, "spokenText"    # Ljava/lang/String;
    .param p3, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 198
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 199
    return-void
.end method

.method protected showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 1
    .param p1, "vlingoText"    # Ljava/lang/String;
    .param p2, "spokenText"    # Ljava/lang/String;
    .param p3, "autoListen"    # Z
    .param p4, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0, p4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 208
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 210
    if-eqz p3, :cond_1

    .line 211
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->startReco()Z

    .line 214
    :cond_1
    return-void
.end method

.method protected showUserTurn(Ljava/lang/String;)V
    .locals 1
    .param p1, "userText"    # Ljava/lang/String;

    .prologue
    .line 169
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showUserText(Ljava/lang/String;)V

    .line 172
    :cond_0
    return-void
.end method

.method protected unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;)V

    return-object v0
.end method
