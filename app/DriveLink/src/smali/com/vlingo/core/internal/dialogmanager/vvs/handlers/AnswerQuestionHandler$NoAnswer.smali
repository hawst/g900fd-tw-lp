.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;
.source "AnswerQuestionHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NoAnswer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction",
        "<",
        "Lcom/vlingo/core/internal/questions/Answer;",
        ">;",
        "Ljava/lang/Runnable;",
        "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;"
    }
.end annotation


# instance fields
.field private question:Ljava/lang/String;

.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 1
    .param p2, "question"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p4, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 242
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    .line 243
    const/4 v0, 0x0

    invoke-direct {p0, v0, p3, p4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;-><init>(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 244
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->question:Ljava/lang/String;

    .line 245
    return-void
.end method


# virtual methods
.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intentArg"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 273
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->question:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 274
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getUserText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->question:Ljava/lang/String;

    .line 277
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;-><init>()V

    .line 278
    .local v0, "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 279
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->question:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->getWebSearchURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 280
    .local v1, "url":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->executeSearchIntentFromURL(Ljava/lang/String;)V

    .line 281
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->reset()V
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$1000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)V

    .line 282
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 249
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 266
    :goto_0
    return-void

    .line 251
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->question:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 252
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->question:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 255
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->useGoogleSearch()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 256
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_GOOGLE_NOW_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->question:Ljava/lang/String;

    aput-object v4, v3, v5

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$500(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 257
    .local v1, "prompt":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 258
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->question:Ljava/lang/String;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->handleWithGoogleNow(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$700(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;Ljava/lang/String;)V

    goto :goto_0

    .line 260
    .end local v1    # "prompt":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getRandomAnswer()Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Ljava/lang/String;

    move-result-object v1

    .line 261
    .restart local v1    # "prompt":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_search_web_label_button:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v3, v5, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$900(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "buttonText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowButton:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4, v0, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0
.end method
