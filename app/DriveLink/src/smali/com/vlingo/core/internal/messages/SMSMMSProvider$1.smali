.class Lcom/vlingo/core/internal/messages/SMSMMSProvider$1;
.super Ljava/lang/Object;
.source "SMSMMSProvider.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/messages/SMSMMSProvider;

.field final synthetic val$ascending:Z


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/messages/SMSMMSProvider;Z)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider$1;->this$0:Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    iput-boolean p2, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider$1;->val$ascending:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Lcom/vlingo/core/internal/messages/SMSMMSAlert;)I
    .locals 6
    .param p1, "o1"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .param p2, "o2"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 178
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getTimeStamp()J

    move-result-wide v0

    .line 179
    .local v0, "t1":J
    invoke-virtual {p2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getTimeStamp()J

    move-result-wide v2

    .line 180
    .local v2, "t2":J
    iget-boolean v4, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider$1;->val$ascending:Z

    if-eqz v4, :cond_0

    .line 181
    sub-long v4, v0, v2

    long-to-int v4, v4

    .line 183
    :goto_0
    return v4

    :cond_0
    sub-long v4, v2, v0

    long-to-int v4, v4

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 175
    check-cast p1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/messages/SMSMMSProvider$1;->compare(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Lcom/vlingo/core/internal/messages/SMSMMSAlert;)I

    move-result v0

    return v0
.end method
