.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;
.super Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$BaseState;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Stopping"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    .locals 0

    .prologue
    .line 654
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$BaseState;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    return-void
.end method


# virtual methods
.method public is(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Z
    .locals 1
    .param p1, "state"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    .prologue
    .line 658
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->STOPPING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEnter()V
    .locals 9

    .prologue
    .line 686
    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;
    invoke-static {v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 687
    :try_start_0
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ".start()"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v5, v5, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    if-nez v5, :cond_1

    .line 689
    const-class v5, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v7, "Stopping.onEnter: mRecordThread is null!"

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v7, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v5, v7}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 700
    :cond_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 701
    invoke-super {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$BaseState;->onEnter()V

    .line 702
    return-void

    .line 692
    :cond_1
    :try_start_1
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Record thread state is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v8, v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    invoke-virtual {v8}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v5, v5, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    .line 695
    .local v4, "stackTraces":[Ljava/lang/StackTraceElement;
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v7, "Record thread stack  ..."

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/StackTraceElement;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 697
    .local v1, "entry":Ljava/lang/StackTraceElement;
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "PhraseSpotterControl.Record thread: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 700
    .end local v0    # "arr$":[Ljava/lang/StackTraceElement;
    .end local v1    # "entry":Ljava/lang/StackTraceElement;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "stackTraces":[Ljava/lang/StackTraceElement;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method public onExit()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 663
    invoke-super {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$BaseState;->onExit()V

    .line 664
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 665
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 666
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .line 668
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getDialogFlowTaskRegulator()Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    move-result-object v0

    .line 669
    .local v0, "regulator":Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
    if-eqz v0, :cond_1

    .line 670
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v1, v2, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 671
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setDialogFlowTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 673
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getListener()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotterStopped()V

    .line 681
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->release()V

    .line 682
    return-void
.end method

.method public start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 3
    .param p1, "phraseSpotterParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "listener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 707
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start: Attempting to start while in state \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'; will restart after fully stopped."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 710
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v2, 0x1

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRestartWhenIdle:Z
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$002(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Z)Z

    .line 711
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 712
    const-class v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "Stopping.start: mRecordThread is null."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v2, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 716
    :cond_0
    monitor-exit v1

    .line 717
    return-void

    .line 716
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 719
    const-string/jumbo v0, "Stopping"

    return-object v0
.end method
