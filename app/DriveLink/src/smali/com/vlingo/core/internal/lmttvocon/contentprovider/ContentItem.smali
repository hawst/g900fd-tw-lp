.class public abstract Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
.super Ljava/lang/Object;
.source "ContentItem.java"


# instance fields
.field private final id:J


# direct methods
.method public constructor <init>(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-wide p1, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->id:J

    .line 10
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 33
    if-nez p1, :cond_1

    .line 40
    :cond_0
    :goto_0
    return v1

    .line 36
    :cond_1
    instance-of v2, p1, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 39
    check-cast v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    .line 40
    .local v0, "item":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getChecksum()I

    move-result v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getChecksum()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public abstract getChecksum()I
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 13
    iget-wide v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->id:J

    return-wide v0
.end method

.method public abstract getOrthographies()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getChecksum()I

    move-result v0

    return v0
.end method

.method protected final varargs makeChecksum([Ljava/lang/Object;)I
    .locals 8
    .param p1, "objects"    # [Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    .line 21
    if-nez p1, :cond_0

    .line 28
    :goto_0
    return v6

    .line 24
    :cond_0
    const/16 v1, 0x11

    .line 25
    .local v1, "code":I
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 26
    .local v4, "obj":Ljava/lang/Object;
    mul-int/lit8 v7, v1, 0x1f

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v5

    :goto_2
    add-int v1, v7, v5

    .line 25
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v5, v6

    .line 26
    goto :goto_2

    .end local v4    # "obj":Ljava/lang/Object;
    :cond_2
    move v6, v1

    .line 28
    goto :goto_0
.end method
