.class public Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;
.super Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;
.source "SocialAcceptedText.java"


# instance fields
.field private site:Ljava/lang/String;

.field private status:Ljava/lang/String;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "site"    # Ljava/lang/String;
    .param p2, "status"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;-><init>(Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->site:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->status:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->text:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method protected getAcceptedTextXML()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 34
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "<AcceptedText pt=\"social:add\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 35
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->status:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 36
    const-string/jumbo v1, "<Tag u=\"status\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 37
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 38
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 40
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->text:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 41
    const-string/jumbo v1, "<Tag u=\"text\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 43
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 45
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->site:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 46
    const-string/jumbo v1, "<Tag u=\"site\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 47
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->site:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 48
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 50
    :cond_2
    const-string/jumbo v1, "</AcceptedText>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 51
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "SocialAcceptedText [site="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->site:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
