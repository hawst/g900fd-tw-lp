.class public final Lcom/vlingo/core/internal/audio/TTSEngine;
.super Ljava/lang/Object;
.source "TTSEngine.java"

# interfaces
.implements Lcom/vlingo/core/facade/tts/ITTSEngine;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;,
        Lcom/vlingo/core/internal/audio/TTSEngine$PronunciationManager;,
        Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;,
        Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;,
        Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;
    }
.end annotation


# static fields
.field private static final ALLOW_LOCAL_TTS:Z = true

.field private static final ALLOW_NETWORK_TTS:Z = true

.field private static final LOCAL_TTS_RENDER_TIMEOUT_MS:I = 0x7530

.field private static final LOCAL_TTS_SPEECH_RATE_NORMAL:F = 1.0f

.field private static final SVOX_REQUIRED_ENGINE_NAME:Ljava/lang/String; = "SVox"

.field private static final TAG:Ljava/lang/String;

.field private static final THREAD_WAIT_TIME_MS:I = 0xfa

.field public static final TTS_ENGINE_PACKAGE_NAME_GOOGLE:Ljava/lang/String; = "com.google.android.tts"

.field public static final TTS_ENGINE_PACKAGE_NAME_SAMSUNG:Ljava/lang/String; = "com.samsung.SMT"

.field private static final TTS_RATE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

.field private static final TTS_TYPE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

.field private static final TTS_VOICE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

.field private static forceSpeechRate:F

.field private static instance:Lcom/vlingo/core/internal/audio/TTSEngine;

.field private static pronunciationManager:Lcom/vlingo/core/internal/audio/TTSEngine$PronunciationManager;

.field private static synthMutex:Ljava/lang/Object;


# instance fields
.field private final context:Landroid/content/Context;

.field private isAboutToDestroy:Z

.field private localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

.field private final localTTSEngineLockObject:Ljava/lang/Object;

.field private tts:Landroid/speech/tts/TextToSpeech;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-class v0, Lcom/vlingo/core/internal/audio/TTSEngine;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;

    .line 54
    const-string/jumbo v0, "tts_local_force_speech_rate"

    const/high16 v1, -0x40800000    # -1.0f

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v0

    sput v0, Lcom/vlingo/core/internal/audio/TTSEngine;->forceSpeechRate:F

    .line 73
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/audio/TTSEngine;->synthMutex:Ljava/lang/Object;

    .line 75
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/audio/TTSEngine;->instance:Lcom/vlingo/core/internal/audio/TTSEngine;

    .line 678
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;->FEMALE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    sput-object v0, Lcom/vlingo/core/internal/audio/TTSEngine;->TTS_VOICE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    .line 679
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->PLAIN:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    sput-object v0, Lcom/vlingo/core/internal/audio/TTSEngine;->TTS_TYPE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    .line 680
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->NORMAL:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    sput-object v0, Lcom/vlingo/core/internal/audio/TTSEngine;->TTS_RATE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngineLockObject:Ljava/lang/Object;

    .line 69
    iput-object v1, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->isAboutToDestroy:Z

    .line 76
    iput-object v1, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->tts:Landroid/speech/tts/TextToSpeech;

    .line 88
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->context:Landroid/content/Context;

    .line 89
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->getDefaultSDKRequest(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/audio/TTSEngine;)Lcom/vlingo/core/internal/audio/TTSLocalEngine;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/TTSEngine;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    return-object v0
.end method

.method private destroyLocalTTSEngine()V
    .locals 3

    .prologue
    .line 142
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    if-eqz v1, :cond_1

    .line 143
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->stop()I

    move-result v0

    .line 144
    .local v0, "ret":I
    if-eqz v0, :cond_0

    .line 145
    sget-object v1, Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "destroyLocalTTSEngine call to stop() failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->shutdown()V

    .line 148
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    .line 150
    .end local v0    # "ret":I
    :cond_1
    return-void
.end method

.method private engineExists(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Ljava/lang/String;)Z
    .locals 4
    .param p1, "engine"    # Lcom/vlingo/core/internal/audio/TTSLocalEngine;
    .param p2, "engineName"    # Ljava/lang/String;

    .prologue
    .line 380
    invoke-virtual {p1}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->getEngines()Ljava/util/List;

    move-result-object v1

    .line 381
    .local v1, "engines":Ljava/util/List;, "Ljava/util/List<Landroid/speech/tts/TextToSpeech$EngineInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 383
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/speech/tts/TextToSpeech$EngineInfo;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 384
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/speech/tts/TextToSpeech$EngineInfo;

    .line 385
    .local v0, "engineInfo":Landroid/speech/tts/TextToSpeech$EngineInfo;
    iget-object v3, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 386
    const/4 v3, 0x1

    .line 389
    .end local v0    # "engineInfo":Landroid/speech/tts/TextToSpeech$EngineInfo;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private fetchLocalTTS(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSRequest;)Ljava/lang/String;
    .locals 19
    .param p1, "contextParam"    # Landroid/content/Context;
    .param p2, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;

    .prologue
    .line 543
    const-string/jumbo v13, "local_tts"

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v13, v1}, Lcom/vlingo/core/internal/audio/TTSCache;->getCachedTTSPath(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 544
    .local v2, "audioFilePath":Ljava/lang/String;
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 606
    .end local v2    # "audioFilePath":Ljava/lang/String;
    .local v3, "audioFilePath":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 548
    .end local v3    # "audioFilePath":Ljava/lang/String;
    .restart local v2    # "audioFilePath":Ljava/lang/String;
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->getLocalTTSEngine()Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    move-result-object v6

    .line 549
    .local v6, "local":Lcom/vlingo/core/internal/audio/TTSLocalEngine;
    if-nez v6, :cond_1

    move-object v3, v2

    .line 550
    .end local v2    # "audioFilePath":Ljava/lang/String;
    .restart local v3    # "audioFilePath":Ljava/lang/String;
    goto :goto_0

    .line 552
    .end local v3    # "audioFilePath":Ljava/lang/String;
    .restart local v2    # "audioFilePath":Ljava/lang/String;
    :cond_1
    sget-object v14, Lcom/vlingo/core/internal/audio/TTSEngine;->synthMutex:Ljava/lang/Object;

    monitor-enter v14

    .line 553
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/core/internal/audio/TTSRequest;->getTextToSpeak()Ljava/lang/String;

    move-result-object v10

    .line 554
    .local v10, "textToSynthesize":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/core/internal/audio/TTSEngine;->pronunciationManager:Lcom/vlingo/core/internal/audio/TTSEngine$PronunciationManager;

    if-eqz v13, :cond_2

    .line 555
    sget-object v13, Lcom/vlingo/core/internal/audio/TTSEngine;->pronunciationManager:Lcom/vlingo/core/internal/audio/TTSEngine$PronunciationManager;

    invoke-interface {v13, v10}, Lcom/vlingo/core/internal/audio/TTSEngine$PronunciationManager;->prepareText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 558
    :cond_2
    const-string/jumbo v13, "local_tts"

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v13, v1}, Lcom/vlingo/core/internal/audio/TTSCache;->getTempFile(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Landroid/content/Context;)Ljava/io/File;

    move-result-object v13

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    .line 562
    .local v7, "localFilePath":Ljava/lang/String;
    new-instance v5, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;-><init>(Lcom/vlingo/core/internal/audio/TTSEngine;)V

    .line 563
    .local v5, "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;
    invoke-virtual {v6, v5}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    .line 564
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 565
    .local v8, "myHashRender":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v13, "utteranceId"

    invoke-virtual {v8, v13, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    const-string/jumbo v13, "com.samsung.SMT.KEY_PARAM"

    const-string/jumbo v15, "DISABLE_NOTICE_POPUP"

    invoke-virtual {v8, v13, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    invoke-virtual {v6, v10, v8, v7}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->synthesizeToFile(Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;)I

    move-result v9

    .line 570
    .local v9, "result":I
    if-nez v9, :cond_4

    .line 571
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    const-wide/16 v17, 0x7530

    add-long v11, v15, v17

    .line 572
    .local v11, "timeoutTime":J
    :goto_1
    invoke-virtual {v5}, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;->isComplete()Z

    move-result v13

    if-nez v13, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v15

    cmp-long v13, v15, v11

    if-gez v13, :cond_3

    .line 573
    const-wide/16 v15, 0xfa

    :try_start_1
    invoke-static/range {v15 .. v16}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v13

    goto :goto_1

    .line 575
    :cond_3
    :try_start_2
    invoke-virtual {v5}, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;->isComplete()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v13

    if-eqz v13, :cond_5

    .line 578
    move-object v2, v7

    .line 582
    :try_start_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/audio/TTSEngine;->trimWaveFile(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 589
    :goto_2
    :try_start_4
    move-object/from16 v0, p2

    iget-boolean v13, v0, Lcom/vlingo/core/internal/audio/TTSRequest;->isCacheable:Z

    if-eqz v13, :cond_4

    .line 590
    const-string/jumbo v13, "local_tts"

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v2, v13, v1}, Lcom/vlingo/core/internal/audio/TTSCache;->cacheTTSRequest(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 603
    .end local v11    # "timeoutTime":J
    :cond_4
    :goto_3
    monitor-exit v14

    move-object v3, v2

    .line 606
    .end local v2    # "audioFilePath":Ljava/lang/String;
    .restart local v3    # "audioFilePath":Ljava/lang/String;
    goto/16 :goto_0

    .line 583
    .end local v3    # "audioFilePath":Ljava/lang/String;
    .restart local v2    # "audioFilePath":Ljava/lang/String;
    .restart local v11    # "timeoutTime":J
    :catch_1
    move-exception v4

    .line 585
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 603
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;
    .end local v7    # "localFilePath":Ljava/lang/String;
    .end local v8    # "myHashRender":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v9    # "result":I
    .end local v10    # "textToSynthesize":Ljava/lang/String;
    .end local v11    # "timeoutTime":J
    :catchall_0
    move-exception v13

    monitor-exit v14
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v13

    .line 596
    .restart local v5    # "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;
    .restart local v7    # "localFilePath":Ljava/lang/String;
    .restart local v8    # "myHashRender":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v9    # "result":I
    .restart local v10    # "textToSynthesize":Ljava/lang/String;
    .restart local v11    # "timeoutTime":J
    :cond_5
    :try_start_5
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->destroyLocalTTSEngine()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method

.method private static fetchNetworkTTS(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSRequest;)Ljava/lang/String;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;

    .prologue
    .line 202
    const-string/jumbo v6, "network_tts"

    invoke-static {p1, v6, p0}, Lcom/vlingo/core/internal/audio/TTSCache;->getCachedTTSPath(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "audioFilePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 242
    .end local v0    # "audioFilePath":Ljava/lang/String;
    .local v1, "audioFilePath":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 212
    .end local v1    # "audioFilePath":Ljava/lang/String;
    .restart local v0    # "audioFilePath":Ljava/lang/String;
    :cond_0
    new-instance v2, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;

    invoke-direct {v2, p1, p0}, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;-><init>(Lcom/vlingo/core/internal/audio/TTSRequest;Landroid/content/Context;)V

    .line 213
    .local v2, "task":Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;
    sget-object v7, Lcom/vlingo/core/internal/audio/TTSEngine;->synthMutex:Ljava/lang/Object;

    monitor-enter v7

    .line 214
    :try_start_0
    new-instance v3, Ljava/lang/Thread;

    invoke-direct {v3, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 215
    .local v3, "thread":Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 216
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-string/jumbo v6, "network_tts_timeout"

    const/16 v10, 0x1388

    invoke-static {v6, v10}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v6

    int-to-long v10, v6

    add-long v4, v8, v10

    .line 217
    .local v4, "timeoutTime":J
    :goto_1
    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isComplete()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    cmp-long v6, v8, v4

    if-gez v6, :cond_1

    .line 218
    const-wide/16 v8, 0xfa

    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v6

    goto :goto_1

    .line 220
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isComplete()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 223
    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isSuccess()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 224
    iget-object v0, v2, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->audioFilePath:Ljava/lang/String;

    .line 225
    iget-boolean v6, p1, Lcom/vlingo/core/internal/audio/TTSRequest;->isCacheable:Z

    if-eqz v6, :cond_2

    .line 226
    const-string/jumbo v6, "network_tts"

    invoke-static {p1, v0, v6, p0}, Lcom/vlingo/core/internal/audio/TTSCache;->cacheTTSRequest(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 239
    :cond_2
    :goto_2
    monitor-exit v7

    move-object v1, v0

    .line 242
    .end local v0    # "audioFilePath":Ljava/lang/String;
    .restart local v1    # "audioFilePath":Ljava/lang/String;
    goto :goto_0

    .line 237
    .end local v1    # "audioFilePath":Ljava/lang/String;
    .restart local v0    # "audioFilePath":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/sdk/VLSdk;->getTextToSpeech()Lcom/vlingo/sdk/tts/VLTextToSpeech;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/sdk/tts/VLTextToSpeech;->cancel()V

    goto :goto_2

    .line 239
    .end local v3    # "thread":Ljava/lang/Thread;
    .end local v4    # "timeoutTime":J
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6
.end method

.method private static getDefaultSDKRequest(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
    .locals 3
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 684
    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    invoke-direct {v0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;-><init>()V

    .line 685
    .local v0, "builder":Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
    invoke-virtual {v0, p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->text(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .line 686
    sget-object v1, Lcom/vlingo/core/internal/audio/TTSEngine;->TTS_VOICE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->voice(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .line 687
    sget-object v1, Lcom/vlingo/core/internal/audio/TTSEngine;->TTS_TYPE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->type(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .line 688
    sget-object v1, Lcom/vlingo/core/internal/audio/TTSEngine;->TTS_RATE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->speechRate(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .line 689
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->language(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .line 691
    invoke-virtual {v0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->build()Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;

    move-result-object v1

    return-object v1
.end method

.method public static getForceSpeechRate()F
    .locals 1

    .prologue
    .line 108
    sget v0, Lcom/vlingo/core/internal/audio/TTSEngine;->forceSpeechRate:F

    return v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/TTSEngine;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    sget-object v0, Lcom/vlingo/core/internal/audio/TTSEngine;->instance:Lcom/vlingo/core/internal/audio/TTSEngine;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lcom/vlingo/core/internal/audio/TTSEngine;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/audio/TTSEngine;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/vlingo/core/internal/audio/TTSEngine;->instance:Lcom/vlingo/core/internal/audio/TTSEngine;

    .line 84
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/audio/TTSEngine;->instance:Lcom/vlingo/core/internal/audio/TTSEngine;

    return-object v0
.end method

.method private getLocalTTSEngine()Lcom/vlingo/core/internal/audio/TTSLocalEngine;
    .locals 13

    .prologue
    const/4 v11, 0x0

    .line 416
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    .line 417
    .local v0, "currentLocale":Ljava/util/Locale;
    const/4 v1, 0x0

    .line 420
    .local v1, "engineIsOk":Z
    const-string/jumbo v10, "tts_local_required_engine"

    invoke-static {v10, v11}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 421
    .local v6, "required":Z
    const-string/jumbo v10, "tts_local_ignore_use_speech_rate"

    invoke-static {v10, v11}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 422
    .local v4, "ignoreUserSpeechRate":Z
    const-string/jumbo v10, "tts_local_tts_fallback_engine"

    const-string/jumbo v11, "com.google.android.tts"

    invoke-static {v10, v11}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 423
    .local v3, "fallbackEngineName":Ljava/lang/String;
    iget-object v11, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngineLockObject:Ljava/lang/Object;

    monitor-enter v11

    .line 424
    :try_start_0
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    if-eqz v10, :cond_0

    .line 425
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-virtual {v10}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->getLanguage()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Locale;

    .line 426
    .local v7, "tempLocale":Ljava/util/Locale;
    if-nez v7, :cond_1

    .line 429
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v10, v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->setLanguage(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Ljava/util/Locale;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 430
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->destroyLocalTTSEngine()V

    .line 443
    .end local v7    # "tempLocale":Ljava/util/Locale;
    :cond_0
    :goto_0
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    if-eqz v10, :cond_3

    .line 446
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v10, v4}, Lcom/vlingo/core/internal/audio/TTSEngine;->processSpeechRate(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Z)V

    .line 447
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    monitor-exit v11

    .line 530
    :goto_1
    return-object v10

    .line 433
    .restart local v7    # "tempLocale":Ljava/util/Locale;
    :cond_1
    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v7}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 437
    :cond_2
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v10, v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->setLanguage(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Ljava/util/Locale;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 438
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->destroyLocalTTSEngine()V

    goto :goto_0

    .line 529
    .end local v7    # "tempLocale":Ljava/util/Locale;
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 449
    :cond_3
    :try_start_1
    new-instance v5, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;

    invoke-direct {v5, p0}, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;-><init>(Lcom/vlingo/core/internal/audio/TTSEngine;)V

    .line 450
    .local v5, "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v10

    sget-object v12, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_required_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v10, v12}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v9

    .line 452
    .local v9, "ttsPackageName":Ljava/lang/String;
    if-eqz v6, :cond_4

    invoke-static {}, Lcom/vlingo/core/internal/util/DeviceWorkarounds;->shouldIgnoreRequiredSamsungTTSEngine()Z

    move-result v10

    if-nez v10, :cond_4

    .line 455
    new-instance v5, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;

    .end local v5    # "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
    invoke-direct {v5, p0}, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;-><init>(Lcom/vlingo/core/internal/audio/TTSEngine;)V

    .line 458
    .restart local v5    # "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
    new-instance v10, Landroid/speech/tts/TextToSpeech;

    iget-object v12, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->context:Landroid/content/Context;

    invoke-direct {v10, v12, v5, v9}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V

    iput-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->tts:Landroid/speech/tts/TextToSpeech;

    .line 459
    new-instance v10, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;

    iget-object v12, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-direct {v10, v12}, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;-><init>(Landroid/speech/tts/TextToSpeech;)V

    iput-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    .line 460
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v10, v5, v4, v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->initializeTextToSpeech(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;ZLjava/util/Locale;)Z

    move-result v1

    .line 461
    if-nez v1, :cond_7

    .line 462
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "required engine "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v12, " initialization not success"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 463
    .local v2, "errorMessage":Ljava/lang/String;
    sget-object v10, Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;

    invoke-static {v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->destroyLocalTTSEngine()V

    .line 465
    const/4 v5, 0x0

    .line 477
    .end local v2    # "errorMessage":Ljava/lang/String;
    :goto_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v10

    sget-object v12, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_fallback_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v10, v12}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v8

    .line 478
    .local v8, "ttsFallbackPackageName":Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 479
    new-instance v5, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;

    .end local v5    # "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
    invoke-direct {v5, p0}, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;-><init>(Lcom/vlingo/core/internal/audio/TTSEngine;)V

    .line 482
    .restart local v5    # "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
    new-instance v10, Landroid/speech/tts/TextToSpeech;

    iget-object v12, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->context:Landroid/content/Context;

    invoke-direct {v10, v12, v5, v8}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V

    iput-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->tts:Landroid/speech/tts/TextToSpeech;

    .line 483
    new-instance v10, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;

    iget-object v12, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-direct {v10, v12}, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;-><init>(Landroid/speech/tts/TextToSpeech;)V

    iput-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    .line 484
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v10, v5, v4, v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->initializeTextToSpeech(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;ZLjava/util/Locale;)Z

    move-result v1

    .line 485
    if-nez v1, :cond_9

    .line 488
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->destroyLocalTTSEngine()V

    .line 489
    const/4 v5, 0x0

    .line 505
    .end local v8    # "ttsFallbackPackageName":Ljava/lang/String;
    :cond_4
    :goto_3
    const/4 v5, 0x0

    .line 506
    new-instance v5, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;

    .end local v5    # "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
    invoke-direct {v5, p0}, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;-><init>(Lcom/vlingo/core/internal/audio/TTSEngine;)V

    .line 507
    .restart local v5    # "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
    new-instance v10, Landroid/speech/tts/TextToSpeech;

    iget-object v12, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->context:Landroid/content/Context;

    invoke-direct {v10, v12, v5}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->tts:Landroid/speech/tts/TextToSpeech;

    .line 508
    new-instance v10, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;

    iget-object v12, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-direct {v10, v12}, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;-><init>(Landroid/speech/tts/TextToSpeech;)V

    iput-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    .line 509
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v10, v5, v4, v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->initializeTextToSpeech(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;ZLjava/util/Locale;)Z

    move-result v1

    .line 510
    if-nez v1, :cond_5

    .line 513
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->destroyLocalTTSEngine()V

    .line 517
    :cond_5
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    if-nez v10, :cond_6

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 518
    new-instance v5, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;

    .end local v5    # "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
    invoke-direct {v5, p0}, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;-><init>(Lcom/vlingo/core/internal/audio/TTSEngine;)V

    .line 519
    .restart local v5    # "listener":Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
    new-instance v10, Landroid/speech/tts/TextToSpeech;

    iget-object v12, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->context:Landroid/content/Context;

    invoke-direct {v10, v12, v5, v3}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V

    iput-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->tts:Landroid/speech/tts/TextToSpeech;

    .line 520
    new-instance v10, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;

    iget-object v12, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-direct {v10, v12}, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;-><init>(Landroid/speech/tts/TextToSpeech;)V

    iput-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    .line 523
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v10, v5, v4, v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->initializeTextToSpeech(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;ZLjava/util/Locale;)Z

    move-result v1

    .line 524
    if-nez v1, :cond_6

    .line 529
    :cond_6
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    goto/16 :goto_1

    .line 466
    :cond_7
    :try_start_2
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v10, v9}, Lcom/vlingo/core/internal/audio/TTSEngine;->engineExists(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 469
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->destroyLocalTTSEngine()V

    .line 470
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 474
    :cond_8
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    monitor-exit v11

    goto/16 :goto_1

    .line 490
    .restart local v8    # "ttsFallbackPackageName":Ljava/lang/String;
    :cond_9
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v10, v8}, Lcom/vlingo/core/internal/audio/TTSEngine;->engineExists(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    .line 493
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->destroyLocalTTSEngine()V

    .line 494
    const/4 v5, 0x0

    goto :goto_3

    .line 495
    :cond_a
    if-eqz v6, :cond_4

    invoke-static {}, Lcom/vlingo/core/internal/util/DeviceWorkarounds;->shouldIgnoreRequiredSamsungTTSEngine()Z

    move-result v10

    if-nez v10, :cond_4

    .line 498
    iget-object v10, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method

.method private initializeTextToSpeech(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;ZLjava/util/Locale;)Z
    .locals 5
    .param p1, "engine"    # Lcom/vlingo/core/internal/audio/TTSLocalEngine;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
    .param p3, "ignoreUserSpeechRate"    # Z
    .param p4, "currentLocale"    # Ljava/util/Locale;

    .prologue
    const/4 v1, 0x0

    .line 351
    const/4 v0, 0x0

    .line 356
    .local v0, "timeWaited":I
    :goto_0
    invoke-virtual {p2}, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->isAboutToDestroy:Z

    if-nez v2, :cond_0

    .line 358
    const-wide/16 v2, 0xfa

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    add-int/lit16 v0, v0, 0xfa

    goto :goto_0

    .line 363
    :cond_0
    invoke-virtual {p2}, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;->isSuccess()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->isAboutToDestroy:Z

    if-eqz v2, :cond_2

    .line 364
    sget-object v2, Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "engine initialization not success after waiting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :cond_1
    :goto_1
    return v1

    .line 367
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->getLanguage()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    .line 372
    :cond_3
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v2, p4}, Lcom/vlingo/core/internal/audio/TTSEngine;->setLanguage(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Ljava/util/Locale;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 375
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    invoke-direct {p0, v1, p3}, Lcom/vlingo/core/internal/audio/TTSEngine;->processSpeechRate(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Z)V

    .line 376
    const/4 v1, 0x1

    goto :goto_1

    .line 360
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private processSpeechRate(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Z)V
    .locals 2
    .param p1, "engine"    # Lcom/vlingo/core/internal/audio/TTSLocalEngine;
    .param p2, "ignoreUserSpeechRate"    # Z

    .prologue
    .line 112
    if-eqz p1, :cond_0

    .line 113
    invoke-static {}, Lcom/vlingo/core/internal/audio/TTSEngine;->getForceSpeechRate()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 114
    invoke-static {}, Lcom/vlingo/core/internal/audio/TTSEngine;->getForceSpeechRate()F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->setSpeechRate(F)V

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    if-eqz p2, :cond_0

    .line 116
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->setSpeechRate(F)V

    goto :goto_0
.end method

.method public static setForceSpeechRate(Landroid/content/Context;F)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "speechRate"    # F

    .prologue
    .line 100
    sget v0, Lcom/vlingo/core/internal/audio/TTSEngine;->forceSpeechRate:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 101
    sput p1, Lcom/vlingo/core/internal/audio/TTSEngine;->forceSpeechRate:F

    .line 102
    const-string/jumbo v0, "tts_local_force_speech_rate"

    sget v1, Lcom/vlingo/core/internal/audio/TTSEngine;->forceSpeechRate:F

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setFloat(Ljava/lang/String;F)V

    .line 103
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Landroid/content/Context;Z)I

    .line 105
    :cond_0
    return-void
.end method

.method private setLanguage(Lcom/vlingo/core/internal/audio/TTSLocalEngine;Ljava/util/Locale;)Z
    .locals 4
    .param p1, "engine"    # Lcom/vlingo/core/internal/audio/TTSLocalEngine;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v1, 0x1

    .line 393
    invoke-virtual {p1, p2}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->setLanguage(Ljava/util/Locale;)I

    move-result v0

    .line 396
    .local v0, "ret":I
    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 412
    :cond_0
    :goto_0
    return v1

    .line 401
    :cond_1
    if-eq v0, v1, :cond_0

    .line 406
    if-eqz v0, :cond_0

    .line 411
    sget-object v1, Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setLanguage returned "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setPronunciationManager(Lcom/vlingo/core/internal/audio/TTSEngine$PronunciationManager;)V
    .locals 0
    .param p0, "pronunciationManager"    # Lcom/vlingo/core/internal/audio/TTSEngine$PronunciationManager;

    .prologue
    .line 695
    sput-object p0, Lcom/vlingo/core/internal/audio/TTSEngine;->pronunciationManager:Lcom/vlingo/core/internal/audio/TTSEngine$PronunciationManager;

    .line 696
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->isAboutToDestroy:Z

    .line 133
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngineLockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 134
    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->destroyLocalTTSEngine()V

    .line 135
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->isAboutToDestroy:Z

    .line 139
    return-void

    .line 135
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 537
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 538
    return-void
.end method

.method public getFilePathForTTSRequest(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSRequest;)Ljava/lang/String;
    .locals 3
    .param p1, "contextParam"    # Landroid/content/Context;
    .param p2, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;

    .prologue
    .line 176
    if-nez p1, :cond_0

    .line 177
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "context passed null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 179
    :cond_0
    if-nez p2, :cond_1

    .line 180
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "TTSRequest passed null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 182
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 188
    const/4 v0, 0x0

    .line 190
    .local v0, "audioFilePath":Ljava/lang/String;
    if-nez v0, :cond_2

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string/jumbo v1, "use_network_tts"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 191
    invoke-static {p1, p2}, Lcom/vlingo/core/internal/audio/TTSEngine;->fetchNetworkTTS(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSRequest;)Ljava/lang/String;

    move-result-object v0

    .line 193
    :cond_2
    if-nez v0, :cond_3

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 194
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/audio/TTSEngine;->fetchLocalTTS(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSRequest;)Ljava/lang/String;

    move-result-object v0

    .line 196
    :cond_3
    return-object v0
.end method

.method public getTTS()Landroid/speech/tts/TextToSpeech;
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->tts:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method public onApplicationLanguageChanged()V
    .locals 2

    .prologue
    .line 327
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/core/internal/audio/TTSEngine$1;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/audio/TTSEngine$1;-><init>(Lcom/vlingo/core/internal/audio/TTSEngine;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 333
    return-void
.end method

.method public prepare()V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->getLocalTTSEngine()Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    .line 162
    return-void
.end method

.method public setTtsSpeechRate(F)V
    .locals 1
    .param p1, "speechRate"    # F

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine;->context:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/audio/TTSEngine;->setForceSpeechRate(Landroid/content/Context;F)V

    .line 125
    return-void
.end method

.method public trimWaveFile(Ljava/lang/String;)V
    .locals 14
    .param p1, "audioFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/high16 v13, -0x1000000

    .line 617
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 618
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 619
    const/4 v3, 0x0

    .line 620
    .local v3, "fileInputStream":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 622
    .local v5, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 624
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .local v4, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v9

    long-to-int v9, v9

    new-array v0, v9, [B

    .line 626
    .local v0, "buffer":[B
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I

    .line 628
    invoke-virtual {v2}, Ljava/io/File;->length()J
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v9

    const-wide/16 v11, 0x2c

    cmp-long v9, v9, v11

    if-gez v9, :cond_2

    .line 667
    if-eqz v4, :cond_0

    .line 668
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 670
    :cond_0
    if-eqz v5, :cond_1

    .line 671
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 675
    .end local v0    # "buffer":[B
    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    return-void

    .line 634
    .restart local v0    # "buffer":[B
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    :cond_2
    const/4 v9, 0x4

    :try_start_2
    aget-byte v9, v0, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x0

    const/4 v10, 0x5

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x8

    or-int/2addr v9, v10

    const/4 v10, 0x6

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x10

    or-int/2addr v9, v10

    const/4 v10, 0x7

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x18

    or-int v7, v9, v10

    .line 638
    .local v7, "fileSize":I
    const/16 v9, 0x28

    aget-byte v9, v0, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x0

    const/16 v10, 0x29

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x8

    or-int/2addr v9, v10

    const/16 v10, 0x2a

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x10

    or-int/2addr v9, v10

    const/16 v10, 0x2b

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x18

    or-int v1, v9, v10

    .line 643
    .local v1, "dataSize":I
    :goto_1
    const/4 v9, 0x2

    if-ge v9, v1, :cond_3

    .line 644
    add-int/lit8 v9, v7, -0x2

    aget-byte v9, v0, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x0

    add-int/lit8 v10, v7, -0x1

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x8

    or-int v8, v9, v10

    .line 646
    .local v8, "v":I
    const/16 v9, 0xaa

    if-ge v9, v8, :cond_5

    const v9, 0xff66

    if-ge v8, v9, :cond_5

    .line 651
    .end local v8    # "v":I
    :cond_3
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 652
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 653
    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v6, "fileOutputStream":Ljava/io/FileOutputStream;
    const/4 v9, 0x4

    and-int/lit16 v10, v7, 0xff

    shr-int/lit8 v10, v10, 0x0

    int-to-byte v10, v10

    :try_start_3
    aput-byte v10, v0, v9

    .line 654
    const/4 v9, 0x5

    const v10, 0xff00

    and-int/2addr v10, v7

    shr-int/lit8 v10, v10, 0x8

    int-to-byte v10, v10

    aput-byte v10, v0, v9

    .line 655
    const/4 v9, 0x6

    const/high16 v10, 0xff0000

    and-int/2addr v10, v7

    shr-int/lit8 v10, v10, 0x10

    int-to-byte v10, v10

    aput-byte v10, v0, v9

    .line 656
    const/4 v9, 0x7

    and-int v10, v7, v13

    shr-int/lit8 v10, v10, 0x18

    int-to-byte v10, v10

    aput-byte v10, v0, v9

    .line 657
    const/16 v9, 0x28

    and-int/lit16 v10, v1, 0xff

    shr-int/lit8 v10, v10, 0x0

    int-to-byte v10, v10

    aput-byte v10, v0, v9

    .line 658
    const/16 v9, 0x29

    const v10, 0xff00

    and-int/2addr v10, v1

    shr-int/lit8 v10, v10, 0x8

    int-to-byte v10, v10

    aput-byte v10, v0, v9

    .line 659
    const/16 v9, 0x2a

    const/high16 v10, 0xff0000

    and-int/2addr v10, v1

    shr-int/lit8 v10, v10, 0x10

    int-to-byte v10, v10

    aput-byte v10, v0, v9

    .line 660
    const/16 v9, 0x2b

    and-int v10, v1, v13

    shr-int/lit8 v10, v10, 0x18

    int-to-byte v10, v10

    aput-byte v10, v0, v9

    .line 661
    const/4 v9, 0x0

    invoke-virtual {v6, v0, v9, v7}, Ljava/io/FileOutputStream;->write([BII)V

    .line 662
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 667
    if-eqz v4, :cond_4

    .line 668
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 670
    :cond_4
    if-eqz v6, :cond_1

    .line 671
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_0

    .line 643
    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "v":I
    :cond_5
    add-int/lit8 v1, v1, -0x2

    add-int/lit8 v7, v7, -0x2

    goto/16 :goto_1

    .line 664
    .end local v0    # "buffer":[B
    .end local v1    # "dataSize":I
    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v7    # "fileSize":I
    .end local v8    # "v":I
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v9

    .line 667
    :goto_2
    if-eqz v3, :cond_6

    .line 668
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 670
    :cond_6
    if-eqz v5, :cond_1

    .line 671
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_0

    .line 667
    :catchall_0
    move-exception v9

    :goto_3
    if-eqz v3, :cond_7

    .line 668
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 670
    :cond_7
    if-eqz v5, :cond_8

    .line 671
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    :cond_8
    throw v9

    .line 667
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v9

    move-object v3, v4

    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "dataSize":I
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "fileSize":I
    :catchall_2
    move-exception v9

    move-object v5, v6

    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 664
    .end local v0    # "buffer":[B
    .end local v1    # "dataSize":I
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v7    # "fileSize":I
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v9

    move-object v3, v4

    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "dataSize":I
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "fileSize":I
    :catch_2
    move-exception v9

    move-object v5, v6

    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_2
.end method
