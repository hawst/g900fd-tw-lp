.class abstract enum Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;
.super Ljava/lang/Enum;
.source "LMTTItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4408
    name = "DBUpdateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

.field public static final enum DEL:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

.field public static final enum INS:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

.field public static final enum NOOP:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

.field public static final enum UP:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType$1;

    const-string/jumbo v1, "INS"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->INS:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    .line 49
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType$2;

    const-string/jumbo v1, "DEL"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->DEL:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    .line 54
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType$3;

    const-string/jumbo v1, "UP"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->UP:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    .line 59
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType$4;

    const-string/jumbo v1, "NOOP"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->NOOP:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    .line 43
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->INS:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->DEL:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->UP:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->NOOP:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->$VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/vlingo/core/internal/lmtt/LMTTItem$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$1;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 43
    const-class v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->$VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    return-object v0
.end method


# virtual methods
.method abstract dbUpdate(Lcom/vlingo/core/internal/lmtt/LMTTItem;Landroid/database/sqlite/SQLiteDatabase;)V
.end method
