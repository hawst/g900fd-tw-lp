.class public Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceBootStart;
.super Landroid/content/BroadcastReceiver;
.source "VoconLmttServiceBootStart.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    .line 17
    invoke-static {}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;->isVoconLmttEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18
    invoke-static {}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;->notifyBootCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    :cond_0
    return-void
.end method
