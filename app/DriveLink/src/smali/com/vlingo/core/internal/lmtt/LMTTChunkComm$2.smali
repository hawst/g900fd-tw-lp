.class Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;
.super Ljava/lang/Object;
.source "LMTTChunkComm.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->handleNextChunk()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/32 v4, 0xea60

    .line 116
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # invokes: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->isLmttEnabledForType()Z
    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$100(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 117
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mListener:Lcom/vlingo/sdk/training/VLTrainerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$200(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Lcom/vlingo/sdk/training/VLTrainerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_SERVER:Lcom/vlingo/sdk/training/VLTrainerErrors;

    const-string/jumbo v4, "LMTT disabled!"

    invoke-interface {v2, v3, v4}, Lcom/vlingo/sdk/training/VLTrainerListener;->onError(Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V

    .line 141
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mActiveChunk:Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    invoke-static {v3}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$300(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    move-result-object v3

    # invokes: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->getTransmitDelay(Lcom/vlingo/core/internal/lmtt/LMTTChunk;)J
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$400(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;Lcom/vlingo/core/internal/lmtt/LMTTChunk;)J

    move-result-wide v0

    .line 122
    .local v0, "transmitDelay":J
    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 124
    # invokes: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->updateTransmitTime()V
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$500()V

    .line 125
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mActiveChunk:Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$300(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->transmit(Lcom/vlingo/sdk/training/VLTrainerListener;)V

    goto :goto_0

    .line 126
    :cond_1
    cmp-long v2, v0, v4

    if-gez v2, :cond_2

    .line 127
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mWorkerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$600(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, p0, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 139
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mWorkerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$600(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
