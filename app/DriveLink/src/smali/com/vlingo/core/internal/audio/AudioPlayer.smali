.class Lcom/vlingo/core/internal/audio/AudioPlayer;
.super Ljava/lang/Object;
.source "AudioPlayer.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;
.implements Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;
.implements Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;,
        Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;,
        Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;
    }
.end annotation


# static fields
.field public static final BT_START_DELAY_TIME:J = 0x7d0L

.field static final COMPLETE:I = 0x1

.field static final ERROR:I = 0x69

.field static final PLAY:I = 0x0

.field static final PREPARE_TTS_ENGINE:I = 0x4

.field static final SHUT_DOWN:I = 0x3

.field static final STOP:I = 0x2


# instance fields
.field final audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

.field final context:Landroid/content/Context;

.field existNotProcessedStopRequest:Z

.field final handler:Landroid/os/Handler;

.field private isCalledMute:Z

.field isInitialized:Z

.field private isPaused:Z

.field isShutDown:Z

.field isSupposedToBePlaying:Z

.field isWorking:Z

.field private final languageListener:Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;

.field mediaPlayer:Landroid/media/MediaPlayer;

.field private onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

.field private final phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

.field playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

.field final serviceListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field final telephonyManager:Landroid/telephony/TelephonyManager;

.field final ttsEngine:Lcom/vlingo/core/internal/audio/TTSEngine;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceListener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->existNotProcessedStopRequest:Z

    .line 48
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isWorking:Z

    .line 49
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isSupposedToBePlaying:Z

    .line 50
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isShutDown:Z

    .line 51
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isPaused:Z

    .line 52
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isCalledMute:Z

    .line 55
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isInitialized:Z

    .line 62
    iput-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 67
    iput-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .line 70
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->createMediaPlayer()V

    .line 71
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "AudioPlayer"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TTSEngine;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/TTSEngine;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->ttsEngine:Lcom/vlingo/core/internal/audio/TTSEngine;

    .line 73
    new-instance v1, Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;-><init>(Lcom/vlingo/core/internal/audio/AudioPlayer;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->languageListener:Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;

    .line 74
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->languageListener:Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;

    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;->register(Landroid/content/Context;)V

    .line 75
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 76
    new-instance v1, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;

    invoke-direct {v1, p0, v0}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;-><init>(Lcom/vlingo/core/internal/audio/AudioPlayer;Landroid/os/HandlerThread;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    .line 77
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->context:Landroid/content/Context;

    .line 78
    iput-object p2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->serviceListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 79
    const-string/jumbo v1, "phone"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 80
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getPhoneStateListener()Landroid/telephony/PhoneStateListener;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    iput-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    .line 81
    invoke-static {p1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    .line 82
    monitor-enter p0

    .line 86
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    invoke-virtual {v1, p0}, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->addListener(Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;)V

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isPaused:Z

    .line 93
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 96
    invoke-static {p1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->addListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V

    .line 97
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->addListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 106
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 107
    return-void

    .line 93
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/audio/AudioPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioPlayer;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isCalledMute:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/core/internal/audio/AudioPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isCalledMute:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/audio/AudioPlayer;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioPlayer;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/core/internal/audio/AudioPlayer;Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioPlayer;
    .param p1, "x1"    # Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    return-object p1
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/audio/AudioPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioPlayer;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->createMediaPlayer()V

    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/audio/AudioPlayer;Lcom/vlingo/core/internal/audio/AudioRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioPlayer;
    .param p1, "x1"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer;->setDataSource(Lcom/vlingo/core/internal/audio/AudioRequest;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/audio/AudioPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioPlayer;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->checkMuteVolume()V

    return-void
.end method

.method private checkMuteVolume()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 515
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 516
    .local v0, "am":Landroid/media/AudioManager;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 517
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v2

    if-nez v2, :cond_0

    .line 518
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 519
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v5, v5}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 524
    :cond_0
    const-string/jumbo v2, "set_audio_volume_for_assoc_svc"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 526
    .local v1, "isAudioVolumeForAssocSvc":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    .line 527
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    .line 530
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v4, v4}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 534
    .end local v1    # "isAudioVolumeForAssocSvc":Z
    :cond_1
    return-void
.end method

.method private createMediaPlayer()V
    .locals 1

    .prologue
    .line 511
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 512
    return-void
.end method

.method private setDataSource(Lcom/vlingo/core/internal/audio/AudioRequest;)Z
    .locals 4
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/4 v3, 0x0

    .line 491
    iput-boolean v3, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isInitialized:Z

    .line 493
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 494
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 495
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p1, v1, v2, p0}, Lcom/vlingo/core/internal/audio/AudioRequest;->setDataSource(Landroid/content/Context;Landroid/media/MediaPlayer;Lcom/vlingo/core/internal/audio/AudioPlayer;)V

    .line 496
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioStream:I

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 497
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 498
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isInitialized:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 505
    :goto_0
    iget-boolean v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isInitialized:Z

    return v1

    .line 499
    :catch_0
    move-exception v0

    .line 502
    .local v0, "ex":Ljava/lang/Exception;
    iput-boolean v3, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isInitialized:Z

    .line 503
    invoke-virtual {p1}, Lcom/vlingo/core/internal/audio/AudioRequest;->onSetDataSourceFailed()V

    goto :goto_0
.end method


# virtual methods
.method public getTTSEngine()Lcom/vlingo/core/internal/audio/TTSEngine;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->ttsEngine:Lcom/vlingo/core/internal/audio/TTSEngine;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 451
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isWorking:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 455
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isSupposedToBePlaying:Z

    return v0
.end method

.method public onAudioFocusChanged(I)V
    .locals 3
    .param p1, "audioFocus"    # I

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 588
    packed-switch p1, :pswitch_data_0

    .line 618
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 592
    :pswitch_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 594
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isCalledMute:Z

    if-eqz v0, :cond_0

    .line 595
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isCalledMute:Z

    .line 599
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 607
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 609
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isCalledMute:Z

    if-nez v0, :cond_0

    .line 610
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isCalledMute:Z

    .line 614
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 588
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onBluetoothServiceConnected()V
    .locals 0

    .prologue
    .line 623
    return-void
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 0
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 139
    packed-switch p1, :pswitch_data_0

    .line 157
    :goto_0
    return-void

    .line 143
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->pause()V

    goto :goto_0

    .line 148
    :pswitch_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->pause()V

    goto :goto_0

    .line 153
    :pswitch_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->resume()V

    goto :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 160
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->removeListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V

    .line 162
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->languageListener:Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;->unregister(Landroid/content/Context;)V

    .line 169
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->removeListener(Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;)V

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 174
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 175
    return-void
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 653
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 659
    return-void
.end method

.method public onScoConnected()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 628
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 632
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStreamMaxVolume()I

    move-result v1

    int-to-float v0, v1

    .line 633
    .local v0, "nVolume":F
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v2, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 636
    .end local v0    # "nVolume":F
    :cond_0
    return-void
.end method

.method public onScoDisconnected()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 640
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 647
    :cond_0
    return-void
.end method

.method public declared-synchronized pause()V
    .locals 1

    .prologue
    .line 459
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isPaused:Z

    .line 462
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->stop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463
    monitor-exit p0

    return-void

    .line 459
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public play(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 476
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->play(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 477
    return-void
.end method

.method public play(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 480
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isPaused:Z

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->serviceListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    sget-object v1, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;->PAUSED:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    invoke-interface {v0, p1, v1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    .line 482
    sget-object v0, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;->PAUSED:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    invoke-virtual {p1, p1, v0}, Lcom/vlingo/core/internal/audio/AudioRequest;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    .line 488
    :goto_0
    return-void

    .line 484
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isWorking:Z

    .line 485
    iput-object p2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 486
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public declared-synchronized resume()V
    .locals 1

    .prologue
    .line 466
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 467
    monitor-exit p0

    return-void

    .line 466
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 470
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->existNotProcessedStopRequest:Z

    .line 471
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 473
    return-void
.end method
