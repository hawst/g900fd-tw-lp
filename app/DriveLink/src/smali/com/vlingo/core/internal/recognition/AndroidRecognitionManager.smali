.class public final Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
.super Ljava/lang/Object;
.source "AndroidRecognitionManager.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/VLRecognitionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager$1;
    }
.end annotation


# static fields
.field private static instance:Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;


# instance fields
.field public final RECOGNITION_SERVICE_FIELDID:Ljava/lang/String;

.field private isActive:Z

.field private m_RecognitionContext:Lcom/vlingo/core/internal/recognition/AndroidSRContext;

.field private m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

.field private micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

.field private final recognitionStateListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->instance:Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string/jumbo v0, "dm_main"

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->RECOGNITION_SERVICE_FIELDID:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_RecognitionContext:Lcom/vlingo/core/internal/recognition/AndroidSRContext;

    .line 44
    iput-object v1, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isActive:Z

    .line 51
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->recognitionStateListeners:Ljava/util/List;

    .line 68
    new-instance v0, Lcom/vlingo/core/internal/recognition/AndroidSRContext;

    const-string/jumbo v1, "dm_main"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/recognition/AndroidSRContext;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_RecognitionContext:Lcom/vlingo/core/internal/recognition/AndroidSRContext;

    .line 69
    return-void
.end method

.method private abortRecognition()V
    .locals 1

    .prologue
    .line 197
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognizer;->cancelRecognition()V

    .line 199
    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->closeMicStream()V

    .line 200
    return-void
.end method

.method public static cancelRecognition()V
    .locals 1

    .prologue
    .line 211
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->abortRecognition()V

    .line 212
    return-void
.end method

.method private closeMicStream()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 371
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    if-eqz v0, :cond_0

    .line 373
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    iput-object v1, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 377
    :catch_0
    move-exception v0

    .line 382
    iput-object v1, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    throw v0
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    .locals 2

    .prologue
    .line 54
    const-class v1, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->instance:Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->instance:Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    .line 61
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->instance:Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static handleIUXNotComplete(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-static {p0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->handleIUXNotCompleteLaunch(Landroid/content/Context;)V

    .line 73
    return-void
.end method

.method private static handleIUXNotCompleteLaunch(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getLaunchingClass()Ljava/lang/Class;

    move-result-object v0

    .line 91
    .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/app/Activity;>;"
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    .local v1, "i":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 93
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 94
    return-void
.end method

.method public static initiateRecognition(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo;I)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fieldID"    # Ljava/lang/String;
    .param p2, "audioSourceInfo"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .param p3, "duration"    # I

    .prologue
    .line 108
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    move-result-object v0

    invoke-direct {v0, p1, p2, p3}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->startRecognition(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo;I)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized isIdle()Z
    .locals 1

    .prologue
    .line 112
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isActive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V
    .locals 3
    .param p1, "recState"    # I
    .param p2, "type"    # I
    .param p3, "note"    # Ljava/lang/String;

    .prologue
    .line 246
    iget-object v2, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->recognitionStateListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;

    .line 247
    .local v1, "listener":Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;
    invoke-interface {v1, p0, p1, p2, p3}, Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;->onRecognitionEvent(Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;IILjava/lang/String;)V

    goto :goto_0

    .line 249
    .end local v1    # "listener":Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;
    :cond_0
    return-void
.end method

.method public static processRecognition()V
    .locals 1

    .prologue
    .line 205
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->stopRecognition()V

    .line 206
    return-void
.end method

.method private recognitionStartCheck()Z
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v4, 0x0

    .line 119
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 120
    .local v1, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isIUXComplete()Z

    move-result v5

    if-nez v5, :cond_0

    .line 121
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->handleIUXNotComplete(Landroid/content/Context;)V

    .line 146
    :goto_0
    return v4

    .line 126
    :cond_0
    const-string/jumbo v5, "phone"

    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 127
    .local v2, "mgr":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v5

    if-eqz v5, :cond_1

    .line 128
    const/16 v5, 0x6c

    const-string/jumbo v6, "Phone in use"

    invoke-direct {p0, v5, v7, v6}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_1
    const-string/jumbo v5, "connectivity"

    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 133
    .local v0, "conMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    .line 134
    .local v3, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v5

    sget-object v6, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v5, v6, :cond_3

    .line 135
    :cond_2
    const/16 v5, 0x6a

    const-string/jumbo v6, "Network not available"

    invoke-direct {p0, v5, v7, v6}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 140
    :cond_3
    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isIdle()Z

    move-result v5

    if-nez v5, :cond_4

    .line 142
    const/16 v5, 0x71

    const-string/jumbo v6, "Recognizer is busy"

    invoke-direct {p0, v5, v7, v6}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 146
    :cond_4
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public static setResultDispatcher(Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;)V
    .locals 3
    .param p0, "dispatcher"    # Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    .prologue
    .line 217
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    move-result-object v1

    .line 218
    .local v1, "me":Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    iput-object p0, v1, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    .line 219
    const/4 v0, 0x0

    .line 220
    .local v0, "custom6Context":Ljava/lang/String;
    iget-object v2, v1, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_RecognitionContext:Lcom/vlingo/core/internal/recognition/AndroidSRContext;

    invoke-virtual {v2, v0}, Lcom/vlingo/core/internal/recognition/AndroidSRContext;->setCustom6Context(Ljava/lang/String;)V

    .line 221
    return-void
.end method

.method private startRecognition(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo;I)Z
    .locals 7
    .param p1, "fieldID"    # Ljava/lang/String;
    .param p2, "audioSourceInfo"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .param p3, "duration"    # I

    .prologue
    const/4 v4, 0x1

    .line 150
    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->recognitionStartCheck()Z

    move-result v3

    if-nez v3, :cond_0

    .line 151
    const/4 v3, 0x0

    .line 185
    :goto_0
    return v3

    .line 155
    :cond_0
    if-nez p2, :cond_4

    .line 156
    sget-object v3, Lcom/vlingo/core/internal/audio/MicrophoneStream;->testStream:Ljava/io/InputStream;

    if-eqz v3, :cond_2

    .line 157
    iget-object v3, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    sget-object v5, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_16KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    invoke-static {v3, v5}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getStreamSource(Ljava/io/InputStream;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object p2

    .line 158
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    invoke-direct {v0, p2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;-><init>(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)V

    .line 172
    .local v0, "builder":Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    :goto_1
    const/16 v3, 0x5dc

    const/16 v5, 0x1388

    invoke-virtual {v0, v3, v5}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoEndPointingTimeouts(II)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 173
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    .line 174
    .local v1, "language":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->language(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 175
    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->fieldID(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    move-result-object v0

    .line 176
    if-lez p3, :cond_1

    .line 177
    invoke-virtual {v0, p3}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->maxAudioTime(I)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 179
    :cond_1
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->build()Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    move-result-object v2

    .line 181
    .local v2, "vlRecognitionContext":Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v3

    new-instance v5, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    invoke-direct {v5}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;-><init>()V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSDKRecoMode()Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v6

    invoke-interface {v3, v2, p0, v5, v6}, Lcom/vlingo/sdk/recognition/VLRecognizer;->startRecognition(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    .line 182
    monitor-enter p0

    .line 183
    const/4 v3, 0x1

    :try_start_0
    iput-boolean v3, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isActive:Z

    .line 184
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v3, v4

    .line 185
    goto :goto_0

    .line 161
    .end local v0    # "builder":Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .end local v1    # "language":Ljava/lang/String;
    .end local v2    # "vlRecognitionContext":Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    :cond_2
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    invoke-direct {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;-><init>()V

    .line 162
    .restart local v0    # "builder":Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->build()Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    move-result-object v3

    sget-object v5, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->RECOGNITION:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    invoke-static {v3, v5}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->request(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 163
    iget-object v5, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    iget-object v3, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->is16KHz()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_16KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    :goto_2
    invoke-static {v5, v3}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getStreamSource(Ljava/io/InputStream;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object p2

    .line 164
    invoke-virtual {v0, p2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioSourceInfo(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 165
    iget-object v3, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->getChannelConfig()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioFormatChannelConfig(I)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    goto :goto_1

    .line 163
    :cond_3
    sget-object v3, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_8KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    goto :goto_2

    .line 168
    .end local v0    # "builder":Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    :cond_4
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    invoke-direct {v0, p2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;-><init>(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)V

    .restart local v0    # "builder":Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    goto :goto_1

    .line 184
    .restart local v1    # "language":Ljava/lang/String;
    .restart local v2    # "vlRecognitionContext":Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private stopRecognition()V
    .locals 1

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognizer;->stopRecognition()V

    goto :goto_0
.end method


# virtual methods
.method public addRecognitionStateListener(Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;)V
    .locals 1
    .param p1, "recoListener"    # Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->recognitionStateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->recognitionStateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    :cond_0
    return-void
.end method

.method public getSRContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_RecognitionContext:Lcom/vlingo/core/internal/recognition/AndroidSRContext;

    return-object v0
.end method

.method public declared-synchronized isActive()Z
    .locals 1

    .prologue
    .line 252
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isIdle()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onASRRecorderClosed()V
    .locals 0

    .prologue
    .line 401
    return-void
.end method

.method public onASRRecorderOpened()V
    .locals 0

    .prologue
    .line 407
    return-void
.end method

.method public onCancelled()V
    .locals 1

    .prologue
    .line 362
    monitor-enter p0

    .line 363
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isActive:Z

    .line 364
    monitor-exit p0

    .line 365
    return-void

    .line 364
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
    .locals 7
    .param p1, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x6b

    const/4 v4, 0x4

    .line 286
    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->closeMicStream()V

    .line 288
    const/16 v2, 0x67

    const/4 v3, 0x5

    invoke-direct {p0, v2, v3, v6}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    .line 290
    sget-object v2, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager$1;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionErrors:[I

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 307
    const/16 v2, 0x6a

    invoke-direct {p0, v2, v4, p2}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    .line 311
    :goto_0
    move-object v0, p2

    .line 312
    .local v0, "errorString":Ljava/lang/String;
    new-instance v1, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;

    invoke-direct {v1, v0, v6}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    .local v1, "results":Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;
    iget-object v2, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    if-eqz v2, :cond_0

    .line 315
    iget-object v2, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    invoke-virtual {v2, v1}, Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;->handleResults(Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;)Z

    .line 317
    :cond_0
    monitor-enter p0

    .line 318
    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isActive:Z

    .line 319
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    return-void

    .line 292
    .end local v0    # "errorString":Ljava/lang/String;
    .end local v1    # "results":Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;
    :pswitch_0
    invoke-direct {p0, v5, v4, p2}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 295
    :pswitch_1
    const/16 v2, 0x6c

    invoke-direct {p0, v2, v4, p2}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 298
    :pswitch_2
    invoke-direct {p0, v5, v4, p2}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 301
    :pswitch_3
    const/16 v2, 0x6f

    invoke-direct {p0, v2, v4, p2}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 304
    :pswitch_4
    const/16 v2, 0x71

    invoke-direct {p0, v2, v4, p2}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 319
    .restart local v0    # "errorString":Ljava/lang/String;
    .restart local v1    # "results":Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 290
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onRecoToneStarting(Z)J
    .locals 2
    .param p1, "startTone"    # Z

    .prologue
    .line 394
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onRecoToneStopped(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 412
    return-void
.end method

.method public onRecognitionResults(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V
    .locals 6
    .param p1, "vlRecognitionResults"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .prologue
    const/4 v5, 0x5

    .line 332
    const/16 v3, 0x67

    const/4 v4, 0x0

    invoke-direct {p0, v3, v5, v4}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    .line 334
    const/4 v0, 0x0

    .line 335
    .local v0, "errorString":Ljava/lang/String;
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getResultString()Ljava/lang/String;

    move-result-object v2

    .line 336
    .local v2, "resultsString":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 337
    const/16 v3, 0x6f

    invoke-direct {p0, v3, v5, v0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    .line 341
    :goto_0
    new-instance v1, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;

    invoke-direct {v1, v0, v2}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    .local v1, "results":Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;
    iget-object v3, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    if-eqz v3, :cond_0

    .line 344
    iget-object v3, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    invoke-virtual {v3, v1}, Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;->handleResults(Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;)Z

    .line 346
    :cond_0
    monitor-enter p0

    .line 347
    const/4 v3, 0x0

    :try_start_0
    iput-boolean v3, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isActive:Z

    .line 348
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    return-void

    .line 339
    .end local v1    # "results":Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;
    :cond_1
    const/16 v3, 0x70

    invoke-direct {p0, v3, v5, v0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 348
    .restart local v1    # "results":Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public onRmsChanged(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 353
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;->onRmsChanged(I)V

    .line 356
    :cond_0
    return-void
.end method

.method public onStateChanged(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 4
    .param p1, "newState"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x5

    .line 258
    sget-object v0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager$1;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 279
    :goto_0
    return-void

    .line 260
    :pswitch_0
    const/16 v0, 0x64

    invoke-direct {p0, v0, v2, v3}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 263
    :pswitch_1
    const/16 v0, 0x66

    invoke-direct {p0, v0, v2, v3}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 266
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;->notifyWorking()V

    .line 269
    :cond_0
    const/16 v0, 0x65

    invoke-direct {p0, v0, v2, v3}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 272
    :pswitch_3
    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->closeMicStream()V

    .line 273
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->m_ResultDispatcher:Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;->notifyWorking()V

    .line 276
    :cond_1
    const/16 v0, 0x68

    invoke-direct {p0, v0, v2, v3}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    goto :goto_0

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)V
    .locals 3
    .param p1, "warning"    # Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 327
    const/16 v0, 0x67

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->notifyRecognitionStateListenersOfEvent(IILjava/lang/String;)V

    .line 328
    return-void
.end method

.method public removeRecognitionStateListener(Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;)V
    .locals 1
    .param p1, "recoListener"    # Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->recognitionStateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->recognitionStateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 241
    :cond_0
    return-void
.end method
