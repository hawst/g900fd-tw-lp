.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;
.super Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$BaseState;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Starting"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    .locals 0

    .prologue
    .line 527
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$BaseState;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    return-void
.end method


# virtual methods
.method public is(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Z
    .locals 1
    .param p1, "state"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    .prologue
    .line 536
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->STARTING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEnter()V
    .locals 7

    .prologue
    .line 552
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Starting.onEnter()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->establishChunkSize()V
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    .line 559
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->establishLogging()V
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 608
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mChunkSize:I
    invoke-static {v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$800(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I

    move-result v5

    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSampleRate:I
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I

    move-result v6

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;II)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "PhraseSpotterControl#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->instanceSerialNumber:I
    invoke-static {v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # ++operator for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->spotterThreadCount:I
    invoke-static {v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1004(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v2, v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    .line 609
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v1, v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 610
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v2, v2, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThreadName:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1102(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Ljava/lang/String;)Ljava/lang/String;

    .line 614
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getListener()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotterStarted()V

    .line 615
    return-void

    .line 592
    :catch_0
    move-exception v0

    .line 593
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "spotting: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error starting phrasespotter: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 3
    .param p1, "phraseSpotterParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "listener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 531
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start: Attempting to start while in state \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' (not IDLE)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 546
    invoke-super {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$BaseState;->stop()V

    .line 547
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->STOPPING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 548
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 541
    const-string/jumbo v0, "Starting"

    return-object v0
.end method
