.class Lcom/vlingo/core/internal/util/SayHello$VVSListener;
.super Ljava/lang/Object;
.source "SayHello.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/SayHello;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VVSListener"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/util/SayHello$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/util/SayHello$1;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/SayHello$VVSListener;-><init>()V

    return-void
.end method


# virtual methods
.method public asyncHandlerDone()V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method public asyncHandlerStarted()V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method public endpointReco()V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method public execute(Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;)V
    .locals 0
    .param p1, "action"    # Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    .prologue
    .line 180
    return-void
.end method

.method public finishDialog()V
    .locals 0

    .prologue
    .line 125
    return-void
.end method

.method public finishTurn()V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public getActivityContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return-object v0
.end method

.method public getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;
    .locals 1
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .prologue
    .line 138
    const/4 v0, 0x0

    return-object v0
.end method

.method public interruptTurn()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 0
    .param p1, "event"    # Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
    .param p2, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 151
    return-void
.end method

.method public sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 0
    .param p1, "event"    # Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
    .param p2, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 112
    return-void
.end method

.method public setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 0
    .param p1, "id"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 121
    return-void
.end method

.method public showUserText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 100
    return-void
.end method

.method public showUserText(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 104
    return-void
.end method

.method public showVlingoText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 108
    return-void
.end method

.method public showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "tts"    # Ljava/lang/String;

    .prologue
    .line 175
    return-void
.end method

.method public showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p2, "decorators"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "TT;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 163
    .local p3, "object":Ljava/lang/Object;, "TT;"
    return-void
.end method

.method public startReco()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    return v0
.end method

.method public storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V
    .locals 0
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 134
    return-void
.end method

.method public tts(Ljava/lang/String;)V
    .locals 0
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 167
    return-void
.end method

.method public tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 0
    .param p1, "tts"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 200
    return-void
.end method

.method public ttsAnyway(Ljava/lang/String;)V
    .locals 0
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 171
    return-void
.end method

.method public ttsAnyway(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 0
    .param p1, "tts"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 194
    return-void
.end method

.method public userCancel()V
    .locals 0

    .prologue
    .line 188
    return-void
.end method
