.class public Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;
.super Ljava/lang/Object;
.source "WeatherInfoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ForecastingWeekly"
.end annotation


# instance fields
.field private date:Ljava/lang/String;

.field private days:Ljava/lang/String;

.field private image:Ljava/lang/String;

.field private maxTemp:Ljava/lang/String;

.field private minTemp:Ljava/lang/String;

.field final synthetic this$0:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

.field private weatherCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/weather/WeatherInfoUtil;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->this$0:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    return-void
.end method


# virtual methods
.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getDays()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->days:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->image:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxTemp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->maxTemp:Ljava/lang/String;

    return-object v0
.end method

.method public getMinTemp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->minTemp:Ljava/lang/String;

    return-object v0
.end method

.method public getWeatherCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->weatherCode:Ljava/lang/String;

    return-object v0
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 266
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->date:Ljava/lang/String;

    .line 267
    return-void
.end method

.method public setDays(Ljava/lang/String;)V
    .locals 0
    .param p1, "days"    # Ljava/lang/String;

    .prologue
    .line 260
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->days:Ljava/lang/String;

    .line 261
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "image"    # Ljava/lang/String;

    .prologue
    .line 272
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->image:Ljava/lang/String;

    .line 273
    return-void
.end method

.method public setMaxTemp(Ljava/lang/String;)V
    .locals 0
    .param p1, "maxTemp"    # Ljava/lang/String;

    .prologue
    .line 278
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->maxTemp:Ljava/lang/String;

    .line 279
    return-void
.end method

.method public setMinTemp(Ljava/lang/String;)V
    .locals 0
    .param p1, "minTemp"    # Ljava/lang/String;

    .prologue
    .line 284
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->minTemp:Ljava/lang/String;

    .line 285
    return-void
.end method

.method public setWeatherCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "weatherCode"    # Ljava/lang/String;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->weatherCode:Ljava/lang/String;

    .line 291
    return-void
.end method
