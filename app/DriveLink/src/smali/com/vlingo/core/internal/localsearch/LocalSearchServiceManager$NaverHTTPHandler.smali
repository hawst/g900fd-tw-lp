.class Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;
.super Ljava/lang/Object;
.source "LocalSearchServiceManager.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NaverHTTPHandler"
.end annotation


# instance fields
.field m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

.field final synthetic this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 1
    .param p2, "listener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 451
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 449
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 452
    iput-object p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 453
    return-void
.end method


# virtual methods
.method public onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 497
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Cancelled"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 500
    return-void
.end method

.method public onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 490
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Failure"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 493
    return-void
.end method

.method public onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 457
    const/4 v0, 0x0

    .line 458
    .local v0, "result":Lcom/vlingo/core/internal/naver/NaverResponseParser;
    iget v2, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    .line 461
    new-instance v0, Lcom/vlingo/core/internal/naver/NaverResponseParser;

    .end local v0    # "result":Lcom/vlingo/core/internal/naver/NaverResponseParser;
    invoke-direct {v0}, Lcom/vlingo/core/internal/naver/NaverResponseParser;-><init>()V

    .line 462
    .restart local v0    # "result":Lcom/vlingo/core/internal/naver/NaverResponseParser;
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->parse(Ljava/lang/String;)V

    .line 463
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->getError()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 466
    .local v1, "success":Z
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    invoke-interface {v2, v1, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestComplete(ZLjava/lang/Object;)V

    .line 471
    .end local v1    # "success":Z
    :goto_1
    return-void

    .line 463
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 468
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "responseCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 482
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Timeout"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 485
    const/4 v0, 0x0

    return v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 477
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestScheduled()V

    .line 478
    return-void
.end method
