.class public final enum Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;
.super Ljava/lang/Enum;
.source "CoreServerInfoUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/CoreServerInfoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ServerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

.field public static final enum ASR:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

.field public static final enum HELLO:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

.field public static final enum LMTT:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

.field public static final enum LOG:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

.field public static final enum TTS:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

.field public static final enum VCS:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    const-string/jumbo v1, "ASR"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->ASR:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    new-instance v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    const-string/jumbo v1, "TTS"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->TTS:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    new-instance v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    const-string/jumbo v1, "VCS"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->VCS:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    new-instance v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    const-string/jumbo v1, "LOG"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->LOG:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    new-instance v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    const-string/jumbo v1, "HELLO"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->HELLO:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    new-instance v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    const-string/jumbo v1, "LMTT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->LMTT:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    sget-object v1, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->ASR:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->TTS:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->VCS:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->LOG:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->HELLO:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->LMTT:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->$VALUES:[Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->$VALUES:[Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    return-object v0
.end method
