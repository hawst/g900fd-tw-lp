.class public Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;
.super Ljava/util/HashSet;
.source "PreferenceUpdateListenerHolder.java"

# interfaces
.implements Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;
.implements Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;",
        ">;",
        "Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;",
        "Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;"
    }
.end annotation


# instance fields
.field private preference:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;)V
    .locals 0
    .param p1, "holder"    # Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 15
    return-void
.end method


# virtual methods
.method public getPreference()Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;->preference:Landroid/preference/Preference;

    return-object v0
.end method

.method public onPreferenceUpdated()V
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;->getPreference()Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;->onPreferenceUpdated(Landroid/preference/Preference;)V

    .line 34
    return-void
.end method

.method public onPreferenceUpdated(Landroid/preference/Preference;)V
    .locals 3
    .param p1, "prefValue"    # Landroid/preference/Preference;

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    .line 20
    .local v1, "listener":Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;->getPreference()Landroid/preference/Preference;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;->onPreferenceUpdated(Landroid/preference/Preference;)V

    goto :goto_0

    .line 21
    .end local v1    # "listener":Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;
    :cond_0
    return-void
.end method

.method public setPreference(Landroid/preference/Preference;)Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;
    .locals 0
    .param p1, "prefValue"    # Landroid/preference/Preference;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;->preference:Landroid/preference/Preference;

    .line 29
    return-object p0
.end method
