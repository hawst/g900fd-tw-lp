.class public final enum Lcom/vlingo/core/internal/util/Alarm$MatchType;
.super Ljava/lang/Enum;
.source "Alarm.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/Alarm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MatchType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/util/Alarm$MatchType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/util/Alarm$MatchType;

.field public static final enum EXACT:Lcom/vlingo/core/internal/util/Alarm$MatchType;

.field public static final enum LOOSE:Lcom/vlingo/core/internal/util/Alarm$MatchType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 76
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$MatchType;

    const-string/jumbo v1, "EXACT"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/util/Alarm$MatchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$MatchType;->EXACT:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    .line 77
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$MatchType;

    const-string/jumbo v1, "LOOSE"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/util/Alarm$MatchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$MatchType;->LOOSE:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    .line 75
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/core/internal/util/Alarm$MatchType;

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$MatchType;->EXACT:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$MatchType;->LOOSE:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$MatchType;->$VALUES:[Lcom/vlingo/core/internal/util/Alarm$MatchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/util/Alarm$MatchType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 75
    const-class v0, Lcom/vlingo/core/internal/util/Alarm$MatchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/Alarm$MatchType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/util/Alarm$MatchType;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/vlingo/core/internal/util/Alarm$MatchType;->$VALUES:[Lcom/vlingo/core/internal/util/Alarm$MatchType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/util/Alarm$MatchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/util/Alarm$MatchType;

    return-object v0
.end method
