.class public abstract Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
.super Ljava/lang/Object;
.source "DialogEvent.java"


# instance fields
.field private final preempt:Z

.field private final terminalState:Z

.field private final unique:Z

.field private final userInitiated:Z


# direct methods
.method protected constructor <init>(ZZZ)V
    .locals 1
    .param p1, "isTerminalState"    # Z
    .param p2, "cancelTtsReco"    # Z
    .param p3, "userInitiated"    # Z

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->terminalState:Z

    .line 21
    iput-boolean p3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->userInitiated:Z

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->unique:Z

    .line 23
    iput-boolean p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->preempt:Z

    .line 24
    return-void
.end method

.method protected constructor <init>(ZZZZ)V
    .locals 0
    .param p1, "isTerminalState"    # Z
    .param p2, "userInitiated"    # Z
    .param p3, "isUnique"    # Z
    .param p4, "cancelTtsReco"    # Z

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->terminalState:Z

    .line 28
    iput-boolean p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->userInitiated:Z

    .line 29
    iput-boolean p3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->unique:Z

    .line 30
    iput-boolean p4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->preempt:Z

    .line 31
    return-void
.end method


# virtual methods
.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
.end method

.method public isTerminalState()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->terminalState:Z

    return v0
.end method

.method public isUnique()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->unique:Z

    return v0
.end method

.method public isUserInitiated()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->userInitiated:Z

    return v0
.end method

.method public shouldStopTtsReco()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->preempt:Z

    return v0
.end method
