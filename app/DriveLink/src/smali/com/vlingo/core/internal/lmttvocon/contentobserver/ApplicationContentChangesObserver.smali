.class public Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver;
.super Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;
.source "ApplicationContentChangesObserver.java"


# instance fields
.field private receiver:Landroid/content/BroadcastReceiver;


# direct methods
.method protected constructor <init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V
    .locals 0
    .param p1, "context"    # Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V

    .line 14
    return-void
.end method


# virtual methods
.method public bridge synthetic executeCallback()V
    .locals 0

    .prologue
    .line 9
    invoke-super {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->executeCallback()V

    return-void
.end method

.method protected registerContentObserver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver;->receiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 27
    new-instance v1, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver$1;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver$1;-><init>(Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver;->receiver:Landroid/content/BroadcastReceiver;

    .line 33
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 34
    .local v0, "f":Landroid/content/IntentFilter;
    const-string/jumbo v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 35
    const-string/jumbo v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 36
    const-string/jumbo v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 37
    const-string/jumbo v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 38
    const-string/jumbo v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 39
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 41
    .end local v0    # "f":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public bridge synthetic resetDelay()V
    .locals 0

    .prologue
    .line 9
    invoke-super {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->resetDelay()V

    return-void
.end method

.method protected unregisterContentObserver(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver;->receiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver;->receiver:Landroid/content/BroadcastReceiver;

    .line 22
    :cond_0
    return-void
.end method
