.class Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;
.super Ljava/lang/Object;
.source "DMServerTask.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/VLRecognitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DMTransactionListener"
.end annotation


# instance fields
.field private processingTone:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;

.field private final recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V
    .locals 1
    .param p2, "recoListener"    # Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .prologue
    .line 506
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 507
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 508
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->processingTone:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;

    .line 509
    return-void
.end method

.method static synthetic access$1500(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;

    .prologue
    .line 502
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    return-object v0
.end method


# virtual methods
.method public onASRRecorderClosed()V
    .locals 0

    .prologue
    .line 621
    return-void
.end method

.method public onASRRecorderOpened()V
    .locals 0

    .prologue
    .line 627
    return-void
.end method

.method public onCancelled()V
    .locals 2

    .prologue
    .line 605
    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onRecoCancelled()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onCancelled()V

    .line 607
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->processingTone:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->stop()V

    .line 608
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    const/4 v1, 0x0

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1400(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)V

    .line 609
    return-void
.end method

.method public onError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 565
    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onError()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->closeMicStream()V
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1200(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)V

    .line 571
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->processingTone:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->stop()V

    .line 573
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    .line 574
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    const/4 v1, 0x0

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1400(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)V

    .line 575
    return-void
.end method

.method public onRecoToneStarting(Z)J
    .locals 2
    .param p1, "startTone"    # Z

    .prologue
    .line 614
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onRecoToneStopped(Z)V
    .locals 0
    .param p1, "startTone"    # Z

    .prologue
    .line 632
    return-void
.end method

.method public onRecognitionResults(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V
    .locals 2
    .param p1, "result"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .prologue
    .line 584
    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onRecognitionResults()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    const/4 v1, 0x1

    # setter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->gotResults:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1602(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)Z

    .line 588
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->processingTone:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->stop()V

    .line 590
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1700(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1700(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->clearEvents()V

    .line 593
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onRecognitionResults(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    .line 595
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    const/4 v1, 0x0

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1400(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)V

    .line 596
    return-void
.end method

.method public onRmsChanged(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 600
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onRmsChanged(I)V

    .line 601
    return-void
.end method

.method public onStateChanged(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 5
    .param p1, "newState"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    const/4 v4, 0x1

    .line 513
    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onStateChanged() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performingReco:Z
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1100(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->THINKING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    if-ne p1, v1, :cond_1

    .line 517
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    if-eqz v1, :cond_0

    .line 518
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onRecoToneStarting(Z)J

    .line 521
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->closeMicStream()V
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1200(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)V

    .line 524
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 525
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v0

    .line 529
    .local v0, "toneId":I
    :goto_0
    if-lez v0, :cond_1

    .line 530
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->useAudioTrackTonePlayer:Z
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$400(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 531
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # setter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->awaitingStopTone:Z
    invoke-static {v1, v4}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1302(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)Z

    .line 532
    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "About to play"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    new-instance v1, Lcom/vlingo/core/internal/audio/TonePlayer;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->getAudioType()Lcom/vlingo/core/internal/audio/AudioType;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1000(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/audio/AudioType;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/vlingo/core/internal/audio/TonePlayer;-><init>(ILcom/vlingo/core/internal/audio/AudioType;)V

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener$1;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;)V

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/audio/TonePlayer;->play(Lcom/vlingo/core/internal/audio/TonePlayer$Listener;)V

    .line 560
    .end local v0    # "toneId":I
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-interface {v1, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onStateChanged(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    .line 561
    return-void

    .line 527
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->getToneId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v0

    .restart local v0    # "toneId":I
    goto :goto_0

    .line 548
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # setter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->awaitingStopTone:Z
    invoke-static {v1, v4}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1302(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)Z

    .line 549
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener$2;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener$2;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;)V

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playTone(ILcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    goto :goto_1
.end method

.method public onWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)V
    .locals 1
    .param p1, "warning"    # Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 579
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)V

    .line 580
    return-void
.end method
