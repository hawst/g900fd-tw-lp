.class public Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
.super Ljava/lang/Object;
.source "PhraseSpotterParameters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field public static final DEFAULT_CHUNK_LENGTH_MS:I = 0xa

.field public static final DEFAULT_PREBUFFER_LENGTH_MS:I = 0x1f4

.field public static final DEFAULT_RECORDER_SLEEP_MS:I = 0x5

.field public static final DEFAULT_SEAMLESS_TIMEOUT_MS:I = 0x3e8


# instance fields
.field private audioSourceType:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

.field private chunkLengthMs:I

.field coreSpotterParams:Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

.field private language:Ljava/lang/String;

.field private preBufferLengthMs:I

.field private recorderSleepMs:I

.field private seamlessTimeoutMs:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/16 v0, 0xa

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->chunkLengthMs:I

    .line 32
    const/4 v0, 0x5

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->recorderSleepMs:I

    .line 33
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->preBufferLengthMs:I

    .line 34
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->seamlessTimeoutMs:I

    .line 35
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->UNSPECIFIED:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->audioSourceType:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;-><init>()V

    .line 40
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "language cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->language:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->coreSpotterParams:Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "csParams"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "language cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->language:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->coreSpotterParams:Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    .line 54
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->language:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->chunkLengthMs:I

    return v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->recorderSleepMs:I

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->preBufferLengthMs:I

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->seamlessTimeoutMs:I

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->audioSourceType:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->recorderSleepMs:I

    iget v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->chunkLengthMs:I

    if-lt v0, v1, :cond_0

    .line 99
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "recorderSleep duration must be smaller than chunk length"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->preBufferLengthMs:I

    iget v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->chunkLengthMs:I

    rem-int/2addr v0, v1

    if-lez v0, :cond_1

    .line 102
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "preBufferLength duration must be a multiple of chunk length"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_1
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$1;)V

    return-object v0
.end method

.method public setAudioSourceType(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    .locals 0
    .param p1, "type"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->audioSourceType:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .line 94
    return-object p0
.end method

.method public setChunkLength(I)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    .locals 0
    .param p1, "durationMs"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->chunkLengthMs:I

    .line 58
    return-object p0
.end method

.method public setPreBufferLength(I)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    .locals 0
    .param p1, "durationMs"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->preBufferLengthMs:I

    .line 74
    return-object p0
.end method

.method public setRecorderSleep(I)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    .locals 0
    .param p1, "durationMs"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->recorderSleepMs:I

    .line 66
    return-object p0
.end method

.method public setSeamlessTimeout(I)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    .locals 0
    .param p1, "durationMs"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->seamlessTimeoutMs:I

    .line 84
    return-object p0
.end method
