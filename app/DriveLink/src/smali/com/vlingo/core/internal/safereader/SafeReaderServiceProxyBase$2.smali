.class Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$2;
.super Ljava/lang/Object;
.source "SafeReaderServiceProxyBase.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->connect()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$2;->this$0:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "iBinder"    # Landroid/os/IBinder;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$2;->this$0:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;

    # getter for: Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->access$000(Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$2;->this$0:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;

    # getter for: Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->access$000(Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 110
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$2;->this$0:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;

    # getter for: Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->access$000(Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$2;->this$0:Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;

    # getter for: Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;->access$000(Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 113
    return-void
.end method
