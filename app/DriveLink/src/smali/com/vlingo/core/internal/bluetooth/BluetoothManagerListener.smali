.class public interface abstract Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
.super Ljava/lang/Object;
.source "BluetoothManagerListener.java"


# virtual methods
.method public abstract onBluetoothServiceConnected()V
.end method

.method public abstract onHeadsetConnected()V
.end method

.method public abstract onHeadsetDisconnected()V
.end method

.method public abstract onScoConnected()V
.end method

.method public abstract onScoDisconnected()V
.end method
