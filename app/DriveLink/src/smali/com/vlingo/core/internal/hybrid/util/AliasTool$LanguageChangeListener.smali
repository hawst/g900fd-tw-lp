.class Lcom/vlingo/core/internal/hybrid/util/AliasTool$LanguageChangeListener;
.super Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;
.source "AliasTool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/hybrid/util/AliasTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LanguageChangeListener"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/hybrid/util/AliasTool$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/hybrid/util/AliasTool$1;

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/vlingo/core/internal/hybrid/util/AliasTool$LanguageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onLanguageChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "newLanguage"    # Ljava/lang/String;

    .prologue
    .line 213
    new-instance v0, Landroid/content/res/Configuration;

    # getter for: Lcom/vlingo/core/internal/hybrid/util/AliasTool;->context:Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->access$200()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 214
    .local v0, "newConfiguration":Landroid/content/res/Configuration;
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 215
    # getter for: Lcom/vlingo/core/internal/hybrid/util/AliasTool;->context:Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->access$200()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 216
    const/4 v1, 0x0

    # setter for: Lcom/vlingo/core/internal/hybrid/util/AliasTool;->isInitialized:Z
    invoke-static {v1}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->access$302(Z)Z

    .line 217
    return-void
.end method
