.class public final enum Lcom/vlingo/core/internal/dialogmanager/DMActionType;
.super Ljava/lang/Enum;
.source "DMActionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/DMActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum CREATE_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum DELETE_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum DELETE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum DELETE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum DELETE_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum LOCATE_CAR:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum MODIFY_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum MODIFY_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum MODIFY_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum NOTIFICATION_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum PLAY_ALBUM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum PLAY_ARTIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum PLAY_OUT_NEWS:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum PLAY_OUT_NEWS_MULTI:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum PLAY_PLAYLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum PLAY_SONGLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum PLAY_TITLE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum RECORD_VOICE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum SAVE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum SCHEDULE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum SCHEDULE_EDIT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum SCHEDULE_VIEW:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum SEND_EMAIL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum SET_TIMER:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum VIDEO_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum VIEW_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

.field public static final enum VOICE_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "CREATE_TASK"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->CREATE_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 13
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "DELETE_ALARM"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "DELETE_APPOINTMENT"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 15
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "DELETE_MEMO"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 16
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "DELETE_TASK"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 17
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "EXECUTE_INTENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 18
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "LAUNCH_ACTIVITY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 19
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "MODIFY_ALARM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 20
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "MODIFY_APPOINTMENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 21
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "MODIFY_TASK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 22
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "OPEN_APP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 23
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "PLAY_ALBUM"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_ALBUM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 24
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "PLAY_ARTIST"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_ARTIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 25
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "PLAY_MUSIC"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 26
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "PLAY_OUT_NEWS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_OUT_NEWS:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "PLAY_OUT_NEWS_MULTI"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_OUT_NEWS_MULTI:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 28
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "PLAY_PLAYLIST"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_PLAYLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 29
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "PLAY_TITLE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_TITLE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 30
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "PLAY_SONGLIST"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_SONGLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 31
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "SAVE_MEMO"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SAVE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 32
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "SCHEDULE_EDIT"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_EDIT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 33
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "SCHEDULE_VIEW"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_VIEW:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 34
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "SCHEDULE_APPOINTMENT"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 35
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "SEND_EMAIL"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_EMAIL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 36
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "SEND_MESSAGE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 37
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "SET_ALARM"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "SET_TIMER"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SET_TIMER:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 39
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "SETTING_CHANGE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 40
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "SOCIAL_UPDATE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 41
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "VIEW_CONTACT"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VIEW_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 42
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "VIDEO_DIAL"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VIDEO_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 43
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "VOICE_DIAL"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VOICE_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 44
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "RECORD_VOICE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->RECORD_VOICE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 45
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "NOTIFICATION_CHANGE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->NOTIFICATION_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 46
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-string/jumbo v1, "LOCATE_CAR"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LOCATE_CAR:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 11
    const/16 v0, 0x23

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->CREATE_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_ALBUM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_ARTIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_OUT_NEWS:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_OUT_NEWS_MULTI:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_PLAYLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_TITLE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_SONGLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SAVE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_EDIT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_VIEW:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_EMAIL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SET_TIMER:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VIEW_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VIDEO_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VOICE_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->RECORD_VOICE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->NOTIFICATION_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LOCATE_CAR:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/DMActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    return-object v0
.end method
