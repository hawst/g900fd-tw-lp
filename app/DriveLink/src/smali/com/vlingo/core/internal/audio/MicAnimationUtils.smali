.class public Lcom/vlingo/core/internal/audio/MicAnimationUtils;
.super Ljava/lang/Object;
.source "MicAnimationUtils.java"


# static fields
.field private static listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/audio/MicAnimationListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->listeners:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addListener(Lcom/vlingo/core/internal/audio/MicAnimationListener;)V
    .locals 1
    .param p0, "l"    # Lcom/vlingo/core/internal/audio/MicAnimationListener;

    .prologue
    .line 27
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->init()V

    .line 28
    sget-object v0, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->listeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    return-void
.end method

.method public static init()V
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->listeners:Ljava/util/List;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->listeners:Ljava/util/List;

    .line 61
    :cond_0
    return-void
.end method

.method public static notifyListeners([I)V
    .locals 3
    .param p0, "v"    # [I

    .prologue
    .line 45
    sget-object v2, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->listeners:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 46
    sget-object v2, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/audio/MicAnimationListener;

    .line 47
    .local v1, "l":Lcom/vlingo/core/internal/audio/MicAnimationListener;
    invoke-interface {v1, p0}, Lcom/vlingo/core/internal/audio/MicAnimationListener;->onMicAnimationData([I)V

    goto :goto_0

    .line 50
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/vlingo/core/internal/audio/MicAnimationListener;
    :cond_0
    return-void
.end method

.method public static removeListener(Lcom/vlingo/core/internal/audio/MicAnimationListener;)V
    .locals 1
    .param p0, "l"    # Lcom/vlingo/core/internal/audio/MicAnimationListener;

    .prologue
    .line 35
    sget-object v0, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->listeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 36
    return-void
.end method
