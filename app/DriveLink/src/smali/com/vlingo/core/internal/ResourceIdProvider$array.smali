.class public final enum Lcom/vlingo/core/internal/ResourceIdProvider$array;
.super Ljava/lang/Enum;
.source "ResourceIdProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/ResourceIdProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "array"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/ResourceIdProvider$array;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$array;

.field public static final enum core_dayMatchers:Lcom/vlingo/core/internal/ResourceIdProvider$array;

.field public static final enum core_languages_iso:Lcom/vlingo/core/internal/ResourceIdProvider$array;

.field public static final enum core_languages_names:Lcom/vlingo/core/internal/ResourceIdProvider$array;

.field public static final enum core_monthMatchers:Lcom/vlingo/core/internal/ResourceIdProvider$array;

.field public static final enum core_russian_weekdays:Lcom/vlingo/core/internal/ResourceIdProvider$array;

.field public static final enum core_weather_phenomenon:Lcom/vlingo/core/internal/ResourceIdProvider$array;

.field public static final enum core_weather_wind_direction:Lcom/vlingo/core/internal/ResourceIdProvider$array;

.field public static final enum core_weather_wind_force:Lcom/vlingo/core/internal/ResourceIdProvider$array;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 410
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;

    const-string/jumbo v1, "core_languages_iso"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/ResourceIdProvider$array;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_iso:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .line 411
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;

    const-string/jumbo v1, "core_monthMatchers"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/ResourceIdProvider$array;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_monthMatchers:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .line 412
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;

    const-string/jumbo v1, "core_dayMatchers"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/ResourceIdProvider$array;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_dayMatchers:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .line 413
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;

    const-string/jumbo v1, "core_weather_phenomenon"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/ResourceIdProvider$array;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_phenomenon:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .line 414
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;

    const-string/jumbo v1, "core_weather_wind_force"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/ResourceIdProvider$array;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_wind_force:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .line 415
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;

    const-string/jumbo v1, "core_weather_wind_direction"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$array;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_wind_direction:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .line 416
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;

    const-string/jumbo v1, "core_languages_names"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$array;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_names:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .line 417
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;

    const-string/jumbo v1, "core_russian_weekdays"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$array;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_russian_weekdays:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .line 409
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_iso:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_monthMatchers:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_dayMatchers:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_phenomenon:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_wind_force:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_wind_direction:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_names:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_russian_weekdays:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$array;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 409
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/ResourceIdProvider$array;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 409
    const-class v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/ResourceIdProvider$array;
    .locals 1

    .prologue
    .line 409
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/ResourceIdProvider$array;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/ResourceIdProvider$array;

    return-object v0
.end method
