.class public Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;
.super Ljava/lang/Object;
.source "WakeUpContext.java"


# static fields
.field private static final X_VLCONF_AUDIO_HAS_WAKEUP_PHRASE:Ljava/lang/String; = "AudioHasWakeUpPhrase"

.field private static final X_VLCONF_WAKEUP_PHRASE:Ljava/lang/String; = "WakeUpPhrase"

.field private static final X_VLCONF_WAKEUP_PHRASE_TO_APP_IN_FOREGROUND:Ljava/lang/String; = "WakeUpPhraseToAppInFg"


# instance fields
.field private final audioWithWakeUpPhrase:Z

.field private final foreground:Z

.field private final phrase:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "phrase"    # Ljava/lang/String;
    .param p2, "foreground"    # Z

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->audioWithWakeUpPhrase:Z

    .line 34
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->phrase:Ljava/lang/String;

    .line 35
    iput-boolean p2, p0, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->foreground:Z

    .line 36
    return-void
.end method


# virtual methods
.method getConfigurationHeaders()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 67
    .local v0, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->isAudioWithWakeUpPhrase()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->phrase:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    const-string/jumbo v1, "AudioHasWakeUpPhrase"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const-string/jumbo v1, "WakeUpPhrase"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->phrase:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    const-string/jumbo v1, "WakeUpPhraseToAppInFg"

    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->foreground:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    :cond_0
    return-object v0
.end method

.method public getPhrase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->phrase:Ljava/lang/String;

    return-object v0
.end method

.method public isAudioWithWakeUpPhrase()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->audioWithWakeUpPhrase:Z

    return v0
.end method

.method public isForeground()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->foreground:Z

    return v0
.end method
