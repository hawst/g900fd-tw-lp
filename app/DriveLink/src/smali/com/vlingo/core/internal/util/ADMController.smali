.class public Lcom/vlingo/core/internal/util/ADMController;
.super Ljava/lang/Object;
.source "ADMController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/ADMController$1;,
        Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;
    }
.end annotation


# static fields
.field public static SAFEREADER:Ljava/lang/String;

.field protected static instance:Lcom/vlingo/core/internal/util/ADMController;


# instance fields
.field protected appModeBroadcastReceiver:Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;

.field private featureListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/ADMFeatureListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private featureStatuses:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected onBoot:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string/jumbo v0, "Feature.SAFEREADER"

    sput-object v0, Lcom/vlingo/core/internal/util/ADMController;->SAFEREADER:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/util/ADMController;->featureListeners:Ljava/util/HashMap;

    .line 33
    iput-object v2, p0, Lcom/vlingo/core/internal/util/ADMController;->appModeBroadcastReceiver:Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;

    .line 34
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/core/internal/util/ADMController;->onBoot:Z

    .line 53
    new-instance v1, Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;-><init>(Lcom/vlingo/core/internal/util/ADMController;Lcom/vlingo/core/internal/util/ADMController$1;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/util/ADMController;->appModeBroadcastReceiver:Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;

    .line 54
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 55
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 56
    const-string/jumbo v1, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 57
    const-string/jumbo v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 58
    const-string/jumbo v1, "com.vlingo.client.app.action.VLINGO_APP_START"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 59
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/util/ADMController;->appModeBroadcastReceiver:Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 60
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/util/ADMController;->featureStatuses:Ljava/util/HashMap;

    .line 61
    return-void
.end method

.method protected static getInstance()Lcom/vlingo/core/internal/util/ADMController;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/vlingo/core/internal/util/ADMController;->instance:Lcom/vlingo/core/internal/util/ADMController;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/vlingo/core/internal/util/ADMController;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/ADMController;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/ADMController;->instance:Lcom/vlingo/core/internal/util/ADMController;

    .line 67
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/util/ADMController;->instance:Lcom/vlingo/core/internal/util/ADMController;

    return-object v0
.end method


# virtual methods
.method public varargs addListener(Lcom/vlingo/core/internal/util/ADMFeatureListener;[Ljava/lang/String;)V
    .locals 6
    .param p1, "listener"    # Lcom/vlingo/core/internal/util/ADMFeatureListener;
    .param p2, "features"    # [Ljava/lang/String;

    .prologue
    .line 100
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 101
    .local v1, "feature":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ADMController;->featureListeners:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 102
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ADMController;->featureListeners:Ljava/util/HashMap;

    new-instance v5, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    invoke-virtual {v4, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    :cond_0
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ADMController;->featureStatuses:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    .line 105
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ADMController;->featureStatuses:Ljava/util/HashMap;

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    :cond_1
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ADMController;->featureListeners:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 100
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 109
    .end local v1    # "feature":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ADMController;->refreshFeatureStates()V

    .line 110
    return-void
.end method

.method protected getFeatureStatus(Ljava/lang/String;)Z
    .locals 2
    .param p1, "feature"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 89
    sget-object v1, Lcom/vlingo/core/internal/util/ADMController;->SAFEREADER:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    iget-boolean v1, p0, Lcom/vlingo/core/internal/util/ADMController;->onBoot:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "tos_accepted"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "iux_complete"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/CorePackageInfoProvider;->hasMessaging()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 96
    :cond_0
    return v0
.end method

.method protected refreshAppModeStates()V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method protected refreshFeatureStates()V
    .locals 6

    .prologue
    .line 71
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ADMController;->featureStatuses:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 72
    .local v1, "feature":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/util/ADMController;->getFeatureStatus(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 73
    .local v0, "enabled":Ljava/lang/Boolean;
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ADMController;->featureStatuses:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/vlingo/core/internal/util/ADMController;->featureStatuses:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 74
    :cond_1
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ADMController;->featureStatuses:Ljava/util/HashMap;

    invoke-virtual {v5, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ADMController;->featureListeners:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/util/ADMFeatureListener;

    .line 76
    .local v4, "listener":Lcom/vlingo/core/internal/util/ADMFeatureListener;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-interface {v4, v1, v5}, Lcom/vlingo/core/internal/util/ADMFeatureListener;->onStatusChanged(Ljava/lang/String;Z)V

    goto :goto_0

    .line 80
    .end local v0    # "enabled":Ljava/lang/Boolean;
    .end local v1    # "feature":Ljava/lang/String;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "listener":Lcom/vlingo/core/internal/util/ADMFeatureListener;
    :cond_2
    iget-boolean v5, p0, Lcom/vlingo/core/internal/util/ADMController;->onBoot:Z

    if-eqz v5, :cond_3

    .line 81
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ADMController;->refreshAppModeStates()V

    .line 82
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/vlingo/core/internal/util/ADMController;->onBoot:Z

    .line 84
    :cond_3
    return-void
.end method

.method public removeListener(Lcom/vlingo/core/internal/util/ADMFeatureListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/vlingo/core/internal/util/ADMFeatureListener;

    .prologue
    .line 113
    iget-object v2, p0, Lcom/vlingo/core/internal/util/ADMController;->featureListeners:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 114
    .local v1, "listeners":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/vlingo/core/internal/util/ADMFeatureListener;>;"
    invoke-virtual {v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    .end local v1    # "listeners":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/vlingo/core/internal/util/ADMFeatureListener;>;"
    :cond_0
    return-void
.end method
