.class public Lcom/vlingo/core/internal/schedule/TaskQueryObject;
.super Lcom/vlingo/core/internal/schedule/ScheduleTask;
.source "TaskQueryObject.java"


# instance fields
.field private count:I

.field private countFromEnd:Z

.field private matchCompleted:Z

.field private matchUndated:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;-><init>()V

    return-void
.end method

.method private matchTime(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Z
    .locals 7
    .param p1, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    const/4 v4, 0x0

    .line 51
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getCompleted()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matchCompleted:Z

    if-nez v5, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v4

    .line 55
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->getBegin()J

    move-result-wide v0

    .line 56
    .local v0, "queryBegin":J
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->getEnd()J

    move-result-wide v2

    .line 57
    .local v2, "queryEnd":J
    iget-boolean v5, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matchUndated:Z

    if-nez v5, :cond_3

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-lez v5, :cond_3

    .line 58
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->hasDueDate()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBegin()J

    move-result-wide v5

    cmp-long v5, v5, v0

    if-ltz v5, :cond_2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBegin()J

    move-result-wide v5

    cmp-long v5, v5, v2

    if-lez v5, :cond_3

    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderSet()I

    move-result v5

    if-lez v5, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v5

    cmp-long v5, v5, v0

    if-ltz v5, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v5

    cmp-long v5, v5, v2

    if-gtz v5, :cond_0

    .line 64
    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method


# virtual methods
.method public containsString(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Z
    .locals 2
    .param p1, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->title:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->title:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/StringUtils;->containsString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    const/4 v0, 0x0

    .line 86
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matchTime(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Z

    move-result v0

    goto :goto_0
.end method

.method public containsWord(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Z
    .locals 2
    .param p1, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->title:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->title:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/StringUtils;->containsWord(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    const/4 v0, 0x0

    .line 97
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matchTime(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Z

    move-result v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->count:I

    return v0
.end method

.method public getEnd()J
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMatchUndated()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matchUndated:Z

    return v0
.end method

.method public matchCompletedItems()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matchCompleted:Z

    .line 47
    return-void
.end method

.method public matchUndatedItems()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matchUndated:Z

    .line 39
    return-void
.end method

.method public matches(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Z
    .locals 2
    .param p1, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->title:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->title:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 75
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matchTime(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Z

    move-result v0

    goto :goto_0
.end method

.method public setCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 23
    if-gez p1, :cond_0

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->countFromEnd:Z

    .line 26
    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->count:I

    .line 27
    return-void
.end method

.method public shouldCountFromEnd()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->countFromEnd:Z

    return v0
.end method
