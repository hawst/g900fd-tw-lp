.class public final enum Lcom/vlingo/core/internal/ResourceIdProvider$layout;
.super Ljava/lang/Enum;
.source "ResourceIdProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/ResourceIdProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "layout"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/ResourceIdProvider$layout;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$layout;

.field public static final enum core_main:Lcom/vlingo/core/internal/ResourceIdProvider$layout;

.field public static final enum core_notification_safereader_on:Lcom/vlingo/core/internal/ResourceIdProvider$layout;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    const-string/jumbo v1, "core_main"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$layout;->core_main:Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    .line 60
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    const-string/jumbo v1, "core_notification_safereader_on"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/ResourceIdProvider$layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$layout;->core_notification_safereader_on:Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    .line 58
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$layout;->core_main:Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$layout;->core_notification_safereader_on:Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$layout;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/ResourceIdProvider$layout;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    const-class v0, Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/ResourceIdProvider$layout;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$layout;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/ResourceIdProvider$layout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    return-object v0
.end method
