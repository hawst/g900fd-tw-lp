.class public Lcom/vlingo/core/internal/util/TimerSingleton;
.super Ljava/lang/Object;
.source "TimerSingleton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;
    }
.end annotation


# static fields
.field private static timer:Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static declared-synchronized destroy()V
    .locals 2

    .prologue
    .line 25
    const-class v1, Lcom/vlingo/core/internal/util/TimerSingleton;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/util/TimerSingleton;->timer:Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;

    if-eqz v0, :cond_0

    .line 26
    sget-object v0, Lcom/vlingo/core/internal/util/TimerSingleton;->timer:Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;->cancelTimer()V

    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/util/TimerSingleton;->timer:Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :cond_0
    monitor-exit v1

    return-void

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getTimer()Ljava/util/Timer;
    .locals 2

    .prologue
    .line 18
    const-class v1, Lcom/vlingo/core/internal/util/TimerSingleton;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/util/TimerSingleton;->timer:Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/TimerSingleton;->timer:Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;

    .line 21
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/util/TimerSingleton;->timer:Lcom/vlingo/core/internal/util/TimerSingleton$TimerSingletonTimer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
