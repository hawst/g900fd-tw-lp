.class public Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;
.super Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
.source "JapaneseContactRuleSet.java"


# static fields
.field public static final FULL_MATCH:Ljava/lang/String; = "Full Match"

.field public static final PARTIAL_MATCH:Ljava/lang/String; = "Partial Match"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;-><init>()V

    return-void
.end method


# virtual methods
.method public canProcess(Ljava/lang/String;)Z
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->nameIsJapanese(Ljava/lang/String;)Z

    move-result v0

    .line 28
    .local v0, "japaneseName":Z
    if-eqz v0, :cond_0

    .line 29
    const/4 v1, 0x1

    .line 39
    :goto_0
    return v1

    .line 35
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ja-JP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 36
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isCJKString(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 39
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public generateRules(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 62
    :goto_0
    return-object v0

    .line 58
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v0, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    new-instance v1, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v2, "Full Match"

    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v5, 0x64

    invoke-direct {v4, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    new-instance v1, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v2, "Partial Match"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClausePartialMatch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v5, 0x28

    invoke-direct {v4, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;-><init>(Z)V

    return-object v0
.end method

.method public queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 66
    const/16 v1, 0xb

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "data3"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "data5"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "data3||data2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "data3||data5||data2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "data9"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "data7"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "data8"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "data9 || data7"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "data9 || data8 || data7"

    aput-object v2, v0, v1

    .line 79
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "vnd.android.cursor.item/name"

    invoke-static {p1, v1, v0}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithTwoWayNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public queryToWhereClausePartialMatch(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 83
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "data3||data2"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "data9"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "data7"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "data9 || data7"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "data9 || data8 || data7"

    aput-object v2, v0, v1

    .line 91
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "vnd.android.cursor.item/name"

    invoke-static {p1, v1, v0}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithTwoWayNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public queryToWhereClauseWithPhoneticFirstName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 108
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "data7"

    aput-object v2, v0, v1

    .line 111
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "vnd.android.cursor.item/name"

    invoke-static {p1, v1, v0}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithTwoWayNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public queryToWhereClauseWithPhoneticFullName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 97
    const/4 v1, 0x5

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "data9"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "data7"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "data8"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "data9 || data7"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "data9 || data8 || data7"

    aput-object v2, v0, v1

    .line 104
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "vnd.android.cursor.item/name"

    invoke-static {p1, v1, v0}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithTwoWayNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public queryToWhereClauseWithPhoneticLastName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 115
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "data9"

    aput-object v2, v0, v1

    .line 118
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "vnd.android.cursor.item/name"

    invoke-static {p1, v1, v0}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithTwoWayNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public skipExtraData()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method
