.class public final enum Lcom/vlingo/core/internal/dialogmanager/DecoratorType;
.super Ljava/lang/Enum;
.source "DecoratorType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/DecoratorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum CancelButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ConfirmButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowBirthday:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowHomeAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowHomeEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowHomePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowMobilePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowWorkAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowWorkEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ContactShowWorkPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum NoOp:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ReadOnly:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum Replaceable:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum SendButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum ShowMessageBody:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum TimerCancelCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum TimerResetCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum TimerRestartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum TimerShowCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum TimerStartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum TimerStopCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

.field public static final enum WebSearchButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "NoOp"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->NoOp:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 10
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ReadOnly"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ReadOnly:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 11
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "SendButton"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->SendButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 12
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "CancelButton"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->CancelButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 13
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ConfirmButton"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ConfirmButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "WebSearchButton"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->WebSearchButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 18
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "TimerStartCmd"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerStartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 19
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "TimerStopCmd"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerStopCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 20
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "TimerResetCmd"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerResetCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 21
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "TimerRestartCmd"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerRestartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 22
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "TimerCancelCmd"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerCancelCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 23
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "TimerShowCmd"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerShowCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 26
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowEmail"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowHomeEmail"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomeEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 28
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowWorkEmail"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 29
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowBirthday"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowBirthday:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 30
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowAddress"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 31
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowWorkAddress"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 32
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowHomeAddress"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomeAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 33
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowPhone"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 34
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowWorkPhone"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 35
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowHomePhone"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 36
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ContactShowMobilePhone"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowMobilePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 40
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "ShowMessageBody"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ShowMessageBody:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 43
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    const-string/jumbo v1, "Replaceable"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->Replaceable:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 8
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->NoOp:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ReadOnly:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->SendButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->CancelButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ConfirmButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->WebSearchButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerStartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerStopCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerResetCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerRestartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerCancelCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerShowCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomeEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowBirthday:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomeAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowMobilePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ShowMessageBody:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->Replaceable:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DecoratorType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/DecoratorType;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    return-object v0
.end method
