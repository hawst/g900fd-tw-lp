.class public Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;
.super Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
.source "ChineseLocalSearchListing.java"


# static fields
.field public static final FIELD_ADDR_CITY:Ljava/lang/String; = "city"

.field public static final FIELD_ADDR_STREET_NAME:Ljava/lang/String; = "address"

.field public static final FIELD_CLICK_URL:Ljava/lang/String; = "business_url"

.field public static final FIELD_DISTANCE:Ljava/lang/String; = "distance"

.field public static final FIELD_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final FIELD_LISTING_ID:Ljava/lang/String; = "business_id"

.field public static final FIELD_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final FIELD_NAME:Ljava/lang/String; = "name"

.field public static final FIELD_PHONE_NUMBER:Ljava/lang/String; = "telephone"

.field public static final FIELD_PROVIDER:Ljava/lang/String; = "Provider"

.field public static final FIELD_RATING:Ljava/lang/String; = "avg_rating"

.field public static final FIELD_REVIEW_COUNT:Ljava/lang/String; = "review_count"

.field public static final FIELD_REVIEW_ID:Ljava/lang/String; = "review_id"

.field public static final FIELD_REVIEW_RATING:Ljava/lang/String; = "review_rating"

.field public static final FIELD_REVIEW_REVIEW_DATE:Ljava/lang/String; = "created_time"

.field public static final FIELD_REVIEW_TEXT:Ljava/lang/String; = "text_excerpt"

.field public static final FIELD_REVIEW_USER_NAME:Ljava/lang/String; = "user_nickname"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "valueBusinessID"    # Ljava/lang/String;

    .prologue
    .line 31
    const-string/jumbo v0, "business_id"

    invoke-direct {p0, v0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method private getAddressLine1(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 116
    const-string/jumbo v0, "address"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const-string/jumbo v0, "address"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    return-void
.end method

.method private getAddressLine2(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 122
    const-string/jumbo v0, "city"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 124
    const-string/jumbo v0, ", "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_0
    const-string/jumbo v0, "city"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :cond_1
    return-void
.end method


# virtual methods
.method public getAddressLine1()Ljava/lang/String;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getAddressLine1(Ljava/lang/StringBuilder;)V

    .line 106
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getAddressLine2()Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getAddressLine2(Ljava/lang/StringBuilder;)V

    .line 112
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getCaption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCityState()Ljava/lang/String;
    .locals 2

    .prologue
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "city"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    const-string/jumbo v1, "city"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getClickUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    const-string/jumbo v0, "business_url"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDistanceString()Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/high16 v5, -0x4010000000000000L    # -1.0

    .line 69
    const-string/jumbo v4, "distance"

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 70
    const-string/jumbo v4, "distance"

    invoke-virtual {p0, v4, v5, v6}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    .line 71
    .local v0, "distance":D
    cmpl-double v4, v0, v5

    if-eqz v4, :cond_0

    .line 72
    new-instance v2, Ljava/text/DecimalFormat;

    const-string/jumbo v4, "######.##"

    invoke-direct {v2, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 73
    .local v2, "distanceFormat":Ljava/text/DecimalFormat;
    const-string/jumbo v3, " m"

    .line 74
    .local v3, "units":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 77
    .end local v0    # "distance":D
    .end local v2    # "distanceFormat":Ljava/text/DecimalFormat;
    .end local v3    # "units":Ljava/lang/String;
    :goto_0
    return-object v4

    :cond_0
    const-string/jumbo v4, ""

    goto :goto_0
.end method

.method public getFullAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "address"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    const-string/jumbo v1, "address"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    :cond_0
    const-string/jumbo v1, "city"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 87
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 88
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    :cond_1
    const-string/jumbo v1, "city"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getLatitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    const-string/jumbo v0, "latitude"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListingID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const-string/jumbo v0, "business_id"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLongitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    const-string/jumbo v0, "longitude"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    const-string/jumbo v0, "name"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string/jumbo v0, "telephone"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneNumberFormatted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    const-string/jumbo v0, "Provider"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRating()D
    .locals 3

    .prologue
    .line 53
    const-string/jumbo v0, "avg_rating"

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public getReserveUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return-object v0
.end method

.method public getReviewCount()I
    .locals 2

    .prologue
    .line 49
    const-string/jumbo v0, "review_count"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getInteger(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getSynopsis()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return-object v0
.end method

.method public isOrganic()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public isSponsored()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, " LocalSearchChineseListing ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 163
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "\n Name :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    const-string/jumbo v1, "\n AddressLine1 :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getAddressLine1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    const-string/jumbo v1, "\n AddressLine2 :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getAddressLine2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    const-string/jumbo v1, "\n CityState :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getCityState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    const-string/jumbo v1, "\n FullAddress :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getFullAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    const-string/jumbo v1, "\n DistanceString :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getDistanceString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    const-string/jumbo v1, "\n ListingID :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getListingID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const-string/jumbo v1, "\n PhoneNumber :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    const-string/jumbo v1, "\n Rating :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getRating()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 172
    const-string/jumbo v1, "\n ReviewCount :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getReviewCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 173
    const-string/jumbo v1, "\n Reviews : {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->getReviews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const-string/jumbo v1, "\n hasReviews :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->hasReviews()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 175
    const-string/jumbo v1, "\n isOrganic :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->isOrganic()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 176
    const-string/jumbo v1, "\n isSponsored :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->isSponsored()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 177
    const-string/jumbo v1, "\n areMoreDetailsAvailable :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->areMoreDetailsAvailable()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 179
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
