.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "VoiceDialPageHandler.java"


# instance fields
.field address:Ljava/lang/String;

.field controller:Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

.field messages:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->controller:Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

    .line 28
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->address:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->messages:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v7, 0x0

    .line 35
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 37
    const-string/jumbo v6, "Action"

    invoke-static {p1, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 39
    .local v3, "paramAction":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v6

    const-string/jumbo v8, "voicedial"

    invoke-virtual {v6, v8}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 41
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/Controller;

    .line 42
    .local v5, "tmpController":Lcom/vlingo/core/internal/dialogmanager/Controller;
    if-eqz v5, :cond_0

    instance-of v6, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

    if-nez v6, :cond_1

    .line 43
    :cond_0
    const-class v6, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v5

    .line 45
    :cond_1
    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

    .end local v5    # "tmpController":Lcom/vlingo/core/internal/dialogmanager/Controller;
    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->controller:Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

    .line 47
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/LinkedList;

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->messages:Ljava/util/LinkedList;

    .line 48
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->messages:Ljava/util/LinkedList;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->messages:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 49
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->messages:Ljava/util/LinkedList;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->address:Ljava/lang/String;

    .line 50
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->messages:Ljava/util/LinkedList;

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "contactName":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->address:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 52
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->controller:Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->address:Ljava/lang/String;

    invoke-virtual {v6, v8, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->redial(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v7

    .line 85
    .end local v0    # "contactName":Ljava/lang/String;
    :goto_0
    return v1

    .line 57
    :cond_2
    const/4 v1, 0x0

    .line 59
    .local v1, "isAsync":Z
    if-eqz v3, :cond_6

    const-string/jumbo v6, "redial"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 60
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/ContactUtil;->getLastOutgoingCall(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->address:Ljava/lang/String;

    .line 61
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v6

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->address:Ljava/lang/String;

    invoke-static {v6, v8}, Lcom/vlingo/core/internal/util/ContactUtil;->getContactDisplayNameByAddress(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .restart local v0    # "contactName":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_call_log:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v6, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 66
    .local v2, "noLogErrorMessage":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->address:Ljava/lang/String;

    if-eqz v6, :cond_5

    .line 67
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isEyesFree()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMessageReadbackFlowEnabled()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 68
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->address:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_redial_confirm_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    :goto_1
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v7

    invoke-static {v6, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 69
    .local v4, "redialConfirmationString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v6

    const-string/jumbo v8, "vp_car_confirm"

    invoke-static {v8}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v8

    invoke-virtual {v6, v4, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 70
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->controller:Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

    invoke-virtual {v6, p2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 71
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->controller:Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->address:Ljava/lang/String;

    invoke-virtual {v6, v8, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->confirmRedial(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v7

    .line 72
    goto :goto_0

    .line 68
    .end local v4    # "redialConfirmationString":Ljava/lang/String;
    :cond_3
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_redial_confirm_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    goto :goto_1

    .line 74
    :cond_4
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->controller:Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->address:Ljava/lang/String;

    invoke-virtual {v6, v8, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->redial(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v7

    .line 75
    goto :goto_0

    .line 78
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    invoke-interface {v6, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 82
    .end local v0    # "contactName":Ljava/lang/String;
    .end local v2    # "noLogErrorMessage":Ljava/lang/String;
    :cond_6
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;->controller:Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;

    invoke-virtual {v6, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v1

    goto/16 :goto_0
.end method
