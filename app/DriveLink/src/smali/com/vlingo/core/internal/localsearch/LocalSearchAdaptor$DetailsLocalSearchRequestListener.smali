.class Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;
.super Ljava/lang/Object;
.source "LocalSearchAdaptor.java"

# interfaces
.implements Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetailsLocalSearchRequestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

.field private widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 0
    .param p2, "widgetDetailResponseListener"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 317
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    .line 318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 319
    iput-object p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 320
    return-void
.end method


# virtual methods
.method public onRequestComplete(ZLjava/lang/Object;)V
    .locals 2
    .param p1, "success"    # Z
    .param p2, "items"    # Ljava/lang/Object;

    .prologue
    .line 327
    instance-of v0, p2, Landroid/util/Pair;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 328
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    check-cast p2, Landroid/util/Pair;

    .end local p2    # "items":Ljava/lang/Object;
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->setLocalSearchDetailsListing(Ljava/util/Vector;)V

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onResponseReceived()V

    .line 331
    return-void
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;)V
    .locals 2
    .param p1, "failureType"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    .prologue
    .line 340
    sget-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$1;->$SwitchMap$com$vlingo$core$internal$localsearch$LocalSearchRequestListener$LocalSearchFailureType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 352
    :goto_0
    return-void

    .line 342
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 345
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 348
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 340
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onRequestFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "failReasonParam"    # Ljava/lang/String;

    .prologue
    .line 335
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    .line 336
    return-void
.end method

.method public onRequestScheduled()V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestScheduled()V

    .line 357
    return-void
.end method
