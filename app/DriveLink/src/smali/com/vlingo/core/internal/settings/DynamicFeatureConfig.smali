.class public final enum Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;
.super Ljava/lang/Enum;
.source "DynamicFeatureConfig.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum ALARM:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum ALARM_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum ALARM_DELETE:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum ALARM_EDIT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum ALARM_LOOKUP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum ANSWER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum CONTACT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum EMAIL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum EVENT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum EVENT_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum EVENT_DELETE:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum EVENT_EDIT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum EVENT_LOOKUP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum LOCALSEARCH:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum MAPS:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum MEMO:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum MEMO_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum MESSAGING:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum MUSIC:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum NAVIGATION:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum OPENAPP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum RADIO:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum READBACK:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final REASON_ACCOUNT:Ljava/lang/String; = "Account"

.field public static final REASON_CARRIER:Ljava/lang/String; = "Carrier"

.field public static final REASON_DEVICE:Ljava/lang/String; = "Device"

.field public static final REASON_LANGUAGE:Ljava/lang/String; = "Language"

.field public static final REASON_LOCATION:Ljava/lang/String; = "Location"

.field public static final REASON_SMS_LOCK:Ljava/lang/String; = "SmsLock"

.field public static final REASON_VERSION:Ljava/lang/String; = "Version"

.field public static final enum SEARCH:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum SETTINGS:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum SOCIAL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field private static final STATUS_DISABLED:Ljava/lang/String; = "DISABLED"

.field public static final STATUS_ENABLED:Ljava/lang/String; = "ENABLED"

.field public static final enum STOCK:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum TASK:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum TASK_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum TASK_DELETE:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum TASK_EDIT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum TASK_LOOKUP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum TIMER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum VOICERECORD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

.field public static final enum WEATHER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;


# instance fields
.field private final settingName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 20
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "ALARM"

    const-string/jumbo v2, "alarm"

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 24
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "ALARM_LOOKUP"

    const-string/jumbo v2, "alarm_lookup"

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM_LOOKUP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 28
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "ALARM_ADD"

    const-string/jumbo v2, "alarm_add"

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 32
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "ALARM_DELETE"

    const-string/jumbo v2, "alarm_delete"

    invoke-direct {v0, v1, v7, v2}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM_DELETE:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 36
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "ALARM_EDIT"

    const-string/jumbo v2, "alarm_edit"

    invoke-direct {v0, v1, v8, v2}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM_EDIT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 40
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "ANSWER"

    const/4 v2, 0x5

    const-string/jumbo v3, "answer"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ANSWER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 44
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "CALL"

    const/4 v2, 0x6

    const-string/jumbo v3, "call"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 48
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "CONTACT"

    const/4 v2, 0x7

    const-string/jumbo v3, "contact"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CONTACT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 52
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "EMAIL"

    const/16 v2, 0x8

    const-string/jumbo v3, "email"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EMAIL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 56
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "EVENT"

    const/16 v2, 0x9

    const-string/jumbo v3, "event"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 60
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "EVENT_LOOKUP"

    const/16 v2, 0xa

    const-string/jumbo v3, "event_lookup"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT_LOOKUP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 64
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "EVENT_ADD"

    const/16 v2, 0xb

    const-string/jumbo v3, "event_add"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 68
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "EVENT_EDIT"

    const/16 v2, 0xc

    const-string/jumbo v3, "event_edit"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT_EDIT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 72
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "EVENT_DELETE"

    const/16 v2, 0xd

    const-string/jumbo v3, "event_delete"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT_DELETE:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 76
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "LOCALSEARCH"

    const/16 v2, 0xe

    const-string/jumbo v3, "localsearch"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->LOCALSEARCH:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 80
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "MAPS"

    const/16 v2, 0xf

    const-string/jumbo v3, "maps"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MAPS:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 84
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "MEMO"

    const/16 v2, 0x10

    const-string/jumbo v3, "memo"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MEMO:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 88
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "MEMO_ADD"

    const/16 v2, 0x11

    const-string/jumbo v3, "memo_add"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MEMO_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 92
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "MESSAGING"

    const/16 v2, 0x12

    const-string/jumbo v3, "messaging"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MESSAGING:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 96
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "MUSIC"

    const/16 v2, 0x13

    const-string/jumbo v3, "music"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MUSIC:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 100
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "NAVIGATION"

    const/16 v2, 0x14

    const-string/jumbo v3, "navigation"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->NAVIGATION:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 104
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "OPENAPP"

    const/16 v2, 0x15

    const-string/jumbo v3, "openapp"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->OPENAPP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 108
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "RADIO"

    const/16 v2, 0x16

    const-string/jumbo v3, "radio"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->RADIO:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 113
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "READBACK"

    const/16 v2, 0x17

    const-string/jumbo v3, "readback"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->READBACK:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 117
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "SEARCH"

    const/16 v2, 0x18

    const-string/jumbo v3, "search"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->SEARCH:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 121
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "SETTINGS"

    const/16 v2, 0x19

    const-string/jumbo v3, "settings"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->SETTINGS:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 125
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "SOCIAL"

    const/16 v2, 0x1a

    const-string/jumbo v3, "social"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->SOCIAL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 129
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "STOCK"

    const/16 v2, 0x1b

    const-string/jumbo v3, "stock"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->STOCK:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 133
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "TASK"

    const/16 v2, 0x1c

    const-string/jumbo v3, "task"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 137
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "TASK_LOOKUP"

    const/16 v2, 0x1d

    const-string/jumbo v3, "task_lookup"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK_LOOKUP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 141
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "TASK_EDIT"

    const/16 v2, 0x1e

    const-string/jumbo v3, "task_edit"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK_EDIT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 145
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "TASK_DELETE"

    const/16 v2, 0x1f

    const-string/jumbo v3, "task_delete"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK_DELETE:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 149
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "TASK_ADD"

    const/16 v2, 0x20

    const-string/jumbo v3, "task_add"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 153
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "TIMER"

    const/16 v2, 0x21

    const-string/jumbo v3, "timer"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TIMER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 157
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "VOICERECORD"

    const/16 v2, 0x22

    const-string/jumbo v3, "voicerecord"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->VOICERECORD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 161
    new-instance v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v1, "WEATHER"

    const/16 v2, 0x23

    const-string/jumbo v3, "weather"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->WEATHER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 15
    const/16 v0, 0x24

    new-array v0, v0, [Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM_LOOKUP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM_DELETE:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM_EDIT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ANSWER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CONTACT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EMAIL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT_LOOKUP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT_EDIT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT_DELETE:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->LOCALSEARCH:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MAPS:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MEMO:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MEMO_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MESSAGING:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MUSIC:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->NAVIGATION:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->OPENAPP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->RADIO:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->READBACK:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->SEARCH:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->SETTINGS:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->SOCIAL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->STOCK:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK_LOOKUP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK_EDIT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK_DELETE:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TIMER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->VOICERECORD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->WEATHER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->$VALUES:[Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "settingName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 178
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 179
    iput-object p3, p0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->settingName:Ljava/lang/String;

    .line 180
    return-void
.end method

.method private disable(Ljava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 238
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, ""

    .line 239
    .local v0, "suffix":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->settingName:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "DISABLED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    return-void

    .line 238
    .end local v0    # "suffix":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getDynamicFeatureConfigMap()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    new-instance v4, Ljava/util/HashMap;

    invoke-static {}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->values()[Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-result-object v5

    array-length v5, v5

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(I)V

    .line 202
    .local v4, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->values()[Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 203
    .local v1, "dynamicFeatureConfig":Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;
    iget-object v5, v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->settingName:Ljava/lang/String;

    invoke-direct {v1}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->getSettingValue()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 205
    .end local v1    # "dynamicFeatureConfig":Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;
    :cond_0
    return-object v4
.end method

.method private getSettingValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->settingName:Ljava/lang/String;

    const-string/jumbo v1, "ENABLED"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final initDisabledFeatures(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p0, "disabledFeatures":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    .line 192
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 193
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .line 195
    .local v1, "feature":Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->disable(Ljava/lang/String;)V

    goto :goto_0

    .line 198
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;Ljava/lang/String;>;"
    .end local v1    # "feature":Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->$VALUES:[Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    return-object v0
.end method


# virtual methods
.method public getReason()Ljava/lang/String;
    .locals 3

    .prologue
    .line 220
    invoke-direct {p0}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->getSettingValue()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 221
    .local v0, "parts":[Ljava/lang/String;
    const/4 v1, 0x2

    array-length v2, v0

    if-ne v1, v2, :cond_0

    .line 222
    const/4 v1, 0x1

    aget-object v1, v0, v1

    .line 224
    :goto_0
    return-object v1

    :cond_0
    const-string/jumbo v1, ""

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 2

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->getSettingValue()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ENABLED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
