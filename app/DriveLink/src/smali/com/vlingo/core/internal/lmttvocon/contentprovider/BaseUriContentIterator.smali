.class abstract Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;
.super Ljava/lang/Object;
.source "BaseUriContentIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final cursor:Landroid/database/Cursor;

.field private next:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "projection"    # [Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 19
    .local p0, "this":Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;, "Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->cursor:Landroid/database/Cursor;

    .line 24
    return-void
.end method

.method private closeCursor()V
    .locals 1

    .prologue
    .line 71
    .local p0, "this":Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;, "Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 74
    :cond_0
    return-void
.end method

.method private isCursorClosed()Z
    .locals 1

    .prologue
    .line 67
    .local p0, "this":Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;, "Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 28
    .local p0, "this":Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;, "Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->next:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    if-nez v0, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->next()Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->next:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->next:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;, "Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator<TT;>;"
    const/4 v2, 0x0

    .line 39
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->next:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    if-eqz v3, :cond_0

    .line 40
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->next:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    .line 41
    .local v1, "result":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;, "TT;"
    iput-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->next:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    .line 56
    .end local v1    # "result":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;, "TT;"
    :goto_0
    return-object v1

    .line 44
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->isCursorClosed()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v1, v2

    .line 45
    goto :goto_0

    .line 47
    :cond_1
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 48
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->cursor:Landroid/database/Cursor;

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->parseItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    move-result-object v0

    .line 49
    .local v0, "item":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;, "TT;"
    if-eqz v0, :cond_1

    move-object v1, v0

    .line 50
    goto :goto_0

    .line 55
    .end local v0    # "item":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;, "TT;"
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->closeCursor()V

    move-object v1, v2

    .line 56
    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    .local p0, "this":Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;, "Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->next()Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    move-result-object v0

    return-object v0
.end method

.method protected abstract parseItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation
.end method

.method public remove()V
    .locals 0

    .prologue
    .line 64
    .local p0, "this":Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;, "Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator<TT;>;"
    return-void
.end method
