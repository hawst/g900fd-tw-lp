.class public abstract Lcom/vlingo/core/internal/audio/AudioRequest;
.super Ljava/lang/Object;
.source "AudioRequest.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# static fields
.field public static final FLAG_IGNORE_STOP_REQUEST:I = 0x2

.field public static final FLAG_KEEP_MEDIAPLAYER_OPEN_ON_COMPLETION:I = 0x4

.field public static final FLAG_OVERRIDE_CURRENTLY_PLAYING_AUDIO:I = 0x1

.field public static final FLAG_PLAY_DURING_RECOGNITION:I = 0x8


# instance fields
.field public audioFocusType:I

.field public audioStream:I

.field public flags:I

.field listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field public requestAudioFocus:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x3

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->audioStream:I

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->flags:I

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->requestAudioFocus:Z

    .line 28
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->audioFocusType:I

    .line 31
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public hasFlag(I)Z
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 49
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->flags:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 60
    :cond_0
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 66
    :cond_0
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    .line 72
    :cond_0
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 78
    :cond_0
    return-void
.end method

.method public onSetDataSourceFailed()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public abstract prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/AudioPlayer;)Z
.end method

.method public abstract setDataSource(Landroid/content/Context;Landroid/media/MediaPlayer;Lcom/vlingo/core/internal/audio/AudioPlayer;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public setFlag(I)V
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 45
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->flags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->flags:I

    .line 46
    return-void
.end method

.method public setStream(I)V
    .locals 0
    .param p1, "stream"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/vlingo/core/internal/audio/AudioRequest;->audioStream:I

    .line 37
    return-void
.end method
