.class public interface abstract Lcom/vlingo/core/internal/questions/Answer;
.super Ljava/lang/Object;
.source "Answer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/questions/Answer$Subsection;,
        Lcom/vlingo/core/internal/questions/Answer$Section;
    }
.end annotation


# virtual methods
.method public abstract getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;
.end method

.method public abstract getSection(Ljava/lang/String;)Lcom/vlingo/core/internal/questions/Answer$Section;
.end method

.method public abstract getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;
.end method

.method public abstract getSimpleResponse()Ljava/lang/String;
.end method

.method public abstract hasAnswer()Z
.end method

.method public abstract hasMoreInformation()Z
.end method

.method public abstract hasSection(Ljava/lang/String;)Z
.end method
