.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "SetParamsHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private setAutoConfirmTimeout(Ljava/lang/String;)V
    .locals 3
    .param p1, "timeout"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    :try_start_0
    const-string/jumbo v1, "DIALOG_MANAGER_AUTO_CONFIRM_TIMEOUT"

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "AutoConfirm timeout not specified as an integer"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setEndpoint(Ljava/lang/String;)V
    .locals 3
    .param p1, "endPoint"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 36
    :try_start_0
    const-string/jumbo v1, "DIALOG_MANAGER_ENDPOINT_THRESHOLD"

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Endpoint not specified as an integer"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setWorkingMessageUpdate(Ljava/lang/String;)V
    .locals 3
    .param p1, "interval"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 53
    :try_start_0
    const-string/jumbo v1, "DIALOG_MANAGER_WORKING_MESSAGE_INTERVAL"

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Message update interval not specified as an integer"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setWorkingMessages(Ljava/lang/String;)V
    .locals 1
    .param p1, "messages"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    const-string/jumbo v0, "DIALOG_MANAGER_WORKING_MESSAGES"

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 26
    const-string/jumbo v0, "endpoint.threshold.msec"

    invoke-interface {p1, v0}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;->setEndpoint(Ljava/lang/String;)V

    .line 27
    const-string/jumbo v0, "working.msgs"

    invoke-interface {p1, v0}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;->setWorkingMessages(Ljava/lang/String;)V

    .line 28
    const-string/jumbo v0, "working.rotate.msec"

    invoke-interface {p1, v0}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;->setWorkingMessageUpdate(Ljava/lang/String;)V

    .line 29
    const-string/jumbo v0, "autoconfirm.timeout.msec"

    invoke-interface {p1, v0}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;->setAutoConfirmTimeout(Ljava/lang/String;)V

    .line 30
    const/4 v0, 0x0

    return v0
.end method
