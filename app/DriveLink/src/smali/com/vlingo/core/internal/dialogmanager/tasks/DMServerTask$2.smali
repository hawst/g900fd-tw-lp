.class Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;
.super Ljava/lang/Object;
.source "DMServerTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performRecoTransactionSub()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 281
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 282
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$300(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 287
    .local v0, "startRecoTone":I
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isDSPSeamlessWakeupEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 288
    const/4 v0, 0x0

    .line 291
    :cond_0
    if-lez v0, :cond_5

    .line 293
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->useAudioTrackTonePlayer:Z
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$400(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 294
    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "[LatencyCheck] useAudioTrackTonePlayer & TonePlayer.play"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    new-instance v1, Lcom/vlingo/core/internal/audio/TonePlayer;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->getAudioType()Lcom/vlingo/core/internal/audio/AudioType;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1000(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/audio/AudioType;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/vlingo/core/internal/audio/TonePlayer;-><init>(ILcom/vlingo/core/internal/audio/AudioType;)V

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;)V

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/audio/TonePlayer;->play(Lcom/vlingo/core/internal/audio/TonePlayer$Listener;)V

    .line 334
    :goto_1
    return-void

    .line 282
    .end local v0    # "startRecoTone":I
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v0

    goto :goto_0

    .line 284
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$300(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v1

    if-eqz v1, :cond_3

    .restart local v0    # "startRecoTone":I
    :goto_2
    goto :goto_0

    .end local v0    # "startRecoTone":I
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->getToneId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v0

    goto :goto_2

    .line 322
    .restart local v0    # "startRecoTone":I
    :cond_4
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$2;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2$2;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;)V

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playTone(ILcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    goto :goto_1

    .line 332
    :cond_5
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->DataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    move-result-object v2

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performReco(Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$700(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V

    goto :goto_1
.end method
