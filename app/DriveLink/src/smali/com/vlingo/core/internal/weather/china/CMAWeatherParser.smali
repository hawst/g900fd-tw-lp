.class public Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;
.super Ljava/lang/Object;
.source "CMAWeatherParser.java"


# instance fields
.field private weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

.field private weatherPhenomenonStrings:[Ljava/lang/String;

.field private windDirectionStrings:[Ljava/lang/String;

.field private windForceStrings:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)V
    .locals 2
    .param p1, "weatherElement"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .line 35
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_phenomenon:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherPhenomenonStrings:[Ljava/lang/String;

    .line 36
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_wind_force:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->windForceStrings:[Ljava/lang/String;

    .line 37
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_wind_direction:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->windDirectionStrings:[Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->lastUpdate:Ljava/util/Date;

    .line 39
    return-void
.end method

.method private fillCityInfo(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "cityInfo"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    const-string/jumbo v1, "c3"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameCity:Ljava/lang/String;

    .line 142
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    const-string/jumbo v1, "c7"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameProvince:Ljava/lang/String;

    .line 143
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    const-string/jumbo v1, "c9"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameCountry:Ljava/lang/String;

    .line 149
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    const-string/jumbo v1, "c2"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->toFirstUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameCity:Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    const-string/jumbo v1, "c6"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->toFirstUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameProvince:Ljava/lang/String;

    .line 147
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    const-string/jumbo v1, "c8"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->toFirstUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameCountry:Ljava/lang/String;

    goto :goto_0
.end method

.method private fillCurrentConditions(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "currentConditions"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;-><init>()V

    .line 98
    .local v0, "weatherCurrent":Lcom/vlingo/core/internal/weather/china/WeatherCurrent;
    const-string/jumbo v1, "l1"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->temperature:Ljava/lang/String;

    .line 99
    iget-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->temperature:Ljava/lang/String;

    const-string/jumbo v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, ""

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->temperature:Ljava/lang/String;

    .line 100
    :cond_0
    const-string/jumbo v1, "l5"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->weatherCode:Ljava/lang/String;

    .line 101
    iget-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->weatherCode:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getPhenomenonStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->weatherPhenomenon:Ljava/lang/String;

    .line 102
    const-string/jumbo v1, "l6"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->precipitation:Ljava/lang/String;

    .line 103
    const-string/jumbo v1, "l2"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->humidity:Ljava/lang/String;

    .line 104
    const-string/jumbo v1, "l3"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getWindForceStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->windForce:Ljava/lang/String;

    .line 105
    const-string/jumbo v1, "l4"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getWindDirectionStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->windDirection:Ljava/lang/String;

    .line 106
    const-string/jumbo v1, "l7"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->timePublished:Ljava/lang/String;

    .line 107
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iput-object v0, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    .line 108
    return-void
.end method

.method private fillNode(Lorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 1
    .param p1, "cityInfo"    # Lorg/json/JSONObject;
    .param p2, "forecasts"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->fillCityInfo(Lorg/json/JSONObject;)V

    .line 71
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 72
    .local v0, "date":Ljava/util/Date;
    invoke-direct {p0, p2, v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->fillWeatherSevenDays(Lorg/json/JSONArray;Ljava/util/Date;)V

    .line 73
    return-void
.end method

.method private fillNode(Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "cityInfo"    # Lorg/json/JSONObject;
    .param p2, "currentConditions"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lcom/vlingo/core/internal/weather/china/CMALocationElement;

    invoke-direct {v0}, Lcom/vlingo/core/internal/weather/china/CMALocationElement;-><init>()V

    .line 77
    .local v0, "locationElement":Lcom/vlingo/core/internal/weather/china/CMALocationElement;
    const-string/jumbo v1, "lon"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/CMALocationElement;->longitude:Ljava/lang/String;

    .line 78
    const-string/jumbo v1, "lat"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/CMALocationElement;->latitude:Ljava/lang/String;

    .line 79
    const-string/jumbo v1, "areaid"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/weather/china/CMALocationElement;->areaID:Ljava/lang/String;

    .line 80
    if-eqz p2, :cond_0

    .line 81
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->fillCurrentConditions(Lorg/json/JSONObject;)V

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iput-object v0, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->location:Lcom/vlingo/core/internal/weather/china/CMALocationElement;

    .line 84
    return-void
.end method

.method private fillNode(Lorg/json/JSONObject;Lorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 1
    .param p1, "cityInfo"    # Lorg/json/JSONObject;
    .param p2, "currentConditions"    # Lorg/json/JSONObject;
    .param p3, "forecast"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->fillCityInfo(Lorg/json/JSONObject;)V

    .line 89
    if-eqz p2, :cond_0

    .line 90
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->fillCurrentConditions(Lorg/json/JSONObject;)V

    .line 92
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 93
    .local v0, "date":Ljava/util/Date;
    invoke-direct {p0, p3, v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->fillWeatherSevenDays(Lorg/json/JSONArray;Ljava/util/Date;)V

    .line 94
    return-void
.end method

.method private fillWeatherSevenDays(Lorg/json/JSONArray;Ljava/util/Date;)V
    .locals 6
    .param p1, "forecast"    # Lorg/json/JSONArray;
    .param p2, "date"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 113
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_8

    .line 114
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 115
    .local v2, "jSonForecast":Lorg/json/JSONObject;
    new-instance v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    invoke-direct {v3}, Lcom/vlingo/core/internal/weather/china/WeatherItem;-><init>()V

    .line 116
    .local v3, "weatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    new-instance v4, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    invoke-direct {v4}, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;-><init>()V

    iput-object v4, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    .line 117
    iget-object v5, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    const-string/jumbo v4, "fa"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "0"

    :goto_1
    iput-object v4, v5, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    .line 118
    iget-object v4, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v5, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v5, v5, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getPhenomenonStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherPhenomenon:Ljava/lang/String;

    .line 119
    iget-object v5, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    const-string/jumbo v4, "fc"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string/jumbo v4, ""

    :goto_2
    iput-object v4, v5, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    .line 120
    iget-object v5, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    const-string/jumbo v4, "fe"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string/jumbo v4, ""

    :goto_3
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getWindDirectionStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->windDirection:Ljava/lang/String;

    .line 121
    iget-object v5, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    const-string/jumbo v4, "fg"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string/jumbo v4, ""

    :goto_4
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getWindForceStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->windForce:Ljava/lang/String;

    .line 122
    new-instance v4, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    invoke-direct {v4}, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;-><init>()V

    iput-object v4, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    .line 123
    iget-object v5, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    const-string/jumbo v4, "fb"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string/jumbo v4, "99"

    :goto_5
    iput-object v4, v5, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    .line 124
    iget-object v4, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v5, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v5, v5, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getPhenomenonStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherPhenomenon:Ljava/lang/String;

    .line 125
    iget-object v5, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    const-string/jumbo v4, "fd"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string/jumbo v4, ""

    :goto_6
    iput-object v4, v5, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    .line 126
    iget-object v5, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    const-string/jumbo v4, "ff"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string/jumbo v4, ""

    :goto_7
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getWindDirectionStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->windDirection:Ljava/lang/String;

    .line 127
    iget-object v5, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    const-string/jumbo v4, "fh"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string/jumbo v4, ""

    :goto_8
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getWindForceStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->windForce:Ljava/lang/String;

    .line 128
    const-string/jumbo v4, "fi"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getSunrise(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->sunrise:Ljava/lang/String;

    .line 129
    const-string/jumbo v4, "fi"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getSunset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->sunset:Ljava/lang/String;

    .line 131
    :try_start_0
    invoke-direct {p0, v1, p2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->getDate(ILjava/util/Date;)Ljava/util/Date;

    move-result-object v4

    iput-object v4, v3, Lcom/vlingo/core/internal/weather/china/WeatherItem;->date:Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_9
    iget-object v4, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v4, v4, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 117
    :cond_0
    const-string/jumbo v4, "fa"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 119
    :cond_1
    const-string/jumbo v4, "fc"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 120
    :cond_2
    const-string/jumbo v4, "fe"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 121
    :cond_3
    const-string/jumbo v4, "fg"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    .line 123
    :cond_4
    const-string/jumbo v4, "fb"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    .line 125
    :cond_5
    const-string/jumbo v4, "fd"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    .line 126
    :cond_6
    const-string/jumbo v4, "ff"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_7

    .line 127
    :cond_7
    const-string/jumbo v4, "fh"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_9

    .line 137
    .end local v0    # "e":Ljava/text/ParseException;
    .end local v2    # "jSonForecast":Lorg/json/JSONObject;
    .end local v3    # "weatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    :cond_8
    return-void
.end method

.method private getDate(ILjava/util/Date;)Ljava/util/Date;
    .locals 7
    .param p1, "day"    # I
    .param p2, "date"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 237
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    int-to-long v3, p1

    sget-wide v5, Lcom/vlingo/core/internal/schedule/DateUtil;->ONE_DAY:J

    mul-long/2addr v3, v5

    add-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method private getPhenomenonStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "codeString"    # Ljava/lang/String;

    .prologue
    .line 166
    :try_start_0
    const-string/jumbo v1, "99"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 167
    :cond_0
    const-string/jumbo v1, ""

    .line 180
    :goto_0
    return-object v1

    .line 168
    :cond_1
    const-string/jumbo v1, "53"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 169
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherPhenomenonStrings:[Ljava/lang/String;

    const/16 v2, 0x20

    aget-object v1, v1, v2

    goto :goto_0

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherPhenomenonStrings:[Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v1, ""

    goto :goto_0

    .line 177
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v1, ""

    goto :goto_0
.end method

.method private getSunrise(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "sunRise"    # Ljava/lang/String;

    .prologue
    .line 229
    const-string/jumbo v0, "\\|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method private getSunset(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "sunSet"    # Ljava/lang/String;

    .prologue
    .line 233
    const-string/jumbo v0, "\\|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private getWindDirectionStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "codeString"    # Ljava/lang/String;

    .prologue
    .line 207
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    const-string/jumbo v1, ""

    .line 223
    :goto_0
    return-object v1

    .line 215
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->windDirectionStrings:[Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v1, ""

    goto :goto_0

    .line 220
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 223
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v1, ""

    goto :goto_0
.end method

.method private getWindForceStringFromArrayRes(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "codeString"    # Ljava/lang/String;

    .prologue
    .line 185
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    const-string/jumbo v1, ""

    .line 201
    :goto_0
    return-object v1

    .line 193
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->windForceStrings:[Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 194
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v1, ""

    goto :goto_0

    .line 198
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 201
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v1, ""

    goto :goto_0
.end method

.method private toFirstUpperCase(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 152
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 154
    .local v0, "result":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 158
    .end local v0    # "result":Ljava/lang/StringBuilder;
    :goto_0
    return-object v1

    :cond_0
    const-string/jumbo v1, ""

    goto :goto_0
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    .locals 6
    .param p1, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    .line 42
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 43
    .local v0, "jSonArray":Lorg/json/JSONArray;
    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string/jumbo v2, "c"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string/jumbo v3, "l"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string/jumbo v4, "f"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string/jumbo v4, "f1"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-direct {p0, v2, v1, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->fillNode(Lorg/json/JSONObject;Lorg/json/JSONObject;Lorg/json/JSONArray;)V

    .line 46
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    return-object v1

    .line 43
    :cond_0
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string/jumbo v3, "l"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    goto :goto_0
.end method

.method public parseCurrentLocation(Ljava/lang/String;)V
    .locals 4
    .param p1, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 51
    .local v1, "jSonObject":Lorg/json/JSONObject;
    const-string/jumbo v3, "geo"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 52
    .local v0, "geoObject":Lorg/json/JSONObject;
    new-instance v2, Lcom/vlingo/core/internal/weather/china/CMALocationElement;

    invoke-direct {v2}, Lcom/vlingo/core/internal/weather/china/CMALocationElement;-><init>()V

    .line 53
    .local v2, "locationElement":Lcom/vlingo/core/internal/weather/china/CMALocationElement;
    const-string/jumbo v3, "id"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/vlingo/core/internal/weather/china/CMALocationElement;->areaID:Ljava/lang/String;

    .line 54
    iget-object v3, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iput-object v2, v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->location:Lcom/vlingo/core/internal/weather/china/CMALocationElement;

    .line 55
    return-void
.end method

.method public parseForecasts(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    .locals 4
    .param p1, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 64
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 65
    .local v0, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string/jumbo v2, "c"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    const-string/jumbo v3, "f"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string/jumbo v3, "f1"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->fillNode(Lorg/json/JSONObject;Lorg/json/JSONArray;)V

    .line 66
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    return-object v1
.end method

.method public parseLocationAndCurrentWeather(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    .locals 5
    .param p1, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 58
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "jsonArray":Lorg/json/JSONArray;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string/jumbo v2, "cityinfo"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string/jumbo v3, "l"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-direct {p0, v2, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->fillNode(Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    .line 60
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    return-object v1

    .line 59
    :cond_0
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string/jumbo v3, "l"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    goto :goto_0
.end method
