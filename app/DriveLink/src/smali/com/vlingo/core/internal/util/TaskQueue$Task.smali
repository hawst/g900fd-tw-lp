.class public abstract Lcom/vlingo/core/internal/util/TaskQueue$Task;
.super Ljava/lang/Object;
.source "TaskQueue.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/TaskQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Task"
.end annotation


# instance fields
.field private finished:Z

.field private isCancelled:Z

.field private mParentQueue:Lcom/vlingo/core/internal/util/TaskQueue;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 252
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->mParentQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    .line 253
    iput-boolean v1, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->finished:Z

    .line 254
    iput-boolean v1, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->isCancelled:Z

    return-void
.end method

.method static synthetic access$002(Lcom/vlingo/core/internal/util/TaskQueue$Task;Lcom/vlingo/core/internal/util/TaskQueue;)Lcom/vlingo/core/internal/util/TaskQueue;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/TaskQueue$Task;
    .param p1, "x1"    # Lcom/vlingo/core/internal/util/TaskQueue;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->mParentQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue$Task;->cancel()V

    return-void
.end method

.method private cancel()V
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->isCancelled:Z

    .line 275
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/TaskQueue$Task;->onCancelled()V

    .line 276
    return-void
.end method


# virtual methods
.method protected getParentQueue()Lcom/vlingo/core/internal/util/TaskQueue;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->mParentQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    return-object v0
.end method

.method protected isCancelled()Z
    .locals 1

    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->isCancelled:Z

    return v0
.end method

.method protected notifyFinished()V
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->finished:Z

    if-nez v0, :cond_0

    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->finished:Z

    .line 267
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->mParentQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue$Task;->mParentQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    # invokes: Lcom/vlingo/core/internal/util/TaskQueue;->onCurrentTaskDone()V
    invoke-static {v0}, Lcom/vlingo/core/internal/util/TaskQueue;->access$300(Lcom/vlingo/core/internal/util/TaskQueue;)V

    .line 271
    :cond_0
    return-void
.end method

.method protected onAborted()V
    .locals 0

    .prologue
    .line 287
    return-void
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 293
    return-void
.end method

.method public abstract run()V
.end method
