.class public abstract Lcom/vlingo/core/internal/memo/MemoUtil;
.super Ljava/lang/Object;
.source "MemoUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/memo/IMemoUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/memo/MemoUtil$1;,
        Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;,
        Lcom/vlingo/core/internal/memo/MemoUtil$Provider;
    }
.end annotation


# static fields
.field private static final CANVAS_HEIGHT:I = 0x4ba

.field private static final CANVAS_WIDTH:I = 0x310

.field private static final DEFAULT_PORT_LINE_HEIGHT:F = 47.0f

.field private static final DEFAULT_PORT_TEXT_SIZE:F = 38.0f

.field private static final DEFAULT_TEXT_PADDING:I = 0x1a

.field private static final DEFAULT_TEXT_PADDING_LEFT:I = 0x1a

.field private static final DEFAULT_TEXT_PADDING_TOP:I = 0x64

.field private static final DEFAULT_THUMB_HEIGHT:I = 0x12a

.field private static final DEFAULT_THUMB_WIDTH:I = 0xe2

.field protected static final KEY_THUMB:Ljava/lang/String; = "Thumb"

.field private static final STR_EXTRA_NAME:Ljava/lang/String; = "Memo_Text"

.field private static final STR_FILE_NAME:Ljava/lang/String; = "FileName"


# instance fields
.field private memoXML:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static buildThumbnail(Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 19
    .param p0, "values"    # Landroid/content/ContentValues;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 206
    const/16 v3, 0x310

    const/16 v6, 0x4ba

    :try_start_0
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 207
    .local v14, "offscreenBitmap":Landroid/graphics/Bitmap;
    new-instance v15, Landroid/graphics/Canvas;

    invoke-direct {v15, v14}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 209
    .local v15, "offscreenCanvas":Landroid/graphics/Canvas;
    new-instance v4, Landroid/text/TextPaint;

    const/4 v3, 0x1

    invoke-direct {v4, v3}, Landroid/text/TextPaint;-><init>(I)V

    .line 210
    .local v4, "textPaint":Landroid/text/TextPaint;
    const/high16 v3, 0x42180000    # 38.0f

    invoke-virtual {v4, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 211
    const/high16 v3, -0x1000000

    invoke-virtual {v4, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 213
    new-instance v2, Landroid/text/DynamicLayout;

    const-string/jumbo v3, "foo"

    const/16 v5, 0x64

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 214
    .local v2, "layout":Landroid/text/DynamicLayout;
    const/high16 v3, 0x423c0000    # 47.0f

    invoke-virtual {v2}, Landroid/text/DynamicLayout;->getHeight()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v3, v6

    float-to-int v0, v3

    move/from16 v16, v0

    .line 216
    .local v16, "spacing":I
    new-instance v5, Landroid/text/DynamicLayout;

    const/16 v8, 0x310

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v10, 0x3f800000    # 1.0f

    move/from16 v0, v16

    int-to-float v11, v0

    const/4 v12, 0x1

    move-object/from16 v6, p1

    move-object v7, v4

    invoke-direct/range {v5 .. v12}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 219
    .local v5, "textLayout":Landroid/text/DynamicLayout;
    invoke-virtual {v5}, Landroid/text/DynamicLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    const/16 v6, 0x1a

    iput v6, v3, Landroid/text/TextPaint;->baselineShift:I

    .line 221
    invoke-virtual {v15}, Landroid/graphics/Canvas;->save()I

    .line 222
    const/high16 v3, 0x41d00000    # 26.0f

    const/high16 v6, 0x42c80000    # 100.0f

    invoke-virtual {v15, v3, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 223
    invoke-virtual {v5, v15}, Landroid/text/DynamicLayout;->draw(Landroid/graphics/Canvas;)V

    .line 224
    invoke-virtual {v15}, Landroid/graphics/Canvas;->restore()V

    .line 226
    const/16 v3, 0xe2

    const/16 v6, 0x12a

    const/4 v7, 0x1

    invoke-static {v14, v3, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 229
    .local v17, "thumbnailImage":Landroid/graphics/Bitmap;
    new-instance v18, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 230
    .local v18, "thumbnailStream":Ljava/io/ByteArrayOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x64

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v3, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 231
    const-string/jumbo v3, "Thumb"

    invoke-virtual/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 232
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->recycle()V

    .line 233
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->recycle()V

    .line 234
    invoke-virtual/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    .end local v2    # "layout":Landroid/text/DynamicLayout;
    .end local v4    # "textPaint":Landroid/text/TextPaint;
    .end local v5    # "textLayout":Landroid/text/DynamicLayout;
    .end local v14    # "offscreenBitmap":Landroid/graphics/Bitmap;
    .end local v15    # "offscreenCanvas":Landroid/graphics/Canvas;
    .end local v16    # "spacing":I
    .end local v17    # "thumbnailImage":Landroid/graphics/Bitmap;
    .end local v18    # "thumbnailStream":Ljava/io/ByteArrayOutputStream;
    :goto_0
    return-void

    .line 235
    :catch_0
    move-exception v13

    .line 237
    .local v13, "e1":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private getWhere(Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 16
    .param p1, "whereType"    # Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;
    .param p2, "searchString"    # Ljava/lang/String;
    .param p3, "fields"    # [Ljava/lang/String;

    .prologue
    .line 336
    const/4 v10, 0x0

    .line 338
    .local v10, "where":Ljava/lang/String;
    if-eqz p2, :cond_a

    .line 339
    sget-object v13, Lcom/vlingo/core/internal/memo/MemoUtil$1;->$SwitchMap$com$vlingo$core$internal$memo$MemoUtil$WhereType:[I

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    .line 384
    const-string/jumbo v13, "\\s+"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 385
    .local v12, "words":[Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 386
    .local v8, "sb":Ljava/lang/StringBuilder;
    move-object/from16 v1, p3

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v5, v4

    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_9

    aget-object v3, v1, v5

    .line 387
    .local v3, "field":Ljava/lang/String;
    move-object v2, v12

    .local v2, "arr$":[Ljava/lang/String;
    array-length v7, v2

    .local v7, "len$":I
    const/4 v4, 0x0

    .end local v5    # "i$":I
    .restart local v4    # "i$":I
    :goto_1
    if-ge v4, v7, :cond_8

    aget-object v11, v2, v4

    .line 388
    .local v11, "word":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    if-eqz v13, :cond_0

    .line 389
    const-string/jumbo v13, " or "

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    :cond_0
    const-string/jumbo v13, "\'"

    const-string/jumbo v14, "\'\'"

    invoke-virtual {v11, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    .line 392
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " LIKE \'%"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "%\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 387
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 341
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v3    # "field":Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v7    # "len$":I
    .end local v8    # "sb":Ljava/lang/StringBuilder;
    .end local v11    # "word":Ljava/lang/String;
    .end local v12    # "words":[Ljava/lang/String;
    :pswitch_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 342
    .restart local v8    # "sb":Ljava/lang/StringBuilder;
    move-object/from16 v1, p3

    .restart local v1    # "arr$":[Ljava/lang/String;
    array-length v6, v1

    .restart local v6    # "len$":I
    const/4 v4, 0x0

    .restart local v4    # "i$":I
    :goto_2
    if-ge v4, v6, :cond_2

    aget-object v3, v1, v4

    .line 343
    .restart local v3    # "field":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    if-eqz v13, :cond_1

    .line 344
    const-string/jumbo v13, " or "

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    :cond_1
    const-string/jumbo v13, "\'"

    const-string/jumbo v14, "\'\'"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 347
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " LIKE \'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 349
    .end local v3    # "field":Ljava/lang/String;
    :cond_2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 419
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    .end local v8    # "sb":Ljava/lang/StringBuilder;
    :goto_3
    return-object v10

    .line 354
    :pswitch_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 355
    .restart local v8    # "sb":Ljava/lang/StringBuilder;
    move-object/from16 v1, p3

    .restart local v1    # "arr$":[Ljava/lang/String;
    array-length v6, v1

    .restart local v6    # "len$":I
    const/4 v4, 0x0

    .restart local v4    # "i$":I
    :goto_4
    if-ge v4, v6, :cond_5

    aget-object v3, v1, v4

    .line 356
    .restart local v3    # "field":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    if-eqz v13, :cond_3

    .line 357
    const-string/jumbo v13, " or "

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    :cond_3
    const-string/jumbo v13, "\'"

    const-string/jumbo v14, "\'\'"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 360
    const-string/jumbo v13, "\\s+"

    const-string/jumbo v14, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 361
    .local v9, "searchStringNoSpace":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x1

    if-le v13, v14, :cond_4

    .line 362
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-virtual {v9, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v9, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 364
    :cond_4
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " LIKE \'%"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "%\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 366
    .end local v3    # "field":Ljava/lang/String;
    .end local v9    # "searchStringNoSpace":Ljava/lang/String;
    :cond_5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 369
    goto/16 :goto_3

    .line 371
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    .end local v8    # "sb":Ljava/lang/StringBuilder;
    :pswitch_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 372
    .restart local v8    # "sb":Ljava/lang/StringBuilder;
    move-object/from16 v1, p3

    .restart local v1    # "arr$":[Ljava/lang/String;
    array-length v6, v1

    .restart local v6    # "len$":I
    const/4 v4, 0x0

    .restart local v4    # "i$":I
    :goto_5
    if-ge v4, v6, :cond_7

    aget-object v3, v1, v4

    .line 373
    .restart local v3    # "field":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    if-eqz v13, :cond_6

    .line 374
    const-string/jumbo v13, " or "

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    :cond_6
    const-string/jumbo v13, "\'"

    const-string/jumbo v14, "\'\'"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 377
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " LIKE \'%"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "%\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 379
    .end local v3    # "field":Ljava/lang/String;
    :cond_7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 382
    goto/16 :goto_3

    .line 386
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v6    # "len$":I
    .restart local v2    # "arr$":[Ljava/lang/String;
    .restart local v3    # "field":Ljava/lang/String;
    .restart local v7    # "len$":I
    .restart local v12    # "words":[Ljava/lang/String;
    :cond_8
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    .end local v4    # "i$":I
    .restart local v5    # "i$":I
    goto/16 :goto_0

    .line 395
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v3    # "field":Ljava/lang/String;
    .end local v7    # "len$":I
    :cond_9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_3

    .line 401
    .end local v5    # "i$":I
    .end local v8    # "sb":Ljava/lang/StringBuilder;
    .end local v12    # "words":[Ljava/lang/String;
    :cond_a
    sget-object v13, Lcom/vlingo/core/internal/memo/MemoUtil$1;->$SwitchMap$com$vlingo$core$internal$memo$MemoUtil$WhereType:[I

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_1

    .line 415
    const/4 v10, 0x0

    goto/16 :goto_3

    .line 403
    :pswitch_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 404
    .restart local v8    # "sb":Ljava/lang/StringBuilder;
    move-object/from16 v1, p3

    .restart local v1    # "arr$":[Ljava/lang/String;
    array-length v6, v1

    .restart local v6    # "len$":I
    const/4 v4, 0x0

    .restart local v4    # "i$":I
    :goto_6
    if-ge v4, v6, :cond_c

    aget-object v3, v1, v4

    .line 405
    .restart local v3    # "field":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    if-eqz v13, :cond_b

    .line 406
    const-string/jumbo v13, " or "

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    :cond_b
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " not null"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 410
    .end local v3    # "field":Ljava/lang/String;
    :cond_c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 413
    goto/16 :goto_3

    .line 339
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 401
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected addAdditionalExtra(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 202
    return-void
.end method

.method public abstract getCreateMemoAction()Ljava/lang/String;
.end method

.method public getMemo(Landroid/content/Context;J)Lcom/vlingo/core/internal/memo/Memo;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 243
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 244
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getContentProviderUri()Landroid/net/Uri;

    move-result-object v6

    .line 247
    .local v6, "contentUri":Landroid/net/Uri;
    invoke-static {v6, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 249
    .local v1, "myMemo":Landroid/net/Uri;
    const/4 v8, 0x0

    .line 250
    .local v8, "cur":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 253
    .local v9, "memo":Lcom/vlingo/core/internal/memo/Memo;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/memo/MemoUtil;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 255
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 257
    .local v7, "count":I
    if-eqz v8, :cond_0

    const/4 v2, 0x1

    if-ge v7, v2, :cond_2

    .line 258
    :cond_0
    new-instance v2, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v3, "Error in getting memo."

    invoke-direct {v2, v3}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    .end local v7    # "count":I
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_1

    .line 266
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2

    .line 261
    .restart local v7    # "count":I
    :cond_2
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 262
    invoke-virtual {p0, v8}, Lcom/vlingo/core/internal/memo/MemoUtil;->getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    .line 265
    :cond_3
    if-eqz v8, :cond_4

    .line 266
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 270
    :cond_4
    return-object v9
.end method

.method protected abstract getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;
.end method

.method public getMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "where"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/core/internal/memo/MemoUtil;->getMemos(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMemos(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    const/4 v6, 0x0

    .line 296
    .local v6, "cur":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 299
    .local v7, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getContentProviderUri()Landroid/net/Uri;

    move-result-object v1

    .line 300
    .local v1, "contentUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/memo/MemoUtil;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object v3, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 301
    if-nez v6, :cond_2

    .line 314
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 315
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 318
    :cond_1
    return-object v7

    .line 304
    :cond_2
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310
    .end local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .local v8, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :cond_3
    :try_start_2
    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/memo/MemoUtil;->getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-nez v0, :cond_3

    move-object v7, v8

    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_0

    .line 314
    .end local v1    # "contentUri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_4

    .line 315
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 314
    .end local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v1    # "contentUri":Landroid/net/Uri;
    .restart local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :catchall_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_1
.end method

.method public abstract getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation
.end method

.method protected getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "fields"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 323
    const/4 v1, 0x0

    .line 324
    .local v1, "toReturn":Lcom/vlingo/core/internal/memo/Memo;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/memo/MemoUtil;->searchMemos(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 325
    .local v0, "foundMemos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    if-eqz v0, :cond_0

    .line 326
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-lt v2, v3, :cond_0

    .line 327
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "toReturn":Lcom/vlingo/core/internal/memo/Memo;
    check-cast v1, Lcom/vlingo/core/internal/memo/Memo;

    .line 330
    .restart local v1    # "toReturn":Lcom/vlingo/core/internal/memo/Memo;
    :cond_0
    return-object v1
.end method

.method protected abstract getProjection()[Ljava/lang/String;
.end method

.method public abstract getViewMemoAction()Ljava/lang/String;
.end method

.method protected prepareMemoData()V
    .locals 6

    .prologue
    .line 61
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 64
    .local v3, "output":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    const-string/jumbo v5, "memo.xml"

    invoke-virtual {v4, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 65
    .local v2, "input":Ljava/io/InputStream;
    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 66
    .local v0, "ch":I
    :goto_0
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    .line 67
    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 68
    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_0

    .line 70
    :cond_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    .end local v0    # "ch":I
    .end local v2    # "input":Ljava/io/InputStream;
    :goto_1
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/memo/MemoUtil;->memoXML:Ljava/lang/String;

    .line 77
    return-void

    .line 71
    :catch_0
    move-exception v1

    .line 73
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public saveMemoData(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v1

    .line 182
    .local v1, "appInfo":Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getIntentNameCreate()Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "action":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 186
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "Memo_Text"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->isSNote()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 190
    const-string/jumbo v3, "FileName"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    :cond_0
    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/memo/MemoUtil;->addAdditionalExtra(Landroid/content/Intent;)V

    .line 195
    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->isBroadcast()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 196
    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 200
    :goto_0
    return-void

    .line 198
    :cond_1
    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public abstract searchMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation
.end method

.method protected searchMemos(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchString"    # Ljava/lang/String;
    .param p3, "fields"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 110
    sget-object v9, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->EXACT:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    invoke-direct {p0, v9, p2, p3}, Lcom/vlingo/core/internal/memo/MemoUtil;->getWhere(Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "bullsEyeWhere":Ljava/lang/String;
    sget-object v9, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->EXACT_WITHOUT_SPACES:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    invoke-direct {p0, v9, p2, p3}, Lcom/vlingo/core/internal/memo/MemoUtil;->getWhere(Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "bullsEyeWithoutSpaceWhere":Ljava/lang/String;
    sget-object v9, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->CONTAINS:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    invoke-direct {p0, v9, p2, p3}, Lcom/vlingo/core/internal/memo/MemoUtil;->getWhere(Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 113
    .local v2, "containsWhere":Ljava/lang/String;
    sget-object v9, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->FALLBACK:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    invoke-direct {p0, v9, p2, p3}, Lcom/vlingo/core/internal/memo/MemoUtil;->getWhere(Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 114
    .local v3, "fallbackWhere":Ljava/lang/String;
    invoke-virtual {p0, p1, v0, p4}, Lcom/vlingo/core/internal/memo/MemoUtil;->getMemos(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 115
    .local v7, "unfilteredMemos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    if-nez v7, :cond_1

    .line 118
    invoke-virtual {p0, p1, v2, p4}, Lcom/vlingo/core/internal/memo/MemoUtil;->getMemos(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 119
    if-nez v7, :cond_1

    .line 122
    invoke-virtual {p0, p1, v1, p4}, Lcom/vlingo/core/internal/memo/MemoUtil;->getMemos(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 123
    if-nez v7, :cond_1

    .line 126
    invoke-virtual {p0, p1, v3, p4}, Lcom/vlingo/core/internal/memo/MemoUtil;->getMemos(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 127
    if-nez v7, :cond_1

    move-object v6, v8

    .line 149
    :cond_0
    :goto_0
    return-object v6

    .line 138
    :cond_1
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 139
    .local v6, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/memo/Memo;

    .line 140
    .local v5, "m":Lcom/vlingo/core/internal/memo/Memo;
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Lcom/vlingo/core/internal/memo/Memo;->getMemoName(Z)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 141
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 144
    .end local v5    # "m":Lcom/vlingo/core/internal/memo/Memo;
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_0

    move-object v6, v8

    .line 147
    goto :goto_0
.end method

.method public abstract updateMemo(Landroid/content/Context;Lcom/vlingo/core/internal/memo/Memo;Lcom/vlingo/core/internal/memo/Memo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation
.end method
