.class public Lcom/vlingo/core/internal/util/CallLogUtil;
.super Ljava/lang/Object;
.source "CallLogUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static findSimilarNameFromCallLog(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    const/4 v9, 0x0

    .line 112
    .local v9, "c":Landroid/database/Cursor;
    new-instance v19, Ljava/util/Vector;

    invoke-direct/range {v19 .. v19}, Ljava/util/Vector;-><init>()V

    .line 114
    .local v19, "nameVector":Ljava/util/Vector;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->getCommonTitles()Ljava/util/HashSet;

    move-result-object v10

    .line 115
    .local v10, "commonTitles":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v10}, Ljava/util/HashSet;->size()I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v22, v0

    .line 118
    .local v22, "titlesArray":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isKorean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 119
    const/4 v3, 0x0

    .line 172
    :cond_0
    :goto_0
    return-object v3

    .line 122
    :cond_1
    const/4 v3, 0x0

    aget-object v3, v22, v3

    if-nez v3, :cond_2

    .line 123
    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 126
    :cond_2
    const-string/jumbo v3, "korean_name_similarity_value_min"

    const v4, 0x3f4ccccd    # 0.8f

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v16

    .line 127
    .local v16, "minSimilarity":F
    const-wide/16 v14, 0x0

    .line 128
    .local v14, "maxScore":D
    const-string/jumbo v13, ""

    .line 132
    .local v13, "lastName":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_STREQUENT_URI:Landroid/net/Uri;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "display_name"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "starred"

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 137
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 138
    const/4 v12, 0x0

    .line 139
    .local v12, "index":I
    const-string/jumbo v3, "display_name"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 142
    .local v18, "nameIdx":I
    :cond_3
    move/from16 v0, v18

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 144
    .local v17, "name":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/util/StringUtils;->isKorean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 145
    const-wide/16 v20, 0x0

    .line 147
    .local v20, "tempScore":D
    move-object/from16 v13, v17

    .line 148
    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/CallLogUtil;->preprocessName(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 150
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->getDistanceBetweenNames(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v20

    .line 151
    move/from16 v0, v16

    float-to-double v3, v0

    cmpl-double v3, v20, v3

    if-ltz v3, :cond_4

    .line 152
    new-instance v3, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-direct {v3, v0, v1, v2}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;-><init>(Ljava/lang/String;D)V

    move-object/from16 v0, v19

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/CallLogUtil;->insertNameByScore(Ljava/util/Vector;Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;)I

    .line 155
    .end local v20    # "tempScore":D
    :cond_4
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_3

    .line 167
    .end local v12    # "index":I
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "nameIdx":I
    :cond_5
    if-eqz v9, :cond_6

    .line 168
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 172
    :cond_6
    invoke-static/range {v19 .. v19}, Lcom/vlingo/core/internal/util/CallLogUtil;->getSortedNameList(Ljava/util/Vector;)Ljava/util/List;

    move-result-object v3

    goto/16 :goto_0

    .line 157
    :catch_0
    move-exception v11

    .line 162
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    .line 163
    invoke-static/range {v19 .. v19}, Lcom/vlingo/core/internal/util/CallLogUtil;->getSortedNameList(Ljava/util/Vector;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 167
    if-eqz v9, :cond_0

    .line 168
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 167
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_7

    .line 168
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v3
.end method

.method public static getLastNMissedCalls(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/LoggedCall;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    const/4 v7, 0x0

    .line 54
    .local v7, "c":Landroid/database/Cursor;
    new-instance v14, Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 55
    .local v14, "missedCalls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/LoggedCall;>;"
    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "name"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "number"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "date"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "new"

    aput-object v2, v3, v1

    .line 61
    .local v3, "projection":[Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v4, "type = ? AND new = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v18, 0x3

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v18, "1"

    aput-object v18, v5, v6

    const-string/jumbo v6, "date DESC "

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 68
    invoke-static {v7}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 69
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 70
    const/4 v13, 0x0

    .line 71
    .local v13, "index":I
    const-string/jumbo v1, "name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 72
    .local v16, "nameIdx":I
    const-string/jumbo v1, "date"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 73
    .local v9, "dateIdx":I
    const-string/jumbo v1, "number"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 76
    .local v17, "numberIdx":I
    :cond_0
    move/from16 v0, v16

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 77
    .local v15, "name":Ljava/lang/String;
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 79
    .local v10, "dateMS":J
    invoke-static {v15}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    move/from16 v0, v17

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 84
    :cond_1
    invoke-static {v15, v10, v11}, Lcom/vlingo/core/internal/util/LoggedCall;->newInstance(Ljava/lang/String;J)Lcom/vlingo/core/internal/util/LoggedCall;

    move-result-object v8

    .line 85
    .local v8, "call":Lcom/vlingo/core/internal/util/LoggedCall;
    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    move/from16 v0, p1

    if-lt v13, v0, :cond_0

    .line 95
    .end local v8    # "call":Lcom/vlingo/core/internal/util/LoggedCall;
    .end local v9    # "dateIdx":I
    .end local v10    # "dateMS":J
    .end local v13    # "index":I
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "nameIdx":I
    .end local v17    # "numberIdx":I
    :cond_2
    if-eqz v7, :cond_3

    .line 96
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 99
    :cond_3
    :goto_0
    return-object v14

    .line 89
    :catch_0
    move-exception v12

    .line 92
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    if-eqz v7, :cond_3

    .line 96
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 95
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_4

    .line 96
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1
.end method

.method private static getSortedNameList(Ljava/util/Vector;)Ljava/util/List;
    .locals 5
    .param p0, "nameVector"    # Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    .line 226
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 227
    invoke-virtual {p0, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    .line 228
    .local v3, "tempData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 231
    .end local v3    # "tempData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    :cond_0
    return-object v1
.end method

.method private static insertNameByScore(Ljava/util/Vector;Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;)I
    .locals 8
    .param p0, "nameVector"    # Ljava/util/Vector;
    .param p1, "data"    # Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    .prologue
    .line 202
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    .line 203
    .local v2, "size":I
    if-lez v2, :cond_2

    .line 204
    invoke-virtual {p0}, Ljava/util/Vector;->lastElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    .line 205
    .local v1, "lastData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getScore()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getScore()D

    move-result-wide v6

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_0

    .line 206
    invoke-virtual {p0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 207
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 219
    .end local v1    # "lastData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    :goto_0
    return v0

    .line 209
    .restart local v1    # "lastData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_3

    .line 210
    invoke-virtual {p0, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    .line 211
    .local v3, "tempData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getScore()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getScore()D

    move-result-wide v6

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    .line 212
    invoke-virtual {p0, p1, v0}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0

    .line 209
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 217
    .end local v0    # "i":I
    .end local v1    # "lastData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    .end local v3    # "tempData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    :cond_2
    invoke-virtual {p0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static preprocessName(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "titlesArray"    # [Ljava/lang/String;

    .prologue
    .line 180
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p1

    if-ge v1, v4, :cond_0

    .line 181
    aget-object v4, p1, v1

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 182
    .local v0, "getIdx":I
    if-lez v0, :cond_2

    .line 183
    const/4 v4, 0x0

    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 189
    .end local v0    # "getIdx":I
    :cond_0
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 190
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->isHangul(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 191
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    .line 192
    .local v3, "specialChars":Ljava/lang/String;
    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 189
    .end local v3    # "specialChars":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 180
    .end local v2    # "j":I
    .restart local v0    # "getIdx":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 196
    .end local v0    # "getIdx":I
    .restart local v2    # "j":I
    :cond_3
    const-string/jumbo v4, "\\s+"

    const-string/jumbo v5, ""

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 198
    return-object p0
.end method
