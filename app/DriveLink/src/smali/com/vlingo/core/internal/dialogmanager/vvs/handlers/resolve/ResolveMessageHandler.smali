.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;
.source "ResolveMessageHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private which:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;-><init>()V

    return-void
.end method


# virtual methods
.method protected executeQuery(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 10
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v9, 0x0

    .line 30
    const-string/jumbo v5, "which"

    invoke-static {p1, v5, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->which:Ljava/lang/String;

    .line 31
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 33
    .local v4, "messages":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/util/LinkedList;

    move-object v4, v0

    .line 34
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 35
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAllNewAlerts()Ljava/util/LinkedList;

    move-result-object v4

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v5, v6, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :cond_1
    const/4 v3, 0x0

    .line 44
    .local v3, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 45
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->which:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 46
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->which:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    check-cast v3, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 51
    .restart local v3    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_2
    :goto_0
    if-nez v3, :cond_4

    .line 52
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->sendEmptyEvent()V

    .line 60
    .end local v3    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :goto_1
    return v9

    .line 38
    :catch_0
    move-exception v1

    .line 39
    .local v1, "e":Ljava/lang/ClassCastException;
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Invalid value stored in messages got object of type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->sendEmptyEvent()V

    goto :goto_1

    .line 48
    .end local v1    # "e":Ljava/lang/ClassCastException;
    .restart local v3    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_3
    invoke-virtual {v4, v9}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    check-cast v3, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .restart local v3    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    goto :goto_0

    .line 54
    :cond_4
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;

    invoke-direct {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;-><init>(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V

    .line 55
    .local v2, "event":Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v5, v2, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_1
.end method

.method protected sendEmptyEvent()V
    .locals 3

    .prologue
    .line 64
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;-><init>()V

    .line 65
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 66
    return-void
.end method
