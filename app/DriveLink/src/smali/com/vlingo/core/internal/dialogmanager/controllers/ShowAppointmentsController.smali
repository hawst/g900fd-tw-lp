.class public Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;
.super Lcom/vlingo/core/internal/dialogmanager/Controller;
.source "ShowAppointmentsController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;-><init>()V

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;->listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 35
    const-string/jumbo v0, "ListPosition"

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    const-string/jumbo v0, "choices"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    .line 39
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "appointment"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;->listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CALENDAR_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEventChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v7, 0x0

    move-object v1, p2

    move-object v2, p1

    move-object v6, p0

    invoke-virtual/range {v0 .. v7}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->showChoiceListWidget(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;Ljava/util/Map;)Z

    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "used"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;->listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    invoke-virtual {v2, p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->getIdOfSelection(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "id":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 80
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;
    .end local v1    # "id":Ljava/lang/String;
    .end local p2    # "used":Ljava/lang/Object;
    :goto_0
    return-void

    .line 62
    .restart local p2    # "used":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.View"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 65
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 66
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_VIEW:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;

    check-cast p2, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .end local p2    # "used":Ljava/lang/Object;
    invoke-virtual {v2, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->scheduleEvent(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->queue()V

    goto :goto_0

    .line 69
    .restart local p2    # "used":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Edit"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 72
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 73
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_EDIT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;

    check-cast p2, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .end local p2    # "used":Ljava/lang/Object;
    invoke-virtual {v2, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->scheduleEvent(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->queue()V

    goto :goto_0

    .line 78
    .restart local p2    # "used":Ljava/lang/Object;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentsController;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method
