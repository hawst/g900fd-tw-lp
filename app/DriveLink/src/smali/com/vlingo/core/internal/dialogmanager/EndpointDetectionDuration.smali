.class public final enum Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;
.super Ljava/lang/Enum;
.source "EndpointDetectionDuration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

.field public static final enum DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

.field public static final enum LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

.field public static final enum LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

.field public static final enum MEDIUM_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

.field public static final enum SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;


# instance fields
.field private duration:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    const-string/jumbo v1, "SHORT"

    invoke-direct {v0, v1, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 11
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    const-string/jumbo v1, "DEFAULT"

    invoke-direct {v0, v1, v3, v3}, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 12
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    const-string/jumbo v1, "MEDIUM_LONG"

    invoke-direct {v0, v1, v4, v4}, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->MEDIUM_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 13
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    const-string/jumbo v1, "LONG"

    invoke-direct {v0, v1, v5, v5}, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    const-string/jumbo v1, "LONG_LONG"

    invoke-direct {v0, v1, v6, v6}, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 9
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->MEDIUM_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "duration"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput p3, p0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->duration:I

    .line 20
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    return-object v0
.end method


# virtual methods
.method public getDuration()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->duration:I

    return v0
.end method
