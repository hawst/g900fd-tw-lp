.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
.super Ljava/lang/Object;
.source "PhraseSpotterControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RecordThread"
.end annotation


# static fields
.field private static final CHAN:I = 0x10

.field private static final ENCODING:I = 0x2

.field private static final MAXQUEUE:I = 0x32


# instance fields
.field private bufferSize:I

.field private buffers:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private final mChunkSize:I

.field private mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

.field private final mSampleRate:I

.field private preBuffer:Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

.field private samplerate:I


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;II)V
    .locals 1
    .param p1, "psc"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p2, "chunkSize"    # I
    .param p3, "sampleRate"    # I

    .prologue
    .line 741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 734
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    .line 736
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->buffers:Ljava/util/Vector;

    .line 742
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .line 743
    iput p2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    .line 744
    iput p3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSampleRate:I

    .line 745
    return-void
.end method

.method private declared-synchronized addBuffer(Ljava/nio/ByteBuffer;I)V
    .locals 2
    .param p1, "buf"    # Ljava/nio/ByteBuffer;
    .param p2, "samples"    # I

    .prologue
    .line 825
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->buffers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    const/16 v1, 0x32

    if-ne v0, v1, :cond_0

    .line 827
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "addBuffer: BUFFER OVERFLOW"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 825
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 829
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->buffers:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 831
    monitor-exit p0

    return-void
.end method

.method private static convertToShort([BI)S
    .locals 2
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 1080
    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    shl-int/lit8 v0, v0, 0x8

    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method private declared-synchronized getBuffer()Ljava/nio/ByteBuffer;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 870
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->buffers:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    const/4 v5, 0x4

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 871
    .local v3, "n":I
    const/4 v1, 0x0

    .line 873
    .local v1, "capacity":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 874
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->buffers:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v4

    add-int/2addr v1, v4

    .line 873
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 876
    :cond_0
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 877
    .local v0, "b":Ljava/nio/ByteBuffer;
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 878
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->buffers:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->firstElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 879
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->buffers:Ljava/util/Vector;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/Vector;->removeElementAt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 877
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 881
    :cond_1
    monitor-exit p0

    return-object v0

    .line 870
    .end local v0    # "b":Ljava/nio/ByteBuffer;
    .end local v1    # "capacity":I
    .end local v2    # "i":I
    .end local v3    # "n":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method private getPrebuffer()Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;
    .locals 4

    .prologue
    .line 1195
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->preBuffer:Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    if-nez v2, :cond_0

    .line 1196
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->isStereo()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x2

    .line 1197
    .local v0, "numChannel":I
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getPreBufferLength()I

    move-result v2

    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSampleRate:I

    mul-int/2addr v2, v3

    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    mul-int/lit16 v3, v3, 0x3e8

    div-int/2addr v2, v3

    mul-int v1, v2, v0

    .line 1198
    .local v1, "preBufferChunkCount":I
    new-instance v2, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    invoke-direct {v2, v3, v1}, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;-><init>(II)V

    iput-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->preBuffer:Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    .line 1201
    .end local v0    # "numChannel":I
    .end local v1    # "preBufferChunkCount":I
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->preBuffer:Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    return-object v2

    .line 1196
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private handlePhraseSpotted(Ljava/lang/String;ZII)V
    .locals 11
    .param p1, "spottedPhrase"    # Ljava/lang/String;
    .param p2, "isSeamlessEnabled"    # Z
    .param p3, "deltaD"    # I
    .param p4, "deltaS"    # I

    .prologue
    .line 1085
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v9

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "spotting: seamless spotting "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-eqz p2, :cond_0

    const-string/jumbo v8, "ENABLED"

    :goto_0
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    if-nez p2, :cond_1

    .line 1090
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getListener()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v8

    invoke-interface {v8, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotted(Ljava/lang/String;)V

    .line 1153
    :goto_1
    return-void

    .line 1085
    :cond_0
    const-string/jumbo v8, "DISABLED"

    goto :goto_0

    .line 1093
    :cond_1
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->isStereo()Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v3, 0x2

    .line 1095
    .local v3, "numChannel":I
    :goto_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->getPrebuffer()Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->getArray()[S

    move-result-object v5

    .line 1098
    .local v5, "preBufferSamples":[S
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getAudioLogger()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->markP()V

    .line 1100
    const/4 v4, 0x0

    .line 1101
    .local v4, "preBufferCarryOverSamples":I
    if-gez p3, :cond_7

    .line 1102
    iget v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSampleRate:I

    mul-int/2addr v8, p3

    div-int/lit16 v8, v8, 0x3e8

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    mul-int v4, v8, v3

    .line 1124
    :cond_2
    new-array v6, v4, [S

    .line 1125
    .local v6, "seamlessBuffer":[S
    const/4 v7, 0x0

    .line 1128
    .local v7, "seamlessBufferOffset":I
    if-lez v4, :cond_4

    .line 1129
    array-length v8, v5

    if-ge v8, v4, :cond_3

    .line 1130
    array-length v4, v5

    .line 1132
    :cond_3
    array-length v8, v5

    sub-int/2addr v8, v4

    const/4 v9, 0x0

    invoke-static {v5, v8, v6, v9, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1133
    add-int/2addr v7, v4

    .line 1136
    :cond_4
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getAudioLogger()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    move-result-object v8

    invoke-virtual {v8, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->markDecisionSample(Z)V

    .line 1138
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->target()Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v2

    .line 1139
    .local v2, "micStream":Lcom/vlingo/core/internal/audio/MicrophoneStream;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->enableAudioFiltering()V

    .line 1140
    invoke-virtual {v2, v6, v7}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->setBufferedData([SI)V

    .line 1141
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getAudioLogger()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->hasTarget()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1142
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getAudioLogger()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->setAudioLogger(Lcom/vlingo/core/internal/audio/AudioLogger;)V

    .line 1146
    :cond_5
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->drop()V

    .line 1148
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v9, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->STOPPING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 1152
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getListener()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v8

    invoke-interface {v8, p1, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    goto/16 :goto_1

    .line 1093
    .end local v2    # "micStream":Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .end local v3    # "numChannel":I
    .end local v4    # "preBufferCarryOverSamples":I
    .end local v5    # "preBufferSamples":[S
    .end local v6    # "seamlessBuffer":[S
    .end local v7    # "seamlessBufferOffset":I
    :cond_6
    const/4 v3, 0x1

    goto :goto_2

    .line 1109
    .restart local v3    # "numChannel":I
    .restart local v4    # "preBufferCarryOverSamples":I
    .restart local v5    # "preBufferSamples":[S
    :cond_7
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->preBuffer:Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    .line 1111
    if-lez p3, :cond_2

    .line 1113
    iget v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSampleRate:I

    mul-int/2addr v8, p3

    iget v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    mul-int/lit16 v9, v9, 0x3e8

    div-int/2addr v8, v9

    mul-int v0, v8, v3

    .line 1117
    .local v0, "chunksToDiscard":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    if-ge v1, v0, :cond_2

    .line 1118
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->readChunk()I

    .line 1117
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private readChunk()I
    .locals 8

    .prologue
    .line 1158
    const/4 v3, 0x0

    .line 1159
    .local v3, "totalNumRead":I
    const/4 v1, 0x0

    .line 1160
    .local v1, "numRead":I
    iget v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    .line 1166
    .local v2, "remainingToRead":I
    :cond_0
    :goto_0
    if-ge v3, v2, :cond_1

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getState()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v5, v5, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->spot:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->isRecording()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1168
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRawAudioBuffer:[S
    invoke-static {v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)[S

    move-result-object v5

    invoke-virtual {v4, v5, v3, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->read([SII)I

    move-result v1

    .line 1170
    const/4 v4, -0x3

    if-eq v1, v4, :cond_0

    .line 1173
    const/4 v4, -0x2

    if-eq v1, v4, :cond_0

    .line 1177
    sub-int/2addr v2, v1

    .line 1178
    add-int/2addr v3, v1

    .line 1179
    if-ge v3, v2, :cond_0

    .line 1181
    :try_start_0
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getRecorderSleep()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1182
    :catch_0
    move-exception v0

    .line 1183
    .local v0, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1189
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getAudioLogger()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRawAudioBuffer:[S
    invoke-static {v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)[S

    move-result-object v5

    const/4 v6, 0x0

    iget v7, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->writeData([SII)V

    .line 1191
    return v1
.end method

.method private readChunkForSensory2()I
    .locals 17

    .prologue
    .line 748
    const/4 v10, 0x0

    .line 749
    .local v10, "numSamples":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    mul-int/lit8 v4, v14, 0x2

    .line 750
    .local v4, "byteBufferSize":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v14}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->isStereo()Z

    move-result v7

    .line 752
    .local v7, "isStereo":Z
    if-eqz v7, :cond_0

    .line 753
    mul-int/lit8 v4, v4, 0x2

    .line 756
    :cond_0
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 757
    .local v2, "buffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v14}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->target()Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->getRecorder()Landroid/media/AudioRecord;

    move-result-object v14

    invoke-virtual {v14, v2, v4}, Landroid/media/AudioRecord;->read(Ljava/nio/ByteBuffer;I)I

    move-result v14

    div-int/lit8 v10, v14, 0x2

    .line 758
    const/4 v14, -0x3

    if-ne v10, v14, :cond_1

    .line 759
    new-instance v14, Ljava/lang/IllegalStateException;

    const-string/jumbo v15, "read() returned AudioRecord.ERROR_INVALID_OPERATION"

    invoke-direct {v14, v15}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 761
    :cond_1
    const/4 v14, -0x2

    if-ne v10, v14, :cond_2

    .line 762
    new-instance v14, Ljava/lang/IllegalStateException;

    const-string/jumbo v15, "read() returned AudioRecord.ERROR_BAD_VALUE"

    invoke-direct {v14, v15}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 764
    :cond_2
    const/4 v14, -0x3

    if-ne v10, v14, :cond_3

    .line 765
    new-instance v14, Ljava/lang/IllegalStateException;

    const-string/jumbo v15, "read() returned AudioRecord.ERROR_INVALID_OPERATION"

    invoke-direct {v14, v15}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 769
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getAudioLogger()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    invoke-static {v14}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    move-result-object v14

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v15

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v14, v15, v0, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->writeData([BII)V

    .line 771
    const/4 v8, 0x0

    .line 772
    .local v8, "j":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    new-array v1, v14, [S

    .line 773
    .local v1, "audioShortArray":[S
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    div-int/lit8 v14, v4, 0x2

    if-ge v5, v14, :cond_4

    .line 774
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v14

    mul-int/lit8 v15, v5, 0x2

    invoke-static {v14, v15}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->convertToShort([BI)S

    move-result v13

    .line 775
    .local v13, "shortAudioElement":S
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "j":I
    .local v9, "j":I
    aput-short v13, v1, v8

    .line 776
    if-eqz v5, :cond_9

    move-object/from16 v0, p0

    iget v14, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    rem-int v14, v9, v14

    if-nez v14, :cond_9

    .line 777
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->getPrebuffer()Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    move-result-object v14

    invoke-virtual {v14, v1}, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->write([S)V

    .line 778
    const/4 v8, 0x0

    .line 773
    .end local v9    # "j":I
    .restart local v8    # "j":I
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 782
    .end local v13    # "shortAudioElement":S
    :cond_4
    if-eqz v7, :cond_7

    .line 783
    invoke-static {v10}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 784
    .local v3, "bufferLeftOnly":Ljava/nio/ByteBuffer;
    const/4 v11, 0x0

    .line 785
    .local v11, "offset":I
    const/4 v5, 0x0

    move v6, v5

    .end local v5    # "i":I
    .local v6, "i":I
    move v12, v11

    .end local v11    # "offset":I
    .local v12, "offset":I
    :goto_2
    mul-int/lit8 v14, v10, 0x2

    if-ge v6, v14, :cond_5

    .line 786
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v14

    add-int/lit8 v11, v12, 0x1

    .end local v12    # "offset":I
    .restart local v11    # "offset":I
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v15

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "i":I
    .restart local v5    # "i":I
    aget-byte v15, v15, v6

    aput-byte v15, v14, v12

    .line 787
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v14

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "offset":I
    .restart local v12    # "offset":I
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v15

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "i":I
    .restart local v6    # "i":I
    aget-byte v15, v15, v5

    aput-byte v15, v14, v11

    .line 785
    add-int/lit8 v5, v6, 0x2

    .end local v6    # "i":I
    .restart local v5    # "i":I
    move v6, v5

    .end local v5    # "i":I
    .restart local v6    # "i":I
    goto :goto_2

    .line 789
    :cond_5
    if-lez v10, :cond_6

    .line 790
    div-int/lit8 v14, v10, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v14}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->addBuffer(Ljava/nio/ByteBuffer;I)V

    .line 799
    .end local v3    # "bufferLeftOnly":Ljava/nio/ByteBuffer;
    .end local v6    # "i":I
    .end local v10    # "numSamples":I
    .end local v12    # "offset":I
    :cond_6
    :goto_3
    return v10

    .line 795
    .restart local v5    # "i":I
    .restart local v10    # "numSamples":I
    :cond_7
    if-lez v10, :cond_8

    .line 796
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v10}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->addBuffer(Ljava/nio/ByteBuffer;I)V

    .line 799
    :cond_8
    mul-int/lit8 v10, v10, 0x2

    goto :goto_3

    .end local v8    # "j":I
    .restart local v9    # "j":I
    .restart local v13    # "shortAudioElement":S
    :cond_9
    move v8, v9

    .end local v9    # "j":I
    .restart local v8    # "j":I
    goto :goto_1
.end method

.method private startSensory2Spotting(ILjava/nio/ByteBuffer;Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;)Ljava/lang/String;
    .locals 8
    .param p1, "sizeOriginal"    # I
    .param p2, "buf"    # Ljava/nio/ByteBuffer;
    .param p3, "spotter"    # Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;

    .prologue
    .line 1058
    const/4 v4, 0x0

    .line 1059
    .local v4, "ps":Ljava/lang/String;
    instance-of v6, p3, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;

    if-eqz v6, :cond_1

    .line 1060
    iget v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->samplerate:I

    int-to-long v6, v6

    invoke-interface {p3, p2, v6, v7}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->phrasespotPipe(Ljava/nio/ByteBuffer;J)Ljava/lang/String;

    move-result-object v4

    .line 1076
    :cond_0
    return-object v4

    .line 1062
    :cond_1
    const/4 v2, 0x0

    .line 1063
    .local v2, "j":I
    iget v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    new-array v0, v6, [S

    .line 1064
    .local v0, "audioShortArray":[S
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    div-int/lit8 v6, p1, 0x2

    if-ge v1, v6, :cond_0

    .line 1065
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    mul-int/lit8 v7, v1, 0x2

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->convertToShort([BI)S

    move-result v5

    .line 1066
    .local v5, "shortAudioElement":S
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .local v3, "j":I
    aput-short v5, v0, v2

    .line 1067
    if-eqz v1, :cond_2

    iget v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    rem-int v6, v3, v6

    if-nez v6, :cond_2

    .line 1068
    const/4 v6, 0x0

    iget v7, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    invoke-interface {p3, v0, v6, v7}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->processShortArray([SII)Ljava/lang/String;

    move-result-object v4

    .line 1069
    const/4 v2, 0x0

    .line 1070
    .end local v3    # "j":I
    .restart local v2    # "j":I
    if-nez v4, :cond_0

    .line 1064
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v2    # "j":I
    .restart local v3    # "j":I
    :cond_2
    move v2, v3

    .end local v3    # "j":I
    .restart local v2    # "j":I
    goto :goto_1
.end method

.method private startSpotting(ILjava/util/ArrayList;)V
    .locals 21
    .param p1, "sizeOriginal"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 975
    .local p2, "spotters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v17

    const-string/jumbo v18, "run(): listening ..."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v17, v0

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v18, v0

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getAudioSourceType()Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->create(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)V

    .line 978
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v17, v0

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v18, v0

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getAudioLogger()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->setLogger(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;)V

    .line 979
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->isSensory2Using()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 980
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->initAudio()Z

    .line 983
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    move/from16 v17, v0

    move/from16 v0, v17

    new-array v4, v0, [S

    .line 985
    .local v4, "audioBufferForSensory1":[S
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getState()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v6

    .line 986
    .local v6, "currState":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    sget-object v17, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->SPOTTING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->is(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Z

    move-result v17

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v17, v0

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStopPhraseSpotterRightNow:Z
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1800(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 987
    :cond_2
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "run: current state is now "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "; exiting"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1055
    return-void

    .line 991
    :cond_3
    const/4 v5, 0x0

    .line 992
    .local v5, "buf":Ljava/nio/ByteBuffer;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->isSensory2Using()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1000
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->readChunkForSensory2()I

    move-result p1

    .line 1003
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1004
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->capacity()I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_1

    .line 1030
    :cond_4
    :goto_1
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;

    .line 1032
    .local v16, "spotter":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    const/4 v14, 0x0

    .line 1033
    .local v14, "ps":Ljava/lang/String;
    :try_start_1
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->isSensory2Using()Z

    move-result v17

    if-eqz v17, :cond_9

    .line 1034
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->startSensory2Spotting(ILjava/nio/ByteBuffer;Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;)Ljava/lang/String;

    move-result-object v14

    .line 1039
    :goto_3
    if-eqz v14, :cond_5

    .line 1040
    invoke-interface/range {v16 .. v16}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->getSpottedPhraseScore()F

    move-result v15

    .line 1041
    .local v15, "score":F
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "run(): phrase spotted = \""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "\", score="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1042
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v17, v0

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getAudioLogger()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->setScore(F)V

    .line 1043
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v17, v0

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getListener()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseDetected(Ljava/lang/String;)V

    .line 1044
    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->useSeamlessFeature(Ljava/lang/String;)Z

    move-result v17

    invoke-interface/range {v16 .. v16}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->getDeltaD()I

    move-result v18

    invoke-interface/range {v16 .. v16}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->getDeltaS()I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v14, v1, v2, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->handlePhraseSpotted(Ljava/lang/String;ZII)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 1047
    .end local v15    # "score":F
    :catch_0
    move-exception v8

    .line 1048
    .local v8, "ex":Ljava/lang/IllegalStateException;
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "run(): failed audio processing on spotter: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1009
    .end local v8    # "ex":Ljava/lang/IllegalStateException;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v14    # "ps":Ljava/lang/String;
    .end local v16    # "spotter":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :catch_1
    move-exception v7

    .line 1010
    .local v7, "e":Ljava/lang/InterruptedException;
    new-instance v17, Ljava/lang/IllegalStateException;

    const-string/jumbo v18, "WorkerThread: getBuffer failed"

    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 1014
    .end local v7    # "e":Ljava/lang/InterruptedException;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v17, v0

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->isStereo()Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1015
    const/4 v12, 0x0

    .line 1016
    .local v12, "offset":I
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_4
    const/16 v17, 0x2

    move/from16 v0, v17

    if-ge v11, v0, :cond_4

    .line 1017
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->readChunk()I

    .line 1018
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->getPrebuffer()Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v18, v0

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRawAudioBuffer:[S
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)[S

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->write([S)V

    .line 1019
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v9, v0, :cond_7

    .line 1020
    add-int/lit8 v13, v12, 0x1

    .end local v12    # "offset":I
    .local v13, "offset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v17, v0

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRawAudioBuffer:[S
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)[S

    move-result-object v17

    aget-short v17, v17, v9

    aput-short v17, v4, v12

    .line 1019
    add-int/lit8 v9, v9, 0x2

    move v12, v13

    .end local v13    # "offset":I
    .restart local v12    # "offset":I
    goto :goto_5

    .line 1016
    :cond_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 1024
    .end local v9    # "i":I
    .end local v11    # "j":I
    .end local v12    # "offset":I
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->readChunk()I

    .line 1025
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->getPrebuffer()Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v18, v0

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRawAudioBuffer:[S
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)[S

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->write([S)V

    .line 1026
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    move-object/from16 v17, v0

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRawAudioBuffer:[S
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)[S

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    move/from16 v20, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v4, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_1

    .line 1036
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v14    # "ps":Ljava/lang/String;
    .restart local v16    # "spotter":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :cond_9
    const/16 v17, 0x0

    :try_start_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    move/from16 v18, v0

    move-object/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-interface {v0, v4, v1, v2}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->processShortArray([SII)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v14

    goto/16 :goto_3

    .line 1050
    :catch_2
    move-exception v8

    .line 1051
    .local v8, "ex":Ljava/lang/Exception;
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "run(): "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method


# virtual methods
.method initAudio()Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    .line 834
    const/4 v3, 0x0

    .line 835
    .local v3, "success":Z
    const/4 v0, 0x0

    .line 836
    .local v0, "i":I
    new-array v2, v7, [I

    fill-array-data v2, :array_0

    .line 838
    .local v2, "samplerates":[I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    array-length v4, v2

    if-ge v1, v4, :cond_5

    .line 839
    aget v4, v2, v1

    iput v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->samplerate:I

    .line 840
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "initAudio: trying samplerate = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->samplerate:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    iget v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->samplerate:I

    const/16 v5, 0x10

    invoke-static {v4, v5, v7}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v4

    iput v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    .line 842
    iget v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    const/4 v5, -0x2

    if-ne v4, v5, :cond_1

    .line 838
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 843
    :cond_1
    iget v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    const/16 v5, 0x1000

    if-ge v4, v5, :cond_2

    const/16 v4, 0x12c0

    iput v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    .line 844
    :cond_2
    iget v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    rem-int/lit16 v4, v4, 0xa0

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    iget v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    rem-int/lit16 v5, v5, 0xa0

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    .line 845
    :cond_3
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "initAudio: buffersize = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->bufferSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    :goto_1
    if-nez v3, :cond_4

    if-ge v0, v7, :cond_4

    .line 847
    add-int/lit8 v0, v0, 0x1

    .line 849
    const/4 v3, 0x1

    goto :goto_1

    .line 863
    :cond_4
    if-eqz v3, :cond_0

    .line 865
    :cond_5
    return v3

    .line 836
    :array_0
    .array-data 4
        0x3e80
        0x1f40
    .end array-data
.end method

.method public run()V
    .locals 10

    .prologue
    .line 885
    const/4 v3, 0x0

    .line 887
    .local v3, "sizeOriginal":I
    :try_start_0
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 888
    :try_start_1
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v6, v6, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->presentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v8, v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->start:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    if-eq v6, v8, :cond_2

    .line 889
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "run(): presentState is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v9, v9, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->presentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "; returning"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 890
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 950
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "run(): going IDLE, mPsc.recording.stop() ..."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->stop()V

    .line 954
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 956
    :try_start_2
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isSpotting()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 957
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->STOPPING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v6, v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 959
    :cond_0
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v6, v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 962
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v6, v6, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    if-ne v6, v8, :cond_1

    .line 963
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v8, 0x0

    iput-object v8, v6, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    .line 969
    :cond_1
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "run(): phrase spotting thread ending"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    monitor-exit v7

    .line 972
    :goto_0
    return-void

    .line 970
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 893
    :cond_2
    :try_start_3
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->SPOTTING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v6, v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 895
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 897
    :try_start_4
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "run(): phrase spotting thread starting ..."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 899
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->createSpotters()Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/util/ArrayList;

    move-result-object v5

    .line 906
    .local v5, "spotters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-result v6

    if-gtz v6, :cond_7

    .line 950
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "run(): going IDLE, mPsc.recording.stop() ..."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->stop()V

    .line 954
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 956
    :try_start_5
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isSpotting()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 957
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->STOPPING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v6, v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 959
    :cond_3
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v6, v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 962
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v6, v6, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    if-ne v6, v8, :cond_4

    .line 963
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v8, 0x0

    iput-object v8, v6, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    .line 969
    :cond_4
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "run(): phrase spotting thread ending"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    monitor-exit v7

    goto :goto_0

    :catchall_1
    move-exception v6

    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v6

    .line 895
    .end local v5    # "spotters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    :catchall_2
    move-exception v6

    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v6
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 947
    :catch_0
    move-exception v0

    .line 948
    .local v0, "e":Ljava/lang/Exception;
    :try_start_8
    const-class v6, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Caught exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 950
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "run(): going IDLE, mPsc.recording.stop() ..."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->stop()V

    .line 954
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 956
    :try_start_9
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isSpotting()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 957
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->STOPPING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v6, v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 959
    :cond_5
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v6, v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 962
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v6, v6, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    if-ne v6, v8, :cond_6

    .line 963
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v8, 0x0

    iput-object v8, v6, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    .line 969
    :cond_6
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "run(): phrase spotting thread ending"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    monitor-exit v7

    goto/16 :goto_0

    :catchall_3
    move-exception v6

    monitor-exit v7
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v6

    .line 913
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v5    # "spotters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    :cond_7
    :try_start_a
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 915
    .local v4, "spotter":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :try_start_b
    invoke-interface {v4}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->init()V
    :try_end_b
    .catch Ljava/lang/IllegalStateException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    goto :goto_1

    .line 916
    :catch_1
    move-exception v6

    goto :goto_1

    .line 926
    .end local v4    # "spotter":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :cond_8
    :try_start_c
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    move-result-object v2

    .line 927
    .local v2, "recoMgr":Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isActive()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 928
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "run() CANNOT START SPOTTING: m_recoMgr.isActive() = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isActive()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    :goto_2
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "run(): phrase spotting thread going dormant"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 938
    .restart local v4    # "spotter":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :try_start_d
    invoke-interface {v4}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->destroy()V
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_d} :catch_2
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    goto :goto_3

    .line 939
    :catch_2
    move-exception v6

    goto :goto_3

    .line 930
    .end local v4    # "spotter":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :cond_9
    :try_start_e
    invoke-direct {p0, v3, v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->startSpotting(ILjava/util/ArrayList;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    goto :goto_2

    .line 950
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "recoMgr":Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    .end local v5    # "spotters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    :catchall_4
    move-exception v6

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "run(): going IDLE, mPsc.recording.stop() ..."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v7, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v7}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->stop()V

    .line 954
    iget-object v7, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;
    invoke-static {v7}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 956
    :try_start_f
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isSpotting()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 957
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v9, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->STOPPING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 959
    :cond_a
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v9, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 962
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v8, v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    if-ne v8, v9, :cond_b

    .line 963
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v9, 0x0

    iput-object v9, v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    .line 969
    :cond_b
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "run(): phrase spotting thread ending"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    monitor-exit v7
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    throw v6

    .line 950
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "recoMgr":Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    .restart local v5    # "spotters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    :cond_c
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "run(): going IDLE, mPsc.recording.stop() ..."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->stop()V

    .line 954
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 956
    :try_start_10
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isSpotting()Z

    move-result v6

    if-eqz v6, :cond_d

    .line 957
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->STOPPING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v6, v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 959
    :cond_d
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    sget-object v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {v6, v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V

    .line 962
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v6, v6, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    if-ne v6, v8, :cond_e

    .line 963
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPsc:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v8, 0x0

    iput-object v8, v6, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    .line 969
    :cond_e
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "run(): phrase spotting thread ending"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    monitor-exit v7

    goto/16 :goto_0

    :catchall_5
    move-exception v6

    monitor-exit v7
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    throw v6

    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "recoMgr":Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    .end local v5    # "spotters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    :catchall_6
    move-exception v6

    :try_start_11
    monitor-exit v7
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    throw v6

    .line 920
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v4    # "spotter":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    .restart local v5    # "spotters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    :catch_3
    move-exception v6

    goto/16 :goto_1

    .line 942
    .restart local v2    # "recoMgr":Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    :catch_4
    move-exception v6

    goto/16 :goto_3
.end method
