.class Lcom/vlingo/core/internal/contacts/ContactMatch$3;
.super Ljava/lang/Object;
.source "ContactMatch.java"

# interfaces
.implements Lcom/vlingo/sdk/util/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/contacts/ContactMatch;->selectMobilePhones(Ljava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/vlingo/sdk/util/Predicate",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/contacts/ContactMatch;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0

    .prologue
    .line 605
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch$3;->this$0:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/vlingo/core/internal/contacts/ContactData;)Z
    .locals 2
    .param p1, "object"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 608
    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 605
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/ContactMatch$3;->apply(Lcom/vlingo/core/internal/contacts/ContactData;)Z

    move-result v0

    return v0
.end method
