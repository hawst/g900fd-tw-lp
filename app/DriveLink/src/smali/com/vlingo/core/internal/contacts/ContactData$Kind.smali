.class public final enum Lcom/vlingo/core/internal/contacts/ContactData$Kind;
.super Ljava/lang/Enum;
.source "ContactData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/ContactData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Kind"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactData$Kind;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/contacts/ContactData$Kind;

.field public static final enum Address:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

.field public static final enum Birthday:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

.field public static final enum Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

.field public static final enum Facebook:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

.field public static final enum Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

.field public static final enum Unspecified:Lcom/vlingo/core/internal/contacts/ContactData$Kind;


# instance fields
.field private final acceptedText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const-string/jumbo v1, "Phone"

    const-string/jumbo v2, "sms:def"

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/core/internal/contacts/ContactData$Kind;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    .line 28
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const-string/jumbo v1, "Email"

    const-string/jumbo v2, "email:def"

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/core/internal/contacts/ContactData$Kind;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    .line 29
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const-string/jumbo v1, "Address"

    const-string/jumbo v2, "address:def"

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/core/internal/contacts/ContactData$Kind;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Address:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    .line 30
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const-string/jumbo v1, "Birthday"

    const-string/jumbo v2, "birthday:def"

    invoke-direct {v0, v1, v7, v2}, Lcom/vlingo/core/internal/contacts/ContactData$Kind;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Birthday:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    .line 31
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const-string/jumbo v1, "Facebook"

    const-string/jumbo v2, "facebook:msg"

    invoke-direct {v0, v1, v8, v2}, Lcom/vlingo/core/internal/contacts/ContactData$Kind;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Facebook:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    .line 32
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const-string/jumbo v1, "Unspecified"

    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactData$Kind;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Unspecified:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    .line 26
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Address:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Birthday:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Facebook:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Unspecified:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->$VALUES:[Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "acceptedText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->acceptedText:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData$Kind;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/contacts/ContactData$Kind;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->$VALUES:[Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/contacts/ContactData$Kind;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    return-object v0
.end method


# virtual methods
.method public getAcceptedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->acceptedText:Ljava/lang/String;

    return-object v0
.end method
