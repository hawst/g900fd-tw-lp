.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowSetAlarmHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private doit(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "time"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    .line 89
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/Alarm;-><init>()V

    .line 90
    .local v0, "alarm":Lcom/vlingo/core/internal/util/Alarm;
    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/util/Alarm;->setTimeFromCanonical(Ljava/lang/String;)V

    .line 91
    if-eqz p2, :cond_0

    .line 92
    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    .line 97
    :goto_0
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;

    invoke-interface {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;->alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;->queue()V

    .line 100
    return-void

    .line 94
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    goto :goto_0
.end method


# virtual methods
.method public actionSuccess()V
    .locals 3

    .prologue
    .line 37
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionSuccess()V

    .line 38
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_alarm_set:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 63
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 65
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v4

    const-string/jumbo v5, "alarm"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 67
    const-string/jumbo v4, "doit"

    invoke-static {p1, v4, v7, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 69
    .local v3, "time":Ljava/lang/String;
    invoke-direct {p0, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->doit(Ljava/lang/String;Z)V

    .line 84
    :goto_0
    return v7

    .line 72
    .end local v3    # "time":Ljava/lang/String;
    :cond_0
    const-string/jumbo v4, "confirm"

    invoke-static {p1, v4, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v1

    .line 73
    .local v1, "isConfirm":Z
    const-string/jumbo v4, "execute"

    invoke-static {p1, v4, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v2

    .line 74
    .local v2, "isExecute":Z
    const/4 v0, 0x0

    .line 75
    .local v0, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    if-nez v1, :cond_1

    if-nez v2, :cond_2

    .line 76
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeConfirmButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 79
    :cond_2
    const-string/jumbo v4, "time"

    invoke-static {p1, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 80
    .restart local v3    # "time":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v4, v5, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 82
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-static {p1}, Lcom/vlingo/core/internal/util/AlarmUtil;->extractAlarm(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/util/Alarm;

    move-result-object v6

    invoke-interface {v4, v5, v0, v6, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 45
    const-string/jumbo v2, "time"

    invoke-static {p1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamString(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, "time":Ljava/lang/String;
    const-string/jumbo v2, "enable"

    const/4 v3, 0x1

    invoke-static {p1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamBool(Landroid/content/Intent;Ljava/lang/String;Z)Z

    move-result v0

    .line 47
    .local v0, "enabled":Z
    invoke-direct {p0, v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->doit(Ljava/lang/String;Z)V

    .line 56
    .end local v0    # "enabled":Z
    .end local v1    # "time":Ljava/lang/String;
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.UpdateTime"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 49
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 50
    const-string/jumbo v2, "time"

    invoke-static {p1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamString(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 51
    .restart local v1    # "time":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    goto :goto_0

    .line 54
    .end local v1    # "time":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 34
    return-void
.end method
