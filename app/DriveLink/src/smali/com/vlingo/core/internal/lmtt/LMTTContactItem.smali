.class public Lcom/vlingo/core/internal/lmtt/LMTTContactItem;
.super Lcom/vlingo/core/internal/lmtt/LMTTItem;
.source "LMTTContactItem.java"


# instance fields
.field final companyName:Ljava/lang/String;

.field final firstName:Ljava/lang/String;

.field final fullName:Ljava/lang/String;

.field final lastName:Ljava/lang/String;

.field final middleName:Ljava/lang/String;

.field final nickname:Ljava/lang/String;

.field final phoneticFirstName:Ljava/lang/String;

.field final phoneticLastName:Ljava/lang/String;

.field final phoneticMiddleName:Ljava/lang/String;

.field final rawContactId:Ljava/lang/Long;


# direct methods
.method constructor <init>(JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 12
    .param p1, "uid"    # J
    .param p3, "changeType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    const/4 v1, 0x0

    .line 43
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    move-wide v9, p1

    move-object v11, p3

    invoke-direct/range {v0 .. v11}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 44
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 1
    .param p1, "firstName"    # Ljava/lang/String;
    .param p2, "middleName"    # Ljava/lang/String;
    .param p3, "lastName"    # Ljava/lang/String;
    .param p4, "nickname"    # Ljava/lang/String;
    .param p5, "fullName"    # Ljava/lang/String;
    .param p6, "phoneticFirstName"    # Ljava/lang/String;
    .param p7, "phoneticMiddleName"    # Ljava/lang/String;
    .param p8, "phoneticLastName"    # Ljava/lang/String;
    .param p9, "rawContactId"    # J
    .param p11, "changeType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 25
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_CONTACT:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-direct {p0, v0, p9, p10, p11}, Lcom/vlingo/core/internal/lmtt/LMTTItem;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 27
    if-nez p1, :cond_0

    const-string/jumbo p1, ""

    .line 28
    :cond_0
    if-nez p3, :cond_1

    const-string/jumbo p3, ""

    .line 30
    :cond_1
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->firstName:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->middleName:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->lastName:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->nickname:Ljava/lang/String;

    .line 34
    iput-object p5, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->fullName:Ljava/lang/String;

    .line 35
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->companyName:Ljava/lang/String;

    .line 36
    invoke-static {p9, p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->rawContactId:Ljava/lang/Long;

    .line 37
    iput-object p6, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticFirstName:Ljava/lang/String;

    .line 38
    iput-object p7, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticMiddleName:Ljava/lang/String;

    .line 39
    iput-object p8, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticLastName:Ljava/lang/String;

    .line 40
    return-void
.end method

.method private static makeHash(Ljava/lang/Object;)I
    .locals 1
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 89
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public getRawContactId()J
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->rawContactId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 75
    const/16 v0, 0x11

    .line 76
    .local v0, "code":I
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->firstName:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->makeHash(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 77
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->middleName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->makeHash(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 78
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->lastName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->makeHash(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 79
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->nickname:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->makeHash(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 80
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->fullName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->makeHash(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 81
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->companyName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->makeHash(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 82
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticFirstName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->makeHash(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 83
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticMiddleName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->makeHash(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 84
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticLastName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->makeHash(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 85
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "LMTTContact: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->firstName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->lastName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->companyName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticFirstName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticLastName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->uid:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " changeType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->changeType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " hashCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
