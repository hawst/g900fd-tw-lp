.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$1;
.super Ljava/lang/Object;
.source "PhraseSpotterControl.java"

# interfaces
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getListener()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$1;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 0
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 326
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 0
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 330
    return-void
.end method

.method public onPhraseSpotterStarted()V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 0

    .prologue
    .line 338
    return-void
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 0
    .param p1, "spottedPhrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 334
    return-void
.end method
