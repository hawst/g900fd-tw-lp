.class Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider$VersionCursor;
.super Landroid/database/AbstractCursor;
.source "ContactContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VersionCursor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider$VersionCursor;->this$0:Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;

    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;
    .param p2, "x1"    # Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider$1;

    .prologue
    .line 179
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider$VersionCursor;-><init>(Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;)V

    return-void
.end method


# virtual methods
.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x1

    return v0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 197
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getFloat(I)F
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method public getInt(I)I
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 182
    const/4 v0, 0x1

    return v0
.end method

.method public getLong(I)J
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 207
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getShort(I)S
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 217
    const/4 v0, 0x0

    return-object v0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 222
    const/4 v0, 0x0

    return v0
.end method
