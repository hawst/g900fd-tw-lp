.class public abstract Lcom/vlingo/core/internal/audio/TTSLocalEngine;
.super Ljava/lang/Object;
.source "TTSLocalEngine.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getEngines()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/speech/tts/TextToSpeech$EngineInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLanguage()Ljava/lang/Object;
.end method

.method public abstract setLanguage(Ljava/util/Locale;)I
.end method

.method public abstract setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I
.end method

.method public abstract setSpeechRate(F)V
.end method

.method public abstract shutdown()V
.end method

.method public abstract stop()I
.end method

.method public abstract synthesizeToFile(Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation
.end method
