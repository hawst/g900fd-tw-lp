.class public Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SettingChangeAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;


# instance fields
.field private VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field private alreadySet:Ljava/lang/String;

.field private confirmOff:Ljava/lang/String;

.field private confirmOffTTS:Ljava/lang/String;

.field private confirmOn:Ljava/lang/String;

.field private confirmOnTTS:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private state:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method

.method private fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "tts"    # Ljava/lang/String;

    .prologue
    .line 253
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object p2, p1

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    return-void
.end method

.method private onBluetoothOffState(Lcom/vlingo/core/internal/util/SystemServicesUtil;)V
    .locals 2
    .param p1, "ssu"    # Lcom/vlingo/core/internal/util/SystemServicesUtil;

    .prologue
    .line 183
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isBluetoothEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :goto_0
    return-void

    .line 186
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setBluetoothEnabled(Z)Z

    .line 187
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOff:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onBluetoothOnState(Lcom/vlingo/core/internal/util/SystemServicesUtil;)V
    .locals 2
    .param p1, "ssu"    # Lcom/vlingo/core/internal/util/SystemServicesUtil;

    .prologue
    .line 192
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isBluetoothEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :goto_0
    return-void

    .line 195
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setBluetoothEnabled(Z)Z

    .line 196
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOn:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onBluetoothToggleState(Lcom/vlingo/core/internal/util/SystemServicesUtil;)V
    .locals 3
    .param p1, "ssu"    # Lcom/vlingo/core/internal/util/SystemServicesUtil;

    .prologue
    .line 176
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isBluetoothEnabled()Z

    move-result v0

    .line 177
    .local v0, "en":Z
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setBluetoothEnabled(Z)Z

    .line 178
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOn:Ljava/lang/String;

    move-object v2, v1

    :goto_1
    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    :goto_2
    invoke-direct {p0, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    return-void

    .line 177
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 178
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOff:Ljava/lang/String;

    move-object v2, v1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    goto :goto_2
.end method

.method private onSafereaderOffState()V
    .locals 2

    .prologue
    .line 160
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 161
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->disablePhoneDrivingMode()V

    .line 162
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->stopSafeReading()V

    .line 163
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOff:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method private onSafereaderOnState()V
    .locals 2

    .prologue
    .line 169
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 170
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->enablePhoneDrivingMode()V

    .line 171
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->startSafeReading()V

    .line 172
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOn:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method private onSafereaderToggleState()V
    .locals 3

    .prologue
    .line 144
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v0

    .line 145
    .local v0, "enabled":Z
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->toggleDrivingModePhoneSetting()V

    .line 146
    if-nez v0, :cond_0

    .line 147
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 148
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->startSafeReading()V

    .line 153
    :goto_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOn:Ljava/lang/String;

    move-object v2, v1

    :goto_1
    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    :goto_2
    invoke-direct {p0, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    return-void

    .line 150
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 151
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->stopSafeReading()V

    goto :goto_0

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOff:Ljava/lang/String;

    move-object v2, v1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    goto :goto_2
.end method

.method private onWifiOffState(Lcom/vlingo/core/internal/util/SystemServicesUtil;Ljava/lang/String;)V
    .locals 3
    .param p1, "ssu"    # Lcom/vlingo/core/internal/util/SystemServicesUtil;
    .param p2, "settingChangeErrorString"    # Ljava/lang/String;

    .prologue
    .line 214
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :goto_0
    return-void

    .line 217
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setWifiEnabled(Z)Z

    move-result v0

    .line 218
    .local v0, "success":Z
    if-eqz v0, :cond_1

    .line 219
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOff:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 221
    :cond_1
    invoke-direct {p0, p2, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onWifiOnState(Lcom/vlingo/core/internal/util/SystemServicesUtil;Ljava/lang/String;)V
    .locals 3
    .param p1, "ssu"    # Lcom/vlingo/core/internal/util/SystemServicesUtil;
    .param p2, "settingChangeErrorString"    # Ljava/lang/String;

    .prologue
    .line 228
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 229
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :goto_0
    return-void

    .line 231
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setWifiEnabled(Z)Z

    move-result v0

    .line 236
    .local v0, "success":Z
    if-eqz v0, :cond_1

    .line 237
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOn:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 239
    :cond_1
    invoke-direct {p0, p2, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onWifiToggleState(Lcom/vlingo/core/internal/util/SystemServicesUtil;Ljava/lang/String;)V
    .locals 4
    .param p1, "ssu"    # Lcom/vlingo/core/internal/util/SystemServicesUtil;
    .param p2, "settingChangeErrorString"    # Ljava/lang/String;

    .prologue
    .line 202
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isWifiEnabled()Z

    move-result v0

    .line 203
    .local v0, "en":Z
    if-nez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {p1, v2}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setWifiEnabled(Z)Z

    move-result v1

    .line 204
    .local v1, "success":Z
    if-eqz v1, :cond_3

    .line 205
    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOn:Ljava/lang/String;

    move-object v3, v2

    :goto_1
    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    :goto_2
    invoke-direct {p0, v3, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :goto_3
    return-void

    .line 203
    .end local v1    # "success":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 205
    .restart local v1    # "success":Z
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOff:Ljava/lang/String;

    move-object v3, v2

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    goto :goto_2

    .line 208
    :cond_3
    invoke-direct {p0, p2, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private static toggleDrivingModePhoneSetting()V
    .locals 1

    .prologue
    .line 245
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->disablePhoneDrivingMode()V

    .line 250
    :goto_0
    return-void

    .line 248
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->disablePhoneDrivingMode()V

    goto :goto_0
.end method


# virtual methods
.method public alreadySet(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;
    .locals 0
    .param p1, "alreadySetTTS"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet:Ljava/lang/String;

    .line 66
    return-object p0
.end method

.method public bridge synthetic alreadySet(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->alreadySet(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public confirmOff(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;
    .locals 0
    .param p1, "confirmOffParam"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOff:Ljava/lang/String;

    .line 51
    return-object p0
.end method

.method public bridge synthetic confirmOff(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOff(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public confirmOffTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;
    .locals 0
    .param p1, "confirmOffTTSParam"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method public bridge synthetic confirmOffTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOffTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public confirmOn(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;
    .locals 0
    .param p1, "confirmOnParam"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOn:Ljava/lang/String;

    .line 46
    return-object p0
.end method

.method public bridge synthetic confirmOn(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOn(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public confirmOnTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;
    .locals 0
    .param p1, "confirmOnTTSParam"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    .line 56
    return-object p0
.end method

.method public bridge synthetic confirmOnTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->confirmOnTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method protected execute()V
    .locals 4

    .prologue
    .line 79
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->name:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "unsupported device"

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 139
    :goto_0
    return-void

    .line 84
    :cond_0
    new-instance v1, Lcom/vlingo/core/internal/util/SystemServicesUtil;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/vlingo/core/internal/util/SystemServicesUtil;-><init>(Landroid/content/Context;)V

    .line 85
    .local v1, "ssu":Lcom/vlingo/core/internal/util/SystemServicesUtil;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "wifi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 87
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wifi_setting_change_on_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "settingChangeErrorString":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v3, "on"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    invoke-direct {p0, v1, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->onWifiOnState(Lcom/vlingo/core/internal/util/SystemServicesUtil;Ljava/lang/String;)V

    .line 138
    .end local v0    # "settingChangeErrorString":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    goto :goto_0

    .line 90
    .restart local v0    # "settingChangeErrorString":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v3, "off"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 91
    invoke-direct {p0, v1, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->onWifiOffState(Lcom/vlingo/core/internal/util/SystemServicesUtil;Ljava/lang/String;)V

    goto :goto_1

    .line 92
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v3, "toggle"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 93
    invoke-direct {p0, v1, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->onWifiToggleState(Lcom/vlingo/core/internal/util/SystemServicesUtil;Ljava/lang/String;)V

    goto :goto_1

    .line 95
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "unsupported state"

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1

    .line 113
    .end local v0    # "settingChangeErrorString":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "bt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 114
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v3, "on"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 115
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->onBluetoothOnState(Lcom/vlingo/core/internal/util/SystemServicesUtil;)V

    goto :goto_1

    .line 116
    :cond_5
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v3, "off"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 117
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->onBluetoothOffState(Lcom/vlingo/core/internal/util/SystemServicesUtil;)V

    goto :goto_1

    .line 118
    :cond_6
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v3, "toggle"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 119
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->onBluetoothToggleState(Lcom/vlingo/core/internal/util/SystemServicesUtil;)V

    goto :goto_1

    .line 121
    :cond_7
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "unsupported state"

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1

    .line 123
    :cond_8
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "safereader"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 124
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v3, "on"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 125
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->onSafereaderOnState()V

    goto/16 :goto_1

    .line 126
    :cond_9
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v3, "off"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 127
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->onSafereaderOffState()V

    goto/16 :goto_1

    .line 128
    :cond_a
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v3, "toggle"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 129
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->onSafereaderToggleState()V

    goto/16 :goto_1

    .line 133
    :cond_b
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "unsupported state"

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 136
    :cond_c
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "unsupported device"

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;
    .locals 0
    .param p1, "nameParam"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->name:Ljava/lang/String;

    .line 36
    return-object p0
.end method

.method public bridge synthetic name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public state(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;
    .locals 0
    .param p1, "stateParam"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state:Ljava/lang/String;

    .line 41
    return-object p0
.end method

.method public bridge synthetic state(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->state(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public vvsListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 71
    return-object p0
.end method

.method public bridge synthetic vvsListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;->vvsListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;

    move-result-object v0

    return-object v0
.end method
