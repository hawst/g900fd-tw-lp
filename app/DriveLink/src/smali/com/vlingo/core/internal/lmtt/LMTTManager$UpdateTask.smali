.class final Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
.super Ljava/lang/Object;
.source "LMTTManager.java"

# interfaces
.implements Lcom/vlingo/sdk/training/VLTrainerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmtt/LMTTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "UpdateTask"
.end annotation


# static fields
.field private static final DEFAULT_TASK_MAX_RETRIES:I = 0x1


# instance fields
.field private mClearLMTT:Z

.field private mDeviceItemCounts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

.field private mRetryCount:I

.field final synthetic this$0:Lcom/vlingo/core/internal/lmtt/LMTTManager;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;Z)V
    .locals 1
    .param p2, "lmttUpdateType"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .param p3, "clearLMTT"    # Z

    .prologue
    .line 301
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZI)V

    .line 302
    return-void
.end method

.method private constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZI)V
    .locals 0
    .param p2, "lmttUpdateType"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .param p3, "clearLMTT"    # Z
    .param p4, "retryCount"    # I

    .prologue
    .line 304
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 305
    iput-boolean p3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mClearLMTT:Z

    .line 306
    iput-object p2, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    .line 307
    iput p4, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mRetryCount:I

    .line 308
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZLcom/vlingo/core/internal/lmtt/LMTTManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTManager;
    .param p2, "x1"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$1;

    .prologue
    .line 291
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    .prologue
    .line 291
    iget-boolean v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mClearLMTT:Z

    return v0
.end method

.method static synthetic access$302(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    .param p1, "x1"    # Z

    .prologue
    .line 291
    iput-boolean p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mClearLMTT:Z

    return p1
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    .prologue
    .line 291
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->runTask()V

    return-void
.end method

.method private end()V
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTManager;->mUploadHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->access$800(Lcom/vlingo/core/internal/lmtt/LMTTManager;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 418
    return-void
.end method

.method private retry(Z)V
    .locals 8
    .param p1, "forceRetry"    # Z

    .prologue
    const/4 v7, 0x1

    .line 394
    const-string/jumbo v3, "lmtt.task_retries"

    invoke-static {v3, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 395
    .local v0, "maxRetries":I
    if-nez p1, :cond_0

    iget v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mRetryCount:I

    if-ge v3, v0, :cond_2

    .line 397
    :cond_0
    if-nez p1, :cond_1

    .line 398
    iget v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mRetryCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mRetryCount:I

    .line 402
    :cond_1
    new-instance v2, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    iget-object v4, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    iget-boolean v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mClearLMTT:Z

    iget v6, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mRetryCount:I

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZI)V

    .line 403
    .local v2, "retryTask":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTManager;->mUploadHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->access$800(Lcom/vlingo/core/internal/lmtt/LMTTManager;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v3, v7, v4, v5, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 404
    .local v1, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTManager;->mUploadHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->access$800(Lcom/vlingo/core/internal/lmtt/LMTTManager;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 410
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "retryTask":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->end()V

    .line 411
    return-void
.end method

.method private runTask()V
    .locals 15

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 314
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mDeviceItemCounts:Ljava/util/HashMap;

    .line 315
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 317
    .local v2, "changedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/lmtt/LMTTItem;>;"
    const/4 v3, 0x1

    .line 320
    .local v3, "isBulk":Z
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v14, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v5, v14, :cond_1

    .line 321
    const/4 v5, 0x2

    new-array v12, v5, [Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    aput-object v5, v12, v4

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    aput-object v5, v12, v1

    .line 330
    .local v12, "lmttItemTypes":[Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    :goto_0
    move-object v6, v12

    .local v6, "arr$":[Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    array-length v10, v6

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_1
    if-ge v9, v10, :cond_5

    aget-object v11, v6, v9

    .line 331
    .local v11, "lmttItemType":Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    iget-boolean v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mClearLMTT:Z

    if-eqz v5, :cond_0

    .line 334
    invoke-static {v11}, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->clearLMTTTable(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;)V

    .line 337
    :cond_0
    invoke-static {v11}, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->getSynchedItems(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;)Ljava/util/HashMap;

    move-result-object v13

    .line 338
    .local v13, "synchedItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    if-nez v13, :cond_3

    .line 342
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->retry(Z)V

    .line 384
    .end local v11    # "lmttItemType":Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .end local v13    # "synchedItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :goto_2
    return-void

    .line 323
    .end local v6    # "arr$":[Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v12    # "lmttItemTypes":[Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    :cond_1
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v14, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v5, v14, :cond_2

    .line 324
    new-array v12, v1, [Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_CONTACT:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    aput-object v5, v12, v4

    .restart local v12    # "lmttItemTypes":[Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    goto :goto_0

    .line 327
    .end local v12    # "lmttItemTypes":[Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    :cond_2
    new-array v12, v4, [Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    .restart local v12    # "lmttItemTypes":[Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    goto :goto_0

    .line 346
    .restart local v6    # "arr$":[Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .restart local v9    # "i$":I
    .restart local v10    # "len$":I
    .restart local v11    # "lmttItemType":Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .restart local v13    # "synchedItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v13}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    move v3, v1

    .line 352
    :goto_3
    # invokes: Lcom/vlingo/core/internal/lmtt/LMTTManager;->getChangedItemsList(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;Ljava/util/HashMap;Ljava/util/ArrayList;)I
    invoke-static {v11, v13, v2}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->access$700(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;Ljava/util/HashMap;Ljava/util/ArrayList;)I

    move-result v7

    .line 353
    .local v7, "deviceItemCount":I
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mDeviceItemCounts:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v11, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .end local v7    # "deviceItemCount":I
    :cond_4
    move v3, v4

    .line 346
    goto :goto_3

    .line 359
    .end local v11    # "lmttItemType":Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .end local v13    # "synchedItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_6

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_LANGUAGE_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v1, v5, :cond_c

    .line 361
    :cond_6
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v1, v5}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 362
    const-string/jumbo v1, "lmtt.sync_status_contact"

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 367
    :cond_7
    :goto_4
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    iget-object v4, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTManager;->mUploadHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->access$800(Lcom/vlingo/core/internal/lmtt/LMTTManager;)Landroid/os/Handler;

    move-result-object v5

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;Ljava/util/ArrayList;ZLcom/vlingo/sdk/training/VLTrainerListener;Landroid/os/Handler;)V

    .line 368
    .local v0, "chunkUpdate":Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->transmitChunks()V

    .line 370
    const-string/jumbo v8, ""

    .line 371
    .local v8, "helpTag":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v4, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v1, v4, :cond_a

    .line 372
    if-eqz v3, :cond_9

    const-string/jumbo v8, "lmtt-scheduled-music-full"

    .line 377
    :goto_5
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->helpPageViewed(Ljava/lang/String;)V

    goto :goto_2

    .line 363
    .end local v0    # "chunkUpdate":Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;
    .end local v8    # "helpTag":Ljava/lang/String;
    :cond_8
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v1, v5}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 364
    const-string/jumbo v1, "lmtt.sync_status_music"

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_4

    .line 372
    .restart local v0    # "chunkUpdate":Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;
    .restart local v8    # "helpTag":Ljava/lang/String;
    :cond_9
    const-string/jumbo v8, "lmtt-scheduled-music-partial"

    goto :goto_5

    .line 375
    :cond_a
    if-eqz v3, :cond_b

    const-string/jumbo v8, "lmtt-scheduled-contact-full"

    :goto_6
    goto :goto_5

    :cond_b
    const-string/jumbo v8, "lmtt-scheduled-contact-partial"

    goto :goto_6

    .line 382
    .end local v0    # "chunkUpdate":Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;
    .end local v8    # "helpTag":Ljava/lang/String;
    :cond_c
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->end()V

    goto/16 :goto_2
.end method


# virtual methods
.method public onError(Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Lcom/vlingo/sdk/training/VLTrainerErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 527
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v0, v1, :cond_0

    .line 528
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "lmtt-comm-error-music-update"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->helpPageViewed(Ljava/lang/String;)V

    .line 536
    :goto_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->end()V

    .line 537
    return-void

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v0, v1, :cond_1

    .line 531
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "lmtt-comm-error-contact-update"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->helpPageViewed(Ljava/lang/String;)V

    goto :goto_0

    .line 534
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "lmtt-comm-error-language-update"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->helpPageViewed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onUpdateReceived(Ljava/util/HashMap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "serverCounts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;Ljava/lang/Integer;>;"
    const/4 v7, 0x1

    .line 439
    const/4 v0, 0x1

    .line 440
    .local v0, "countsMatch":Z
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mDeviceItemCounts:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    .line 441
    .local v3, "id":Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    const/4 v1, 0x0

    .line 442
    .local v1, "deviceItemCount":I
    const/4 v4, 0x0

    .line 443
    .local v4, "serverItemCount":I
    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    if-ne v3, v5, :cond_4

    .line 444
    sget-object v5, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->SONG:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_3

    .line 447
    const/4 v0, 0x0

    .line 473
    :cond_1
    :goto_0
    if-eq v1, v4, :cond_0

    .line 476
    const/4 v0, 0x0

    .line 481
    .end local v1    # "deviceItemCount":I
    .end local v3    # "id":Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .end local v4    # "serverItemCount":I
    :cond_2
    if-nez v0, :cond_8

    .line 485
    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTManager$1;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTManager$LmttUpdateType:[I

    iget-object v6, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 500
    :goto_1
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->retry(Z)V

    .line 517
    :goto_2
    return-void

    .line 449
    .restart local v1    # "deviceItemCount":I
    .restart local v3    # "id":Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .restart local v4    # "serverItemCount":I
    :cond_3
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mDeviceItemCounts:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 450
    sget-object v5, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->SONG:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_0

    .line 453
    :cond_4
    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    if-ne v3, v5, :cond_6

    .line 454
    sget-object v5, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->PLAYLIST:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_5

    .line 457
    const/4 v0, 0x0

    goto :goto_0

    .line 459
    :cond_5
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mDeviceItemCounts:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 460
    sget-object v5, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->PLAYLIST:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_0

    .line 463
    :cond_6
    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_CONTACT:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    if-ne v3, v5, :cond_1

    .line 464
    sget-object v5, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->CONTACT:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_7

    .line 467
    const/4 v0, 0x0

    goto :goto_0

    .line 469
    :cond_7
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mDeviceItemCounts:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 470
    sget-object v5, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->CONTACT:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_0

    .line 487
    .end local v1    # "deviceItemCount":I
    .end local v3    # "id":Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .end local v4    # "serverItemCount":I
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v5

    const-string/jumbo v6, "lmtt-reschedule-full-contact-update"

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->helpPageViewed(Ljava/lang/String;)V

    .line 488
    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_CONTACT:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-static {v5}, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->clearLMTTTable(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;)V

    goto :goto_1

    .line 491
    :pswitch_1
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v5

    const-string/jumbo v6, "lmtt-reschedule-full-music-update"

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->helpPageViewed(Ljava/lang/String;)V

    .line 492
    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-static {v5}, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->clearLMTTTable(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;)V

    .line 493
    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-static {v5}, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->clearLMTTTable(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;)V

    goto/16 :goto_1

    .line 505
    :cond_8
    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTManager$1;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTManager$LmttUpdateType:[I

    iget-object v6, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 515
    :goto_3
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->end()V

    goto/16 :goto_2

    .line 507
    :pswitch_2
    const-string/jumbo v5, "lmtt.sync_status_contact"

    invoke-static {v5, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_3

    .line 510
    :pswitch_3
    const-string/jumbo v5, "lmtt.sync_status_music"

    invoke-static {v5, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_3

    .line 485
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 505
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 421
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 422
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 423
    const-string/jumbo v1, " [mLmttUpdateType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 424
    const-string/jumbo v1, ", mClearLMTT="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mClearLMTT:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 425
    const-string/jumbo v1, ", mRetryCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 426
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
