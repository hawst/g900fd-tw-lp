.class public final Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
.super Ljava/lang/Object;
.source "WidgetDecorator.java"


# instance fields
.field private next:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

.field private final type:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->next:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 27
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->NoOp:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->type:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 28
    return-void
.end method

.method private constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V
    .locals 1
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->next:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 31
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->type:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .line 32
    return-void
.end method

.method public static makeCancelButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->CancelButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeConfirmButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ConfirmButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeContactShowAddress()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeContactShowBirthday()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 133
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowBirthday:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeContactShowEmail()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 124
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeContactShowPhone()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeReadOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ReadOnly:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeReplaceable()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->Replaceable:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeSendButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->SendButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 174
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ShowMessageBody:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeTimerCancelCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerCancelCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeTimerResetCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 93
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerResetCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeTimerRestartCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 96
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerRestartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeTimerShowCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerShowCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeTimerStartCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerStartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeTimerStopCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 90
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerStopCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static makeWebSearchButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 80
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->WebSearchButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    return-object v0
.end method

.method public static noOp()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>()V

    return-object v0
.end method

.method private prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 0
    .param p1, "head"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .prologue
    .line 18
    iput-object p0, p1, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->next:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 19
    return-object p1
.end method


# virtual methods
.method public cancelButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->CancelButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public confirmButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ConfirmButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public constactShowHomeAddressOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomeAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public constactShowHomeEmailOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomeEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public constactShowHomePhoneOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public constactShowMobilePhoneOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowMobilePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public constactShowWorkAddressOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 142
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public constactShowWorkEmailOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public constactShowWorkPhoneOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public contactShowAddress()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 163
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public contactShowBirthday()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 160
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowBirthday:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public contactShowEmail()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 157
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public contactShowPhone()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 166
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z
    .locals 1
    .param p1, "decoratorType"    # Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->type:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    if-ne v0, p1, :cond_0

    .line 185
    const/4 v0, 0x1

    .line 192
    :goto_0
    return v0

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->next:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    if-eqz v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->next:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    goto :goto_0

    .line 192
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ReadOnly:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public sendButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->SendButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public showMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ShowMessageBody:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public timerResetCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerResetCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public timerRestartCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerRestartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public timerShowCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 120
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerShowCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public timerStartCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerStartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public timerStopCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerStopCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method

.method public webSearchButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->WebSearchButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;-><init>(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->prepend(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    return-object v0
.end method
