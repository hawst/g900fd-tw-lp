.class public abstract Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;
.super Ljava/lang/Object;
.source "MusicUtil.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/MusicUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MusicInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private id:I

.field protected subtitle:Ljava/lang/String;

.field protected title:Ljava/lang/String;

.field protected type:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "subtitle"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->title:Ljava/lang/String;

    .line 107
    iput-object p2, p0, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->subtitle:Ljava/lang/String;

    .line 108
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;)I
    .locals 2
    .param p1, "another"    # Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 98
    check-cast p1, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->compareTo(Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;)I

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->id:I

    return v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->subtitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/vlingo/core/internal/util/MusicUtil$MusicType;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->type:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    return-object v0
.end method

.method protected setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 138
    iput p1, p0, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->id:I

    .line 139
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->title:Ljava/lang/String;

    .line 123
    return-void
.end method
