.class public Lcom/vlingo/core/internal/debug/PreferenceBuilder;
.super Ljava/lang/Object;
.source "PreferenceBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/preference/Preference;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static defaultUpdateActions:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;


# instance fields
.field private clickListeners:Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;

.field private defaultTo:Ljava/lang/Object;

.field private listeners:Lcom/vlingo/core/internal/debug/PreferenceChangeListenerHolder;

.field private setting:Ljava/lang/String;

.field private showAsSummary:Z

.field private showSummaryAsValueAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

.field private summary:Ljava/lang/Object;

.field private updateListeners:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

.field private value:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150
    new-instance v0, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->defaultUpdateActions:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    new-instance v0, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    sget-object v1, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->defaultUpdateActions:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;-><init>(Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->updateListeners:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    .line 147
    new-instance v0, Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->clickListeners:Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;

    .line 148
    new-instance v0, Lcom/vlingo/core/internal/debug/PreferenceChangeListenerHolder;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/PreferenceChangeListenerHolder;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->listeners:Lcom/vlingo/core/internal/debug/PreferenceChangeListenerHolder;

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "setting"    # Ljava/lang/String;

    .prologue
    .line 28
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    new-instance v0, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    sget-object v1, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->defaultUpdateActions:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;-><init>(Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->updateListeners:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    .line 147
    new-instance v0, Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->clickListeners:Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;

    .line 148
    new-instance v0, Lcom/vlingo/core/internal/debug/PreferenceChangeListenerHolder;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/PreferenceChangeListenerHolder;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->listeners:Lcom/vlingo/core/internal/debug/PreferenceChangeListenerHolder;

    .line 29
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->onSetting(Ljava/lang/String;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    .line 30
    return-void
.end method

.method public static onAllUpdatedPreferences(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    .prologue
    .line 24
    sget-object v0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->defaultUpdateActions:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;->add(Ljava/lang/Object;)Z

    .line 25
    return-void
.end method


# virtual methods
.method protected attach(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;
    .locals 2
    .param p1, "activity"    # Landroid/preference/PreferenceActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/PreferenceActivity;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 125
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->getSetting()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 126
    .local v0, "t":Landroid/preference/Preference;, "TT;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->getDefault()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setDefaultValue(Ljava/lang/Object;)V

    .line 127
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->getListener()Landroid/preference/Preference$OnPreferenceChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 128
    return-object v0
.end method

.method public defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;
    .locals 0
    .param p1, "defaultToValue"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->defaultTo:Ljava/lang/Object;

    .line 67
    return-object p0
.end method

.method public getClickListener()Landroid/preference/Preference$OnPreferenceClickListener;
    .locals 1

    .prologue
    .line 49
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->clickListeners:Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;

    return-object v0
.end method

.method public getDefault()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->defaultTo:Ljava/lang/Object;

    return-object v0
.end method

.method public getListener()Landroid/preference/Preference$OnPreferenceChangeListener;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->listeners:Lcom/vlingo/core/internal/debug/PreferenceChangeListenerHolder;

    return-object v0
.end method

.method public getSetting()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->setting:Ljava/lang/String;

    return-object v0
.end method

.method protected getShowAsSummary()Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;
    .locals 1

    .prologue
    .line 132
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showSummaryAsValueAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    return-object v0
.end method

.method public getSummary()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->isShowAsSummary()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->getValue()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->summary:Ljava/lang/Object;

    goto :goto_0
.end method

.method public getUpdateListener()Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;
    .locals 1

    .prologue
    .line 57
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->updateListeners:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->value:Ljava/lang/Object;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->getDefault()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->value:Ljava/lang/Object;

    goto :goto_0
.end method

.method protected hasValue()Z
    .locals 1

    .prologue
    .line 120
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->value:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isShowAsSummary()Z
    .locals 1

    .prologue
    .line 116
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-boolean v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary:Z

    return v0
.end method

.method public onPreferenceChange(Landroid/preference/Preference$OnPreferenceChangeListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;
    .locals 1
    .param p1, "listener"    # Landroid/preference/Preference$OnPreferenceChangeListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/Preference$OnPreferenceChangeListener;",
            ")",
            "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->listeners:Lcom/vlingo/core/internal/debug/PreferenceChangeListenerHolder;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/debug/PreferenceChangeListenerHolder;->add(Ljava/lang/Object;)Z

    .line 92
    return-object p0
.end method

.method public onPreferenceClick(Landroid/preference/Preference$OnPreferenceClickListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;
    .locals 1
    .param p1, "listener"    # Landroid/preference/Preference$OnPreferenceClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/Preference$OnPreferenceClickListener;",
            ")",
            "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->clickListeners:Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;->add(Ljava/lang/Object;)Z

    .line 102
    return-object p0
.end method

.method public onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;",
            ")",
            "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 96
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->updateListeners:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;->add(Ljava/lang/Object;)Z

    .line 97
    return-object p0
.end method

.method public onSetting(Ljava/lang/String;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;
    .locals 0
    .param p1, "settingValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->setting:Ljava/lang/String;

    .line 62
    return-object p0
.end method

.method public register(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;
    .locals 1
    .param p1, "activity"    # Landroid/preference/PreferenceActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/PreferenceActivity;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->attach(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method public register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;
    .locals 3
    .param p1, "activity"    # Landroid/preference/PreferenceActivity;
    .param p2, "updater"    # Lcom/vlingo/core/internal/debug/PreferencesUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/PreferenceActivity;",
            "Lcom/vlingo/core/internal/debug/PreferencesUpdater;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;

    move-result-object v0

    .line 111
    .local v0, "pref":Landroid/preference/Preference;, "TT;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->getSetting()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->updateListeners:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    invoke-virtual {v2, v0}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;->setPreference(Landroid/preference/Preference;)Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->register(Ljava/lang/String;Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;)V

    .line 112
    return-object v0
.end method

.method protected setShowAsSummary(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    .prologue
    .line 136
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showSummaryAsValueAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    .line 137
    return-void
.end method

.method public showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary:Z

    .line 86
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->getShowAsSummary()Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    .line 87
    return-object p0
.end method

.method public withSummary(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;
    .locals 2
    .param p1, "summaryValue"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->summary:Ljava/lang/Object;

    .line 77
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->isShowAsSummary()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->updateListeners:Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->getShowAsSummary()Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/debug/PreferenceUpdateListenerHolder;->remove(Ljava/lang/Object;)Z

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary:Z

    .line 81
    :cond_0
    return-object p0
.end method

.method public withValue(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;
    .locals 0
    .param p1, "v"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lcom/vlingo/core/internal/debug/PreferenceBuilder;, "Lcom/vlingo/core/internal/debug/PreferenceBuilder<TT;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->value:Ljava/lang/Object;

    .line 72
    return-object p0
.end method
