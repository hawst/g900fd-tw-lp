.class Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;
.super Landroid/database/ContentObserver;
.source "SMSMMSAlertSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SMSMMSContentObserver"
.end annotation


# instance fields
.field final TAG:Ljava/lang/String;

.field final synthetic this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Landroid/os/Handler;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)V
    .locals 1
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "safeReaderAlertChangeListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    .line 209
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 207
    const-class v0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->TAG:Ljava/lang/String;

    .line 210
    # setter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderSMSAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    invoke-static {p1, p3}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$202(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    .line 211
    return-void
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->isCCForeground()Z

    move-result v0

    return v0
.end method

.method private isCCForeground()Z
    .locals 5

    .prologue
    .line 258
    iget-object v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    # getter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$600(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "activity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 259
    .local v1, "mActivityManager":Landroid/app/ActivityManager;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 260
    .local v2, "runningTask":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 261
    .local v0, "ar":Landroid/app/ActivityManager$RunningTaskInfo;
    const-string/jumbo v3, "com.nuance.sample"

    iget-object v4, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    return v3
.end method


# virtual methods
.method public deliverSelfNotifications()Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    return v0
.end method

.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 221
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 222
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onChange() : selfChange="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    if-eqz p1, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    new-instance v0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver$1;-><init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
