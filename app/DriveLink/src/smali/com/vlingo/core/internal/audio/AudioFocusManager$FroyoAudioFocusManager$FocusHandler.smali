.class final Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;
.super Landroid/os/Handler;
.source "AudioFocusManager.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FocusHandler"
.end annotation


# static fields
.field private static final ABANDON_FOCUS:I = 0x2

.field private static final REQUEST_FOCUS:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;)V
    .locals 1

    .prologue
    .line 225
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    .line 226
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 227
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;Lcom/vlingo/core/internal/audio/AudioFocusManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;
    .param p2, "x1"    # Lcom/vlingo/core/internal/audio/AudioFocusManager$1;

    .prologue
    .line 221
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;-><init>(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 229
    monitor-enter p0

    const/4 v0, 0x0

    .line 230
    .local v0, "result":I
    :try_start_0
    iget v2, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v2, :pswitch_data_0

    .line 279
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 237
    :pswitch_0
    :try_start_1
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$400(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;)Landroid/media/AudioManager;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, p0, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 238
    if-nez v0, :cond_1

    .line 240
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 241
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    .line 242
    .local v1, "retryMsg":Landroid/os/Message;
    const-wide/16 v2, 0xc8

    invoke-virtual {p0, v1, v2, v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 229
    .end local v1    # "retryMsg":Landroid/os/Message;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 246
    :cond_1
    :try_start_2
    invoke-static {}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->getInstance()Lcom/vlingo/core/internal/audio/RemoteControlManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->setPlaybackState(I)V

    .line 247
    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mSetTaskOnAudioFocus:Ljava/lang/Runnable;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$500()Ljava/lang/Runnable;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 248
    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mSetTaskOnAudioFocus:Ljava/lang/Runnable;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$500()Ljava/lang/Runnable;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 249
    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mSetTaskOnAudioFocus:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$502(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 252
    :cond_2
    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$300()Z

    move-result v2

    if-nez v2, :cond_3

    .line 254
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->broadcastFocuseChanged(I)V
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$600(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;I)V

    .line 256
    :cond_3
    const/4 v2, 0x1

    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$302(Z)Z

    goto :goto_0

    .line 263
    :pswitch_1
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$400(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;)Landroid/media/AudioManager;

    move-result-object v2

    if-eqz v2, :cond_0

    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$300()Z

    move-result v2

    if-nez v2, :cond_4

    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->isAudioFocusLossTransient:Z
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$700()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bAbandonInSync:Z
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$200()Z

    move-result v2

    if-nez v2, :cond_0

    .line 265
    invoke-static {}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->getInstance()Lcom/vlingo/core/internal/audio/RemoteControlManager;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->setPlaybackState(I)V

    .line 266
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$400(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;)Landroid/media/AudioManager;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    .line 267
    if-ne v0, v4, :cond_0

    .line 268
    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$302(Z)Z

    .line 269
    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->isAudioFocusLossTransient:Z
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$702(Z)Z

    .line 270
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    const/4 v3, -0x1

    # invokes: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->broadcastFocuseChanged(I)V
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$600(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 230
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onAudioFocusChange(I)V
    .locals 7
    .param p1, "focusChange"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x2

    const/4 v3, -0x3

    .line 281
    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[LatencyCheck] onAudioFocusChange("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    if-eq p1, v4, :cond_0

    if-ne p1, v3, :cond_3

    .line 299
    :cond_0
    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->isAudioFocusLossTransient:Z
    invoke-static {v6}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$702(Z)Z

    .line 304
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 338
    :cond_1
    :goto_1
    :pswitch_0
    # invokes: Lcom/vlingo/core/internal/audio/AudioFocusManager;->notifyAudioFocusChangeToListeners(I)V
    invoke-static {p1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$900(I)V

    .line 346
    if-eq p1, v4, :cond_2

    if-ne p1, v3, :cond_5

    .line 353
    :cond_2
    :goto_2
    return-void

    .line 301
    :cond_3
    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->isAudioFocusLossTransient:Z
    invoke-static {v5}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$702(Z)Z

    goto :goto_0

    .line 308
    :pswitch_1
    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z
    invoke-static {v6}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$302(Z)Z

    goto :goto_1

    .line 313
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 316
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 317
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 324
    :cond_4
    :pswitch_3
    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z
    invoke-static {v5}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$302(Z)Z

    .line 328
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$400(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;)Landroid/media/AudioManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 329
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_1

    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->isAudioFocusLossTransient:Z
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$700()Z

    move-result v0

    if-nez v0, :cond_1

    .line 330
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$400(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;)Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    goto :goto_1

    .line 350
    :cond_5
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    # invokes: Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->broadcastFocuseChanged(I)V
    invoke-static {v0, p1}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->access$600(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;I)V

    goto :goto_2

    .line 304
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
