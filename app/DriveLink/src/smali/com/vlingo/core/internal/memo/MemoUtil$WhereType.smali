.class final enum Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;
.super Ljava/lang/Enum;
.source "MemoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/memo/MemoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "WhereType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

.field public static final enum CONTAINS:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

.field public static final enum EXACT:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

.field public static final enum EXACT_WITHOUT_SPACES:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

.field public static final enum FALLBACK:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 333
    new-instance v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    const-string/jumbo v1, "EXACT"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->EXACT:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    new-instance v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    const-string/jumbo v1, "EXACT_WITHOUT_SPACES"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->EXACT_WITHOUT_SPACES:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    new-instance v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    const-string/jumbo v1, "CONTAINS"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->CONTAINS:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    new-instance v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    const-string/jumbo v1, "FALLBACK"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->FALLBACK:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    sget-object v1, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->EXACT:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->EXACT_WITHOUT_SPACES:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->CONTAINS:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->FALLBACK:Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->$VALUES:[Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 333
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 333
    const-class v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;
    .locals 1

    .prologue
    .line 333
    sget-object v0, Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->$VALUES:[Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/memo/MemoUtil$WhereType;

    return-object v0
.end method
