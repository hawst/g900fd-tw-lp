.class Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;
.super Ljava/lang/Object;
.source "ServiceBindingUtil.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/ServiceBindingUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ServiceBinder"
.end annotation


# instance fields
.field interfaceClass:Ljava/lang/Class;

.field mCallback:Landroid/content/ServiceConnection;


# direct methods
.method constructor <init>(Landroid/content/ServiceConnection;Ljava/lang/Class;)V
    .locals 0
    .param p1, "callback"    # Landroid/content/ServiceConnection;
    .param p2, "interfaceClass"    # Ljava/lang/Class;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    .line 83
    iput-object p2, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;->interfaceClass:Ljava/lang/Class;

    .line 84
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 88
    # getter for: Lcom/vlingo/core/internal/util/ServiceBindingUtil;->sServiceMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->access$000()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;->interfaceClass:Ljava/lang/Class;

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 93
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 101
    :cond_0
    # getter for: Lcom/vlingo/core/internal/util/ServiceBindingUtil;->sServiceMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->access$000()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;->interfaceClass:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    return-void
.end method
