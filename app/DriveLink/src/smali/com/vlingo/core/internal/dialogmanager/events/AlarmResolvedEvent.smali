.class public Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;
.super Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;
.source "AlarmResolvedEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent",
        "<",
        "Lcom/vlingo/core/internal/util/Alarm;",
        ">;"
    }
.end annotation


# instance fields
.field private final NAME:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;-><init>()V

    .line 23
    const-string/jumbo v0, "alarm-resolved"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;->NAME:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/util/List;II)V
    .locals 1
    .param p2, "offset"    # I
    .param p3, "totalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p1, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;-><init>(Ljava/util/List;II)V

    .line 23
    const-string/jumbo v0, "alarm-resolved"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;->NAME:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string/jumbo v0, "alarm-resolved"

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 9

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_0

    const/4 v6, 0x0

    .line 63
    :goto_0
    return-object v6

    .line 42
    :cond_0
    new-instance v3, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v6, "alarm-resolved"

    invoke-direct {v3, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 43
    .local v3, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;->writeTotalCount(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)V

    .line 44
    new-instance v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v6, "alarms"

    invoke-direct {v2, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 45
    .local v2, "alarmsBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;->getOffset()I

    move-result v5

    .line 47
    .local v5, "id":I
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/Alarm;

    .line 48
    .local v0, "alarm":Lcom/vlingo/core/internal/util/Alarm;
    new-instance v1, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v6, "alarm"

    invoke-direct {v1, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 50
    .local v1, "alarmBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    const-string/jumbo v6, "id"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 51
    add-int/lit8 v5, v5, 0x1

    .line 52
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/Alarm;->getTimeCanonical()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 53
    const-string/jumbo v6, "time"

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/Alarm;->getTimeCanonical()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 55
    :cond_1
    const-string/jumbo v7, "set"

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/Alarm;->getAlarmStatus()Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    move-result-object v6

    sget-object v8, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    if-eq v6, v8, :cond_2

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v7, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 58
    const-string/jumbo v6, "days"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/Alarm;->getDayMask()I

    move-result v8

    invoke-static {v8}, Lcom/vlingo/core/internal/util/Alarm;->getDaysCanonical(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 59
    const-string/jumbo v6, "repeat"

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/Alarm;->isWeeklyRepeating()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 60
    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    goto :goto_1

    .line 55
    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 62
    .end local v0    # "alarm":Lcom/vlingo/core/internal/util/Alarm;
    .end local v1    # "alarmBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    :cond_3
    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 63
    invoke-virtual {v3}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v6

    goto/16 :goto_0
.end method
