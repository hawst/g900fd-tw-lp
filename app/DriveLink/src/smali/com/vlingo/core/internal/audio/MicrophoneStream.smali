.class public final Lcom/vlingo/core/internal/audio/MicrophoneStream;
.super Ljava/io/InputStream;
.source "MicrophoneStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;,
        Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;,
        Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
    }
.end annotation


# static fields
.field public static final BLUETOOTH_SAMPLE_RATE:I = 0x1f40

.field private static final CHAN:I = 0x10

.field public static final DEFAULT_AUDIO_FORMAT:I = 0x2

.field public static final DEFAULT_BUFFER_DURATION:I = 0x5

.field public static final DEFAULT_CHANNEL_CONFIG:I = 0x2

.field public static final DEFAULT_SAMPLE_RATE:I = 0x3e80

.field private static final ENCODING:I = 0x2

.field private static final LOCK:Ljava/lang/Object;

.field private static final LOGTAG:Ljava/lang/String;

.field public static final SAMPLE_RATE_16KHZ:I = 0x3e80

.field public static final SAMPLE_RATE_8KHZ:I = 0x1f40

.field private static final START_RECORDING_MAX_RETRY:I = 0x5

.field private static final START_RECORDING_RETRY_DELAY:I = 0xf

.field private static currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

.field public static testStream:Ljava/io/InputStream;


# instance fields
.field private currentState:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

.field private mAudioLogger:Lcom/vlingo/core/internal/audio/AudioLogger;

.field private mAudioSource:I

.field private mBufferedData:[S

.field private mBufferedDataIndex:I

.field private mRecorder:Landroid/media/AudioRecord;

.field private mSampleRate:I

.field private noiseCancelFilter:Lcom/vlingo/core/internal/audio/AudioFilterAdapter;

.field private volatile noiseCancelFilterEnabled:Z

.field private remainingByte:B

.field private remainingByteOffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    .line 50
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    .line 72
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOCK:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;I)V
    .locals 19
    .param p1, "srContext"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .param p2, "type"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
    .param p3, "audioSourceType"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;
    .param p4, "audioSessionId"    # I

    .prologue
    .line 149
    invoke-direct/range {p0 .. p0}, Ljava/io/InputStream;-><init>()V

    .line 150
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->useVoiceRecognitionAudioSource()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v3, 0x6

    .line 152
    .local v3, "audioSource":I
    :goto_0
    const/4 v8, 0x0

    .line 153
    .local v8, "audioSourceUtil":Lcom/vlingo/core/internal/audio/AudioSourceUtil;
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->AudioSourceSelector:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v12

    .line 154
    .local v12, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    if-eqz v12, :cond_0

    .line 156
    :try_start_0
    invoke-virtual {v12}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioSourceUtil;

    move-object v8, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 164
    :cond_0
    :goto_1
    const/4 v5, 0x2

    .line 165
    .local v5, "channelConfig":I
    const/16 v2, 0x3e80

    move-object/from16 v0, p0

    iput v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mSampleRate:I

    .line 166
    if-eqz v8, :cond_1

    .line 167
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-interface {v8, v0, v1}, Lcom/vlingo/core/internal/audio/AudioSourceUtil;->chooseAudioSource(Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)I

    move-result v3

    .line 168
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->getChannelConfig(I)I

    move-result v5

    .line 171
    invoke-interface {v8}, Lcom/vlingo/core/internal/audio/AudioSourceUtil;->chooseMicSampleRate()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mSampleRate:I

    .line 174
    :cond_1
    const/4 v6, 0x2

    .line 175
    .local v6, "audioFormat":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mSampleRate:I

    invoke-static {v2, v5, v6}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v13

    .line 176
    .local v13, "minBufferSize":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mSampleRate:I

    mul-int/lit8 v4, v2, 0x5

    const/4 v2, 0x2

    if-ne v6, v2, :cond_7

    const/4 v2, 0x2

    :goto_2
    mul-int v9, v4, v2

    .line 177
    .local v9, "desiredBufferSize":I
    invoke-static {v13, v9}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 179
    .local v7, "bufferSizeInBytes":I
    invoke-direct/range {p0 .. p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->initializeNoiseCancelFilter(Lcom/vlingo/sdk/recognition/VLRecognitionContext;)V

    .line 182
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    sget-object v4, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->RECOGNITION:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    if-ne v2, v4, :cond_2

    .line 183
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->enableAudioFiltering()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_4

    .line 187
    :cond_2
    const/16 v2, 0xc

    if-ne v2, v5, :cond_3

    .line 188
    mul-int/lit8 v7, v7, 0x2

    .line 190
    :cond_3
    :try_start_2
    new-instance v2, Landroid/media/AudioRecord;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mSampleRate:I

    invoke-direct/range {v2 .. v7}, Landroid/media/AudioRecord;-><init>(IIIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4

    .line 199
    :goto_3
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    if-nez v2, :cond_5

    .line 200
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->useVoiceRecognitionAudioSource()Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v3, 0x6

    .line 201
    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->getChannelConfig(I)I

    move-result v5

    .line 202
    const/16 v2, 0xc

    if-ne v2, v5, :cond_4

    .line 203
    mul-int/lit8 v7, v7, 0x2

    .line 205
    :cond_4
    new-instance v2, Landroid/media/AudioRecord;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mSampleRate:I

    invoke-direct/range {v2 .. v7}, Landroid/media/AudioRecord;-><init>(IIIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    .line 207
    :cond_5
    move-object/from16 v0, p0

    iput v3, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioSource:I

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getState()I

    move-result v2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_9

    .line 211
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->name()Ljava/lang/String;

    move-result-object v15

    .line 212
    .local v15, "name":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "AudioRecord not initialized for AudioSource "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " Owner: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 213
    .local v14, "msg":Ljava/lang/String;
    const/4 v2, 0x0

    sput-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    .line 215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->release()V

    .line 216
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    .line 218
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    invoke-static {v2, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v14}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4

    .line 246
    .end local v14    # "msg":Ljava/lang/String;
    .end local v15    # "name":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 247
    .local v10, "e":Ljava/lang/InterruptedException;
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    invoke-static {v10}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    .end local v10    # "e":Ljava/lang/InterruptedException;
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v2

    const/4 v4, 0x3

    if-eq v2, v4, :cond_e

    .line 259
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->name()Ljava/lang/String;

    move-result-object v15

    .line 260
    .restart local v15    # "name":Ljava/lang/String;
    const/4 v2, 0x0

    sput-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->release()V

    .line 263
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    .line 265
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "MicrophoneStream startRecording failed.  Owner: "

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 150
    .end local v3    # "audioSource":I
    .end local v5    # "channelConfig":I
    .end local v6    # "audioFormat":I
    .end local v7    # "bufferSizeInBytes":I
    .end local v8    # "audioSourceUtil":Lcom/vlingo/core/internal/audio/AudioSourceUtil;
    .end local v9    # "desiredBufferSize":I
    .end local v12    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v13    # "minBufferSize":I
    .end local v15    # "name":Ljava/lang/String;
    :cond_6
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 157
    .restart local v3    # "audioSource":I
    .restart local v8    # "audioSourceUtil":Lcom/vlingo/core/internal/audio/AudioSourceUtil;
    .restart local v12    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    :catch_1
    move-exception v10

    .line 158
    .local v10, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v10}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_1

    .line 159
    .end local v10    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v10

    .line 160
    .local v10, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v10}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_1

    .line 176
    .end local v10    # "e":Ljava/lang/IllegalAccessException;
    .restart local v5    # "channelConfig":I
    .restart local v6    # "audioFormat":I
    .restart local v13    # "minBufferSize":I
    :cond_7
    const/4 v2, 0x1

    goto/16 :goto_2

    .line 191
    .restart local v7    # "bufferSizeInBytes":I
    .restart local v9    # "desiredBufferSize":I
    :catch_3
    move-exception v11

    .line 192
    .local v11, "ex":Ljava/lang/IllegalArgumentException;
    :try_start_4
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    invoke-static {v11}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_3

    .line 248
    .end local v11    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v10

    .line 249
    .local v10, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    invoke-static {v10}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "MicrophoneStream Create error."

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 200
    .end local v10    # "e":Ljava/lang/Throwable;
    :cond_8
    const/4 v3, 0x1

    goto/16 :goto_4

    .line 221
    :cond_9
    :try_start_5
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "AudioRecord initialized for AudioSource "

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v17, ", sample rate :"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mSampleRate:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    if-eqz v8, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    if-eqz v2, :cond_a

    .line 223
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getAudioSource()I

    move-result v2

    invoke-interface {v8, v2}, Lcom/vlingo/core/internal/audio/AudioSourceUtil;->notifyAudioSourceSet(I)V

    .line 226
    :cond_a
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->lockFileCreateIfNotExist()Z
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4

    .line 228
    const/16 v16, 0x5

    .line 230
    .local v16, "nbrTriesLeft":I
    :cond_b
    if-eqz p4, :cond_c

    .line 231
    :try_start_6
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->startRecordingJB(I)V

    .line 237
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getRecordingState()I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v2

    const/4 v4, 0x3

    if-ne v2, v4, :cond_d

    .line 244
    :goto_7
    :try_start_7
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->lockFileDeleteIfExists()Z
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_5

    .line 233
    :cond_c
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->startRecording()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_6

    .line 244
    :catchall_0
    move-exception v2

    :try_start_9
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->lockFileDeleteIfExists()Z

    throw v2
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4

    .line 240
    :cond_d
    :try_start_a
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    const-string/jumbo v4, "MicrophoneStream start recording failed. retry ..."

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const-wide/16 v17, 0xf

    invoke-static/range {v17 .. v18}, Ljava/lang/Thread;->sleep(J)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 242
    add-int/lit8 v16, v16, -0x1

    if-gtz v16, :cond_b

    goto :goto_7

    .line 268
    .end local v16    # "nbrTriesLeft":I
    :cond_e
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->STARTED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentState:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    .line 269
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Landroid/media/AudioRecord;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/core/internal/audio/MicrophoneStream;Landroid/media/AudioRecord;)Landroid/media/AudioRecord;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .param p1, "x1"    # Landroid/media/AudioRecord;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentState:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    return-object v0
.end method

.method static synthetic access$302(Lcom/vlingo/core/internal/audio/MicrophoneStream;Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;)Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .param p1, "x1"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentState:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    return-object p1
.end method

.method static synthetic access$402(Lcom/vlingo/core/internal/audio/MicrophoneStream;[S)[S
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .param p1, "x1"    # [S

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedData:[S

    return-object p1
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/AudioLogger;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioLogger:Lcom/vlingo/core/internal/audio/AudioLogger;

    return-object v0
.end method

.method static synthetic access$502(Lcom/vlingo/core/internal/audio/MicrophoneStream;Lcom/vlingo/core/internal/audio/AudioLogger;)Lcom/vlingo/core/internal/audio/AudioLogger;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .param p1, "x1"    # Lcom/vlingo/core/internal/audio/AudioLogger;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioLogger:Lcom/vlingo/core/internal/audio/AudioLogger;

    return-object p1
.end method

.method static synthetic access$602(Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;)Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    .prologue
    .line 30
    sput-object p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    return-object p0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/AudioFilterAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->noiseCancelFilter:Lcom/vlingo/core/internal/audio/AudioFilterAdapter;

    return-object v0
.end method

.method private getChannelConfig(I)I
    .locals 6
    .param p1, "audioSource"    # I

    .prologue
    .line 113
    const/4 v2, 0x2

    .line 115
    .local v2, "channelConfig":I
    const/4 v1, 0x0

    .line 116
    .local v1, "audioSourceUtil":Lcom/vlingo/core/internal/audio/AudioSourceUtil;
    sget-object v5, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->AudioSourceSelector:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v5}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v4

    .line 117
    .local v4, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    if-eqz v4, :cond_0

    .line 119
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioSourceUtil;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 127
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 128
    invoke-interface {v1, p1}, Lcom/vlingo/core/internal/audio/AudioSourceUtil;->chooseChannelConfig(I)I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 138
    :cond_1
    :goto_1
    return v2

    .line 120
    :catch_0
    move-exception v3

    .line 121
    .local v3, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v3}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 122
    .end local v3    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v3

    .line 123
    .local v3, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 131
    .end local v3    # "e":Ljava/lang/IllegalAccessException;
    :sswitch_0
    const/16 v2, 0xc

    .line 132
    goto :goto_1

    .line 128
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xc -> :sswitch_0
    .end sparse-switch
.end method

.method private static getLockFile()Ljava/io/File;
    .locals 3

    .prologue
    .line 534
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "mic.lock.file"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private initializeNoiseCancelFilter(Lcom/vlingo/sdk/recognition/VLRecognitionContext;)V
    .locals 6
    .param p1, "srContext"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    .prologue
    .line 296
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->NoiseCancel:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v1

    .line 297
    .local v1, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    if-eqz v1, :cond_0

    .line 299
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/audio/AudioFilterAdapter;

    iput-object v2, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->noiseCancelFilter:Lcom/vlingo/core/internal/audio/AudioFilterAdapter;

    .line 300
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->noiseCancelFilter:Lcom/vlingo/core/internal/audio/AudioFilterAdapter;

    iget v5, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mSampleRate:I

    if-nez p1, :cond_1

    sget-object v2, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_SPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v2

    move v3, v2

    :goto_0
    if-nez p1, :cond_2

    sget-object v2, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_NOSPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v2

    :goto_1
    invoke-interface {v4, v5, v3, v2}, Lcom/vlingo/core/internal/audio/AudioFilterAdapter;->init(III)V

    .line 309
    :cond_0
    :goto_2
    return-void

    .line 300
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getSpeechEndpointTimeout()I

    move-result v2

    move v3, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getNoSpeechEndPointTimeout()I
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    goto :goto_1

    .line 303
    :catch_0
    move-exception v0

    .line 304
    .local v0, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_2

    .line 305
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v0

    .line 306
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2
.end method

.method private static declared-synchronized lockFileCreateIfNotExist()Z
    .locals 6

    .prologue
    .line 511
    const-class v3, Lcom/vlingo/core/internal/audio/MicrophoneStream;

    monitor-enter v3

    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->getLockFile()Ljava/io/File;

    move-result-object v1

    .line 512
    .local v1, "lockFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 516
    :goto_0
    monitor-exit v3

    return v2

    .line 513
    :catch_0
    move-exception v0

    .line 514
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Error creating file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 516
    const/4 v2, 0x0

    goto :goto_0

    .line 511
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized lockFileDeleteIfExists()Z
    .locals 7

    .prologue
    .line 521
    const-class v4, Lcom/vlingo/core/internal/audio/MicrophoneStream;

    monitor-enter v4

    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->getLockFile()Ljava/io/File;

    move-result-object v2

    .line 522
    .local v2, "lockFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    .line 523
    .local v1, "existed":Z
    if-eqz v1, :cond_0

    .line 524
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529
    .end local v1    # "existed":Z
    :cond_0
    :goto_0
    monitor-exit v4

    return v1

    .line 527
    :catch_0
    move-exception v0

    .line 528
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Error deleting file"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529
    const/4 v1, 0x0

    goto :goto_0

    .line 521
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static request(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;)Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .locals 1
    .param p0, "srContext"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .param p1, "type"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->request(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;I)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v0

    return-object v0
.end method

.method public static request(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;I)Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .locals 1
    .param p0, "srContext"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .param p1, "type"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
    .param p2, "audioSessionId"    # I

    .prologue
    .line 83
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->UNSPECIFIED:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    invoke-static {p0, p1, v0, p2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->request(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;I)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v0

    return-object v0
.end method

.method public static request(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;I)Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .locals 6
    .param p0, "srContext"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .param p1, "type"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
    .param p2, "audioSourceType"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;
    .param p3, "audioSessionId"    # I

    .prologue
    .line 87
    sget-object v3, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 90
    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "MicrophoneStream requested for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " audioSessionId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    if-eqz v2, :cond_0

    .line 92
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "MicrophoneStream already in use. Requestor: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", Owner: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 105
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 96
    :cond_0
    :try_start_1
    sput-object p1, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    .line 97
    new-instance v1, Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/vlingo/core/internal/audio/MicrophoneStream;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    .local v1, "ms":Lcom/vlingo/core/internal/audio/MicrophoneStream;
    :try_start_2
    monitor-exit v3

    return-object v1

    .line 98
    .end local v1    # "ms":Lcom/vlingo/core/internal/audio/MicrophoneStream;
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    sput-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    .line 100
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "MicrophoneStream creation failed!!!"

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private static useVoiceRecognitionAudioSource()Z
    .locals 1

    .prologue
    .line 109
    invoke-static {}, Lcom/vlingo/core/internal/util/DeviceWorkarounds;->useVoiceRecognitionAudioPath()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 469
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;-><init>(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    const-string/jumbo v2, "MicrophoneStream.close()"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500
    monitor-exit p0

    return-void

    .line 469
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized disableAudioFiltering()V
    .locals 2

    .prologue
    .line 391
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    const-string/jumbo v1, "Disable audio filtering"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->noiseCancelFilterEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 393
    monitor-exit p0

    return-void

    .line 391
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized enableAudioFiltering()V
    .locals 2

    .prologue
    .line 386
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    const-string/jumbo v1, "Enable audio filtering"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->noiseCancelFilterEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    monitor-exit p0

    return-void

    .line 386
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getChannelConfig()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioSource:I

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->getChannelConfig(I)I

    move-result v0

    return v0
.end method

.method public getRecorder()Landroid/media/AudioRecord;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    return-object v0
.end method

.method public is16KHz()Z
    .locals 2

    .prologue
    .line 326
    iget v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mSampleRate:I

    const/16 v1, 0x3e80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public is8KHz()Z
    .locals 2

    .prologue
    .line 330
    iget v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mSampleRate:I

    const/16 v1, 0x1f40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized isRecording()Z
    .locals 2

    .prologue
    .line 503
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentState:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    sget-object v1, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->STARTED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getRecordingState()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 402
    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    new-array v0, v2, [B

    .line 403
    .local v0, "ba":[B
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->read([BII)I

    move-result v1

    .line 404
    .local v1, "bytesRead":I
    if-lez v1, :cond_0

    .line 405
    const/4 v2, 0x0

    aget-byte v2, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407
    :goto_0
    monitor-exit p0

    return v2

    :cond_0
    const/4 v2, -0x1

    goto :goto_0

    .line 402
    .end local v0    # "ba":[B
    .end local v1    # "bytesRead":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized read([B)I
    .locals 1
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 397
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read([BII)I
    .locals 11
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 417
    monitor-enter p0

    add-int/lit8 v9, p3, 0x1

    :try_start_0
    div-int/lit8 v9, v9, 0x2

    iget v10, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->remainingByteOffset:I

    mul-int/2addr v10, p3

    rem-int/lit8 v10, v10, 0x2

    sub-int v8, v9, v10

    .line 418
    .local v8, "shortsToRead":I
    move v0, p3

    .line 420
    .local v0, "bytesToRead":I
    new-array v6, v8, [S

    .line 421
    .local v6, "shortBuffer":[S
    const/4 v9, 0x0

    invoke-virtual {p0, v6, v9, v8}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->read([SII)I

    move-result v4

    .line 423
    .local v4, "readShorts":I
    iget v9, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->remainingByteOffset:I

    if-lez v9, :cond_0

    .line 424
    iget-byte v9, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->remainingByte:B

    aput-byte v9, p1, p2

    .line 425
    add-int/lit8 p2, p2, 0x1

    .line 426
    add-int/lit8 v0, v0, -0x1

    .line 429
    :cond_0
    const/4 v2, 0x0

    .line 430
    .local v2, "hasRemainingByte":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_2

    .line 431
    aget-short v7, v6, v3

    .line 432
    .local v7, "shortValue":S
    and-int/lit16 v9, v7, 0xff

    int-to-byte v9, v9

    aput-byte v9, p1, p2

    .line 433
    add-int/lit8 p2, p2, 0x1

    .line 434
    add-int/lit8 v0, v0, -0x1

    .line 436
    shr-int/lit8 v9, v7, 0x8

    and-int/lit16 v9, v9, 0xff

    int-to-byte v5, v9

    .line 438
    .local v5, "secondByte":B
    if-lez v0, :cond_1

    .line 439
    aput-byte v5, p1, p2

    .line 440
    add-int/lit8 p2, p2, 0x1

    .line 441
    add-int/lit8 v0, v0, -0x1

    .line 430
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 443
    :cond_1
    iput-byte v5, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->remainingByte:B

    .line 444
    const/4 v2, 0x1

    goto :goto_1

    .line 448
    .end local v5    # "secondByte":B
    .end local v7    # "shortValue":S
    :cond_2
    const/4 v9, -0x1

    if-ne v4, v9, :cond_4

    .line 449
    iget v9, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->remainingByteOffset:I

    if-lez v9, :cond_3

    .line 450
    const/4 v9, 0x0

    iput v9, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->remainingByteOffset:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 451
    const/4 v9, 0x1

    .line 459
    :goto_2
    monitor-exit p0

    return v9

    .line 453
    :cond_3
    const/4 v9, -0x1

    goto :goto_2

    .line 457
    :cond_4
    mul-int/lit8 v9, v4, 0x2

    :try_start_1
    iget v10, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->remainingByteOffset:I

    add-int v1, v9, v10

    .line 458
    .local v1, "bytesToReturn":I
    if-eqz v2, :cond_5

    const/4 v9, 0x1

    :goto_3
    iput v9, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->remainingByteOffset:I

    .line 459
    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v9

    goto :goto_2

    .line 458
    :cond_5
    const/4 v9, 0x0

    goto :goto_3

    .line 417
    .end local v0    # "bytesToRead":I
    .end local v1    # "bytesToReturn":I
    .end local v2    # "hasRemainingByte":Z
    .end local v3    # "i":I
    .end local v4    # "readShorts":I
    .end local v6    # "shortBuffer":[S
    .end local v8    # "shortsToRead":I
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9
.end method

.method public declared-synchronized read([SII)I
    .locals 7
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I

    .prologue
    .line 334
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOCK:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    :try_start_1
    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentState:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    sget-object v5, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->STARTED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    if-eq v3, v5, :cond_1

    .line 336
    sget-object v3, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "read while in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentState:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " state."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    const/4 v2, -0x1

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 382
    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    .line 339
    :cond_1
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 340
    const/4 v2, 0x0

    .line 341
    .local v2, "shortsRead":I
    const/4 v1, 0x0

    .line 342
    .local v1, "logData":Z
    :try_start_3
    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedData:[S

    if-eqz v3, :cond_6

    .line 343
    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedData:[S

    array-length v3, v3

    iget v4, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedDataIndex:I

    sub-int v0, v3, v4

    .line 344
    .local v0, "bufferedShorts":I
    if-le p3, v0, :cond_2

    .line 345
    move p3, v0

    .line 347
    :cond_2
    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedData:[S

    iget v4, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedDataIndex:I

    invoke-static {v3, v4, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 348
    iget v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedDataIndex:I

    add-int/2addr v3, p3

    iput v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedDataIndex:I

    .line 349
    iget v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedDataIndex:I

    iget-object v4, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedData:[S

    array-length v4, v4

    if-lt v3, v4, :cond_3

    .line 350
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedData:[S

    .line 352
    :cond_3
    move v2, p3

    .line 361
    .end local v0    # "bufferedShorts":I
    :cond_4
    :goto_1
    if-lez v2, :cond_0

    .line 362
    iget-boolean v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->noiseCancelFilterEnabled:Z

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->is16KHz()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 363
    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->noiseCancelFilter:Lcom/vlingo/core/internal/audio/AudioFilterAdapter;

    if-eqz v3, :cond_5

    .line 364
    sget-object v3, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    const-string/jumbo v4, "Filtering audio data"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->noiseCancelFilter:Lcom/vlingo/core/internal/audio/AudioFilterAdapter;

    iget v4, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioSource:I

    invoke-interface {v3, p1, p2, v2, v4}, Lcom/vlingo/core/internal/audio/AudioFilterAdapter;->filter([SIII)I

    move-result v2

    .line 377
    :cond_5
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioLogger:Lcom/vlingo/core/internal/audio/AudioLogger;

    if-eqz v3, :cond_0

    .line 378
    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioLogger:Lcom/vlingo/core/internal/audio/AudioLogger;

    invoke-interface {v3, p1, p2, v2}, Lcom/vlingo/core/internal/audio/AudioLogger;->writeData([SII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 334
    .end local v1    # "logData":Z
    .end local v2    # "shortsRead":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 339
    :catchall_1
    move-exception v3

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v3

    .line 354
    .restart local v1    # "logData":Z
    .restart local v2    # "shortsRead":I
    :cond_6
    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v3, p1, p2, p3}, Landroid/media/AudioRecord;->read([SII)I

    move-result v2

    .line 355
    const/4 v1, 0x1

    .line 356
    if-gez v2, :cond_4

    .line 357
    sget-object v3, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Error returned from mRecorder.read: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public setAudioLogger(Lcom/vlingo/core/internal/audio/AudioLogger;)V
    .locals 0
    .param p1, "logger"    # Lcom/vlingo/core/internal/audio/AudioLogger;

    .prologue
    .line 322
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioLogger:Lcom/vlingo/core/internal/audio/AudioLogger;

    .line 323
    return-void
.end method

.method public setBufferedData([SI)V
    .locals 4
    .param p1, "bufferedData"    # [S
    .param p2, "length"    # I

    .prologue
    const/4 v3, 0x0

    .line 312
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setBufferedData() length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    array-length v0, p1

    if-ltz v0, :cond_0

    .line 316
    new-array v0, p2, [S

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedData:[S

    .line 317
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedData:[S

    invoke-static {p1, v3, v0, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 319
    :cond_0
    return-void
.end method

.method public startRecordingJB(I)V
    .locals 0
    .param p1, "audioSessionId"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 292
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->startRecordingJBOld(I)V

    .line 293
    return-void
.end method

.method public startRecordingJBNew(I)V
    .locals 2
    .param p1, "audioSessionId"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 280
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/core/internal/audio/MicrophoneStream$1;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream$1;-><init>(Lcom/vlingo/core/internal/audio/MicrophoneStream;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 289
    return-void
.end method

.method public startRecordingJBOld(I)V
    .locals 2
    .param p1, "audioSessionId"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 273
    const/4 v1, 0x1

    invoke-static {v1}, Landroid/media/MediaSyncEvent;->createEvent(I)Landroid/media/MediaSyncEvent;

    move-result-object v0

    .line 274
    .local v0, "mse":Landroid/media/MediaSyncEvent;
    invoke-virtual {v0, p1}, Landroid/media/MediaSyncEvent;->setAudioSessionId(I)Landroid/media/MediaSyncEvent;

    .line 275
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v1, v0}, Landroid/media/AudioRecord;->startRecording(Landroid/media/MediaSyncEvent;)V

    .line 276
    return-void
.end method
