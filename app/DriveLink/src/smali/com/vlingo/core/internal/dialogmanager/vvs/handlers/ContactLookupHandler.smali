.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ContactLookupHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;
    }
.end annotation


# static fields
.field private static final ALL_ADDRESS_TYPES:[I

.field private static final ALL_EMAIL_TYPES:[I

.field private static final ALL_PHONE_TYPES:[I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private addressTypes:[I

.field protected decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

.field protected displayedContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private emailTypes:[I

.field protected name:Ljava/lang/String;

.field private ordinal:Ljava/lang/String;

.field protected phoneType:Ljava/lang/String;

.field private phoneTypes:[I

.field protected query:Ljava/lang/String;

.field private type:Lcom/vlingo/core/internal/contacts/ContactType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 50
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->TAG:Ljava/lang/String;

    .line 61
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ALL_PHONE_TYPES:[I

    .line 62
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ALL_EMAIL_TYPES:[I

    .line 63
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ALL_ADDRESS_TYPES:[I

    return-void

    .line 61
    nop

    :array_0
    .array-data 4
        0x3
        0x2
        0x1
        0x7
    .end array-data

    .line 62
    :array_1
    .array-data 4
        0x2
        0x0
        0x1
        0x3
    .end array-data

    .line 63
    :array_2
    .array-data 4
        0x2
        0x0
        0x1
        0x3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 53
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 54
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    .line 55
    const-string/jumbo v0, "call"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->phoneType:Ljava/lang/String;

    .line 64
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ALL_PHONE_TYPES:[I

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->phoneTypes:[I

    .line 65
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ALL_EMAIL_TYPES:[I

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->emailTypes:[I

    .line 66
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ALL_ADDRESS_TYPES:[I

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->addressTypes:[I

    .line 356
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getPhoneType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->additionalMatchContacts()V

    return-void
.end method

.method static synthetic access$1400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)[I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->emailTypes:[I

    return-object v0
.end method

.method static synthetic access$1500(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)[I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getPhoneTypes()[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method private additionalMatchContacts()V
    .locals 12

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    .line 242
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v11, v0

    check-cast v11, Ljava/util/List;

    .line 243
    .local v11, "disambiguationList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatcher;

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)V

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getPhoneTypes()[I

    move-result-object v4

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getEmailTypes()[I

    move-result-object v5

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getSocialTypes()[I

    move-result-object v6

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getAddressTypes()[I

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    const/high16 v9, 0x42c80000    # 100.0f

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatcher;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V

    .line 311
    return-void
.end method

.method private getAddressTypes()[I
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->addressTypes:[I

    return-object v0
.end method

.method private getEmailTypes()[I
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->emailTypes:[I

    return-object v0
.end method

.method private getPhoneType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Ljava/lang/String;
    .locals 1
    .param p1, "mWidgetDecor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .prologue
    .line 343
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->phoneType:Ljava/lang/String;

    .line 353
    :goto_0
    return-object v0

    .line 344
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getTypeStringEN(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 347
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowMobilePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 348
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getTypeStringEN(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 350
    :cond_2
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 351
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getTypeStringEN(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 353
    :cond_3
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->phoneType:Ljava/lang/String;

    goto :goto_0
.end method

.method private getPhoneTypes()[I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->phoneTypes:[I

    return-object v0
.end method

.method private getSocialTypes()[I
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return-object v0
.end method

.method private matchContacts(Lcom/vlingo/core/internal/contacts/ContactType;)V
    .locals 12
    .param p1, "contactType"    # Lcom/vlingo/core/internal/contacts/ContactType;

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    .line 227
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v11, v0

    check-cast v11, Ljava/util/List;

    .line 228
    .local v11, "disambiguationList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatcher;

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;)V

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getPhoneTypes()[I

    move-result-object v4

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getEmailTypes()[I

    move-result-object v5

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getSocialTypes()[I

    move-result-object v6

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getAddressTypes()[I

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    const/high16 v9, 0x42c80000    # 100.0f

    const/4 v10, 0x0

    move-object v3, p1

    invoke-direct/range {v0 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatcher;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V

    .line 230
    return-void
.end method

.method private showSystemTurn(Ljava/lang/String;)V
    .locals 3
    .param p1, "prompt"    # Ljava/lang/String;

    .prologue
    .line 429
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v1}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 431
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_QUERY:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 432
    return-void
.end method


# virtual methods
.method protected checkWrongDateOfBirthday(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 9
    .param p1, "match"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v1, 0x0

    .line 482
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBirthdayData()Ljava/util/List;

    move-result-object v6

    .line 483
    .local v6, "birthdayData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 484
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 485
    .local v7, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v3, v7, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 487
    .local v3, "data":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v0, "-"

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 488
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/vlingo/core/internal/schedule/DateUtil;->getDayAndMonthFromString(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 494
    :goto_0
    const/4 v8, 0x0

    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v1, v7, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v2, v7, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    iget v4, v7, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    iget v5, v7, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    invoke-interface {v6, v8, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 499
    .end local v3    # "data":Ljava/lang/String;
    .end local v7    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_0
    :goto_1
    return-void

    .line 491
    .restart local v3    # "data":Ljava/lang/String;
    .restart local v7    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_1
    invoke-static {v3}, Lcom/vlingo/core/internal/schedule/DateUtil;->getLongDateStringFromPhonePreferencesString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 495
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 11
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 95
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 97
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v4

    const-string/jumbo v7, "contact"

    invoke-virtual {v4, v7}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 99
    const-string/jumbo v4, "Name"

    invoke-static {p1, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->isParamSpecified(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 100
    const-string/jumbo v4, "Name"

    invoke-static {p1, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->removePossessives(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_NAME:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    invoke-interface {v4, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 106
    :goto_0
    const-string/jumbo v4, "Query"

    invoke-static {p1, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    .line 107
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 108
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_QUERY:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    .line 112
    :goto_1
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    .line 113
    .local v1, "mName":Ljava/lang/String;
    :goto_2
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    if-eqz v4, :cond_4

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    .line 114
    .local v2, "mType":Ljava/lang/String;
    :goto_3
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "[ActionQuery] Contact Action : Query = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", type = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const-string/jumbo v4, "Which"

    invoke-static {p1, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ordinal:Ljava/lang/String;

    .line 118
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 119
    iput-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 121
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ALL_PHONE_TYPES:[I

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->phoneTypes:[I

    .line 122
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ALL_EMAIL_TYPES:[I

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->emailTypes:[I

    .line 123
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ALL_ADDRESS_TYPES:[I

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->addressTypes:[I

    .line 124
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 125
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "email"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 126
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 127
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowEmail()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 175
    :cond_0
    :goto_4
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ordinal:Ljava/lang/String;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 178
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ordinal:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElement(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_f

    .line 179
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v0

    .line 180
    .local v0, "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 181
    .local v3, "pageContactInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ordinal:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->openContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 182
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .end local v0    # "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v3    # "pageContactInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :goto_5
    move v4, v5

    .line 204
    :goto_6
    return v4

    .line 103
    .end local v1    # "mName":Ljava/lang/String;
    .end local v2    # "mType":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_NAME:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 110
    :cond_2
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_QUERY:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    invoke-interface {p2, v4, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 112
    :cond_3
    const-string/jumbo v1, "null"

    goto/16 :goto_2

    .line 113
    .restart local v1    # "mName":Ljava/lang/String;
    :cond_4
    const-string/jumbo v2, "null"

    goto/16 :goto_3

    .line 128
    .restart local v2    # "mType":Ljava/lang/String;
    :cond_5
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "homeemail"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 129
    new-array v4, v5, [I

    aput v5, v4, v6

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->emailTypes:[I

    .line 130
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 131
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowEmail()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 132
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->constactShowHomeEmailOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    goto :goto_4

    .line 133
    :cond_6
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "workemail"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 134
    new-array v4, v5, [I

    aput v9, v4, v6

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->emailTypes:[I

    .line 135
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 136
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowEmail()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 137
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->constactShowWorkEmailOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    goto/16 :goto_4

    .line 138
    :cond_7
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "pn"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 139
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 140
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowPhone()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    goto/16 :goto_4

    .line 141
    :cond_8
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "wpn"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 142
    new-array v4, v5, [I

    const/4 v7, 0x3

    aput v7, v4, v6

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->phoneTypes:[I

    .line 143
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 144
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowPhone()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 145
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->constactShowWorkPhoneOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    goto/16 :goto_4

    .line 146
    :cond_9
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "hpn"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 147
    new-array v4, v5, [I

    aput v5, v4, v6

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->phoneTypes:[I

    .line 148
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 149
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowPhone()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 150
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->constactShowHomePhoneOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    goto/16 :goto_4

    .line 151
    :cond_a
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "mpn"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 152
    new-array v4, v5, [I

    aput v9, v4, v6

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->phoneTypes:[I

    .line 153
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 154
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowPhone()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 155
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->constactShowMobilePhoneOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    goto/16 :goto_4

    .line 156
    :cond_b
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "address"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 157
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 158
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowAddress()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    goto/16 :goto_4

    .line 159
    :cond_c
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "homeaddress"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 160
    new-array v4, v5, [I

    aput v5, v4, v6

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->addressTypes:[I

    .line 161
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 162
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowAddress()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 163
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->constactShowHomeAddressOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    goto/16 :goto_4

    .line 164
    :cond_d
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "workaddress"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 165
    new-array v4, v5, [I

    aput v9, v4, v6

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->addressTypes:[I

    .line 166
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 167
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowAddress()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 168
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->constactShowWorkAddressOnly()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    goto/16 :goto_4

    .line 169
    :cond_e
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "birthday"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 170
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 171
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowBirthday()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    goto/16 :goto_4

    .line 184
    :cond_f
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v4, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->displayedContacts:Ljava/util/List;

    .line 185
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v4, v6, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 186
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->displayedContacts:Ljava/util/List;

    if-eqz v4, :cond_10

    .line 187
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->displayedContacts:Ljava/util/List;

    invoke-static {v4, v6}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    .line 188
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->displayedContacts:Ljava/util/List;

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->multipleContacts(Ljava/util/List;)V

    .line 192
    :goto_7
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    goto/16 :goto_5

    .line 190
    :cond_10
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->ordinal:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getContactNotFoundString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V

    goto :goto_7

    .line 195
    :cond_11
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 198
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getWhichContactString()Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP_CHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v7}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    move v4, v6

    .line 199
    goto/16 :goto_6

    .line 203
    :cond_12
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->matchContacts(Lcom/vlingo/core/internal/contacts/ContactType;)V

    move v4, v5

    .line 204
    goto/16 :goto_6
.end method

.method protected getAddressNotFoundString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 519
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_address_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getBirthdayNotFoundString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 515
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_birthday_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContactData(Lcom/vlingo/core/internal/contacts/ContactMatch;)Ljava/lang/String;
    .locals 9
    .param p1, "match"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v8, 0x0

    .line 444
    const-string/jumbo v3, ""

    .line 445
    .local v3, "data":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "email"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 447
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getEmailData()Ljava/util/List;

    move-result-object v4

    .line 448
    .local v4, "emailData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 449
    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 450
    .local v2, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v3, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 477
    .end local v2    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v4    # "emailData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_0
    :goto_0
    return-object v3

    .line 453
    :cond_1
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "pn"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 455
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v5

    .line 456
    .local v5, "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v5, :cond_0

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 457
    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 458
    .restart local v2    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v3, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    goto :goto_0

    .line 461
    .end local v2    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v5    # "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_2
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "address"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 463
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAddressData()Ljava/util/List;

    move-result-object v0

    .line 464
    .local v0, "addressData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 465
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 466
    .restart local v2    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v3, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    goto :goto_0

    .line 469
    .end local v0    # "addressData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v2    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_3
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v7, "birthday"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 471
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBirthdayData()Ljava/util/List;

    move-result-object v1

    .line 472
    .local v1, "birthdayData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 473
    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 474
    .restart local v2    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v3, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    goto :goto_0
.end method

.method protected getContactNotFoundString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "queryString"    # Ljava/lang/String;

    .prologue
    .line 511
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_no_match_openquote:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getEmailNotFoundString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 507
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_email_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getMultipleContactsString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 531
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_contacts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getPhoneNotFoundString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "mPhoneType"    # Ljava/lang/String;

    .prologue
    .line 523
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_phone_number_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSingleContactString(Lcom/vlingo/core/internal/contacts/ContactMatch;)Ljava/lang/String;
    .locals 4
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 527
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_single_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getWhichContactString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 502
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_WHICH_CONTACT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.ContactChoice"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 77
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 78
    .local v1, "extras":Landroid/os/Bundle;
    const-string/jumbo v3, "choice"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 79
    .local v2, "index":I
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 80
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->displayedContacts:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 81
    .local v0, "contactSelected":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->openContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 87
    .end local v0    # "contactSelected":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v1    # "extras":Landroid/os/Bundle;
    .end local v2    # "index":I
    .end local p2    # "object":Ljava/lang/Object;
    :goto_0
    return-void

    .line 82
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    if-eqz v3, :cond_2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.DataTransfered"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 83
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    instance-of v3, p2, Ljava/lang/Integer;

    if-eqz v3, :cond_1

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_1
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v4, v3}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;I)V

    goto :goto_0

    .restart local p2    # "object":Ljava/lang/Object;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected multipleContacts(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 436
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v1, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 437
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getMultipleContactsString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP_CHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 438
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->displayedContacts:Ljava/util/List;

    .line 439
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddressBook:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 440
    return-void
.end method

.method protected openContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 6
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v5, 0x0

    .line 314
    const/4 v2, 0x0

    .line 315
    .local v2, "wdecor":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->checkWrongDateOfBirthday(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 316
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 317
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getContactData(Lcom/vlingo/core/internal/contacts/ContactMatch;)Ljava/lang/String;

    move-result-object v1

    .line 318
    .local v1, "data":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 319
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v4, "pn"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 320
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 334
    .end local v1    # "data":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ContactDetail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v3, v4, v2, p1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 335
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v4}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 336
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SELECTED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 337
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_QUERY:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 338
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 339
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_NAME:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 340
    return-void

    .line 322
    .restart local v1    # "data":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 326
    :cond_1
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getSingleContactString(Lcom/vlingo/core/internal/contacts/ContactMatch;)Ljava/lang/String;

    move-result-object v0

    .line 327
    .local v0, "contactFound":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3, v0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 330
    .end local v0    # "contactFound":Ljava/lang/String;
    .end local v1    # "data":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getSingleContactString(Lcom/vlingo/core/internal/contacts/ContactMatch;)Ljava/lang/String;

    move-result-object v0

    .line 331
    .restart local v0    # "contactFound":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3, v0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 71
    return-void
.end method
