.class public Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;
.super Ljava/lang/Object;
.source "GrammarActionIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
        ">;"
    }
.end annotation


# instance fields
.field private addedGrammarActions:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
            ">;"
        }
    .end annotation
.end field

.field private allFromScratchGrammarActions:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
            ">;"
        }
    .end annotation
.end field

.field private final contentFullIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+",
            "Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;",
            ">;"
        }
    .end annotation
.end field

.field private modifiedGrammarActions:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
            ">;"
        }
    .end annotation
.end field

.field private next:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

.field private removedGrammarActions:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadedChecksums:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+",
            "Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "contentFullIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;>;"
    .local p2, "uploadedChecksums":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->next:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->removedGrammarActions:Ljava/util/Collection;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->modifiedGrammarActions:Ljava/util/Collection;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->addedGrammarActions:Ljava/util/Collection;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->allFromScratchGrammarActions:Ljava/util/Collection;

    .line 31
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    .line 32
    if-eqz p2, :cond_0

    .end local p2    # "uploadedChecksums":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :goto_0
    iput-object p2, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->uploadedChecksums:Ljava/util/Map;

    .line 33
    return-void

    .line 32
    .restart local p2    # "uploadedChecksums":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :cond_0
    new-instance p2, Ljava/util/HashMap;

    .end local p2    # "uploadedChecksums":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;"
    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public getAddedGrammarActions()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "GrammarActionIterator not iterated yet to provide added grammar actions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->addedGrammarActions:Ljava/util/Collection;

    return-object v0
.end method

.method public getAllFromScratchGrammarActions()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "GrammarActionIterator not iterated yet to provide all grammar actions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->allFromScratchGrammarActions:Ljava/util/Collection;

    return-object v0
.end method

.method public getContentFullIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<+",
            "Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    return-object v0
.end method

.method public getModifiedGrammarActions()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "GrammarActionIterator not iterated yet to provide modified grammar actions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->modifiedGrammarActions:Ljava/util/Collection;

    return-object v0
.end method

.method public getRemovedGrammarActions()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "GrammarActionIterator not iterated yet to provide removed grammar actions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->removedGrammarActions:Ljava/util/Collection;

    return-object v0
.end method

.method public getUploadedChecksums()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->uploadedChecksums:Ljava/util/Map;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->next:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    if-nez v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->next()Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->next:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->next:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 48
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->next:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    if-eqz v6, :cond_1

    .line 49
    iget-object v5, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->next:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    .line 50
    .local v5, "result":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    iput-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->next:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    .line 84
    .end local v5    # "result":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    :goto_0
    return-object v5

    .line 65
    .local v0, "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    :cond_0
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->uploadedChecksums:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    .end local v0    # "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    :cond_1
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 55
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->contentFullIterator:Ljava/util/Iterator;

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    .line 56
    .restart local v0    # "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    if-eqz v0, :cond_1

    .line 57
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->allFromScratchGrammarActions:Ljava/util/Collection;

    new-instance v7, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    invoke-direct {v7, v0, v9}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;-><init>(Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;Z)V

    invoke-interface {v6, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->uploadedChecksums:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 59
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->uploadedChecksums:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getChecksum()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 60
    new-instance v3, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->uploadedChecksums:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {v3, v0, v6}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;-><init>(Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;I)V

    .line 61
    .local v3, "grammarActionModify":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->modifiedGrammarActions:Ljava/util/Collection;

    invoke-interface {v6, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 62
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->uploadedChecksums:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v5, v3

    .line 63
    goto/16 :goto_0

    .line 68
    .end local v3    # "grammarActionModify":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    :cond_2
    new-instance v2, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    invoke-direct {v2, v0, v9}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;-><init>(Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;Z)V

    .line 69
    .local v2, "grammarActionAdd":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->addedGrammarActions:Ljava/util/Collection;

    invoke-interface {v6, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v5, v2

    .line 70
    goto/16 :goto_0

    .line 75
    .end local v0    # "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .end local v2    # "grammarActionAdd":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    :cond_3
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->uploadedChecksums:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    move-object v1, v3

    .line 76
    .local v1, "deletedChecksum":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :goto_1
    if-nez v1, :cond_5

    move-object v5, v3

    .line 79
    goto/16 :goto_0

    .line 75
    .end local v1    # "deletedChecksum":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :cond_4
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->uploadedChecksums:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    move-object v1, v6

    goto :goto_1

    .line 81
    .restart local v1    # "deletedChecksum":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :cond_5
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->uploadedChecksums:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 82
    new-instance v4, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    new-instance v7, Lcom/vlingo/core/internal/lmttvocon/contentprovider/StubContentItem;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {v7, v8, v9, v6}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/StubContentItem;-><init>(JI)V

    const/4 v6, 0x0

    invoke-direct {v4, v7, v6}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;-><init>(Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;Z)V

    .line 83
    .local v4, "grammarActionRemove":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->removedGrammarActions:Ljava/util/Collection;

    invoke-interface {v6, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v5, v4

    .line 84
    goto/16 :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->next()Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
