.class public Lcom/vlingo/core/internal/messages/MessageSenderSummary;
.super Ljava/lang/Object;
.source "MessageSenderSummary.java"


# instance fields
.field private alert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

.field private count:I


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/messages/SMSMMSAlert;I)V
    .locals 0
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .param p2, "count"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->alert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 14
    iput p2, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->count:I

    .line 15
    return-void
.end method


# virtual methods
.method public getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->alert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->count:I

    return v0
.end method

.method public setAlert(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V
    .locals 0
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->alert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 24
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->count:I

    .line 27
    return-void
.end method
