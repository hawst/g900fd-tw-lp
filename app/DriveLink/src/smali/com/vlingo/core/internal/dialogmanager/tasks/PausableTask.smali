.class public abstract Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;
.super Lcom/vlingo/core/internal/util/TaskQueue$Task;
.source "PausableTask.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/ResumeControl;


# instance fields
.field private hasBeenPaused:Z

.field private hasBeenResumed:Z

.field private vlRecognitionListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue$Task;-><init>()V

    return-void
.end method


# virtual methods
.method public getVlRecognitionListener()Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->vlRecognitionListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    return-object v0
.end method

.method public isPausable()Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->hasBeenPaused:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->hasBeenResumed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->hasBeenPaused:Z

    if-nez v0, :cond_0

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->hasBeenPaused:Z

    .line 25
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->getParentQueue()Lcom/vlingo/core/internal/util/TaskQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/TaskQueue;->pause()V

    .line 27
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->hasBeenPaused:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->hasBeenResumed:Z

    if-nez v0, :cond_0

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->hasBeenResumed:Z

    .line 33
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->getParentQueue()Lcom/vlingo/core/internal/util/TaskQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/TaskQueue;->resume()V

    .line 35
    :cond_0
    return-void
.end method

.method public setVlRecognitionListener(Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V
    .locals 0
    .param p1, "vlRecognitionListener"    # Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->vlRecognitionListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 49
    return-void
.end method
