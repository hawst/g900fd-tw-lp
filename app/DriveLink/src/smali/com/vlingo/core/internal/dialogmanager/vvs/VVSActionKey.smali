.class public Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;
.super Lcom/vlingo/core/internal/util/DynamicEnum;
.source "VVSActionKey.java"


# static fields
.field public static ADDRESS_BOOK:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static ANSWER_QUESTION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static CALENDAR_READBACK:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static CALL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static CHANGE_VOLUME:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static CONFIRM_HANDLER:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static CONTACT_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static DATE_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static DEFAULT_WEB_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static DIALOG_CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static DIAL_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static EVENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static EXECUTE_UNKNOWN_DEF_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static INTENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static LP_ACTION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static MAP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static MUSIC_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static NAVIGATE_HOME:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static NAVIGATE_HOME_KOREAN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static NOTIFICATION_CHANGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static ONE_ROUND_WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static PLAY_MEDIA:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static POPULATE_TEXTBOX:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static RESOLVE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static RESOLVE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static RESOLVE_CONTACT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static RESOLVE_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SAFEREADER_REPLY:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SAMSUNG_NAVIGATE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SEARCH_WEB_PROMPT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SET_CONFIG:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SET_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SET_TURN_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_0PEN_APP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_ALARMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_ALARM_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_APPOINTMENTS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_APPOINTMENT_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_CALL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_CALL_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_COMPOSE_MEMO:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_COMPOSE_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_CONTACT_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_CONTACT_TYPE_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_CREATE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_CREATE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_DELETE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_DELETE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_EDIT_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_EDIT_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_FORWARD_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_LOCAL_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_MEMO_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_PLAY_ALBUM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_PLAY_ARTIST:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_PLAY_GENERIC_MUSIC:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_PLAY_PLAYLIST:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_PLAY_TITLE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_REPLY_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_SET_TIMER:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_SYSTEM_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_USER_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SHOW_WEB_SEARCH_BUTTON:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SMS_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SOCIAL_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static SPEAK_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static UPDATE_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static WEB_SEARCH_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field public static WORLD_TIME_RESPONSE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowPlayGenericWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_PLAY_GENERIC_MUSIC:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "AddressBook"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->ADDRESS_BOOK:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 34
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "AnswerQuestion"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->ANSWER_QUESTION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 41
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "CalendarReadback"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CALENDAR_READBACK:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 48
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "CallContact"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CALL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 55
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "Cancel"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 61
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ConfirmHandler"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CONFIRM_HANDLER:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 68
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ContactLookup"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CONTACT_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 75
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "DateLookup"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DATE_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 82
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "DefaultWebSearch"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DEFAULT_WEB_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 89
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "DialogCancel"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DIALOG_CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 96
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "DialPage"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DIAL_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 104
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "MusicPage"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->MUSIC_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 111
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "Event"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->EVENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 118
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ExecuteUnknownDefSearch"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->EXECUTE_UNKNOWN_DEF_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 125
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "Intent"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->INTENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 132
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "Listen"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 143
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "LPAction"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LP_ACTION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 150
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "Map"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->MAP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 157
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "NavigateHome"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->NAVIGATE_HOME:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 164
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "NavigateHomeKorean"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->NAVIGATE_HOME_KOREAN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 171
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "Nav"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SAMSUNG_NAVIGATE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 178
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "PlayMedia"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->PLAY_MEDIA:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 185
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "PopulateTextbox"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->POPULATE_TEXTBOX:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 192
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ResolveContact"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->RESOLVE_CONTACT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 199
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "SafereaderReply"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SAFEREADER_REPLY:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 206
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "SearchWebPrompt"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SEARCH_WEB_PROMPT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 213
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "SendMessage"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 220
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "SetConfig"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_CONFIG:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 227
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "SetParams"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 234
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "SettingChange"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 241
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "SetTurnParams"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_TURN_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 248
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowCallWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CALL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 255
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowCallMessageWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CALL_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 262
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowComposeMessageWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_COMPOSE_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 269
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowContactChoicesWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CONTACT_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 276
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowContactTypeChoicesWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CONTACT_TYPE_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 283
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowForwardMessageWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_FORWARD_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 290
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowLocalSearchWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_LOCAL_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 297
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowMessage"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 304
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowOpenAppWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_0PEN_APP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 311
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowReplyMessageWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_REPLY_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 318
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowSetTimerWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SET_TIMER:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 325
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowSystemTurn"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SYSTEM_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 332
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowUserTurn"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_USER_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 339
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "SMSPage"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SMS_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 346
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "SocialPage"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SOCIAL_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 353
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "SpeakMessage"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SPEAK_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 360
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowWebSearchButton"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_WEB_SEARCH_BUTTON:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 367
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "WeatherLookup"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 374
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "WeatherResponse"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->ONE_ROUND_WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 381
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "WorldTimeResponse"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->WORLD_TIME_RESPONSE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 388
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowSetAlarmWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 395
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ResolveAppointment"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->RESOLVE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 402
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowAppointmentsWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_APPOINTMENTS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 409
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowAppointmentChoicesWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_APPOINTMENT_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 416
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowCreateAppointmentWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CREATE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 423
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowDeleteAppointmentWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_DELETE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 430
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowEditAppointmentWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_EDIT_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 437
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "UpdatePage"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->UPDATE_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 444
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "WebSearchPage"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->WEB_SEARCH_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 451
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowComposeMemoWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_COMPOSE_MEMO:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 458
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowMemoLookupWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_MEMO_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 465
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowCreateAlarmWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CREATE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 472
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ResolveAlarm"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->RESOLVE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 479
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowAlarmsWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_ALARMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 486
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowAlarmChoicesWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_ALARM_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 493
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowDeleteAlarmWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_DELETE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 500
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowEditAlarmWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_EDIT_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 507
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowPlayPlaylistWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_PLAY_PLAYLIST:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 514
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ResolveMessage"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->RESOLVE_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 520
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowPlayArtistWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_PLAY_ARTIST:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 528
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowPlayTitleWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_PLAY_TITLE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 535
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ShowPlayAlbumWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_PLAY_ALBUM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 542
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "NotificationChange"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->NOTIFICATION_CHANGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 549
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "ChangeVolume"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CHANGE_VOLUME:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 552
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/util/DynamicEnum;-><init>(Ljava/lang/String;)V

    .line 553
    return-void
.end method
