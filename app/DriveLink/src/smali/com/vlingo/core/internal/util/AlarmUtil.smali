.class public Lcom/vlingo/core/internal/util/AlarmUtil;
.super Ljava/lang/Object;
.source "AlarmUtil.java"


# static fields
.field public static final DAYS_ACTION_PARAM:Ljava/lang/String; = "days"

.field public static final DAYS_MATCH_TYPE_ACTION_PARAM:Ljava/lang/String; = "days.match.type"

.field public static final EXACT_DAYS_MATCH_TYPE_PARAM_VALUE:Ljava/lang/String; = "exact"

.field public static final LOOSE_DAYS_MATCH_TYPE_PARAM_VALUE:Ljava/lang/String; = "loose"

.field public static final REPEAT_ACTION_PARAM:Ljava/lang/String; = "repeat"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static extractAlarm(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/util/Alarm;
    .locals 3
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v2, 0x0

    .line 72
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/Alarm;-><init>()V

    .line 73
    .local v0, "alarm":Lcom/vlingo/core/internal/util/Alarm;
    const-string/jumbo v1, "set"

    invoke-static {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBoolOrNull(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    .line 78
    :goto_0
    const-string/jumbo v1, "time"

    invoke-static {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setTimeFromCanonical(Ljava/lang/String;)V

    .line 79
    const-string/jumbo v1, "repeat"

    invoke-static {p0, v1, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setWeeklyRepeating(Z)V

    .line 80
    const-string/jumbo v1, "days"

    invoke-static {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->addDayFromDaysCanonicalForm(Ljava/lang/String;)V

    .line 81
    return-object v0

    .line 76
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    goto :goto_0
.end method

.method public static extractAlarmQuery(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/alarms/AlarmQueryObject;
    .locals 6
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v5, 0x0

    .line 33
    new-instance v3, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;

    invoke-direct {v3}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;-><init>()V

    .line 34
    .local v3, "query":Lcom/vlingo/core/internal/alarms/AlarmQueryObject;
    const-string/jumbo v4, "range.start"

    invoke-static {p0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->setBegin(Ljava/lang/String;)V

    .line 35
    const-string/jumbo v4, "range.end"

    invoke-static {p0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->setEnd(Ljava/lang/String;)V

    .line 36
    const-string/jumbo v4, "count"

    invoke-static {p0, v4, v5, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->setCount(I)V

    .line 37
    const-string/jumbo v4, "set"

    invoke-static {p0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBoolOrNull(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->setActive(Ljava/lang/Boolean;)V

    .line 38
    const-string/jumbo v4, "repeat"

    invoke-static {p0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBoolOrNull(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->setRepeating(Ljava/lang/Boolean;)V

    .line 39
    const-string/jumbo v4, "days.match.type"

    invoke-static {p0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "daysMatchType":Ljava/lang/String;
    const-string/jumbo v4, "exact"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 41
    sget-object v4, Lcom/vlingo/core/internal/util/Alarm$MatchType;->EXACT:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->setDaysMatchType(Lcom/vlingo/core/internal/util/Alarm$MatchType;)V

    .line 47
    :goto_0
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/Alarm;-><init>()V

    .line 48
    .local v0, "alarm":Lcom/vlingo/core/internal/util/Alarm;
    const-string/jumbo v4, "days"

    invoke-static {p0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "daysToLookup":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 51
    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/util/Alarm;->addDayFromDaysCanonicalForm(Ljava/lang/String;)V

    .line 52
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/Alarm;->getDayMask()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->setDayMask(Ljava/lang/Integer;)V

    .line 56
    :cond_0
    invoke-virtual {v3}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->normalize()V

    .line 58
    return-object v3

    .line 43
    .end local v0    # "alarm":Lcom/vlingo/core/internal/util/Alarm;
    .end local v2    # "daysToLookup":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/vlingo/core/internal/util/Alarm$MatchType;->LOOSE:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->setDaysMatchType(Lcom/vlingo/core/internal/util/Alarm$MatchType;)V

    goto :goto_0
.end method

.method private static isDeleteKeyword(Ljava/lang/String;)Z
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 134
    if-eqz p0, :cond_0

    const-string/jumbo v0, "#NULL#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static mergeChanges(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/util/Alarm;
    .locals 7
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p1, "origAlarm"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    const/4 v6, 0x0

    .line 85
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->clone()Lcom/vlingo/core/internal/util/Alarm;

    move-result-object v0

    .line 87
    .local v0, "alarm":Lcom/vlingo/core/internal/util/Alarm;
    const-string/jumbo v4, "time"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getTimeCanonical()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v4, v5}, Lcom/vlingo/core/internal/util/AlarmUtil;->mergeValues(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/util/Alarm;->setTimeFromCanonical(Ljava/lang/String;)V

    .line 89
    const-string/jumbo v4, "set"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v4, v5}, Lcom/vlingo/core/internal/util/AlarmUtil;->mergeValues(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 90
    .local v2, "isActive":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/AlarmUtil;->isDeleteKeyword(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 91
    const-string/jumbo v4, "AlarmUtil"

    const-string/jumbo v5, "Didn\'t expect NULL keyword for \"set\". Setting value to false"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    sget-object v4, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    .line 101
    :goto_0
    const-string/jumbo v4, "repeat"

    invoke-static {p0, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBoolOrNull(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 102
    .local v3, "isRepeating":Ljava/lang/Boolean;
    if-eqz v3, :cond_0

    .line 103
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/util/Alarm;->setWeeklyRepeating(Z)V

    .line 105
    :cond_0
    const-string/jumbo v4, "days"

    invoke-static {p0, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 106
    .local v1, "daysToSetCanonical":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 107
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setDayFromDaysCanonicalForm(Ljava/lang/String;)V

    .line 115
    :cond_1
    return-object v0

    .line 95
    .end local v1    # "daysToSetCanonical":Ljava/lang/String;
    .end local v3    # "isRepeating":Ljava/lang/Boolean;
    :cond_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 96
    sget-object v4, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    goto :goto_0

    .line 98
    :cond_3
    sget-object v4, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    goto :goto_0
.end method

.method private static mergeValues(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "origName"    # Ljava/lang/String;

    .prologue
    .line 119
    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "newName":Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/vlingo/core/internal/util/AlarmUtil;->mergeValues(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static mergeValues(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "newName"    # Ljava/lang/String;
    .param p1, "origName"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    .end local p1    # "origName":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 127
    .restart local p1    # "origName":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/vlingo/core/internal/util/AlarmUtil;->isDeleteKeyword(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    move-object p1, p0

    .line 130
    goto :goto_0
.end method
