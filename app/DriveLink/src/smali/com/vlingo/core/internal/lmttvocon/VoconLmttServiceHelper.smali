.class public final Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;
.super Ljava/lang/Object;
.source "VoconLmttServiceHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method private static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public static isVoconLmttEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 47
    const-string/jumbo v1, "tos_accepted"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSDKRecoMode()Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/recognition/RecognitionMode;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static notifyApplicationActivity()Z
    .locals 3

    .prologue
    .line 33
    invoke-static {}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 34
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.vlingo.lmttvocon.action.APP_ACTIVITY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static notifyBootCompleted()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 40
    invoke-static {}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 41
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    .local v1, "startServiceIntent":Landroid/content/Intent;
    const-string/jumbo v3, "com.vlingo.lmttvocon.extra.BOOT_START"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 43
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static sendUpdate(Z)Z
    .locals 3
    .param p0, "startFromScratch"    # Z

    .prologue
    .line 16
    invoke-static {}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 17
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 18
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.vlingo.lmttvocon.action.SEND_UPDATE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 19
    const-string/jumbo v2, "com.vlingo.lmttvocon.extra.CLEAR_LMTT"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 20
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static sendUpdate(ZLcom/vlingo/core/internal/lmttvocon/UploadType;)Z
    .locals 4
    .param p0, "startFromScratch"    # Z
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .prologue
    .line 24
    invoke-static {}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 25
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 26
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.vlingo.lmttvocon.action.SEND_UPDATE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 27
    const-string/jumbo v2, "com.vlingo.lmttvocon.extra.CLEAR_LMTT"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 28
    const-string/jumbo v2, "com.vlingo.lmttvocon.extra.LMTT_TYPE"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/UploadType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 29
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
