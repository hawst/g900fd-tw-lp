.class public Lcom/vlingo/core/internal/tts/ContactTextProcessor;
.super Ljava/lang/Object;
.source "ContactTextProcessor.java"

# interfaces
.implements Lcom/vlingo/core/internal/tts/TTSTextProcessor;


# static fields
.field private static final PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string/jumbo v0, "<Contact id=\"(\\d+)\">([^<>]+)</Contact>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/tts/ContactTextProcessor;->PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public process(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 23
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    move-object v6, p2

    .line 67
    .end local p2    # "text":Ljava/lang/String;
    .local v6, "text":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 28
    .end local v6    # "text":Ljava/lang/String;
    .restart local p2    # "text":Ljava/lang/String;
    :cond_0
    sget-object v7, Lcom/vlingo/core/internal/tts/ContactTextProcessor;->PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v7, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 29
    .local v4, "matcher":Ljava/util/regex/Matcher;
    :goto_1
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 30
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 31
    .local v1, "contactId":I
    const/4 v7, 0x2

    invoke-virtual {v4, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 35
    .local v3, "contactName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 37
    .local v5, "phoneticContactName":Ljava/lang/String;
    if-eqz p3, :cond_1

    .line 38
    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p3, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 40
    .local v0, "allMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-eqz v0, :cond_1

    .line 41
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getContactMatchFromId(Ljava/util/List;I)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v2

    .line 43
    .local v2, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v2, :cond_1

    .line 44
    iget-object v5, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    .line 49
    .end local v0    # "allMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v2    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_1
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isJapaneseString(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 50
    const/4 v5, 0x0

    .line 53
    :cond_2
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 54
    invoke-static {v5}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 61
    :goto_2
    sget-object v7, Lcom/vlingo/core/internal/tts/ContactTextProcessor;->PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v7, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 62
    goto :goto_1

    .line 59
    :cond_3
    invoke-static {v3}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    .end local v1    # "contactId":I
    .end local v3    # "contactName":Ljava/lang/String;
    .end local v5    # "phoneticContactName":Ljava/lang/String;
    :cond_4
    move-object v6, p2

    .line 67
    .end local p2    # "text":Ljava/lang/String;
    .restart local v6    # "text":Ljava/lang/String;
    goto :goto_0
.end method
