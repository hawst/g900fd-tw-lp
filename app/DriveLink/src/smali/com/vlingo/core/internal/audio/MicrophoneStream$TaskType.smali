.class public final enum Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
.super Ljava/lang/Enum;
.source "MicrophoneStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/MicrophoneStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TaskType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

.field public static final enum LOCALRECORD:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

.field public static final enum PHRASESPOTTING:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

.field public static final enum RECOGNITION:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    new-instance v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    const-string/jumbo v1, "PHRASESPOTTING"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->PHRASESPOTTING:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    new-instance v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    const-string/jumbo v1, "RECOGNITION"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->RECOGNITION:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    new-instance v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    const-string/jumbo v1, "LOCALRECORD"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->LOCALRECORD:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    sget-object v1, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->PHRASESPOTTING:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->RECOGNITION:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->LOCALRECORD:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->$VALUES:[Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    const-class v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->$VALUES:[Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    return-object v0
.end method
