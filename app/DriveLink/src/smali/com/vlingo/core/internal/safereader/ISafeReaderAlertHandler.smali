.class public interface abstract Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
.super Ljava/lang/Object;
.source "ISafeReaderAlertHandler.java"


# virtual methods
.method public abstract handleAlert(Ljava/util/LinkedList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract handleAlert(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            ")V"
        }
    .end annotation
.end method

.method public abstract isSilentMode()Z
.end method

.method public abstract readoutDelay()J
.end method

.method public abstract setSilentMode(Z)V
.end method
