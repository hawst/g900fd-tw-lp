.class public final Lcom/vlingo/core/internal/recognition/VLRecognizerIntent;
.super Ljava/lang/Object;
.source "VLRecognizerIntent.java"


# static fields
.field public static final ACTION_RECOGNIZE:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE"

.field public static final EXTRA_AMR_FILE:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.amr_wavefile"

.field public static final EXTRA_APP_ID:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.app_id"

.field public static final EXTRA_CURSOR_POS:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.cursor_pos"

.field public static final EXTRA_CUR_TEXT:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.cur_text"

.field public static final EXTRA_EXECUTE_ACTION:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.execute_action"

.field public static final EXTRA_FIELD_CONTEXT:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.field_context"

.field public static final EXTRA_FIELD_ID:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.field_id"

.field public static final EXTRA_PCM_FILE:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.pcm_wavefile"

.field public static final EXTRA_PCM_SAMPLE_BITS:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.pcm_sample_bits"

.field public static final EXTRA_PCM_SAMPLE_RATE:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.pcm_sample_rate"

.field public static final EXTRA_PENDING_INTENT:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.pending_intent"

.field public static final EXTRA_RECO_DURATION_MS:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.duration_ms"

.field public static final EXTRA_RESULTS:Ljava/lang/String; = "com.vlingo.client.actions.RESULTS"

.field public static final EXTRA_SERVER:Ljava/lang/String; = "com.vlingo.client.actions.RECOGNIZE.server"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method
