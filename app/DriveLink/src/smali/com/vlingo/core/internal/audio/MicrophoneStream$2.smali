.class Lcom/vlingo/core/internal/audio/MicrophoneStream$2;
.super Ljava/lang/Object;
.source "MicrophoneStream.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/audio/MicrophoneStream;->close()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 472
    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOCK:Ljava/lang/Object;
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$100()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 473
    :try_start_0
    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->LOGTAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "close() [inner] currentState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentState:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;
    invoke-static {v3}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$300(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentState:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$300(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->STARTED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    if-ne v0, v2, :cond_1

    .line 476
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->mBufferedData:[S
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$402(Lcom/vlingo/core/internal/audio/MicrophoneStream;[S)[S

    .line 477
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$000(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Landroid/media/AudioRecord;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 478
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$000(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Landroid/media/AudioRecord;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 479
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$002(Lcom/vlingo/core/internal/audio/MicrophoneStream;Landroid/media/AudioRecord;)Landroid/media/AudioRecord;

    .line 480
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->CLOSED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    # setter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentState:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$302(Lcom/vlingo/core/internal/audio/MicrophoneStream;Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;)Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    .line 482
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioLogger:Lcom/vlingo/core/internal/audio/AudioLogger;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$500(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/AudioLogger;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioLogger:Lcom/vlingo/core/internal/audio/AudioLogger;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$500(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/AudioLogger;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/audio/AudioLogger;->dumpToFile()V

    .line 484
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->mAudioLogger:Lcom/vlingo/core/internal/audio/AudioLogger;
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$502(Lcom/vlingo/core/internal/audio/MicrophoneStream;Lcom/vlingo/core/internal/audio/AudioLogger;)Lcom/vlingo/core/internal/audio/AudioLogger;

    .line 487
    :cond_0
    const/4 v0, 0x0

    # setter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->currentOwner:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$602(Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;)Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    .line 493
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->noiseCancelFilter:Lcom/vlingo/core/internal/audio/AudioFilterAdapter;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$700(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/AudioFilterAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 494
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$2;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->noiseCancelFilter:Lcom/vlingo/core/internal/audio/AudioFilterAdapter;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$700(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/AudioFilterAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/audio/AudioFilterAdapter;->quit()Z

    .line 495
    :cond_2
    monitor-exit v1

    .line 498
    return-void

    .line 495
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
