.class public Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil$AppendBasicMatchToWhereClauseOnlyImpl;
.super Ljava/lang/Object;
.source "ContactDBQueryUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil$ContactMatchQueryInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppendBasicMatchToWhereClauseOnlyImpl"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public generateBasicWhereClause(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p2, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1, p2}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->appendBasicMatchToWhereClauseOnly(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
