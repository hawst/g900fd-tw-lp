.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowOpenAppWidgetHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field protected listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private launchApp(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 11
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "ordinal"    # Ljava/lang/String;
    .param p3, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p4, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 51
    new-instance v2, Lcom/vlingo/core/internal/util/OpenAppUtil;

    invoke-direct {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;-><init>()V

    .line 52
    .local v2, "oau":Lcom/vlingo/core/internal/util/OpenAppUtil;
    const/4 v0, 0x0

    .line 53
    .local v0, "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 55
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 56
    invoke-virtual {v2, p1}, Lcom/vlingo/core/internal/util/OpenAppUtil;->buildMatchingAppList(Ljava/lang/String;)V

    .line 57
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 58
    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_opening_app:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v6, 0x1

    new-array v8, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v6

    const/4 v10, 0x0

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v9

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 60
    .local v5, "text":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V

    .line 102
    .end local v5    # "text":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_2

    .line 62
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v6

    invoke-static {p4, v6}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    .line 63
    iput-object p4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 64
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_applications:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 65
    invoke-static {p4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v6

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 66
    .local v3, "pageAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->OpenApp:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v7, 0x0

    invoke-interface {p4, v6, v7, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 67
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->promptForApp(Ljava/util/List;)V

    goto :goto_0

    .line 71
    .end local v3    # "pageAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    :cond_2
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 72
    const-string/jumbo v6, "camera"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 73
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND_FOR_CAMERA_ONLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-interface {v6, v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 83
    .local v4, "spokenText":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 84
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V

    goto :goto_0

    .line 76
    .end local v4    # "spokenText":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-interface {v6, v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "spokenText":Ljava/lang/String;
    goto :goto_1

    .line 80
    .end local v4    # "spokenText":Ljava/lang/String;
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "spokenText":Ljava/lang/String;
    goto :goto_1

    .line 86
    .end local v4    # "spokenText":Ljava/lang/String;
    :cond_5
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-static {p4, p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElement(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_6

    .line 87
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->promptForApp(Ljava/util/List;)V

    .line 88
    iput-object p4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 89
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p4, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 90
    .local v1, "listAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    if-eqz v1, :cond_0

    .line 91
    invoke-static {p4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 92
    .restart local v3    # "pageAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->OpenApp:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v7, 0x0

    invoke-interface {p4, v6, v7, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_0

    .line 95
    .end local v1    # "listAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    .end local v3    # "pageAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    :cond_6
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p4, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 96
    .restart local v1    # "listAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    invoke-static {p4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 97
    .restart local v3    # "pageAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    invoke-static {v3, p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    check-cast v0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 98
    .restart local v0    # "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    invoke-static {p4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 99
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_opening_app:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 100
    .restart local v5    # "text":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V

    goto/16 :goto_0
.end method

.method private promptForApp(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "appInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_LAUNCHAPP_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_LAUNCHAPP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 137
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "car-sms-message"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 138
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v4, 0x0

    .line 42
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 43
    const-string/jumbo v2, "Which"

    invoke-static {p1, v2, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 44
    .local v1, "ordinal":Ljava/lang/String;
    const-string/jumbo v2, "Name"

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    invoke-static {p1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "name":Ljava/lang/String;
    invoke-direct {p0, v0, v1, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->launchApp(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 46
    return v4
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    if-eqz v1, :cond_1

    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.DataTransfered"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    instance-of v1, p2, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v2, v1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;I)V

    .line 121
    :goto_1
    return-void

    .line 115
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 117
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    move-object v0, p2

    .line 118
    check-cast v0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 119
    .local v0, "ai":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V

    goto :goto_1
.end method

.method public launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V
    .locals 3
    .param p1, "appInfo"    # Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .prologue
    .line 124
    const-string/jumbo v0, "launch"

    .line 125
    .local v0, "landingPageId":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 128
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 129
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;

    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->queue()V

    .line 132
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 110
    return-void
.end method
