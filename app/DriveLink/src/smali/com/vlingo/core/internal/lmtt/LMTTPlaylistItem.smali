.class public Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;
.super Lcom/vlingo/core/internal/lmtt/LMTTItem;
.source "LMTTPlaylistItem.java"


# instance fields
.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 1
    .param p1, "uid"    # J
    .param p3, "changeType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;-><init>(Ljava/lang/String;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "uid"    # J
    .param p4, "changeType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 13
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/vlingo/core/internal/lmtt/LMTTItem;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 15
    if-nez p1, :cond_0

    .line 16
    const-string/jumbo p1, ""

    .line 19
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;->title:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public hashCode()I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;->title:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "LMTTPlaylistItem | uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;->uid:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | changeType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;->changeType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
