.class public final Lcom/vlingo/core/internal/audio/ResourceManager;
.super Ljava/lang/Object;
.source "ResourceManager.java"


# static fields
.field private static mgr:Lcom/vlingo/core/internal/audio/ResourceManager;


# instance fields
.field private recognitionInUse:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/vlingo/core/internal/audio/ResourceManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/audio/ResourceManager;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/audio/ResourceManager;->mgr:Lcom/vlingo/core/internal/audio/ResourceManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/ResourceManager;->recognitionInUse:Z

    .line 17
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/audio/ResourceManager;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/vlingo/core/internal/audio/ResourceManager;->mgr:Lcom/vlingo/core/internal/audio/ResourceManager;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized isRecognizing()Z
    .locals 1

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/ResourceManager;->recognitionInUse:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized releaseRecognition()V
    .locals 1

    .prologue
    .line 48
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/ResourceManager;->recognitionInUse:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    monitor-exit p0

    return-void

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized requestRecognition(Z)Z
    .locals 2
    .param p1, "force"    # Z

    .prologue
    const/4 v0, 0x1

    .line 32
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/vlingo/core/internal/audio/ResourceManager;->recognitionInUse:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 33
    const/4 v0, 0x0

    .line 36
    :goto_0
    monitor-exit p0

    return v0

    .line 35
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/ResourceManager;->recognitionInUse:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
