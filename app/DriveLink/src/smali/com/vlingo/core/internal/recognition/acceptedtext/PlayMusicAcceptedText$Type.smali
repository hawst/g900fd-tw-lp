.class final enum Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;
.super Ljava/lang/Enum;
.source "PlayMusicAcceptedText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

.field public static final enum ALBUM:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

.field public static final enum ARTIST:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

.field public static final enum ELSE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

.field public static final enum GENERIC:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

.field public static final enum TITLE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    const-string/jumbo v1, "ARTIST"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ARTIST:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    .line 19
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    const-string/jumbo v1, "ALBUM"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ALBUM:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    .line 20
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    const-string/jumbo v1, "TITLE"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->TITLE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    .line 21
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    const-string/jumbo v1, "GENERIC"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->GENERIC:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    .line 22
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    const-string/jumbo v1, "ELSE"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ELSE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    .line 17
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ARTIST:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ALBUM:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->TITLE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->GENERIC:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ELSE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->$VALUES:[Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->$VALUES:[Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    return-object v0
.end method


# virtual methods
.method public getPt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->GENERIC:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    if-ne p0, v1, :cond_0

    .line 27
    const-string/jumbo v0, ""

    .line 32
    .local v0, "ret":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 29
    .end local v0    # "ret":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "ret":Ljava/lang/String;
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ELSE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    if-ne p0, v1, :cond_0

    .line 40
    const-string/jumbo v0, ""

    .line 45
    .local v0, "ret":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 42
    .end local v0    # "ret":Ljava/lang/String;
    :cond_0
    invoke-super {p0}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "ret":Ljava/lang/String;
    goto :goto_0
.end method
