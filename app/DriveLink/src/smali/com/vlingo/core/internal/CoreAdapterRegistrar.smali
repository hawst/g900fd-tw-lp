.class public final Lcom/vlingo/core/internal/CoreAdapterRegistrar;
.super Ljava/lang/Object;
.source "CoreAdapterRegistrar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;
    }
.end annotation


# static fields
.field private static final HANDLERS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/CoreAdapter;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->HANDLERS:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;
    .locals 1
    .param p0, "key"    # Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/CoreAdapter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    sget-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->HANDLERS:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method

.method public static registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V
    .locals 1
    .param p0, "key"    # Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/CoreAdapter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "handler":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    sget-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->HANDLERS:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    return-void
.end method

.method public static unregisterHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)V
    .locals 1
    .param p0, "key"    # Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .prologue
    .line 106
    sget-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->HANDLERS:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    return-void
.end method
