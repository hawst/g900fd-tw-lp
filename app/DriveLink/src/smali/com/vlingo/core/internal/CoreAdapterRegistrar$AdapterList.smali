.class public final enum Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;
.super Ljava/lang/Enum;
.source "CoreAdapterRegistrar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/CoreAdapterRegistrar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AdapterList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

.field public static final enum AudioSourceSelector:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

.field public static final enum BDeviceUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

.field public static final enum DomainDecider:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

.field public static final enum ExternalTTSEngine:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

.field public static final enum MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

.field public static final enum MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

.field public static final enum NoiseCancel:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

.field public static final enum PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-string/jumbo v1, "NoiseCancel"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->NoiseCancel:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 33
    new-instance v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-string/jumbo v1, "PhraseSpotter"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 40
    new-instance v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-string/jumbo v1, "ExternalTTSEngine"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->ExternalTTSEngine:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 55
    new-instance v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-string/jumbo v1, "AudioSourceSelector"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->AudioSourceSelector:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 64
    new-instance v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-string/jumbo v1, "MsgUtil"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 73
    new-instance v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-string/jumbo v1, "BDeviceUtil"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->BDeviceUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 80
    new-instance v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-string/jumbo v1, "MusicPlusUtil"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 86
    new-instance v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-string/jumbo v1, "DomainDecider"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->DomainDecider:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 21
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->NoiseCancel:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->ExternalTTSEngine:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->AudioSourceSelector:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->BDeviceUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->DomainDecider:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->$VALUES:[Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->$VALUES:[Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    return-object v0
.end method
