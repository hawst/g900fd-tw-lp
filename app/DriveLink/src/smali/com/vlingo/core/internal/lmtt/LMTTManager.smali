.class Lcom/vlingo/core/internal/lmtt/LMTTManager;
.super Ljava/lang/Object;
.source "LMTTManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmtt/LMTTManager$1;,
        Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;,
        Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;,
        Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;,
        Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;
    }
.end annotation


# instance fields
.field private mLastReceivedRequestId:I

.field private mUploadHandler:Landroid/os/Handler;

.field private mWorkerThread:Landroid/os/HandlerThread;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;)V
    .locals 3
    .param p1, "listener"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "UploadWorker"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mWorkerThread:Landroid/os/HandlerThread;

    .line 90
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 91
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Landroid/os/Looper;Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;Lcom/vlingo/core/internal/lmtt/LMTTManager$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mUploadHandler:Landroid/os/Handler;

    .line 92
    return-void
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/lmtt/LMTTManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTManager;

    .prologue
    .line 50
    iget v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mLastReceivedRequestId:I

    return v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;Ljava/util/HashMap;Ljava/util/ArrayList;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .param p1, "x1"    # Ljava/util/HashMap;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 50
    invoke-static {p0, p1, p2}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->getChangedItemsList(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;Ljava/util/HashMap;Ljava/util/ArrayList;)I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/lmtt/LMTTManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mUploadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static determineDeletes(Ljava/util/HashMap;Ljava/util/ArrayList;Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;)V
    .locals 8
    .param p2, "lmttItemType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem;",
            ">;",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 822
    .local p0, "synchedItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .local p1, "changedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/lmtt/LMTTItem;>;"
    const/4 v0, 0x0

    .line 823
    .local v0, "deleteCount":I
    invoke-virtual {p0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 824
    .local v3, "id":J
    const/4 v5, 0x0

    .line 825
    .local v5, "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    sget-object v6, Lcom/vlingo/core/internal/lmtt/LMTTManager$1;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$LmttItemType:[I

    invoke-virtual {p2}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 837
    :goto_1
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 838
    add-int/lit8 v0, v0, 0x1

    .line 839
    rem-int/lit8 v6, v0, 0xa

    if-nez v6, :cond_0

    .line 841
    const-wide/16 v6, 0xa

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 842
    :catch_0
    move-exception v1

    .line 843
    .local v1, "ex":Ljava/lang/Exception;
    const-string/jumbo v6, "VLG_EXCEPTION"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 827
    .end local v1    # "ex":Ljava/lang/Exception;
    :pswitch_0
    new-instance v5, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;

    .end local v5    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    sget-object v6, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v5, v3, v4, v6}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;-><init>(JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 828
    .restart local v5    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    goto :goto_1

    .line 830
    :pswitch_1
    new-instance v5, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;

    .end local v5    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    sget-object v6, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v5, v3, v4, v6}, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;-><init>(JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 831
    .restart local v5    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    goto :goto_1

    .line 833
    :pswitch_2
    new-instance v5, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;

    .end local v5    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    sget-object v6, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v5, v3, v4, v6}, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;-><init>(JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 834
    .restart local v5    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    goto :goto_1

    .line 849
    .end local v3    # "id":J
    .end local v5    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    :cond_1
    return-void

    .line 825
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static determineInsertsAndUpdates(Ljava/util/HashMap;Ljava/util/ArrayList;Lcom/vlingo/core/internal/lmtt/LMTTItem;)V
    .locals 5
    .param p2, "item"    # Lcom/vlingo/core/internal/lmtt/LMTTItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem;",
            ">;",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 802
    .local p0, "synchedItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .local p1, "changedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/lmtt/LMTTItem;>;"
    if-eqz p2, :cond_0

    .line 803
    iget-wide v1, p2, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    .line 804
    .local v1, "id":J
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 805
    .local v0, "hashcode":Ljava/lang/Integer;
    if-nez v0, :cond_1

    .line 808
    sget-object v3, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->INSERT:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {p2, v3}, Lcom/vlingo/core/internal/lmtt/LMTTItem;->setChangeType(Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 809
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 818
    .end local v0    # "hashcode":Ljava/lang/Integer;
    .end local v1    # "id":J
    :cond_0
    :goto_0
    return-void

    .line 810
    .restart local v0    # "hashcode":Ljava/lang/Integer;
    .restart local v1    # "id":J
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 812
    sget-object v3, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {p2, v3}, Lcom/vlingo/core/internal/lmtt/LMTTItem;->setChangeType(Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 813
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static getChangedItemsList(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;Ljava/util/HashMap;Ljava/util/ArrayList;)I
    .locals 18
    .param p0, "lmttItemType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 551
    .local p1, "synchedItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .local p2, "changedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/lmtt/LMTTItem;>;"
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 554
    .local v3, "contentResolver":Landroid/content/ContentResolver;
    const/4 v10, 0x0

    .line 556
    .local v10, "deviceItemCount":I
    sget-object v7, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v7}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v13

    .line 557
    .local v13, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v15, 0x0

    .line 558
    .local v15, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v13, :cond_0

    .line 560
    :try_start_0
    invoke-virtual {v13}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v15, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 570
    :cond_0
    :goto_0
    const/4 v9, 0x0

    .line 574
    .local v9, "cur":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 575
    .local v6, "where":Ljava/lang/String;
    const/4 v8, 0x0

    .line 579
    .local v8, "sortOrder":Ljava/lang/String;
    :try_start_1
    sget-object v7, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    move-object/from16 v0, p0

    if-ne v0, v7, :cond_4

    .line 580
    if-eqz v15, :cond_3

    .line 581
    sget-object v7, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v15, v7}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v4

    .line 585
    .local v4, "uri":Landroid/net/Uri;
    :goto_1
    const/4 v7, 0x7

    new-array v5, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string/jumbo v16, "_id"

    aput-object v16, v5, v7

    const/4 v7, 0x1

    const-string/jumbo v16, "title"

    aput-object v16, v5, v7

    const/4 v7, 0x2

    const-string/jumbo v16, "artist"

    aput-object v16, v5, v7

    const/4 v7, 0x3

    const-string/jumbo v16, "composer"

    aput-object v16, v5, v7

    const/4 v7, 0x4

    const-string/jumbo v16, "album"

    aput-object v16, v5, v7

    const/4 v7, 0x5

    const-string/jumbo v16, "year"

    aput-object v16, v5, v7

    const/4 v7, 0x6

    const-string/jumbo v16, "_data"

    aput-object v16, v5, v7

    .line 594
    .local v5, "proj":[Ljava/lang/String;
    const-string/jumbo v6, "is_music!=0"

    .line 595
    const/4 v7, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    .line 644
    :cond_1
    :goto_2
    if-nez v9, :cond_8

    .line 647
    const/4 v7, 0x0

    .line 684
    if-eqz v9, :cond_2

    .line 686
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    .line 693
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "proj":[Ljava/lang/String;
    :cond_2
    :goto_3
    return v7

    .line 561
    .end local v6    # "where":Ljava/lang/String;
    .end local v8    # "sortOrder":Ljava/lang/String;
    .end local v9    # "cur":Landroid/database/Cursor;
    :catch_0
    move-exception v11

    .line 562
    .local v11, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v11}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 563
    .end local v11    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v11

    .line 564
    .local v11, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v11}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 565
    .end local v11    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v11

    .line 566
    .local v11, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v11}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 583
    .end local v11    # "e":Ljava/lang/NullPointerException;
    .restart local v6    # "where":Ljava/lang/String;
    .restart local v8    # "sortOrder":Ljava/lang/String;
    .restart local v9    # "cur":Landroid/database/Cursor;
    :cond_3
    :try_start_3
    sget-object v4, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .restart local v4    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 596
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_4
    sget-object v7, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    move-object/from16 v0, p0

    if-ne v0, v7, :cond_6

    .line 597
    if-eqz v15, :cond_5

    .line 598
    sget-object v7, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v15, v7}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v4

    .line 603
    .restart local v4    # "uri":Landroid/net/Uri;
    :goto_4
    const/4 v7, 0x2

    new-array v5, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string/jumbo v16, "_id"

    aput-object v16, v5, v7

    const/4 v7, 0x1

    const-string/jumbo v16, "name"

    aput-object v16, v5, v7

    .line 604
    .restart local v5    # "proj":[Ljava/lang/String;
    const/4 v7, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto :goto_2

    .line 600
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "proj":[Ljava/lang/String;
    :cond_5
    sget-object v4, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .restart local v4    # "uri":Landroid/net/Uri;
    goto :goto_4

    .line 605
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_6
    sget-object v7, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_CONTACT:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    move-object/from16 v0, p0

    if-ne v0, v7, :cond_7

    .line 611
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 612
    .restart local v4    # "uri":Landroid/net/Uri;
    const/16 v7, 0xb

    new-array v5, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string/jumbo v16, "_id"

    aput-object v16, v5, v7

    const/4 v7, 0x1

    const-string/jumbo v16, "data2"

    aput-object v16, v5, v7

    const/4 v7, 0x2

    const-string/jumbo v16, "data5"

    aput-object v16, v5, v7

    const/4 v7, 0x3

    const-string/jumbo v16, "data3"

    aput-object v16, v5, v7

    const/4 v7, 0x4

    const-string/jumbo v16, "raw_contact_id"

    aput-object v16, v5, v7

    const/4 v7, 0x5

    const-string/jumbo v16, "data1"

    aput-object v16, v5, v7

    const/4 v7, 0x6

    const-string/jumbo v16, "data7"

    aput-object v16, v5, v7

    const/4 v7, 0x7

    const-string/jumbo v16, "data8"

    aput-object v16, v5, v7

    const/16 v7, 0x8

    const-string/jumbo v16, "data9"

    aput-object v16, v5, v7

    const/16 v7, 0x9

    const-string/jumbo v16, "mimetype"

    aput-object v16, v5, v7

    const/16 v7, 0xa

    const-string/jumbo v16, "data_version"

    aput-object v16, v5, v7

    .line 625
    .restart local v5    # "proj":[Ljava/lang/String;
    const-string/jumbo v6, "mimetype in (\'vnd.android.cursor.item/name\', \'vnd.android.cursor.item/nickname\') AND in_visible_group=1"

    .line 628
    const-string/jumbo v8, "raw_contact_id, mimetype DESC, _id"

    .line 629
    const/4 v7, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 631
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v7

    invoke-interface {v7, v3}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v7

    if-nez v7, :cond_1

    .line 632
    const/4 v7, 0x0

    .line 684
    if-eqz v9, :cond_2

    .line 686
    :try_start_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_3

    .line 687
    :catch_3
    move-exception v12

    .line 688
    .local v12, "ex":Ljava/lang/Exception;
    const-string/jumbo v16, "VLG_EXCEPTION"

    invoke-static {v12}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 638
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "proj":[Ljava/lang/String;
    .end local v12    # "ex":Ljava/lang/Exception;
    :cond_7
    const/4 v7, -0x1

    .line 684
    if-eqz v9, :cond_2

    .line 686
    :try_start_5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_3

    .line 687
    :catch_4
    move-exception v12

    .line 688
    .restart local v12    # "ex":Ljava/lang/Exception;
    const-string/jumbo v16, "VLG_EXCEPTION"

    invoke-static {v12}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 687
    .end local v12    # "ex":Ljava/lang/Exception;
    .restart local v4    # "uri":Landroid/net/Uri;
    .restart local v5    # "proj":[Ljava/lang/String;
    :catch_5
    move-exception v12

    .line 688
    .restart local v12    # "ex":Ljava/lang/Exception;
    const-string/jumbo v16, "VLG_EXCEPTION"

    invoke-static {v12}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 650
    .end local v12    # "ex":Ljava/lang/Exception;
    :cond_8
    :goto_5
    :try_start_6
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 651
    const/4 v14, 0x0

    .line 652
    .local v14, "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    sget-object v7, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    move-object/from16 v0, p0

    if-ne v0, v7, :cond_9

    .line 653
    invoke-static {v9}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->getSongItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmtt/LMTTItem;

    move-result-object v14

    .line 660
    :goto_6
    if-eqz v14, :cond_8

    .line 663
    add-int/lit8 v10, v10, 0x1

    .line 664
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v14}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->determineInsertsAndUpdates(Ljava/util/HashMap;Ljava/util/ArrayList;Lcom/vlingo/core/internal/lmtt/LMTTItem;)V

    .line 667
    rem-int/lit8 v7, v10, 0xa
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-nez v7, :cond_8

    .line 669
    const-wide/16 v16, 0xa

    :try_start_7
    invoke-static/range {v16 .. v17}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_5

    .line 670
    :catch_6
    move-exception v12

    .line 671
    .restart local v12    # "ex":Ljava/lang/Exception;
    :try_start_8
    const-string/jumbo v7, "VLG_EXCEPTION"

    invoke-static {v12}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_5

    .line 680
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "proj":[Ljava/lang/String;
    .end local v12    # "ex":Ljava/lang/Exception;
    .end local v14    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    :catch_7
    move-exception v11

    .line 681
    .local v11, "e":Ljava/lang/Exception;
    :try_start_9
    const-string/jumbo v7, "VLG_EXCEPTION"

    invoke-static {v11}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 682
    const/4 v7, -0x1

    .line 684
    if-eqz v9, :cond_2

    .line 686
    :try_start_a
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8

    goto/16 :goto_3

    .line 687
    :catch_8
    move-exception v12

    .line 688
    .restart local v12    # "ex":Ljava/lang/Exception;
    const-string/jumbo v16, "VLG_EXCEPTION"

    invoke-static {v12}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 654
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v12    # "ex":Ljava/lang/Exception;
    .restart local v4    # "uri":Landroid/net/Uri;
    .restart local v5    # "proj":[Ljava/lang/String;
    .restart local v14    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    :cond_9
    :try_start_b
    sget-object v7, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    move-object/from16 v0, p0

    if-ne v0, v7, :cond_a

    .line 655
    invoke-static {v9}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->getPlaylistItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmtt/LMTTItem;

    move-result-object v14

    goto :goto_6

    .line 657
    :cond_a
    invoke-static {v9}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->getContactItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmtt/LMTTItem;

    move-result-object v14

    goto :goto_6

    .line 676
    .end local v14    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    :cond_b
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->determineDeletes(Ljava/util/HashMap;Ljava/util/ArrayList;Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 684
    if-eqz v9, :cond_c

    .line 686
    :try_start_c
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_9

    :cond_c
    :goto_7
    move v7, v10

    .line 693
    goto/16 :goto_3

    .line 687
    :catch_9
    move-exception v12

    .line 688
    .restart local v12    # "ex":Ljava/lang/Exception;
    const-string/jumbo v7, "VLG_EXCEPTION"

    invoke-static {v12}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 684
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "proj":[Ljava/lang/String;
    .end local v12    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v9, :cond_d

    .line 686
    :try_start_d
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a

    .line 689
    :cond_d
    :goto_8
    throw v7

    .line 687
    :catch_a
    move-exception v12

    .line 688
    .restart local v12    # "ex":Ljava/lang/Exception;
    const-string/jumbo v16, "VLG_EXCEPTION"

    invoke-static {v12}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8
.end method

.method private static getContactItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmtt/LMTTItem;
    .locals 15
    .param p0, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 709
    const/4 v0, 0x4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 710
    .local v9, "rawContactId":J
    const/4 v1, 0x0

    .line 711
    .local v1, "firstName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 712
    .local v2, "middleName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 713
    .local v3, "lastName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 714
    .local v5, "fullName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 715
    .local v6, "phoneticFirstName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 716
    .local v7, "phoneticMiddleName":Ljava/lang/String;
    const/4 v8, 0x0

    .line 717
    .local v8, "phoneticLastName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 720
    .local v4, "nickname":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 721
    .local v13, "rowRawContactId":J
    cmp-long v0, v13, v9

    if-eqz v0, :cond_1

    .line 723
    invoke-interface {p0}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 758
    :goto_0
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 767
    const/4 v0, 0x0

    .line 769
    :goto_1
    return-object v0

    .line 726
    :cond_1
    const/16 v0, 0x9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 727
    .local v12, "mimeType":Ljava/lang/String;
    const-string/jumbo v0, "vnd.android.cursor.item/nickname"

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728
    if-nez v4, :cond_2

    .line 729
    const/4 v0, 0x5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 756
    :cond_2
    :goto_2
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 731
    :cond_3
    const-string/jumbo v0, "vnd.android.cursor.item/name"

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 732
    if-nez v1, :cond_4

    .line 733
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 735
    :cond_4
    if-nez v2, :cond_5

    .line 736
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 738
    :cond_5
    if-nez v3, :cond_6

    .line 739
    const/4 v0, 0x3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 741
    :cond_6
    if-nez v5, :cond_7

    .line 742
    const/4 v0, 0x5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 744
    :cond_7
    if-nez v6, :cond_8

    .line 745
    const/4 v0, 0x6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 747
    :cond_8
    if-nez v7, :cond_9

    .line 748
    const/4 v0, 0x7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 750
    :cond_9
    if-nez v8, :cond_2

    .line 751
    const/16 v0, 0x8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    .line 769
    .end local v12    # "mimeType":Ljava/lang/String;
    :cond_a
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;

    sget-object v11, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->NOCHANGE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct/range {v0 .. v11}, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    goto :goto_1
.end method

.method private static getFolderName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 852
    if-nez p0, :cond_0

    .line 853
    const/4 v0, 0x0

    .line 855
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getPlaylistItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmtt/LMTTItem;
    .locals 6
    .param p0, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 793
    const/4 v2, 0x0

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 794
    .local v0, "id":I
    const/4 v2, 0x1

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 795
    .local v1, "title":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 796
    const/4 v2, 0x0

    .line 798
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;

    int-to-long v3, v0

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->NOCHANGE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;-><init>(Ljava/lang/String;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    goto :goto_0
.end method

.method private static getSongItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmtt/LMTTItem;
    .locals 13
    .param p0, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 775
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 776
    .local v12, "id":I
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 777
    .local v1, "title":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 778
    const/4 v0, 0x0

    .line 789
    :goto_0
    return-object v0

    .line 780
    :cond_0
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 781
    .local v2, "artist":Ljava/lang/String;
    const/4 v0, 0x3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 782
    .local v3, "composer":Ljava/lang/String;
    const/4 v0, 0x4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 783
    .local v4, "album":Ljava/lang/String;
    const/4 v0, 0x5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 784
    .local v6, "year":I
    const/4 v0, 0x6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 788
    .local v11, "dataStream":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 789
    .local v5, "genres":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;

    invoke-static {v11}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->getFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    int-to-long v8, v12

    sget-object v10, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->NOCHANGE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct/range {v0 .. v10}, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    goto :goto_0
.end method


# virtual methods
.method destroy()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 127
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mUploadHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 128
    return-void
.end method

.method fireUpdate(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZZ)V
    .locals 5
    .param p1, "lmttUpdateType"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .param p2, "skipDelay"    # Z
    .param p3, "clearLMTT"    # Z

    .prologue
    .line 115
    iget v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mLastReceivedRequestId:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mLastReceivedRequestId:I

    .line 116
    new-instance v2, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, p3, v3}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZLcom/vlingo/core/internal/lmtt/LMTTManager$1;)V

    .line 117
    .local v2, "newTask":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mUploadHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 118
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 119
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v3, "skip_delay"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    const-string/jumbo v3, "request_id"

    iget v4, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mLastReceivedRequestId:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 121
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 122
    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager;->mUploadHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 123
    return-void
.end method
