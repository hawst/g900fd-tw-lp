.class public Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
.super Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;
.source "PlayTTSTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isTTSAnyway:Z

.field private mIsSystemTts:Z

.field private mTTSText:Ljava/lang/String;

.field private mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

.field private mTts:Landroid/speech/tts/TextToSpeech;

.field private onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

.field private talkbackCheckHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Ljava/lang/String;)V
    .locals 1
    .param p1, "audioPlaybackListener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;-><init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Ljava/lang/String;Z)V

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "audioPlaybackListener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "isSystemTts"    # Z

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->isTTSAnyway:Z

    .line 41
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .line 42
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 43
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    .line 163
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->talkbackCheckHandler:Landroid/os/Handler;

    .line 58
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTTSText:Ljava/lang/String;

    .line 59
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 60
    iput-boolean p3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mIsSystemTts:Z

    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;-><init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "isSystemTts"    # Z

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;-><init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Ljava/lang/String;Z)V

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->playTTS()V

    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    .param p1, "x1"    # Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    return-object p1
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTTSText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->notifyFinished()V

    return-void
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)Landroid/speech/tts/TextToSpeech;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTts:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$602(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;)Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->notifyFinished()V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->notifyFinished()V

    return-void
.end method

.method private playTTS()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 114
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->isTTSAnyway()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTTSText:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/audio/TTSRequest;->getPrompt(Ljava/lang/String;Z)Lcom/vlingo/core/internal/audio/TTSRequest;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playAnyway(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 153
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 119
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$2;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$2;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 126
    :cond_1
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "TTS: voice prompts are off, ignoring \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTTSText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 129
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    if-eqz v1, :cond_2

    .line 130
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 131
    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    .line 134
    :cond_2
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    invoke-direct {v1, p0, v4}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$1;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    .line 135
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 136
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 139
    new-instance v1, Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 141
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->talkbackCheckHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 143
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_3
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$3;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$3;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V

    const-wide/16 v2, 0x3e8

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    goto/16 :goto_0
.end method


# virtual methods
.method public isSystemTts()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mIsSystemTts:Z

    return v0
.end method

.method public isTTSAnyway()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->isTTSAnyway:Z

    return v0
.end method

.method public onCancelled()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->cancel()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .line 76
    :cond_0
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->onCancelled()V

    .line 77
    return-void
.end method

.method public declared-synchronized run()V
    .locals 3

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTTSText:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 82
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 85
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->setCancelTurnTiming(Z)V

    .line 86
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startScoOnStartRecognition()V

    .line 87
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->setCancelTurnTiming(Z)V

    .line 88
    const-wide/16 v0, 0x7d0

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$1;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->runTaskOnBluetoothAudioOn(JLjava/lang/Runnable;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 96
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->playTTS()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setTTSAnyway(Z)V
    .locals 0
    .param p1, "ttsAnyway"    # Z

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->isTTSAnyway:Z

    .line 161
    return-void
.end method
