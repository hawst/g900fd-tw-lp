.class public Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;
.super Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
.source "KoreanPlusPTFContactScore.java"


# instance fields
.field private baseScore:I

.field private familiarityHonorific:Ljava/lang/String;

.field private hasSuffixes:Z

.field private postProposition:Ljava/lang/String;

.field private regexQuery:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "baseScore"    # I
    .param p2, "regexQuery"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "familiarityHonorific"    # Ljava/lang/String;
    .param p5, "postProposition"    # Ljava/lang/String;
    .param p6, "hasSuffixes"    # Z

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/scoring/ContactScore;-><init>()V

    .line 13
    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->title:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->familiarityHonorific:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->postProposition:Ljava/lang/String;

    .line 21
    iput p1, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->baseScore:I

    .line 22
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->regexQuery:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->title:Ljava/lang/String;

    .line 24
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->familiarityHonorific:Ljava/lang/String;

    .line 25
    iput-object p5, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->postProposition:Ljava/lang/String;

    .line 26
    iput-boolean p6, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->hasSuffixes:Z

    .line 27
    return-void
.end method

.method private matchAllowNull(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "a"    # Ljava/lang/String;
    .param p2, "b"    # Ljava/lang/String;

    .prologue
    .line 52
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 53
    :cond_0
    const/4 v0, 0x1

    .line 55
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public getScore(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 9
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "match"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v8, 0x1

    .line 31
    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->regexQuery:Ljava/lang/String;

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    iget-object v7, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 32
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v8, :cond_5

    .line 33
    :cond_0
    iget-object v6, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v8, :cond_4

    iget-object v4, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 34
    .local v4, "strippedQuery":Ljava/lang/String;
    :goto_0
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 35
    .local v1, "strippedExtras":Ljava/lang/String;
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 36
    .local v5, "strippedTitle":Ljava/lang/String;
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 37
    .local v2, "strippedFamiliarityHonorific":Ljava/lang/String;
    const/4 v6, 0x5

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "strippedPostProposition":Ljava/lang/String;
    if-eqz v4, :cond_1

    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->regexQuery:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    iget-object v6, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iget-object v7, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->regexQuery:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    :cond_2
    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->title:Ljava/lang/String;

    invoke-direct {p0, v6, v5}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->matchAllowNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->familiarityHonorific:Ljava/lang/String;

    invoke-direct {p0, v6, v2}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->matchAllowNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->postProposition:Ljava/lang/String;

    invoke-direct {p0, v6, v3}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->matchAllowNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-boolean v6, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->hasSuffixes:Z

    if-eqz v6, :cond_3

    invoke-static {v1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 43
    :cond_3
    iget v6, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->baseScore:I

    .line 47
    .end local v1    # "strippedExtras":Ljava/lang/String;
    .end local v2    # "strippedFamiliarityHonorific":Ljava/lang/String;
    .end local v3    # "strippedPostProposition":Ljava/lang/String;
    .end local v4    # "strippedQuery":Ljava/lang/String;
    .end local v5    # "strippedTitle":Ljava/lang/String;
    :goto_1
    return v6

    .line 33
    :cond_4
    invoke-virtual {v0, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 47
    :cond_5
    const/4 v6, 0x0

    goto :goto_1
.end method
