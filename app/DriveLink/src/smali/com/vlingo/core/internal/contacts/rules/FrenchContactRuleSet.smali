.class public Lcom/vlingo/core/internal/contacts/rules/FrenchContactRuleSet;
.super Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;
.source "FrenchContactRuleSet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;-><init>()V

    return-void
.end method


# virtual methods
.method public canProcess(Ljava/lang/String;)Z
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "fr-FR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .locals 3

    .prologue
    .line 13
    new-instance v0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;

    const/16 v1, 0x14

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;-><init>(II)V

    return-object v0
.end method

.method protected getPartialMatchScorer()Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lcom/vlingo/core/internal/contacts/scoring/FullyNormalizedSetMatchContactScore;

    const/16 v1, 0x5f

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/scoring/FullyNormalizedSetMatchContactScore;-><init>(II)V

    return-object v0
.end method
