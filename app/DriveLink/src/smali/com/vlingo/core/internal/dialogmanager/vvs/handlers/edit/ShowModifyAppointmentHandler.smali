.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowModifyAppointmentHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

.field private originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private getEvent(I)Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .locals 4
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CALENDAR_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 86
    .local v1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    if-nez v1, :cond_0

    .line 87
    const/4 v2, 0x0

    .line 91
    :goto_0
    return-object v2

    .line 90
    :cond_0
    if-eqz v1, :cond_1

    .line 91
    :try_start_0
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    goto :goto_0

    .line 93
    :cond_1
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "There is no events in cache"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    new-instance v2, Ljava/security/InvalidParameterException;

    const-string/jumbo v3, "There is no events in cache"

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Schedule Index passed in is not in the cache"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    new-instance v2, Ljava/security/InvalidParameterException;

    const-string/jumbo v3, "Schedule Index passed in is not in the cache"

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private sendAcceptedText()V
    .locals 18

    .prologue
    .line 119
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    if-eqz v1, :cond_2

    .line 120
    const/4 v8, 0x0

    .line 121
    .local v8, "attendees":Ljava/lang/String;
    const/16 v16, 0x0

    .line 122
    .local v16, "oldAttendees":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 123
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 125
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 126
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v16

    .line 128
    :cond_1
    new-instance v1, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginDate()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEndTime()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDuration()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v10}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v11}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginDate()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v13}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEndTime()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v14}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDuration()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v15}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v15

    const/16 v17, 0x0

    invoke-direct/range {v1 .. v17}, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 133
    .end local v8    # "attendees":Ljava/lang/String;
    .end local v16    # "oldAttendees":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private storeEvent()V
    .locals 3

    .prologue
    .line 105
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 106
    .local v0, "storedEvents":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CALENDAR_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 108
    return-void
.end method

.method private updateAppointment()V
    .locals 2

    .prologue
    .line 111
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->original(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->modified(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->queue()V

    .line 115
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->sendAcceptedText()V

    .line 116
    return-void
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 148
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 149
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 150
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 141
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 142
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 143
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->storeEvent()V

    .line 144
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 61
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 62
    const-string/jumbo v3, "id"

    const/4 v4, -0x1

    const/4 v5, 0x1

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v2

    .line 63
    .local v2, "index":I
    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->getEvent(I)Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 65
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    move-object v0, v3

    check-cast v0, Ljava/util/List;

    .line 67
    .local v0, "attendees":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->originalEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-static {p1, v3, v0}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->mergeChanges(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/schedule/ScheduleEvent;Ljava/util/List;)Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 69
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v3

    const-string/jumbo v4, "appointment"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 71
    const/4 v1, 0x0

    .line 72
    .local v1, "decor":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    const-string/jumbo v3, "confirm"

    invoke-static {p1, v3, v6, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeConfirmButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 75
    :cond_0
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-interface {p2, v3, v1, v4, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 77
    const-string/jumbo v3, "execute"

    invoke-static {p1, v3, v6, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 78
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->updateAppointment()V

    .line 80
    :cond_1
    return v6
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;-><init>()V

    .line 167
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 175
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    :goto_0
    return-void

    .line 168
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;-><init>()V

    .line 170
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 173
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method
