.class public abstract Lcom/vlingo/core/internal/settings/util/StringSetting;
.super Lcom/vlingo/core/internal/settings/util/Setting;
.source "StringSetting.java"


# static fields
.field public static final VALUE_EMPTY:Ljava/lang/String; = "__EMPTY__"


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 13
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/vlingo/core/internal/settings/util/Setting;-><init>(Ljava/lang/String;ILjava/lang/Object;Ljava/lang/String;)V

    .line 14
    return-void
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/StringSetting;->value:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/StringSetting;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/StringSetting;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 25
    if-nez p1, :cond_0

    .line 26
    const-string/jumbo p1, ""

    .line 27
    :cond_0
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/settings/util/StringSetting;->setValueInternal(Ljava/lang/Object;)V

    .line 28
    return-void
.end method
