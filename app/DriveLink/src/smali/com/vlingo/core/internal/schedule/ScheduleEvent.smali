.class public Lcom/vlingo/core/internal/schedule/ScheduleEvent;
.super Lcom/vlingo/core/internal/schedule/ScheduleEventBase;
.source "ScheduleEvent.java"


# instance fields
.field private final DEFAULT_DURATION:I

.field protected allDay:Z

.field private contactDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private isCreatingEvent:Z

.field private isReadOnly:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;-><init>()V

    .line 20
    const v0, 0x36ee80

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->DEFAULT_DURATION:I

    .line 16
    return-void
.end method

.method public static clone(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 38
    .local v0, "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 39
    .local v4, "t":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->clone()Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v1

    .line 40
    .local v1, "copy":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    .end local v0    # "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    .end local v1    # "copy":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "t":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    :catch_0
    move-exception v2

    .line 44
    .local v2, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "List cloning unsupported"

    invoke-direct {v5, v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 42
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v0
.end method

.method private contactDataListToString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 54
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 57
    iget-object v3, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 58
    .local v0, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-nez v3, :cond_0

    .line 59
    const-string/jumbo v3, "List<ContactData>{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    :cond_0
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactData;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 64
    .end local v0    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_2

    .line 65
    const-string/jumbo v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public addContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V
    .locals 1
    .param p1, "data"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 113
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_1
    return-void
.end method

.method public bridge synthetic clone()Lcom/vlingo/core/internal/schedule/EventBase;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->clone()Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .locals 2

    .prologue
    .line 30
    invoke-super {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->clone()Lcom/vlingo/core/internal/schedule/ScheduleEventBase;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 31
    .local v0, "clone":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/ContactData;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    .line 32
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/vlingo/core/internal/schedule/ScheduleEventBase;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->clone()Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->clone()Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v0

    return-object v0
.end method

.method public getAllDay()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->allDay:Z

    return v0
.end method

.method public getAttendeeNames()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 136
    .local v0, "attendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 137
    iget-object v4, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 138
    .local v1, "contact":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v4, v1, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v3, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 139
    .local v3, "name":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 140
    :cond_0
    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactData;->getNormalizedAddress()Ljava/lang/String;

    move-result-object v3

    .line 142
    :cond_1
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    .end local v1    # "contact":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "name":Ljava/lang/String;
    :cond_2
    return-object v0
.end method

.method public getContactDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    return-object v0
.end method

.method public getDuration()J
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public isCreatingEvent()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->isCreatingEvent:Z

    return v0
.end method

.method public isReadOnly()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->isReadOnly:Z

    return v0
.end method

.method public resetContactData()V
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    .line 122
    return-void
.end method

.method public setAllDay(Z)V
    .locals 0
    .param p1, "allDay"    # Z

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->allDay:Z

    .line 126
    return-void
.end method

.method public setContactDataList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "contactDataList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataList:Ljava/util/List;

    .line 132
    return-void
.end method

.method public setCreatingEvent(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->isCreatingEvent:Z

    .line 77
    return-void
.end method

.method public setDuration(I)V
    .locals 5
    .param p1, "duration"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 86
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 87
    if-lez p1, :cond_2

    .line 88
    new-instance v0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v1

    int-to-long v3, p1

    add-long/2addr v1, v3

    invoke-direct {v0, p0, v1, v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    .line 98
    :cond_1
    :goto_0
    return-void

    .line 93
    :cond_2
    new-instance v0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v1

    const-wide/32 v3, 0x36ee80

    add-long/2addr v1, v3

    invoke-direct {v0, p0, v1, v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    goto :goto_0
.end method

.method protected setEndTime()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 151
    return-void
.end method

.method public setReadOnly(Z)V
    .locals 0
    .param p1, "readOnly"    # Z

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->isReadOnly:Z

    .line 159
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ScheduleEvent{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->contactDataListToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
