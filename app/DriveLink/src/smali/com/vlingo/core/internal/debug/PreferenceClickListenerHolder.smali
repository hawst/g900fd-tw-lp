.class public Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;
.super Ljava/util/HashSet;
.source "PreferenceClickListenerHolder.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Landroid/preference/Preference$OnPreferenceClickListener;",
        ">;",
        "Landroid/preference/Preference$OnPreferenceClickListener;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 17
    const/4 v2, 0x1

    .line 18
    .local v2, "result":Z
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/PreferenceClickListenerHolder;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/Preference$OnPreferenceClickListener;

    .line 19
    .local v1, "listener":Landroid/preference/Preference$OnPreferenceClickListener;
    invoke-interface {v1, p1}, Landroid/preference/Preference$OnPreferenceClickListener;->onPreferenceClick(Landroid/preference/Preference;)Z

    move-result v3

    and-int/2addr v2, v3

    goto :goto_0

    .line 20
    .end local v1    # "listener":Landroid/preference/Preference$OnPreferenceClickListener;
    :cond_0
    return v2
.end method
