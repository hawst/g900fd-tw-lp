.class public final enum Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;
.super Ljava/lang/Enum;
.source "Alarm.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/Alarm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AlarmStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

.field public static final enum ACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

.field public static final enum INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

.field public static final enum SNOOZE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

.field public static final enum SUBDUE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    const-string/jumbo v1, "INACTIVE"

    invoke-direct {v0, v1, v2, v2}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    .line 37
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    const-string/jumbo v1, "ACTIVE"

    invoke-direct {v0, v1, v3, v3}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    const-string/jumbo v1, "SNOOZE"

    invoke-direct {v0, v1, v4, v4}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->SNOOZE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    .line 39
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    const-string/jumbo v1, "SUBDUE"

    invoke-direct {v0, v1, v5, v5}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->SUBDUE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->SNOOZE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->SUBDUE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->$VALUES:[Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput p3, p0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->value:I

    .line 44
    return-void
.end method

.method public static fromInt(I)Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 51
    packed-switch p0, :pswitch_data_0

    .line 59
    sget-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    :goto_0
    return-object v0

    .line 53
    :pswitch_0
    sget-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    goto :goto_0

    .line 55
    :pswitch_1
    sget-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->SNOOZE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    goto :goto_0

    .line 57
    :pswitch_2
    sget-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->SUBDUE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    goto :goto_0

    .line 51
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->$VALUES:[Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->value:I

    return v0
.end method
