.class Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;
.super Ljava/lang/Object;
.source "SafeReaderEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->onNewSafeReaderAlert(Ljava/util/LinkedList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

.field final synthetic val$alerts:Ljava/util/LinkedList;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;Ljava/util/LinkedList;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    iput-object p2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->val$alerts:Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const-wide/16 v9, 0x64

    .line 255
    iget-object v6, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    monitor-enter v6

    .line 256
    :try_start_0
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSafeReaderOn()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    iget-boolean v5, v5, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isPaused:Z

    if-nez v5, :cond_5

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMessagingLocked()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isBlockingMode()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 258
    const/4 v2, 0x0

    .line 261
    .local v2, "currentMsgListeners":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;>;"
    const-wide/16 v0, 0x0

    .line 262
    .local v0, "currentDelay":J
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    iget-boolean v5, v5, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->useDelay:Z

    if-eqz v5, :cond_1

    .line 263
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    iget-object v5, v5, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->remoteMsgListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .line 265
    .local v4, "listener":Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
    invoke-interface {v4}, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;->readoutDelay()J

    move-result-wide v7

    invoke-static {v0, v1, v7, v8}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    goto :goto_0

    .line 298
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "listener":Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
    :cond_0
    sub-long/2addr v0, v9

    .line 300
    const-wide/16 v7, 0x64

    :try_start_1
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 272
    :cond_1
    :goto_1
    :try_start_2
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    iget-object v5, v5, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->remoteMsgListeners:Ljava/util/List;

    invoke-virtual {v5, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 273
    if-nez v2, :cond_2

    .line 274
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    .end local v2    # "currentMsgListeners":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;>;"
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    iget-object v5, v5, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->remoteMsgListeners:Ljava/util/List;

    invoke-direct {v2, v5}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    .line 283
    .restart local v2    # "currentMsgListeners":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;>;"
    :cond_2
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    iget-object v5, v5, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->remoteMsgListeners:Ljava/util/List;

    invoke-virtual {v5, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-wide/16 v7, 0x0

    cmp-long v5, v0, v7

    if-gtz v5, :cond_0

    .line 286
    :cond_3
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    iget-object v5, v5, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->remoteMsgListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .line 289
    .restart local v4    # "listener":Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSilenced()Z

    move-result v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;->setSilentMode(Z)V

    .line 290
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->val$alerts:Ljava/util/LinkedList;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;->handleAlert(Ljava/util/LinkedList;)V

    goto :goto_2

    .line 312
    .end local v0    # "currentDelay":J
    .end local v2    # "currentMsgListeners":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "listener":Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 292
    .restart local v0    # "currentDelay":J
    .restart local v2    # "currentMsgListeners":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    const-wide/16 v0, 0x0

    .line 293
    :try_start_3
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    const/4 v7, 0x0

    iput-boolean v7, v5, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->useDelay:Z

    .line 294
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    # getter for: Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertQueue:Ljava/util/LinkedList;
    invoke-static {v5}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->access$100(Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;)Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->clear()V

    .line 312
    .end local v0    # "currentDelay":J
    .end local v2    # "currentMsgListeners":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :goto_3
    monitor-exit v6

    .line 313
    return-void

    .line 307
    :cond_5
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    # getter for: Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertQueue:Ljava/util/LinkedList;
    invoke-static {v5}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->access$100(Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;)Ljava/util/LinkedList;

    move-result-object v5

    if-nez v5, :cond_6

    .line 308
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    # setter for: Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertQueue:Ljava/util/LinkedList;
    invoke-static {v5, v7}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->access$102(Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;Ljava/util/LinkedList;)Ljava/util/LinkedList;

    .line 310
    :cond_6
    iget-object v5, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    # getter for: Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertQueue:Ljava/util/LinkedList;
    invoke-static {v5}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->access$100(Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;)Ljava/util/LinkedList;

    move-result-object v5

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;->val$alerts:Ljava/util/LinkedList;

    invoke-virtual {v5, v7, v8}, Ljava/util/LinkedList;->addAll(ILjava/util/Collection;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 301
    .restart local v0    # "currentDelay":J
    .restart local v2    # "currentMsgListeners":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;>;"
    :catch_0
    move-exception v5

    goto :goto_1
.end method
