.class public Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;
.super Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
.source "DefaultLocalSearchListing.java"


# static fields
.field public static final FIELD_ADDR_CITY:Ljava/lang/String; = "City"

.field public static final FIELD_ADDR_STATE:Ljava/lang/String; = "State"

.field public static final FIELD_ADDR_STREET_NAME:Ljava/lang/String; = "StName"

.field public static final FIELD_ADDR_STREET_NUMBER:Ljava/lang/String; = "StNum"

.field public static final FIELD_ADDR_ZIP:Ljava/lang/String; = "Zip"

.field public static final FIELD_BUSINESS_HOURS:Ljava/lang/String; = "BizHrs"

.field public static final FIELD_CALL_PAYABLE:Ljava/lang/String; = "CallPayable"

.field public static final FIELD_CAPTION:Ljava/lang/String; = "Caption"

.field public static final FIELD_CATEGORY:Ljava/lang/String; = "Category"

.field public static final FIELD_CLICK_PAYABLE:Ljava/lang/String; = "ClickPayable"

.field public static final FIELD_CLICK_URL:Ljava/lang/String; = "ClickUrl"

.field public static final FIELD_DISTANCE:Ljava/lang/String; = "Dist"

.field public static final FIELD_IMAGE_URL:Ljava/lang/String; = "ImgUrl"

.field public static final FIELD_LATITUDE:Ljava/lang/String; = "Lat"

.field public static final FIELD_LISTING_ID:Ljava/lang/String; = "ListingID"

.field public static final FIELD_LONGITUDE:Ljava/lang/String; = "Lon"

.field public static final FIELD_NAME:Ljava/lang/String; = "Name"

.field public static final FIELD_PHONE_NUMBER:Ljava/lang/String; = "PhoneNumber"

.field public static final FIELD_PROVIDER:Ljava/lang/String; = "Provider"

.field public static final FIELD_RATING:Ljava/lang/String; = "Rating"

.field public static final FIELD_RESERVE_URL:Ljava/lang/String; = "ReservationURL"

.field public static final FIELD_REVIEW_AUTHOR:Ljava/lang/String; = "Author"

.field public static final FIELD_REVIEW_BODY:Ljava/lang/String; = "Body"

.field public static final FIELD_REVIEW_COUNT:Ljava/lang/String; = "RevCnt"

.field public static final FIELD_REVIEW_DATE:Ljava/lang/String; = "Date"

.field public static final FIELD_REVIEW_ID:Ljava/lang/String; = "Id"

.field public static final FIELD_REVIEW_RATING:Ljava/lang/String; = "Rating"

.field public static final FIELD_REVIEW_TITLE:Ljava/lang/String; = "Title"

.field public static final FIELD_SYNOPSIS:Ljava/lang/String; = "Synopsis"

.field public static final FIELD_TEASER:Ljava/lang/String; = "Teaser"

.field public static final FIELD_TYPE:Ljava/lang/String; = "Type"

.field public static final FIELD_URL:Ljava/lang/String; = "Url"

.field public static final TYPE_ORGANIC:Ljava/lang/String; = "Reg"

.field public static final TYPE_SPONSORED:Ljava/lang/String; = "Spon"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "valueBusinessID"    # Ljava/lang/String;

    .prologue
    .line 54
    const-string/jumbo v0, "ListingID"

    invoke-direct {p0, v0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method private getAddressLine1(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 163
    const-string/jumbo v0, "StName"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    const-string/jumbo v0, "StNum"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    const-string/jumbo v0, "StNum"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    const-string/jumbo v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    :cond_0
    const-string/jumbo v0, "StName"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    :cond_1
    return-void
.end method

.method private getAddressLine2(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 173
    const-string/jumbo v0, "City"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 175
    const-string/jumbo v0, ", "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    :cond_0
    const-string/jumbo v0, "City"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    :cond_1
    const-string/jumbo v0, "State"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 180
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 181
    const-string/jumbo v0, ", "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    :cond_2
    const-string/jumbo v0, "State"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    :cond_3
    return-void
.end method


# virtual methods
.method public getAddressLine1()Ljava/lang/String;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getAddressLine1(Ljava/lang/StringBuilder;)V

    .line 153
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getAddressLine2()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getAddressLine2(Ljava/lang/StringBuilder;)V

    .line 159
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getCaption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    const-string/jumbo v0, "Caption"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCityState()Ljava/lang/String;
    .locals 2

    .prologue
    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "City"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 137
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    :cond_0
    const-string/jumbo v1, "City"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    const-string/jumbo v1, "State"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 141
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 142
    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    :cond_1
    const-string/jumbo v1, "State"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getClickUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    const-string/jumbo v0, "ClickUrl"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDistanceString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 96
    const-string/jumbo v4, "Dist"

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 97
    const-string/jumbo v4, "Dist"

    const-wide/high16 v5, -0x4010000000000000L    # -1.0

    invoke-virtual {p0, v4, v5, v6}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    .line 98
    .local v0, "distance":D
    new-instance v2, Ljava/text/DecimalFormat;

    const-string/jumbo v4, "######.##"

    invoke-direct {v2, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 102
    .local v2, "distanceFormat":Ljava/text/DecimalFormat;
    const-string/jumbo v3, " mi"

    .line 103
    .local v3, "units":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 105
    .end local v0    # "distance":D
    .end local v2    # "distanceFormat":Ljava/text/DecimalFormat;
    .end local v3    # "units":Ljava/lang/String;
    :goto_0
    return-object v4

    :cond_0
    const-string/jumbo v4, ""

    goto :goto_0
.end method

.method public getFullAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "StName"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    const-string/jumbo v1, "StNum"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    const-string/jumbo v1, "StNum"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    :cond_0
    const-string/jumbo v1, "StName"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    :cond_1
    const-string/jumbo v1, "City"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 118
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 119
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_2
    const-string/jumbo v1, "City"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string/jumbo v1, "State"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 123
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 124
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_3
    const-string/jumbo v1, "State"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getLatitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method public getListingID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string/jumbo v0, "ListingID"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLongitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const-string/jumbo v0, "Name"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    const-string/jumbo v0, "PhoneNumber"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneNumberFormatted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    const-string/jumbo v0, "Provider"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRating()D
    .locals 3

    .prologue
    .line 74
    const-string/jumbo v0, "Rating"

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public getReserveUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    const-string/jumbo v0, "ReservationURL"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReviewCount()I
    .locals 2

    .prologue
    .line 70
    const-string/jumbo v0, "RevCnt"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getInteger(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getSynopsis()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    const-string/jumbo v0, "Synopsis"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    const-string/jumbo v0, "Url"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isOrganic()Z
    .locals 2

    .prologue
    .line 66
    const-string/jumbo v0, "Reg"

    const-string/jumbo v1, "Type"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSponsored()Z
    .locals 2

    .prologue
    .line 62
    const-string/jumbo v0, "Spon"

    const-string/jumbo v1, "Type"

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, " LocalSearchListing ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 213
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "\n Name :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    const-string/jumbo v1, "\n AddressLine1 :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getAddressLine1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    const-string/jumbo v1, "\n AddressLine2 :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getAddressLine2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    const-string/jumbo v1, "\n CityState :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getCityState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    const-string/jumbo v1, "\n FullAddress :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getFullAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    const-string/jumbo v1, "\n DistanceString :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getDistanceString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    const-string/jumbo v1, "\n ListingID :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getListingID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    const-string/jumbo v1, "\n PhoneNumber :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    const-string/jumbo v1, "\n Rating :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getRating()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 222
    const-string/jumbo v1, "\n ReviewCount :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getReviewCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 223
    const-string/jumbo v1, "\n Reviews : {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->getReviews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    const-string/jumbo v1, "\n hasReviews :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->hasReviews()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 225
    const-string/jumbo v1, "\n isOrganic :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->isOrganic()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 226
    const-string/jumbo v1, "\n isSponsored :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->isSponsored()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 227
    const-string/jumbo v1, "\n areMoreDetailsAvailable :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/DefaultLocalSearchListing;->areMoreDetailsAvailable()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 229
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
