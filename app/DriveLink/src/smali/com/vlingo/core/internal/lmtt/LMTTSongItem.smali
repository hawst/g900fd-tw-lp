.class Lcom/vlingo/core/internal/lmtt/LMTTSongItem;
.super Lcom/vlingo/core/internal/lmtt/LMTTItem;
.source "LMTTSongItem.java"


# instance fields
.field album:Ljava/lang/String;

.field artist:Ljava/lang/String;

.field composer:Ljava/lang/String;

.field folder:Ljava/lang/String;

.field genre:Ljava/lang/String;

.field title:Ljava/lang/String;

.field year:I


# direct methods
.method constructor <init>(JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 11
    .param p1, "uid"    # J
    .param p3, "changeType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    const/4 v1, 0x0

    .line 36
    const/4 v6, -0x1

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    move-wide v8, p1

    move-object v10, p3

    invoke-direct/range {v0 .. v10}, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 37
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;
    .param p3, "composer"    # Ljava/lang/String;
    .param p4, "album"    # Ljava/lang/String;
    .param p5, "genre"    # Ljava/lang/String;
    .param p6, "year"    # I
    .param p7, "folder"    # Ljava/lang/String;
    .param p8, "uid"    # J
    .param p10, "changeType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 20
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-direct {p0, v0, p8, p9, p10}, Lcom/vlingo/core/internal/lmtt/LMTTItem;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V

    .line 22
    if-nez p1, :cond_0

    .line 23
    const-string/jumbo p1, ""

    .line 26
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->title:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->artist:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->composer:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->album:Ljava/lang/String;

    .line 30
    iput-object p5, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->genre:Ljava/lang/String;

    .line 31
    iput p6, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->year:I

    .line 32
    iput-object p7, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->folder:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public hashCode()I
    .locals 3

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->artist:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->composer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->album:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->genre:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->year:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->folder:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "LMTTSongItem | uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->uid:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | changeType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->changeType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
