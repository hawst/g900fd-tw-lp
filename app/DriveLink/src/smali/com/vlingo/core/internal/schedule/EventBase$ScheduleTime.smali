.class public Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;
.super Ljava/lang/Object;
.source "EventBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/schedule/EventBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScheduleTime"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/schedule/EventBase;

.field private time:J


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V
    .locals 0
    .param p2, "time"    # J

    .prologue
    .line 222
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->this$0:Lcom/vlingo/core/internal/schedule/EventBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    iput-wide p2, p0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->time:J

    .line 224
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/schedule/EventBase;Ljava/lang/String;)V
    .locals 2
    .param p2, "date"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 230
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->this$0:Lcom/vlingo/core/internal/schedule/EventBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    invoke-static {p2}, Lcom/vlingo/core/internal/schedule/DateUtil;->getDateFromCanonicalDateAndTimeString(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->time:J

    .line 232
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/schedule/EventBase;Ljava/util/Date;)V
    .locals 2
    .param p2, "date"    # Ljava/util/Date;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->this$0:Lcom/vlingo/core/internal/schedule/EventBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->time:J

    .line 228
    return-void
.end method


# virtual methods
.method public getDate()Ljava/util/Date;
    .locals 3

    .prologue
    .line 249
    new-instance v0, Ljava/util/Date;

    iget-wide v1, p0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->time:J

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getDateStr()Ljava/lang/String;
    .locals 6

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getDate()Ljava/util/Date;

    move-result-object v1

    .line 240
    .local v1, "dateVal":Ljava/util/Date;
    const-string/jumbo v2, "%04d-%02d-%02d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/util/Date;->getYear()I

    move-result v5

    add-int/lit16 v5, v5, 0x76c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v1}, Ljava/util/Date;->getMonth()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v1}, Ljava/util/Date;->getDate()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "date":Ljava/lang/String;
    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 245
    iget-wide v0, p0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->time:J

    return-wide v0
.end method

.method public getTimeStr(Z)Ljava/lang/String;
    .locals 3
    .param p1, "is12Format"    # Z

    .prologue
    .line 235
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->this$0:Lcom/vlingo/core/internal/schedule/EventBase;

    iget-wide v1, p0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->time:J

    invoke-virtual {v0, v1, v2, p1}, Lcom/vlingo/core/internal/schedule/EventBase;->formatTime(JZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
