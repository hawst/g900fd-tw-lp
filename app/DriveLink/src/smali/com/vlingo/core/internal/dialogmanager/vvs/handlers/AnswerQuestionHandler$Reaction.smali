.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;
.super Ljava/lang/Object;
.source "AnswerQuestionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Reaction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private answer:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field protected turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 0
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p3, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogTurn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 218
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;, "Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction<TT;>;"
    .local p1, "answer":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;->answer:Ljava/lang/Object;

    .line 220
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 221
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 222
    return-void
.end method


# virtual methods
.method public getAnswer()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 225
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;, "Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;->answer:Ljava/lang/Object;

    return-object v0
.end method

.method public getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1

    .prologue
    .line 229
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;, "Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    return-object v0
.end method
