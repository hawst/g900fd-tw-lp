.class public Lcom/vlingo/core/internal/weather/WeatherElement;
.super Ljava/lang/Object;
.source "WeatherElement.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0xa9c3991fb564f99L


# instance fields
.field private attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private child:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/weather/WeatherElement;",
            ">;"
        }
    .end annotation
.end field

.field private errorMessage:Ljava/lang/String;

.field private final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "nm"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->attributes:Ljava/util/Map;

    .line 34
    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    .line 35
    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->errorMessage:Ljava/lang/String;

    .line 42
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->name:Ljava/lang/String;

    .line 43
    return-void
.end method

.method private setChild(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/weather/WeatherElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "chld":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/weather/WeatherElement;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    .line 141
    return-void
.end method


# virtual methods
.method public addAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 46
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 47
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "key OR value of attribute cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    return-void
.end method

.method public addChild(Lcom/vlingo/core/internal/weather/WeatherElement;)Z
    .locals 1
    .param p1, "chld"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected clone()Lcom/vlingo/core/internal/weather/WeatherElement;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    new-instance v0, Lcom/vlingo/core/internal/weather/WeatherElement;

    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 129
    .local v0, "clone":Lcom/vlingo/core/internal/weather/WeatherElement;
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->attributes:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->setAttributes(Ljava/util/Map;)V

    .line 130
    iput-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->attributes:Ljava/util/Map;

    .line 131
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->setChild(Ljava/util/List;)V

    .line 132
    iput-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    .line 133
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/vlingo/core/internal/weather/WeatherElement;->clone()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v0

    return-object v0
.end method

.method public findChildWithAttr(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;
    .locals 5
    .param p1, "attrName"    # Ljava/lang/String;
    .param p2, "targetAttrValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
        }
    .end annotation

    .prologue
    .line 169
    invoke-static {p2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 170
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 171
    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v0

    .line 172
    .local v0, "childElement":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 173
    return-object v0

    .line 170
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 177
    .end local v0    # "childElement":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v1    # "index":I
    :cond_1
    new-instance v2, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "child with \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\' of \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\' not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Ljava/lang/String;)V

    throw v2
.end method

.method public findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;
    .locals 5
    .param p1, "childName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
        }
    .end annotation

    .prologue
    .line 159
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 160
    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v0

    .line 161
    .local v0, "childElement":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 162
    return-object v0

    .line 159
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 165
    .end local v0    # "childElement":Lcom/vlingo/core/internal/weather/WeatherElement;
    :cond_1
    new-instance v2, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "named child \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\' not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Ljava/lang/String;)V

    throw v2
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->attributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->attributes:Ljava/util/Map;

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->attributes:Ljava/util/Map;

    return-object v0
.end method

.method public getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "val":Lcom/vlingo/core/internal/weather/WeatherElement;
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 68
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "val":Lcom/vlingo/core/internal/weather/WeatherElement;
    check-cast v0, Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 70
    .restart local v0    # "val":Lcom/vlingo/core/internal/weather/WeatherElement;
    :cond_0
    return-object v0
.end method

.method public getChildCount()I
    .locals 2

    .prologue
    .line 77
    const/4 v0, 0x0

    .line 78
    .local v0, "count":I
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 79
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 81
    :cond_0
    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->name:Ljava/lang/String;

    return-object v0
.end method

.method public isDefaultLocation()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 182
    :try_start_0
    const-string/jumbo v3, "WeatherLocation"

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const-string/jumbo v4, "Location"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v3

    const-string/jumbo v4, "SelectedLocationType"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 185
    .local v1, "selectedLocationType":Ljava/lang/String;
    const-string/jumbo v3, "DEFAULT"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "USERCACHEDLOCATION"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "USERPHONENUMBER"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 189
    .end local v1    # "selectedLocationType":Ljava/lang/String;
    :cond_1
    :goto_0
    return v2

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    goto :goto_0
.end method

.method public setAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->attributes:Ljava/util/Map;

    .line 117
    return-void
.end method

.method public setErrorMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->errorMessage:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\nName "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 87
    .local v0, "bldr":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    const-string/jumbo v2, "\n[Attributes] [ "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->attributes:Ljava/util/Map;

    if-eqz v2, :cond_0

    .line 90
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->attributes:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    :cond_0
    const-string/jumbo v2, "]\n Children ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 94
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 95
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherElement;->child:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 100
    .end local v1    # "i":I
    :cond_1
    const-string/jumbo v2, "]\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
