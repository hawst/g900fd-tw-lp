.class public final enum Lcom/vlingo/core/internal/ResourceIdProvider$string;
.super Ljava/lang/Enum;
.source "ResourceIdProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/ResourceIdProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "string"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/ResourceIdProvider$string;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_address_book_is_empty:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_alarm_set:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_alert_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_and:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_answer_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_already_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_already_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_event_save_confirm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_event_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_redial_confirm_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_redial_confirm_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_safereader_hidden_message_body:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_sms_error_help_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_sms_error_sending:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_sms_message_empty_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_sms_send_confirm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_sms_speak_msg_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_sms_text_who:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_social_final_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_social_prompt_ex1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_social_service_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_social_status_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_task_save_cancel_update_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_ALBUM_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_EVENT_SAY_TITLE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_GOTO_URL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_LAUNCHAPP_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_MAP_OF:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_MEMO_SAVED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_MUSIC_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NAVIGATE_TO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_ALBUMMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_ANYMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_ARTISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_MATCH_DEMAND_CALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_MATCH_DEMAND_MESSAGE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_PLAYLISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_SPOKEN_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_SPOKEN_APPMATCH_DEMAND_FOR_CAMERA_ONLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_NO_TITLEMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_MULTIPLE_NEW_MESSAGES:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MSG_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_MSG_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_SMS_FROM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SAFEREADER_NO_REPLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SCHEDULE_EVENTS:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SCHEDULE_NO_EVENTS:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_SMS_SENT_CONFIRM_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_TASK_SAY_TITLE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_TITLE_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_VD_CALLING_CONFIRM_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_VD_MULTIPLE_CONTACTS_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_VD_MULTIPLE_TYPES_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_tts_WHICH_CONTACT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_car_util_loading:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_checkEventJapaneseMoreTenYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_checkEventOneYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_checkEventRussianTwoThreeFourYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_checkEventTitleTimeDetailBrief:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_checkEventYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_checkEventYouHaveOnDate:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_checkEventYouHaveToday:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_colon:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_address:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_address_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_birthday_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_email:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_email_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_home_address_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_home_email_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_home_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_mobile_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_phone_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_phone_number_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_work_address_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_work_email_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contact_work_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_contacts_no_match_openquote:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_cradle_date:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_default_alarm_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_ellipsis:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_format_time_AM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_format_time_PM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_local_search_blank_request_message_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_local_search_blank_request_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_localsearch:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_localsearch_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_localsearch_no_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_localsearch_provider_dianping:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_map_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_memo_confirm_delete:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_memo_multiple_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_memo_not_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_message_none_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_message_none_from_sender_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_message_reading_none:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_message_reading_none_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_message_reading_preface:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_message_reading_single_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_message_sending:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_message_sending_readout:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_mic_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_multiple_applications:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_multiple_contacts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_multiple_phone_numbers:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_multiple_phone_numbers2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_music_play_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_music_play_playlist:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_music_playing_calm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_music_playing_exciting:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_music_playing_joyful:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_music_playing_passionate:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_nav_home_prompt_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_nav_home_prompt_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_navigate_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_navigation_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_network_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_no_call_log:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_no_memo_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_no_memo_saved_about:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_no_more_messages_regular:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_no_more_messages_verbose:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_not_detected_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_opening_app:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_permission_internet_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_phone_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_phone_type_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_phone_type_mobile:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_phone_type_other:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_phone_type_work:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_playing_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_qa_more:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_qa_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_message_nocall_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_message_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_message_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_sender_command_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_sender_dm_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_sender_info_verbose_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_sender_info_verbose_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_sender_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_sender_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_multi_sender_overflow_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_new_messages_for_russian_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_new_messages_for_russian_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_new_messages_for_russian_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_no_match_found_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_no_match_found_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_hidden:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_hidden_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_hidden_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_hidden_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_hidden_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_hidden_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_initial_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_next:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_next_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_next_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_next_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_next_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_next_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_readout_single_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_redial:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_safe_reader_default_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_safereader_enabled:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_safereader_new_sms_from:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_safereader_notif_reading_ticker:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_safereader_notif_reading_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_safereader_notif_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_safereader_notif_title_silent:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_safereader_subject:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_say_command:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_schedule_all_day:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_schedule_to:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_search_web_label_button:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_semicolon:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_settings_default_support_url:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_single_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_api_err_auth1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_api_err_auth2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_api_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_api_twitter_err_login:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_api_twitter_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_api_twitter_update_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_api_weibo_err_login:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_api_weibo_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_api_weibo_update_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_err_msg1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_err_msg2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_login_to_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_login_to_network_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_login_to_twitter_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_login_to_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_logout_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_logout_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_logout_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_logout_twitter_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_logout_weibo:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_logout_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_no_network:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_no_status:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_status_updated:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_too_long:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_social_update_failed:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_time_at_present:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_tomorrow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_tts_NO_ANS_GOOGLE_NOW_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_tts_NO_ANS_WEB_SEARCH_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_tts_NO_ANS_WEB_SEARCH_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_tts_NO_ANS_WEB_SEARCH_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_tts_NO_ANS_WEB_SEARCH_4:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_tts_local_fallback_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_tts_local_required_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_unknown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_NAME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_BAIDU_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_BING_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_BING_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_DAUM_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_DAUM_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_GOOGLE_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT_US:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_NAVER_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_NAVER_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_YAHOO_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_util_WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_voice_recognition_service_not_thru_iux_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_voicedial_call_name_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_voicevideodial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_voicevideodial_call_name_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_volume_down:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_volume_is_already_muted:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_volume_is_already_unmuted:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_volume_is_at_maximum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_volume_is_at_minimum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_volume_up:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_wcis_social_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_wcis_social_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_wcis_social_weibo:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_weather_current:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_weather_date_display:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_weather_date_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_weather_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_weather_general:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_weather_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_weather_plus_seven:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_weather_with_locatin:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_weibo_dialog_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_what_is_the_weather_date_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_what_is_the_weather_date_var_location_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_what_is_the_weather_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_what_is_the_weather_today_location_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_who_would_you_like_to_call:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum core_wifi_setting_change_on_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum playlists_quicklist_default_value:Lcom/vlingo/core/internal/ResourceIdProvider$string;

.field public static final enum playlists_quicklist_default_value1:Lcom/vlingo/core/internal/ResourceIdProvider$string;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_sms_error_help_tts"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_error_help_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 68
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_sms_error_sending"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_error_sending:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 69
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_sms_send_confirm"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_send_confirm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 70
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_sms_speak_msg_tts"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_speak_msg_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 71
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_sms_text_who"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_text_who:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 72
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SMS_SENT_CONFIRM_DEMAND"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SMS_SENT_CONFIRM_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 73
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_TASK_CANCELLED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 74
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_VD_MULTIPLE_CONTACTS_DEMAND"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_VD_MULTIPLE_CONTACTS_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 75
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_VD_MULTIPLE_TYPES_DEMAND"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_VD_MULTIPLE_TYPES_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 76
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contacts_no_match_openquote"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_no_match_openquote:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 77
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_nav_home_prompt_shown"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 78
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_nav_home_prompt_spoken"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 79
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_navigate_home"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_navigate_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 80
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_navigation_default_location"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_navigation_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 81
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_map_default_location"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_map_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 83
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_safe_reader_default_error"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safe_reader_default_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 84
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_safereader_enabled"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_enabled:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 85
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_safereader_notif_reading_title"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_reading_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 86
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_safereader_notif_reading_ticker"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_reading_ticker:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 87
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_safereader_notif_title"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 88
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_safereader_subject"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_subject:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 89
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_safereader_new_sms_from"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_new_sms_from:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 91
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_settings_default_support_url"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_settings_default_support_url:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 92
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_answer_prompt"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_answer_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 93
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_alert_message"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_alert_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 94
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_NAME_DEFAULT"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_NAME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 95
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_HOME_DEFAULT"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 96
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_DEFAULT"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 97
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_BING_HOME_DEFAULT"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BING_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 98
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_BING_DEFAULT"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BING_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 99
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_YAHOO_HOME_DEFAULT"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 100
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_YAHOO_DEFAULT"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_YAHOO_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 101
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_BAIDU_HOME_DEFAULT"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 102
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_BAIDU_DEFAULT"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BAIDU_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 103
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT_US"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT_US:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 104
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_GOOGLE_DEFAULT"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 105
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 106
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_NAVER_HOME_DEFAULT"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_NAVER_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 107
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_NAVER_DEFAULT"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_NAVER_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 108
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_DAUM_HOME_DEFAULT"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DAUM_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 109
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_util_WEB_SEARCH_URL_DAUM_DEFAULT"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DAUM_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 110
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SCHEDULE_NO_EVENTS"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SCHEDULE_NO_EVENTS:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 111
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_schedule_all_day"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_schedule_all_day:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 112
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_schedule_to"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_schedule_to:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 113
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_say_command"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_say_command:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 114
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_event_saved"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_event_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 115
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_task_save_cancel_update_prompt"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_task_save_cancel_update_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 116
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_EVENT_SAY_TITLE"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_EVENT_SAY_TITLE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 117
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_who_would_you_like_to_call"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_who_would_you_like_to_call:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 118
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SCHEDULE_EVENTS"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SCHEDULE_EVENTS:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 119
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_TASK_SAY_TITLE"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_SAY_TITLE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 120
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_APPMATCH_DEMAND"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 121
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_SPOKEN_APPMATCH_DEMAND"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 122
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_SPOKEN_APPMATCH_DEMAND_FOR_CAMERA_ONLY"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND_FOR_CAMERA_ONLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 123
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_LAUNCHAPP_PROMPT_DEMAND"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_LAUNCHAPP_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 124
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_event_save_confirm"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_event_save_confirm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 126
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_api_twitter_err_login"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_err_login:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 127
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_api_twitter_update_error"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_update_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 128
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_api_err_auth1"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_err_auth1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 129
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_api_err_auth2"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_err_auth2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 130
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_api_error"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 131
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_api_twitter_error"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 132
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_wcis_social_twitter"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 133
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_util_loading"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_util_loading:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 134
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_PLAYLISTMATCH_DEMAND"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_PLAYLISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 135
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "playlists_quicklist_default_value"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->playlists_quicklist_default_value:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 136
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "playlists_quicklist_default_value1"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->playlists_quicklist_default_value1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 137
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 139
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_logout_facebook_msg"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 140
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_logout_facebook"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 141
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_wcis_social_facebook"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 142
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_logout_twitter_msg"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_twitter_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 143
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_logout_twitter"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 144
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_safereader_hidden_message_body"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_safereader_hidden_message_body:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 145
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_SMS_FROM"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 146
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 147
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 148
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 149
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 150
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SPOKEN"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 151
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_WHICH_CONTACT_DEMAND"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_WHICH_CONTACT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 152
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_status_updated"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_status_updated:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 153
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_no_network"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_no_network:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 154
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_no_status"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_no_status:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 155
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_social_service_prompt"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_service_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 156
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_social_status_prompt"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_status_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 157
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_too_long"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_too_long:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 158
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_login_to_facebook_msg"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 159
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_login_to_network_msg"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_network_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 160
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_login_to_twitter_msg"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_twitter_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 161
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_social_final_prompt"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_final_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 162
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_social_prompt_ex1"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_prompt_ex1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 163
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_err_msg1"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_err_msg1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 164
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_err_msg2"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_err_msg2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 165
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_sms_message_empty_tts"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_message_empty_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 166
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 167
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NAVIGATE_TO"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NAVIGATE_TO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 168
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_MAP_OF"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MAP_OF:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 169
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_GOTO_URL"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_GOTO_URL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 170
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_voice_recognition_service_not_thru_iux_error"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voice_recognition_service_not_thru_iux_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 171
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_memo_multiple_found"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_multiple_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 172
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_memo_confirm_delete"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_confirm_delete:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 173
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_memo_not_saved"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_not_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 174
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_MEMO_SAVED"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MEMO_SAVED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 176
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_api_weibo_err_login"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_err_login:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 177
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_api_weibo_error"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 178
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_api_weibo_update_error"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_update_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 179
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_login_to_weibo_msg"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 180
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_logout_weibo"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_weibo:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 181
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_logout_weibo_msg"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 182
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_wcis_social_weibo"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_weibo:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 184
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_ARTISTMATCH_DEMAND"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ARTISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 185
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_ARTIST_PROMPT_DEMAND"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 186
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_ALBUMMATCH_DEMAND"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ALBUMMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 187
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_ALBUM_PROMPT_DEMAND"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ALBUM_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 188
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_TITLEMATCH_DEMAND"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_TITLEMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 189
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_TITLE_PROMPT_DEMAND"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TITLE_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 190
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_MUSICMATCH_DEMAND"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 191
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_MUSIC_PROMPT_DEMAND"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MUSIC_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 192
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_ANYMATCH_DEMAND"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ANYMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 194
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SPOKEN"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 196
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_MULTIPLE_NEW_MESSAGES"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTIPLE_NEW_MESSAGES:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 197
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_voicedial_call_name"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 198
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_voicedial_call_name_type"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 199
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_MATCH_DEMAND_CALL"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MATCH_DEMAND_CALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 200
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_NO_MATCH_DEMAND_MESSAGE"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MATCH_DEMAND_MESSAGE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 201
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MSG_SHOWN"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MSG_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 202
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MSG_SPOKEN"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MSG_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 203
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 204
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 205
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_checkEventTitleTimeDetailBrief"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventTitleTimeDetailBrief:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 206
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_checkEventYouHave"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 207
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_checkEventYouHaveToday"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHaveToday:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 208
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_checkEventYouHaveOnDate"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHaveOnDate:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 210
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_multiple_contacts"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_contacts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 211
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_multiple_phone_numbers"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_phone_numbers:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 212
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_multiple_phone_numbers2"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_phone_numbers2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 213
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_multiple_applications"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_applications:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 215
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_weather_general"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_general:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 216
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_weather_with_locatin"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_with_locatin:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 217
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_weather_default_location"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 218
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_weather_plus_seven"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_plus_seven:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 220
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_qa_more"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_more:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 221
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_qa_tts_NO_ANS_WEB_SEARCH"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 222
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_tts_NO_ANS_WEB_SEARCH"

    const/16 v2, 0x90

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 223
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_tts_NO_ANS_WEB_SEARCH_1"

    const/16 v2, 0x91

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 224
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_tts_NO_ANS_WEB_SEARCH_2"

    const/16 v2, 0x92

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 225
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_tts_NO_ANS_WEB_SEARCH_3"

    const/16 v2, 0x93

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 226
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_tts_NO_ANS_WEB_SEARCH_4"

    const/16 v2, 0x94

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_4:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 227
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_tts_NO_ANS_GOOGLE_NOW_SEARCH"

    const/16 v2, 0x95

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_GOOGLE_NOW_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 228
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_VD_CALLING_CONFIRM_DEMAND"

    const/16 v2, 0x96

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_VD_CALLING_CONFIRM_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 229
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_default_alarm_title"

    const/16 v2, 0x97

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_default_alarm_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 231
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_redial"

    const/16 v2, 0x98

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_redial:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 232
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_no_call_log"

    const/16 v2, 0x99

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_call_log:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 234
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_alarm_set"

    const/16 v2, 0x9a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_alarm_set:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 235
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_single_contact"

    const/16 v2, 0x9b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_single_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 236
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_weather_no_results"

    const/16 v2, 0x9c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 237
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_social_update_failed"

    const/16 v2, 0x9d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_update_failed:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 238
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_tts_local_required_engine_name"

    const/16 v2, 0x9e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_required_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 240
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_weather_current"

    const/16 v2, 0x9f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_current:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 241
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_weather_date_tts"

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_date_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 242
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_weather_date_display"

    const/16 v2, 0xa1

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_date_display:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 243
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_tomorrow"

    const/16 v2, 0xa2

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tomorrow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 244
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_today"

    const/16 v2, 0xa3

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 246
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_localsearch"

    const/16 v2, 0xa4

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 247
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_localsearch_no_location"

    const/16 v2, 0xa5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 248
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_localsearch_provider_dianping"

    const/16 v2, 0xa6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_provider_dianping:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 249
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_localsearch_default_location"

    const/16 v2, 0xa7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 250
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_localsearch_bad_response"

    const/16 v2, 0xa8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 251
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_localsearch_no_results"

    const/16 v2, 0xa9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 252
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_localsearch_bad_location"

    const/16 v2, 0xaa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 254
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_search_web_label_button"

    const/16 v2, 0xab

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_search_web_label_button:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 256
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_home_phone_found"

    const/16 v2, 0xac

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 257
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_work_phone_found"

    const/16 v2, 0xad

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 258
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_mobile_phone_found"

    const/16 v2, 0xae

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_mobile_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 259
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_home_address_found"

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_address_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 260
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_work_address_found"

    const/16 v2, 0xb0

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_address_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 261
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_home_email_found"

    const/16 v2, 0xb1

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_email_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 262
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_work_email_found"

    const/16 v2, 0xb2

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_email_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 263
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_email_not_found"

    const/16 v2, 0xb3

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_email_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 264
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_address_not_found"

    const/16 v2, 0xb4

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_address_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 265
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_birthday_not_found"

    const/16 v2, 0xb5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_birthday_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 266
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_phone_number_not_found"

    const/16 v2, 0xb6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_phone_number_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 268
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_voicevideodial_call_name"

    const/16 v2, 0xb7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicevideodial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 269
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_voicevideodial_call_name_type"

    const/16 v2, 0xb8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicevideodial_call_name_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 271
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_no_memo_saved"

    const/16 v2, 0xb9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 272
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_phone_in_use"

    const/16 v2, 0xba

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 273
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_mic_in_use"

    const/16 v2, 0xbb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_mic_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 274
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_what_is_the_weather_today_location_var"

    const/16 v2, 0xbc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_today_location_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 275
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_what_is_the_weather_today"

    const/16 v2, 0xbd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 276
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_what_is_the_weather_date_var_location_var"

    const/16 v2, 0xbe

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_date_var_location_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 277
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_what_is_the_weather_date_var"

    const/16 v2, 0xbf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_date_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 279
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_no_memo_saved_about"

    const/16 v2, 0xc0

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved_about:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 280
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_local_search_blank_request_message_shown"

    const/16 v2, 0xc1

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_local_search_blank_request_message_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 281
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_local_search_blank_request_message_spoken"

    const/16 v2, 0xc2

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_local_search_blank_request_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 282
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_address_book_is_empty"

    const/16 v2, 0xc3

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_address_book_is_empty:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 283
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_tts_local_fallback_engine_name"

    const/16 v2, 0xc4

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_fallback_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 284
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_checkEventOneYouHave"

    const/16 v2, 0xc5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventOneYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 285
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_checkEventRussianTwoThreeFourYouHave"

    const/16 v2, 0xc6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventRussianTwoThreeFourYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 286
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_checkEventJapaneseMoreTenYouHave"

    const/16 v2, 0xc7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventJapaneseMoreTenYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 287
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_dot"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 288
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_comma"

    const/16 v2, 0xc9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 289
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_space"

    const/16 v2, 0xca

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 290
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_colon"

    const/16 v2, 0xcb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_colon:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 291
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_semicolon"

    const/16 v2, 0xcc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_semicolon:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 292
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_ellipsis"

    const/16 v2, 0xcd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_ellipsis:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 293
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_network_error"

    const/16 v2, 0xce

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_network_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 294
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_opening_app"

    const/16 v2, 0xcf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_opening_app:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 295
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_message_sending"

    const/16 v2, 0xd0

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_sending:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 296
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_message_sending_readout"

    const/16 v2, 0xd1

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_sending_readout:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 297
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_message_reading_preface"

    const/16 v2, 0xd2

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_preface:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 298
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_message_reading_none"

    const/16 v2, 0xd3

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 299
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_message_reading_single_from_sender"

    const/16 v2, 0xd4

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_single_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 300
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_message_reading_multi_from_sender"

    const/16 v2, 0xd5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 301
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_current_location"

    const/16 v2, 0xd6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 302
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_safereader_notif_title_silent"

    const/16 v2, 0xd7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_title_silent:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 303
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN_NOCALL"

    const/16 v2, 0xd8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 304
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SHOWN"

    const/16 v2, 0xd9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 305
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SPOKEN"

    const/16 v2, 0xda

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 306
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SHOWN"

    const/16 v2, 0xdb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 307
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SPOKEN"

    const/16 v2, 0xdc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 308
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN_NOCALL"

    const/16 v2, 0xdd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 309
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_tts_SAFEREADER_NO_REPLY"

    const/16 v2, 0xde

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NO_REPLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 310
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_wifi_setting_change_on_error"

    const/16 v2, 0xdf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wifi_setting_change_on_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 311
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_playing_music"

    const/16 v2, 0xe0

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_playing_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 312
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_not_detected_current_location"

    const/16 v2, 0xe1

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_not_detected_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 313
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_unknown"

    const/16 v2, 0xe2

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_unknown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 314
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_format_time_AM"

    const/16 v2, 0xe3

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_AM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 315
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_format_time_PM"

    const/16 v2, 0xe4

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_PM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 316
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_error"

    const/16 v2, 0xe5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 317
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_permission_internet_error"

    const/16 v2, 0xe6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_permission_internet_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 319
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_message_reading_none_spoken"

    const/16 v2, 0xe7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 320
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_message_none_from_sender"

    const/16 v2, 0xe8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 321
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_message_none_from_sender_spoken"

    const/16 v2, 0xe9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 322
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_no_more_messages_verbose"

    const/16 v2, 0xea

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_verbose:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 323
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_no_more_messages_regular"

    const/16 v2, 0xeb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_regular:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 324
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_no_more_messages_spoken"

    const/16 v2, 0xec

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 325
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_no_match_found_shown"

    const/16 v2, 0xed

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 326
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_no_match_found_spoken"

    const/16 v2, 0xee

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 327
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_hidden"

    const/16 v2, 0xef

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 328
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_hidden_spoken"

    const/16 v2, 0xf0

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 329
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_hidden_withoutaskingmsg_spoken"

    const/16 v2, 0xf1

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 330
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_shown"

    const/16 v2, 0xf2

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 331
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_spoken"

    const/16 v2, 0xf3

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 332
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_withoutaskingmsg_spoken"

    const/16 v2, 0xf4

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 333
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_next"

    const/16 v2, 0xf5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 334
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_next_spoken"

    const/16 v2, 0xf6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 335
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_next_withoutaskingmsg_spoken"

    const/16 v2, 0xf7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 336
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message"

    const/16 v2, 0xf8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 337
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_spoken"

    const/16 v2, 0xf9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 338
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_withoutaskingmsg_spoken"

    const/16 v2, 0xfa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 339
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_message_shown"

    const/16 v2, 0xfb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 340
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_message_spoken"

    const/16 v2, 0xfc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 341
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_message_withoutaskingmsg_spoken"

    const/16 v2, 0xfd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 342
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_message_overflow"

    const/16 v2, 0xfe

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 343
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_sender"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 344
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_sender_info_verbose_spoken"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 345
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_sender_info_verbose_overflow_spoken"

    const/16 v2, 0x101

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 346
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_sender_command_spoken"

    const/16 v2, 0x102

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_command_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 347
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_sender_dm_overflow"

    const/16 v2, 0x103

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_dm_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 348
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_sender_overflow_spoken"

    const/16 v2, 0x104

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 349
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_sender_overflow_withoutaskingmsg_spoken"

    const/16 v2, 0x105

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 350
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_sender_overflow"

    const/16 v2, 0x106

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 351
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_new_messages_for_russian_1"

    const/16 v2, 0x107

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 352
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_new_messages_for_russian_2"

    const/16 v2, 0x108

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 353
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_new_messages_for_russian_3"

    const/16 v2, 0x109

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 354
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_hidden_nocall"

    const/16 v2, 0x10a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 355
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_hidden_nocall_spoken"

    const/16 v2, 0x10b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 356
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_hidden_nocall_withoutaskingmsg_spoken"

    const/16 v2, 0x10c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 357
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_nocall_spoken"

    const/16 v2, 0x10d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 358
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_initial_nocall_withoutaskingmsg_spoken"

    const/16 v2, 0x10e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 359
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_next_nocall"

    const/16 v2, 0x10f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 360
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_next_nocall_spoken"

    const/16 v2, 0x110

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 361
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_next_nocall_withoutaskingmsg_spoken"

    const/16 v2, 0x111

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 362
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_nocall"

    const/16 v2, 0x112

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 363
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_nocall_spoken"

    const/16 v2, 0x113

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 364
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_single_message_nocall_withoutaskingmsg_spoken"

    const/16 v2, 0x114

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 365
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_message_nocall_shown"

    const/16 v2, 0x115

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 366
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_message_nocall_spoken"

    const/16 v2, 0x116

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 367
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_readout_multi_message_nocall_withoutaskingmsg_spoken"

    const/16 v2, 0x117

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 369
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_and"

    const/16 v2, 0x118

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_and:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 370
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_cradle_date"

    const/16 v2, 0x119

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_cradle_date:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 372
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_redial_confirm_number"

    const/16 v2, 0x11a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_redial_confirm_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 373
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_redial_confirm_contact"

    const/16 v2, 0x11b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_redial_confirm_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 374
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_weibo_dialog_title"

    const/16 v2, 0x11c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weibo_dialog_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 375
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_phone_number"

    const/16 v2, 0x11d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_phone_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 376
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_email"

    const/16 v2, 0x11e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_email:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 377
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_contact_address"

    const/16 v2, 0x11f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_address:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 379
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_time_at_present"

    const/16 v2, 0x120

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_time_at_present:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 381
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_music_playing_calm"

    const/16 v2, 0x121

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_playing_calm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 382
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_music_playing_exciting"

    const/16 v2, 0x122

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_playing_exciting:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 383
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_music_playing_joyful"

    const/16 v2, 0x123

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_playing_joyful:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 384
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_music_playing_passionate"

    const/16 v2, 0x124

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_playing_passionate:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 385
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_music_play_music"

    const/16 v2, 0x125

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_play_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 386
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_music_play_playlist"

    const/16 v2, 0x126

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_play_playlist:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 388
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_volume_is_at_maximum"

    const/16 v2, 0x127

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_at_maximum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 389
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_volume_is_at_minimum"

    const/16 v2, 0x128

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_at_minimum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 390
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_volume_is_already_muted"

    const/16 v2, 0x129

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_already_muted:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 391
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_volume_is_already_unmuted"

    const/16 v2, 0x12a

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_already_unmuted:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 392
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_volume_down"

    const/16 v2, 0x12b

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_down:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 393
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_volume_up"

    const/16 v2, 0x12c

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_up:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 395
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_enable"

    const/16 v2, 0x12d

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 396
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_already_enable"

    const/16 v2, 0x12e

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_already_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 397
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_disable"

    const/16 v2, 0x12f

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 398
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_car_already_disable"

    const/16 v2, 0x130

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_already_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 400
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_phone_type_home"

    const/16 v2, 0x131

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 401
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_phone_type_mobile"

    const/16 v2, 0x132

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_mobile:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 402
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_phone_type_other"

    const/16 v2, 0x133

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_other:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 403
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const-string/jumbo v1, "core_phone_type_work"

    const/16 v2, 0x134

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$string;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_work:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 66
    const/16 v0, 0x135

    new-array v0, v0, [Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_error_help_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_error_sending:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_send_confirm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_speak_msg_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_text_who:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SMS_SENT_CONFIRM_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_VD_MULTIPLE_CONTACTS_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_VD_MULTIPLE_TYPES_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_no_match_openquote:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_navigate_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_navigation_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_map_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safe_reader_default_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_enabled:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_reading_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_reading_ticker:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_subject:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_new_sms_from:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_settings_default_support_url:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_answer_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_alert_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_NAME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BING_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BING_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_YAHOO_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BAIDU_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT_US:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_NAVER_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_NAVER_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DAUM_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DAUM_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SCHEDULE_NO_EVENTS:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_schedule_all_day:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_schedule_to:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_say_command:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_event_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_task_save_cancel_update_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_EVENT_SAY_TITLE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_who_would_you_like_to_call:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SCHEDULE_EVENTS:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_SAY_TITLE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND_FOR_CAMERA_ONLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_LAUNCHAPP_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_event_save_confirm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_err_login:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_update_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_err_auth1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_err_auth2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_util_loading:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_PLAYLISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->playlists_quicklist_default_value:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->playlists_quicklist_default_value1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_twitter_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_safereader_hidden_message_body:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_WHICH_CONTACT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_status_updated:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_no_network:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_no_status:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_service_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_status_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_too_long:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_network_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_twitter_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_final_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_prompt_ex1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_err_msg1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_err_msg2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_message_empty_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NAVIGATE_TO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MAP_OF:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_GOTO_URL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voice_recognition_service_not_thru_iux_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_multiple_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_confirm_delete:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_not_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MEMO_SAVED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_err_login:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_update_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_weibo:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_weibo:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ARTISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ALBUMMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ALBUM_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_TITLEMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TITLE_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MUSIC_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ANYMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTIPLE_NEW_MESSAGES:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MATCH_DEMAND_CALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MATCH_DEMAND_MESSAGE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MSG_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MSG_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventTitleTimeDetailBrief:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHaveToday:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHaveOnDate:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_contacts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_phone_numbers:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_phone_numbers2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_applications:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_general:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_with_locatin:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_plus_seven:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_more:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_4:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_GOOGLE_NOW_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_VD_CALLING_CONFIRM_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_default_alarm_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_redial:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_call_log:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_alarm_set:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_single_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_update_failed:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_required_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_current:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_date_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_date_display:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tomorrow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_provider_dianping:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xab

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_search_web_label_button:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xac

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xad

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xae

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_mobile_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_address_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_address_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_email_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_email_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_email_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_address_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_birthday_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_phone_number_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicevideodial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicevideodial_call_name_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xba

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_mic_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_today_location_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_date_var_location_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_date_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved_about:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_local_search_blank_request_message_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_local_search_blank_request_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_address_book_is_empty:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_fallback_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventOneYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventRussianTwoThreeFourYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventJapaneseMoreTenYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xca

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_colon:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_semicolon:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_ellipsis:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xce

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_network_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_opening_app:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_sending:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_sending_readout:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_preface:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_single_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_title_silent:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xda

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xde

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NO_REPLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wifi_setting_change_on_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_playing_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_not_detected_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_unknown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_AM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_PM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_permission_internet_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xea

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_verbose:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_regular:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xec

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xed

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xee

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xef

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0xff

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x100

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x101

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x102

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_command_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x103

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_dm_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x104

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x105

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x106

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x107

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x108

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x109

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x110

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x111

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x112

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x113

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x114

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x115

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x116

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x117

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x118

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_and:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x119

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_cradle_date:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_redial_confirm_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_redial_confirm_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weibo_dialog_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_phone_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_email:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_address:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x120

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_time_at_present:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x121

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_playing_calm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x122

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_playing_exciting:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x123

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_playing_joyful:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x124

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_playing_passionate:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x125

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_play_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x126

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_play_playlist:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x127

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_at_maximum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x128

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_at_minimum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x129

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_already_muted:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x12a

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_already_unmuted:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x12b

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_down:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x12c

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_up:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x12d

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x12e

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_already_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x12f

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x130

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_already_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x131

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x132

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_mobile:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x133

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_other:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    const/16 v1, 0x134

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_work:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$string;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/ResourceIdProvider$string;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/ResourceIdProvider$string;

    return-object v0
.end method
