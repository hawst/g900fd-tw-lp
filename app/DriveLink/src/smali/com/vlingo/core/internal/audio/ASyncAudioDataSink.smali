.class public abstract Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;
.super Ljava/lang/Object;
.source "ASyncAudioDataSink.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/vlingo/core/internal/audio/AudioFilterAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;
    }
.end annotation


# static fields
.field public static final FILTER_WITHOUT_SOURCE:I = 0x2

.field public static final FILTER_WITH_SOURCE:I = 0x1


# instance fields
.field private handler:Landroid/os/Handler;

.field private handlerThread:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method


# virtual methods
.method public filter([SII)I
    .locals 5
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->handler:Landroid/os/Handler;

    const/4 v2, 0x2

    new-instance v3, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;

    const/4 v4, -0x1

    invoke-direct {v3, p1, p2, p3, v4}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;-><init>([SIII)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public filter([SIII)I
    .locals 4
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I
    .param p4, "audioSourceId"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->handler:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;

    invoke-direct {v3, p1, p2, p3, p4}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;-><init>([SIII)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 52
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;

    .line 53
    .local v0, "micData":Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 61
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 55
    :pswitch_0
    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->getAudioData()[S

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->getOffsetInShorts()I

    move-result v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->getSizeInShorts()I

    move-result v3

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->getAudioSourceId()I

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->sink([SIII)I

    goto :goto_0

    .line 58
    :pswitch_1
    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->getAudioData()[S

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->getOffsetInShorts()I

    move-result v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->getSizeInShorts()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->sink([SII)I

    goto :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public init(III)V
    .locals 1
    .param p1, "sampleRate"    # I
    .param p2, "speechEndpointTimeout"    # I
    .param p3, "noSpeechEndPointTimeout"    # I

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->init(IIII)V

    .line 87
    return-void
.end method

.method public init(IIII)V
    .locals 2
    .param p1, "sampleRate"    # I
    .param p2, "speechEndpointTimeout"    # I
    .param p3, "noSpeechEndPointTimeout"    # I
    .param p4, "threadPriority"    # I

    .prologue
    .line 92
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "ASyncAudioDataSink"

    invoke-direct {v0, v1, p4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->handlerThread:Landroid/os/HandlerThread;

    .line 93
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 94
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->handler:Landroid/os/Handler;

    .line 96
    return-void
.end method

.method public quit()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->handlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 103
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public abstract sink([SII)I
.end method

.method public abstract sink([SIII)I
.end method
