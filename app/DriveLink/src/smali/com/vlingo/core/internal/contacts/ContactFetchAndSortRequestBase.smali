.class public abstract Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;
.super Ljava/lang/Thread;
.source "ContactFetchAndSortRequestBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$ContactMatchQueryComparator;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static log:Lcom/vlingo/core/internal/logging/Logger;

.field private static query:Ljava/lang/String;


# instance fields
.field private bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field private final callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

.field private final contactType:Lcom/vlingo/core/internal/contacts/ContactType;

.field private volatile doneRunning:Z

.field private volatile hasStarted:Z

.field private mContext:Landroid/content/Context;

.field private matchString:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private matches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private final mru:Lcom/vlingo/core/internal/contacts/mru/MRU;

.field private final pruningThreshold:F

.field private final requestedTypes:[I

.field private final sortedContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 20
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;[ILcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p4, "requestedTypes"    # [I
    .param p5, "callback"    # Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 35
    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->doneRunning:Z

    .line 36
    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->hasStarted:Z

    .line 39
    sput-object p2, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->query:Ljava/lang/String;

    .line 40
    invoke-static {}, Lcom/vlingo/core/internal/contacts/mru/MRU;->getMRU()Lcom/vlingo/core/internal/contacts/mru/MRU;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->mru:Lcom/vlingo/core/internal/contacts/mru/MRU;

    .line 41
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->contactType:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 42
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->requestedTypes:[I

    .line 43
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->pruningThreshold:F

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->sortedContacts:Ljava/util/List;

    .line 45
    iput-object p5, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    .line 46
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->matchString:Ljava/util/List;

    .line 47
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method static synthetic access$002(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;
    .param p1, "x1"    # Z

    .prologue
    .line 15
    iput-boolean p1, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->hasStarted:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->matches:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->contactType:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)[I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->requestedTypes:[I

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/mru/MRU;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->mru:Lcom/vlingo/core/internal/contacts/mru/MRU;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->sortedContacts:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->query:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;
    .param p1, "x1"    # Z

    .prologue
    .line 15
    iput-boolean p1, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->doneRunning:Z

    return p1
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    return-object v0
.end method

.method static synthetic access$900()Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-object v0
.end method

.method private findMatchesAndSortContacts()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 73
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->query:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->contactType:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {p0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->getContactMatches(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->matches:Ljava/util/List;

    .line 75
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->setContext(Landroid/content/Context;)V

    .line 76
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->scoreContacts()V

    .line 77
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->sortContacts()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->setContext(Landroid/content/Context;)V

    .line 83
    :goto_0
    return-void

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "run(): Caught RuntimeException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; stack trace:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->setContext(Landroid/content/Context;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->setContext(Landroid/content/Context;)V

    throw v1
.end method

.method private sortContacts()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;-><init>(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)V

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->start()V

    .line 157
    return-void
.end method


# virtual methods
.method public getBestContact()Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    return-object v0
.end method

.method protected getContactMatchComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch$ReverseComparator;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch$ReverseComparator;-><init>()V

    return-object v0
.end method

.method protected getContactMatchQueryComparator(Ljava/lang/String;)Ljava/util/Comparator;
    .locals 1
    .param p1, "queryParam"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Comparator",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$ContactMatchQueryComparator;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$ContactMatchQueryComparator;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected abstract getContactMatches(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end method

.method public getContactType()Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->contactType:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getMatchString()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->matchString:Ljava/util/List;

    return-object v0
.end method

.method public getMatches()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->matches:Ljava/util/List;

    return-object v0
.end method

.method public getMru()Lcom/vlingo/core/internal/contacts/mru/MRU;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->mru:Lcom/vlingo/core/internal/contacts/mru/MRU;

    return-object v0
.end method

.method public getPruningThreshold()F
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->pruningThreshold:F

    return v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->query:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestedTypes()[I
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->requestedTypes:[I

    return-object v0
.end method

.method public getSortedContacts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->sortedContacts:Ljava/util/List;

    return-object v0
.end method

.method public hasStarted()Z
    .locals 1

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->hasStarted:Z

    return v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->doneRunning:Z

    return v0
.end method

.method protected matchExtraSortedContacts()V
    .locals 5

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->getSortedContacts()Ljava/util/List;

    move-result-object v2

    .line 90
    .local v2, "sortedContactsList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v3, "typeMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 92
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasMatchedType()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 93
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 97
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 98
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 100
    :cond_2
    return-void
.end method

.method public run()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->findMatchesAndSortContacts()V

    .line 55
    return-void
.end method

.method protected abstract scoreContacts()V
.end method

.method public setBestContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0
    .param p1, "bestContact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 204
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->mContext:Landroid/content/Context;

    .line 180
    return-void
.end method

.method public setMatches(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 215
    .local p1, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->matches:Ljava/util/List;

    .line 216
    return-void
.end method
