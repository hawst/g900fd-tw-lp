.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
.source "DefaultWebSearchHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v7, 0x0

    .line 31
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 33
    const-string/jumbo v4, "query"

    invoke-static {p1, v4, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 34
    .local v1, "query":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PARSE_TYPE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 35
    .local v0, "parseType":Ljava/lang/String;
    const-string/jumbo v2, "def"

    .line 36
    .local v2, "searchType":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 37
    const-string/jumbo v4, "wsearch:"

    const-string/jumbo v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 40
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;->useEmbeddedBrowser()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->useGoogleSearch()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {}, Lcom/vlingo/core/internal/util/WebSearchUtils;->isDefaultGoogleSearch()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string/jumbo v4, "def"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    const-string/jumbo v4, "google"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 43
    :cond_2
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v5, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    const-string/jumbo v5, "com.google.android.googlequicksearchbox"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->enclosingPackage(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v4

    const-string/jumbo v5, "com.google.android.googlequicksearchbox.SearchActivity"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->activity(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "query,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v4

    const-string/jumbo v5, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->action(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v4

    const-string/jumbo v5, "Google Search"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->app(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->queue()V

    .line 51
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 62
    :cond_3
    :goto_0
    return v7

    .line 55
    :cond_4
    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;->getWebSearchURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 57
    .local v3, "url":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 58
    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;->executeSearchIntentFromURL(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    goto :goto_0
.end method
