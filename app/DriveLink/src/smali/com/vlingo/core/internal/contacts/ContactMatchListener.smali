.class public interface abstract Lcom/vlingo/core/internal/contacts/ContactMatchListener;
.super Ljava/lang/Object;
.source "ContactMatchListener.java"


# virtual methods
.method public abstract onAutoAction(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
.end method

.method public abstract onContactMatchResultsUpdated(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onContactMatchingFailed()V
.end method

.method public abstract onContactMatchingFinished(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation
.end method
