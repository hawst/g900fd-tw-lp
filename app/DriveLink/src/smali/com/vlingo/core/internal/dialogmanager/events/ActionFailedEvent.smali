.class public Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
.super Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
.source "ActionFailedEvent.java"


# instance fields
.field private final NAME:Ljava/lang/String;

.field private final reason:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 22
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;-><init>(ZZZ)V

    .line 18
    const-string/jumbo v0, "action-failed"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;->NAME:Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;->reason:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    const-string/jumbo v0, "action-failed"

    return-object v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;->reason:Ljava/lang/String;

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v1, "action-failed"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    const-string/jumbo v1, "reason"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;->reason:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 41
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v1

    return-object v1
.end method
