.class public Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;
.super Lcom/vlingo/core/internal/dialogmanager/StateController;
.source "SafeReaderController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field protected alertQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field protected currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

.field private isDialingEnabled:Z

.field private isSilentMode:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    return-void
.end method

.method private displayMultipleMessages()V
    .locals 5

    .prologue
    .line 271
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 274
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTIPLE_NEW_MESSAGES:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_POSTMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 276
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageDisplay:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getMessagesForWidget(Ljava/util/Queue;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 279
    :cond_0
    return-void
.end method

.method private getBodyShown(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "contactName"    # Ljava/lang/String;
    .param p2, "fromReadout"    # Z
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 428
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 429
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isSilentMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 438
    :goto_0
    return-object v0

    .line 434
    :cond_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isDialingEnabled:Z

    if-eqz v0, :cond_2

    .line 435
    if-eqz p2, :cond_1

    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p3, v1, v3

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v3

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 438
    :cond_2
    if-eqz p2, :cond_3

    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p3, v1, v3

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v3

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getBodySpoken(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "contactName"    # Ljava/lang/String;
    .param p2, "fromReadout"    # Z
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 445
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 446
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isSilentMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    const-string/jumbo v0, ""

    .line 453
    :goto_0
    return-object v0

    .line 449
    :cond_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isDialingEnabled:Z

    if-eqz v0, :cond_2

    .line 450
    if-eqz p2, :cond_1

    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    aput-object p3, v1, v3

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 453
    :cond_2
    if-eqz p2, :cond_3

    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    aput-object p3, v1, v3

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getFieldId(Z)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 1
    .param p1, "isDisplayMessageBody"    # Z

    .prologue
    .line 462
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isSilentMode()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v0}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_POSTMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v0}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_NEWMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v0}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    goto :goto_0
.end method

.method private getMessageBody(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;
    .locals 1
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 362
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getDisplayableMessageText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMessagesForWidget(Ljava/util/Queue;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)",
            "Ljava/util/List",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    .local p1, "alerts":Ljava/util/Queue;, "Ljava/util/Queue<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 367
    .local v4, "msgList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageType;>;"
    if-eqz p1, :cond_3

    .line 368
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Queue;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 369
    invoke-interface {p1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 370
    .local v1, "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v3, v1

    .line 371
    check-cast v3, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 373
    .local v3, "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getMessageBody(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v2

    .line 374
    .local v2, "displayBody":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v5

    .line 377
    .local v5, "name":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    const-string/jumbo v6, ""

    invoke-direct {v0, v2, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    .local v0, "aMessageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAttachments()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setAttachments(I)V

    .line 379
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setId(J)V

    .line 380
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "SMS"

    if-ne v6, v7, :cond_2

    .line 381
    const-string/jumbo v6, "SMS"

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setType(Ljava/lang/String;)V

    .line 386
    :cond_1
    :goto_1
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getTimeStamp()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setTimeStamp(J)V

    .line 388
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 383
    :cond_2
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "MMS"

    if-ne v6, v7, :cond_1

    .line 384
    const-string/jumbo v6, "MMS"

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setType(Ljava/lang/String;)V

    goto :goto_1

    .line 392
    .end local v0    # "aMessageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .end local v1    # "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    .end local v2    # "displayBody":Ljava/lang/String;
    .end local v3    # "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v5    # "name":Ljava/lang/String;
    :cond_3
    return-object v4
.end method

.method private getPromptShown(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "contactName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 397
    .local v0, "builder":Ljava/lang/StringBuilder;
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isSilentMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 400
    iget-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isDialingEnabled:Z

    if-eqz v1, :cond_1

    .line 401
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 403
    :cond_1
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private getPromptSpoken(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "contactName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 410
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 411
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isSilentMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 412
    const-string/jumbo v1, ""

    .line 423
    :goto_0
    return-object v1

    .line 414
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 415
    .local v0, "sb":Ljava/lang/StringBuilder;
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    const-string/jumbo v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    iget-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isDialingEnabled:Z

    if-eqz v1, :cond_1

    .line 418
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 420
    :cond_1
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private getSenderDisplayText(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Ljava/lang/String;
    .locals 1
    .param p1, "alert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    .line 287
    if-nez p1, :cond_0

    .line 288
    const-string/jumbo v0, ""

    .line 293
    :goto_0
    return-object v0

    .line 290
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 293
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private next()V
    .locals 0

    .prologue
    .line 284
    return-void
.end method


# virtual methods
.method callSender()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 301
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v4, :cond_0

    .line 302
    new-instance v4, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->DIAL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    invoke-direct {v4, v5, v6}, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;-><init>(Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 304
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/PhoneUtil;->turnOnSpeakerphoneIfRequired(Landroid/content/Context;)V

    .line 306
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 309
    .local v2, "name":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 312
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "address":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v2, v5, v7

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 314
    .local v1, "displayText":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/DialUtil;->getTTSForAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 321
    .end local v0    # "address":Ljava/lang/String;
    .local v3, "ttsText":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    invoke-static {v4, v5, p0, v6}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 328
    .end local v1    # "displayText":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "ttsText":Ljava/lang/String;
    :cond_0
    return-void

    .line 316
    .restart local v2    # "name":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v2, v5, v7

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 317
    .restart local v1    # "displayText":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "ttsText":Ljava/lang/String;
    goto :goto_0
.end method

.method protected displayMessageBody(Z)V
    .locals 11
    .param p1, "fromReadout"    # Z

    .prologue
    const/4 v10, 0x1

    .line 226
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-nez v8, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v8}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 234
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v3, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 236
    .local v3, "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getBody()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "body":Ljava/lang/String;
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v8}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 238
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSubject()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 239
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 240
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSubject()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 247
    :cond_2
    :goto_1
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 248
    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_message_empty_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 250
    :cond_3
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v5

    .line 251
    .local v5, "name":Ljava/lang/String;
    const/4 v6, 0x0

    .line 252
    .local v6, "nameSpoken":Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 253
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 255
    :cond_4
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 257
    new-instance v4, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    const-string/jumbo v8, ""

    invoke-direct {v4, v0, v5, v8}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    .local v4, "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v8, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAttachments()I

    move-result v8

    invoke-virtual {v4, v8}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setAttachments(I)V

    .line 259
    new-instance v7, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v5, v8}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    .local v7, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v4, v7}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 261
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getTimeStamp()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setTimeStamp(J)V

    .line 263
    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getBodyShown(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 264
    .local v1, "bodyShown":Ljava/lang/String;
    invoke-direct {p0, v6, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getBodySpoken(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 266
    .local v2, "bodySpoken":Ljava/lang/String;
    invoke-direct {p0, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getFieldId(Z)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v8

    invoke-virtual {p0, v1, v2, v10, v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 267
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v8

    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10, v4, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_0

    .line 242
    .end local v1    # "bodyShown":Ljava/lang/String;
    .end local v2    # "bodySpoken":Ljava/lang/String;
    .end local v4    # "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "nameSpoken":Ljava/lang/String;
    .end local v7    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_5
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSubject()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected displayShowMessagePrompt()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 201
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v8, :cond_1

    .line 202
    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_safereader_hidden_message_body:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v9, v10, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "hiddenBody":Ljava/lang/String;
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v3

    .line 204
    .local v3, "name":Ljava/lang/String;
    const/4 v4, 0x0

    .line 205
    .local v4, "nameSpoken":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 206
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v3

    .line 208
    :cond_0
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 210
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    const-string/jumbo v8, ""

    invoke-direct {v2, v0, v3, v8}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    .local v2, "mtHiddenBody":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v8, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAttachments()I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setAttachments(I)V

    .line 212
    new-instance v7, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    .local v7, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v2, v7}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 214
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getTimeStamp()J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setTimeStamp(J)V

    .line 216
    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getPromptShown(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 217
    .local v5, "promptShown":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getPromptSpoken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 219
    .local v6, "promptSpoken":Ljava/lang/String;
    const/4 v8, 0x1

    invoke-direct {p0, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getFieldId(Z)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v9

    invoke-virtual {p0, v5, v6, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 220
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    .line 221
    .local v1, "listener":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadbackBodyHidden:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v9, 0x0

    invoke-interface {v1, v8, v9, v2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 223
    .end local v0    # "hiddenBody":Ljava/lang/String;
    .end local v1    # "listener":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .end local v2    # "mtHiddenBody":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "nameSpoken":Ljava/lang/String;
    .end local v5    # "promptShown":Ljava/lang/String;
    .end local v6    # "promptSpoken":Ljava/lang/String;
    .end local v7    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_1
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 4
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v3, 0x0

    .line 67
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/StateController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 70
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isDialingEnabled:Z

    .line 71
    if-eqz p1, :cond_2

    .line 72
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "LPAction"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v3

    .line 76
    :cond_1
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "SafereaderReply"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    const-string/jumbo v1, "Text"

    invoke-static {p1, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "message":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->reply(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    .end local v0    # "message":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 98
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_3

    .line 101
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->displayMultipleMessages()V

    goto :goto_0

    .line 103
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 104
    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->displayMessageBody(Z)V

    goto :goto_0
.end method

.method public getRuleMappings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRules()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const-wide/16 v7, -0x1

    .line 125
    sget-object v5, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v5

    iput-boolean v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isDialingEnabled:Z

    .line 126
    if-nez p1, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Readout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 131
    const/4 v1, 0x1

    .line 132
    .local v1, "fromReadout":Z
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 133
    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->displayMessageBody(Z)V

    goto :goto_0

    .line 134
    .end local v1    # "fromReadout":Z
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Call"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 135
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 136
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->callSender()V

    goto :goto_0

    .line 137
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Reply"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 138
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 139
    const-string/jumbo v5, ""

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->reply(Ljava/lang/String;)V

    goto :goto_0

    .line 140
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 144
    const-string/jumbo v5, "id"

    invoke-virtual {p1, v5, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 145
    .local v2, "id":J
    const-string/jumbo v5, "message_type"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 146
    .local v4, "type":Ljava/lang/String;
    cmp-long v5, v2, v7

    if-eqz v5, :cond_0

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 149
    const-string/jumbo v5, "SMS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string/jumbo v5, "MMS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 154
    :cond_5
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5, v2, v3, v4, v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->findbyId(JLjava/lang/String;Ljava/util/LinkedList;)Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v0

    .line 155
    .local v0, "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    if-eqz v0, :cond_0

    .line 160
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 161
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->displayMessageBody(Z)V

    goto/16 :goto_0
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 6
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 178
    sget-object v5, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v5

    iput-boolean v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isDialingEnabled:Z

    .line 179
    const-string/jumbo v5, "Action"

    invoke-static {p1, v5, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "actionValue":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 181
    const-string/jumbo v5, "safereader:read"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 182
    const/4 v2, 0x1

    .line 183
    .local v2, "readout":Z
    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->displayMessageBody(Z)V

    .line 197
    .end local v2    # "readout":Z
    :goto_0
    return v3

    .line 185
    :cond_0
    const-string/jumbo v5, "safereader:call"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 186
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->callSender()V

    goto :goto_0

    .line 188
    :cond_1
    const-string/jumbo v5, "safereader:reply"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 189
    const-string/jumbo v5, "Text"

    invoke-static {p1, v5, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "message":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->reply(Ljava/lang/String;)V

    goto :goto_0

    .line 192
    .end local v1    # "message":Ljava/lang/String;
    :cond_2
    const-string/jumbo v5, "safereader:next"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 193
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->next()V

    goto :goto_0

    :cond_3
    move v3, v4

    .line 197
    goto :goto_0
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 478
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isSilentMode:Z

    return v0
.end method

.method public newMessageArrived()V
    .locals 0

    .prologue
    .line 121
    return-void
.end method

.method reply(Ljava/lang/String;)V
    .locals 7
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 336
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v4, :cond_1

    .line 337
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 338
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v2, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 339
    .local v2, "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;

    invoke-direct {v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;-><init>()V

    .line 340
    .local v1, "controller":Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v4, v5, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 342
    move-object v0, p1

    .line 343
    .local v0, "body":Ljava/lang/String;
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    .local v3, "mt":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAttachments()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setAttachments(I)V

    .line 347
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getTimeStamp()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setTimeStamp(J)V

    .line 348
    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-virtual {v1, v4, v5, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V

    .line 359
    .end local v0    # "body":Ljava/lang/String;
    .end local v1    # "controller":Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;
    .end local v2    # "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v3    # "mt":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NO_REPLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 474
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->reset()V

    .line 475
    return-void
.end method

.method public setAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V
    .locals 0
    .param p1, "alert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 60
    return-void
.end method

.method public setAlertQueue(Ljava/util/LinkedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    .line 52
    return-void
.end method

.method public setAlertQueue(Ljava/util/LinkedList;Z)V
    .locals 0
    .param p2, "capped"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->alertQueue:Ljava/util/LinkedList;

    .line 56
    return-void
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "silentMode"    # Z

    .prologue
    .line 482
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->isSilentMode:Z

    .line 483
    return-void
.end method
