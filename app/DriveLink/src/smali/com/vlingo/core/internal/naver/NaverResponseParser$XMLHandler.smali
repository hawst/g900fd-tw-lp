.class Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "NaverResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/naver/NaverResponseParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "XMLHandler"
.end annotation


# instance fields
.field public data:Lcom/vlingo/core/internal/naver/NaverResponseParser;

.field elementOn:Ljava/lang/Boolean;

.field elementValue:Ljava/lang/String;

.field inDetails:Ljava/lang/Boolean;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/naver/NaverResponseParser;)V
    .locals 3
    .param p1, "d"    # Lcom/vlingo/core/internal/naver/NaverResponseParser;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 78
    iput-object v2, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->elementValue:Ljava/lang/String;

    .line 79
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->elementOn:Ljava/lang/Boolean;

    .line 80
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->inDetails:Ljava/lang/Boolean;

    .line 81
    iput-object v2, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->data:Lcom/vlingo/core/internal/naver/NaverResponseParser;

    .line 84
    iput-object p1, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->data:Lcom/vlingo/core/internal/naver/NaverResponseParser;

    .line 85
    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->elementOn:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->elementValue:Ljava/lang/String;

    .line 153
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->elementOn:Ljava/lang/Boolean;

    .line 156
    :cond_0
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->elementOn:Ljava/lang/Boolean;

    .line 133
    const-string/jumbo v0, "QueryUsed"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->data:Lcom/vlingo/core/internal/naver/NaverResponseParser;

    iget-object v1, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->elementValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->setQueryUsed(Ljava/lang/String;)V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    const-string/jumbo v0, "RawResponseXML"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    iget-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->data:Lcom/vlingo/core/internal/naver/NaverResponseParser;

    iget-object v1, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->elementValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->setRawResponseXML(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :cond_2
    const-string/jumbo v0, "Message"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->inDetails:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 138
    iget-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->data:Lcom/vlingo/core/internal/naver/NaverResponseParser;

    iget-object v1, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->elementValue:Ljava/lang/String;

    # invokes: Lcom/vlingo/core/internal/naver/NaverResponseParser;->setError(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->access$000(Lcom/vlingo/core/internal/naver/NaverResponseParser;Ljava/lang/String;)V

    goto :goto_0

    .line 139
    :cond_3
    const-string/jumbo v0, "Details"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->inDetails:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 94
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->elementOn:Ljava/lang/Boolean;

    .line 99
    const-string/jumbo v4, "NaverPassThroughResponse"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 100
    invoke-interface {p4}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v1

    .line 102
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_3

    .line 104
    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v2

    .line 105
    .local v2, "name":Ljava/lang/String;
    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v3

    .line 108
    .local v3, "value":Ljava/lang/String;
    const-string/jumbo v4, "QueryUsed"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 109
    iget-object v4, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->data:Lcom/vlingo/core/internal/naver/NaverResponseParser;

    invoke-virtual {v4, v3}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->setQueryUsed(Ljava/lang/String;)V

    .line 102
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :cond_1
    const-string/jumbo v4, "RawResponseXML"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 111
    iget-object v4, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->data:Lcom/vlingo/core/internal/naver/NaverResponseParser;

    invoke-virtual {v4, v3}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->setRawResponseXML(Ljava/lang/String;)V

    goto :goto_1

    .line 113
    .end local v0    # "i":I
    .end local v1    # "length":I
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/String;
    :cond_2
    const-string/jumbo v4, "Details"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 114
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;->inDetails:Ljava/lang/Boolean;

    .line 116
    :cond_3
    return-void
.end method
