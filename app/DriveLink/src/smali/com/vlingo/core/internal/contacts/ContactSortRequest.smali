.class public abstract Lcom/vlingo/core/internal/contacts/ContactSortRequest;
.super Ljava/lang/Object;
.source "ContactSortRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/ContactSortRequest$ReverseComparator;
    }
.end annotation


# static fields
.field private static final LOG:Lcom/vlingo/core/internal/logging/Logger;

.field protected static query:Ljava/lang/String;


# instance fields
.field private bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field protected final callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

.field protected volatile doneRunning:Z

.field private volatile hasStarted:Z

.field private matchString:[Ljava/lang/String;

.field protected matches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field protected final mru:Lcom/vlingo/core/internal/contacts/mru/MRU;

.field protected final pruningThreshold:F

.field protected final requestedTypes:[I

.field private final sortedContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field protected final type:Lcom/vlingo/core/internal/contacts/ContactType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->LOG:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;[ILcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p3, "requestedTypes"    # [I
    .param p4, "callback"    # Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->doneRunning:Z

    .line 35
    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->hasStarted:Z

    .line 38
    sput-object p1, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->query:Ljava/lang/String;

    .line 39
    invoke-static {}, Lcom/vlingo/core/internal/contacts/mru/MRU;->getMRU()Lcom/vlingo/core/internal/contacts/mru/MRU;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->mru:Lcom/vlingo/core/internal/contacts/mru/MRU;

    .line 40
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 41
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->requestedTypes:[I

    .line 42
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->pruningThreshold:F

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortedContacts:Ljava/util/List;

    .line 44
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    .line 46
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matchString:[Ljava/lang/String;

    .line 47
    return-void
.end method

.method private breakAllWords(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 154
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 155
    .local v1, "newName":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 156
    sget-object v2, Lcom/vlingo/core/internal/util/StringUtils;->CHINESE_UNICODE_BLOCKS:Ljava/util/Set;

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 157
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 155
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 159
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 161
    :cond_1
    return-object v1
.end method

.method private breakAtPosition(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 165
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 166
    .local v1, "newName":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 167
    if-ne v0, p2, :cond_0

    .line 168
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 166
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 170
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 172
    :cond_1
    return-object v1
.end method

.method private computeChineseScore(FLcom/vlingo/core/internal/contacts/ContactMatch;)F
    .locals 10
    .param p1, "score"    # F
    .param p2, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/high16 v9, 0x41a00000    # 20.0f

    .line 114
    iget-object v4, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 115
    .local v4, "name":Ljava/lang/String;
    move v0, p1

    .line 116
    .local v0, "chineseScore":F
    cmpl-float v7, v0, v9

    if-ltz v7, :cond_0

    move v1, v0

    .line 141
    .end local v0    # "chineseScore":F
    .local v1, "chineseScore":F
    :goto_0
    return v1

    .line 119
    .end local v1    # "chineseScore":F
    .restart local v0    # "chineseScore":F
    :cond_0
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->breakAllWords(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 120
    .local v5, "newName":Ljava/lang/String;
    invoke-direct {p0, p2, v5}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v6

    .line 121
    .local v6, "newScore":F
    cmpl-float v7, v6, v0

    if-lez v7, :cond_1

    .line 122
    move v0, v6

    .line 124
    :cond_1
    cmpg-float v7, v0, v9

    if-gez v7, :cond_5

    .line 125
    const-string/jumbo v7, " "

    const-string/jumbo v8, ""

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 126
    invoke-direct {p0, p2, v5}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v6

    .line 127
    cmpl-float v7, v6, v0

    if-lez v7, :cond_2

    .line 128
    move v0, v6

    .line 130
    :cond_2
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x4

    if-le v7, v8, :cond_4

    const/4 v3, 0x3

    .line 131
    .local v3, "maxBreakPosition":I
    :goto_1
    move v2, v3

    .local v2, "i":I
    :goto_2
    if-ltz v2, :cond_5

    .line 132
    cmpg-float v7, v0, v9

    if-gez v7, :cond_3

    .line 133
    invoke-direct {p0, v4, v2}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->breakAtPosition(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 134
    invoke-direct {p0, p2, v5}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v6

    .line 135
    cmpl-float v7, v6, v0

    if-lez v7, :cond_3

    .line 136
    move v0, v6

    .line 131
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 130
    .end local v2    # "i":I
    .end local v3    # "maxBreakPosition":I
    :cond_4
    const/4 v3, 0x1

    goto :goto_1

    :cond_5
    move v1, v0

    .line 141
    .end local v0    # "chineseScore":F
    .restart local v1    # "chineseScore":F
    goto :goto_0
.end method

.method private computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;)F
    .locals 4
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 145
    iget-object v0, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 146
    .local v0, "name":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v1

    .line 147
    .local v1, "score":F
    sget-object v2, Lcom/vlingo/core/internal/util/StringUtils;->CHINESE_UNICODE_BLOCKS:Ljava/util/Set;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    invoke-direct {p0, v1, p1}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->computeChineseScore(FLcom/vlingo/core/internal/contacts/ContactMatch;)F

    move-result v1

    .line 150
    :cond_0
    return v1
.end method

.method private computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F
    .locals 8
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, ", &/+.-"

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "contactWords":[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->computeScore([Ljava/lang/String;)F

    move-result v5

    .line 209
    .local v5, "score":F
    const-string/jumbo v6, "contacts.use_other_names"

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 211
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasExtraNames()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 212
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getExtraNames()Ljava/util/List;

    move-result-object v3

    .line 213
    .local v3, "extraNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 215
    .local v1, "extraName":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, ", &/+."

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->computeScore([Ljava/lang/String;)F

    move-result v2

    .line 217
    .local v2, "extraNameScore":F
    const/high16 v6, -0x40000000    # -2.0f

    add-float/2addr v2, v6

    .line 218
    cmpg-float v6, v5, v2

    if-gez v6, :cond_0

    .line 219
    move v5, v2

    .line 220
    iput-object v1, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNameUsed:Ljava/lang/String;

    goto :goto_0

    .line 225
    .end local v1    # "extraName":Ljava/lang/String;
    .end local v2    # "extraNameScore":F
    .end local v3    # "extraNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    return v5
.end method

.method private computeScore([Ljava/lang/String;)F
    .locals 12
    .param p1, "contactWords"    # [Ljava/lang/String;

    .prologue
    const/high16 v11, -0x40800000    # -1.0f

    .line 229
    const/4 v8, 0x0

    .line 234
    .local v8, "score":F
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matchString:[Ljava/lang/String;

    array-length v10, v10

    new-array v9, v10, [Z

    .line 235
    .local v9, "wasWordUsed":[Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matchString:[Ljava/lang/String;

    array-length v10, v10

    if-ge v3, v10, :cond_0

    .line 236
    const/4 v10, 0x0

    aput-boolean v10, v9, v3

    .line 235
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 237
    :cond_0
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    array-length v10, p1

    if-ge v5, v10, :cond_d

    .line 238
    const/4 v1, 0x0

    .line 239
    .local v1, "bestMatchScore":I
    const/4 v0, -0x1

    .line 240
    .local v0, "bestMatchIndex":I
    aget-object v2, p1, v5

    .line 241
    .local v2, "contactWord":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_1

    .line 237
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 243
    :cond_1
    const/4 v3, 0x0

    :goto_3
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matchString:[Ljava/lang/String;

    array-length v10, v10

    if-ge v3, v10, :cond_b

    .line 244
    aget-boolean v10, v9, v3

    if-nez v10, :cond_5

    .line 245
    const/4 v4, 0x0

    .line 246
    .local v4, "isMatchingPosition":Z
    if-nez v3, :cond_2

    if-eqz v5, :cond_3

    :cond_2
    if-eqz v3, :cond_4

    if-eqz v5, :cond_4

    .line 247
    :cond_3
    const/4 v4, 0x1

    .line 248
    :cond_4
    const/4 v6, 0x0

    .line 249
    .local v6, "matchScore":I
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matchString:[Ljava/lang/String;

    aget-object v7, v10, v3

    .line 250
    .local v7, "matchWord":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_6

    .line 243
    .end local v4    # "isMatchingPosition":Z
    .end local v6    # "matchScore":I
    .end local v7    # "matchWord":Ljava/lang/String;
    :cond_5
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 252
    .restart local v4    # "isMatchingPosition":Z
    .restart local v6    # "matchScore":I
    .restart local v7    # "matchWord":Ljava/lang/String;
    :cond_6
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 253
    if-eqz v4, :cond_8

    .line 254
    const/16 v6, 0x14

    .line 265
    :cond_7
    :goto_5
    if-le v6, v1, :cond_5

    .line 266
    move v1, v6

    .line 267
    move v0, v3

    goto :goto_4

    .line 256
    :cond_8
    const/16 v6, 0x12

    goto :goto_5

    .line 258
    :cond_9
    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 259
    if-eqz v4, :cond_a

    .line 260
    const/16 v6, 0xa

    goto :goto_5

    .line 262
    :cond_a
    const/16 v6, 0x8

    goto :goto_5

    .line 271
    .end local v4    # "isMatchingPosition":Z
    .end local v6    # "matchScore":I
    .end local v7    # "matchWord":Ljava/lang/String;
    :cond_b
    if-ltz v0, :cond_c

    .line 272
    int-to-float v10, v1

    add-float/2addr v8, v10

    .line 273
    const/4 v10, 0x1

    aput-boolean v10, v9, v0

    goto :goto_2

    .line 276
    :cond_c
    add-float/2addr v8, v11

    goto :goto_2

    .line 279
    .end local v0    # "bestMatchIndex":I
    .end local v1    # "bestMatchScore":I
    .end local v2    # "contactWord":Ljava/lang/String;
    :cond_d
    const/4 v3, 0x0

    :goto_6
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matchString:[Ljava/lang/String;

    array-length v10, v10

    if-ge v3, v10, :cond_f

    .line 280
    aget-boolean v10, v9, v3

    if-nez v10, :cond_e

    .line 281
    add-float/2addr v8, v11

    .line 279
    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 283
    :cond_f
    return v8
.end method

.method private matchExtraSortedContacts()V
    .locals 4

    .prologue
    .line 317
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 318
    .local v2, "typeMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortedContacts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 319
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasMatchedType()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 320
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 323
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 324
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortedContacts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 325
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortedContacts:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 327
    :cond_2
    return-void
.end method

.method private static split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 9
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "delimiters"    # Ljava/lang/String;

    .prologue
    .line 287
    const/4 v1, 0x0

    .line 288
    .local v1, "count":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    .line 289
    .local v7, "len":I
    const/4 v6, 0x1

    .line 290
    .local v6, "lastWasDelimiter":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_2

    .line 291
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ltz v8, :cond_0

    .line 292
    const/4 v6, 0x1

    .line 290
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 295
    :cond_0
    if-eqz v6, :cond_1

    add-int/lit8 v1, v1, 0x1

    .line 296
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 299
    :cond_2
    new-array v0, v1, [Ljava/lang/String;

    .line 300
    .local v0, "arr":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 301
    .local v3, "index":I
    const/4 v5, -0x1

    .line 302
    .local v5, "lastStart":I
    const/4 v2, 0x0

    move v4, v3

    .end local v3    # "index":I
    .local v4, "index":I
    :goto_2
    if-gt v2, v7, :cond_5

    .line 303
    if-eq v2, v7, :cond_3

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ltz v8, :cond_4

    .line 304
    :cond_3
    if-ltz v5, :cond_7

    .line 305
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "index":I
    .restart local v3    # "index":I
    invoke-virtual {p0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v4

    .line 307
    :goto_3
    const/4 v5, -0x1

    .line 302
    :goto_4
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    .end local v3    # "index":I
    .restart local v4    # "index":I
    goto :goto_2

    .line 310
    :cond_4
    if-gez v5, :cond_6

    move v5, v2

    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_4

    .line 313
    .end local v3    # "index":I
    .restart local v4    # "index":I
    :cond_5
    return-object v0

    :cond_6
    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_4

    .end local v3    # "index":I
    .restart local v4    # "index":I
    :cond_7
    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_3
.end method


# virtual methods
.method public getBestContact()Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->query:Ljava/lang/String;

    return-object v0
.end method

.method public getSortedContacts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortedContacts:Ljava/util/List;

    return-object v0
.end method

.method public hasStarted()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->hasStarted:Z

    return v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->doneRunning:Z

    return v0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortContacts()V

    .line 332
    return-void
.end method

.method public scoreContacts(Z)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 6
    .param p1, "mixedForPhone"    # Z

    .prologue
    const/4 v5, 0x1

    .line 70
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matches:Ljava/util/List;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matches:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_8

    .line 71
    const/4 v1, 0x0

    .line 72
    .local v1, "foundBest":Z
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matches:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 73
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const-string/jumbo v3, "new_contact_match_algo"

    invoke-static {v3, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 74
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;)F

    move-result v3

    iput v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 76
    :cond_1
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactType;->getPreferredTarget()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->requestedTypes:[I

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeScores(I[I)V

    .line 80
    if-eqz p1, :cond_2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_2

    .line 81
    iget v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    const v4, 0x3dcccccd    # 0.1f

    mul-float/2addr v3, v4

    iput v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 86
    :cond_2
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v3, :cond_3

    if-eqz v1, :cond_3

    iget v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget v4, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 87
    :cond_3
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->mru:Lcom/vlingo/core/internal/contacts/mru/MRU;

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeMRUScore(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 88
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->mru:Lcom/vlingo/core/internal/contacts/mru/MRU;

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeMRUScoresForData(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 90
    if-eqz v1, :cond_4

    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->compareTo(Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 91
    :cond_4
    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 92
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->sortDetails()V

    .line 93
    const/4 v1, 0x1

    goto :goto_0

    .line 100
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_5
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matches:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 101
    .restart local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget v4, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget v5, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->pruningThreshold:F

    sub-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-lez v3, :cond_6

    .line 104
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortedContacts:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 108
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_7
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matchExtraSortedContacts()V

    .line 110
    .end local v1    # "foundBest":Z
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_8
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->bestContact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    return-object v3
.end method

.method public sortContacts()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 335
    iput-boolean v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->hasStarted:Z

    .line 341
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->matches:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 342
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactType;->getPreferredTarget()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->requestedTypes:[I

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeScores(I[I)V

    .line 343
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->mru:Lcom/vlingo/core/internal/contacts/mru/MRU;

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeMRUScore(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 344
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->mru:Lcom/vlingo/core/internal/contacts/mru/MRU;

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeMRUScoresForData(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 345
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->sortDetails()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 380
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v1

    .line 381
    .local v1, "ex":Ljava/lang/RuntimeException;
    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->LOG:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Caught RuntimeException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " stack trace: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 383
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    invoke-interface {v3}, Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;->onAsyncSortingFailed()V

    .line 385
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :cond_0
    :goto_1
    return-void

    .line 355
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortedContacts:Ljava/util/List;

    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactMatch$ReverseComparator;

    invoke-direct {v4}, Lcom/vlingo/core/internal/contacts/ContactMatch$ReverseComparator;-><init>()V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 364
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortedContacts:Ljava/util/List;

    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactSortRequest$ReverseComparator;

    invoke-direct {v4}, Lcom/vlingo/core/internal/contacts/ContactSortRequest$ReverseComparator;-><init>()V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 374
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->doneRunning:Z

    .line 376
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    if-eqz v3, :cond_0

    .line 377
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortedContacts:Ljava/util/List;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;->onAsyncSortingUpdated(Ljava/util/List;)V

    .line 378
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactSortRequest;->sortedContacts:Ljava/util/List;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;->onAsyncSortingFinished(Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
