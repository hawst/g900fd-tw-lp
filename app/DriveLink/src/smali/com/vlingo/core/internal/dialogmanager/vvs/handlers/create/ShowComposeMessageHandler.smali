.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowComposeMessageHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private allMatches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field protected forwardedMessage:Ljava/lang/String;

.field protected messages:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation
.end field

.field protected replyRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private extractAddresses(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)Ljava/util/List;
    .locals 5
    .param p1, "messageType"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 202
    .local v0, "addresses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v3

    .line 203
    .local v3, "recipients":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;>;"
    if-eqz v3, :cond_1

    .line 204
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 205
    .local v2, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 206
    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 211
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_1
    const/4 v0, 0x0

    .end local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    return-object v0
.end method

.method private extractContactNames(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "recipients"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 216
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 217
    .local v3, "receiverName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->allMatches:Ljava/util/List;

    invoke-static {v6, p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRecipientContacts(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v5

    .line 218
    .local v5, "recipientContactData":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    .line 219
    .local v2, "rawPhoneNumbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v5, :cond_1

    .line 220
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 221
    .local v4, "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v6, v4, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v6, :cond_0

    .line 222
    iget-object v6, v4, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v6, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 224
    :cond_0
    const-string/jumbo v6, ""

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 228
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v4    # "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_1
    if-eqz v2, :cond_2

    .line 229
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 230
    .local v1, "number":Ljava/lang/String;
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 233
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "number":Ljava/lang/String;
    :cond_2
    return-object v3
.end method

.method private extractPhoneNumbers(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "recipients"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 237
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 238
    .local v3, "receiverAddresses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->allMatches:Ljava/util/List;

    invoke-static {v6, p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRecipientContacts(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v5

    .line 239
    .local v5, "recipientContactData":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    .line 240
    .local v2, "rawPhoneNumbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v5, :cond_0

    .line 241
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 242
    .local v4, "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v6, v4, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 245
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v4    # "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_0
    if-eqz v2, :cond_1

    .line 246
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 247
    .local v1, "number":Ljava/lang/String;
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 250
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "number":Ljava/lang/String;
    :cond_1
    return-object v3
.end method

.method private sendEmail(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    .locals 2
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 191
    .local p3, "recipients":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_EMAIL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->contacts(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->subject(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->queue()V

    .line 198
    :cond_0
    return-void
.end method

.method private sendSMS(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    .locals 6
    .param p1, "content"    # Ljava/lang/String;
    .param p2, "recipients"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 172
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->extractAddresses(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)Ljava/util/List;

    move-result-object v3

    .line 173
    .local v3, "numbers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 174
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v5, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    invoke-interface {v4, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->addresses(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    move-result-object v4

    invoke-interface {v4, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->queue()V

    .line 180
    :cond_0
    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    .line 181
    :try_start_0
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->extractAddresses(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)Ljava/util/List;

    move-result-object v1

    .line 182
    .local v1, "contactNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_1

    .line 183
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 184
    .local v0, "contact":Ljava/lang/String;
    new-instance v4, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;

    invoke-direct {v4, p1, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 187
    .end local v0    # "contact":Ljava/lang/String;
    .end local v1    # "contactNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v4

    .line 188
    :cond_1
    return-void
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 300
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 301
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 302
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 303
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 293
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 294
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 295
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 296
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 28
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 67
    invoke-super/range {p0 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 69
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v25

    const-string/jumbo v26, "car-sms-message"

    invoke-virtual/range {v25 .. v26}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 71
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v25

    sget-object v26, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface/range {v25 .. v26}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/List;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->allMatches:Ljava/util/List;

    .line 73
    const-string/jumbo v25, "subject"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 74
    .local v22, "subject":Ljava/lang/String;
    const-string/jumbo v25, "message"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 75
    .local v23, "text":Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 76
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->messages:Ljava/util/LinkedList;

    move-object/from16 v25, v0

    if-eqz v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->messages:Ljava/util/LinkedList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_0

    .line 77
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getText()Ljava/lang/String;

    move-result-object v23

    .line 80
    :cond_0
    const-string/jumbo v25, "msgtype"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 81
    .local v15, "msgType":Ljava/lang/String;
    new-instance v14, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    const-string/jumbo v25, ""

    const-string/jumbo v26, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-direct {v14, v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .local v14, "messageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    new-instance v25, Ljava/util/Date;

    invoke-direct/range {v25 .. v25}, Ljava/util/Date;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/util/Date;->getTime()J

    move-result-wide v25

    move-wide/from16 v0, v25

    invoke-virtual {v14, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setTimeStamp(J)V

    .line 84
    const-string/jumbo v25, "recipients"

    const/16 v26, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v21

    .line 86
    .local v21, "recipients":Ljava/lang/String;
    const/16 v20, 0x0

    .line 87
    .local v20, "recipientContactData":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/16 v17, 0x0

    .line 88
    .local v17, "rawPhoneNumbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->allMatches:Ljava/util/List;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->allMatches:Ljava/util/List;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_1

    .line 90
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->allMatches:Ljava/util/List;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRecipientContacts(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v20

    .line 97
    :cond_1
    :try_start_1
    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v17

    .line 102
    if-eqz v20, :cond_3

    .line 103
    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 104
    .local v19, "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v19, :cond_2

    .line 105
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 106
    .local v4, "address":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_2

    .line 107
    new-instance v18, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .local v18, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    goto :goto_0

    .line 91
    .end local v4    # "address":Ljava/lang/String;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v18    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    .end local v19    # "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :catch_0
    move-exception v8

    .line 92
    .local v8, "e":Lorg/json/JSONException;
    sget-object v25, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v26, "Error extracting json contacts and numbers from recipients parameter"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const/16 v25, 0x0

    .line 164
    .end local v8    # "e":Lorg/json/JSONException;
    :goto_1
    return v25

    .line 98
    :catch_1
    move-exception v9

    .line 99
    .local v9, "e1":Lorg/json/JSONException;
    sget-object v25, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v26, "Error extracting json numbers from recipients parameter"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/16 v25, 0x0

    goto :goto_1

    .line 113
    .end local v9    # "e1":Lorg/json/JSONException;
    :cond_3
    if-eqz v17, :cond_4

    .line 114
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 115
    .local v16, "number":Ljava/lang/String;
    new-instance v18, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    const-string/jumbo v25, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    .restart local v18    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    goto :goto_2

    .line 119
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v16    # "number":Ljava/lang/String;
    .end local v18    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->replyRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    move-object/from16 v25, v0

    if-eqz v25, :cond_5

    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->replyRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 123
    :cond_5
    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setSubject(Ljava/lang/String;)V

    .line 124
    invoke-virtual {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setType(Ljava/lang/String;)V

    .line 126
    const/4 v7, 0x0

    .line 127
    .local v7, "decor":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    const-string/jumbo v25, "confirm"

    const/16 v26, 0x1

    const/16 v27, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v11

    .line 128
    .local v11, "isConfirm":Z
    const-string/jumbo v25, "execute"

    const/16 v26, 0x1

    const/16 v27, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v12

    .line 129
    .local v12, "isExecute":Z
    if-nez v11, :cond_8

    if-eqz v12, :cond_8

    const/4 v13, 0x1

    .line 130
    .local v13, "isExecuteStep":Z
    :goto_3
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeSendButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->cancelButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v5

    .line 131
    .local v5, "cancelButton":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    if-nez v13, :cond_9

    invoke-virtual {v14}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_9

    .line 132
    move-object v7, v5

    .line 140
    :cond_6
    :goto_4
    invoke-virtual {v14}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getType()Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, "email"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    sget-object v24, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeEmail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 141
    .local v24, "wk":Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    :goto_5
    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move-object/from16 v2, p0

    invoke-interface {v0, v1, v7, v14, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 143
    const-string/jumbo v25, "execute"

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 144
    const-string/jumbo v25, "message"

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 145
    .local v6, "content":Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 146
    invoke-virtual {v14}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_c

    .line 147
    invoke-virtual {v14}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v6

    .line 154
    :cond_7
    :try_start_2
    const-string/jumbo v25, "msgtype"

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, "email"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 155
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->sendEmail(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 159
    :goto_6
    const/16 v25, 0x1

    goto/16 :goto_1

    .line 129
    .end local v5    # "cancelButton":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .end local v6    # "content":Ljava/lang/String;
    .end local v13    # "isExecuteStep":Z
    .end local v24    # "wk":Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    :cond_8
    const/4 v13, 0x0

    goto/16 :goto_3

    .line 133
    .restart local v5    # "cancelButton":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .restart local v13    # "isExecuteStep":Z
    :cond_9
    invoke-virtual {v14}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v25

    if-nez v25, :cond_a

    .line 134
    move-object v7, v5

    goto :goto_4

    .line 135
    :cond_a
    invoke-virtual {v14}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->isEmpty()Z

    move-result v25

    if-eqz v25, :cond_6

    .line 136
    move-object v7, v5

    goto/16 :goto_4

    .line 140
    :cond_b
    sget-object v24, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    goto :goto_5

    .line 149
    .restart local v6    # "content":Ljava/lang/String;
    .restart local v24    # "wk":Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    :cond_c
    new-instance v25, Ljava/security/InvalidParameterException;

    const-string/jumbo v26, "Null or empty message on execute step."

    invoke-direct/range {v25 .. v26}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 157
    :cond_d
    :try_start_3
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v14}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->sendSMS(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_6

    .line 160
    :catch_2
    move-exception v8

    .line 161
    .restart local v8    # "e":Lorg/json/JSONException;
    new-instance v25, Ljava/security/InvalidParameterException;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "Invalid format for recipients action parameter: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 164
    .end local v6    # "content":Ljava/lang/String;
    .end local v8    # "e":Lorg/json/JSONException;
    :cond_e
    const/16 v25, 0x0

    goto/16 :goto_1
.end method

.method protected getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    const-string/jumbo v0, " "

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 260
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 261
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;-><init>()V

    .line 262
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 285
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 264
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;-><init>()V

    .line 265
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 266
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.BodyChange"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 267
    const-string/jumbo v1, ""

    .line 268
    .local v1, "message":Ljava/lang/String;
    const-string/jumbo v2, "message"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 269
    const-string/jumbo v2, "message"

    invoke-static {p1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamString(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 271
    :cond_3
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;

    const-string/jumbo v2, "message"

    invoke-direct {v0, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 273
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    .end local v1    # "message":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.SubjectChange"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 274
    const-string/jumbo v2, "subject"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 275
    const-string/jumbo v2, "subject"

    invoke-static {p1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamString(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 276
    .restart local v1    # "message":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;

    const-string/jumbo v2, "subject"

    invoke-direct {v0, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    .restart local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 279
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    .end local v1    # "message":Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.EditTextClicked"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 280
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishTurn()V

    goto/16 :goto_0

    .line 283
    :cond_6
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
