.class public Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactMatcher;
.super Lcom/vlingo/core/internal/contacts/ContactMatcher;
.source "PhoneDisambiguationContactMatcher.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/vlingo/core/internal/contacts/ContactMatchListener;
    .param p3, "actionType"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p4, "phoneTypes"    # [I
    .param p5, "emailTypes"    # [I
    .param p6, "socialTypes"    # [I
    .param p7, "addressTypes"    # [I
    .param p8, "query"    # Ljava/lang/String;
    .param p9, "confidenceScore"    # F
    .param p10, "mAutoActionType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/contacts/ContactMatchListener;",
            "Lcom/vlingo/core/internal/contacts/ContactType;",
            "[I[I[I[I",
            "Ljava/lang/String;",
            "FI",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p11, "superList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-direct/range {p0 .. p11}, Lcom/vlingo/core/internal/contacts/ContactMatcher;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected getContactFetchAndSortRequest()Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;
    .locals 6

    .prologue
    .line 21
    new-instance v0, Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactFetchAndSortRequest;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactMatcher;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactMatcher;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactMatcher;->getActionType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactMatcher;->getRequestedTypes()[I

    move-result-object v4

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactFetchAndSortRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;[ILcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;)V

    return-object v0
.end method
