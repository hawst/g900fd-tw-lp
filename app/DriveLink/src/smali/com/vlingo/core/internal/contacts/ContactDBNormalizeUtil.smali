.class public Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;
.super Ljava/lang/Object;
.source "ContactDBNormalizeUtil.java"


# static fields
.field public static final NORMALIZED_MIME_EXTENSION:Ljava/lang/String; = "_svoice_normalization"

.field private static final NORMALIZERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field protected static normalizeValuesCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->TAG:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValuesCache:Ljava/util/Map;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    .line 48
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/SpacesContactNameNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/SpacesContactNameNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/DowncaseContactNameNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/DowncaseContactNameNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/AccentedCharactersContactNameNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/AccentedCharactersContactNameNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/JapaneseHonorificsContactNameNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/JapaneseHonorificsContactNameNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/LatinNumericContactNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/LatinNumericContactNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cleanCache()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValuesCache:Ljava/util/Map;

    .line 88
    return-void
.end method

.method private static cleanNormalizers()V
    .locals 3

    .prologue
    .line 209
    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;

    .line 210
    .local v1, "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    instance-of v2, v1, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;

    if-eqz v2, :cond_0

    .line 211
    check-cast v1, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;

    .end local v1    # "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->cleanTool()V

    goto :goto_0

    .line 214
    :cond_1
    return-void
.end method

.method public static normalizeContactData(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/util/List;)V
    .locals 33
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "origUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p2, "rawContactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const-string/jumbo v3, ","

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/StringUtils;->join(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 95
    .local v20, "joinedIds":Ljava/lang/String;
    const/16 v3, 0x9

    new-array v13, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "data1"

    aput-object v4, v13, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "data2"

    aput-object v4, v13, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "data3"

    aput-object v4, v13, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "data4"

    aput-object v4, v13, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "data5"

    aput-object v4, v13, v3

    const/4 v3, 0x5

    const-string/jumbo v4, "data6"

    aput-object v4, v13, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "data7"

    aput-object v4, v13, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "data8"

    aput-object v4, v13, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "data9"

    aput-object v4, v13, v3

    .line 96
    .local v13, "dataColumnNames":[Ljava/lang/String;
    const/16 v3, 0x11

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "mimetype"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "contact_id"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "raw_contact_id"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "lookup"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "display_name"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string/jumbo v4, "starred"

    aput-object v4, v5, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "data1"

    aput-object v4, v5, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "data2"

    aput-object v4, v5, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "data3"

    aput-object v4, v5, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "data4"

    aput-object v4, v5, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "data5"

    aput-object v4, v5, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "data6"

    aput-object v4, v5, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "data7"

    aput-object v4, v5, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "data8"

    aput-object v4, v5, v3

    const/16 v3, 0xe

    const-string/jumbo v4, "data9"

    aput-object v4, v5, v3

    const/16 v3, 0xf

    const-string/jumbo v4, "times_contacted"

    aput-object v4, v5, v3

    const/16 v3, 0x10

    const-string/jumbo v4, "phonetic_name"

    aput-object v4, v5, v3

    .line 98
    .local v5, "projection":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 100
    .local v11, "c":Landroid/database/Cursor;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "caller_is_syncadapter"

    const-string/jumbo v6, "true"

    invoke-virtual {v3, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v31

    .line 102
    .local v31, "uri":Landroid/net/Uri;
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v3, 0x0

    const-string/jumbo v4, "vnd.android.cursor.item/name_svoice_normalization"

    aput-object v4, v22, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "vnd.android.cursor.item/nickname_svoice_normalization"

    aput-object v4, v22, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "vnd.android.cursor.item/organization_svoice_normalization"

    aput-object v4, v22, v3

    .line 112
    .local v22, "mimetypeList":[Ljava/lang/String;
    :try_start_0
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    .local v23, "mimetypes":Ljava/lang/StringBuilder;
    const/16 v16, 0x1

    .line 114
    .local v16, "first":Z
    move-object/from16 v9, v22

    .local v9, "arr$":[Ljava/lang/String;
    array-length v0, v9

    move/from16 v21, v0

    .local v21, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v21

    if-ge v0, v1, :cond_2

    aget-object v30, v9, v18

    .line 115
    .local v30, "type":Ljava/lang/String;
    if-eqz v16, :cond_0

    .line 116
    const/16 v16, 0x0

    .line 120
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 118
    :cond_0
    const-string/jumbo v3, ", "

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 195
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v16    # "first":Z
    .end local v18    # "i$":I
    .end local v21    # "len$":I
    .end local v23    # "mimetypes":Ljava/lang/StringBuilder;
    .end local v30    # "type":Ljava/lang/String;
    :catch_0
    move-exception v15

    .line 196
    .local v15, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "normalizeContactData: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v15}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 198
    if-eqz v11, :cond_1

    .line 200
    :try_start_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 206
    .end local v15    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return-void

    .line 123
    .restart local v9    # "arr$":[Ljava/lang/String;
    .restart local v16    # "first":Z
    .restart local v18    # "i$":I
    .restart local v21    # "len$":I
    .restart local v23    # "mimetypes":Ljava/lang/StringBuilder;
    :cond_2
    :try_start_3
    const-string/jumbo v3, "%s IN (%s) AND %s in (%s)"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "raw_contact_id"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    aput-object v20, v4, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "mimetype"

    aput-object v7, v4, v6

    const/4 v6, 0x3

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    .line 124
    .local v32, "where":Ljava/lang/String;
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v14

    .line 133
    .local v14, "deletedItems":I
    const-string/jumbo v3, "%s IN (?,?,?) AND %s IN (%s)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "mimetype"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "raw_contact_id"

    aput-object v7, v4, v6

    const/4 v6, 0x2

    aput-object v20, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x3

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "vnd.android.cursor.item/name"

    aput-object v4, v7, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "vnd.android.cursor.item/organization"

    aput-object v4, v7, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "vnd.android.cursor.item/nickname"

    aput-object v4, v7, v3

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 141
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v28, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual/range {v31 .. v31}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v10

    .line 144
    .local v10, "authority":Ljava/lang/String;
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_3

    .line 145
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->cleanNormalizers()V

    .line 146
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v10, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 198
    if-eqz v11, :cond_1

    .line 200
    :try_start_4
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 201
    :catch_1
    move-exception v15

    .line 202
    .restart local v15    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "normalizeContactData: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v15}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 151
    .end local v15    # "e":Ljava/lang/Exception;
    .end local v18    # "i$":I
    :cond_3
    :try_start_5
    invoke-static/range {v31 .. v31}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v27

    .line 153
    .local v27, "operationBuilder":Landroid/content/ContentProviderOperation$Builder;
    const-string/jumbo v3, "raw_contact_id"

    const-string/jumbo v4, "raw_contact_id"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 154
    const-string/jumbo v3, "mimetype"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "mimetype"

    invoke-interface {v11, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v11, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "_svoice_normalization"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 155
    const-string/jumbo v3, "contact_id"

    const-string/jumbo v4, "contact_id"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 156
    const-string/jumbo v3, "lookup"

    const-string/jumbo v4, "lookup"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 157
    const-string/jumbo v3, "display_name"

    const-string/jumbo v4, "display_name"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 158
    const-string/jumbo v3, "starred"

    const-string/jumbo v4, "starred"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 159
    const-string/jumbo v3, "times_contacted"

    const-string/jumbo v4, "times_contacted"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 160
    const-string/jumbo v3, "phonetic_name"

    const-string/jumbo v4, "phonetic_name"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 162
    const/16 v17, 0x0

    .line 163
    .local v17, "foundNormalizedData":Z
    move-object v9, v13

    array-length v0, v9

    move/from16 v21, v0

    const/16 v18, 0x0

    .restart local v18    # "i$":I
    move/from16 v19, v18

    .end local v18    # "i$":I
    .local v19, "i$":I
    :goto_3
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_8

    aget-object v12, v9, v19

    .line 166
    .local v12, "columnName":Ljava/lang/String;
    invoke-interface {v11, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 167
    .local v25, "name":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 168
    move-object/from16 v26, v25

    .line 170
    .local v26, "normalizedName":Ljava/lang/String;
    const/16 v29, 0x0

    .line 171
    .local v29, "saveNormalizedAnyway":Z
    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .end local v19    # "i$":I
    .local v18, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;

    .line 172
    .local v24, "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    invoke-virtual/range {v24 .. v24}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;->scoringOnly()Z

    move-result v3

    if-nez v3, :cond_4

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;->canNormalize(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 173
    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 174
    move-object/from16 v0, v24

    instance-of v3, v0, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;

    if-eqz v3, :cond_4

    .line 175
    const/16 v29, 0x1

    goto :goto_4

    .line 180
    .end local v24    # "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    :cond_5
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    if-eqz v29, :cond_7

    .line 181
    :cond_6
    const/16 v17, 0x1

    .line 182
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v12, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 163
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v26    # "normalizedName":Ljava/lang/String;
    .end local v29    # "saveNormalizedAnyway":Z
    :cond_7
    add-int/lit8 v18, v19, 0x1

    .local v18, "i$":I
    move/from16 v19, v18

    .end local v18    # "i$":I
    .restart local v19    # "i$":I
    goto :goto_3

    .line 188
    .end local v12    # "columnName":Ljava/lang/String;
    .end local v25    # "name":Ljava/lang/String;
    :cond_8
    if-eqz v17, :cond_9

    .line 189
    invoke-virtual/range {v27 .. v27}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_9
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 193
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->cleanNormalizers()V

    .line 194
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v10, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 198
    if-eqz v11, :cond_1

    .line 200
    :try_start_6
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_2

    .line 201
    :catch_2
    move-exception v15

    .line 202
    .restart local v15    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "normalizeContactData: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v15}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 201
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v10    # "authority":Ljava/lang/String;
    .end local v14    # "deletedItems":I
    .end local v16    # "first":Z
    .end local v17    # "foundNormalizedData":Z
    .end local v19    # "i$":I
    .end local v21    # "len$":I
    .end local v23    # "mimetypes":Ljava/lang/StringBuilder;
    .end local v27    # "operationBuilder":Landroid/content/ContentProviderOperation$Builder;
    .end local v28    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v32    # "where":Ljava/lang/String;
    :catch_3
    move-exception v15

    .line 202
    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "normalizeContactData: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v15}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 198
    .end local v15    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v11, :cond_a

    .line 200
    :try_start_7
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 203
    :cond_a
    :goto_5
    throw v3

    .line 201
    :catch_4
    move-exception v15

    .line 202
    .restart local v15    # "e":Ljava/lang/Exception;
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "normalizeContactData: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v15}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public static normalizeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 66
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValuesCache:Ljava/util/Map;

    if-nez v4, :cond_0

    .line 67
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValuesCache:Ljava/util/Map;

    .line 69
    :cond_0
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValuesCache:Ljava/util/Map;

    invoke-interface {v4, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 70
    .local v0, "cachedValue":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 80
    .end local v0    # "cachedValue":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 73
    .restart local v0    # "cachedValue":Ljava/lang/String;
    :cond_1
    move-object v3, p0

    .line 74
    .local v3, "origQeury":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->NORMALIZERS:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;

    .line 75
    .local v2, "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    invoke-virtual {v2, p0}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;->canNormalize(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 76
    invoke-virtual {v2, p0}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 79
    .end local v2    # "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    :cond_3
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValuesCache:Ljava/util/Map;

    invoke-interface {v4, v3, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, p0

    .line 80
    goto :goto_0
.end method
