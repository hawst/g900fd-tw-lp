.class public Lcom/vlingo/core/internal/vlservice/ServiceEnableBootReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ServiceEnableBootReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 35
    const-string/jumbo v4, "iux_complete"

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 36
    .local v2, "isIuxComplete":Z
    const-string/jumbo v4, "tos_accepted"

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 37
    .local v3, "isTosAccepted":Z
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 40
    if-eqz p1, :cond_0

    .line 44
    :try_start_0
    new-instance v0, Landroid/content/ComponentName;

    const-class v4, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    invoke-direct {v0, p1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 45
    .local v0, "comp":Landroid/content/ComponentName;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-virtual {v4, v0, v5, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .end local v0    # "comp":Landroid/content/ComponentName;
    :cond_0
    :goto_0
    return-void

    .line 47
    :catch_0
    move-exception v1

    .line 48
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v4, "ServiceEnableBootReceiver"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
