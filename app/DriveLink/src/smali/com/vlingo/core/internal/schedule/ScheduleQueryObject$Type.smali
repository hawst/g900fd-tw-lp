.class public final enum Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;
.super Ljava/lang/Enum;
.source "ScheduleQueryObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

.field public static final enum BEGIN:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

.field public static final enum END:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

.field public static final enum INTERSECT:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    const-string/jumbo v1, "BEGIN"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->BEGIN:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    .line 32
    new-instance v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    const-string/jumbo v1, "END"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->END:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    .line 33
    new-instance v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    const-string/jumbo v1, "INTERSECT"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->INTERSECT:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    .line 30
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    sget-object v1, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->BEGIN:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->END:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->INTERSECT:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->$VALUES:[Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static of(Ljava/lang/String;)Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;
    .locals 5
    .param p0, "typeStr"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-static {}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->values()[Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 37
    .local v3, "type":Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 41
    .end local v3    # "type":Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;
    :goto_1
    return-object v3

    .line 36
    .restart local v3    # "type":Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 41
    .end local v3    # "type":Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->BEGIN:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->$VALUES:[Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    return-object v0
.end method
