.class public Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;
.super Ljava/lang/Object;
.source "LocalSearchListing.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Field"
.end annotation


# instance fields
.field public isMoreAvailable:Z

.field final synthetic this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Ljava/lang/String;)V
    .locals 1
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->value:Ljava/lang/String;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->isMoreAvailable:Z

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Z)V
    .locals 1
    .param p2, "isMoreAvailable"    # Z

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->value:Ljava/lang/String;

    .line 50
    iput-boolean p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->isMoreAvailable:Z

    .line 51
    return-void
.end method


# virtual methods
.method public hasMoreInfo(Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;)Z
    .locals 2
    .param p1, "oldField"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->value:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->value:Ljava/lang/String;

    iget-object v1, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " Field { value [ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ], isMoreAvailable ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->isMoreAvailable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
