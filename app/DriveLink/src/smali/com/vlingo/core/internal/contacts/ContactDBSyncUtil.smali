.class public Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil;
.super Ljava/lang/Object;
.source "ContactDBSyncUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$1;,
        Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;
    }
.end annotation


# static fields
.field public static final SYNC_MIME_TYPES:Ljava/lang/String; = "mimetype in (\'vnd.android.cursor.item/nickname\', \'vnd.android.cursor.item/organization\', \'vnd.android.cursor.item/name\')"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    return-void
.end method

.method protected static pullContactDataWithWhere(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;Ljava/lang/String;[Ljava/lang/String;)[Landroid/content/ContentValues;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "table"    # Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 161
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v8, "contentValuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const/4 v9, 0x0

    .line 163
    .local v9, "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getFieldsMap()Ljava/util/LinkedHashMap;

    move-result-object v11

    .line 164
    .local v11, "fieldMap":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;>;"
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 168
    .local v6, "columnIndexMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getContactsContractUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getRenamedFieldsMap()Ljava/util/LinkedHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 174
    const-wide/16 v14, 0x0

    .line 175
    .local v14, "loopCount":J
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 177
    :cond_0
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 178
    .local v7, "contentValues":Landroid/content/ContentValues;
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getRenamedFieldsMap()Ljava/util/LinkedHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 179
    .local v10, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    .line 180
    .local v13, "index":Ljava/lang/Integer;
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "_id"

    if-eq v0, v1, :cond_1

    .line 183
    if-nez v13, :cond_2

    .line 184
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 185
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v6, v0, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :cond_2
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$1;->$SwitchMap$com$vlingo$core$internal$contacts$contentprovider$FieldType:[I

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 206
    :goto_1
    const-wide/16 v0, 0x1

    add-long/2addr v14, v0

    .line 207
    goto :goto_0

    .line 191
    :pswitch_0
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 200
    :catch_0
    move-exception v0

    goto :goto_1

    .line 194
    :pswitch_1
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 213
    .end local v7    # "contentValues":Landroid/content/ContentValues;
    .end local v10    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "index":Ljava/lang/Integer;
    .end local v14    # "loopCount":J
    :catch_1
    move-exception v0

    .line 219
    if-eqz v9, :cond_3

    .line 220
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 223
    :cond_3
    :goto_2
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/content/ContentValues;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/ContentValues;

    return-object v0

    .line 197
    .restart local v7    # "contentValues":Landroid/content/ContentValues;
    .restart local v10    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v13    # "index":Ljava/lang/Integer;
    .restart local v14    # "loopCount":J
    :pswitch_2
    :try_start_2
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 219
    .end local v7    # "contentValues":Landroid/content/ContentValues;
    .end local v10    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "index":Ljava/lang/Integer;
    .end local v14    # "loopCount":J
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_4

    .line 220
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 208
    .restart local v7    # "contentValues":Landroid/content/ContentValues;
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v14    # "loopCount":J
    :cond_5
    :try_start_3
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 219
    .end local v7    # "contentValues":Landroid/content/ContentValues;
    .end local v12    # "i$":Ljava/util/Iterator;
    :cond_6
    if-eqz v9, :cond_3

    .line 220
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static syncContacts(Landroid/content/Context;)V
    .locals 23
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 41
    .local v3, "resolver":Landroid/content/ContentResolver;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri()Landroid/net/Uri;

    move-result-object v20

    .line 43
    .local v20, "uri":Landroid/net/Uri;
    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    .line 44
    .local v17, "origContacts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;>;"
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 46
    .local v16, "localContacts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;>;"
    new-instance v19, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;

    invoke-direct/range {v19 .. v19}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;-><init>()V

    .line 48
    .local v19, "table":Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 49
    .local v13, "idsToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 52
    .local v14, "idsToSync":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v9, 0x0

    .line 54
    .local v9, "cur":Landroid/database/Cursor;
    :try_start_0
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "raw_contact_id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "version"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "starred"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string/jumbo v7, "times_contacted"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string/jumbo v7, "lookup"

    aput-object v7, v5, v6

    const-string/jumbo v6, "mimetype in (\'vnd.android.cursor.item/nickname\', \'vnd.android.cursor.item/organization\', \'vnd.android.cursor.item/name\')"

    const/4 v7, 0x0

    const-string/jumbo v8, "raw_contact_id"

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 55
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 57
    :cond_0
    const/4 v4, 0x0

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;

    const/4 v6, 0x1

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v9, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x3

    invoke-interface {v9, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/16 v22, 0x4

    move/from16 v0, v22

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v5, v6, v7, v8, v0}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 64
    :cond_1
    if-eqz v9, :cond_2

    .line 65
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 71
    :cond_2
    :goto_0
    const/4 v4, 0x5

    :try_start_1
    new-array v5, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v6, "DISTINCT raw_contact_id"

    aput-object v6, v5, v4

    const/4 v4, 0x1

    const-string/jumbo v6, "version"

    aput-object v6, v5, v4

    const/4 v4, 0x2

    const-string/jumbo v6, "starred"

    aput-object v6, v5, v4

    const/4 v4, 0x3

    const-string/jumbo v6, "times_contacted"

    aput-object v6, v5, v4

    const/4 v4, 0x4

    const-string/jumbo v6, "lookup"

    aput-object v6, v5, v4

    const-string/jumbo v6, "mimetype in (\'vnd.android.cursor.item/nickname\', \'vnd.android.cursor.item/organization\', \'vnd.android.cursor.item/name\')"

    const/4 v7, 0x0

    const-string/jumbo v8, "raw_contact_id"

    move-object/from16 v4, v20

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v9

    .line 73
    if-eqz v9, :cond_4

    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 75
    :cond_3
    const/4 v4, 0x0

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;

    const/4 v6, 0x1

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v9, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x3

    invoke-interface {v9, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/16 v22, 0x4

    move/from16 v0, v22

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v5, v6, v7, v8, v0}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v4

    if-nez v4, :cond_3

    .line 82
    :cond_4
    if-eqz v9, :cond_5

    .line 83
    :try_start_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 91
    :cond_5
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 92
    .local v10, "entries":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;>;"
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;

    .line 93
    .local v15, "localCompareData":Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;
    if-nez v15, :cond_9

    .line 94
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v14, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 60
    .end local v10    # "entries":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v15    # "localCompareData":Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;
    :catch_0
    move-exception v4

    .line 64
    if-eqz v9, :cond_2

    .line 65
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 64
    :catchall_0
    move-exception v4

    if-eqz v9, :cond_7

    .line 65
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v4

    .line 78
    :catch_1
    move-exception v4

    .line 82
    if-eqz v9, :cond_5

    .line 83
    :try_start_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 86
    :catch_2
    move-exception v4

    goto :goto_1

    .line 82
    :catchall_1
    move-exception v4

    if-eqz v9, :cond_8

    .line 83
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v4
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 95
    .restart local v10    # "entries":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;>;"
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v15    # "localCompareData":Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;
    :cond_9
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v15}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->getVersion()Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->getVersion()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v15}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->getStarred()Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->getStarred()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v15}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->getTimesContacted()Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->getTimesContacted()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v15}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->getLookup()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->getLookup()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 99
    :cond_a
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v14, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v13, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 104
    .end local v10    # "entries":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;>;"
    .end local v15    # "localCompareData":Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;
    :cond_b
    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_c
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 105
    .restart local v10    # "entries":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;>;"
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_c

    .line 106
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v13, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 111
    .end local v10    # "entries":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;>;"
    :cond_d
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    .local v18, "sb":Ljava/lang/StringBuilder;
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    .line 113
    .local v11, "i":Ljava/lang/Long;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_e

    const-string/jumbo v4, ", "

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_e
    invoke-virtual {v11}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 116
    .end local v11    # "i":Ljava/lang/Long;
    :cond_f
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "raw_contact_id in ( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " )"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 118
    .local v21, "where":Ljava/lang/String;
    const/4 v4, 0x0

    :try_start_5
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 123
    :goto_5
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->cleanCache()V

    .line 125
    :try_start_6
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2, v14}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil;->updateContactDataWithLatestVersions(Landroid/content/Context;Landroid/net/Uri;Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;Ljava/util/List;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 129
    :goto_6
    return-void

    .line 119
    :catch_3
    move-exception v4

    goto :goto_5

    .line 126
    :catch_4
    move-exception v4

    goto :goto_6
.end method

.method private static updateContactDataWithLatestVersions(Landroid/content/Context;Landroid/net/Uri;Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;Ljava/util/List;)V
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "table"    # Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 227
    .local p3, "idsToSync":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v10, 0x0

    .line 228
    .local v10, "selection":Ljava/lang/StringBuilder;
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v9

    .line 231
    .local v9, "pairsQuantity":I
    const/4 v11, 0x0

    .line 232
    .local v11, "start":I
    const/16 v12, 0x64

    .line 233
    .local v12, "step":I
    const/16 v15, 0x64

    if-le v9, v15, :cond_2

    const/16 v3, 0x64

    .line 234
    .local v3, "end":I
    :goto_0
    if-eq v11, v9, :cond_7

    .line 235
    sub-int v15, v3, v11

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    if-nez v10, :cond_4

    .line 236
    :cond_0
    new-instance v10, Ljava/lang/StringBuilder;

    .end local v10    # "selection":Ljava/lang/StringBuilder;
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 237
    .restart local v10    # "selection":Ljava/lang/StringBuilder;
    const-string/jumbo v15, "raw_contact_id"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, " in ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    move v4, v11

    .local v4, "i":I
    :goto_1
    if-ge v4, v3, :cond_3

    .line 239
    const-string/jumbo v15, "?"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    add-int/lit8 v15, v4, 0x1

    if-eq v15, v3, :cond_1

    .line 241
    const-string/jumbo v15, ","

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .end local v3    # "end":I
    .end local v4    # "i":I
    :cond_2
    move v3, v9

    .line 233
    goto :goto_0

    .line 244
    .restart local v3    # "end":I
    .restart local v4    # "i":I
    :cond_3
    const-string/jumbo v15, ")"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    .end local v4    # "i":I
    :cond_4
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 247
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    move-object/from16 v0, p3

    invoke-interface {v0, v11, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v14

    .line 248
    .local v14, "subList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v15

    new-array v13, v15, [Ljava/lang/String;

    .line 249
    .local v13, "stringArray":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 250
    .restart local v4    # "i":I
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 251
    .local v7, "id":J
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .local v5, "i":I
    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v4

    move v4, v5

    .end local v5    # "i":I
    .restart local v4    # "i":I
    goto :goto_2

    .line 253
    .end local v7    # "id":J
    :cond_5
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1, v15, v13}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil;->pullContactDataWithWhere(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;Ljava/lang/String;[Ljava/lang/String;)[Landroid/content/ContentValues;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v15}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    .line 254
    move-object/from16 v0, p1

    invoke-static {v2, v0, v14}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeContactData(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/util/List;)V

    .line 255
    move-object/from16 v0, p1

    invoke-static {v2, v0, v14}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->encodeContactData(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/util/List;)V

    .line 256
    move v11, v3

    .line 257
    add-int/lit8 v15, v3, 0x64

    if-le v9, v15, :cond_6

    add-int/lit8 v3, v3, 0x64

    .line 258
    :goto_3
    goto/16 :goto_0

    :cond_6
    move v3, v9

    .line 257
    goto :goto_3

    .line 259
    .end local v2    # "contentResolver":Landroid/content/ContentResolver;
    .end local v4    # "i":I
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v13    # "stringArray":[Ljava/lang/String;
    .end local v14    # "subList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :cond_7
    return-void
.end method
