.class public Lcom/vlingo/core/internal/contacts/ContactDBUtil;
.super Ljava/lang/Object;
.source "ContactDBUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/IContactDBUtil;


# static fields
.field public static final EVENT_LUNAR_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.pantech.cursor.item/lunar_event"

.field protected static final RULE_SETS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/vlingo/core/internal/contacts/ContactDBUtil;


# instance fields
.field private mSkipNormalCheck:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->TAG:Ljava/lang/String;

    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    .line 62
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/FrenchContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/FrenchContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->mSkipNormalCheck:Z

    return-void
.end method

.method protected static applyScores(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;Lcom/vlingo/core/internal/contacts/ContactExitCriteria;Ljava/lang/String;)V
    .locals 6
    .param p2, "scorer"    # Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    .param p3, "exitCriteria"    # Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .param p4, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Lcom/vlingo/core/internal/contacts/scoring/ContactScore;",
            "Lcom/vlingo/core/internal/contacts/ContactExitCriteria;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p1, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/high16 v5, 0x40a00000    # 5.0f

    .line 506
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 507
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 508
    .local v2, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {p2, p4, v2}, Lcom/vlingo/core/internal/contacts/scoring/ContactScore;->getScore(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v3

    int-to-float v3, v3

    iput v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 509
    iget v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    float-to-int v3, v3

    invoke-virtual {p3, v3}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->tallyScore(I)Z

    goto :goto_0

    .line 511
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v2    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_0
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 512
    .restart local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 513
    .restart local v2    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    float-to-int v3, v3

    invoke-virtual {p3, v3}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->keepMatch(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 515
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget v4, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    div-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 516
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    :cond_2
    iget v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    div-float/2addr v3, v5

    iput v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    goto :goto_1

    .line 521
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v2    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_3
    return-void
.end method

.method private getContactCursor(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 938
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 943
    :goto_0
    return-object v4

    .line 941
    :cond_0
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "data2"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "data3"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v1, "data1"

    aput-object v1, v2, v0

    .line 942
    .local v2, "proj":[Ljava/lang/String;
    const-string/jumbo v3, "mimetype=\'vnd.android.cursor.item/name\' AND in_visible_group=1"

    .line 943
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0
.end method

.method private getContactDetails(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I
    .locals 46
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 532
    .local p2, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p3, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p4, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v36, 0x0

    .line 533
    .local v36, "count":I
    const/4 v2, 0x7

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "contact_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "data1"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "data2"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "data3"

    aput-object v3, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "mimetype"

    aput-object v3, v4, v2

    const/4 v2, 0x5

    const-string/jumbo v3, "is_super_primary"

    aput-object v3, v4, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "data_id"

    aput-object v3, v4, v2

    .line 542
    .local v4, "proj":[Ljava/lang/String;
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->getWhereForContactDetails(Ljava/util/List;Ljava/util/EnumSet;)Ljava/lang/String;

    move-result-object v5

    .line 546
    .local v5, "where":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 547
    const/4 v2, 0x0

    .line 641
    :goto_0
    return v2

    .line 549
    :cond_0
    const/16 v38, 0x1

    .line 550
    .local v38, "firstTime":Z
    const/16 v27, 0x0

    .local v27, "colIndexContactID":I
    const/16 v28, 0x0

    .local v28, "colIndexData1":I
    const/16 v29, 0x0

    .local v29, "colIndexData2":I
    const/16 v30, 0x0

    .local v30, "colIndexData3":I
    const/16 v32, 0x0

    .local v32, "colMime":I
    const/16 v31, 0x0

    .local v31, "colIsDefault":I
    const/16 v33, 0x0

    .line 551
    .local v33, "coldataId":I
    const/16 v26, 0x0

    .line 556
    .local v26, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    new-array v6, v2, [Ljava/lang/String;

    .line 558
    .local v6, "rawContactIDsString":[Ljava/lang/String;
    const/16 v39, 0x0

    .line 559
    .local v39, "i":I
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    .local v40, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Ljava/lang/Long;

    .line 560
    .local v41, "l":Ljava/lang/Long;
    invoke-static/range {v41 .. v41}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v39

    .line 561
    add-int/lit8 v39, v39, 0x1

    goto :goto_1

    .line 564
    .end local v41    # "l":Ljava/lang/Long;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v7, "contact_id"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    .line 569
    const-wide/16 v42, -0x1

    .line 571
    .local v42, "lastContactID":J
    if-eqz v26, :cond_5

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 572
    if-eqz v38, :cond_2

    .line 573
    const-string/jumbo v2, "contact_id"

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 574
    const-string/jumbo v2, "data1"

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 575
    const-string/jumbo v2, "data2"

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 576
    const-string/jumbo v2, "data3"

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 577
    const-string/jumbo v2, "mimetype"

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 578
    const-string/jumbo v2, "is_super_primary"

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 579
    const-string/jumbo v2, "data_id"

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 580
    const/16 v38, 0x0

    .line 582
    :cond_2
    const/4 v8, 0x0

    .line 584
    .local v8, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_3
    invoke-interface/range {v26 .. v27}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v34

    .line 585
    .local v34, "contactID":J
    cmp-long v2, v42, v34

    if-nez v2, :cond_4

    if-nez v8, :cond_7

    .line 586
    :cond_4
    move-wide/from16 v42, v34

    .line 587
    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    check-cast v8, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 588
    .restart local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-nez v8, :cond_7

    .line 634
    :goto_2
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_3

    .line 637
    .end local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v34    # "contactID":J
    :cond_5
    if-eqz v26, :cond_6

    .line 638
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    :cond_6
    move/from16 v2, v36

    .line 641
    goto/16 :goto_0

    .line 592
    .restart local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v34    # "contactID":J
    :cond_7
    const/4 v7, 0x0

    .line 593
    .local v7, "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :try_start_1
    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 594
    .local v10, "data1":Ljava/lang/String;
    move-object/from16 v0, v26

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 595
    .local v18, "data2":I
    move-object/from16 v0, v26

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 596
    .local v11, "label":Ljava/lang/String;
    move-object/from16 v0, v26

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v44

    .line 597
    .local v44, "mime":Ljava/lang/String;
    move-object/from16 v0, v26

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 598
    .local v13, "dataId":I
    move-object/from16 v0, v26

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 599
    .local v12, "isDefault":I
    const-string/jumbo v2, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 600
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    if-nez v18, :cond_9

    .line 601
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v9, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-direct/range {v7 .. v13}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;Ljava/lang/String;II)V

    .line 605
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :goto_3
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 633
    :cond_8
    :goto_4
    add-int/lit8 v36, v36, 0x1

    goto :goto_2

    .line 603
    :cond_9
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v16, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v17, v10

    move/from16 v19, v12

    move/from16 v20, v13

    invoke-direct/range {v14 .. v20}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;III)V

    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    goto :goto_3

    .line 606
    :cond_a
    const-string/jumbo v2, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 607
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_c

    if-nez v18, :cond_c

    .line 608
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v9, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-direct/range {v7 .. v12}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;Ljava/lang/String;I)V

    .line 612
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :goto_5
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addEmail(Lcom/vlingo/core/internal/contacts/ContactData;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 637
    .end local v6    # "rawContactIDsString":[Ljava/lang/String;
    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v10    # "data1":Ljava/lang/String;
    .end local v11    # "label":Ljava/lang/String;
    .end local v12    # "isDefault":I
    .end local v13    # "dataId":I
    .end local v18    # "data2":I
    .end local v34    # "contactID":J
    .end local v39    # "i":I
    .end local v40    # "i$":Ljava/util/Iterator;
    .end local v42    # "lastContactID":J
    .end local v44    # "mime":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v26, :cond_b

    .line 638
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v2

    .line 610
    .restart local v6    # "rawContactIDsString":[Ljava/lang/String;
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .restart local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v10    # "data1":Ljava/lang/String;
    .restart local v11    # "label":Ljava/lang/String;
    .restart local v12    # "isDefault":I
    .restart local v13    # "dataId":I
    .restart local v18    # "data2":I
    .restart local v34    # "contactID":J
    .restart local v39    # "i":I
    .restart local v40    # "i$":Ljava/util/Iterator;
    .restart local v42    # "lastContactID":J
    .restart local v44    # "mime":Ljava/lang/String;
    :cond_c
    :try_start_2
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v21, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->convertEmailType(I)I

    move-result v23

    move-object/from16 v19, v7

    move-object/from16 v20, v8

    move-object/from16 v22, v10

    move/from16 v24, v12

    invoke-direct/range {v19 .. v24}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    goto :goto_5

    .line 613
    :cond_d
    const-string/jumbo v2, "vnd.android.cursor.item/postal-address_v2"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 614
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_e

    if-nez v18, :cond_e

    .line 615
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v9, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Address:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-direct/range {v7 .. v12}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;Ljava/lang/String;I)V

    .line 619
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :goto_6
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addAddress(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto :goto_4

    .line 617
    :cond_e
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v16, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Address:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v17, v10

    move/from16 v19, v12

    invoke-direct/range {v14 .. v19}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    goto :goto_6

    .line 620
    :cond_f
    const-string/jumbo v2, "vnd.android.cursor.item/contact_event"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    const-string/jumbo v2, "vnd.pantech.cursor.item/lunar_event"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 621
    :cond_10
    new-instance v45, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd"

    move-object/from16 v0, v45

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 622
    .local v45, "sdfInput":Ljava/text/SimpleDateFormat;
    const/16 v17, 0x0

    .line 624
    .local v17, "formattedBirthday":Ljava/lang/String;
    :try_start_3
    move-object/from16 v0, v45

    invoke-virtual {v0, v10}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v25

    .line 625
    .local v25, "birthday":Ljava/util/Date;
    invoke-static/range {p1 .. p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v17

    .line 630
    .end local v25    # "birthday":Ljava/util/Date;
    :goto_7
    :try_start_4
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v16, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Birthday:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v14, v7

    move-object v15, v8

    move/from16 v19, v12

    invoke-direct/range {v14 .. v19}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 631
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addBirthday(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto/16 :goto_4

    .line 626
    :catch_0
    move-exception v37

    .line 627
    .local v37, "e":Ljava/text/ParseException;
    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "date string did not match \"yyyy-MM-dd\""

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 628
    move-object/from16 v17, v10

    goto :goto_7
.end method

.method private getExactContactMatchByQuery(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nameSelection"    # Ljava/lang/String;

    .prologue
    .line 886
    const/16 v1, 0x8

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "contact_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "raw_contact_id"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "lookup"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "display_name"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "starred"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "data1"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "times_contacted"

    aput-object v2, v3, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "phonetic_name"

    aput-object v2, v3, v1

    .line 896
    .local v3, "projection":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mimetype=\'vnd.android.cursor.item/name\' OR mimetype=\'vnd.android.cursor.item/nickname\' OR mimetype=\'vnd.android.cursor.item/organization\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 898
    .local v4, "selection":Ljava/lang/String;
    const/4 v13, 0x0

    .line 899
    .local v13, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v14, 0x0

    .line 903
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 924
    if-eqz v14, :cond_0

    .line 925
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v5, v13

    .line 929
    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :goto_0
    return-object v13

    .line 906
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_1
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 908
    if-eqz v14, :cond_5

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 909
    const-string/jumbo v1, "contact_id"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 910
    .local v12, "contactId":Ljava/lang/Long;
    const-string/jumbo v1, "lookup"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 911
    .local v10, "lookupKey":Ljava/lang/String;
    const-string/jumbo v1, "display_name"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 912
    .local v6, "displayName":Ljava/lang/String;
    const-string/jumbo v1, "starred"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 913
    .local v17, "starred":I
    const-string/jumbo v1, "times_contacted"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 914
    .local v18, "timesContacted":I
    const-string/jumbo v1, "data1"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 915
    .local v15, "data1":Ljava/lang/String;
    const-string/jumbo v1, "phonetic_name"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 917
    .local v7, "phoneticName":Ljava/lang/String;
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v1, 0x1

    move/from16 v0, v17

    if-ne v0, v1, :cond_3

    const/4 v11, 0x1

    :goto_1
    invoke-direct/range {v5 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 918
    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .local v5, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :try_start_2
    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setTimesContacted(I)V

    .line 919
    invoke-virtual {v5, v15}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 924
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v12    # "contactId":Ljava/lang/Long;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v17    # "starred":I
    .end local v18    # "timesContacted":I
    :goto_2
    if-eqz v14, :cond_2

    .line 925
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_3
    move-object v13, v5

    .line 929
    .end local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto/16 :goto_0

    .line 917
    .restart local v6    # "displayName":Ljava/lang/String;
    .restart local v7    # "phoneticName":Ljava/lang/String;
    .restart local v10    # "lookupKey":Ljava/lang/String;
    .restart local v12    # "contactId":Ljava/lang/Long;
    .restart local v15    # "data1":Ljava/lang/String;
    .restart local v17    # "starred":I
    .restart local v18    # "timesContacted":I
    :cond_3
    const/4 v11, 0x0

    goto :goto_1

    .line 921
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v12    # "contactId":Ljava/lang/Long;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v17    # "starred":I
    .end local v18    # "timesContacted":I
    :catch_0
    move-exception v16

    move-object v5, v13

    .line 922
    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .local v16, "ex":Ljava/lang/Exception;
    :goto_4
    :try_start_3
    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "getExactContactMatch: Caught Exception: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v8, "\n"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v16 .. v16}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 924
    if-eqz v14, :cond_2

    .line 925
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 924
    .end local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v16    # "ex":Ljava/lang/Exception;
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :catchall_0
    move-exception v1

    move-object v5, v13

    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :goto_5
    if-eqz v14, :cond_4

    .line 925
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1

    .line 924
    :catchall_1
    move-exception v1

    goto :goto_5

    .line 921
    .restart local v6    # "displayName":Ljava/lang/String;
    .restart local v7    # "phoneticName":Ljava/lang/String;
    .restart local v10    # "lookupKey":Ljava/lang/String;
    .restart local v12    # "contactId":Ljava/lang/Long;
    .restart local v15    # "data1":Ljava/lang/String;
    .restart local v17    # "starred":I
    .restart local v18    # "timesContacted":I
    :catch_1
    move-exception v16

    goto :goto_4

    .end local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v12    # "contactId":Ljava/lang/Long;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v17    # "starred":I
    .end local v18    # "timesContacted":I
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_5
    move-object v5, v13

    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto :goto_2
.end method

.method private static getFacebookAccountTypeAndName(Landroid/content/Context;)[Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 783
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    .line 784
    .local v5, "mgr":Landroid/accounts/AccountManager;
    invoke-virtual {v5}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 785
    .local v1, "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 788
    .local v0, "a":Landroid/accounts/Account;
    iget-object v6, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "facebook"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 789
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v8, v6, v7

    .line 792
    .end local v0    # "a":Landroid/accounts/Account;
    :goto_1
    return-object v6

    .line 785
    .restart local v0    # "a":Landroid/accounts/Account;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 792
    .end local v0    # "a":Landroid/accounts/Account;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/core/internal/contacts/ContactDBUtil;
    .locals 2

    .prologue
    .line 71
    const-class v1, Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    .line 74
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/ContactDBUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getSocialContactData(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/List;)I
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 645
    .local p2, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p3, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v16, 0x0

    .line 647
    .local v16, "count":I
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getFacebookAccountTypeAndName(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v18

    .line 649
    .local v18, "facebookAccountTypeAndName":[Ljava/lang/String;
    if-eqz v18, :cond_9

    .line 650
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v5, "contact_id"

    aput-object v5, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v5, "sourceid"

    aput-object v5, v3, v1

    .line 655
    .local v3, "proj":[Ljava/lang/String;
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    .line 656
    .local v23, "whereBuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "_id"

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v5, " IN ("

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 657
    const/16 v17, 0x0

    .line 658
    .local v17, "counter":I
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, v20

    if-ge v0, v1, :cond_1

    .line 659
    const-string/jumbo v1, "?"

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 660
    add-int/lit8 v17, v17, 0x1

    .line 661
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, v17

    if-ge v0, v1, :cond_0

    .line 662
    const-string/jumbo v1, ","

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    :cond_0
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    .line 665
    :cond_1
    const-string/jumbo v1, ")"

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 667
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v5, "account_name"

    const/4 v7, 0x1

    aget-object v7, v18, v7

    invoke-virtual {v1, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v5, "account_type"

    const/4 v7, 0x0

    aget-object v7, v18, v7

    invoke-virtual {v1, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 674
    .local v2, "rawContactUri":Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 675
    const/4 v1, 0x0

    .line 708
    .end local v2    # "rawContactUri":Landroid/net/Uri;
    .end local v3    # "proj":[Ljava/lang/String;
    .end local v17    # "counter":I
    .end local v20    # "i":I
    .end local v23    # "whereBuilder":Ljava/lang/StringBuilder;
    :goto_1
    return v1

    .line 677
    .restart local v2    # "rawContactUri":Landroid/net/Uri;
    .restart local v3    # "proj":[Ljava/lang/String;
    .restart local v17    # "counter":I
    .restart local v20    # "i":I
    .restart local v23    # "whereBuilder":Ljava/lang/StringBuilder;
    :cond_2
    const/16 v19, 0x1

    .line 678
    .local v19, "firstTime":Z
    const/4 v12, 0x0

    .local v12, "colIndexContactID":I
    const/4 v13, 0x0

    .line 679
    .local v13, "colIndexSource":I
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Long;

    .line 680
    .local v22, "rawID":Ljava/lang/Long;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "_id="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v22

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 681
    .local v4, "where":Ljava/lang/String;
    const/4 v11, 0x0

    .line 683
    .local v11, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 684
    if-eqz v11, :cond_7

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 685
    if-eqz v19, :cond_4

    .line 686
    const-string/jumbo v1, "contact_id"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 687
    const-string/jumbo v1, "sourceid"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 688
    const/16 v19, 0x0

    .line 690
    :cond_4
    invoke-interface {v11, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 691
    .local v14, "contactID":J
    invoke-interface {v11, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 692
    .local v8, "source":Ljava/lang/String;
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 693
    .local v6, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v8, :cond_5

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_6

    .line 694
    :cond_5
    const-string/jumbo v8, "facebook"

    .line 695
    :cond_6
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v7, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Facebook:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const/16 v9, 0x7d1

    const/4 v10, 0x0

    invoke-direct/range {v5 .. v10}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    invoke-virtual {v6, v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addSocial(Lcom/vlingo/core/internal/contacts/ContactData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 696
    add-int/lit8 v16, v16, 0x1

    .line 702
    .end local v6    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v8    # "source":Ljava/lang/String;
    .end local v14    # "contactID":J
    :cond_7
    if-eqz v11, :cond_3

    .line 703
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 698
    :catch_0
    move-exception v1

    .line 702
    if-eqz v11, :cond_3

    .line 703
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 702
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_8

    .line 703
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v1

    .end local v2    # "rawContactUri":Landroid/net/Uri;
    .end local v3    # "proj":[Ljava/lang/String;
    .end local v4    # "where":Ljava/lang/String;
    .end local v11    # "c":Landroid/database/Cursor;
    .end local v12    # "colIndexContactID":I
    .end local v13    # "colIndexSource":I
    .end local v17    # "counter":I
    .end local v19    # "firstTime":Z
    .end local v20    # "i":I
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v22    # "rawID":Ljava/lang/Long;
    .end local v23    # "whereBuilder":Ljava/lang/StringBuilder;
    :cond_9
    move/from16 v1, v16

    .line 708
    goto/16 :goto_1
.end method


# virtual methods
.method protected additionalPhoneticMatching(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactRule;Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)V
    .locals 21
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "rule"    # Lcom/vlingo/core/internal/contacts/ContactRule;
    .param p4, "ruleSet"    # Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            "Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 446
    .local p5, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p6, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p7, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 448
    .local v6, "phoneticLocalMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    move-object/from16 v0, p4

    instance-of v1, v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    if-nez v1, :cond_0

    move-object/from16 v0, p4

    instance-of v1, v0, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;

    if-nez v1, :cond_0

    .line 449
    move-object/from16 v0, p6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 500
    :goto_0
    return-void

    .line 453
    :cond_0
    invoke-virtual/range {p6 .. p6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 454
    .local v16, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    move-object/from16 v0, p4

    instance-of v1, v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    if-eqz v1, :cond_2

    .line 455
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v1, p4

    .line 456
    check-cast v1, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 457
    .local v4, "newQueryWithPhoneticName":Ljava/lang/String;
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v5

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v7, p7

    invoke-virtual/range {v1 .. v7}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    goto :goto_1

    .end local v4    # "newQueryWithPhoneticName":Ljava/lang/String;
    :cond_2
    move-object/from16 v15, p4

    .line 460
    check-cast v15, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;

    .line 461
    .local v15, "japanRuleSet":Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/core/internal/contacts/ContactRule;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Partial Match"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 462
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticFirstName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v19, 0x1

    .line 463
    .local v19, "queryHasFirstName":Z
    :goto_2
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticLastName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v20, 0x1

    .line 464
    .local v20, "queryHasLastName":Z
    :goto_3
    if-eqz v19, :cond_9

    if-eqz v20, :cond_9

    .line 475
    .end local v19    # "queryHasFirstName":Z
    .end local v20    # "queryHasLastName":Z
    :cond_3
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticLastName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticFirstName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 476
    .local v18, "phoneticName":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 477
    .local v17, "nameNormalized":Ljava/lang/String;
    const/4 v10, 0x0

    .line 478
    .local v10, "query":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 479
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 481
    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 482
    :cond_5
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticFirstName()Ljava/lang/String;

    move-result-object v18

    .line 483
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 484
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticFirstName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 493
    :cond_6
    :goto_5
    if-eqz v10, :cond_1

    .line 494
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v11

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object v12, v6

    move-object/from16 v13, p7

    invoke-virtual/range {v7 .. v13}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    goto/16 :goto_1

    .line 462
    .end local v10    # "query":Ljava/lang/String;
    .end local v17    # "nameNormalized":Ljava/lang/String;
    .end local v18    # "phoneticName":Ljava/lang/String;
    :cond_7
    const/16 v19, 0x0

    goto/16 :goto_2

    .line 463
    .restart local v19    # "queryHasFirstName":Z
    :cond_8
    const/16 v20, 0x0

    goto :goto_3

    .line 466
    .restart local v20    # "queryHasLastName":Z
    :cond_9
    if-eqz v19, :cond_a

    .line 467
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticFirstName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v11

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object v12, v6

    move-object/from16 v13, p7

    invoke-virtual/range {v7 .. v13}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    goto/16 :goto_4

    .line 469
    :cond_a
    if-eqz v20, :cond_3

    .line 470
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticLastName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticLastName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v11

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object v12, v6

    move-object/from16 v13, p7

    invoke-virtual/range {v7 .. v13}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    goto/16 :goto_4

    .line 486
    .end local v19    # "queryHasFirstName":Z
    .end local v20    # "queryHasLastName":Z
    .restart local v10    # "query":Ljava/lang/String;
    .restart local v17    # "nameNormalized":Ljava/lang/String;
    .restart local v18    # "phoneticName":Ljava/lang/String;
    :cond_b
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 487
    :cond_c
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticLastName()Ljava/lang/String;

    move-result-object v18

    .line 488
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 489
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticLastName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    goto :goto_5

    .line 499
    .end local v10    # "query":Ljava/lang/String;
    .end local v15    # "japanRuleSet":Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;
    .end local v16    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v17    # "nameNormalized":Ljava/lang/String;
    .end local v18    # "phoneticName":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    goto/16 :goto_0
.end method

.method public convertEmailType(I)I
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/4 v0, 0x3

    .line 525
    if-ne p1, v0, :cond_0

    const/4 p1, 0x7

    .line 526
    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    move p1, v0

    .line 527
    :cond_1
    return p1
.end method

.method public findMatchesByName(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/List;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p3, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p4, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 195
    :cond_0
    return-void

    .line 187
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->mSkipNormalCheck:Z

    .line 188
    invoke-static {p2}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseForMatchesByName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 189
    .local v10, "where":[Ljava/lang/String;
    move-object v7, v10

    .local v7, "arr$":[Ljava/lang/String;
    array-length v9, v7

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v3, v7, v8

    .line 191
    .local v3, "sWhere":Ljava/lang/String;
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->nameIsKorean(Ljava/lang/String;)Z

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public findMatchesByName2(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;Z)V
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p6, "keepUnrelatedContacts"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p3, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p4, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p5, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    const/16 v24, 0x0

    .line 214
    .local v24, "isQueryHasLatinSymbols":Z
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;

    .line 215
    .local v14, "ruleSet":Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
    if-eqz v24, :cond_3

    instance-of v4, v14, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;

    if-eqz v4, :cond_2

    .line 218
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->canProcess(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 219
    invoke-virtual {v14}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;

    move-result-object v20

    .line 220
    .local v20, "exitCriteria":Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->generateRules(Ljava/lang/String;)Ljava/util/List;

    move-result-object v28

    .line 221
    .local v28, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    const/16 v27, 0x0

    .line 222
    .local v27, "nRuleCnt":I
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vlingo/core/internal/contacts/ContactRule;

    .line 223
    .local v13, "rule":Lcom/vlingo/core/internal/contacts/ContactRule;
    invoke-virtual {v13}, Lcom/vlingo/core/internal/contacts/ContactRule;->getName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "CallLog"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 226
    :cond_5
    invoke-virtual {v13}, Lcom/vlingo/core/internal/contacts/ContactRule;->getQuery()Ljava/lang/String;

    move-result-object v7

    .line 229
    .local v7, "query":Ljava/lang/String;
    add-int/lit8 v27, v27, 0x1

    .line 230
    invoke-virtual {v13}, Lcom/vlingo/core/internal/contacts/ContactRule;->getScore()Lcom/vlingo/core/internal/contacts/scoring/ContactScore;

    move-result-object v29

    .line 232
    .local v29, "scorer":Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 233
    .local v9, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->mSkipNormalCheck:Z

    .line 234
    invoke-virtual {v14}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v8

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v10, p5

    invoke-virtual/range {v4 .. v10}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    .line 237
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->mSkipNormalCheck:Z

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v15, p3

    move-object/from16 v16, v9

    move-object/from16 v17, p5

    .line 238
    invoke-virtual/range {v10 .. v17}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->additionalPhoneticMatching(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactRule;Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)V

    .line 241
    invoke-virtual {v9}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 243
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-virtual {v0, v1, v2, v9, v3}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getAllContactData(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v18

    .line 248
    .local v18, "count":I
    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .line 249
    .local v25, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;>;"
    :cond_6
    :goto_1
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 250
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Map$Entry;

    .line 251
    .local v19, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 252
    .local v26, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-nez p6, :cond_7

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/contacts/ContactMatch;->containsData()Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_7
    move-object/from16 v0, v26

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasLookupData(Ljava/util/EnumSet;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 255
    :cond_8
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 263
    .end local v19    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v26    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_9
    move-object/from16 v0, p4

    move-object/from16 v1, v29

    move-object/from16 v2, v20

    move-object/from16 v3, p2

    invoke-static {v0, v9, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->applyScores(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;Lcom/vlingo/core/internal/contacts/ContactExitCriteria;Ljava/lang/String;)V

    .line 265
    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->returnMatches()Z

    move-result v21

    .line 268
    .local v21, "exitCriteriaFromLoop":Z
    instance-of v4, v14, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;

    if-eqz v4, :cond_a

    const-string/jumbo v4, "\\s+"

    const-string/jumbo v5, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_a

    .line 269
    const/16 v21, 0x0

    .line 273
    :cond_a
    if-eqz v21, :cond_4

    invoke-virtual {v9}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    goto/16 :goto_0

    .line 279
    .end local v7    # "query":Ljava/lang/String;
    .end local v9    # "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v13    # "rule":Lcom/vlingo/core/internal/contacts/ContactRule;
    .end local v18    # "count":I
    .end local v21    # "exitCriteriaFromLoop":Z
    .end local v25    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;>;"
    .end local v29    # "scorer":Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    :cond_b
    instance-of v4, v14, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;

    if-nez v4, :cond_0

    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 282
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/StringUtils;->stringHasLatinSymbols(Ljava/lang/String;)Z

    move-result v24

    .line 283
    if-nez v24, :cond_2

    goto/16 :goto_0
.end method

.method public findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z
    .locals 36
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "skipExtraData"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 305
    .local p5, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p6, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v2, 0xc

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v11, "contact_id"

    aput-object v11, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v11, "raw_contact_id"

    aput-object v11, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v11, "lookup"

    aput-object v11, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v11, "display_name"

    aput-object v11, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v11, "starred"

    aput-object v11, v4, v2

    const/4 v2, 0x5

    const-string/jumbo v11, "data1"

    aput-object v11, v4, v2

    const/4 v2, 0x6

    const-string/jumbo v11, "data2"

    aput-object v11, v4, v2

    const/4 v2, 0x7

    const-string/jumbo v11, "data3"

    aput-object v11, v4, v2

    const/16 v2, 0x8

    const-string/jumbo v11, "data7"

    aput-object v11, v4, v2

    const/16 v2, 0x9

    const-string/jumbo v11, "data9"

    aput-object v11, v4, v2

    const/16 v2, 0xa

    const-string/jumbo v11, "times_contacted"

    aput-object v11, v4, v2

    const/16 v2, 0xb

    const-string/jumbo v11, "phonetic_name"

    aput-object v11, v4, v2

    .line 320
    .local v4, "proj":[Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    .line 322
    .local v3, "lookupUri":Landroid/net/Uri;
    const/16 v33, 0x0

    .line 325
    .local v33, "retVal":Z
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->mSkipNormalCheck:Z

    if-nez v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 326
    const/4 v2, 0x0

    .line 402
    :goto_0
    return v2

    .line 330
    :cond_0
    const/4 v12, 0x0

    .line 331
    .local v12, "c":Landroid/database/Cursor;
    const/16 v32, 0x0

    .line 334
    .local v32, "recordsFound":I
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v5, p3

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 335
    if-eqz v12, :cond_2

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 336
    const-string/jumbo v2, "raw_contact_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 337
    .local v22, "colIndexRawContactID":I
    const-string/jumbo v2, "contact_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 338
    .local v13, "colIndexContactID":I
    const-string/jumbo v2, "lookup"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 339
    .local v19, "colIndexLookupKey":I
    const-string/jumbo v2, "display_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 340
    .local v20, "colIndexName":I
    const-string/jumbo v2, "starred"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 341
    .local v23, "colIndexStarred":I
    const-string/jumbo v2, "data1"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 342
    .local v14, "colIndexData1":I
    const-string/jumbo v2, "data2"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 343
    .local v15, "colIndexData2":I
    const-string/jumbo v2, "data3"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 344
    .local v16, "colIndexData3":I
    const-string/jumbo v2, "data7"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 345
    .local v17, "colIndexData7":I
    const-string/jumbo v2, "data9"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 346
    .local v18, "colIndexData9":I
    const-string/jumbo v2, "times_contacted"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 347
    .local v24, "colIndexTimesContacted":I
    const-string/jumbo v2, "phonetic_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 350
    .local v21, "colIndexPhoneticName":I
    :cond_1
    invoke-interface {v12, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 351
    .local v8, "contactID":J
    move/from16 v0, v20

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 352
    .local v6, "name":Ljava/lang/String;
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 355
    .local v5, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz p4, :cond_4

    move-object/from16 v0, p2

    invoke-static {v0, v6}, Lcom/vlingo/core/internal/util/StringUtils;->matchesKoreanNamePattern(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 389
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 397
    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v6    # "name":Ljava/lang/String;
    .end local v8    # "contactID":J
    .end local v13    # "colIndexContactID":I
    .end local v14    # "colIndexData1":I
    .end local v15    # "colIndexData2":I
    .end local v16    # "colIndexData3":I
    .end local v17    # "colIndexData7":I
    .end local v18    # "colIndexData9":I
    .end local v19    # "colIndexLookupKey":I
    .end local v20    # "colIndexName":I
    .end local v21    # "colIndexPhoneticName":I
    .end local v22    # "colIndexRawContactID":I
    .end local v23    # "colIndexStarred":I
    .end local v24    # "colIndexTimesContacted":I
    :cond_2
    if-eqz v12, :cond_3

    .line 398
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_2
    move/from16 v2, v33

    .line 402
    goto/16 :goto_0

    .line 359
    .restart local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v8    # "contactID":J
    .restart local v13    # "colIndexContactID":I
    .restart local v14    # "colIndexData1":I
    .restart local v15    # "colIndexData2":I
    .restart local v16    # "colIndexData3":I
    .restart local v17    # "colIndexData7":I
    .restart local v18    # "colIndexData9":I
    .restart local v19    # "colIndexLookupKey":I
    .restart local v20    # "colIndexName":I
    .restart local v21    # "colIndexPhoneticName":I
    .restart local v22    # "colIndexRawContactID":I
    .restart local v23    # "colIndexStarred":I
    .restart local v24    # "colIndexTimesContacted":I
    :cond_4
    :try_start_1
    invoke-interface {v12, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 361
    .local v25, "data1":Ljava/lang/String;
    if-eqz v5, :cond_6

    .line 362
    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V

    .line 381
    :goto_3
    move/from16 v0, v22

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 382
    .local v30, "rawContactID":J
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 383
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    move-wide/from16 v0, v30

    invoke-virtual {v5, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setRawContactID(J)V

    .line 385
    add-int/lit8 v32, v32, 0x1

    .line 388
    :cond_5
    const/16 v33, 0x1

    goto :goto_1

    .line 365
    .end local v30    # "rawContactID":J
    :cond_6
    invoke-interface {v12, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 366
    .local v26, "firstName":Ljava/lang/String;
    move/from16 v0, v16

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 367
    .local v27, "lastName":Ljava/lang/String;
    move/from16 v0, v17

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 368
    .local v28, "phoneticFirstName":Ljava/lang/String;
    move/from16 v0, v18

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 369
    .local v29, "phoneticLastName":Ljava/lang/String;
    move/from16 v0, v24

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v35

    .line 370
    .local v35, "timesContacted":I
    move/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 372
    .local v7, "phoneticName":Ljava/lang/String;
    move/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 373
    .local v10, "lookupKey":Ljava/lang/String;
    move/from16 v0, v23

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v34

    .line 374
    .local v34, "starred":I
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v2, 0x1

    move/from16 v0, v34

    if-ne v0, v2, :cond_7

    const/4 v11, 0x1

    :goto_4
    invoke-direct/range {v5 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 375
    .restart local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V

    .line 377
    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v5, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addNameFields(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v5, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhoneticFields(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    move/from16 v0, v35

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setTimesContacted(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 391
    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v8    # "contactID":J
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v13    # "colIndexContactID":I
    .end local v14    # "colIndexData1":I
    .end local v15    # "colIndexData2":I
    .end local v16    # "colIndexData3":I
    .end local v17    # "colIndexData7":I
    .end local v18    # "colIndexData9":I
    .end local v19    # "colIndexLookupKey":I
    .end local v20    # "colIndexName":I
    .end local v21    # "colIndexPhoneticName":I
    .end local v22    # "colIndexRawContactID":I
    .end local v23    # "colIndexStarred":I
    .end local v24    # "colIndexTimesContacted":I
    .end local v25    # "data1":Ljava/lang/String;
    .end local v26    # "firstName":Ljava/lang/String;
    .end local v27    # "lastName":Ljava/lang/String;
    .end local v28    # "phoneticFirstName":Ljava/lang/String;
    .end local v29    # "phoneticLastName":Ljava/lang/String;
    .end local v34    # "starred":I
    .end local v35    # "timesContacted":I
    :catch_0
    move-exception v2

    .line 397
    if-eqz v12, :cond_3

    .line 398
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 374
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v7    # "phoneticName":Ljava/lang/String;
    .restart local v8    # "contactID":J
    .restart local v10    # "lookupKey":Ljava/lang/String;
    .restart local v13    # "colIndexContactID":I
    .restart local v14    # "colIndexData1":I
    .restart local v15    # "colIndexData2":I
    .restart local v16    # "colIndexData3":I
    .restart local v17    # "colIndexData7":I
    .restart local v18    # "colIndexData9":I
    .restart local v19    # "colIndexLookupKey":I
    .restart local v20    # "colIndexName":I
    .restart local v21    # "colIndexPhoneticName":I
    .restart local v22    # "colIndexRawContactID":I
    .restart local v23    # "colIndexStarred":I
    .restart local v24    # "colIndexTimesContacted":I
    .restart local v25    # "data1":Ljava/lang/String;
    .restart local v26    # "firstName":Ljava/lang/String;
    .restart local v27    # "lastName":Ljava/lang/String;
    .restart local v28    # "phoneticFirstName":Ljava/lang/String;
    .restart local v29    # "phoneticLastName":Ljava/lang/String;
    .restart local v34    # "starred":I
    .restart local v35    # "timesContacted":I
    :cond_7
    const/4 v11, 0x0

    goto :goto_4

    .line 397
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v8    # "contactID":J
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v13    # "colIndexContactID":I
    .end local v14    # "colIndexData1":I
    .end local v15    # "colIndexData2":I
    .end local v16    # "colIndexData3":I
    .end local v17    # "colIndexData7":I
    .end local v18    # "colIndexData9":I
    .end local v19    # "colIndexLookupKey":I
    .end local v20    # "colIndexName":I
    .end local v21    # "colIndexPhoneticName":I
    .end local v22    # "colIndexRawContactID":I
    .end local v23    # "colIndexStarred":I
    .end local v24    # "colIndexTimesContacted":I
    .end local v25    # "data1":Ljava/lang/String;
    .end local v26    # "firstName":Ljava/lang/String;
    .end local v27    # "lastName":Ljava/lang/String;
    .end local v28    # "phoneticFirstName":Ljava/lang/String;
    .end local v29    # "phoneticLastName":Ljava/lang/String;
    .end local v34    # "starred":I
    .end local v35    # "timesContacted":I
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_8

    .line 398
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v2
.end method

.method public getAllContactData(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 417
    .local p2, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p3, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p4, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 418
    .local v0, "count":I
    invoke-virtual {p3}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_1

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->PHONE_NUMBER:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->EMAIL_ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 426
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getContactDetails(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v0

    .line 433
    :cond_1
    const/4 v0, 0x0

    .line 434
    invoke-virtual {p3}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_3

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->SOCIAL_NETWORK:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 435
    :cond_2
    invoke-direct {p0, p1, p3, p4}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getSocialContactData(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v0

    .line 438
    :cond_3
    return v0
.end method

.method public getContactDataByEmail(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "email"    # Ljava/lang/String;

    .prologue
    .line 721
    const/16 v18, 0x0

    .line 722
    .local v18, "c":Landroid/database/Cursor;
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    .line 725
    .local v3, "lookupUri":Landroid/net/Uri;
    const/16 v2, 0xa

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v12, "contact_id"

    aput-object v12, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v12, "raw_contact_id"

    aput-object v12, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v12, "lookup"

    aput-object v12, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v12, "display_name"

    aput-object v12, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v12, "starred"

    aput-object v12, v4, v2

    const/4 v2, 0x5

    const-string/jumbo v12, "data1"

    aput-object v12, v4, v2

    const/4 v2, 0x6

    const-string/jumbo v12, "data2"

    aput-object v12, v4, v2

    const/4 v2, 0x7

    const-string/jumbo v12, "is_super_primary"

    aput-object v12, v4, v2

    const/16 v2, 0x8

    const-string/jumbo v12, "times_contacted"

    aput-object v12, v4, v2

    const/16 v2, 0x9

    const-string/jumbo v12, "phonetic_name"

    aput-object v12, v4, v2

    .line 739
    .local v4, "proj":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 740
    const/4 v12, 0x0

    .line 779
    :cond_0
    :goto_0
    return-object v12

    .line 742
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "data1 like \'"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v12, "\'"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 747
    .local v5, "where":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 748
    if-eqz v18, :cond_3

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->isLast()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 749
    const-string/jumbo v2, "contact_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 750
    .local v19, "colIndexContactID":I
    const-string/jumbo v2, "lookup"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 751
    .local v23, "colIndexLookupKey":I
    const-string/jumbo v2, "display_name"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 752
    .local v24, "colIndexName":I
    const-string/jumbo v2, "starred"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 753
    .local v26, "colIndexStarred":I
    const-string/jumbo v2, "data1"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 754
    .local v21, "colIndexEmail":I
    const-string/jumbo v2, "data2"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 755
    .local v20, "colIndexData2":I
    const-string/jumbo v2, "is_super_primary"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 756
    .local v22, "colIndexIsDefault":I
    const-string/jumbo v2, "times_contacted"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 757
    .local v27, "colIndexTimesContacted":I
    const-string/jumbo v2, "phonetic_name"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 759
    .local v25, "colIndexPhoneticName":I
    invoke-interface/range {v18 .. v19}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 760
    .local v9, "contactID":J
    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 761
    .local v11, "lookupKey":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 762
    .local v7, "name":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    .line 763
    .local v28, "starred":I
    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 764
    .local v8, "phoneticName":Ljava/lang/String;
    new-instance v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    const/4 v2, 0x1

    move/from16 v0, v28

    if-ne v0, v2, :cond_2

    const/4 v12, 0x1

    :goto_1
    invoke-direct/range {v6 .. v12}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 765
    .local v6, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 766
    .local v15, "data1":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 767
    .local v16, "data2":I
    move-object/from16 v0, v18

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v29

    .line 768
    .local v29, "timesContacted":I
    invoke-virtual {v6, v15}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V

    .line 769
    move/from16 v0, v29

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setTimesContacted(I)V

    .line 770
    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 772
    .local v17, "isDefault":I
    new-instance v12, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v14, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v13, v6

    invoke-direct/range {v12 .. v17}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 775
    if-eqz v18, :cond_0

    .line 776
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 764
    .end local v6    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v16    # "data2":I
    .end local v17    # "isDefault":I
    .end local v29    # "timesContacted":I
    :cond_2
    const/4 v12, 0x0

    goto :goto_1

    .line 775
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "phoneticName":Ljava/lang/String;
    .end local v9    # "contactID":J
    .end local v11    # "lookupKey":Ljava/lang/String;
    .end local v19    # "colIndexContactID":I
    .end local v20    # "colIndexData2":I
    .end local v21    # "colIndexEmail":I
    .end local v22    # "colIndexIsDefault":I
    .end local v23    # "colIndexLookupKey":I
    .end local v24    # "colIndexName":I
    .end local v25    # "colIndexPhoneticName":I
    .end local v26    # "colIndexStarred":I
    .end local v27    # "colIndexTimesContacted":I
    .end local v28    # "starred":I
    :cond_3
    if-eqz v18, :cond_4

    .line 776
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 779
    :cond_4
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 775
    :catchall_0
    move-exception v2

    if-eqz v18, :cond_5

    .line 776
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2
.end method

.method public getContactMatchByPhoneNumber(Ljava/lang/String;Landroid/content/Context;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 18
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 986
    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v6, "lookup"

    aput-object v6, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v6, "display_name"

    aput-object v6, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v6, "starred"

    aput-object v6, v3, v1

    .line 987
    .local v3, "projection":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 988
    .local v16, "cursor":Landroid/database/Cursor;
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 989
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 990
    const/4 v4, 0x0

    .line 1016
    :goto_0
    return-object v4

    .line 992
    :cond_0
    sget-object v1, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 993
    .local v2, "contactUri":Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 997
    const/4 v4, 0x0

    .line 999
    .local v4, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static/range {v16 .. v16}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v6, 0x1

    if-lt v1, v6, :cond_5

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1001
    :try_start_0
    const-string/jumbo v1, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 1002
    .local v15, "contactId":Ljava/lang/Long;
    const-string/jumbo v1, "lookup"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1003
    .local v8, "lookupKey":Ljava/lang/String;
    const-string/jumbo v1, "display_name"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1004
    .local v5, "displayName":Ljava/lang/String;
    const-string/jumbo v1, "starred"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 1006
    .local v17, "starred":I
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v1, 0x1

    move/from16 v0, v17

    if-ne v0, v1, :cond_3

    const/4 v9, 0x1

    :goto_1
    invoke-direct/range {v4 .. v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1008
    .restart local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v16, :cond_1

    .line 1009
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 1015
    .end local v5    # "displayName":Ljava/lang/String;
    .end local v8    # "lookupKey":Ljava/lang/String;
    .end local v15    # "contactId":Ljava/lang/Long;
    .end local v17    # "starred":I
    :cond_1
    :goto_2
    new-instance v9, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v11, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v10, v4

    move-object/from16 v12, p1

    invoke-direct/range {v9 .. v14}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    invoke-virtual {v4, v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto/16 :goto_0

    .line 995
    .end local v2    # "contactUri":Landroid/net/Uri;
    .end local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1006
    .restart local v2    # "contactUri":Landroid/net/Uri;
    .restart local v5    # "displayName":Ljava/lang/String;
    .restart local v8    # "lookupKey":Ljava/lang/String;
    .restart local v15    # "contactId":Ljava/lang/Long;
    .restart local v17    # "starred":I
    :cond_3
    const/4 v9, 0x0

    goto :goto_1

    .line 1008
    .end local v5    # "displayName":Ljava/lang/String;
    .end local v8    # "lookupKey":Ljava/lang/String;
    .end local v15    # "contactId":Ljava/lang/Long;
    .end local v17    # "starred":I
    :catchall_0
    move-exception v1

    if-eqz v16, :cond_4

    .line 1009
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1

    .line 1013
    .restart local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_5
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const-wide/16 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v9, v4

    move-object/from16 v10, p1

    invoke-direct/range {v9 .. v14}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    .restart local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto :goto_2
.end method

.method public getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p4, "keepUnrelatedContacts"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    .local p3, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    const-string/jumbo v0, "new_contact_match_algo"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;ZZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;ZZ)Ljava/util/List;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p4, "keepUnrelatedContacts"    # Z
    .param p5, "newAlgo"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;ZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    .local p3, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "getContactMatches()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 137
    .local v4, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 142
    .local v5, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz p5, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, p4

    .line 143
    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByName2(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;Z)V

    .line 144
    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 163
    .local v10, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "**found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " contact matches"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    return-object v10

    .line 146
    .end local v10    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_1
    invoke-virtual {p0, p1, p2, v4, v5}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByName(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/List;)V

    .line 148
    invoke-virtual {p0, p1, p3, v4, v5}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getAllContactData(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v7

    .line 153
    .local v7, "count":I
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 154
    .restart local v10    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 155
    .local v9, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-nez p4, :cond_3

    invoke-virtual {v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;->containsData()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_3
    invoke-virtual {v9, p3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasLookupData(Ljava/util/EnumSet;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156
    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getExactContactMatch(Landroid/content/Context;J)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J

    .prologue
    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "contact_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "idSelection":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getExactContactMatchByQuery(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    return-object v1
.end method

.method public getExactContactMatch(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 85
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    :cond_0
    const/4 v1, 0x0

    .line 91
    :goto_0
    return-object v1

    .line 90
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "data1 LIKE \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "nameSelection":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getExactContactMatchByQuery(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    goto :goto_0
.end method

.method public isAddressBookEmpty(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 802
    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v0, "contact_id"

    aput-object v0, v2, v8

    .line 803
    .local v2, "proj":[Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 804
    .local v1, "lookupUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 807
    .local v6, "c":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 818
    :goto_0
    return v8

    .line 812
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 813
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 814
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    move v0, v7

    .line 817
    :goto_1
    if-eqz v6, :cond_1

    .line 818
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v8, v0

    goto :goto_0

    :cond_2
    move v0, v8

    .line 814
    goto :goto_1

    .line 817
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 818
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public isProviderStatusNormal(Landroid/content/ContentResolver;)Z
    .locals 9
    .param p1, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 956
    const-string/jumbo v0, "content://com.android.contacts/provider_status"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "status"

    aput-object v0, v2, v4

    const/4 v0, 0x1

    const-string/jumbo v4, "data1"

    aput-object v4, v2, v0

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 957
    .local v6, "c":Landroid/database/Cursor;
    const/4 v8, -0x1

    .line 958
    .local v8, "status":I
    const/4 v7, 0x0

    .line 960
    .local v7, "retVal":Z
    if-eqz v6, :cond_1

    .line 962
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 963
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 965
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 972
    :cond_1
    if-nez v8, :cond_2

    .line 973
    const/4 v7, 0x1

    .line 975
    :cond_2
    return v7

    .line 965
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public populateContactMapping(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 830
    const/4 v0, 0x0

    .line 833
    .local v0, "cur":Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getContactCursor(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    .line 834
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 835
    .local v7, "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_4

    .line 837
    const-string/jumbo v8, "data2"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 838
    .local v6, "givenNameColumn":I
    const-string/jumbo v8, "data3"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 839
    .local v4, "familyNameColumn":I
    const-string/jumbo v8, "data1"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 840
    .local v2, "displayNameColumn":I
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 843
    :cond_0
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 844
    .local v5, "givenName":Ljava/lang/String;
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 845
    .local v3, "familyName":Ljava/lang/String;
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 848
    .local v1, "displayName":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 852
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 855
    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 859
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 862
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 866
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 868
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 871
    .end local v1    # "displayName":Ljava/lang/String;
    .end local v2    # "displayNameColumn":I
    .end local v3    # "familyName":Ljava/lang/String;
    .end local v4    # "familyNameColumn":I
    .end local v5    # "givenName":Ljava/lang/String;
    .end local v6    # "givenNameColumn":I
    :cond_4
    invoke-static {v7}, Lcom/vlingo/core/internal/settings/Settings;->setContactNameList(Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 874
    if-eqz v0, :cond_5

    .line 878
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 883
    :cond_5
    :goto_0
    return-void

    .line 874
    .end local v7    # "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v8

    if-eqz v0, :cond_6

    .line 878
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 880
    :cond_6
    :goto_1
    throw v8

    .line 879
    .restart local v7    # "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v8

    goto :goto_0

    .end local v7    # "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_1
    move-exception v9

    goto :goto_1
.end method
