.class public Lcom/vlingo/core/internal/weather/WeatherAttributeListener;
.super Ljava/lang/Object;
.source "WeatherAttributeListener.java"

# interfaces
.implements Landroid/sax/StartElementListener;


# instance fields
.field private element:Lcom/vlingo/core/internal/weather/WeatherElement;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/weather/WeatherElement;)V
    .locals 2
    .param p1, "element"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    if-nez p1, :cond_0

    .line 27
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "element cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherAttributeListener;->element:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 30
    return-void
.end method


# virtual methods
.method public start(Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 35
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    invoke-interface {p1}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 36
    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getLocalName(I)Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, "key":Ljava/lang/String;
    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/core/internal/weather/WeatherAttributeListener;->element:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v3, v1, v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->addAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    return-void
.end method
