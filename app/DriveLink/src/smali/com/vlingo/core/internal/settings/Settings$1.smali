.class final Lcom/vlingo/core/internal/settings/Settings$1;
.super Ljava/lang/Object;
.source "Settings.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/settings/Settings;->commitHashEditSetting()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 1586
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1587
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v4, 0x0

    .line 1588
    .local v4, "intItem":Ljava/lang/Integer;
    const/4 v7, 0x0

    .line 1589
    .local v7, "stringItem":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1590
    .local v2, "floatItem":Ljava/lang/Float;
    const/4 v6, 0x0

    .line 1591
    .local v6, "longItem":Ljava/lang/Long;
    const/4 v0, 0x0

    .line 1592
    .local v0, "booleanItem":Ljava/lang/Boolean;
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$200()Z

    move-result v8

    if-nez v8, :cond_0

    .line 1623
    :goto_0
    return-void

    .line 1595
    :cond_0
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsInt:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$300()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1596
    .local v5, "key":Ljava/lang/String;
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsInt:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$300()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "intItem":Ljava/lang/Integer;
    check-cast v4, Ljava/lang/Integer;

    .line 1597
    .restart local v4    # "intItem":Ljava/lang/Integer;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-interface {v1, v5, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 1599
    .end local v5    # "key":Ljava/lang/String;
    :cond_1
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsString:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$400()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1600
    .restart local v5    # "key":Ljava/lang/String;
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsString:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$400()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "stringItem":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 1601
    .restart local v7    # "stringItem":Ljava/lang/String;
    invoke-interface {v1, v5, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    .line 1603
    .end local v5    # "key":Ljava/lang/String;
    :cond_2
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsLong:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$500()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1604
    .restart local v5    # "key":Ljava/lang/String;
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsLong:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$500()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "longItem":Ljava/lang/Long;
    check-cast v6, Ljava/lang/Long;

    .line 1605
    .restart local v6    # "longItem":Ljava/lang/Long;
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-interface {v1, v5, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_3

    .line 1607
    .end local v5    # "key":Ljava/lang/String;
    :cond_3
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsFloat:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$600()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1608
    .restart local v5    # "key":Ljava/lang/String;
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsFloat:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$600()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "floatItem":Ljava/lang/Float;
    check-cast v2, Ljava/lang/Float;

    .line 1609
    .restart local v2    # "floatItem":Ljava/lang/Float;
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-interface {v1, v5, v8}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_4

    .line 1611
    .end local v5    # "key":Ljava/lang/String;
    :cond_4
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsBoolean:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$700()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1612
    .restart local v5    # "key":Ljava/lang/String;
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsBoolean:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$700()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "booleanItem":Ljava/lang/Boolean;
    check-cast v0, Ljava/lang/Boolean;

    .line 1613
    .restart local v0    # "booleanItem":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-interface {v1, v5, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_5

    .line 1615
    .end local v5    # "key":Ljava/lang/String;
    :cond_5
    invoke-static {v1}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 1616
    const/4 v8, 0x0

    # setter for: Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z
    invoke-static {v8}, Lcom/vlingo/core/internal/settings/Settings;->access$202(Z)Z

    .line 1618
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsInt:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$300()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    .line 1619
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsString:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$400()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    .line 1620
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsLong:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$500()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    .line 1621
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsFloat:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$600()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    .line 1622
    # getter for: Lcom/vlingo/core/internal/settings/Settings;->hsBoolean:Ljava/util/HashMap;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->access$700()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    goto/16 :goto_0
.end method
