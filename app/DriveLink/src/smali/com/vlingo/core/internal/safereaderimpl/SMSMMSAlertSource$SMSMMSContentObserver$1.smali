.class Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver$1;
.super Ljava/lang/Object;
.source "SMSMMSAlertSource.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->onChange(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 238
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    # getter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->priority:I
    invoke-static {v1}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$400(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)I

    move-result v1

    if-eq v1, v4, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;

    # invokes: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->isCCForeground()Z
    invoke-static {v1}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->access$500(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 239
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getNewSMSSafereaderAlerts(I)Ljava/util/LinkedList;

    move-result-object v0

    .line 241
    .local v0, "smsAlerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 242
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 245
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 246
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 247
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver$1;->this$1:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    iget-object v1, v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 251
    .end local v0    # "smsAlerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_1
    return-void
.end method
