.class Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;
.super Ljava/lang/Thread;
.source "CoreTester.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/CoreTester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ReplayThread"
.end annotation


# static fields
.field private static final DEFAULT_DELAY_TIME:I = 0x2710


# instance fields
.field private final log:Lcom/vlingo/core/internal/logging/Logger;

.field private mDelayTime:I

.field private volatile mDialogFlowRunning:Z

.field private volatile mDoingReplay:Z

.field private mFis:Ljava/io/FileInputStream;

.field private mHandler:Landroid/os/Handler;

.field final mOnReplayFinishedCallback:Lcom/vlingo/core/internal/dialogmanager/CoreTester$OnFinishedCallback;

.field private mReader:Ljava/io/BufferedReader;

.field private mWaitingForWidget:Z

.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/CoreTester;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/CoreTester;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/CoreTester$OnFinishedCallback;)V
    .locals 3
    .param p2, "inFilePath"    # Ljava/lang/String;
    .param p3, "onReplayFinishedCallback"    # Lcom/vlingo/core/internal/dialogmanager/CoreTester$OnFinishedCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 369
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;->this$0:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    .line 370
    const-string/jumbo v0, "CoreTester.ReplayThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 359
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 360
    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;->mWaitingForWidget:Z

    .line 361
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;->mFis:Ljava/io/FileInputStream;

    .line 362
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;->mReader:Ljava/io/BufferedReader;

    .line 363
    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;->mDialogFlowRunning:Z

    .line 364
    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;->mDoingReplay:Z

    .line 366
    const/16 v0, 0x2710

    iput v0, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;->mDelayTime:I

    .line 371
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;->mOnReplayFinishedCallback:Lcom/vlingo/core/internal/dialogmanager/CoreTester$OnFinishedCallback;

    .line 429
    return-void
.end method

.method private getNextInput()Ljava/lang/String;
    .locals 1

    .prologue
    .line 519
    const/4 v0, 0x0

    return-object v0
.end method

.method private initializeMsg(Ljava/lang/String;Landroid/os/Message;)Z
    .locals 1
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 611
    const/4 v0, 0x1

    return v0
.end method

.method private makeIntent(Ljava/lang/String;Landroid/os/Message;)V
    .locals 0
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 572
    return-void
.end method

.method private makeServerResponse(Ljava/lang/String;Landroid/os/Message;)V
    .locals 0
    .param p1, "xmlString"    # Ljava/lang/String;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 622
    return-void
.end method

.method private waitForWidget(Ljava/lang/String;)V
    .locals 0
    .param p1, "rest"    # Ljava/lang/String;

    .prologue
    .line 650
    return-void
.end method


# virtual methods
.method public run()V
    .locals 0

    .prologue
    .line 488
    return-void
.end method

.method public stopReplay()V
    .locals 0

    .prologue
    .line 499
    return-void
.end method
