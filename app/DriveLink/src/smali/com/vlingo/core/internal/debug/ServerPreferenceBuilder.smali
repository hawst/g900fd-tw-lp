.class public Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;
.super Lcom/vlingo/core/internal/debug/PreferenceBuilder;
.source "ServerPreferenceBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
        "<",
        "Lcom/vlingo/core/internal/debug/ServerPreferenceValue;",
        ">;"
    }
.end annotation


# static fields
.field private static summaryDisplay:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder$1;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder$1;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->summaryDisplay:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;-><init>()V

    .line 9
    sget-object v0, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->summaryDisplay:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->setShowAsSummary(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V

    .line 10
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "setting"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;-><init>(Ljava/lang/String;)V

    .line 14
    sget-object v0, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->summaryDisplay:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->setShowAsSummary(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V

    .line 15
    return-void
.end method


# virtual methods
.method public bridge synthetic register(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;
    .locals 1
    .param p1, "x0"    # Landroid/preference/PreferenceActivity;

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;)Lcom/vlingo/core/internal/debug/ServerPreferenceValue;

    move-result-object v0

    return-object v0
.end method

.method public register(Landroid/preference/PreferenceActivity;)Lcom/vlingo/core/internal/debug/ServerPreferenceValue;
    .locals 2
    .param p1, "activity"    # Landroid/preference/PreferenceActivity;

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->attach(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/debug/ServerPreferenceValue;

    .line 20
    .local v0, "server":Lcom/vlingo/core/internal/debug/ServerPreferenceValue;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/debug/ServerPreferenceValue;->setValue(Ljava/lang/String;)V

    .line 21
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->getSummary()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/debug/ServerPreferenceValue;->setSummary(Ljava/lang/CharSequence;)V

    .line 22
    return-object v0
.end method
