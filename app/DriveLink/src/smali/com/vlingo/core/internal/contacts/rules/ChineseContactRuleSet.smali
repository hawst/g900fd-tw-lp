.class public Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;
.super Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
.source "ChineseContactRuleSet.java"


# static fields
.field private static DEFAULT_FIELDS:[Ljava/lang/String;

.field private static GIVEN_NAME_FIELDS:[Ljava/lang/String;


# instance fields
.field private criteria:Lcom/vlingo/core/internal/contacts/ContactExitCriteria;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "data1"

    aput-object v1, v0, v3

    const-string/jumbo v1, "data9"

    aput-object v1, v0, v4

    const-string/jumbo v1, "data7"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string/jumbo v2, "data9 || data7"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "data7 || data9"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->DEFAULT_FIELDS:[Ljava/lang/String;

    .line 31
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "data1"

    aput-object v1, v0, v3

    const-string/jumbo v1, "phonetic_name"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;-><init>()V

    .line 37
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;-><init>(Z)V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->criteria:Lcom/vlingo/core/internal/contacts/ContactExitCriteria;

    .line 38
    return-void
.end method


# virtual methods
.method public canProcess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isChineseString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public generateRules(Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x4

    const/16 v12, 0x1e

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 57
    const/4 v3, 0x0

    .line 59
    .local v3, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 60
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 61
    .restart local v3    # "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    const-string/jumbo v0, "\u5c0f"

    .line 62
    .local v0, "hieroglyphBig":Ljava/lang/String;
    const-string/jumbo v1, "\u5927"

    .line 64
    .local v1, "hieroglyphSmall":Ljava/lang/String;
    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "\u5c0f\u59d0"

    aput-object v5, v4, v9

    const-string/jumbo v5, "\u592a\u592a"

    aput-object v5, v4, v10

    const-string/jumbo v5, "\u5973\u58eb"

    aput-object v5, v4, v11

    const/4 v5, 0x3

    const-string/jumbo v6, "\u8001\u7237"

    aput-object v6, v4, v5

    const-string/jumbo v5, "\u5148\u751f"

    aput-object v5, v4, v13

    const/4 v5, 0x5

    const-string/jumbo v6, "\u8001\u5e08"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string/jumbo v6, "\u592b\u4eba"

    aput-object v6, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 74
    .local v2, "honorificPostfixes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v13, :cond_0

    .line 77
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v5, "Full Match"

    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p1, v6}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v8, 0x64

    invoke-direct {v7, v8}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v4, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v4, v13, :cond_2

    .line 82
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v5, "Full Match"

    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v8, 0x64

    invoke-direct {v7, v8}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v4, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    invoke-virtual {p1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 89
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v5, "Partial Match"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v9, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v7, v12}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v4, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    .end local v0    # "hieroglyphBig":Ljava/lang/String;
    .end local v1    # "hieroglyphSmall":Ljava/lang/String;
    .end local v2    # "honorificPostfixes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :goto_0
    return-object v3

    .line 93
    .restart local v0    # "hieroglyphBig":Ljava/lang/String;
    .restart local v1    # "hieroglyphSmall":Ljava/lang/String;
    .restart local v2    # "honorificPostfixes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    .line 96
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v5, "Full Match"

    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v8, 0x64

    invoke-direct {v7, v8}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v4, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-virtual {p1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 108
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v5, "Partial Match w/o honorific"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v7, v12}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v4, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v4, v11, :cond_4

    .line 124
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v5, "Partial Match on First Name"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p1, v7}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " OR "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v7, v8}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " OR "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "__"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v7, v8}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v8, 0x3c

    invoke-direct {v7, v8}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v4, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_1

    .line 138
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v5, "Partial Match doubling first or second hieroglyph"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "_;_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v7, v12}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v4, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v5, "Partial Match first name with last name"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v7, v12}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v4, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 142
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v4, v10, :cond_1

    .line 151
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v5, "Partial Match only first name"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p1, v7}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " OR "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v7, v8}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " OR "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "__"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v7, v8}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " OR "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "___"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v7, v8}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v8, 0x50

    invoke-direct {v7, v8}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v4, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->criteria:Lcom/vlingo/core/internal/contacts/ContactExitCriteria;

    return-object v0
.end method

.method public queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 163
    const-string/jumbo v0, "vnd.android.cursor.item/name"

    sget-object v1, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->DEFAULT_FIELDS:[Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public varargs queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;

    .prologue
    .line 167
    const-string/jumbo v0, "vnd.android.cursor.item/name"

    invoke-static {p1, v0, p2}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public skipExtraData()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method
