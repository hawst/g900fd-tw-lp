.class public final Lcom/vlingo/core/internal/location/LocationUtils;
.super Ljava/lang/Object;
.source "LocationUtils.java"


# static fields
.field private static final GOOGLE_NAV_SUPPORTED_COUNTRIES_CODES:[Ljava/lang/String;

.field private static final MAX_ADDRESSES:I = 0x1

.field private static final NO_LAT:D

.field private static final NO_LONG:D

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const-class v0, Lcom/vlingo/core/internal/location/LocationUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/location/LocationUtils;->TAG:Ljava/lang/String;

    .line 42
    const/16 v0, 0x4a

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "DZ"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "AR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "AU"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "AT"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "BH"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "BE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "BR"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "BG"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "CM"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "CA"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "CR"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "CI"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "CZ"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "DK"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "DO"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "EC"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "EG"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "EE"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "ET"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "FI"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "FR"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "DE"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "GH"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "GR"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "GT"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "HK"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "HU"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "IS"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "IN"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "ID"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "IE"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "IL"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "IT"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "JP"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "JO"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "KE"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "KW"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "LV"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "LB"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "LT"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "LU"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "MY"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "MX"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "NL"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "NZ"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "NG"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "NO"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "OM"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string/jumbo v2, "PE"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "PH"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "PL"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string/jumbo v2, "PT"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "QA"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string/jumbo v2, "RO"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string/jumbo v2, "RU"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string/jumbo v2, "RW"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string/jumbo v2, "SA"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string/jumbo v2, "SN"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string/jumbo v2, "SG"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string/jumbo v2, "SK"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string/jumbo v2, "SI"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string/jumbo v2, "ZA"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string/jumbo v2, "ES"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string/jumbo v2, "SE"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string/jumbo v2, "CH"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string/jumbo v2, "TW"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string/jumbo v2, "TH"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string/jumbo v2, "UG"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string/jumbo v2, "UA"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string/jumbo v2, "AE"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string/jumbo v2, "GB"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string/jumbo v2, "US"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string/jumbo v2, "UY"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string/jumbo v2, "VE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/location/LocationUtils;->GOOGLE_NAV_SUPPORTED_COUNTRIES_CODES:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static doesLocationUseNetworkProvider()Z
    .locals 3

    .prologue
    .line 171
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 172
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    .line 173
    .local v1, "locationManager":Landroid/location/LocationManager;
    const-string/jumbo v2, "network"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    return v2
.end method

.method public static getAddress(Ljava/lang/String;)Landroid/location/Address;
    .locals 8
    .param p0, "location"    # Ljava/lang/String;

    .prologue
    .line 218
    const/4 v1, 0x0

    .line 219
    .local v1, "address":Landroid/location/Address;
    new-instance v4, Landroid/location/Geocoder;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 222
    .local v4, "geocoder":Landroid/location/Geocoder;
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {v4, p0, v5}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    .line 223
    .local v2, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 224
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Landroid/location/Address;

    move-object v1, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    move-object v5, v1

    .line 230
    .end local v2    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :goto_0
    return-object v5

    .line 226
    :catch_0
    move-exception v3

    .line 227
    .local v3, "e":Ljava/io/IOException;
    sget-object v5, Lcom/vlingo/core/internal/location/LocationUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "getLongLat(String location) catch exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static getCellTowerInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getCellTowerInfo()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLastLat()D
    .locals 2

    .prologue
    .line 142
    const-string/jumbo v0, "FAKE_LAT_LONG"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    const-string/jumbo v0, "FAKE_LAT"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 145
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getLastLat()D

    move-result-wide v0

    goto :goto_0
.end method

.method public static getLastLong()D
    .locals 2

    .prologue
    .line 154
    const-string/jumbo v0, "FAKE_LAT_LONG"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const-string/jumbo v0, "FAKE_LONG"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 157
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getLastLong()D

    move-result-wide v0

    goto :goto_0
.end method

.method public static getUserLat()Ljava/lang/String;
    .locals 3

    .prologue
    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLat()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUserLong()Ljava/lang/String;
    .locals 3

    .prologue
    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLong()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isGoogleNavAvailable()Z
    .locals 13

    .prologue
    const/4 v9, 0x1

    const-wide/16 v11, 0x0

    const/4 v10, 0x0

    .line 185
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLat()D

    move-result-wide v1

    .line 186
    .local v1, "latitude":D
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLong()D

    move-result-wide v3

    .line 189
    .local v3, "longitude":D
    cmpl-double v5, v3, v11

    if-eqz v5, :cond_0

    cmpl-double v5, v1, v11

    if-nez v5, :cond_1

    :cond_0
    move v5, v10

    .line 206
    :goto_0
    return v5

    .line 191
    :cond_1
    new-instance v0, Landroid/location/Geocoder;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 193
    .local v0, "geocoder":Landroid/location/Geocoder;
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 196
    .local v7, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 197
    const/4 v5, 0x0

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    .line 198
    .local v6, "addr":Landroid/location/Address;
    sget-object v5, Lcom/vlingo/core/internal/location/LocationUtils;->GOOGLE_NAV_SUPPORTED_COUNTRIES_CODES:[Ljava/lang/String;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v6}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v5, v11}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_2

    move v5, v9

    .line 199
    goto :goto_0

    :cond_2
    move v5, v10

    .line 200
    goto :goto_0

    .end local v6    # "addr":Landroid/location/Address;
    :cond_3
    move v5, v10

    .line 202
    goto :goto_0

    .line 204
    .end local v7    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :catch_0
    move-exception v8

    .line 205
    .local v8, "e":Ljava/io/IOException;
    sget-object v5, Lcom/vlingo/core/internal/location/LocationUtils;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "isGoogleNavAvailable() catch exception: "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v10

    .line 206
    goto :goto_0
.end method
