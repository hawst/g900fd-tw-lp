.class public Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;
.super Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;
.source "MessageResolvedEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent",
        "<",
        "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
        ">;"
    }
.end annotation


# instance fields
.field private final ID_FIELD_NAME:Ljava/lang/String;

.field private final MESSAGES_FIELD_GROUP_NAME:Ljava/lang/String;

.field private final MESSAGE_FILED_GROUP_NAME:Ljava/lang/String;

.field private final NAME:Ljava/lang/String;

.field private final SENDER_NAME_FIELD_NAME:Ljava/lang/String;

.field private final SENDER_PHHONE_FIELD_NAME:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;-><init>()V

    .line 18
    const-string/jumbo v0, "message-resolved"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->NAME:Ljava/lang/String;

    .line 19
    const-string/jumbo v0, "messages"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->MESSAGES_FIELD_GROUP_NAME:Ljava/lang/String;

    .line 20
    const-string/jumbo v0, "message"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->MESSAGE_FILED_GROUP_NAME:Ljava/lang/String;

    .line 21
    const-string/jumbo v0, "id"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->ID_FIELD_NAME:Ljava/lang/String;

    .line 22
    const-string/jumbo v0, "sender.name"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->SENDER_NAME_FIELD_NAME:Ljava/lang/String;

    .line 23
    const-string/jumbo v0, "sender.phone"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->SENDER_PHHONE_FIELD_NAME:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V
    .locals 3
    .param p1, "message"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 38
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;-><init>(Ljava/util/List;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v1, 0x0

    .line 30
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, p1, v1, v0}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;-><init>(Ljava/util/List;II)V

    .line 18
    const-string/jumbo v0, "message-resolved"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->NAME:Ljava/lang/String;

    .line 19
    const-string/jumbo v0, "messages"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->MESSAGES_FIELD_GROUP_NAME:Ljava/lang/String;

    .line 20
    const-string/jumbo v0, "message"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->MESSAGE_FILED_GROUP_NAME:Ljava/lang/String;

    .line 21
    const-string/jumbo v0, "id"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->ID_FIELD_NAME:Ljava/lang/String;

    .line 22
    const-string/jumbo v0, "sender.name"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->SENDER_NAME_FIELD_NAME:Ljava/lang/String;

    .line 23
    const-string/jumbo v0, "sender.phone"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->SENDER_PHHONE_FIELD_NAME:Ljava/lang/String;

    .line 31
    return-void

    .line 30
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string/jumbo v0, "message-resolved"

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 8

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_0

    .line 52
    const/4 v6, 0x0

    .line 70
    :goto_0
    return-object v6

    .line 55
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v6, "message-resolved"

    invoke-direct {v0, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 56
    .local v0, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->writeTotalCount(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)V

    .line 57
    new-instance v5, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v6, "messages"

    invoke-direct {v5, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 58
    .local v5, "messagesBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/MessageResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 59
    .local v3, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    new-instance v4, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v6, "message"

    invoke-direct {v4, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 60
    .local v4, "messageBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "idStr":Ljava/lang/String;
    const-string/jumbo v6, "id"

    invoke-virtual {v4, v6, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 62
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 63
    const-string/jumbo v6, "sender.name"

    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 65
    :cond_1
    const-string/jumbo v6, "sender.phone"

    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 67
    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    goto :goto_1

    .line 69
    .end local v2    # "idStr":Ljava/lang/String;
    .end local v3    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v4    # "messageBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    :cond_2
    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 70
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v6

    goto :goto_0
.end method
