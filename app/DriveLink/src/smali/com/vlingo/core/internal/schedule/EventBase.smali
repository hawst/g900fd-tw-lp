.class public abstract Lcom/vlingo/core/internal/schedule/EventBase;
.super Ljava/lang/Object;
.source "EventBase.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Cloneable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/vlingo/core/internal/schedule/EventBase;",
        ">;"
    }
.end annotation


# instance fields
.field protected begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

.field protected description:Ljava/lang/String;

.field protected end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

.field protected id:J

.field private mAmPmText:Ljava/lang/String;

.field private newEventUri:Landroid/net/Uri;

.field protected title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-direct {v0, p0, v1, v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    .line 34
    new-instance v0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-direct {v0, p0, v1, v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    .line 36
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->mAmPmText:Ljava/lang/String;

    .line 219
    return-void
.end method


# virtual methods
.method public clone()Lcom/vlingo/core/internal/schedule/EventBase;
    .locals 2

    .prologue
    .line 43
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/schedule/EventBase;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_0
    return-object v1

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    .line 47
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/EventBase;->clone()Lcom/vlingo/core/internal/schedule/EventBase;

    move-result-object v0

    return-object v0
.end method

.method public compareTo(Lcom/vlingo/core/internal/schedule/EventBase;)I
    .locals 4
    .param p1, "compareObject"    # Lcom/vlingo/core/internal/schedule/EventBase;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v0

    iget-object v2, p1, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 53
    const/4 v0, -0x1

    .line 57
    :goto_0
    return v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v0

    iget-object v2, p1, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 55
    const/4 v0, 0x0

    goto :goto_0

    .line 57
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, Lcom/vlingo/core/internal/schedule/EventBase;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/schedule/EventBase;->compareTo(Lcom/vlingo/core/internal/schedule/EventBase;)I

    move-result v0

    return v0
.end method

.method protected determineDate(JLjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "val"    # J
    .param p3, "canonical"    # Ljava/lang/String;

    .prologue
    .line 138
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_0

    invoke-static {p3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 142
    .local v1, "dateVal":Ljava/util/Date;
    const-string/jumbo v2, "%04d-%02d-%02d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/util/Date;->getYear()I

    move-result v5

    add-int/lit16 v5, v5, 0x76c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v1}, Ljava/util/Date;->getMonth()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v1}, Ljava/util/Date;->getDate()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .end local v1    # "dateVal":Ljava/util/Date;
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p3

    goto :goto_0
.end method

.method protected determineTime(JLjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "val"    # J
    .param p3, "canonical"    # Ljava/lang/String;

    .prologue
    .line 167
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-lez v4, :cond_2

    invoke-static {p3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 170
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 171
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "time_12_24"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 172
    .local v2, "phoneTimeFormat":Ljava/lang/String;
    const/4 v1, 0x0

    .line 173
    .local v1, "is12Format":Z
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 174
    const-string/jumbo v4, "12"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    .line 176
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2, v1}, Lcom/vlingo/core/internal/schedule/EventBase;->formatTime(JZ)Ljava/lang/String;

    move-result-object v3

    .line 183
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "is12Format":Z
    .end local v2    # "phoneTimeFormat":Ljava/lang/String;
    :goto_1
    return-object v3

    .line 174
    .restart local v0    # "context":Landroid/content/Context;
    .restart local v1    # "is12Format":Z
    .restart local v2    # "phoneTimeFormat":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "is12Format":Z
    .end local v2    # "phoneTimeFormat":Ljava/lang/String;
    :cond_2
    move-object v3, p3

    .line 183
    goto :goto_1
.end method

.method protected formatTime(JZ)Ljava/lang/String;
    .locals 6
    .param p1, "mills"    # J
    .param p3, "is12Format"    # Z

    .prologue
    const/4 v0, 0x1

    .line 187
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 188
    .local v2, "sb":Ljava/lang/StringBuffer;
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 189
    .local v3, "t1":Landroid/text/format/Time;
    invoke-virtual {v3, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 190
    invoke-virtual {v3, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 192
    iget v4, v3, Landroid/text/format/Time;->hour:I

    const/16 v5, 0xc

    if-ge v4, v5, :cond_0

    .line 193
    .local v0, "am":Z
    :goto_0
    if-eqz p3, :cond_2

    .line 194
    const-string/jumbo v4, "%I:%M"

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 195
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_AM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 198
    .local v1, "ampm":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/schedule/EventBase;->setAmPmText(Ljava/lang/String;)V

    .line 203
    .end local v1    # "ampm":Ljava/lang/String;
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 192
    .end local v0    # "am":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 195
    .restart local v0    # "am":Z
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_PM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 200
    :cond_2
    const-string/jumbo v4, "%H:%M"

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 201
    const-string/jumbo v4, ""

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/schedule/EventBase;->setAmPmText(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public getAmPmText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->mAmPmText:Ljava/lang/String;

    return-object v0
.end method

.method public getBegin()J
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getBeginCanonical()Ljava/lang/String;
    .locals 3

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getDateStr()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTimeStr(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBeginDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getDateStr()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBeginTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTimeStr(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getEventID()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->id:J

    return-wide v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->id:J

    return-wide v0
.end method

.method public getNewEventUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->newEventUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->title:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->title:Ljava/lang/String;

    goto :goto_0
.end method

.method protected normalizeToNow()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 208
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 209
    .local v0, "nowMillis":J
    new-instance v2, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-direct {v2, p0, v0, v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V

    iput-object v2, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    .line 213
    return-void
.end method

.method public setAmPmText(Ljava/lang/String;)V
    .locals 0
    .param p1, "ampmText"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/EventBase;->mAmPmText:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setBegin(J)V
    .locals 1
    .param p1, "begin"    # J

    .prologue
    .line 85
    new-instance v0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    .line 86
    return-void
.end method

.method public setBegin(Ljava/lang/String;)V
    .locals 3
    .param p1, "canonical"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    new-instance v0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    const-wide/16 v1, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/EventBase;->description:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setEnd(J)V
    .locals 1
    .param p1, "end"    # J

    .prologue
    .line 102
    new-instance v0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/EventBase;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    .line 103
    return-void
.end method

.method public setEnd(Ljava/lang/String;)V
    .locals 4
    .param p1, "canonical"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 107
    const/16 v1, 0x20

    invoke-static {p1, v1}, Lcom/vlingo/core/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "dateTime":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 109
    new-instance v1, Ljava/security/InvalidParameterException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Canonical begin must be of format YYYY-MM-DD hh:mm, received: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 111
    :cond_0
    new-instance v1, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/schedule/EventBase;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    .line 113
    .end local v0    # "dateTime":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected setEndTime()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 217
    return-void
.end method

.method public setEventID(J)V
    .locals 0
    .param p1, "eventID"    # J

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/vlingo/core/internal/schedule/EventBase;->id:J

    .line 74
    return-void
.end method

.method public setNewEventUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "newEventUri"    # Landroid/net/Uri;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/EventBase;->newEventUri:Landroid/net/Uri;

    .line 66
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/EventBase;->title:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ": event title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/EventBase;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " desc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/EventBase;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " start="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/EventBase;->getBegin()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/EventBase;->getID()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
