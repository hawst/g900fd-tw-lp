.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.super Ljava/lang/Object;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$2;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Spotting;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Idle;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$BaseState;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static volatile instancesCounter:I


# instance fields
.field private dialogFlowTaskRegulator:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

.field final idle:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

.field private instanceSerialNumber:I

.field private logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

.field private mChunkSize:I

.field private mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

.field private mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

.field private mRawAudioBuffer:[S

.field mRecordThread:Ljava/lang/Thread;

.field private mRecordThreadName:Ljava/lang/String;

.field private volatile mRestartWhenIdle:Z

.field private mSampleRate:I

.field private mStopPhraseSpotterRightNow:Z

.field volatile presentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

.field private recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

.field private resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

.field final spot:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

.field private spotterThreadCount:I

.field final start:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

.field private final stateTransition:Ljava/lang/Object;

.field final stop:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->instancesCounter:I

    .line 65
    const-class v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->spotterThreadCount:I

    .line 71
    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRawAudioBuffer:[S

    .line 72
    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mChunkSize:I

    .line 73
    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSampleRate:I

    .line 74
    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRestartWhenIdle:Z

    .line 83
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;

    .line 84
    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Ljava/lang/Thread;

    .line 85
    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThreadName:Ljava/lang/String;

    .line 88
    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->dialogFlowTaskRegulator:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    .line 143
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    invoke-direct {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    .line 201
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    invoke-direct {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    .line 205
    sget v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->instancesCounter:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->instancesCounter:I

    .line 206
    sget v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->instancesCounter:I

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->instanceSerialNumber:I

    .line 208
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Idle;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Idle;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->idle:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 209
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Spotting;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Spotting;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->spot:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 210
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Stopping;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stop:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 211
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Starting;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->start:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 214
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getStateInstance(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->presentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 215
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRestartWhenIdle:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRestartWhenIdle:Z

    return p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1004(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->spotterThreadCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->spotterThreadCount:I

    return v0
.end method

.method static synthetic access$1102(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThreadName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getListener()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getAudioLogger()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->createSpotters()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStopPhraseSpotterRightNow:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)[S
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRawAudioBuffer:[S

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    return-object v0
.end method

.method static synthetic access$302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    return-object p1
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    return-object p1
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSampleRate:I

    return v0
.end method

.method static synthetic access$502(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSampleRate:I

    return p1
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->establishChunkSize()V

    return-void
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->establishLogging()V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mChunkSize:I

    return v0
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 54
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->instanceSerialNumber:I

    return v0
.end method

.method private createSpotters()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 382
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 384
    .local v3, "spotterEngines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    sget-object v4, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v4}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v1

    .line 386
    .local v1, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    if-eqz v1, :cond_0

    .line 388
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;

    .line 392
    .local v2, "spotterEngine":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    .end local v2    # "spotterEngine":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 403
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->getInstance(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 406
    :cond_1
    return-object v3

    .line 393
    :catch_0
    move-exception v0

    .line 396
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private establishChunkSize()V
    .locals 3

    .prologue
    .line 359
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getChunkLength()I

    move-result v1

    iget v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSampleRate:I

    mul-int/2addr v1, v2

    div-int/lit16 v0, v1, 0x3e8

    .line 361
    .local v0, "newChunkSize":I
    iget v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mChunkSize:I

    if-eq v0, v1, :cond_0

    .line 362
    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mChunkSize:I

    .line 363
    iget v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mChunkSize:I

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRawAudioBuffer:[S

    .line 365
    :cond_0
    return-void
.end method

.method private establishLogging()V
    .locals 2

    .prologue
    .line 349
    const/4 v0, 0x0

    .line 355
    .local v0, "target":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->setLogger(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;)V

    .line 356
    return-void
.end method

.method private getAudioLogger()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    invoke-direct {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    .line 377
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    return-object v0

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    goto :goto_0
.end method

.method private getListener()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    if-nez v0, :cond_0

    .line 322
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$1;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    return-object v0
.end method


# virtual methods
.method public getDialogFlowTaskRegulator()Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->dialogFlowTaskRegulator:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    return-object v0
.end method

.method getState()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->presentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    return-object v0
.end method

.method final getStateInstance(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    .locals 3
    .param p1, "state"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->idle:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 269
    .local v0, "spotterState":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    sget-object v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$2;->$SwitchMap$com$vlingo$core$internal$phrasespotter$PhraseSpotterControl$State:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 276
    :goto_0
    return-object v0

    .line 270
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->spot:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    goto :goto_0

    .line 271
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stop:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    goto :goto_0

    .line 272
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->start:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    goto :goto_0

    .line 273
    :pswitch_3
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->idle:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    goto :goto_0

    .line 269
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method isRecording()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->target()Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->target()Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isSpotting()Z
    .locals 2

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getState()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->SPOTTING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->is(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getState()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->STARTING:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->is(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDialogFlowTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V
    .locals 0
    .param p1, "dialogFlowTaskRegulator"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->dialogFlowTaskRegulator:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    .line 98
    return-void
.end method

.method setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    .locals 4
    .param p1, "state"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .prologue
    .line 301
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;

    monitor-enter v1

    .line 302
    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setState(): old: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->presentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; new: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->presentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    invoke-interface {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->onExit()V

    .line 304
    if-nez p1, :cond_0

    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getStateInstance(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object p1

    .end local p1    # "state":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->presentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 305
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->presentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    invoke-interface {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->onEnter()V

    .line 306
    monitor-exit v1

    .line 307
    return-void

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)V
    .locals 1
    .param p1, "state"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    .prologue
    .line 286
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getStateInstance(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 287
    return-void
.end method

.method public setStopPhraseSpotterRightNow(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStopPhraseSpotterRightNow:Z

    .line 102
    if-eqz p1, :cond_0

    .line 103
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->recording:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->stop()V

    .line 107
    :cond_0
    return-void
.end method

.method public start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 1
    .param p1, "params"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "listener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getState()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 229
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getState()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->stop()V

    .line 235
    return-void
.end method

.method public stop(Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    .locals 4
    .param p1, "resumeControl"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 241
    const/4 v0, 0x1

    .line 242
    .local v0, "resumeControlImmediately":Z
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stateTransition:Ljava/lang/Object;

    monitor-enter v2

    .line 243
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getState()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v1

    sget-object v3, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-interface {v1, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->is(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 244
    const/4 v0, 0x0

    .line 245
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 250
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .line 251
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getState()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->stop()V

    .line 253
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    if-eqz v0, :cond_2

    .line 255
    invoke-interface {p1}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 257
    :cond_2
    return-void

    .line 253
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
