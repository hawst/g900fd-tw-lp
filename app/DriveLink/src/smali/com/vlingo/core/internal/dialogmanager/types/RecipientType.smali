.class public Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
.super Ljava/lang/Object;
.source "RecipientType.java"


# instance fields
.field private address:Ljava/lang/String;

.field private displayName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;
    .param p2, "address"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->displayName:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->address:Ljava/lang/String;

    .line 11
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 30
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 34
    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 36
    .local v0, "recipientObj":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->address:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->address:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 39
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->displayName:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->displayName:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 42
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 36
    :cond_4
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->address:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->address:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 39
    :cond_5
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->displayName:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->displayName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x1

    .line 48
    .local v0, "hash":I
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->address:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->address:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit8 v0, v1, 0x11

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->displayName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 52
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->displayName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 54
    :cond_1
    return v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->address:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->displayName:Ljava/lang/String;

    .line 19
    return-void
.end method
