.class public Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;
.super Landroid/content/ContentProvider;
.source "ContactContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider$1;,
        Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider$VersionCursor;
    }
.end annotation


# static fields
.field private static final DATA:I = 0xa

.field private static final DATA_ID:I = 0x3f2

.field private static final ID_OFFSET:I = 0x3e8

.field private static S_URI_MATCHER:Landroid/content/UriMatcher; = null

.field private static final VERSION:I = 0x1

.field private static final VERSION_CODE:I = 0x14


# instance fields
.field private database:Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 179
    return-void
.end method

.method private static getUriMatcher()Landroid/content/UriMatcher;
    .locals 4

    .prologue
    .line 31
    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->S_URI_MATCHER:Landroid/content/UriMatcher;

    if-nez v1, :cond_0

    .line 32
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->S_URI_MATCHER:Landroid/content/UriMatcher;

    .line 33
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getAuthority()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "authority":Ljava/lang/String;
    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->S_URI_MATCHER:Landroid/content/UriMatcher;

    const-string/jumbo v2, "view_data"

    const/16 v3, 0xa

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 35
    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->S_URI_MATCHER:Landroid/content/UriMatcher;

    const-string/jumbo v2, "view_data/#"

    const/16 v3, 0x3f2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 36
    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->S_URI_MATCHER:Landroid/content/UriMatcher;

    const-string/jumbo v2, "get_version"

    const/16 v3, 0x14

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 38
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->S_URI_MATCHER:Landroid/content/UriMatcher;

    return-object v1
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 106
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getUriMatcher()Landroid/content/UriMatcher;

    move-result-object v8

    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    .line 107
    .local v7, "uriType":I
    iget-object v8, p0, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->database:Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 108
    .local v4, "sqlDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 109
    .local v1, "count":I
    invoke-virtual {p0, v7}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getTableClass(I)Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;

    move-result-object v5

    .line 110
    .local v5, "tableBase":Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;
    if-eqz v5, :cond_0

    .line 111
    invoke-virtual {v5, v4, p2, p1}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->bulkInsert(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;Landroid/net/Uri;)I

    move-result v1

    .line 115
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getUpdateURIs(Ljava/lang/String;)[Landroid/net/Uri;

    move-result-object v0

    .local v0, "arr$":[Landroid/net/Uri;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v6, v0, v2

    .line 116
    .local v6, "updateUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 115
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 113
    .end local v0    # "arr$":[Landroid/net/Uri;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v6    # "updateUri":Landroid/net/Uri;
    :cond_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Unknown URI: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 120
    .restart local v0    # "arr$":[Landroid/net/Uri;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    :cond_1
    return v1
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 56
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getUriMatcher()Landroid/content/UriMatcher;

    move-result-object v8

    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    .line 57
    .local v7, "uriType":I
    iget-object v8, p0, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->database:Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 58
    .local v4, "sqlDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .line 59
    .local v3, "rowsDeleted":I
    invoke-virtual {p0, v7}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getTableClass(I)Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;

    move-result-object v5

    .line 60
    .local v5, "tableBase":Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;
    if-eqz v5, :cond_1

    .line 61
    const/16 v8, 0x3e8

    if-ge v7, v8, :cond_0

    .line 62
    invoke-virtual {v5, v4, p2, p3}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->delete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 69
    :goto_0
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getUpdateURIs(Ljava/lang/String;)[Landroid/net/Uri;

    move-result-object v0

    .local v0, "arr$":[Landroid/net/Uri;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v6, v0, v1

    .line 70
    .local v6, "updateUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 64
    .end local v0    # "arr$":[Landroid/net/Uri;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v6    # "updateUri":Landroid/net/Uri;
    :cond_0
    invoke-virtual {v5, v4, p1, p2, p3}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->deleteByID(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 67
    :cond_1
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Unknown URI: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 74
    .restart local v0    # "arr$":[Landroid/net/Uri;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    :cond_2
    return v3
.end method

.method getTableClass(I)Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;
    .locals 1
    .param p1, "uriType"    # I

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 50
    new-instance v0, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;-><init>()V

    :goto_0
    return-object v0

    .line 45
    :sswitch_0
    new-instance v0, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;-><init>()V

    goto :goto_0

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x3f2 -> :sswitch_0
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 85
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getUriMatcher()Landroid/content/UriMatcher;

    move-result-object v10

    invoke-virtual {v10, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    .line 86
    .local v9, "uriType":I
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->database:Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;

    invoke-virtual {v10}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 87
    .local v6, "sqlDB":Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v3, 0x0

    .line 89
    .local v3, "id":J
    invoke-virtual {p0, v9}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getTableClass(I)Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;

    move-result-object v7

    .line 90
    .local v7, "tableBase":Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;
    if-eqz v7, :cond_0

    .line 91
    invoke-virtual {v7, v6, p2}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->insert(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v3

    .line 92
    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getBasePath()Ljava/lang/String;

    move-result-object v1

    .line 96
    .local v1, "basePath":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getUpdateURIs(Ljava/lang/String;)[Landroid/net/Uri;

    move-result-object v0

    .local v0, "arr$":[Landroid/net/Uri;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v8, v0, v2

    .line 97
    .local v8, "updateUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v8, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 96
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 94
    .end local v0    # "arr$":[Landroid/net/Uri;
    .end local v1    # "basePath":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v5    # "len$":I
    .end local v8    # "updateUri":Landroid/net/Uri;
    :cond_0
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Unknown URI: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 101
    .restart local v0    # "arr$":[Landroid/net/Uri;
    .restart local v1    # "basePath":Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v5    # "len$":I
    :cond_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    return-object v10
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 125
    new-instance v0, Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->database:Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;

    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v9

    .line 140
    .local v9, "authority":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getUriMatcher()Landroid/content/UriMatcher;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v11

    .line 141
    .local v11, "uriType":I
    const/16 v3, 0x14

    if-ne v11, v3, :cond_0

    .line 142
    new-instance v10, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider$VersionCursor;

    const/4 v3, 0x0

    invoke-direct {v10, p0, v3}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider$VersionCursor;-><init>(Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider$1;)V

    .line 159
    :goto_0
    return-object v10

    .line 144
    :cond_0
    invoke-virtual {p0, v11}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getTableClass(I)Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;

    move-result-object v0

    .line 145
    .local v0, "tableBase":Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;
    if-eqz v0, :cond_1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    .line 146
    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/sqlite/SQLiteQueryBuilder;

    move-result-object v1

    .line 151
    .local v1, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->database:Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 152
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 155
    .local v10, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v0, v9}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getNotificationUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v10, v3, v4}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto :goto_0

    .line 148
    .end local v1    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Unknown URI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 165
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getUriMatcher()Landroid/content/UriMatcher;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v11

    .line 166
    .local v11, "uriType":I
    invoke-virtual {p0, v11}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getTableClass(I)Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;

    move-result-object v0

    .line 167
    .local v0, "tableBase":Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->database:Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 168
    .local v1, "sqlDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v9, 0x0

    .local v9, "rowsUpdated":I
    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    .line 169
    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->update(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v9

    .line 170
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;->getUpdateURIs(Ljava/lang/String;)[Landroid/net/Uri;

    move-result-object v6

    .local v6, "arr$":[Landroid/net/Uri;
    array-length v8, v6

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v10, v6, v7

    .line 171
    .local v10, "updateUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/contentprovider/ContactContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v10, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 170
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 175
    .end local v10    # "updateUri":Landroid/net/Uri;
    :cond_0
    return v9
.end method
