.class public final Lcom/vlingo/core/internal/hybrid/util/AliasTool;
.super Ljava/lang/Object;
.source "AliasTool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/hybrid/util/AliasTool$1;,
        Lcom/vlingo/core/internal/hybrid/util/AliasTool$LanguageChangeListener;,
        Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;
    }
.end annotation


# static fields
.field private static final LANGUAGE_CHANGE_LISTENER:Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;

.field private static final UTF_8:Ljava/lang/String; = "UTF8"

.field private static aliasesToLabels:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static context:Landroid/content/Context;

.field private static isInitialized:Z

.field private static labelsToAliases:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$LanguageChangeListener;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/hybrid/util/AliasTool$LanguageChangeListener;-><init>(Lcom/vlingo/core/internal/hybrid/util/AliasTool$1;)V

    sput-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->LANGUAGE_CHANGE_LISTENER:Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->labelsToAliases:Ljava/util/Map;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->aliasesToLabels:Ljava/util/Map;

    .line 57
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->isInitialized:Z

    .line 61
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->context:Landroid/content/Context;

    .line 62
    sget-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->LANGUAGE_CHANGE_LISTENER:Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;

    sget-object v1, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;->register(Landroid/content/Context;)V

    .line 63
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->labelsToAliases:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200()Landroid/content/Context;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 48
    sput-boolean p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->isInitialized:Z

    return p0
.end method

.method public static changeLanguage(Ljava/lang/String;)V
    .locals 1
    .param p0, "newLanguage"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    sget-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->LANGUAGE_CHANGE_LISTENER:Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;->onLanguageChanged(Ljava/lang/String;)V

    .line 134
    :cond_0
    return-void
.end method

.method private static convertInputStreamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 5
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    const-string/jumbo v4, "UTF8"

    invoke-direct {v3, p0, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 168
    .local v1, "r":Ljava/io/BufferedReader;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 170
    .local v2, "total":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .local v0, "line":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 173
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static generateAliasesToLabelMap()V
    .locals 6

    .prologue
    .line 157
    sget-object v5, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->aliasesToLabels:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 158
    sget-object v5, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->labelsToAliases:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 159
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 160
    .local v4, "label":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 161
    .local v0, "alias":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->aliasesToLabels:Ljava/util/Map;

    invoke-interface {v5, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 164
    .end local v0    # "alias":Ljava/lang/String;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "label":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static getAliasesForLabel(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    invoke-static {}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->initialize()V

    .line 114
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->labelsToAliases:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    new-instance v1, Ljava/util/ArrayList;

    sget-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->labelsToAliases:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    .line 117
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public static getAppListByAlias(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "alias"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-static {}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->initialize()V

    .line 95
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 96
    new-instance v0, Lcom/vlingo/core/internal/util/OpenAppUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/OpenAppUtil;-><init>()V

    .line 97
    .local v0, "openAppUtil":Lcom/vlingo/core/internal/util/OpenAppUtil;
    invoke-static {p0}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->getLabelByAlias(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/OpenAppUtil;->buildMatchingAppList(Ljava/lang/String;)V

    .line 98
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v1

    .line 100
    .end local v0    # "openAppUtil":Lcom/vlingo/core/internal/util/OpenAppUtil;
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public static getLabelByAlias(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "alias"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-static {}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->initialize()V

    .line 78
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->aliasesToLabels:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    sget-object v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->aliasesToLabels:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 81
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method private static initialize()V
    .locals 7

    .prologue
    .line 137
    sget-boolean v4, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->isInitialized:Z

    if-nez v4, :cond_0

    .line 140
    :try_start_0
    sget-object v4, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_app_aliases:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 142
    .local v1, "inputStream":Ljava/io/InputStream;
    invoke-static {v1}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->convertInputStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    .line 143
    .local v3, "xmlContent":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->labelsToAliases:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 144
    new-instance v2, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;

    invoke-direct {v2}, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;-><init>()V

    .line 145
    .local v2, "parser":Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;
    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->parseXML(Ljava/lang/String;)V

    .line 146
    invoke-static {}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->generateAliasesToLabelMap()V

    .line 147
    const/4 v4, 0x1

    sput-boolean v4, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->isInitialized:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    .end local v2    # "parser":Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;
    .end local v3    # "xmlContent":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/io/IOException;
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->isInitialized:Z

    goto :goto_0
.end method

.method public static isInitialized()Z
    .locals 1

    .prologue
    .line 124
    sget-boolean v0, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->isInitialized:Z

    return v0
.end method
