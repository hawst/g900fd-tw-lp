.class public final Lcom/vlingo/core/internal/associatedservice/ServerHostUtil;
.super Ljava/lang/Object;
.source "ServerHostUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$1;,
        Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getApplicationHostName(Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;)Ljava/lang/String;
    .locals 5
    .param p0, "applicationQueryManager"    # Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    .param p1, "type"    # Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    .prologue
    .line 13
    sget-object v3, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$1;->$SwitchMap$com$vlingo$core$internal$associatedservice$ServerHostUtil$HostType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 31
    const/4 v2, 0x0

    .line 44
    :cond_0
    :goto_0
    return-object v2

    .line 15
    :pswitch_0
    const-string/jumbo v1, "SERVER_NAME"

    .line 16
    .local v1, "settingKeyName":Ljava/lang/String;
    const-string/jumbo v0, "server_host_asr"

    .line 37
    .local v0, "namedConfigItemName":Ljava/lang/String;
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "SETTINGS/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "svoiceValue":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 43
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 44
    goto :goto_0

    .line 19
    .end local v0    # "namedConfigItemName":Ljava/lang/String;
    .end local v1    # "settingKeyName":Ljava/lang/String;
    .end local v2    # "svoiceValue":Ljava/lang/String;
    :pswitch_1
    const-string/jumbo v1, "SERVICES_HOST_NAME"

    .line 20
    .restart local v1    # "settingKeyName":Ljava/lang/String;
    const-string/jumbo v0, "server_host_vcs"

    .line 21
    .restart local v0    # "namedConfigItemName":Ljava/lang/String;
    goto :goto_1

    .line 23
    .end local v0    # "namedConfigItemName":Ljava/lang/String;
    .end local v1    # "settingKeyName":Ljava/lang/String;
    :pswitch_2
    const-string/jumbo v1, "EVENTLOG_HOST_NAME"

    .line 24
    .restart local v1    # "settingKeyName":Ljava/lang/String;
    const-string/jumbo v0, "server_host_log"

    .line 25
    .restart local v0    # "namedConfigItemName":Ljava/lang/String;
    goto :goto_1

    .line 27
    .end local v0    # "namedConfigItemName":Ljava/lang/String;
    .end local v1    # "settingKeyName":Ljava/lang/String;
    :pswitch_3
    const-string/jumbo v1, "HELLO_HOST_NAME"

    .line 28
    .restart local v1    # "settingKeyName":Ljava/lang/String;
    const-string/jumbo v0, "server_host_hello"

    .line 29
    .restart local v0    # "namedConfigItemName":Ljava/lang/String;
    goto :goto_1

    .line 13
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
