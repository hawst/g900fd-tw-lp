.class public Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentProvider;
.super Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseContentProvider;
.source "ApplicationContentProvider.java"


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V
    .locals 0
    .param p1, "uploadContext"    # Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseContentProvider;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V

    .line 10
    return-void
.end method


# virtual methods
.method public getContentIterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<+",
            "Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentIterator;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentProvider;->getUploadContext()Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentIterator;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
