.class public interface abstract Lcom/vlingo/core/internal/questions/Answer$Subsection;
.super Ljava/lang/Object;
.source "Answer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/questions/Answer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Subsection"
.end annotation


# virtual methods
.method public abstract getHeight()I
.end method

.method public abstract getImage()Lcom/vlingo/core/internal/questions/DownloadableImage;
.end method

.method public abstract getImageUrl()Ljava/lang/String;
.end method

.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract getWidth()I
.end method

.method public abstract hasImage()Z
.end method
