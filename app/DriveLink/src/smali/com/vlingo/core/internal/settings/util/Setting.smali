.class public abstract Lcom/vlingo/core/internal/settings/util/Setting;
.super Ljava/lang/Object;
.source "Setting.java"


# static fields
.field public static final SETTING_RESOURCE_DOMAIN:Ljava/lang/String; = "vlingoVoiceSettingRD"

.field public static final TYPE_BOOLEAN:I = 0x0

.field public static final TYPE_DATA:I = 0x6

.field public static final TYPE_ENUM:I = 0x3

.field public static final TYPE_INT:I = 0x2

.field public static final TYPE_LONG:I = 0x4

.field public static final TYPE_SERIALIZABLE:I = 0x5

.field public static final TYPE_STRING:I = 0x1


# instance fields
.field private final changeHandler:Lcom/vlingo/core/internal/settings/util/SettingChangeHandler;

.field public defaultValue:Ljava/lang/Object;

.field public description:Ljava/lang/String;

.field protected isDirty:Z

.field public key:Ljava/lang/String;

.field protected mirrorInResourceManager:Z

.field public type:I

.field public value:Ljava/lang/Object;


# direct methods
.method protected constructor <init>(Ljava/lang/String;ILjava/lang/Object;Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "defaultValue"    # Ljava/lang/Object;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->type:I

    .line 20
    iput-object v1, p0, Lcom/vlingo/core/internal/settings/util/Setting;->key:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/vlingo/core/internal/settings/util/Setting;->description:Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lcom/vlingo/core/internal/settings/util/Setting;->value:Ljava/lang/Object;

    .line 23
    iput-object v1, p0, Lcom/vlingo/core/internal/settings/util/Setting;->defaultValue:Ljava/lang/Object;

    .line 24
    iput-boolean v2, p0, Lcom/vlingo/core/internal/settings/util/Setting;->isDirty:Z

    .line 30
    iput-boolean v2, p0, Lcom/vlingo/core/internal/settings/util/Setting;->mirrorInResourceManager:Z

    .line 34
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->key:Ljava/lang/String;

    .line 35
    iput p2, p0, Lcom/vlingo/core/internal/settings/util/Setting;->type:I

    .line 36
    iput-object p3, p0, Lcom/vlingo/core/internal/settings/util/Setting;->value:Ljava/lang/Object;

    .line 37
    iput-object p3, p0, Lcom/vlingo/core/internal/settings/util/Setting;->defaultValue:Ljava/lang/Object;

    .line 38
    iput-object p4, p0, Lcom/vlingo/core/internal/settings/util/Setting;->description:Ljava/lang/String;

    .line 39
    invoke-static {}, Lcom/vlingo/core/internal/settings/util/SettingChangeHandler;->getInstance()Lcom/vlingo/core/internal/settings/util/SettingChangeHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->changeHandler:Lcom/vlingo/core/internal/settings/util/SettingChangeHandler;

    .line 40
    return-void
.end method


# virtual methods
.method public declared-synchronized clearDirty()V
    .locals 1

    .prologue
    .line 87
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->isDirty:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    monitor-exit p0

    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->type:I

    return v0
.end method

.method public declared-synchronized isDirty()Z
    .locals 1

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->isDirty:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isSetToDefault()Z
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->defaultValue:Ljava/lang/Object;

    iget-object v1, p0, Lcom/vlingo/core/internal/settings/util/Setting;->value:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized migrate(Lcom/vlingo/core/internal/settings/util/Setting;)V
    .locals 2
    .param p1, "newSetting"    # Lcom/vlingo/core/internal/settings/util/Setting;

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/vlingo/core/internal/settings/util/Setting;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p1, Lcom/vlingo/core/internal/settings/util/Setting;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->description:Ljava/lang/String;

    .line 93
    invoke-virtual {p0}, Lcom/vlingo/core/internal/settings/util/Setting;->setDirty()V

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->defaultValue:Ljava/lang/Object;

    iget-object v1, p1, Lcom/vlingo/core/internal/settings/util/Setting;->defaultValue:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 96
    iget-object v0, p1, Lcom/vlingo/core/internal/settings/util/Setting;->defaultValue:Ljava/lang/Object;

    iput-object v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->defaultValue:Ljava/lang/Object;

    .line 97
    invoke-virtual {p0}, Lcom/vlingo/core/internal/settings/util/Setting;->setDirty()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :cond_1
    monitor-exit p0

    return-void

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onSavedValue(Ljava/lang/Object;)V
    .locals 1
    .param p1, "valueParam"    # Ljava/lang/Object;

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/settings/util/Setting;->value:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    monitor-exit p0

    return-void

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reset()V
    .locals 1

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->defaultValue:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/settings/util/Setting;->setValueInternal(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    monitor-exit p0

    return-void

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setDefault()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->defaultValue:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/settings/util/Setting;->setValueInternal(Ljava/lang/Object;)V

    .line 107
    return-void
.end method

.method protected setDefaultValue(Ljava/lang/Object;)V
    .locals 0
    .param p1, "defaultValue"    # Ljava/lang/Object;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/core/internal/settings/util/Setting;->value:Ljava/lang/Object;

    .line 44
    iput-object p1, p0, Lcom/vlingo/core/internal/settings/util/Setting;->defaultValue:Ljava/lang/Object;

    .line 45
    return-void
.end method

.method public declared-synchronized setDirty()V
    .locals 1

    .prologue
    .line 83
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->isDirty:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    monitor-exit p0

    return-void

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized setValueInternal(Ljava/lang/Object;)V
    .locals 2
    .param p1, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 48
    monitor-enter p0

    if-nez p1, :cond_0

    .line 49
    :try_start_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "cannot assign null value to a Setting"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 51
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/Setting;->value:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    iput-object p1, p0, Lcom/vlingo/core/internal/settings/util/Setting;->value:Ljava/lang/Object;

    .line 53
    invoke-virtual {p0}, Lcom/vlingo/core/internal/settings/util/Setting;->setDirty()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    :cond_1
    monitor-exit p0

    return-void
.end method
