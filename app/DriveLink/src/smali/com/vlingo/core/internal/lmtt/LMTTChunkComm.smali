.class public Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;
.super Ljava/lang/Object;
.source "LMTTChunkComm.java"

# interfaces
.implements Lcom/vlingo/sdk/training/VLTrainerListener;


# static fields
.field private static final DEFAULT_CHUNK_SIZE:I = 0x9c4

.field private static final MS_PER_MINUTE:J = 0xea60L

.field private static final RETRY_CHECK_DELAY:J = 0xea60L


# instance fields
.field private mActiveChunk:Lcom/vlingo/core/internal/lmtt/LMTTChunk;

.field private mChunkQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTChunk;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/vlingo/sdk/training/VLTrainerListener;

.field private mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

.field private mServerCounts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWorkerHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;Ljava/util/ArrayList;ZLcom/vlingo/sdk/training/VLTrainerListener;Landroid/os/Handler;)V
    .locals 2
    .param p1, "lmttUpdateType"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .param p3, "doWhole"    # Z
    .param p4, "listener"    # Lcom/vlingo/sdk/training/VLTrainerListener;
    .param p5, "workerHandler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem;",
            ">;Z",
            "Lcom/vlingo/sdk/training/VLTrainerListener;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    .local p2, "itemsToSend":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/lmtt/LMTTItem;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    if-nez p1, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "lmttUpdateType cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    if-nez p2, :cond_1

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "itemsToSend cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_1
    if-nez p4, :cond_2

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_2
    if-nez p5, :cond_3

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "workerHandler cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_3
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    .line 77
    iput-object p4, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mListener:Lcom/vlingo/sdk/training/VLTrainerListener;

    .line 78
    invoke-direct {p0, p2, p3}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->createChunkQueue(Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mChunkQueue:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mServerCounts:Ljava/util/HashMap;

    .line 82
    iput-object p5, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mWorkerHandler:Landroid/os/Handler;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->handleNextChunk()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->isLmttEnabledForType()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Lcom/vlingo/sdk/training/VLTrainerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mListener:Lcom/vlingo/sdk/training/VLTrainerListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mActiveChunk:Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;Lcom/vlingo/core/internal/lmtt/LMTTChunk;)J
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;
    .param p1, "x1"    # Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->getTransmitDelay(Lcom/vlingo/core/internal/lmtt/LMTTChunk;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$500()V
    .locals 0

    .prologue
    .line 25
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->updateTransmitTime()V

    return-void
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mWorkerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mServerCounts:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mChunkQueue:Ljava/util/ArrayList;

    return-object v0
.end method

.method private createChunkQueue(Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 12
    .param p2, "doWhole"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem;",
            ">;Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTChunk;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "itemsToSend":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/lmtt/LMTTItem;>;"
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 161
    const-string/jumbo v10, "lmtt.chunk_size"

    const/16 v11, 0x9c4

    invoke-static {v10, v11}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 162
    .local v2, "chunkSize":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 167
    .local v6, "itemCount":I
    new-instance v1, Ljava/util/ArrayList;

    div-int v10, v6, v2

    add-int/lit8 v10, v10, 0x1

    invoke-direct {v1, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 169
    .local v1, "chunkQueue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/lmtt/LMTTChunk;>;"
    iget-object v10, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v11, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_LANGUAGE_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v10, v11, :cond_1

    .line 170
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    invoke-direct {v0, v9, v8}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;-><init>(ZZ)V

    .line 171
    .local v0, "chunk":Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    .end local v0    # "chunk":Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    :cond_0
    return-object v1

    .line 173
    :cond_1
    const/4 v7, 0x0

    .local v7, "start":I
    :goto_0
    if-ge v7, v6, :cond_0

    .line 174
    if-nez v7, :cond_3

    move v4, v8

    .line 175
    .local v4, "isFirst":Z
    :goto_1
    if-eqz p2, :cond_4

    if-eqz v4, :cond_4

    move v5, v8

    .line 176
    .local v5, "isWhole":Z
    :goto_2
    add-int v3, v7, v2

    .line 177
    .local v3, "end":I
    if-le v3, v6, :cond_2

    .line 178
    move v3, v6

    .line 180
    :cond_2
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    invoke-virtual {p1, v7, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v10

    invoke-direct {v0, v10, v5, v4}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;-><init>(Ljava/util/List;ZZ)V

    .line 184
    .restart local v0    # "chunk":Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    add-int/2addr v7, v2

    goto :goto_0

    .end local v0    # "chunk":Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    .end local v3    # "end":I
    .end local v4    # "isFirst":Z
    .end local v5    # "isWhole":Z
    :cond_3
    move v4, v9

    .line 174
    goto :goto_1

    .restart local v4    # "isFirst":Z
    :cond_4
    move v5, v9

    .line 175
    goto :goto_2
.end method

.method private static getLastTransmitTime()J
    .locals 7

    .prologue
    .line 225
    const-string/jumbo v4, "lmtt.last_transmit_timestamp"

    const-wide/16 v5, 0x0

    invoke-static {v4, v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 229
    .local v2, "lastTransmitTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 230
    .local v0, "currentTime":J
    cmp-long v4, v2, v0

    if-lez v4, :cond_0

    .line 231
    move-wide v2, v0

    .line 233
    :cond_0
    return-wide v2
.end method

.method private getTransmitDelay(Lcom/vlingo/core/internal/lmtt/LMTTChunk;)J
    .locals 14
    .param p1, "chunk"    # Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    .prologue
    .line 192
    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->getDelay()J

    move-result-wide v0

    .line 193
    .local v0, "chunkDelay":J
    const-wide/16 v2, 0x0

    .line 196
    .local v2, "clientShieldDelay":J
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->isInitialUploadDone()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 197
    const-string/jumbo v12, "lmtt.client_shield_duration_mins"

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v12

    int-to-long v2, v12

    .line 199
    const-wide/32 v12, 0xea60

    mul-long/2addr v2, v12

    .line 203
    :cond_0
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 205
    .local v4, "delay":J
    const-wide/16 v12, 0x0

    cmp-long v12, v4, v12

    if-gez v12, :cond_1

    .line 206
    const-wide/16 v4, 0x0

    .line 209
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->getLastTransmitTime()J

    move-result-wide v6

    .line 210
    .local v6, "lastChunkTransmitTime":J
    add-long v8, v6, v4

    .line 213
    .local v8, "nextTransmitTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v10, v8, v12

    .line 214
    .local v10, "relativeDelay":J
    const-wide/16 v12, 0x0

    cmp-long v12, v10, v12

    if-gez v12, :cond_2

    .line 215
    const-wide/16 v10, 0x0

    .line 221
    :cond_2
    return-wide v10
.end method

.method private handleNextChunk()V
    .locals 3

    .prologue
    .line 106
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mChunkQueue:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mChunkQueue:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mActiveChunk:Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    .line 114
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$2;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)V

    .line 143
    .local v0, "runnable":Ljava/lang/Runnable;
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 152
    .end local v0    # "runnable":Ljava/lang/Runnable;
    :goto_0
    return-void

    .line 148
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->setInitialUploadDone()V

    .line 150
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mListener:Lcom/vlingo/sdk/training/VLTrainerListener;

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mServerCounts:Ljava/util/HashMap;

    invoke-interface {v1, v2}, Lcom/vlingo/sdk/training/VLTrainerListener;->onUpdateReceived(Ljava/util/HashMap;)V

    goto :goto_0
.end method

.method private isInitialUploadDone()Z
    .locals 3

    .prologue
    .line 241
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "lmtt.initial_upload_done."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->getTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 242
    .local v0, "initialUploadSettingKey":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private isLmttEnabledForType()Z
    .locals 3

    .prologue
    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "lmtt.enable."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->getTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "settingKey":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private setInitialUploadDone()V
    .locals 3

    .prologue
    .line 246
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "lmtt.initial_upload_done."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->getTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "initialUploadSettingKey":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 248
    return-void
.end method

.method private static updateTransmitTime()V
    .locals 3

    .prologue
    .line 237
    const-string/jumbo v0, "lmtt.last_transmit_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 238
    return-void
.end method


# virtual methods
.method public onError(Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Lcom/vlingo/sdk/training/VLTrainerErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 292
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mWorkerHandler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 314
    return-void
.end method

.method public onUpdateReceived(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, "serverCounts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mWorkerHandler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$3;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$3;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;Ljava/util/HashMap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 285
    return-void
.end method

.method transmitChunks()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mWorkerHandler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$1;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$1;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 97
    return-void
.end method
