.class public Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;
.super Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;
.source "BaseAcceptedText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText$1;
    }
.end annotation


# instance fields
.field private acceptedType:Ljava/lang/String;

.field private tag:Ljava/lang/String;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V
    .locals 2
    .param p1, "dialogGuid"    # Ljava/lang/String;
    .param p2, "dialogTurn"    # I
    .param p3, "gUttId"    # Ljava/lang/String;
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "type"    # Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 62
    iput-object p4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->text:Ljava/lang/String;

    .line 63
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText$1;->$SwitchMap$com$vlingo$core$internal$recognition$acceptedtext$AcceptedText$TextType:[I

    invoke-virtual {p5}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 74
    :goto_0
    return-void

    .line 65
    :pswitch_0
    const-string/jumbo v0, "memo:def"

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->acceptedType:Ljava/lang/String;

    .line 66
    const-string/jumbo v0, "text"

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->tag:Ljava/lang/String;

    goto :goto_0

    .line 69
    :pswitch_1
    const-string/jumbo v0, "dial:def"

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->acceptedType:Ljava/lang/String;

    .line 70
    const-string/jumbo v0, "contact"

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->tag:Ljava/lang/String;

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "dialogGuid"    # Ljava/lang/String;
    .param p2, "dialogTurn"    # I
    .param p3, "gUttId"    # Ljava/lang/String;
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "acceptedType"    # Ljava/lang/String;
    .param p6, "tag"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 50
    iput-object p4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->text:Ljava/lang/String;

    .line 51
    iput-object p5, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->acceptedType:Ljava/lang/String;

    .line 52
    iput-object p6, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->tag:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V
    .locals 6
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    .prologue
    const/4 v1, 0x0

    .line 82
    const/4 v2, -0x1

    move-object v0, p0

    move-object v3, v1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "acceptedType"    # Ljava/lang/String;
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "gUttId"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "acceptedType"    # Ljava/lang/String;
    .param p4, "tag"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;-><init>(Ljava/lang/String;)V

    .line 34
    iput-object p2, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->text:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->acceptedType:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->tag:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method protected getAcceptedTextXML()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 88
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "<AcceptedText pt=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 89
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->acceptedType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 90
    const-string/jumbo v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 91
    const-string/jumbo v1, "<Tag u=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 92
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->tag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 93
    const-string/jumbo v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 94
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 95
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 96
    const-string/jumbo v1, "</AcceptedText>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "AcceptedText [ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " , text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", acceptedType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->acceptedType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;->tag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
