.class public Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
.super Ljava/lang/Object;
.source "ContactExitCriteria.java"


# instance fields
.field defaultReturnValue:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "defaultReturnValue"    # Z

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-boolean p1, p0, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->defaultReturnValue:Z

    .line 9
    return-void
.end method


# virtual methods
.method public keepMatch(I)Z
    .locals 1
    .param p1, "score"    # I

    .prologue
    .line 15
    const/4 v0, 0x1

    return v0
.end method

.method public returnMatches()Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->defaultReturnValue:Z

    return v0
.end method

.method public tallyScore(I)Z
    .locals 1
    .param p1, "score"    # I

    .prologue
    .line 13
    const/4 v0, 0x1

    return v0
.end method
