.class public Lcom/vlingo/core/internal/util/Alarm;
.super Ljava/lang/Object;
.source "Alarm.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/Alarm$1;,
        Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;,
        Lcom/vlingo/core/internal/util/Alarm$MatchType;,
        Lcom/vlingo/core/internal/util/Alarm$WeekendType;,
        Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;
    }
.end annotation


# static fields
.field private static final DELETE_FRIDAY_MASK:I = 0x11111101

.field private static final DELETE_MONDAY_MASK:I = 0x11011111

.field private static final DELETE_SATURDAY_MASK:I = 0x11111110

.field private static final DELETE_SUNDAY_MASK:I = 0x10111111

.field private static final DELETE_THURSDAY_MASK:I = 0x11111011

.field private static final DELETE_TUESDAY_MASK:I = 0x11101111

.field private static final DELETE_WEDNESDAY_MASK:I = 0x11101111

.field public static final FRIDAY_MASK:I = 0x10

.field private static final LOG:Lcom/vlingo/core/internal/logging/Logger;

.field public static final MONDAY_MASK:I = 0x100000

.field public static final SATURDAY_MASK:I = 0x1

.field public static final SUNDAY_MASK:I = 0x1000000

.field public static final THURSDAY_MASK:I = 0x100

.field public static final TUESDAY_MASK:I = 0x10000

.field public static final WEDNESDAY_MASK:I = 0x1000

.field public static final WEEKDAY_MASK_FRI_SAT:I = 0x1111100

.field public static final WEEKDAY_MASK_SAT_SUN:I = 0x111110

.field public static final WEEKEND_MASK_FRI_SAT:I = 0x11

.field public static final WEEKEND_MASK_SAT_SUN:I = 0x1000001


# instance fields
.field private active:Z

.field private alarmState:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

.field private dayMask:I

.field private id:I

.field private name:Ljava/lang/String;

.field private repeating:Z

.field private time:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/vlingo/core/internal/util/Alarm;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm;->LOG:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput v2, p0, Lcom/vlingo/core/internal/util/Alarm;->id:I

    .line 28
    iput-boolean v2, p0, Lcom/vlingo/core/internal/util/Alarm;->active:Z

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/Alarm;->name:Ljava/lang/String;

    .line 30
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/util/Alarm;->time:J

    .line 31
    iput v2, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    .line 32
    iput-boolean v2, p0, Lcom/vlingo/core/internal/util/Alarm;->repeating:Z

    .line 33
    sget-object v0, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/Alarm;->alarmState:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    .line 81
    return-void
.end method

.method private addDayToAlarm(I)V
    .locals 2
    .param p1, "weekDay"    # I

    .prologue
    .line 449
    packed-switch p1, :pswitch_data_0

    .line 472
    :goto_0
    return-void

    .line 451
    :pswitch_0
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const/high16 v1, 0x1000000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 454
    :pswitch_1
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const/high16 v1, 0x100000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 457
    :pswitch_2
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 460
    :pswitch_3
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 463
    :pswitch_4
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 466
    :pswitch_5
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 469
    :pswitch_6
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 449
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static addDayToCanonical(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "days"    # Ljava/lang/String;
    .param p1, "day"    # Ljava/lang/String;

    .prologue
    .line 340
    if-eqz p0, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 345
    :goto_0
    return-object p0

    .line 343
    :cond_0
    move-object p0, p1

    goto :goto_0
.end method

.method public static clone(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 125
    .local v0, "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/util/Alarm;

    .line 126
    .local v4, "t":Lcom/vlingo/core/internal/util/Alarm;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/Alarm;->clone()Lcom/vlingo/core/internal/util/Alarm;

    move-result-object v1

    .line 127
    .local v1, "copy":Lcom/vlingo/core/internal/util/Alarm;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 130
    .end local v0    # "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    .end local v1    # "copy":Lcom/vlingo/core/internal/util/Alarm;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "t":Lcom/vlingo/core/internal/util/Alarm;
    :catch_0
    move-exception v2

    .line 131
    .local v2, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "List cloning unsupported"

    invoke-direct {v5, v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 129
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v0
.end method

.method private deleteDayFromAlarm(I)V
    .locals 2
    .param p1, "weekDay"    # I

    .prologue
    const v1, 0x11101111

    .line 491
    packed-switch p1, :pswitch_data_0

    .line 514
    :goto_0
    return-void

    .line 493
    :pswitch_0
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const v1, 0x10111111

    and-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 496
    :pswitch_1
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const v1, 0x11011111

    and-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 499
    :pswitch_2
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    and-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 502
    :pswitch_3
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    and-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 505
    :pswitch_4
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const v1, 0x11111011

    and-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 508
    :pswitch_5
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const v1, 0x11111101

    and-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 511
    :pswitch_6
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const v1, 0x11111110

    and-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    goto :goto_0

    .line 491
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static getDaysCanonical(I)Ljava/lang/String;
    .locals 2
    .param p0, "dayMask"    # I

    .prologue
    .line 328
    const-string/jumbo v0, ""

    .line 329
    .local v0, "days":Ljava/lang/String;
    const/high16 v1, 0x1000000

    and-int/2addr v1, p0

    if-lez v1, :cond_0

    const-string/jumbo v1, "sun"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->addDayToCanonical(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 330
    :cond_0
    const/high16 v1, 0x100000

    and-int/2addr v1, p0

    if-lez v1, :cond_1

    const-string/jumbo v1, "mon"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->addDayToCanonical(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 331
    :cond_1
    const/high16 v1, 0x10000

    and-int/2addr v1, p0

    if-lez v1, :cond_2

    const-string/jumbo v1, "tue"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->addDayToCanonical(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 332
    :cond_2
    and-int/lit16 v1, p0, 0x1000

    if-lez v1, :cond_3

    const-string/jumbo v1, "wed"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->addDayToCanonical(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 333
    :cond_3
    and-int/lit16 v1, p0, 0x100

    if-lez v1, :cond_4

    const-string/jumbo v1, "thu"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->addDayToCanonical(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 334
    :cond_4
    and-int/lit8 v1, p0, 0x10

    if-lez v1, :cond_5

    const-string/jumbo v1, "fri"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->addDayToCanonical(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 335
    :cond_5
    and-int/lit8 v1, p0, 0x1

    if-lez v1, :cond_6

    const-string/jumbo v1, "sat"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->addDayToCanonical(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 336
    :cond_6
    return-object v0
.end method

.method private isHourMinuteLaterThanNow()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 361
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 362
    .local v0, "t":Landroid/text/format/Time;
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 363
    iget v2, v0, Landroid/text/format/Time;->hour:I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/Alarm;->getHour()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 386
    :cond_0
    :goto_0
    return v1

    .line 372
    :cond_1
    iget v2, v0, Landroid/text/format/Time;->hour:I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/Alarm;->getHour()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 373
    iget v2, v0, Landroid/text/format/Time;->minute:I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/Alarm;->getMinute()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 386
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addDayFromDaysCanonicalForm(Ljava/lang/String;)V
    .locals 6
    .param p1, "days"    # Ljava/lang/String;

    .prologue
    .line 414
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 415
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 416
    .local v3, "t":Landroid/text/format/Time;
    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    .line 418
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/Alarm;->isHourMinuteLaterThanNow()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 419
    iget v4, v3, Landroid/text/format/Time;->weekDay:I

    .line 423
    .local v4, "weekDay":I
    :goto_0
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/util/Alarm;->addDayToAlarm(I)V

    .line 437
    .end local v3    # "t":Landroid/text/format/Time;
    .end local v4    # "weekDay":I
    :cond_0
    return-void

    .line 421
    .restart local v3    # "t":Landroid/text/format/Time;
    :cond_1
    iget v5, v3, Landroid/text/format/Time;->weekDay:I

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v4, v5, 0x7

    .restart local v4    # "weekDay":I
    goto :goto_0

    .line 425
    .end local v3    # "t":Landroid/text/format/Time;
    .end local v4    # "weekDay":I
    :cond_2
    new-instance v1, Ljava/util/StringTokenizer;

    const-string/jumbo v5, " "

    invoke-direct {v1, p1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    .local v1, "dayTokens":Ljava/util/StringTokenizer;
    :cond_3
    :goto_1
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 427
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 428
    .local v2, "stringDayOfWeek":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->convertDayOfWeekToInt(Ljava/lang/String;)I

    move-result v0

    .line 432
    .local v0, "dayOfWeekInt":I
    const/4 v5, -0x1

    if-eq v0, v5, :cond_3

    .line 433
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/util/Alarm;->addDayToAlarm(I)V

    goto :goto_1
.end method

.method public clone()Lcom/vlingo/core/internal/util/Alarm;
    .locals 2

    .prologue
    .line 115
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/Alarm;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    return-object v1

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    .line 119
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/Alarm;->clone()Lcom/vlingo/core/internal/util/Alarm;

    move-result-object v0

    return-object v0
.end method

.method public deleteDayFromDaysCanonicalForm(Ljava/lang/String;)V
    .locals 4
    .param p1, "days"    # Ljava/lang/String;

    .prologue
    .line 475
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 476
    new-instance v1, Ljava/util/StringTokenizer;

    const-string/jumbo v3, " "

    invoke-direct {v1, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    .local v1, "dayTokens":Ljava/util/StringTokenizer;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 478
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 479
    .local v2, "stringDayOfWeek":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->convertDayOfWeekToInt(Ljava/lang/String;)I

    move-result v0

    .line 483
    .local v0, "dayOfWeekInt":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 484
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/util/Alarm;->deleteDayFromAlarm(I)V

    goto :goto_0

    .line 488
    .end local v0    # "dayOfWeekInt":I
    .end local v1    # "dayTokens":Ljava/util/StringTokenizer;
    .end local v2    # "stringDayOfWeek":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public getAlarmStatus()Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/vlingo/core/internal/util/Alarm;->alarmState:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    return-object v0
.end method

.method public getDayMask()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    return v0
.end method

.method public getHour()I
    .locals 2

    .prologue
    .line 349
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/Alarm;->time:J

    long-to-int v0, v0

    div-int/lit8 v0, v0, 0x64

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/vlingo/core/internal/util/Alarm;->id:I

    return v0
.end method

.method public getMinute()I
    .locals 2

    .prologue
    .line 353
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/Alarm;->time:J

    long-to-int v0, v0

    rem-int/lit8 v0, v0, 0x64

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/vlingo/core/internal/util/Alarm;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 290
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/Alarm;->time:J

    return-wide v0
.end method

.method public getTimeCanonical()Ljava/lang/String;
    .locals 5

    .prologue
    .line 323
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v1, "%02d:%02d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/Alarm;->getHour()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/Alarm;->getMinute()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isDay(Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;Lcom/vlingo/core/internal/util/Alarm$MatchType;)Z
    .locals 7
    .param p1, "dayOfWeek"    # Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;
    .param p2, "matchType"    # Lcom/vlingo/core/internal/util/Alarm$MatchType;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 191
    const/4 v2, 0x0

    .line 192
    .local v2, "selectedMask":I
    sget-object v5, Lcom/vlingo/core/internal/util/Alarm$1;->$SwitchMap$com$vlingo$core$internal$util$Alarm$DaysOfWeek:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 212
    const/4 v2, 0x1

    .line 215
    :goto_0
    iget v5, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    and-int/2addr v5, v2

    if-eqz v5, :cond_0

    move v0, v3

    .line 216
    .local v0, "includesExactDay":Z
    :goto_1
    sget-object v5, Lcom/vlingo/core/internal/util/Alarm$MatchType;->EXACT:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    if-ne p2, v5, :cond_3

    .line 217
    iget v5, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    xor-int/2addr v5, v2

    if-eqz v5, :cond_1

    move v1, v3

    .line 218
    .local v1, "includesOtherDays":Z
    :goto_2
    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    .line 220
    .end local v1    # "includesOtherDays":Z
    :goto_3
    return v3

    .line 194
    .end local v0    # "includesExactDay":Z
    :pswitch_0
    const/high16 v2, 0x1000000

    .line 195
    goto :goto_0

    .line 197
    :pswitch_1
    const/high16 v2, 0x100000

    .line 198
    goto :goto_0

    .line 200
    :pswitch_2
    const/high16 v2, 0x10000

    .line 201
    goto :goto_0

    .line 203
    :pswitch_3
    const/16 v2, 0x1000

    .line 204
    goto :goto_0

    .line 206
    :pswitch_4
    const/16 v2, 0x100

    .line 207
    goto :goto_0

    .line 209
    :pswitch_5
    const/16 v2, 0x10

    .line 210
    goto :goto_0

    :cond_0
    move v0, v4

    .line 215
    goto :goto_1

    .restart local v0    # "includesExactDay":Z
    :cond_1
    move v1, v4

    .line 217
    goto :goto_2

    .restart local v1    # "includesOtherDays":Z
    :cond_2
    move v3, v4

    .line 218
    goto :goto_3

    .end local v1    # "includesOtherDays":Z
    :cond_3
    move v3, v0

    .line 220
    goto :goto_3

    .line 192
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public isWeekday(Lcom/vlingo/core/internal/util/Alarm$WeekendType;Lcom/vlingo/core/internal/util/Alarm$MatchType;)Z
    .locals 1
    .param p1, "weekendType"    # Lcom/vlingo/core/internal/util/Alarm$WeekendType;
    .param p2, "matchType"    # Lcom/vlingo/core/internal/util/Alarm$MatchType;

    .prologue
    .line 263
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/util/Alarm;->isWeekend(Lcom/vlingo/core/internal/util/Alarm$WeekendType;Lcom/vlingo/core/internal/util/Alarm$MatchType;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWeekend(Lcom/vlingo/core/internal/util/Alarm$WeekendType;Lcom/vlingo/core/internal/util/Alarm$MatchType;)Z
    .locals 6
    .param p1, "weekendType"    # Lcom/vlingo/core/internal/util/Alarm$WeekendType;
    .param p2, "matchType"    # Lcom/vlingo/core/internal/util/Alarm$MatchType;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 228
    sget-object v4, Lcom/vlingo/core/internal/util/Alarm$MatchType;->EXACT:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    if-ne p2, v4, :cond_6

    .line 233
    const/4 v1, 0x0

    .line 234
    .local v1, "hasTwoWeekendDays":Z
    const/4 v0, 0x1

    .line 235
    .local v0, "hasNoWeekdayDays":Z
    sget-object v4, Lcom/vlingo/core/internal/util/Alarm$1;->$SwitchMap$com$vlingo$core$internal$util$Alarm$WeekendType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm$WeekendType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 242
    iget v4, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const/high16 v5, 0x1000000

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    move v1, v2

    .line 243
    :goto_0
    iget v4, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const v5, 0x111110

    and-int/2addr v4, v5

    if-nez v4, :cond_4

    move v0, v2

    .line 245
    :goto_1
    if-eqz v1, :cond_5

    if-eqz v0, :cond_5

    .line 253
    .end local v0    # "hasNoWeekdayDays":Z
    .end local v1    # "hasTwoWeekendDays":Z
    :cond_0
    :goto_2
    return v2

    .line 237
    .restart local v0    # "hasNoWeekdayDays":Z
    .restart local v1    # "hasTwoWeekendDays":Z
    :pswitch_0
    iget v4, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    move v1, v2

    .line 238
    :goto_3
    iget v4, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const v5, 0x1111100

    and-int/2addr v4, v5

    if-nez v4, :cond_2

    move v0, v2

    .line 239
    :goto_4
    goto :goto_1

    :cond_1
    move v1, v3

    .line 237
    goto :goto_3

    :cond_2
    move v0, v3

    .line 238
    goto :goto_4

    :cond_3
    move v1, v3

    .line 242
    goto :goto_0

    :cond_4
    move v0, v3

    .line 243
    goto :goto_1

    :cond_5
    move v2, v3

    .line 245
    goto :goto_2

    .line 248
    .end local v0    # "hasNoWeekdayDays":Z
    .end local v1    # "hasTwoWeekendDays":Z
    :cond_6
    sget-object v4, Lcom/vlingo/core/internal/util/Alarm$1;->$SwitchMap$com$vlingo$core$internal$util$Alarm$WeekendType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm$WeekendType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 253
    iget v4, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    const v5, 0x1000001

    and-int/2addr v4, v5

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_2

    .line 250
    :pswitch_1
    iget v4, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    and-int/lit8 v4, v4, 0x11

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_2

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 248
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public isWeeklyRepeating()Z
    .locals 1

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/Alarm;->repeating:Z

    return v0
.end method

.method public setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V
    .locals 0
    .param p1, "alarmState"    # Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/vlingo/core/internal/util/Alarm;->alarmState:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    .line 172
    return-void
.end method

.method public setDayFromDaysCanonicalForm(Ljava/lang/String;)V
    .locals 1
    .param p1, "days"    # Ljava/lang/String;

    .prologue
    .line 396
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    .line 397
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/util/Alarm;->addDayFromDaysCanonicalForm(Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method public setDayMask(I)V
    .locals 0
    .param p1, "dayMask"    # I

    .prologue
    .line 155
    iput p1, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    .line 156
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 167
    iput p1, p0, Lcom/vlingo/core/internal/util/Alarm;->id:I

    .line 168
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/vlingo/core/internal/util/Alarm;->name:Ljava/lang/String;

    .line 276
    return-void
.end method

.method public setTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 294
    iput-wide p1, p0, Lcom/vlingo/core/internal/util/Alarm;->time:J

    .line 295
    return-void
.end method

.method public setTimeFromCanonical(Ljava/lang/String;)V
    .locals 16
    .param p1, "canonical"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 304
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    .line 305
    .local v7, "t":Landroid/text/format/Time;
    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    .line 306
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v7, v12}, Lcom/vlingo/core/internal/schedule/DateUtil;->getTimeFromTimeString(Ljava/lang/String;Landroid/text/format/Time;Z)J

    move-result-wide v8

    .line 308
    .local v8, "timeMillis":J
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v10

    .line 309
    .local v10, "tz":Ljava/util/TimeZone;
    invoke-virtual {v10, v8, v9}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v11

    .line 310
    .local v11, "tzOffset":I
    int-to-long v12, v11

    add-long/2addr v12, v8

    const-wide/16 v14, 0x3e8

    div-long v3, v12, v14

    .line 312
    .local v3, "lTime":J
    const-wide/16 v12, 0xe10

    rem-long v12, v3, v12

    const-wide/16 v14, 0x3c

    div-long v5, v12, v14

    .line 313
    .local v5, "minute":J
    const-wide/16 v12, 0xe10

    div-long v12, v3, v12

    const-wide/16 v14, 0x18

    rem-long v1, v12, v14

    .line 315
    .local v1, "hour":J
    const-wide/16 v12, 0x64

    mul-long/2addr v12, v1

    add-long/2addr v12, v5

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/vlingo/core/internal/util/Alarm;->time:J

    .line 316
    return-void
.end method

.method public setWeeklyRepeating(Z)V
    .locals 0
    .param p1, "isRepeating"    # Z

    .prologue
    .line 286
    iput-boolean p1, p0, Lcom/vlingo/core/internal/util/Alarm;->repeating:Z

    .line 287
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Alarm{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/util/Alarm;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "active="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/vlingo/core/internal/util/Alarm;->active:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/util/Alarm;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/core/internal/util/Alarm;->time:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "dayMask="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/util/Alarm;->dayMask:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "isRepeating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/vlingo/core/internal/util/Alarm;->repeating:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
