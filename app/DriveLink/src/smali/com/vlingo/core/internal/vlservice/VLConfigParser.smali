.class public Lcom/vlingo/core/internal/vlservice/VLConfigParser;
.super Lcom/vlingo/core/internal/xml/SimpleXmlParser;
.source "VLConfigParser.java"


# static fields
.field public static final TYPE_BOOLEAN:Ljava/lang/String; = "boolean"

.field public static final TYPE_FLOAT:Ljava/lang/String; = "float"

.field public static final TYPE_INTEGER:Ljava/lang/String; = "integer"

.field public static final TYPE_LONG:Ljava/lang/String; = "long"

.field public static final TYPE_STRING:Ljava/lang/String; = "string"


# instance fields
.field final ATTR_MIN_APP_VERSION:I

.field final ATTR_N:I

.field final ATTR_T:I

.field final ATTR_V:I

.field final ELEM_SETTING:I

.field final ELEM_VLCONFIG:I

.field minimumAppVersion:Ljava/lang/String;

.field settings:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/SimpleXmlParser;-><init>()V

    .line 41
    const-string/jumbo v0, "Setting"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ELEM_SETTING:I

    .line 42
    const-string/jumbo v0, "VLConfig"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ELEM_VLCONFIG:I

    .line 44
    const-string/jumbo v0, "n"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ATTR_N:I

    .line 45
    const-string/jumbo v0, "v"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ATTR_V:I

    .line 46
    const-string/jumbo v0, "t"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ATTR_T:I

    .line 47
    const-string/jumbo v0, "MinAppVersion"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ATTR_MIN_APP_VERSION:I

    .line 48
    return-void
.end method

.method private addSetting(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;

    .prologue
    .line 74
    if-eqz p1, :cond_3

    .line 75
    if-nez p2, :cond_0

    .line 76
    const-string/jumbo p2, ""

    .line 78
    :cond_0
    if-nez p3, :cond_2

    .line 79
    const-string/jumbo v0, "false"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "true"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 80
    :cond_1
    const-string/jumbo p3, "boolean"

    .line 86
    :cond_2
    :goto_0
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p3

    .line 87
    iget-object v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->settings:Ljava/util/Hashtable;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p2, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :cond_3
    return-void

    .line 83
    :cond_4
    const-string/jumbo p3, "string"

    goto :goto_0
.end method


# virtual methods
.method public beginElement(ILcom/vlingo/core/internal/xml/XmlAttributes;[CI)V
    .locals 4
    .param p1, "elementType"    # I
    .param p2, "attributes"    # Lcom/vlingo/core/internal/xml/XmlAttributes;
    .param p3, "cData"    # [C
    .param p4, "elementEndPosition"    # I

    .prologue
    .line 56
    iget v3, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ELEM_VLCONFIG:I

    if-ne v3, p1, :cond_1

    .line 57
    iget v3, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ATTR_MIN_APP_VERSION:I

    invoke-virtual {p2, v3}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->minimumAppVersion:Ljava/lang/String;

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iget v3, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ELEM_SETTING:I

    if-ne v3, p1, :cond_0

    .line 60
    iget v3, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ATTR_N:I

    invoke-virtual {p2, v3}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "name":Ljava/lang/String;
    iget v3, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ATTR_T:I

    invoke-virtual {p2, v3}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "type":Ljava/lang/String;
    iget v3, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->ATTR_V:I

    invoke-virtual {p2, v3}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "value":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 64
    new-instance v2, Ljava/lang/String;

    .end local v2    # "value":Ljava/lang/String;
    invoke-direct {v2, p3}, Ljava/lang/String;-><init>([C)V

    .line 66
    .restart local v2    # "value":Ljava/lang/String;
    :cond_2
    invoke-direct {p0, v0, v2, v1}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->addSetting(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public endElement(II)V
    .locals 0
    .param p1, "elementType"    # I
    .param p2, "elementStartPosition"    # I

    .prologue
    .line 71
    return-void
.end method

.method public getMinimumAppVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->minimumAppVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getSettings()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->settings:Ljava/util/Hashtable;

    return-object v0
.end method

.method public onParseBegin([C)V
    .locals 1
    .param p1, "xml"    # [C

    .prologue
    .line 51
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->settings:Ljava/util/Hashtable;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/vlservice/VLConfigParser;->minimumAppVersion:Ljava/lang/String;

    .line 53
    return-void
.end method
