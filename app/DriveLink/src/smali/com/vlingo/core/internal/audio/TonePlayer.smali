.class public Lcom/vlingo/core/internal/audio/TonePlayer;
.super Ljava/lang/Object;
.source "TonePlayer.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;
.implements Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;,
        Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;,
        Lcom/vlingo/core/internal/audio/TonePlayer$Listener;
    }
.end annotation


# static fields
.field private static final DELAY_TONE_TIME_MS:I = 0x64

.field private static final MAX_TONE_LENGTH_MS:I = 0x3e8

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private audioTrack:Landroid/media/AudioTrack;

.field private audioType:Lcom/vlingo/core/internal/audio/AudioType;

.field private volatile busy:Z

.field private isCalledMute:Z

.field private notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

.field private toneHandler:Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;

.field private toneResourceId:I

.field private totalSamples:I

.field private workerThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/vlingo/core/internal/audio/TonePlayer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/audio/TonePlayer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILcom/vlingo/core/internal/audio/AudioType;)V
    .locals 2
    .param p1, "toneResourceId"    # I
    .param p2, "audioType"    # Lcom/vlingo/core/internal/audio/AudioType;

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->isCalledMute:Z

    .line 138
    if-nez p2, :cond_0

    .line 139
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "audioType cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_0
    iput p1, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->toneResourceId:I

    .line 142
    iput-object p2, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioType:Lcom/vlingo/core/internal/audio/AudioType;

    .line 143
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/audio/TonePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/TonePlayer;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TonePlayer;->playInternal()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/audio/TonePlayer;)Landroid/media/AudioTrack;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/TonePlayer;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/audio/TonePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/TonePlayer;

    .prologue
    .line 25
    iget v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->totalSamples:I

    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/vlingo/core/internal/audio/TonePlayer;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/audio/TonePlayer;)Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/TonePlayer;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/audio/TonePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/TonePlayer;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TonePlayer;->cleanup()V

    return-void
.end method

.method private checkMuteVolume()V
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 262
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 263
    .local v1, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "audio"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 264
    .local v0, "am":Landroid/media/AudioManager;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 265
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v4

    if-nez v4, :cond_0

    .line 266
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v4

    const/4 v5, 0x6

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 267
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v4}, Landroid/media/AudioTrack;->getState()I

    move-result v4

    if-ne v4, v7, :cond_0

    .line 268
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStreamMaxVolume()I

    move-result v4

    int-to-float v3, v4

    .line 271
    .local v3, "volume":F
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v4, v9, v9}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    .line 275
    .end local v3    # "volume":F
    :cond_0
    const-string/jumbo v4, "set_audio_volume_for_assoc_svc"

    invoke-static {v4, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 277
    .local v2, "isAudioVolumeForAssocSvc":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v2, :cond_1

    .line 278
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v4}, Landroid/media/AudioTrack;->getState()I

    move-result v4

    if-ne v4, v7, :cond_1

    .line 281
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v4, v8, v8}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    .line 285
    .end local v2    # "isAudioVolumeForAssocSvc":Z
    :cond_1
    return-void
.end method

.method private cleanup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 240
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->release()V

    .line 242
    iput-object v2, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->workerThread:Landroid/os/HandlerThread;

    if-eqz v1, :cond_1

    .line 245
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->workerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 246
    iput-object v2, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->workerThread:Landroid/os/HandlerThread;

    .line 249
    :cond_1
    iput-object v2, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->toneHandler:Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;

    .line 250
    iput-object v2, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    .line 251
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->busy:Z

    .line 253
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 254
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 255
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->removeListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V

    .line 256
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 259
    .end local v0    # "context":Landroid/content/Context;
    :cond_2
    return-void
.end method

.method private playInternal()V
    .locals 19

    .prologue
    .line 165
    const/4 v14, 0x0

    .line 167
    .local v14, "inputStream":Ljava/io/InputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioType:Lcom/vlingo/core/internal/audio/AudioType;

    iget v3, v1, Lcom/vlingo/core/internal/audio/AudioType;->frequency:I

    .line 168
    .local v3, "sampleRate":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioType:Lcom/vlingo/core/internal/audio/AudioType;

    iget v4, v1, Lcom/vlingo/core/internal/audio/AudioType;->channel:I

    .line 169
    .local v4, "channelConfig":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioType:Lcom/vlingo/core/internal/audio/AudioType;

    iget v5, v1, Lcom/vlingo/core/internal/audio/AudioType;->encoding:I

    .line 170
    .local v5, "audioFormat":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioType:Lcom/vlingo/core/internal/audio/AudioType;

    iget v11, v1, Lcom/vlingo/core/internal/audio/AudioType;->bytesPerSample:I

    .line 171
    .local v11, "bytesPerSample":I
    mul-int/lit16 v1, v3, 0x3e8

    mul-int/2addr v1, v11

    div-int/lit16 v0, v1, 0x3e8

    move/from16 v18, v0

    .line 173
    .local v18, "readBufferSize":I
    move/from16 v0, v18

    new-array v10, v0, [B

    .line 178
    .local v10, "buffer":[B
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    .line 179
    .local v13, "context":Landroid/content/Context;
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->toneResourceId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v8

    .line 180
    .local v8, "afd":Landroid/content/res/AssetFileDescriptor;
    invoke-virtual {v8}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v14

    .line 181
    const/4 v12, 0x0

    .line 182
    .local v12, "bytesRead":I
    const/16 v17, 0x0

    .line 183
    .local v17, "read":I
    :goto_0
    array-length v1, v10

    sub-int/2addr v1, v12

    invoke-virtual {v14, v10, v12, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v17

    .line 184
    const/4 v1, -0x1

    move/from16 v0, v17

    if-eq v0, v1, :cond_0

    array-length v1, v10

    if-ne v1, v12, :cond_5

    .line 190
    :cond_0
    div-int v1, v12, v11

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->totalSamples:I

    .line 191
    sget-object v1, Lcom/vlingo/core/internal/audio/TonePlayer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "[LatencyCheck] AudioTrack totalSamples = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v7, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->totalSamples:I

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    invoke-static {v3, v4, v5}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v16

    .line 193
    .local v16, "minBufferSize":I
    move/from16 v0, v16

    invoke-static {v0, v12}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 197
    .local v6, "playerBufferSize":I
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 198
    invoke-static {v13}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->addListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V

    .line 199
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->addListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 202
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "use_stream_bluetooth_sco_for_assoc_svc"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 203
    :cond_3
    new-instance v1, Landroid/media/AudioTrack;

    const/4 v2, 0x6

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    .line 208
    :goto_1
    sget-object v1, Lcom/vlingo/core/internal/audio/TonePlayer;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[LatencyCheck] create AudioTrack instance"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/TonePlayer;->checkMuteVolume()V

    .line 210
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    const/4 v2, 0x0

    invoke-virtual {v1, v10, v2, v12}, Landroid/media/AudioTrack;->write([BII)I

    .line 212
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getState()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_8

    .line 213
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->play()V

    .line 214
    sget-object v1, Lcom/vlingo/core/internal/audio/TonePlayer;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[LatencyCheck] audioTrack.play()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v9

    .line 216
    .local v9, "audioSessionId":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    const/4 v2, 0x1

    const/4 v7, 0x0

    invoke-virtual {v1, v2, v9, v7}, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 218
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->toneHandler:Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    .end local v9    # "audioSessionId":I
    :goto_3
    if-eqz v14, :cond_4

    .line 234
    :try_start_1
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 237
    .end local v3    # "sampleRate":I
    .end local v4    # "channelConfig":I
    .end local v5    # "audioFormat":I
    .end local v6    # "playerBufferSize":I
    .end local v8    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v10    # "buffer":[B
    .end local v11    # "bytesPerSample":I
    .end local v12    # "bytesRead":I
    .end local v13    # "context":Landroid/content/Context;
    .end local v16    # "minBufferSize":I
    .end local v17    # "read":I
    .end local v18    # "readBufferSize":I
    :cond_4
    :goto_4
    return-void

    .line 187
    .restart local v3    # "sampleRate":I
    .restart local v4    # "channelConfig":I
    .restart local v5    # "audioFormat":I
    .restart local v8    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v10    # "buffer":[B
    .restart local v11    # "bytesPerSample":I
    .restart local v12    # "bytesRead":I
    .restart local v13    # "context":Landroid/content/Context;
    .restart local v17    # "read":I
    .restart local v18    # "readBufferSize":I
    :cond_5
    add-int v12, v12, v17

    goto/16 :goto_0

    .line 205
    .restart local v6    # "playerBufferSize":I
    .restart local v16    # "minBufferSize":I
    :cond_6
    :try_start_2
    new-instance v1, Landroid/media/AudioTrack;

    const/4 v2, 0x3

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 227
    .end local v3    # "sampleRate":I
    .end local v4    # "channelConfig":I
    .end local v5    # "audioFormat":I
    .end local v6    # "playerBufferSize":I
    .end local v8    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v10    # "buffer":[B
    .end local v11    # "bytesPerSample":I
    .end local v12    # "bytesRead":I
    .end local v13    # "context":Landroid/content/Context;
    .end local v16    # "minBufferSize":I
    .end local v17    # "read":I
    .end local v18    # "readBufferSize":I
    :catch_0
    move-exception v15

    .line 230
    .local v15, "ioe":Ljava/io/IOException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 231
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/TonePlayer;->cleanup()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 233
    if-eqz v14, :cond_4

    .line 234
    :try_start_4
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_4

    .line 215
    .end local v15    # "ioe":Ljava/io/IOException;
    .restart local v3    # "sampleRate":I
    .restart local v4    # "channelConfig":I
    .restart local v5    # "audioFormat":I
    .restart local v6    # "playerBufferSize":I
    .restart local v8    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v10    # "buffer":[B
    .restart local v11    # "bytesPerSample":I
    .restart local v12    # "bytesRead":I
    .restart local v13    # "context":Landroid/content/Context;
    .restart local v16    # "minBufferSize":I
    .restart local v17    # "read":I
    .restart local v18    # "readBufferSize":I
    :cond_7
    const/4 v9, 0x0

    goto :goto_2

    .line 223
    :cond_8
    :try_start_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/core/internal/audio/TonePlayer;->notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 224
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/TonePlayer;->cleanup()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 233
    .end local v3    # "sampleRate":I
    .end local v4    # "channelConfig":I
    .end local v5    # "audioFormat":I
    .end local v6    # "playerBufferSize":I
    .end local v8    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v10    # "buffer":[B
    .end local v11    # "bytesPerSample":I
    .end local v12    # "bytesRead":I
    .end local v13    # "context":Landroid/content/Context;
    .end local v16    # "minBufferSize":I
    .end local v17    # "read":I
    .end local v18    # "readBufferSize":I
    :catchall_0
    move-exception v1

    if-eqz v14, :cond_9

    .line 234
    :try_start_6
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :cond_9
    :goto_5
    throw v1

    .restart local v3    # "sampleRate":I
    .restart local v4    # "channelConfig":I
    .restart local v5    # "audioFormat":I
    .restart local v6    # "playerBufferSize":I
    .restart local v8    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v10    # "buffer":[B
    .restart local v11    # "bytesPerSample":I
    .restart local v12    # "bytesRead":I
    .restart local v13    # "context":Landroid/content/Context;
    .restart local v16    # "minBufferSize":I
    .restart local v17    # "read":I
    .restart local v18    # "readBufferSize":I
    :catch_2
    move-exception v1

    goto :goto_4

    .end local v3    # "sampleRate":I
    .end local v4    # "channelConfig":I
    .end local v5    # "audioFormat":I
    .end local v6    # "playerBufferSize":I
    .end local v8    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v10    # "buffer":[B
    .end local v11    # "bytesPerSample":I
    .end local v12    # "bytesRead":I
    .end local v13    # "context":Landroid/content/Context;
    .end local v16    # "minBufferSize":I
    .end local v17    # "read":I
    .end local v18    # "readBufferSize":I
    :catch_3
    move-exception v2

    goto :goto_5
.end method


# virtual methods
.method public onAudioFocusChanged(I)V
    .locals 4
    .param p1, "focusChange"    # I

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 292
    packed-switch p1, :pswitch_data_0

    .line 323
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 296
    :pswitch_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 298
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->isCalledMute:Z

    if-eqz v0, :cond_0

    .line 299
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->isCalledMute:Z

    .line 303
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0, v3, v3}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    goto :goto_0

    .line 311
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 313
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->isCalledMute:Z

    if-nez v0, :cond_0

    .line 314
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->isCalledMute:Z

    .line 318
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0, v2, v2}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    goto :goto_0

    .line 292
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onBluetoothServiceConnected()V
    .locals 0

    .prologue
    .line 328
    return-void
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 358
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 364
    return-void
.end method

.method public onScoConnected()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 332
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 334
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0, v2, v2}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    .line 341
    :cond_0
    return-void
.end method

.method public onScoDisconnected()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 345
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 349
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0, v2, v2}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    .line 352
    :cond_0
    return-void
.end method

.method public play()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/audio/TonePlayer;->play(Lcom/vlingo/core/internal/audio/TonePlayer$Listener;)V

    .line 147
    return-void
.end method

.method public play(Lcom/vlingo/core/internal/audio/TonePlayer$Listener;)V
    .locals 4
    .param p1, "listener"    # Lcom/vlingo/core/internal/audio/TonePlayer$Listener;

    .prologue
    const/4 v3, 0x1

    .line 150
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->busy:Z

    if-eqz v0, :cond_0

    .line 151
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Busy"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_0
    iput-boolean v3, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->busy:Z

    .line 155
    new-instance v0, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;-><init>(Lcom/vlingo/core/internal/audio/TonePlayer$Listener;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    .line 156
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "TonePlayer"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->workerThread:Landroid/os/HandlerThread;

    .line 157
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->workerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 158
    new-instance v0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->workerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;-><init>(Lcom/vlingo/core/internal/audio/TonePlayer;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->toneHandler:Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;

    .line 159
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer;->toneHandler:Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v3, v1, v2}, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 160
    return-void
.end method
