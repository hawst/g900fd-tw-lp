.class public final enum Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;
.super Ljava/lang/Enum;
.source "IntegratedAppInfoInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IntegrateAppType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

.field public static final enum FM_RADIO:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

.field public static final enum MEMO:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    const-string/jumbo v1, "MEMO"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;->MEMO:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    .line 9
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    const-string/jumbo v1, "FM_RADIO"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;->FM_RADIO:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    .line 7
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;->MEMO:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;->FM_RADIO:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    return-object v0
.end method
