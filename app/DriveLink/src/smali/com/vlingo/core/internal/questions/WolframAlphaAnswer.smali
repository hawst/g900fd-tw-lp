.class public Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;
.super Lcom/vlingo/core/internal/questions/AnswerAdaptation;
.source "WolframAlphaAnswer.java"


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/questions/Answer;)V
    .locals 0
    .param p1, "answer"    # Lcom/vlingo/core/internal/questions/Answer;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/questions/AnswerAdaptation;-><init>(Lcom/vlingo/core/internal/questions/Answer;)V

    .line 17
    return-void
.end method

.method private getResultText()Ljava/lang/String;
    .locals 10

    .prologue
    .line 94
    const-string/jumbo v3, ""

    .line 98
    .local v3, "result":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/questions/Answer$Section;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v4, v0, v1

    .line 99
    .local v4, "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    invoke-interface {v4}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "Result"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 101
    invoke-interface {v4}, Lcom/vlingo/core/internal/questions/Answer$Section;->getSubsections()[Lcom/vlingo/core/internal/questions/Answer$Subsection;

    move-result-object v6

    .line 102
    .local v6, "subsections":[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    move-object v0, v6

    .local v0, "arr$":[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v5, v0, v1

    .line 103
    .local v5, "subsection":Lcom/vlingo/core/internal/questions/Answer$Subsection;
    invoke-interface {v5}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getText()Ljava/lang/String;

    move-result-object v7

    .line 104
    .local v7, "text":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 105
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 102
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 98
    .end local v5    # "subsection":Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .end local v6    # "subsections":[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .end local v7    # "text":Ljava/lang/String;
    .local v0, "arr$":[Lcom/vlingo/core/internal/questions/Answer$Section;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    .end local v0    # "arr$":[Lcom/vlingo/core/internal/questions/Answer$Section;
    .end local v4    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    :cond_2
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->shouldNix(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 115
    const-string/jumbo v3, ""

    .line 117
    :cond_3
    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->stripText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method private shouldNix(Ljava/lang/String;)Z
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 128
    if-nez p1, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v0

    .line 131
    :cond_1
    const-string/jumbo v1, "^\\(noun\\)"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 134
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;
    .locals 12

    .prologue
    .line 62
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 64
    .local v8, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/questions/Answer$Section;>;"
    const-string/jumbo v10, "Result"

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->hasSection(Ljava/lang/String;)Z

    move-result v2

    .line 66
    .local v2, "hasResult":Z
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/questions/Answer$Section;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v4, v3

    .end local v0    # "arr$":[Lcom/vlingo/core/internal/questions/Answer$Section;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_4

    aget-object v7, v0, v4

    .line 68
    .local v7, "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    const-string/jumbo v10, "Result"

    invoke-interface {v7}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 70
    invoke-interface {v7}, Lcom/vlingo/core/internal/questions/Answer$Section;->getSubsections()[Lcom/vlingo/core/internal/questions/Answer$Subsection;

    move-result-object v1

    .local v1, "arr$":[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v3, 0x0

    .end local v4    # "i$":I
    .restart local v3    # "i$":I
    :goto_1
    if-ge v3, v6, :cond_2

    aget-object v9, v1, v3

    .line 71
    .local v9, "subsection":Lcom/vlingo/core/internal/questions/Answer$Subsection;
    invoke-interface {v9}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->hasImage()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 72
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 75
    .end local v1    # "arr$":[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .end local v3    # "i$":I
    .end local v6    # "len$":I
    .end local v9    # "subsection":Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .restart local v4    # "i$":I
    :cond_1
    const-string/jumbo v10, "Input interpretation"

    invoke-interface {v7}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    if-eqz v2, :cond_3

    .line 66
    .end local v4    # "i$":I
    :cond_2
    :goto_2
    add-int/lit8 v3, v4, 0x1

    .restart local v3    # "i$":I
    move v4, v3

    .end local v3    # "i$":I
    .restart local v4    # "i$":I
    goto :goto_0

    .line 78
    :cond_3
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 85
    .end local v7    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    :cond_4
    if-eqz v2, :cond_5

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-direct {p0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->getResultText()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 87
    const-string/jumbo v10, "Result"

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->getSection(Ljava/lang/String;)Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_5
    const/4 v10, 0x0

    new-array v10, v10, [Lcom/vlingo/core/internal/questions/Answer$Section;

    invoke-interface {v8, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lcom/vlingo/core/internal/questions/Answer$Section;

    return-object v10
.end method

.method public getSimpleResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    const-string/jumbo v0, "Result"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->hasSection(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->getResultText()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->getSimpleResponse()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public hasAnswer()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->getSimpleResponse()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 22
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_2

    .line 27
    :cond_0
    :goto_0
    return v0

    .line 24
    :cond_1
    const-string/jumbo v1, "(data not available)"

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->getSimpleResponse()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 27
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
