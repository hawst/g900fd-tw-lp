.class public Lcom/vlingo/core/internal/contacts/mru/MRUTable;
.super Ljava/lang/Object;
.source "MRUTable.java"


# instance fields
.field private m_countSinceLastNormalization:I

.field private m_name:Ljava/lang/String;

.field private m_table:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private m_tableSum:F


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "expectedSize"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/Hashtable;

    mul-int/lit8 v1, p2, 0x2

    invoke-direct {v0, v1}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_countSinceLastNormalization:I

    .line 27
    return-void
.end method


# virtual methods
.method public getCountSinceLastUdate()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_countSinceLastNormalization:I

    return v0
.end method

.method public getNormalizedCount(I)F
    .locals 4
    .param p1, "contact"    # I

    .prologue
    const/4 v2, 0x0

    .line 122
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 123
    .local v0, "contactInt":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v3, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;

    .line 124
    .local v1, "mfc":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    if-nez v1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v2

    .line 128
    :cond_1
    iget v3, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    cmpl-float v3, v3, v2

    if-eqz v3, :cond_0

    .line 129
    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;->getMaxCount()F

    move-result v2

    iget v3, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    div-float/2addr v2, v3

    goto :goto_0
.end method

.method public getNormalizedCount(ILjava/lang/String;)F
    .locals 4
    .param p1, "contact"    # I
    .param p2, "address"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 88
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 89
    .local v0, "contactInt":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v3, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;

    .line 90
    .local v1, "mfc":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    if-nez v1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v2

    .line 92
    :cond_1
    iget v3, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    cmpl-float v3, v3, v2

    if-eqz v3, :cond_0

    .line 94
    invoke-virtual {v1, p2}, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;->getCount(Ljava/lang/String;)F

    move-result v2

    iget v3, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    div-float/2addr v2, v3

    goto :goto_0
.end method

.method public getNumItems()I
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    return v0
.end method

.method public incrementCount(ILjava/lang/String;)F
    .locals 4
    .param p1, "contact"    # I
    .param p2, "address"    # Ljava/lang/String;

    .prologue
    .line 101
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 102
    .local v0, "contactInt":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;

    .line 103
    .local v1, "mfc":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    if-nez v1, :cond_0

    .line 104
    new-instance v1, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;

    .end local v1    # "mfc":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;-><init>()V

    .line 105
    .restart local v1    # "mfc":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v2, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    :cond_0
    iget v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v2, v3

    iput v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    .line 108
    iget v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_countSinceLastNormalization:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_countSinceLastNormalization:I

    .line 109
    invoke-virtual {v1, p2}, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;->incrementCount(Ljava/lang/String;)F

    move-result v2

    return v2
.end method

.method public removeLowestScoringContact()Ljava/lang/Integer;
    .locals 8

    .prologue
    .line 49
    const/4 v3, 0x0

    .line 50
    .local v3, "lowestScoringContact":Ljava/lang/Integer;
    iget v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    .line 52
    .local v2, "lowestScore":F
    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v6}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 53
    .local v1, "keys":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 54
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 55
    .local v0, "key":Ljava/lang/Integer;
    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;

    .line 56
    .local v4, "mru":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    if-eqz v4, :cond_0

    .line 57
    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;->getMaxCount()F

    move-result v5

    .line 58
    .local v5, "mruScore":F
    if-eqz v3, :cond_1

    cmpg-float v6, v5, v2

    if-gez v6, :cond_0

    .line 59
    :cond_1
    move v2, v5

    .line 60
    move-object v3, v0

    goto :goto_0

    .line 64
    .end local v0    # "key":Ljava/lang/Integer;
    .end local v4    # "mru":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    .end local v5    # "mruScore":F
    :cond_2
    if-eqz v3, :cond_4

    .line 65
    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v6, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;

    .line 66
    .restart local v4    # "mru":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    if-eqz v4, :cond_3

    .line 67
    iget v6, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;->getSum()F

    move-result v7

    sub-float/2addr v6, v7

    iput v6, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    .line 69
    :cond_3
    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v6, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    .end local v4    # "mru":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    :cond_4
    return-object v3
.end method

.method public scaleValues(F)V
    .locals 3
    .param p1, "scale"    # F

    .prologue
    .line 112
    const/4 v2, 0x0

    iput v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_countSinceLastNormalization:I

    .line 113
    iget v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    mul-float/2addr v2, p1

    iput v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    .line 114
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .line 115
    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;

    .line 117
    .local v1, "mru":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;->scaleValues(F)V

    goto :goto_0

    .line 119
    .end local v1    # "mru":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    :cond_0
    return-void
.end method

.method public setCount(ILjava/lang/String;F)V
    .locals 3
    .param p1, "contact"    # I
    .param p2, "address"    # Ljava/lang/String;
    .param p3, "count"    # F

    .prologue
    .line 78
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 79
    .local v0, "contactInt":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;

    .line 80
    .local v1, "mfc":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    if-nez v1, :cond_0

    .line 81
    new-instance v1, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;

    .end local v1    # "mfc":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;-><init>()V

    .line 82
    .restart local v1    # "mfc":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v2, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    :cond_0
    invoke-virtual {v1, p2, p3}, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;->setCount(Ljava/lang/String;F)V

    .line 85
    iget v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    add-float/2addr v2, p3

    iput v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_tableSum:F

    .line 86
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 35
    const-string/jumbo v3, ""

    .line 36
    .local v3, "outputString":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 37
    .local v1, "keys":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 38
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 39
    .local v0, "key":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->m_table:Ljava/util/Hashtable;

    invoke-virtual {v4, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;

    .line 40
    .local v2, "mru":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    if-eqz v2, :cond_0

    .line 41
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " Contact "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/mru/MRUForContact;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 44
    .end local v0    # "key":Ljava/lang/Integer;
    .end local v2    # "mru":Lcom/vlingo/core/internal/contacts/mru/MRUForContact;
    :cond_1
    return-object v3
.end method
