.class public interface abstract Lcom/vlingo/core/internal/util/MusicPlusUtil;
.super Ljava/lang/Object;
.source "MusicPlusUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/CoreAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;
    }
.end annotation


# virtual methods
.method public abstract getFavoriteMusicIntent()Landroid/content/Intent;
.end method

.method public abstract getMusicAppVersion()I
.end method

.method public abstract getMusicFavorite()Ljava/lang/String;
.end method

.method public abstract getMusicMoodUri(Ljava/lang/String;)Landroid/net/Uri;
.end method

.method public abstract getMusicPlaylistUri(J)Landroid/net/Uri;
.end method

.method public abstract getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;
.end method

.method public abstract usingLocalMusicDB(Z)Z
.end method
