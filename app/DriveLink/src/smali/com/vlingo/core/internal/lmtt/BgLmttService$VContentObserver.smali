.class Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;
.super Landroid/database/ContentObserver;
.source "BgLmttService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmtt/BgLmttService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VContentObserver"
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

.field final synthetic this$0:Lcom/vlingo/core/internal/lmtt/BgLmttService;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/lmtt/BgLmttService;Ljava/lang/String;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;)V
    .locals 1
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "updateType"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->this$0:Lcom/vlingo/core/internal/lmtt/BgLmttService;

    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 59
    const-string/jumbo v0, "VContentObserver"

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->TAG:Ljava/lang/String;

    .line 63
    iput-object p2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->mName:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->mUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    .line 65
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 69
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 72
    const-string/jumbo v0, "VContentObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onChange() : selfChange="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", Uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", mUpdateType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->mUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->this$0:Lcom/vlingo/core/internal/lmtt/BgLmttService;

    # getter for: Lcom/vlingo/core/internal/lmtt/BgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->access$100(Lcom/vlingo/core/internal/lmtt/BgLmttService;)Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->this$0:Lcom/vlingo/core/internal/lmtt/BgLmttService;

    # getter for: Lcom/vlingo/core/internal/lmtt/BgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->access$100(Lcom/vlingo/core/internal/lmtt/BgLmttService;)Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->fireSync(Z)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->this$0:Lcom/vlingo/core/internal/lmtt/BgLmttService;

    # getter for: Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->access$200(Lcom/vlingo/core/internal/lmtt/BgLmttService;)Lcom/vlingo/core/internal/lmtt/LMTTManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->this$0:Lcom/vlingo/core/internal/lmtt/BgLmttService;

    # getter for: Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->access$200(Lcom/vlingo/core/internal/lmtt/BgLmttService;)Lcom/vlingo/core/internal/lmtt/LMTTManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;->mUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v0, v1, v3, v3}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->fireUpdate(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZZ)V

    .line 82
    :cond_1
    return-void
.end method
