.class public interface abstract Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
.super Ljava/lang/Object;
.source "DialogFlowListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;
    }
.end annotation


# virtual methods
.method public abstract enteredDomain(Lcom/vlingo/core/internal/domain/DomainName;)V
.end method

.method public abstract getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
.end method

.method public abstract onASRRecorderClosed()V
.end method

.method public abstract onASRRecorderOpened()V
.end method

.method public abstract onInterceptStartReco()Z
.end method

.method public abstract onRecoCancelled()V
.end method

.method public abstract onRecoToneStarting(Z)J
.end method

.method public abstract onRecoToneStopped(Z)V
.end method

.method public abstract onResultsNoAction()V
.end method

.method public abstract showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V
.end method

.method public abstract showError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
.end method

.method public abstract showRMSChange(I)V
.end method

.method public abstract showReceivedResults(Lcom/vlingo/core/internal/logging/EventLog;)V
.end method

.method public abstract showRecoStateChange(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
.end method

.method public abstract showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V
.end method

.method public abstract showVlingoText(Ljava/lang/String;)V
.end method

.method public abstract showWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;
.end method

.method public abstract userCancel()V
.end method
