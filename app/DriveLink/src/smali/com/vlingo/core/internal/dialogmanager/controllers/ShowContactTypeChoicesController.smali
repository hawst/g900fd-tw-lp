.class public Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;
.super Lcom/vlingo/core/internal/dialogmanager/Controller;
.source "ShowContactTypeChoicesController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field choiceIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field contactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field mappedContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field matchContactData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;-><init>()V

    return-void
.end method

.method private matchData(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "contactData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v4, "matchedContacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :try_start_0
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->choiceIds:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 149
    .local v0, "choice":Ljava/lang/String;
    const/16 v5, 0x2e

    invoke-static {v0, v5}, Lcom/vlingo/core/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, "choices":[Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v5, v1, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 152
    .end local v0    # "choice":Ljava/lang/String;
    .end local v1    # "choices":[Ljava/lang/String;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 153
    .local v2, "e":Ljava/lang/NumberFormatException;
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Expected Contact Data contactData to be of form <contact_id>.<address_id>: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    new-instance v5, Ljava/security/InvalidParameterException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Expected Contact Data contactData to be of form <contact_id>.<address_id>: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 155
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v2

    .line 156
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Expected Contact Data contactData to be of form <contact_id>.<address_id>: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    .end local v2    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_0
    return-object v4
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 13
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 50
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 52
    const/4 v8, 0x0

    .line 53
    .local v8, "tempChoiceIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 54
    .local v3, "decorator":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    const-string/jumbo v9, "ListPosition"

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 55
    const-string/jumbo v9, "choices"

    invoke-static {p1, v9, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    .line 66
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v9

    const-string/jumbo v10, "contact"

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 69
    if-eqz v8, :cond_3

    .line 70
    :try_start_0
    iput-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->choiceIds:Ljava/util/List;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_1
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->choiceIds:Ljava/util/List;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->choiceIds:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-nez v9, :cond_4

    .line 106
    :cond_0
    :goto_2
    return v12

    .line 57
    :cond_1
    const-string/jumbo v9, "Start"

    invoke-static {p1, v9, v12, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v7

    .line 58
    .local v7, "start":I
    const-string/jumbo v9, "End"

    invoke-static {p1, v9, v12, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v5

    .line 59
    .local v5, "end":I
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "tempChoiceIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .restart local v8    # "tempChoiceIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move v6, v7

    .local v6, "i":I
    :goto_3
    if-ge v6, v5, :cond_2

    .line 61
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "0."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 63
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeReplaceable()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v3

    goto :goto_0

    .line 72
    .end local v5    # "end":I
    .end local v6    # "i":I
    .end local v7    # "start":I
    :cond_3
    :try_start_1
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getChoicesAsStrings(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->choiceIds:Ljava/util/List;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 75
    :catch_0
    move-exception v4

    .line 76
    .local v4, "e":Lorg/json/JSONException;
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->TAG:Ljava/lang/String;

    invoke-virtual {v4}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 84
    .end local v4    # "e":Lorg/json/JSONException;
    :cond_4
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->choiceIds:Ljava/util/List;

    invoke-interface {v9, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->splitAddressFromContactId(Ljava/lang/String;)[I

    move-result-object v9

    aget v0, v9, v12

    .line 87
    .local v0, "contactId":I
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 89
    .local v2, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-static {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getContactMatchFromId(Ljava/util/List;I)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v9

    iput-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->contactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 90
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->contactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v9, :cond_0

    .line 93
    new-instance v9, Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->contactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v10}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v9}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->matchData(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v9

    iput-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->matchContactData:Ljava/util/List;

    .line 96
    const/4 v1, 0x0

    .line 97
    .local v1, "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->matchContactData:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_5

    .line 98
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->matchContactData:Ljava/util/List;

    const-string/jumbo v11, "call"

    invoke-static {v9, v10, v11}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeMap(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 102
    :cond_5
    if-eqz v1, :cond_0

    .line 103
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactTypeChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {p2, v9, v3, v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 104
    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->mappedContacts:Ljava/util/List;

    goto/16 :goto_2
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "unused"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    .line 120
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 121
    const-string/jumbo v5, "choice"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamInt(Landroid/content/Intent;Ljava/lang/String;I)I

    move-result v2

    .line 122
    .local v2, "index":I
    if-ne v2, v7, :cond_0

    .line 123
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "Expected positive BUNDLE_CHOICE index for ShowContactTyepChoice"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    new-instance v5, Ljava/security/InvalidParameterException;

    const-string/jumbo v6, "Expected positive BUNDLE_CHOICE index for ShowContactTypeChoice"

    invoke-direct {v5, v6}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 126
    :cond_0
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->mappedContacts:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 127
    .local v4, "selectedContact":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->matchContactData:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 128
    .local v3, "position":I
    if-ne v3, v7, :cond_2

    .line 129
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "Unexpected mismatch between stored contacts"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    .end local v2    # "index":I
    .end local v3    # "position":I
    .end local v4    # "selectedContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_1
    :goto_0
    return-void

    .line 133
    .restart local v2    # "index":I
    .restart local v3    # "position":I
    .restart local v4    # "selectedContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->choiceIds:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 134
    .local v1, "idString":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;-><init>(Ljava/lang/String;)V

    .line 135
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v5, v0, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 136
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;
    .end local v1    # "idString":Ljava/lang/String;
    .end local v2    # "index":I
    .end local v3    # "position":I
    .end local v4    # "selectedContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_3
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.DataTransfered"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 139
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactTypeChoicesController;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method
