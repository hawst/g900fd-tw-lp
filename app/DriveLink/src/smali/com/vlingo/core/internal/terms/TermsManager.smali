.class public final Lcom/vlingo/core/internal/terms/TermsManager;
.super Ljava/lang/Object;
.source "TermsManager.java"

# interfaces
.implements Lcom/vlingo/core/facade/terms/ITermsManager;


# static fields
.field private static final INSTANCE:Lcom/vlingo/core/internal/terms/TermsManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/vlingo/core/internal/terms/TermsManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/terms/TermsManager;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/terms/TermsManager;->INSTANCE:Lcom/vlingo/core/internal/terms/TermsManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/terms/TermsManager;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/vlingo/core/internal/terms/TermsManager;->INSTANCE:Lcom/vlingo/core/internal/terms/TermsManager;

    return-object v0
.end method


# virtual methods
.method public isTOSAccepted()Z
    .locals 2

    .prologue
    .line 29
    const-string/jumbo v0, "tos_accepted"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setTOSAccepted(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 38
    invoke-static {p1}, Lcom/vlingo/core/internal/settings/Settings;->setTOSAccepted(Z)V

    .line 39
    return-void
.end method
