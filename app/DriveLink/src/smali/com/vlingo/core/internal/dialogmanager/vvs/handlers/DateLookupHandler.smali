.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/DateLookupHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "DateLookupHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 15
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 26
    invoke-super/range {p0 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 28
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v11

    const-string/jumbo v12, "date"

    invoke-virtual {v11, v12}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 31
    const-string/jumbo v11, "Date"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v12}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "date":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 34
    const/4 v11, 0x1

    invoke-static {v1, v11}, Lcom/vlingo/core/internal/schedule/DateUtil;->getMillisFromString(Ljava/lang/String;Z)J

    move-result-wide v6

    .line 35
    .local v6, "miliDate":J
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 36
    .local v5, "lookupDate":Ljava/util/Date;
    const/4 v11, 0x0

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v12

    invoke-static {v11, v12}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "formattedLookupDate":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-interface {v0, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .end local v2    # "formattedLookupDate":Ljava/lang/String;
    .end local v5    # "lookupDate":Ljava/util/Date;
    .end local v6    # "miliDate":J
    :cond_0
    const-string/jumbo v11, "Time"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v12}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    .line 42
    .local v9, "time":Ljava/lang/String;
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 43
    const-string/jumbo v11, "current"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 46
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "ko-KR"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 47
    new-instance v11, Ljava/util/Date;

    invoke-direct {v11}, Ljava/util/Date;-><init>()V

    invoke-static {v11}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 48
    .local v4, "hourMin":Ljava/lang/String;
    const/4 v11, 0x0

    const-string/jumbo v12, ":"

    invoke-virtual {v4, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v4, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 49
    .local v3, "hour":Ljava/lang/String;
    const-string/jumbo v11, ":"

    invoke-virtual {v4, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v4, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 50
    .local v8, "min":Ljava/lang/String;
    sget-object v11, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_time_at_present:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v3, v12, v13

    const/4 v13, 0x1

    aput-object v8, v12, v13

    invoke-static {v11, v12}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/DateLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 55
    .end local v3    # "hour":Ljava/lang/String;
    .end local v4    # "hourMin":Ljava/lang/String;
    .end local v8    # "min":Ljava/lang/String;
    .local v10, "ttsStr":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p2

    invoke-interface {v0, v10, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    sget-object v11, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowClock:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v11, v12, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 60
    .end local v10    # "ttsStr":Ljava/lang/String;
    :cond_1
    const/4 v11, 0x0

    return v11

    .line 52
    :cond_2
    sget-object v11, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_time_at_present:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    new-instance v14, Ljava/util/Date;

    invoke-direct {v14}, Ljava/util/Date;-><init>()V

    invoke-static {v14}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/DateLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "ttsStr":Ljava/lang/String;
    goto :goto_0
.end method
