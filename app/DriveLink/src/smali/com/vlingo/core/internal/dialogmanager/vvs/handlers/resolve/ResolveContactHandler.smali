.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;
.source "ResolveContactHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected processContactMatching(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;)V
    .locals 12
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "contactType"    # Lcom/vlingo/core/internal/contacts/ContactType;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    const/high16 v9, 0x42c80000    # 100.0f

    .line 23
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[ActionQuery] Contact Action : Query = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", Type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->doesUsePhoneDisambiguation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    new-instance v0, Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactMatcher;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getContactMatchListener()Lcom/vlingo/core/internal/contacts/ContactMatchListener;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getPhoneTypes()[I

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getEmailTypes()[I

    move-result-object v5

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getSocialTypes()[I

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getAddressTypes()[I

    move-result-object v7

    move-object v3, p2

    move-object v8, p1

    invoke-direct/range {v0 .. v11}, Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactMatcher;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V

    .line 34
    :goto_0
    return-void

    .line 30
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatcher;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getContactMatchListener()Lcom/vlingo/core/internal/contacts/ContactMatchListener;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getPhoneTypes()[I

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getEmailTypes()[I

    move-result-object v5

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getSocialTypes()[I

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;->getAddressTypes()[I

    move-result-object v7

    move-object v3, p2

    move-object v8, p1

    invoke-direct/range {v0 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatcher;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V

    goto :goto_0
.end method
