.class Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;
.super Ljava/lang/Object;
.source "LocalSearchServiceManager.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HttpCallbackHandler"
.end annotation


# instance fields
.field m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

.field private processingDone:Z

.field final synthetic this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 1
    .param p2, "listener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 507
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 504
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 508
    iput-object p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 509
    return-void
.end method


# virtual methods
.method public onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 554
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Cancelled"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 555
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->processingDone:Z

    .line 558
    return-void
.end method

.method public onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 546
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Failure"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 547
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->processingDone:Z

    .line 550
    return-void
.end method

.method public onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 513
    iget v0, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_2

    .line 516
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 517
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const/4 v1, 0x1

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestComplete(ZLjava/lang/Object;)V

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 519
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Response is empty"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 522
    :cond_2
    iget-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->processingDone:Z

    if-nez v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "responseCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 537
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Timeout"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 538
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->processingDone:Z

    .line 541
    const/4 v0, 0x0

    return v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 532
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestScheduled()V

    .line 533
    return-void
.end method
