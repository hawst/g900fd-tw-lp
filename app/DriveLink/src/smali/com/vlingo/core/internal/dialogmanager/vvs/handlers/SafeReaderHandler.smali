.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "SafeReaderHandler.java"


# instance fields
.field alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

.field alerts:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field private isSilentMode:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "isSilentMode"    # Z

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 29
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->isSilentMode:Z

    .line 30
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 39
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/Controller;

    .line 41
    .local v0, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    const-class v1, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    move-object v1, v0

    .line 42
    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->isSilentMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->setSilentMode(Z)V

    move-object v1, v0

    .line 43
    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->setAlertQueue(Ljava/util/LinkedList;)V

    move-object v1, v0

    .line 44
    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->setAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V

    .line 45
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 46
    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 47
    const/4 v1, 0x0

    return v1
.end method

.method public init(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V
    .locals 0
    .param p1, "safeReaderAlert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 56
    return-void
.end method

.method public init(Ljava/util/LinkedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "safeReaderAlerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->alerts:Ljava/util/LinkedList;

    .line 52
    return-void
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->isSilentMode:Z

    return v0
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "silentMode"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->isSilentMode:Z

    .line 64
    return-void
.end method
