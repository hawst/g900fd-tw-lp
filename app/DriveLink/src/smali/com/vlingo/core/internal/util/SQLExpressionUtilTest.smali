.class public Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;
.super Landroid/test/AndroidTestCase;
.source "SQLExpressionUtilTest.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/test/AndroidTestCase;-><init>()V

    return-void
.end method


# virtual methods
.method public escapeQuotesTest()V
    .locals 3

    .prologue
    .line 72
    const-string/jumbo v2, "Mc\'Dc"

    .line 73
    .local v2, "value":Ljava/lang/String;
    const-string/jumbo v0, "Mc\'\'Dc"

    .line 74
    .local v0, "expectedResult":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "result":Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public likeArrayTest()V
    .locals 7

    .prologue
    .line 42
    const-string/jumbo v0, "Artist"

    .line 43
    .local v0, "columnName":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v3, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "Lenon"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "Star"

    aput-object v5, v3, v4

    .line 44
    .local v3, "values":[Ljava/lang/String;
    const-string/jumbo v1, "Artist like \'%Lenon%\' or Artist like \'%Star%\'"

    .line 45
    .local v1, "expectedResult":Ljava/lang/String;
    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "result":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "expectedResult="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    sget-object v4, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "result="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public likeListTest()V
    .locals 7

    .prologue
    .line 52
    const-string/jumbo v0, "Artist"

    .line 53
    .local v0, "columnName":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v3, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string/jumbo v4, "Lenon"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    const-string/jumbo v4, "Star"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    const-string/jumbo v1, "Artist like \'%Lenon%\' or Artist like \'%Star%\'"

    .line 57
    .local v1, "expectedResult":Ljava/lang/String;
    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 58
    .local v2, "result":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "expectedResult="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    sget-object v4, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "result="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public likeTest()V
    .locals 7

    .prologue
    .line 32
    const-string/jumbo v0, "Artist"

    .line 33
    .local v0, "columnName":Ljava/lang/String;
    const-string/jumbo v3, "John Lenon"

    .line 34
    .local v3, "value":Ljava/lang/String;
    const-string/jumbo v1, "Artist like \'%John Lenon%\'"

    .line 35
    .local v1, "expectedResult":Ljava/lang/String;
    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 36
    .local v2, "result":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "expectedResult="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    sget-object v4, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "result="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public orTest()V
    .locals 4

    .prologue
    .line 64
    const-string/jumbo v0, "a=1"

    .line 65
    .local v0, "exp1":Ljava/lang/String;
    const-string/jumbo v1, "b=2"

    .line 66
    .local v1, "exp2":Ljava/lang/String;
    const-string/jumbo v2, "a=1 or b=2"

    .line 67
    .local v2, "expectedResult":Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->or(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 68
    .local v3, "result":Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/util/SQLExpressionUtilTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method protected setUp()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 24
    invoke-super {p0}, Landroid/test/AndroidTestCase;->setUp()V

    .line 25
    return-void
.end method

.method protected tearDown()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    invoke-super {p0}, Landroid/test/AndroidTestCase;->tearDown()V

    .line 29
    return-void
.end method
