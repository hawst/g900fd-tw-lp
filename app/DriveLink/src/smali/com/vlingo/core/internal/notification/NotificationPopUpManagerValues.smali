.class public Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;
.super Ljava/lang/Object;
.source "NotificationPopUpManagerValues.java"


# static fields
.field private static notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static existDialog()Z
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;->existDialog()Z

    move-result v0

    return v0
.end method

.method public static hasNotifications(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)Z
    .locals 1
    .param p0, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .prologue
    .line 21
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    invoke-interface {v0, p0}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;->hasNotifications(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)Z

    move-result v0

    return v0
.end method

.method public static needNotifications()Z
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;->needNotifications()Z

    move-result v0

    return v0
.end method

.method public static setNotificationPopUpManager(Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;)V
    .locals 0
    .param p0, "notificationManagerValue"    # Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    .prologue
    .line 10
    sput-object p0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    .line 11
    return-void
.end method

.method public static showNextNotification()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;->showNextNotification()V

    .line 40
    return-void
.end method

.method public static showNextNotification(Landroid/app/Activity;)V
    .locals 1
    .param p0, "ctx"    # Landroid/app/Activity;

    .prologue
    .line 44
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    invoke-interface {v0, p0}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;->setContext(Landroid/app/Activity;)V

    .line 45
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;->showNextNotification()V

    .line 46
    return-void
.end method

.method public static showNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp;Landroid/app/Activity;)V
    .locals 1
    .param p0, "notification"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 60
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    invoke-interface {v0, p0, p1}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;->showNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp;Landroid/app/Activity;)V

    .line 61
    return-void
.end method

.method public static showingNotifications()Z
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;->showingNotifications()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public dismissNotifications()V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;->dismissNotifications()V

    .line 57
    return-void
.end method

.method public sendResult(I)V
    .locals 1
    .param p1, "result"    # I

    .prologue
    .line 50
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->notificationManager:Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;->sendResult(I)V

    .line 51
    return-void
.end method
