.class public final Lcom/vlingo/core/internal/answers/AnswerManager;
.super Ljava/lang/Object;
.source "AnswerManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final UNKNOWN_LOCATION:Ljava/lang/String; = "unknown"

.field private static final VARIABLE_POSTFIX:Ljava/lang/String; = "}"

.field private static final VARIABLE_PREFIX:Ljava/lang/String; = "${"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/vlingo/core/internal/answers/AnswerManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/answers/AnswerManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getDateString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getLocation(Landroid/content/Context;)Ljava/lang/String;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v12, 0x0

    .line 70
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLat()D

    move-result-wide v1

    .line 71
    .local v1, "lastLat":D
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLong()D

    move-result-wide v3

    .line 74
    .local v3, "lastLong":D
    cmpl-double v5, v1, v12

    if-nez v5, :cond_1

    cmpl-double v5, v3, v12

    if-nez v5, :cond_1

    .line 75
    const-string/jumbo v10, "unknown"

    .line 107
    .end local v1    # "lastLat":D
    .end local v3    # "lastLong":D
    :cond_0
    :goto_0
    return-object v10

    .line 79
    .restart local v1    # "lastLat":D
    .restart local v3    # "lastLong":D
    :cond_1
    new-instance v0, Landroid/location/Geocoder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v0, p0, v5}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 80
    .local v0, "geocoder":Landroid/location/Geocoder;
    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 81
    .local v7, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 82
    const/4 v5, 0x0

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    .line 85
    .local v6, "a":Landroid/location/Address;
    invoke-virtual {v6}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v11

    .line 86
    .local v11, "thoroughfare":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v9

    .line 87
    .local v9, "locality":Ljava/lang/String;
    if-nez v11, :cond_2

    if-eqz v9, :cond_4

    .line 88
    :cond_2
    const-string/jumbo v10, ""

    .line 89
    .local v10, "place":Ljava/lang/String;
    if-eqz v11, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v12, " "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 90
    :cond_3
    if-eqz v9, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v12, " "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    goto :goto_0

    .line 103
    .end local v0    # "geocoder":Landroid/location/Geocoder;
    .end local v1    # "lastLat":D
    .end local v3    # "lastLong":D
    .end local v6    # "a":Landroid/location/Address;
    .end local v7    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v9    # "locality":Ljava/lang/String;
    .end local v10    # "place":Ljava/lang/String;
    .end local v11    # "thoroughfare":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 104
    .local v8, "ex":Ljava/lang/Exception;
    sget-object v5, Lcom/vlingo/core/internal/answers/AnswerManager;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Exception: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v5, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    .end local v8    # "ex":Ljava/lang/Exception;
    :cond_4
    const-string/jumbo v10, "unknown"

    goto/16 :goto_0
.end method

.method private static getTimeString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getVariable(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "variableName"    # Ljava/lang/String;

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "${"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static replaceAnswerVariables(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "answer"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 36
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v4, "time"

    invoke-static {v4}, Lcom/vlingo/core/internal/answers/AnswerManager;->getVariable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 37
    invoke-static {v0}, Lcom/vlingo/core/internal/answers/AnswerManager;->getTimeString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "time":Ljava/lang/String;
    const-string/jumbo v4, "time"

    invoke-static {v4}, Lcom/vlingo/core/internal/answers/AnswerManager;->getVariable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 40
    .end local v3    # "time":Ljava/lang/String;
    :cond_0
    const-string/jumbo v4, "date"

    invoke-static {v4}, Lcom/vlingo/core/internal/answers/AnswerManager;->getVariable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 41
    invoke-static {v0}, Lcom/vlingo/core/internal/answers/AnswerManager;->getDateString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "date":Ljava/lang/String;
    const-string/jumbo v4, "date"

    invoke-static {v4}, Lcom/vlingo/core/internal/answers/AnswerManager;->getVariable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 44
    .end local v1    # "date":Ljava/lang/String;
    :cond_1
    const-string/jumbo v4, "location"

    invoke-static {v4}, Lcom/vlingo/core/internal/answers/AnswerManager;->getVariable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 45
    invoke-static {v0}, Lcom/vlingo/core/internal/answers/AnswerManager;->getLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "location":Ljava/lang/String;
    const-string/jumbo v4, "location"

    invoke-static {v4}, Lcom/vlingo/core/internal/answers/AnswerManager;->getVariable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 48
    .end local v2    # "location":Ljava/lang/String;
    :cond_2
    const-string/jumbo v4, "whereami"

    invoke-static {v4}, Lcom/vlingo/core/internal/answers/AnswerManager;->getVariable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 49
    invoke-static {v0}, Lcom/vlingo/core/internal/answers/AnswerManager;->getLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 50
    .restart local v2    # "location":Ljava/lang/String;
    const-string/jumbo v4, "unknown"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 51
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_not_detected_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object p0

    .line 57
    .end local v2    # "location":Ljava/lang/String;
    :cond_3
    :goto_0
    return-object p0

    .line 54
    .restart local v2    # "location":Ljava/lang/String;
    :cond_4
    const-string/jumbo v4, "whereami"

    invoke-static {v4}, Lcom/vlingo/core/internal/answers/AnswerManager;->getVariable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
