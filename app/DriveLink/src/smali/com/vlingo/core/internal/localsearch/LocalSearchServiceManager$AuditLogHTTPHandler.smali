.class Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;
.super Ljava/lang/Object;
.source "LocalSearchServiceManager.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AuditLogHTTPHandler"
.end annotation


# instance fields
.field action:Ljava/lang/String;

.field requestType:Ljava/lang/String;

.field retryNum:I

.field final synthetic this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

.field trackingId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p2, "requestType"    # Ljava/lang/String;
    .param p3, "trackingId"    # Ljava/lang/String;
    .param p4, "action"    # Ljava/lang/String;
    .param p5, "retryNum"    # I

    .prologue
    .line 711
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 712
    iput-object p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->requestType:Ljava/lang/String;

    .line 713
    iput-object p3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->trackingId:Ljava/lang/String;

    .line 714
    iput-object p4, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->action:Ljava/lang/String;

    .line 715
    iput p5, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->retryNum:I

    .line 716
    return-void
.end method


# virtual methods
.method public onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 764
    return-void
.end method

.method public onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 758
    return-void
.end method

.method public onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 7
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 718
    const/4 v1, 0x0

    .line 721
    .local v1, "success":Z
    iget v2, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    .line 722
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v0

    .line 723
    .local v0, "res":Ljava/lang/String;
    const-string/jumbo v2, "Status=\"OK\""

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 726
    const/4 v1, 0x1

    .line 729
    .end local v0    # "res":Ljava/lang/String;
    :cond_0
    if-nez v1, :cond_1

    .line 730
    # getter for: Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "AuditLogHandler failure!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    iget v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->retryNum:I

    if-lez v2, :cond_2

    .line 732
    # getter for: Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Retrying, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->retryNum:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " retries remain"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    iget v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->retryNum:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->retryNum:I

    .line 734
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    iget-object v3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->requestType:Ljava/lang/String;

    iget-object v4, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->trackingId:Ljava/lang/String;

    iget-object v5, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->action:Ljava/lang/String;

    iget v6, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;->retryNum:I

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendAuditLogRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 740
    :cond_1
    :goto_0
    return-void

    .line 737
    :cond_2
    # getter for: Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "NOT retrying, out of retries"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 751
    const/4 v0, 0x0

    return v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 745
    return-void
.end method
