.class public final Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;
.super Ljava/lang/Object;
.source "ApplicationInterfaceImpl.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/VlingoApplicationInterface;


# static fields
.field private static instance:Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;->instance:Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;->instance:Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;

    invoke-direct {v0}, Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;->instance:Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;

    .line 23
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;->instance:Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;

    return-object v0
.end method


# virtual methods
.method public isAppInForeground()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method

.method public startMainActivity()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public startMainService(Z)V
    .locals 0
    .param p1, "closeApplication"    # Z

    .prologue
    .line 44
    return-void
.end method
