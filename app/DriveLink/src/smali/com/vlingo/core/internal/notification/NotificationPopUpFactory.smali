.class public Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;
.super Ljava/lang/Object;
.source "NotificationPopUpFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/notification/NotificationPopUpFactory$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method


# virtual methods
.method public getNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/vlingo/core/internal/notification/NotificationPopUp;
    .locals 3
    .param p1, "type"    # Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;
    .param p2, "version"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "notificationText"    # Ljava/lang/String;
    .param p5, "negativeButton"    # Ljava/lang/String;
    .param p6, "positiveButton"    # Ljava/lang/String;
    .param p7, "exitOnDecline"    # Z
    .param p8, "actionRequired"    # Z

    .prologue
    const/4 v2, 0x0

    .line 7
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpFactory$1;->$SwitchMap$com$vlingo$core$internal$notification$NotificationPopUp$Type:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 15
    :pswitch_0
    return-object v2

    .line 7
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
