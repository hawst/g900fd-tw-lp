.class Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;
.super Lcom/vlingo/core/internal/contacts/normalizers/RegexContactNameNormalizer;
.source "SetMatchContactScore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SpecificAbbreviationNormalizer"
.end annotation


# static fields
.field private static final MAPPINGS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static canNormalizePattern:Ljava/util/regex/Pattern;

.field private static canNormalizeStr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 20
    sput-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->canNormalizeStr:Ljava/lang/String;

    .line 21
    sput-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    .line 25
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->MAPPINGS:Ljava/util/Map;

    .line 26
    sget-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Sig.na|Sig.ra)\\b"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "Signora"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Sr.\u00aa)\\b"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "Sr\u00aa"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Dr.\u00aa)\\b"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "Dr\u00aa"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Prof.ssa)\\b"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "Professoressa"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/normalizers/RegexContactNameNormalizer;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$1;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;-><init>()V

    return-void
.end method

.method private static createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 38
    sget-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->canNormalizeStr:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->canNormalizeStr:Ljava/lang/String;

    .line 43
    :goto_0
    const/4 v0, 0x2

    invoke-static {p0, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0

    .line 41
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->canNormalizeStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->canNormalizeStr:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected getCanNormalizePattern()Ljava/util/regex/Pattern;
    .locals 2

    .prologue
    .line 48
    sget-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    if-nez v0, :cond_0

    .line 49
    sget-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->canNormalizeStr:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    .line 51
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method protected getPatterns()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    sget-object v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore$SpecificAbbreviationNormalizer;->MAPPINGS:Ljava/util/Map;

    return-object v0
.end method
