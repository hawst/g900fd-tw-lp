.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;
.super Ljava/lang/Object;
.source "PhraseSpotter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updatePhraseSpotterState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 216
    iget-object v7, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    monitor-enter v7

    .line 217
    :try_start_0
    const-string/jumbo v8, "car_word_spotter_enabled"

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 218
    .local v3, "isEnabled":Z
    const-string/jumbo v8, "car_word_spotter_when_charging_only"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 219
    .local v4, "isOnlyWhenCharging":Z
    const-string/jumbo v8, "showing_notifications"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 220
    .local v5, "showingNotifications":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v2

    .line 221
    .local v2, "isBTConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v1

    .line 223
    .local v1, "checkPhoneInUse":Z
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$200()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "updatePhraseSpotterState() spottingRequested="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->spottingRequested:Z
    invoke-static {v10}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", isEnabled="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", isBTConnected="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", isOnlyWhenCharging="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", isCharging="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging:Z
    invoke-static {v10}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", isShowingNotifications="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    if-eqz v3, :cond_1

    if-nez v2, :cond_1

    if-eqz v4, :cond_0

    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging:Z
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->recoInProgress:Z
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 227
    .local v0, "canBeTurnedOn":Z
    :goto_0
    if-eqz v0, :cond_2

    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->spottingRequested:Z
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-nez v5, :cond_2

    if-nez v1, :cond_2

    .line 231
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    const/4 v8, 0x1

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->startPhraseSpottingInternal(Z)V
    invoke-static {v6, v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)V

    .line 238
    :goto_1
    monitor-exit v7

    .line 239
    return-void

    .end local v0    # "canBeTurnedOn":Z
    :cond_1
    move v0, v6

    .line 224
    goto :goto_0

    .line 236
    .restart local v0    # "canBeTurnedOn":Z
    :cond_2
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpottingInternal()V
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    goto :goto_1

    .line 238
    .end local v0    # "canBeTurnedOn":Z
    .end local v1    # "checkPhoneInUse":Z
    .end local v2    # "isBTConnected":Z
    .end local v3    # "isEnabled":Z
    .end local v4    # "isOnlyWhenCharging":Z
    .end local v5    # "showingNotifications":Z
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method
