.class public Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;
.super Ljava/lang/Object;
.source "ServiceBindingUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/ServiceBindingUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ServiceToken"
.end annotation


# instance fields
.field public interfaceClass:Ljava/lang/Class;

.field public mWrappedContext:Landroid/content/ContextWrapper;


# direct methods
.method public constructor <init>(Landroid/content/ContextWrapper;Ljava/lang/Class;)V
    .locals 0
    .param p1, "context"    # Landroid/content/ContextWrapper;
    .param p2, "interfaceClass"    # Ljava/lang/Class;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;->mWrappedContext:Landroid/content/ContextWrapper;

    .line 30
    iput-object p2, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;->interfaceClass:Ljava/lang/Class;

    .line 31
    return-void
.end method
