.class Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;
.super Ljava/lang/Object;
.source "UserDataManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;

.field final synthetic val$errorMessage:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;->this$0:Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;

    iput-object p2, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;->val$errorMessage:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;->val$errorMessage:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isBlank(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;->this$0:Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;

    # getter for: Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->callback:Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;
    invoke-static {v0}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->access$000(Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;)Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;->this$0:Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;

    # getter for: Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->callback:Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;
    invoke-static {v0}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->access$000(Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;)Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;->val$errorMessage:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;->onFailure(Ljava/lang/String;)V

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;->this$0:Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;

    # getter for: Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->callback:Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;
    invoke-static {v0}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->access$000(Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;)Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;->this$0:Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;

    # getter for: Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->callback:Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;
    invoke-static {v0}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->access$000(Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;)Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

    move-result-object v0

    new-instance v1, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1$1;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1$1;-><init>(Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;)V

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;->onSuccess(Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;)V

    goto :goto_0
.end method
