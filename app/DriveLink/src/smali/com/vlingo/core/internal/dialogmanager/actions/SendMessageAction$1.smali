.class Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction$1;
.super Ljava/lang/Object;
.source "SendMessageAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->sendSMS(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSMSSent(ZI)V
    .locals 2
    .param p1, "error"    # Z
    .param p2, "errCode"    # I

    .prologue
    .line 60
    if-nez p1, :cond_0

    .line 61
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->access$000(Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->access$200(Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->errString(I)Ljava/lang/String;
    invoke-static {v1, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->access$100(Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method
