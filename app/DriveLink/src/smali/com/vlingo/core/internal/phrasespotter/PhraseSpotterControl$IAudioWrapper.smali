.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;
.super Ljava/lang/Object;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IAudioWrapper"
.end annotation


# instance fields
.field private logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dumpToFile()V
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->dumpToFile()V

    :cond_0
    return-void
.end method

.method public hasTarget()Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public markDecisionSample(Z)V
    .locals 1
    .param p1, "seamless"    # Z

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->markDecisionSample(Z)V

    :cond_0
    return-void
.end method

.method public markP()V
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->markP()V

    :cond_0
    return-void
.end method

.method public markS()V
    .locals 1

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->markS()V

    :cond_0
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    .line 134
    return-void
.end method

.method public setLogger(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;)V
    .locals 0
    .param p1, "logger"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    .line 140
    return-void
.end method

.method public setScore(F)V
    .locals 1
    .param p1, "score"    # F

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->setScore(F)V

    :cond_0
    return-void
.end method

.method public target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    return-object v0
.end method

.method public writeData([BII)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->writeData([BII)V

    :cond_0
    return-void
.end method

.method public writeData([SII)V
    .locals 1
    .param p1, "data"    # [S
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->writeData([SII)V

    :cond_0
    return-void
.end method
