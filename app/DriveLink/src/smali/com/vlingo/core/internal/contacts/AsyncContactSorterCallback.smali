.class public interface abstract Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;
.super Ljava/lang/Object;
.source "AsyncContactSorterCallback.java"


# virtual methods
.method public abstract onAsyncSortingFailed()V
.end method

.method public abstract onAsyncSortingFinished(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onAsyncSortingUpdated(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation
.end method
