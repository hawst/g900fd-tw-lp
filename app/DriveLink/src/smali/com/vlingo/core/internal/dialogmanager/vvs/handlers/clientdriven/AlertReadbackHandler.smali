.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/AlertReadbackHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "AlertReadbackHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v5, 0x0

    .line 32
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/AlertReadbackHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v2

    .line 34
    .local v2, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAllNewAlerts(I)Ljava/util/LinkedList;

    move-result-object v0

    .line 40
    .local v0, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-static {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->createSMSMMSSenderQueueMap(Ljava/util/LinkedList;)Ljava/util/HashMap;

    move-result-object v3

    .line 41
    .local v3, "senderQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    const-string/jumbo v4, "Contact"

    invoke-static {p1, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "contactName":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 43
    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getMessageQueueByContactName(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v3

    :cond_0
    move-object v4, v2

    .line 47
    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    invoke-virtual {v4, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->setCurrentContactSearch(Ljava/lang/String;)V

    move-object v4, v2

    .line 48
    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    invoke-virtual {v4, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->setSenderQueue(Ljava/util/HashMap;)V

    .line 49
    invoke-virtual {v2, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v4

    return v4
.end method
