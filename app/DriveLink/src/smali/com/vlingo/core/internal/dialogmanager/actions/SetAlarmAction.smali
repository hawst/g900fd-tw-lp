.class public Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SetAlarmAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;


# instance fields
.field private time:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method public alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;
    .locals 1
    .param p1, "alarm"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 31
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getTimeCanonical()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;->time:Ljava/lang/String;

    .line 32
    return-object p0
.end method

.method public bridge synthetic alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;->alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;

    move-result-object v0

    return-object v0
.end method

.method protected execute()V
    .locals 17

    .prologue
    .line 37
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;->time:Ljava/lang/String;

    invoke-static {v13}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 38
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v13

    sget-object v14, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_default_alarm_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v13, v14}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "defaultAlarmTitle":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v13, "android.intent.action.SET_ALARM"

    invoke-direct {v3, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 41
    .local v3, "i":Landroid/content/Intent;
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 42
    .local v5, "t":Landroid/text/format/Time;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;->time:Ljava/lang/String;

    const/4 v14, 0x1

    invoke-static {v13, v5, v14}, Lcom/vlingo/core/internal/schedule/DateUtil;->getTimeFromTimeString(Ljava/lang/String;Landroid/text/format/Time;Z)J

    move-result-wide v6

    .line 44
    .local v6, "timeMillis":J
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v10

    .line 45
    .local v10, "tz":Ljava/util/TimeZone;
    invoke-virtual {v10, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v11

    .line 46
    .local v11, "tzOffset":I
    int-to-long v13, v11

    add-long/2addr v13, v6

    const-wide/16 v15, 0x3e8

    div-long v8, v13, v15

    .line 47
    .local v8, "timeWithOffset":J
    const-wide/16 v13, 0xe10

    rem-long v13, v8, v13

    const-wide/16 v15, 0x3c

    div-long/2addr v13, v15

    long-to-int v4, v13

    .line 48
    .local v4, "minutes":I
    const-wide/16 v13, 0xe10

    div-long v13, v8, v13

    long-to-int v2, v13

    .line 50
    .local v2, "hours":I
    const-string/jumbo v13, "android.intent.extra.alarm.MESSAGE"

    invoke-virtual {v3, v13, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const-string/jumbo v13, "android.intent.extra.alarm.HOUR"

    invoke-virtual {v3, v13, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 52
    const-string/jumbo v13, "android.intent.extra.alarm.MINUTES"

    invoke-virtual {v3, v13, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 53
    sget-object v13, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 54
    .local v12, "ver":I
    const/16 v13, 0xb

    if-lt v12, v13, :cond_0

    .line 55
    const-string/jumbo v13, "android.intent.extra.alarm.SKIP_UI"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 57
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 58
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v13

    invoke-interface {v13}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 64
    .end local v1    # "defaultAlarmTitle":Ljava/lang/String;
    .end local v2    # "hours":I
    .end local v3    # "i":Landroid/content/Intent;
    .end local v4    # "minutes":I
    .end local v5    # "t":Landroid/text/format/Time;
    .end local v6    # "timeMillis":J
    .end local v8    # "timeWithOffset":J
    .end local v10    # "tz":Ljava/util/TimeZone;
    .end local v11    # "tzOffset":I
    .end local v12    # "ver":I
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;->getContext()Landroid/content/Context;

    move-result-object v13

    const-string/jumbo v14, "execute(), no time so not sending"

    const/4 v15, 0x1

    invoke-static {v13, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
