.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;
.super Ljava/lang/Object;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Recording"
.end annotation


# instance fields
.field private logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

.field private mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)V
    .locals 3
    .param p1, "audioSourceType"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .prologue
    .line 168
    const/4 v0, 0x0

    .line 169
    .local v0, "audioSessionId":I
    const/4 v1, 0x0

    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->PHRASESPOTTING:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    invoke-static {v1, v2, p1, v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->request(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;I)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 172
    return-void
.end method

.method public drop()V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    return-void
.end method

.method public isRecording()Z
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStereo()Z
    .locals 2

    .prologue
    .line 177
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->getChannelConfig()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public read([SII)I
    .locals 1
    .param p1, "buffer"    # [S
    .param p2, "total"    # I
    .param p3, "remaining"    # I

    .prologue
    .line 175
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->read([SII)I

    move-result v0

    return v0
.end method

.method public setLogger(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;)V
    .locals 0
    .param p1, "logger"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    return-void
.end method

.method public stop()V
    .locals 4

    .prologue
    .line 184
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IAudioWrapper;->dumpToFile()V

    .line 189
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 196
    :cond_0
    return-void

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "PhraseSpotter VLG_EXCEPTION-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public target()Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$Recording;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    return-object v0
.end method
