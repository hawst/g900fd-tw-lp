.class final Lcom/vlingo/core/internal/util/SayHello$1;
.super Ljava/lang/Object;
.source "SayHello.java"

# interfaces
.implements Lcom/vlingo/sdk/services/VLServicesListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/util/SayHello;->sendHello()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/vlingo/sdk/services/VLServicesErrors;Ljava/lang/String;)V
    .locals 1
    .param p1, "error"    # Lcom/vlingo/sdk/services/VLServicesErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/core/internal/VlingoAndroidCore;->preventSdkReload(Z)V

    .line 77
    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "actionList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    const/4 v1, 0x0

    .line 69
    new-instance v0, Lcom/vlingo/core/internal/util/SayHello$VVSListener;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/util/SayHello$VVSListener;-><init>(Lcom/vlingo/core/internal/util/SayHello$1;)V

    .line 70
    .local v0, "listener":Lcom/vlingo/core/internal/util/SayHello$VVSListener;
    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->processActionList(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)I

    .line 71
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/core/internal/VlingoAndroidCore;->preventSdkReload(Z)V

    .line 72
    return-void
.end method
