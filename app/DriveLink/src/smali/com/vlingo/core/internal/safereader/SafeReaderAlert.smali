.class public Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
.super Ljava/lang/Object;
.source "SafeReaderAlert.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x23975ed1723f8aafL


# instance fields
.field protected address:Ljava/lang/String;

.field protected id:J

.field protected senderDisplayName:Ljava/lang/String;

.field protected timeStamp:J

.field protected type:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 2
    .param p1, "id"    # J
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "senderDisplayName"    # Ljava/lang/String;
    .param p5, "timeStamp"    # J
    .param p7, "type"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->id:J

    .line 21
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->address:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->senderDisplayName:Ljava/lang/String;

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->timeStamp:J

    .line 24
    const-string/jumbo v0, "SafeReaderAlert"

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->type:Ljava/lang/String;

    .line 36
    iput-wide p1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->id:J

    .line 37
    iput-object p3, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->address:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->senderDisplayName:Ljava/lang/String;

    .line 39
    iput-wide p5, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->timeStamp:J

    .line 40
    iput-object p7, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->type:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)I
    .locals 6
    .param p1, "alert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getTimeStamp()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getTimeStamp()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 60
    .local v0, "diff":J
    long-to-int v2, v0

    return v2
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 14
    check-cast p1, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->compareTo(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 45
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 48
    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 49
    .local v0, "msg":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    iget-wide v2, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->id:J

    iget-wide v4, v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->id:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 52
    iget-object v2, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->type:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->type:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 55
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 52
    :cond_3
    iget-object v2, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->type:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->id:J

    return-wide v0
.end method

.method public getSenderDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->senderDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->timeStamp:J

    return-wide v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->type:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    return v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->address:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 81
    iput-wide p1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->id:J

    .line 82
    return-void
.end method

.method public setSenderDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "senderDisplayName"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->senderDisplayName:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setTimeStamp(J)V
    .locals 0
    .param p1, "timeStamp"    # J

    .prologue
    .line 126
    iput-wide p1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->timeStamp:J

    .line 127
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->type:Ljava/lang/String;

    .line 142
    return-void
.end method
