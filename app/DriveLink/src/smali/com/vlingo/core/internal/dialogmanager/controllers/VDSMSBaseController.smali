.class public abstract Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;
.super Lcom/vlingo/core/internal/dialogmanager/Controller;
.source "VDSMSBaseController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$1;,
        Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;,
        Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;,
        Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;,
        Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;
    }
.end annotation


# static fields
.field protected static final PARAM_VIDEO_MODE:Ljava/lang/String; = "videophone"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected action:Lcom/vlingo/sdk/recognition/VLAction;

.field protected address:Ljava/lang/String;

.field protected callMode:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

.field protected contactMatcher:Lcom/vlingo/core/internal/contacts/ContactMatcher;

.field protected contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field protected contactSelectedName:Ljava/lang/String;

.field private displayedContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private displayedPhoneInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private displayedPhoneLabels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private executed:Z

.field protected message:Ljava/lang/String;

.field protected ordinal:Ljava/lang/String;

.field protected phoneType:Ljava/lang/String;

.field protected query:Ljava/lang/String;

.field protected state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;-><init>()V

    .line 70
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z

    .line 79
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneInfo:Ljava/util/Map;

    .line 80
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneLabels:Ljava/util/List;

    .line 81
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedContacts:Ljava/util/List;

    .line 85
    const-string/jumbo v0, "call"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    .line 86
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->VOICECALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->callMode:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    .line 87
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->ordinal:Ljava/lang/String;

    .line 675
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 48
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method private clearState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 493
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 494
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z

    .line 495
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    .line 496
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 497
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactMatcher:Lcom/vlingo/core/internal/contacts/ContactMatcher;

    .line 498
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneInfo:Ljava/util/Map;

    .line 499
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneLabels:Ljava/util/List;

    .line 500
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedContacts:Ljava/util/List;

    .line 501
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SELECTED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 502
    return-void
.end method

.method private exists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 463
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getAddrBookEmptyPrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 753
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_address_book_is_empty:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getAutoDial()I
    .locals 4

    .prologue
    .line 606
    const/4 v0, 0x0

    .line 608
    .local v0, "autoDialType":I
    const-string/jumbo v2, "auto_dial"

    const-string/jumbo v3, "confident"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 610
    .local v1, "autodial":Ljava/lang/String;
    const-string/jumbo v2, "always"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 611
    const/4 v0, 0x2

    .line 617
    :cond_0
    :goto_0
    return v0

    .line 612
    :cond_1
    const-string/jumbo v2, "confident"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 613
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getParameter(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p1, "parameter"    # Ljava/lang/String;
    .param p2, "required"    # Z

    .prologue
    .line 485
    const-string/jumbo v0, "To"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->action:Lcom/vlingo/sdk/recognition/VLAction;

    invoke-static {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->removePossessives(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 488
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->action:Lcom/vlingo/sdk/recognition/VLAction;

    invoke-static {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private handleLPAction()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 467
    const-string/jumbo v2, "Action"

    invoke-direct {p0, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 468
    .local v0, "lpAction":Ljava/lang/String;
    const-string/jumbo v2, "send"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 469
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->message:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 470
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedContact()V

    .line 476
    :cond_0
    const-string/jumbo v1, "cancel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 477
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 478
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 479
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->reset()V

    .line 481
    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 472
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->sendAction()V

    goto :goto_0
.end method


# virtual methods
.method protected actionDefault()V
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 230
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->sendAction()V

    .line 231
    return-void
.end method

.method protected abstract checkHistory(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z
.end method

.method protected checkMessageType(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z
    .locals 1
    .param p1, "item"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 270
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->checkMessageType(Lcom/vlingo/core/internal/contacts/ContactMatch;Z)Z

    move-result v0

    return v0
.end method

.method protected checkMessageType(Lcom/vlingo/core/internal/contacts/ContactMatch;Z)Z
    .locals 6
    .param p1, "item"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "promptForType"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 275
    iget-boolean v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z

    if-eqz v5, :cond_1

    move v3, v4

    .line 338
    :cond_0
    :goto_0
    return v3

    .line 280
    :cond_1
    if-eqz p1, :cond_0

    .line 283
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v5

    if-nez v5, :cond_2

    .line 285
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getNoContactPrompt()Ljava/lang/String;

    move-result-object v2

    .line 286
    .local v2, "prompt":Ljava/lang/String;
    invoke-virtual {p0, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 291
    .end local v2    # "prompt":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v4, :cond_3

    .line 294
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->address:Ljava/lang/String;

    .line 295
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedMessage()V

    move v3, v4

    .line 296
    goto :goto_0

    .line 300
    :cond_3
    const-string/jumbo v5, "use_default_phone"

    invoke-static {v5, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 301
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/util/DialUtil;->getDefaultNumber(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 302
    .local v1, "defaultAddress":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 303
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->address:Ljava/lang/String;

    .line 304
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedMessage()V

    move v3, v4

    .line 305
    goto :goto_0

    .line 308
    .end local v1    # "defaultAddress":Ljava/lang/String;
    :cond_4
    if-nez p2, :cond_5

    .line 309
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->address:Ljava/lang/String;

    .line 310
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedMessage()V

    move v3, v4

    .line 311
    goto :goto_0

    .line 314
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleTypeMatch()Ljava/util/Map;

    move-result-object v0

    .line 315
    .local v0, "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-nez v0, :cond_6

    move v3, v4

    .line 317
    goto :goto_0

    .line 320
    :cond_6
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->checkHistory(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z

    move-result v5

    if-eqz v5, :cond_7

    move v3, v4

    .line 322
    goto :goto_0

    .line 334
    :cond_7
    if-eqz p2, :cond_0

    .line 335
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v4

    const-string/jumbo v5, "car-voicedial-phonetype"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 336
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->promptForMessageType(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public clone()Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;->clone()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    .line 98
    .local v0, "clone":Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-nez v1, :cond_0

    move-object v1, v2

    :goto_0
    iput-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 99
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneInfo:Ljava/util/Map;

    if-nez v1, :cond_1

    move-object v1, v2

    :goto_1
    iput-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneInfo:Ljava/util/Map;

    .line 100
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneLabels:Ljava/util/List;

    if-nez v1, :cond_2

    move-object v1, v2

    :goto_2
    iput-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneLabels:Ljava/util/List;

    .line 101
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedContacts:Ljava/util/List;

    if-nez v1, :cond_3

    :goto_3
    iput-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedContacts:Ljava/util/List;

    .line 103
    return-object v0

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->clone()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    goto :goto_0

    .line 99
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneInfo:Ljava/util/Map;

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/ContactData;->clone(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    goto :goto_1

    .line 100
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneLabels:Ljava/util/List;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_2

    .line 101
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedContacts:Ljava/util/List;

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    goto :goto_3
.end method

.method public bridge synthetic clone()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->clone()Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->clone()Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    move-result-object v0

    return-object v0
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 11
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 349
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 350
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->action:Lcom/vlingo/sdk/recognition/VLAction;

    .line 352
    if-nez p1, :cond_0

    move v5, v6

    .line 459
    :goto_0
    return v5

    .line 356
    :cond_0
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v8, "LPAction"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 357
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleLPAction()Z

    move-result v5

    goto :goto_0

    .line 360
    :cond_1
    const-string/jumbo v5, "To"

    invoke-direct {p0, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    .line 361
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->exists(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 362
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->clearState()V

    .line 365
    :cond_2
    const-string/jumbo v5, "Mode"

    invoke-direct {p0, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 366
    .local v2, "mode":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->exists(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 367
    const-string/jumbo v5, "videophone"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 368
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isVideoCallingSupported()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 369
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->VOICEVIDEOCALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->callMode:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    .line 381
    :cond_3
    :goto_1
    const-string/jumbo v5, "Message"

    invoke-direct {p0, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 382
    .local v1, "messageParam":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->exists(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 383
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->message:Ljava/lang/String;

    .line 386
    :cond_4
    const-string/jumbo v5, "Which"

    invoke-static {p1, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->ordinal:Ljava/lang/String;

    .line 388
    const-string/jumbo v5, "PhoneType"

    invoke-direct {p0, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 389
    .local v3, "phoneTypeParam":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->exists(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 390
    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    .line 391
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->checkMessageType(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z

    .line 401
    :cond_5
    :goto_2
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$1;->$SwitchMap$com$vlingo$core$internal$dialogmanager$controllers$VDSMSBaseController$State:[I

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->ordinal()I

    move-result v8

    aget v5, v5, v8

    packed-switch v5, :pswitch_data_0

    :cond_6
    :goto_3
    move v5, v6

    .line 459
    goto/16 :goto_0

    .line 377
    .end local v1    # "messageParam":Ljava/lang/String;
    .end local v3    # "phoneTypeParam":Ljava/lang/String;
    :cond_7
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Unexpected Mode Parameter value \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 392
    .restart local v1    # "messageParam":Ljava/lang/String;
    .restart local v3    # "phoneTypeParam":Ljava/lang/String;
    :cond_8
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->ordinal:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 393
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->ordinal:Ljava/lang/String;

    invoke-static {v5, v8}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElement(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 394
    .local v0, "element":Ljava/lang/Object;
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->ordinal:Ljava/lang/String;

    if-eqz v5, :cond_5

    if-eqz v0, :cond_5

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_5

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->exists(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 395
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 396
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    .line 397
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->checkMessageType(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z

    goto :goto_2

    .line 403
    .end local v0    # "element":Ljava/lang/Object;
    :pswitch_0
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->exists(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 404
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    const-string/jumbo v8, "[\\d|-|\\ ]+"

    invoke-virtual {v5, v8}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 405
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->address:Ljava/lang/String;

    .line 406
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedMessage()V

    goto :goto_3

    .line 410
    :cond_9
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SELECTED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v5, v6, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 411
    sget-object v5, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->matchContacts(Lcom/vlingo/core/internal/contacts/ContactType;)V

    move v5, v7

    .line 412
    goto/16 :goto_0

    .line 415
    :cond_a
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedContact()V

    goto/16 :goto_3

    .line 419
    :pswitch_1
    const-string/jumbo v5, "To"

    invoke-direct {p0, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 420
    .local v4, "plus":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v5, :cond_b

    .line 421
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    .line 422
    iput-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    .line 423
    sget-object v5, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->matchContacts(Lcom/vlingo/core/internal/contacts/ContactType;)V

    move v5, v7

    .line 424
    goto/16 :goto_0

    .line 425
    :cond_b
    if-eqz v4, :cond_6

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-nez v5, :cond_6

    .line 426
    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    .line 427
    sget-object v5, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->matchContacts(Lcom/vlingo/core/internal/contacts/ContactType;)V

    move v5, v7

    .line 428
    goto/16 :goto_0

    .line 432
    .end local v4    # "plus":Ljava/lang/String;
    :pswitch_2
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->exists(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 437
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedMessage()V

    goto/16 :goto_3

    .line 439
    :cond_c
    sget-object v5, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->matchContacts(Lcom/vlingo/core/internal/contacts/ContactType;)V

    move v5, v7

    .line 440
    goto/16 :goto_0

    .line 448
    :pswitch_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedMessage()V

    goto/16 :goto_3

    .line 455
    :pswitch_4
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->promptForConfirmation()V

    goto/16 :goto_3

    .line 401
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected findKey(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 236
    .local p1, "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 237
    .local v5, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 238
    .local v4, "k":Ljava/lang/String;
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 239
    .local v0, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    const-string/jumbo v7, "\\D"

    const-string/jumbo v8, ""

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 240
    .local v6, "numericValue":Ljava/lang/String;
    iget v7, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-static {v7}, Lcom/vlingo/core/internal/util/ContactUtil;->getTypeStringEN(I)Ljava/lang/String;

    move-result-object v1

    .line 241
    .local v1, "englishType":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 242
    .local v2, "formattedKey":Ljava/lang/String;
    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 246
    .end local v0    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v1    # "englishType":Ljava/lang/String;
    .end local v2    # "formattedKey":Ljava/lang/String;
    .end local v4    # "k":Ljava/lang/String;
    .end local v6    # "numericValue":Ljava/lang/String;
    :goto_0
    return-object v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected getAddressTypes()[I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 666
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 672
    :cond_0
    :goto_0
    return-object v3

    .line 669
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getCapability()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v0, v1, :cond_0

    .line 670
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "capability "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getCapability()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " unrecognized as an address type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected abstract getCapability()Lcom/vlingo/core/internal/contacts/ContactType;
.end method

.method protected abstract getContactPrompt()Ljava/lang/String;
.end method

.method protected getEmailTypes()[I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 656
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 662
    :cond_0
    :goto_0
    return-object v3

    .line 659
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getCapability()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v0, v1, :cond_0

    .line 660
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "capability "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getCapability()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " unrecognized as an email type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected getFieldID(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 1
    .param p1, "id"    # Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getFieldIds()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    return-object v0
.end method

.method protected abstract getFieldIds()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getNoContactPrompt()Ljava/lang/String;
.end method

.method protected getPhoneTypes()[I
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 621
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 642
    :cond_0
    :goto_0
    return-object v0

    .line 624
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getCapability()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v1, v2, :cond_0

    .line 625
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    const-string/jumbo v2, "work"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 626
    new-array v0, v3, [I

    const/4 v1, 0x3

    aput v1, v0, v4

    goto :goto_0

    .line 628
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    const-string/jumbo v2, "mobile"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 629
    new-array v0, v3, [I

    const/4 v1, 0x2

    aput v1, v0, v4

    goto :goto_0

    .line 631
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    const-string/jumbo v2, "home"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 632
    new-array v0, v3, [I

    aput v3, v0, v4

    goto :goto_0

    .line 634
    :cond_4
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    const-string/jumbo v2, "other"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 635
    new-array v0, v3, [I

    const/4 v1, 0x7

    aput v1, v0, v4

    goto :goto_0
.end method

.method protected getSocialTypes()[I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 646
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 652
    :cond_0
    :goto_0
    return-object v3

    .line 649
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getCapability()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->FACEBOOK:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v0, v1, :cond_0

    .line 650
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "capability "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getCapability()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " unrecognized as a social type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected abstract getType()Ljava/lang/String;
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 186
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 187
    .local v1, "extras":Landroid/os/Bundle;
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.ContactChoice"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 188
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 190
    const-string/jumbo v4, "choice"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v4, v5, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 192
    const-string/jumbo v3, "choice"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 193
    .local v2, "index":I
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedContacts:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 194
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    .line 195
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->checkMessageType(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z

    .line 196
    new-instance v3, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    sget-object v5, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->DIAL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    invoke-direct {v3, v4, v5}, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;-><init>(Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 226
    .end local v2    # "index":I
    .end local p2    # "object":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 198
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 199
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 201
    const-string/jumbo v3, "choice"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 202
    const-string/jumbo v3, "choice"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 203
    .restart local v2    # "index":I
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneLabels:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    .line 204
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneInfo:Ljava/util/Map;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 205
    .local v0, "data":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->address:Ljava/lang/String;

    .line 206
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedMessage()V

    .line 207
    new-instance v3, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->address:Ljava/lang/String;

    sget-object v5, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->DIAL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    invoke-direct {v3, v4, v5}, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;-><init>(Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    goto :goto_0

    .line 209
    .end local v0    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v2    # "index":I
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 210
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 211
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 212
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->reset()V

    goto :goto_0

    .line 213
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 214
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->actionDefault()V

    goto/16 :goto_0

    .line 215
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.BodyChange"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 216
    const-string/jumbo v4, "message"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 218
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishTurn()V

    .line 219
    const-string/jumbo v3, "message"

    invoke-static {p1, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamString(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->message:Ljava/lang/String;

    goto/16 :goto_0

    .line 221
    :cond_5
    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->message:Ljava/lang/String;

    goto/16 :goto_0

    .line 223
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.DataTransfered"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 224
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    instance-of v5, p2, Ljava/lang/Integer;

    if-eqz v5, :cond_7

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_7
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v4, v3}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;I)V

    goto/16 :goto_0
.end method

.method protected handleNeedContact()V
    .locals 6

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SELECTED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 513
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->action:Lcom/vlingo/sdk/recognition/VLAction;

    const-string/jumbo v4, "Action"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 514
    .local v0, "actionType":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "redial"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 531
    :goto_0
    return-void

    .line 517
    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-nez v3, :cond_2

    .line 518
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 519
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getContactPrompt()Ljava/lang/String;

    move-result-object v2

    .line 520
    .local v2, "msg":Ljava/lang/String;
    const/4 v1, 0x1

    .line 521
    .local v1, "autoListen":Z
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactMatcher:Lcom/vlingo/core/internal/contacts/ContactMatcher;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactMatcher:Lcom/vlingo/core/internal/contacts/ContactMatcher;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactMatcher;->isAddressBookEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 522
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getAddrBookEmptyPrompt()Ljava/lang/String;

    move-result-object v2

    .line 523
    const/4 v1, 0x0

    .line 525
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;->CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getFieldID(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {p0, v2, v2, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 526
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v3

    const-string/jumbo v4, "car-voicedial-contact"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    goto :goto_0

    .line 528
    .end local v1    # "autoListen":Z
    .end local v2    # "msg":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    .line 529
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->checkMessageType(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z

    goto :goto_0
.end method

.method protected abstract handleNeedMessage()V
.end method

.method protected handleTypeMatch()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v2

    .line 251
    .local v2, "phoneList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v0, 0x0

    .line 252
    .local v0, "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 253
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    invoke-static {v3, v2, v4}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeMap(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 255
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string/jumbo v4, "call"

    invoke-static {v3, v2, v4}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeMap(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 259
    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    invoke-virtual {p0, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->findKey(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 260
    .local v1, "key":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 261
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->address:Ljava/lang/String;

    .line 262
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedMessage()V

    .line 263
    const/4 v3, 0x0

    .line 266
    .end local v1    # "key":Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_1
    move-object v3, v0

    goto :goto_0
.end method

.method protected isVideoCall()Z
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->callMode:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->VOICEVIDEOCALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected matchContacts(Lcom/vlingo/core/internal/contacts/ContactType;)V
    .locals 13
    .param p1, "contactType"    # Lcom/vlingo/core/internal/contacts/ContactType;

    .prologue
    .line 591
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    .line 592
    .local v1, "context":Landroid/content/Context;
    iget-object v12, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    .line 593
    .local v12, "localQuery":Ljava/lang/String;
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 594
    iget-object v12, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    .line 597
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v11, v0

    check-cast v11, Ljava/util/List;

    .line 600
    .local v11, "disambiguationList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatcher;

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;-><init>(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)V

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getPhoneTypes()[I

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getEmailTypes()[I

    move-result-object v5

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getSocialTypes()[I

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getAddressTypes()[I

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    const/high16 v9, 0x42c80000    # 100.0f

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne p1, v3, :cond_1

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getAutoDial()I

    move-result v10

    :goto_0
    move-object v3, p1

    invoke-direct/range {v0 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatcher;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactMatcher:Lcom/vlingo/core/internal/contacts/ContactMatcher;

    .line 603
    return-void

    .line 600
    :cond_1
    const/4 v10, 0x0

    goto :goto_0
.end method

.method protected abstract promptForConfirmation()V
.end method

.method protected promptForMessageType(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 534
    .local p1, "phoneInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    const-string/jumbo v1, "call"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->findKey(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT_REFINEMENT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 540
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_phone_numbers2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;->TYPE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getFieldID(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 542
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneInfo:Ljava/util/Map;

    .line 543
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneLabels:Ljava/util/List;

    .line 544
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneLabels:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    .line 545
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactTypeChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 546
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "car-voicedial-phonetype"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 547
    return-void

    .line 537
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_TYPE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    goto :goto_0
.end method

.method protected promptForRefineContact(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 550
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v1, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 551
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT_REFINEMENT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 552
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_contacts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;->CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getFieldID(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 555
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedContacts:Ljava/util/List;

    .line 556
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddressBook:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 557
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "car-voicedial-contact"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 558
    return-void
.end method

.method protected abstract sendAction()V
.end method

.method protected showCallContact(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 561
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v1, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 562
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 563
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedContacts:Ljava/util/List;

    .line 564
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddressBook:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 565
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "car-voicedial-contact"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 566
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 107
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .local v4, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v6, "VDSMSBaseController{"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "executed="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "address="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->address:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "contactSelectedName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "contactSelected="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "contactMatcher="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactMatcher:Lcom/vlingo/core/internal/contacts/ContactMatcher;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    const-string/jumbo v6, "Map<String, ContactData>{"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneInfo:Ljava/util/Map;

    if-eqz v6, :cond_0

    .line 134
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneInfo:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 135
    .local v0, "es":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 136
    .local v2, "key":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 137
    .local v5, "value":Lcom/vlingo/core/internal/contacts/ContactData;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "key="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "value="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/vlingo/core/internal/contacts/ContactData;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 141
    .end local v0    # "es":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "key":Ljava/lang/String;
    .end local v5    # "value":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_0
    const-string/jumbo v6, "null"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_1
    const-string/jumbo v6, "}"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string/jumbo v6, "List<String>displayedPhoneLabels{"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneLabels:Ljava/util/List;

    if-eqz v6, :cond_2

    .line 147
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedPhoneLabels:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 148
    .local v3, "s":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 151
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "s":Ljava/lang/String;
    :cond_2
    const-string/jumbo v6, "null"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_3
    const-string/jumbo v6, "}"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const-string/jumbo v6, "List<ContactMatch> displayedContacts{"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedContacts:Ljava/util/List;

    if-eqz v6, :cond_4

    .line 157
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->displayedContacts:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 158
    .local v3, "s":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 161
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "s":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_4
    const-string/jumbo v6, "null"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_5
    const-string/jumbo v6, "}"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "message="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->message:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "query="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "phoneType="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->phoneType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "callMode="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->callMode:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    const-string/jumbo v6, "}"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method
