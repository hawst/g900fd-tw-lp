.class public abstract Lcom/vlingo/core/internal/audio/AudioFocusManager;
.super Ljava/lang/Object;
.source "AudioFocusManager.java"

# interfaces
.implements Lcom/vlingo/core/facade/audio/IAudioFocusManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/AudioFocusManager$1;,
        Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;,
        Lcom/vlingo/core/internal/audio/AudioFocusManager$EclairAudioFocusManager;
    }
.end annotation


# static fields
.field public static final ACTION_AUDIO_FOCUS_CHANGED:Ljava/lang/String; = "com.vlingo.client.app.action.AUDIO_FOCUS_CHANGED"

.field public static final EXTRA_FOCUS_CHANGE:Ljava/lang/String; = "com.vlingo.client.app.extra.FOCUS_CHANGE"

.field public static final EXTRA_FOCUS_CHANGE_FROM:Ljava/lang/String; = "com.vlingo.client.app.extra.FOCUS_CHANGE_FROM"

.field public static final FOCUS_TYPE_FULL:I = 0x1

.field public static final FOCUS_TYPE_TRANSIENT:I = 0x2

.field public static final FOCUS_TYPE_TRANSIENT_MAY_DUCK:I = 0x3

.field private static final LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static bAbandonInSync:Z

.field private static bHasFocus:Z

.field private static instance:Lcom/vlingo/core/internal/audio/AudioFocusManager;

.field private static isAudioFocusLossTransient:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    const-class v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->TAG:Ljava/lang/String;

    .line 48
    sput-boolean v1, Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z

    .line 49
    sput-boolean v1, Lcom/vlingo/core/internal/audio/AudioFocusManager;->isAudioFocusLossTransient:Z

    .line 50
    sput-boolean v1, Lcom/vlingo/core/internal/audio/AudioFocusManager;->bAbandonInSync:Z

    .line 52
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->instance:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    return-void
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 33
    sget-boolean v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->bAbandonInSync:Z

    return v0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 33
    sput-boolean p0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->bAbandonInSync:Z

    return p0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 33
    sget-boolean v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z

    return v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 33
    sput-boolean p0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z

    return p0
.end method

.method static synthetic access$700()Z
    .locals 1

    .prologue
    .line 33
    sget-boolean v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->isAudioFocusLossTransient:Z

    return v0
.end method

.method static synthetic access$702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 33
    sput-boolean p0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->isAudioFocusLossTransient:Z

    return p0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 33
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->notifyAudioFocusChangeToListeners(I)V

    return-void
.end method

.method public static addListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    .prologue
    .line 71
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const-class v1, Lcom/vlingo/core/internal/audio/AudioFocusManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->instance:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    if-nez v0, :cond_0

    .line 58
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->newInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->instance:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    .line 60
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->instance:Lcom/vlingo/core/internal/audio/AudioFocusManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static newInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    sget-object v2, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 81
    .local v1, "sdkVersion":I
    const/4 v0, 0x0

    .line 82
    .local v0, "detector":Lcom/vlingo/core/internal/audio/AudioFocusManager;
    const/16 v2, 0x8

    if-ge v1, v2, :cond_0

    .line 83
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioFocusManager$EclairAudioFocusManager;

    .end local v0    # "detector":Lcom/vlingo/core/internal/audio/AudioFocusManager;
    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager$EclairAudioFocusManager;-><init>(Lcom/vlingo/core/internal/audio/AudioFocusManager$1;)V

    .line 88
    .restart local v0    # "detector":Lcom/vlingo/core/internal/audio/AudioFocusManager;
    :goto_0
    return-object v0

    .line 85
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    .end local v0    # "detector":Lcom/vlingo/core/internal/audio/AudioFocusManager;
    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;-><init>(Landroid/content/Context;)V

    .restart local v0    # "detector":Lcom/vlingo/core/internal/audio/AudioFocusManager;
    goto :goto_0
.end method

.method private static notifyAudioFocusChangeToListeners(I)V
    .locals 3
    .param p0, "audioFocusState"    # I

    .prologue
    .line 64
    sget-object v2, Lcom/vlingo/core/internal/audio/AudioFocusManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    .line 65
    .local v1, "listener":Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;
    invoke-interface {v1, p0}, Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;->onAudioFocusChanged(I)V

    goto :goto_0

    .line 67
    .end local v1    # "listener":Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;
    :cond_0
    return-void
.end method

.method public static removeListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    .prologue
    .line 76
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 77
    return-void
.end method


# virtual methods
.method public abstract abandonAudioFocusInSync()V
.end method

.method public hasAudioFocus()Z
    .locals 1

    .prologue
    .line 97
    sget-boolean v0, Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z

    return v0
.end method

.method public abstract setAbandonInSync(Z)V
.end method

.method public abstract setNullTask()V
.end method

.method public abstract setTaskOnGetAudioFocus(IILjava/lang/Runnable;)V
.end method
