.class public interface abstract Lcom/vlingo/core/internal/weather/WeatherElementNames;
.super Ljava/lang/Object;
.source "WeatherElementNames.java"


# static fields
.field public static final CURRENT_CONDITION:Ljava/lang/String; = "CurrentCondition"

.field public static final CURRENT_CONDITIONS:Ljava/lang/String; = "CurrentCondition"

.field public static final DAYNIGHTTIME_FORECAST:Ljava/lang/String; = "DayNighttimeForecast"

.field public static final DAYTIME_FORECAST:Ljava/lang/String; = "DaytimeForecast"

.field public static final FORECAST:Ljava/lang/String; = "Forecast"

.field public static final FORECASTS:Ljava/lang/String; = "Forecasts"

.field public static final IMAGE:Ljava/lang/String; = "Images"

.field public static final IMAGES:Ljava/lang/String; = "Images"

.field public static final LOCATION:Ljava/lang/String; = "Location"

.field public static final MOON_PHASE:Ljava/lang/String; = "MoonPhase"

.field public static final MOON_PHASES:Ljava/lang/String; = "MoonPhases"

.field public static final NIGHTTIME_FORECAST:Ljava/lang/String; = "NighttimeForecast"

.field public static final ROOT:Ljava/lang/String; = "WeatherResponse"

.field public static final UNIT:Ljava/lang/String; = "Units"

.field public static final UNITS:Ljava/lang/String; = "Units"

.field public static final WEATHER_LOCATION:Ljava/lang/String; = "WeatherLocation"
