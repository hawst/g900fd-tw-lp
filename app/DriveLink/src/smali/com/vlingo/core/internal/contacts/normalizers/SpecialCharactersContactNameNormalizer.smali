.class public Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;
.super Lcom/vlingo/core/internal/contacts/normalizers/RegexContactNameNormalizer;
.source "SpecialCharactersContactNameNormalizer.java"


# static fields
.field private static final PATTERNS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static canNormalizePattern:Ljava/util/regex/Pattern;

.field private static canNormalizeStr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->canNormalizeStr:Ljava/lang/String;

    .line 15
    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    .line 17
    new-instance v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer$1;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer$1;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->PATTERNS:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/normalizers/RegexContactNameNormalizer;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-static {p0}, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0
.end method

.method private static createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 28
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->canNormalizeStr:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->canNormalizeStr:Ljava/lang/String;

    .line 33
    :goto_0
    invoke-static {p0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0

    .line 31
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->canNormalizeStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->canNormalizeStr:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected getCanNormalizePattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    if-nez v0, :cond_0

    .line 40
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->canNormalizeStr:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    .line 42
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method protected getPatterns()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;->PATTERNS:Ljava/util/Map;

    return-object v0
.end method
