.class public Lcom/vlingo/core/internal/memo/Memo;
.super Ljava/lang/Object;
.source "Memo.java"


# static fields
.field private static final ELLIPSE_STRING:Ljava/lang/String; = "..."

.field public static final MEMO_ID_CREATING_NEW:I = -0x1

.field public static final MEMO_ID_UNKNOWN_ID:I = -0x2


# instance fields
.field private content:Ljava/lang/String;

.field private date:Ljava/lang/String;

.field private id:I

.field private memoNameSpoken:Ljava/lang/String;

.field private memoNameText:Ljava/lang/String;

.field private text:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/memo/Memo;->id:I

    .line 30
    return-void
.end method


# virtual methods
.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/core/internal/memo/Memo;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/core/internal/memo/Memo;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/vlingo/core/internal/memo/Memo;->id:I

    return v0
.end method

.method public getMemoName(Z)Ljava/lang/String;
    .locals 6
    .param p1, "spoken"    # Z

    .prologue
    const/4 v5, 0x0

    .line 64
    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 65
    invoke-virtual {p0}, Lcom/vlingo/core/internal/memo/Memo;->getTitle()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    .line 66
    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/core/internal/memo/Memo;->getContent()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    .line 68
    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/vlingo/core/internal/memo/Memo;->getText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    .line 73
    :cond_0
    const/16 v0, 0x1e

    .line 74
    .local v0, "cutoff":I
    const/16 v1, 0xa

    .line 75
    .local v1, "minLength":I
    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v0, :cond_2

    .line 76
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-eq v2, v3, :cond_1

    if-le v0, v1, :cond_1

    .line 77
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 79
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    invoke-virtual {v3, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    .line 80
    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const-string/jumbo v4, "..."

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameSpoken:Ljava/lang/String;

    .line 83
    .end local v0    # "cutoff":I
    .end local v1    # "minLength":I
    :cond_2
    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameSpoken:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameSpoken:Ljava/lang/String;

    :goto_1
    return-object v2

    :cond_3
    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->memoNameText:Ljava/lang/String;

    goto :goto_1
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vlingo/core/internal/memo/Memo;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/core/internal/memo/Memo;->title:Ljava/lang/String;

    const-string/jumbo v1, ".snb"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/memo/Memo;->title:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vlingo/core/internal/memo/Memo;->title:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x4

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/memo/Memo;->title:Ljava/lang/String;

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/memo/Memo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vlingo/core/internal/memo/Memo;->content:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/vlingo/core/internal/memo/Memo;->date:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/vlingo/core/internal/memo/Memo;->id:I

    .line 36
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/core/internal/memo/Memo;->text:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/vlingo/core/internal/memo/Memo;->title:Ljava/lang/String;

    .line 43
    return-void
.end method
