.class public Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
.super Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;
.source "DMServerTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;,
        Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;
    }
.end annotation


# static fields
.field public static AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType; = null

.field public static final BT_START_DELAY_TIME:J = 0x7d0L

.field private static final TAG:Ljava/lang/String;

.field private static processingToneFadeOut:I


# instance fields
.field private DataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

.field private audioFileFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

.field private audioFilePath:Ljava/lang/String;

.field private awaitingStopTone:Z

.field private completedReco:Z

.field private final contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

.field private gotResults:Z

.field private micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

.field private onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

.field private performingReco:Z

.field private final recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

.field private sendingAudioFileRequest:Z

.field private sendingTextRequest:Z

.field private textRequest:Ljava/lang/String;

.field private final turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

.field private final useAudioTrackTonePlayer:Z

.field private final useMediaSyncApproach:Z

.field private userProperties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    .line 83
    const/4 v0, -0x1

    sput v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->processingToneFadeOut:I

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/sdk/recognition/VLRecognitionListener;Ljava/util/Map;)V
    .locals 6
    .param p1, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .param p2, "recoListener"    # Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/DialogTurn;",
            "Lcom/vlingo/sdk/recognition/VLRecognitionListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "userProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v5, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 90
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;-><init>()V

    .line 64
    iput-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performingReco:Z

    .line 65
    iput-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendingTextRequest:Z

    .line 66
    iput-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendingAudioFileRequest:Z

    .line 68
    iput-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->awaitingStopTone:Z

    .line 69
    iput-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->completedReco:Z

    .line 71
    iput-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->gotResults:Z

    .line 73
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    invoke-direct {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .line 81
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .line 85
    const-string/jumbo v0, "use_mediasync_tone_approach"

    invoke-static {v0, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->useMediaSyncApproach:Z

    .line 86
    const-string/jumbo v0, "use_audiotrack_tone_player"

    invoke-static {v0, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->useAudioTrackTonePlayer:Z

    .line 88
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->DataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    .line 91
    if-nez p1, :cond_1

    .line 92
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "DMServerTask() created with null turn"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    invoke-static {v0, v2, v5, v2}, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->init(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/lang/String;I[B)V

    .line 98
    :goto_0
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->userProperties:Ljava/util/Map;

    .line 99
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 100
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 101
    sget v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->processingToneFadeOut:I

    if-ne v0, v5, :cond_0

    .line 102
    const-string/jumbo v0, "processing_tone_fadeout_period"

    invoke-static {v0, v4}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->processingToneFadeOut:I

    .line 105
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "DMServerTask() useMediaSyncApproach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->useMediaSyncApproach:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    return-void

    .line 95
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "DMServerTask() created with turn="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getTurnNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getTurnNumber()I

    move-result v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getServerState()[B

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->init(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/lang/String;I[B)V

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performRecoTransactionSub()V

    return-void
.end method

.method static synthetic access$1000(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/audio/AudioType;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->getAudioType()Lcom/vlingo/core/internal/audio/AudioType;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performingReco:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->closeMicStream()V

    return-void
.end method

.method static synthetic access$1302(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->awaitingStopTone:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->gotResults:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->gotResults:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    return-object v0
.end method

.method static synthetic access$1800()I
    .locals 1

    .prologue
    .line 58
    sget v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->processingToneFadeOut:I

    return v0
.end method

.method static synthetic access$202(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    .param p1, "x1"    # Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    return-object p1
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->useAudioTrackTonePlayer:Z

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->DataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performReco(Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->useMediaSyncApproach:Z

    return v0
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    .param p2, "x2"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performReco(Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;I)V

    return-void
.end method

.method private closeMicStream()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 464
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    if-eqz v2, :cond_1

    .line 466
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->close()V

    .line 467
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    if-eqz v2, :cond_0

    .line 468
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-interface {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onASRRecorderClosed()V

    .line 470
    :cond_0
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "closeMicStream: stopped"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478
    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 487
    :goto_0
    return-void

    .line 472
    :catch_0
    move-exception v0

    .line 473
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Error playing tone: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478
    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    throw v2

    .line 482
    :cond_1
    const-string/jumbo v1, "closeMicStream: micStream is null"

    .line 483
    .local v1, "msg":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getAudioType()Lcom/vlingo/core/internal/audio/AudioType;
    .locals 5

    .prologue
    .line 339
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;

    if-nez v2, :cond_0

    .line 340
    const-string/jumbo v2, "custom_tone_encoding"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 342
    .local v1, "encoding":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioType;->valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/AudioType;

    move-result-object v2

    sput-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 348
    .end local v1    # "encoding":Ljava/lang/String;
    :cond_0
    :goto_0
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;

    return-object v2

    .line 343
    .restart local v1    # "encoding":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 344
    .local v0, "e":Ljava/lang/Exception;
    const-class v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unable to process encoding type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    sget-object v2, Lcom/vlingo/core/internal/audio/AudioType;->PCM_22k:Lcom/vlingo/core/internal/audio/AudioType;

    sput-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;

    goto :goto_0
.end method

.method private logMicStreamCloseProblem(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "logMessage"    # Ljava/lang/String;
    .param p2, "exceptionMessage"    # Ljava/lang/String;

    .prologue
    .line 459
    return-void
.end method

.method private logStartRecoException(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 359
    return-void
.end method

.method private notifyIfDone(Z)V
    .locals 3
    .param p1, "justPlayedStopTone"    # Z

    .prologue
    .line 435
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "notifyIfDone: justPlayedStopTone="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    if-eqz p1, :cond_1

    .line 437
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->awaitingStopTone:Z

    .line 441
    :goto_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->completedReco:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->awaitingStopTone:Z

    if-nez v0, :cond_0

    .line 442
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyFinished()V

    .line 447
    :cond_0
    return-void

    .line 439
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->completedReco:Z

    goto :goto_0
.end method

.method private performReco(Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    .prologue
    .line 362
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performReco(Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;I)V

    .line 363
    return-void
.end method

.method private performReco(Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;I)V
    .locals 9
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    .param p2, "audioSessionId"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 366
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "[LatencyCheck] performReco()"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 374
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V

    .line 407
    :goto_0
    return-void

    .line 378
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v2

    .line 379
    .local v2, "isBtAudioOn":Z
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    .line 380
    .local v1, "isBVoice":Z
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    if-eqz v2, :cond_3

    if-nez v1, :cond_3

    move v6, v4

    :goto_1
    invoke-virtual {v7, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->autoEndpointing(Z)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->build()Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    move-result-object v3

    check-cast v3, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    .line 383
    .local v3, "srContext":Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;
    iget-boolean v6, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendingAudioFileRequest:Z

    if-nez v6, :cond_4

    .line 384
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    if-nez v6, :cond_1

    .line 385
    sget-object v6, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->RECOGNITION:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    invoke-static {v3, v6, p2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->request(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;I)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 387
    :cond_1
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->setRecoSource(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    .line 388
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->getChannelConfig()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->audioFormatChannelConfig(I)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 393
    :goto_2
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    if-eqz v6, :cond_2

    .line 394
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-interface {v6}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onASRRecorderOpened()V

    .line 397
    :cond_2
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    if-eqz v2, :cond_5

    if-nez v1, :cond_5

    :goto_3
    invoke-virtual {v7, v4}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->autoEndpointing(Z)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->build()Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    move-result-object v4

    new-instance v7, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-direct {v7, p0, v8}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSDKRecoMode()Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v8

    invoke-interface {v6, v4, v7, p1, v8}, Lcom/vlingo/sdk/recognition/VLRecognizer;->startRecognition(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 400
    .end local v1    # "isBVoice":Z
    .end local v2    # "isBtAudioOn":Z
    .end local v3    # "srContext":Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;
    :catch_0
    move-exception v0

    .line 401
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 404
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    sget-object v6, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_AUDIO:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    .line 405
    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "isBVoice":Z
    .restart local v2    # "isBtAudioOn":Z
    :cond_3
    move v6, v5

    .line 380
    goto :goto_1

    .line 390
    .restart local v3    # "srContext":Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;
    :cond_4
    :try_start_1
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->audioFilePath:Ljava/lang/String;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->audioFileFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    invoke-static {v6, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->setFileSource(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_5
    move v4, v5

    .line 397
    goto :goto_3
.end method

.method private performRecoTransaction()V
    .locals 6

    .prologue
    .line 221
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[LatencyCheck] performRecoTransaction()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 226
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V

    .line 259
    :goto_0
    return-void

    .line 230
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    .line 231
    .local v2, "isBtHeadsetConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v1

    .line 232
    .local v1, "isBluetoothAudioSupported":Z
    const/4 v0, 0x0

    .line 234
    .local v0, "audioStream":I
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 235
    :cond_1
    const/4 v0, 0x6

    .line 240
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v0, v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 245
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v3

    if-nez v3, :cond_3

    .line 247
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[LatencyCheck] performRecoTransaction() wait for BT connection."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    const-wide/16 v3, 0x7d0

    new-instance v5, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$1;

    invoke-direct {v5, p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)V

    invoke-static {v3, v4, v5}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->runTaskOnBluetoothAudioOn(JLjava/lang/Runnable;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    goto :goto_0

    .line 237
    :cond_2
    const/4 v0, 0x3

    goto :goto_1

    .line 257
    :cond_3
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performRecoTransactionSub()V

    goto :goto_0
.end method

.method private performRecoTransactionSub()V
    .locals 4

    .prologue
    .line 262
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[LatencyCheck] performRecoTransactionSub()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const-wide/16 v0, 0x0

    .line 265
    .local v0, "delay":J
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 268
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V

    .line 336
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    if-eqz v2, :cond_1

    .line 273
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onRecoToneStarting(Z)J

    move-result-wide v0

    .line 276
    :cond_1
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$2;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)V

    invoke-static {v2, v0, v1}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method private performTextReco(Ljava/lang/String;)V
    .locals 9
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 410
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "performTextReco()"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 415
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V

    .line 432
    :goto_0
    return-void

    .line 419
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v2

    .line 420
    .local v2, "isBtAudioOn":Z
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    .line 421
    .local v1, "isBVoice":Z
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    invoke-static {v3, p1}, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->setTextSource(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/lang/String;)V

    .line 422
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v6, v3}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->autoEndpointing(Z)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->build()Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    move-result-object v3

    new-instance v6, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-direct {v6, p0, v7}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V

    new-instance v7, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    invoke-direct {v7}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;-><init>()V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSDKRecoMode()Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v8

    invoke-interface {v5, v3, v6, v7, v8}, Lcom/vlingo/sdk/recognition/VLRecognizer;->startRecognition(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 425
    .end local v1    # "isBVoice":Z
    .end local v2    # "isBtAudioOn":Z
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 429
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    sget-object v5, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_AUDIO:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    .line 430
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "isBVoice":Z
    .restart local v2    # "isBtAudioOn":Z
    :cond_1
    move v3, v4

    .line 422
    goto :goto_1
.end method


# virtual methods
.method protected getToneId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I
    .locals 2
    .param p1, "normalTone"    # Lcom/vlingo/core/internal/ResourceIdProvider$raw;
    .param p2, "drivingModeTone"    # Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .prologue
    .line 490
    const/4 v0, -0x1

    .line 492
    .local v0, "toneId":I
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 493
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v0

    .line 496
    :cond_0
    if-gtz v0, :cond_1

    .line 497
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v0

    .line 499
    :cond_1
    return v0
.end method

.method public isPerformingReco()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performingReco:Z

    return v0
.end method

.method public declared-synchronized onCancelled()V
    .locals 2

    .prologue
    .line 192
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCancelled()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->cancel()V

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .line 204
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 205
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V

    .line 211
    :cond_1
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->gotResults:Z

    if-nez v0, :cond_2

    .line 212
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 213
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognizer;->cancelRecognition()V

    .line 217
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->closeMicStream()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    monitor-exit p0

    return-void

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public performAudioFileReco(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;Ljava/util/Map;)V
    .locals 2
    .param p1, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .param p2, "file"    # Ljava/lang/String;
    .param p3, "format"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139
    .local p4, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "perfromAudioFile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendingAudioFileRequest:Z

    if-nez v0, :cond_0

    .line 145
    iput-object p4, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->userProperties:Ljava/util/Map;

    .line 146
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->addReco(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 147
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->audioFileFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .line 148
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->audioFilePath:Ljava/lang/String;

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendingAudioFileRequest:Z

    .line 151
    :cond_0
    return-void
.end method

.method public performReco(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/audio/MicrophoneStream;Ljava/util/Map;)V
    .locals 2
    .param p1, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .param p2, "microphoneStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            "Lcom/vlingo/core/internal/audio/MicrophoneStream;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p3, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "performReco()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 116
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performingReco:Z

    if-nez v0, :cond_0

    .line 117
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->userProperties:Ljava/util/Map;

    .line 118
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->addReco(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performingReco:Z

    .line 121
    :cond_0
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    .line 160
    const/4 v1, 0x0

    .line 161
    .local v1, "hasEvents":Z
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[LatencyCheck] DMServerTask.run()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v2, :cond_1

    .line 163
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getPendingEvents()Ljava/util/List;

    move-result-object v0

    .line 164
    .local v0, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/DialogEvent;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 165
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    invoke-static {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->addEvents(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/util/List;)V

    .line 166
    const/4 v1, 0x1

    .line 168
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->isFromEDM()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->setFromEDM(Z)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .line 172
    .end local v0    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/DialogEvent;>;"
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->userProperties:Ljava/util/Map;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->addUserProperties(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/util/Map;)V

    .line 174
    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performingReco:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendingAudioFileRequest:Z

    if-eqz v2, :cond_3

    .line 175
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performRecoTransaction()V

    .line 188
    :goto_0
    return-void

    .line 177
    :cond_3
    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendingTextRequest:Z

    if-eqz v2, :cond_4

    .line 178
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->textRequest:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performTextReco(Ljava/lang/String;)V

    goto :goto_0

    .line 180
    :cond_4
    if-eqz v1, :cond_5

    .line 181
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Sending events only, no reco."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    invoke-virtual {v3}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    move-result-object v3

    new-instance v4, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-direct {v4, p0, v5}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V

    invoke-interface {v2, v3, v4}, Lcom/vlingo/sdk/recognition/VLRecognizer;->sendEvent(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V

    goto :goto_0

    .line 185
    :cond_5
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "notifyFinished"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyFinished()V

    goto :goto_0
.end method

.method public sendTextRequest(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .param p2, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    .local p3, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sendText"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendingTextRequest:Z

    if-nez v0, :cond_0

    .line 131
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->userProperties:Ljava/util/Map;

    .line 132
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->contextBuilder:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->addReco(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 133
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->textRequest:Ljava/lang/String;

    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendingTextRequest:Z

    .line 136
    :cond_0
    return-void
.end method
