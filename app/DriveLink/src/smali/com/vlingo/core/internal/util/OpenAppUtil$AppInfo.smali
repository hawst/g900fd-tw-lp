.class public Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
.super Ljava/lang/Object;
.source "OpenAppUtil.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/OpenAppUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityName:Ljava/lang/String;

.field private final appName:Ljava/lang/String;

.field private final appNameLabel:Ljava/lang/String;

.field private final packageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "appNameLabel"    # Ljava/lang/String;
    .param p3, "pkg"    # Ljava/lang/String;
    .param p4, "act"    # Ljava/lang/String;

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    if-nez p1, :cond_0

    const-string/jumbo p1, ""

    .end local p1    # "name":Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appName:Ljava/lang/String;

    .line 195
    iput-object p3, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->packageName:Ljava/lang/String;

    .line 196
    iput-object p4, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->activityName:Ljava/lang/String;

    .line 197
    if-nez p2, :cond_1

    const-string/jumbo p2, ""

    .end local p2    # "appNameLabel":Ljava/lang/String;
    :cond_1
    iput-object p2, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appNameLabel:Ljava/lang/String;

    .line 198
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appNameLabel:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)I
    .locals 2
    .param p1, "another"    # Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appName:Ljava/lang/String;

    iget-object v1, p1, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 186
    check-cast p1, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->compareTo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)I

    move-result v0

    return v0
.end method

.method public getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->activityName:Ljava/lang/String;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getAppNameLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appNameLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 201
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 202
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    iget-object v1, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->activityName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 204
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->packageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->activityName:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 206
    :cond_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 207
    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appNameLabel:Ljava/lang/String;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appNameLabel:Ljava/lang/String;

    goto :goto_0
.end method
