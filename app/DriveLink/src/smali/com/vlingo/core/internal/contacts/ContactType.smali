.class public final enum Lcom/vlingo/core/internal/contacts/ContactType;
.super Ljava/lang/Enum;
.source "ContactType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/contacts/ContactType;

.field public static final enum ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

.field public static final enum BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactType;

.field public static final enum CALL:Lcom/vlingo/core/internal/contacts/ContactType;

.field public static final enum EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

.field public static final enum FACEBOOK:Lcom/vlingo/core/internal/contacts/ContactType;

.field public static final enum MESSAGE:Lcom/vlingo/core/internal/contacts/ContactType;

.field public static final enum SMS:Lcom/vlingo/core/internal/contacts/ContactType;

.field public static final enum UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;


# instance fields
.field final action:Ljava/lang/String;

.field keepUnrelatedContacts:Z

.field subType:I

.field final types:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 13
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactType;

    const-string/jumbo v1, "CALL"

    const-string/jumbo v3, "call"

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactLookupType;->PHONE_NUMBER:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;IZ)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 14
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactType;

    const-string/jumbo v8, "SMS"

    const-string/jumbo v10, "sms"

    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;->PHONE_NUMBER:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v11

    move v9, v6

    move v12, v5

    invoke-direct/range {v7 .. v12}, Lcom/vlingo/core/internal/contacts/ContactType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;I)V

    sput-object v7, Lcom/vlingo/core/internal/contacts/ContactType;->SMS:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 15
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactType;

    const-string/jumbo v1, "EMAIL"

    const-string/jumbo v3, "email"

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactLookupType;->EMAIL_ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {v0, v1, v5, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 16
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactType;

    const-string/jumbo v1, "ADDRESS"

    const-string/jumbo v3, "address"

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactLookupType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {v0, v1, v13, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 17
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactType;

    const-string/jumbo v1, "BIRTHDAY"

    const-string/jumbo v3, "birthday"

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactLookupType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {v0, v1, v14, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 18
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactType;

    const-string/jumbo v1, "FACEBOOK"

    const/4 v3, 0x5

    const-string/jumbo v4, "facebook"

    sget-object v7, Lcom/vlingo/core/internal/contacts/ContactLookupType;->SOCIAL_NETWORK:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-static {v7}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v7

    invoke-direct {v0, v1, v3, v4, v7}, Lcom/vlingo/core/internal/contacts/ContactType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->FACEBOOK:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 19
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactType;

    const-string/jumbo v1, "MESSAGE"

    const/4 v3, 0x6

    const-string/jumbo v4, "message"

    const-class v7, Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-static {v7}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v7

    invoke-direct {v0, v1, v3, v4, v7}, Lcom/vlingo/core/internal/contacts/ContactType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->MESSAGE:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 20
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactType;

    const-string/jumbo v1, "UNDEFINED"

    const/4 v3, 0x7

    const-string/jumbo v4, ""

    const-class v7, Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-static {v7}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v7

    invoke-direct {v0, v1, v3, v4, v7}, Lcom/vlingo/core/internal/contacts/ContactType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 12
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->SMS:Lcom/vlingo/core/internal/contacts/ContactType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactType;

    aput-object v1, v0, v14

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->FACEBOOK:Lcom/vlingo/core/internal/contacts/ContactType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->MESSAGE:Lcom/vlingo/core/internal/contacts/ContactType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->$VALUES:[Lcom/vlingo/core/internal/contacts/ContactType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;)V
    .locals 0
    .param p3, "action"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p4, "types":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactType;->action:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactType;->types:Ljava/util/EnumSet;

    .line 30
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;I)V
    .locals 0
    .param p3, "action"    # Ljava/lang/String;
    .param p5, "subType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p4, "types":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactType;->action:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactType;->types:Ljava/util/EnumSet;

    .line 35
    iput p5, p0, Lcom/vlingo/core/internal/contacts/ContactType;->subType:I

    .line 36
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;IZ)V
    .locals 0
    .param p3, "action"    # Ljava/lang/String;
    .param p5, "subType"    # I
    .param p6, "keepUnrelatedContacts"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p4, "types":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactType;->action:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactType;->types:Ljava/util/EnumSet;

    .line 47
    iput p5, p0, Lcom/vlingo/core/internal/contacts/ContactType;->subType:I

    .line 48
    iput-boolean p6, p0, Lcom/vlingo/core/internal/contacts/ContactType;->keepUnrelatedContacts:Z

    .line 49
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/EnumSet;Z)V
    .locals 0
    .param p3, "action"    # Ljava/lang/String;
    .param p5, "keepUnrelatedContacts"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p4, "types":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactType;->action:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactType;->types:Ljava/util/EnumSet;

    .line 41
    iput-boolean p5, p0, Lcom/vlingo/core/internal/contacts/ContactType;->keepUnrelatedContacts:Z

    .line 42
    return-void
.end method

.method public static of(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 5
    .param p0, "action"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactType;->values()[Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/contacts/ContactType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 59
    .local v3, "type":Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 63
    .end local v3    # "type":Lcom/vlingo/core/internal/contacts/ContactType;
    :goto_1
    return-object v3

    .line 58
    .restart local v3    # "type":Lcom/vlingo/core/internal/contacts/ContactType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    .end local v3    # "type":Lcom/vlingo/core/internal/contacts/ContactType;
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->$VALUES:[Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/contacts/ContactType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method


# virtual methods
.method public getLookupTypes()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactType;->types:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getPreferredTarget()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactType;->subType:I

    return v0
.end method

.method public keepUnrelatedContacts()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactType;->keepUnrelatedContacts:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactType;->action:Ljava/lang/String;

    return-object v0
.end method
