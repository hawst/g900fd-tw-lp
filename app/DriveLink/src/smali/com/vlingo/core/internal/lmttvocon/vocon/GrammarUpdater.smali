.class public interface abstract Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;
.super Ljava/lang/Object;
.source "GrammarUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;,
        Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;
    }
.end annotation


# virtual methods
.method public abstract getCurrentChecksum(Lcom/vlingo/core/internal/lmttvocon/UploadType;)I
.end method

.method public abstract updateGrammar(Lcom/vlingo/core/internal/lmttvocon/UploadType;Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;ZLcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;)Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;
.end method
