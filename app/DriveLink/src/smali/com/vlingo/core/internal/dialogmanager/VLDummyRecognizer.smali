.class public Lcom/vlingo/core/internal/dialogmanager/VLDummyRecognizer;
.super Ljava/lang/Object;
.source "VLDummyRecognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/VLRecognizer;


# instance fields
.field mRealRecognizer:Lcom/vlingo/sdk/internal/VLRecognizerImpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method public acceptedText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "gUttId"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 72
    return-void
.end method

.method public cancelRecognition()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public getIsStoppedDataReader()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/VLDummyRecognizer;->mRealRecognizer:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/VLDummyRecognizer;->mRealRecognizer:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getIsStoppedDataReader()Z

    move-result v0

    .line 108
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupportedLanguageList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 81
    .local v0, "ret":[Ljava/lang/String;
    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/VLDummyRecognizer;->mRealRecognizer:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/VLDummyRecognizer;->mRealRecognizer:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendEvent(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V
    .locals 0
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .prologue
    .line 54
    return-void
.end method

.method public startRecognition(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    .locals 0
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    .param p3, "dataReadyListener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    .param p4, "recoMode"    # Lcom/vlingo/sdk/recognition/RecognitionMode;

    .prologue
    .line 48
    return-void
.end method

.method public stopRecognition()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method unregister()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method
