.class public abstract Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;
.source "ResolveContactHandlerBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$1;,
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;
    }
.end annotation


# static fields
.field private static final ANAPHORA:Ljava/lang/String; = "ANAPHORA"

.field protected static final NAME_ACTION_PARAMETER:Ljava/lang/String; = "name"

.field private static final REDIAL:Ljava/lang/String; = "REDIAL"


# instance fields
.field private mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

.field private mDetailed:Z

.field private mLimit:I

.field private mQuery:Ljava/lang/String;

.field private mType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;-><init>()V

    .line 36
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 175
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    .prologue
    .line 23
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mLimit:I

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mDetailed:Z

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected doesUsePhoneDisambiguation()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 288
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactType;->getLookupTypes()Ljava/util/EnumSet;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactLookupType;->PHONE_NUMBER:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "USE_PHONE_DISAMBIG"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected executeQuery(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    const-string/jumbo v3, "contact"

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->initParameters(Lcom/vlingo/sdk/recognition/VLAction;)V

    .line 50
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 70
    :goto_0
    return v1

    .line 55
    :cond_0
    const-string/jumbo v1, "clear.cache"

    invoke-static {p1, v1, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 58
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mQuery:Ljava/lang/String;

    const-string/jumbo v3, "ANAPHORA"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 59
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SELECTED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 60
    .local v0, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->doesUsePhoneDisambiguation()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 61
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getPhoneTypes()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->chooseSinglePhone([I)V

    .line 63
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SELECTED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->sendContactResolvedEvent(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    move v1, v2

    .line 64
    goto :goto_0

    .line 65
    .end local v0    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mQuery:Ljava/lang/String;

    const-string/jumbo v3, "REDIAL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 66
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getContactMatchForRedial()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->sendContactResolvedEvent(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    move v1, v2

    .line 67
    goto :goto_0

    .line 69
    :cond_4
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mQuery:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->processContactMatching(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 70
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected getAddressTypes()[I
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCapability()Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method protected getContactMatchForRedial()Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 3

    .prologue
    .line 77
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ContactUtil;->getLastOutgoingCall(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->getContactMatchByPhoneNumber(Ljava/lang/String;Landroid/content/Context;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    return-object v0
.end method

.method protected getContactMatchListener()Lcom/vlingo/core/internal/contacts/ContactMatchListener;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$1;)V

    return-object v0
.end method

.method protected getEmailTypes()[I
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getPhoneTypes()[I
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 128
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mType:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-object v0

    .line 131
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->SMS:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v1, v2, :cond_0

    .line 132
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mType:Ljava/lang/String;

    const-string/jumbo v2, "work"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 133
    new-array v0, v3, [I

    const/4 v1, 0x3

    aput v1, v0, v4

    goto :goto_0

    .line 134
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mType:Ljava/lang/String;

    const-string/jumbo v2, "mobile"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 135
    new-array v0, v3, [I

    const/4 v1, 0x2

    aput v1, v0, v4

    goto :goto_0

    .line 136
    :cond_4
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mType:Ljava/lang/String;

    const-string/jumbo v2, "home"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 137
    new-array v0, v3, [I

    aput v3, v0, v4

    goto :goto_0

    .line 138
    :cond_5
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mType:Ljava/lang/String;

    const-string/jumbo v2, "other"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    new-array v0, v3, [I

    const/4 v1, 0x7

    aput v1, v0, v4

    goto :goto_0
.end method

.method protected getSocialTypes()[I
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mType:Ljava/lang/String;

    return-object v0
.end method

.method protected getmQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method protected initParameters(Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v3, 0x0

    .line 82
    const-string/jumbo v1, "name"

    invoke-interface {p1, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->setmQuery(Ljava/lang/String;)V

    .line 83
    const-string/jumbo v1, "capability"

    invoke-interface {p1, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "capabilityStr":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/ContactType;->of(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 87
    :cond_0
    const-string/jumbo v1, "type"

    invoke-interface {p1, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mType:Ljava/lang/String;

    .line 88
    const-string/jumbo v1, "limit"

    const/4 v2, -0x1

    invoke-static {p1, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v1

    iput v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mLimit:I

    .line 89
    const-string/jumbo v1, "detail"

    const/4 v2, 0x1

    invoke-static {p1, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mDetailed:Z

    .line 90
    return-void
.end method

.method protected abstract processContactMatching(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;)V
.end method

.method protected sendContactResolvedEvent(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 7
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 112
    .local v1, "allContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-eqz p1, :cond_0

    .line 113
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 116
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

    iget-boolean v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mDetailed:Z

    const/4 v4, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getPhoneTypes()[I

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;-><init>(Ljava/util/List;Lcom/vlingo/core/internal/contacts/ContactType;ZII[I)V

    .line 117
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 118
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 120
    :cond_1
    return-void
.end method

.method protected sendEmptyEvent()V
    .locals 3

    .prologue
    .line 102
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;-><init>()V

    .line 103
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 104
    return-void
.end method

.method public setCapability(Lcom/vlingo/core/internal/contacts/ContactType;)V
    .locals 0
    .param p1, "capability"    # Lcom/vlingo/core/internal/contacts/ContactType;

    .prologue
    .line 268
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 269
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mType:Ljava/lang/String;

    .line 277
    return-void
.end method

.method protected setmQuery(Ljava/lang/String;)V
    .locals 0
    .param p1, "mQuery"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mQuery:Ljava/lang/String;

    .line 281
    return-void
.end method
