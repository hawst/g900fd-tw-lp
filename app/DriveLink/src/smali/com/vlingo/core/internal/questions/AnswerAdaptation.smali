.class public Lcom/vlingo/core/internal/questions/AnswerAdaptation;
.super Ljava/lang/Object;
.source "AnswerAdaptation.java"

# interfaces
.implements Lcom/vlingo/core/internal/questions/Answer;


# static fields
.field static final DUPLICATED_UNIT_TABLE:[Ljava/lang/String;


# instance fields
.field private mAnswer:Lcom/vlingo/core/internal/questions/Answer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "lb  (pounds)"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "kg  (kilograms)"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "mg  (milligrams)"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "gr  (grams)"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "t  (metric tons)"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "km  (kilometers)"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "m  (meters)"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "cm  (centimeters)"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "mm  (millimeters)"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "dm  (decimeters)"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "fl oz  (fluid ounces)"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "mph  (miles per hour)"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "km/h  (kilometers per hour)"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "m/s  (meters per second)"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "/L  (US dollars per liter)"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "/gal  (US dollars per gallon)"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "km^2  (square kilometers)"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "mi^2  (square miles)"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "m^3  (cubic meters)"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "mi^3  (cubic miles)"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "in^3  (cubic inches)"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "L  (liters)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->DUPLICATED_UNIT_TABLE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/questions/Answer;)V
    .locals 0
    .param p1, "answer"    # Lcom/vlingo/core/internal/questions/Answer;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->mAnswer:Lcom/vlingo/core/internal/questions/Answer;

    return-void
.end method


# virtual methods
.method public getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->mAnswer:Lcom/vlingo/core/internal/questions/Answer;

    invoke-interface {v0}, Lcom/vlingo/core/internal/questions/Answer;->getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    return-object v0
.end method

.method public getSection(Ljava/lang/String;)Lcom/vlingo/core/internal/questions/Answer$Section;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->mAnswer:Lcom/vlingo/core/internal/questions/Answer;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/questions/Answer;->getSection(Ljava/lang/String;)Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    return-object v0
.end method

.method public getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->mAnswer:Lcom/vlingo/core/internal/questions/Answer;

    invoke-interface {v0}, Lcom/vlingo/core/internal/questions/Answer;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    return-object v0
.end method

.method public getSimpleResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->mAnswer:Lcom/vlingo/core/internal/questions/Answer;

    invoke-interface {v0}, Lcom/vlingo/core/internal/questions/Answer;->getSimpleResponse()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasAnswer()Z
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->getSimpleResponse()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_1

    .line 41
    :cond_0
    const/4 v0, 0x0

    .line 43
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasMoreInformation()Z
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSection(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->mAnswer:Lcom/vlingo/core/internal/questions/Answer;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/questions/Answer;->hasSection(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected removeDuplicatedUnit(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "inputString"    # Ljava/lang/String;

    .prologue
    .line 75
    const/4 v2, 0x2

    .line 76
    .local v2, "spaceCount":I
    const/4 v1, 0x0

    .line 77
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v6, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->DUPLICATED_UNIT_TABLE:[Ljava/lang/String;

    array-length v6, v6

    if-ge v0, v6, :cond_0

    .line 78
    sget-object v6, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->DUPLICATED_UNIT_TABLE:[Ljava/lang/String;

    aget-object v4, v6, v0

    .line 79
    .local v4, "unit":Ljava/lang/String;
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 80
    const/4 v6, -0x1

    if-eq v1, v6, :cond_1

    .line 81
    const-string/jumbo v6, "("

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v3, v6, -0x2

    .line 82
    .local v3, "subIndex":I
    const/4 v6, 0x0

    invoke-virtual {v4, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 83
    .local v5, "unitString":Ljava/lang/String;
    const-string/jumbo v6, "^2"

    const-string/jumbo v7, "\u00b2"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 84
    const-string/jumbo v6, "^3"

    const-string/jumbo v7, "\u00b3"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 86
    sget-object v6, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->DUPLICATED_UNIT_TABLE:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-static {p1, v6, v5}, Lcom/vlingo/core/internal/util/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 90
    .end local v3    # "subIndex":I
    .end local v4    # "unit":Ljava/lang/String;
    .end local v5    # "unitString":Ljava/lang/String;
    .end local p1    # "inputString":Ljava/lang/String;
    :cond_0
    return-object p1

    .line 77
    .restart local v4    # "unit":Ljava/lang/String;
    .restart local p1    # "inputString":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected stripText(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "oriTTSresult"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->removeDuplicatedUnit(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
