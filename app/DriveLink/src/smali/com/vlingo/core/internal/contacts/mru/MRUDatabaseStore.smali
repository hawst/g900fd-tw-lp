.class public Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;
.super Ljava/lang/Object;
.source "MRUDatabaseStore.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/mru/MRUStore;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private m_applicationNames:[Ljava/lang/String;

.field private m_columns:[Ljava/lang/String;

.field private m_mruMaxSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private loadMRUTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    .locals 18
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 142
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->m_columns:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 143
    .local v16, "cur":Landroid/database/Cursor;
    new-instance v17, Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->m_mruMaxSize:I

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;-><init>(Ljava/lang/String;I)V

    .line 145
    .local v17, "table":Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    if-eqz v16, :cond_0

    .line 149
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 150
    const-string/jumbo v2, "contactID"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 151
    .local v12, "contactColumn":I
    if-gez v12, :cond_1

    .line 152
    sget-object v2, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Cannot find contactID in MRU Db null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 154
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 155
    const/16 v17, 0x0

    .line 183
    .end local v12    # "contactColumn":I
    .end local v17    # "table":Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    :cond_0
    :goto_0
    return-object v17

    .line 157
    .restart local v12    # "contactColumn":I
    .restart local v17    # "table":Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    :cond_1
    const-string/jumbo v2, "address"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 158
    .local v11, "addressColumn":I
    if-gez v11, :cond_2

    .line 159
    sget-object v2, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Cannot find address in MRU Db null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 161
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 162
    const/16 v17, 0x0

    goto :goto_0

    .line 164
    :cond_2
    const-string/jumbo v2, "count"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 165
    .local v15, "countColumn":I
    if-gez v15, :cond_3

    .line 166
    sget-object v2, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Cannot find count in MRU Db null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 168
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 169
    const/16 v17, 0x0

    goto :goto_0

    .line 172
    :cond_3
    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getFloat(I)F

    move-result v14

    .line 173
    .local v14, "count":F
    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 174
    .local v10, "address":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 175
    .local v13, "contactID":I
    move-object/from16 v0, v17

    invoke-virtual {v0, v13, v10, v14}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->setCount(ILjava/lang/String;F)V

    .line 179
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 181
    .end local v10    # "address":Ljava/lang/String;
    .end local v11    # "addressColumn":I
    .end local v12    # "contactColumn":I
    .end local v13    # "contactID":I
    .end local v14    # "count":F
    .end local v15    # "countColumn":I
    :cond_4
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private openDb()Landroid/database/sqlite/SQLiteDatabase;
    .locals 9

    .prologue
    .line 116
    const/4 v2, 0x0

    .line 118
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 119
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_0

    .line 120
    const-string/jumbo v6, "MRU"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v1, v6, v7, v8}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 123
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->m_applicationNames:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 124
    .local v5, "name":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "CREATE TABLE IF NOT EXISTS "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " (contactID INT, address VARCHAR, count REAL)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 128
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "context":Landroid/content/Context;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "name":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 135
    :cond_0
    return-object v2
.end method


# virtual methods
.method public init([Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 0
    .param p1, "applicationNames"    # [Ljava/lang/String;
    .param p2, "columns"    # [Ljava/lang/String;
    .param p3, "mruMaxSize"    # I

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->m_applicationNames:[Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->m_columns:[Ljava/lang/String;

    .line 35
    iput p3, p0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->m_mruMaxSize:I

    .line 36
    return-void
.end method

.method public loadMRUTables()[Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    .locals 4

    .prologue
    .line 39
    const/4 v2, 0x0

    .line 41
    .local v2, "tables":[Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->openDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 42
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v0, :cond_1

    .line 43
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->m_applicationNames:[Ljava/lang/String;

    array-length v3, v3

    new-array v2, v3, [Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    .line 44
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->m_applicationNames:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 45
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->m_applicationNames:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-direct {p0, v0, v3}, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->loadMRUTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    move-result-object v3

    aput-object v3, v2, v1

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 50
    .end local v1    # "i":I
    :cond_1
    return-object v2
.end method

.method public removeEntry(Ljava/lang/String;I)V
    .locals 4
    .param p1, "applicationName"    # Ljava/lang/String;
    .param p2, "contactToRemove"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->openDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 55
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v0, :cond_0

    .line 56
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "DELETE FROM "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " WHERE contactID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "sqlStatement":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 61
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 63
    .end local v1    # "sqlStatement":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public rescaleAllCounts(Ljava/lang/String;F)Z
    .locals 4
    .param p1, "applicationName"    # Ljava/lang/String;
    .param p2, "scaleFactor"    # F

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->openDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 100
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 101
    const/4 v2, 0x0

    .line 112
    :goto_0
    return v2

    .line 103
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "UPDATE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " SET count = count * "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "sqlStatement":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 111
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 112
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public setEntryCount(Ljava/lang/String;ILjava/lang/String;F)V
    .locals 8
    .param p1, "applicationName"    # Ljava/lang/String;
    .param p2, "contactID"    # I
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "newCount"    # F

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;->openDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 67
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v0, :cond_1

    .line 68
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 69
    .local v4, "values":Landroid/content/ContentValues;
    const-string/jumbo v6, "count"

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 71
    const-string/jumbo v6, "\'"

    const-string/jumbo v7, "\'\'"

    invoke-virtual {p3, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "normAddress":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "contactID="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " AND address=\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 77
    .local v5, "whereString":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {v0, p1, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 82
    .local v2, "numRowsAffected":I
    if-gtz v2, :cond_0

    .line 83
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " INSERT INTO "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " (contactID, address, count) "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " VALUES ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\', "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ");"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 90
    .local v3, "sqlStatement":Ljava/lang/String;
    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 92
    .end local v3    # "sqlStatement":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 96
    .end local v1    # "normAddress":Ljava/lang/String;
    .end local v2    # "numRowsAffected":I
    .end local v4    # "values":Landroid/content/ContentValues;
    .end local v5    # "whereString":Ljava/lang/String;
    :cond_1
    return-void
.end method
