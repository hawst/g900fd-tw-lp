.class public Lcom/vlingo/core/internal/util/MusicUtil$ArtistInfo;
.super Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;
.source "MusicUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/MusicUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArtistInfo"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;-><init>()V

    .line 159
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ARTIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/MusicUtil$ArtistInfo;->type:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    .line 160
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 163
    const-string/jumbo v0, ""

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ARTIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/MusicUtil$ArtistInfo;->type:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    .line 165
    return-void
.end method
