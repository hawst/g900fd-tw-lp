.class Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;
.super Landroid/os/HandlerThread;
.source "BluetoothManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/bluetooth/BluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScoWorkerTask"
.end annotation


# static fields
.field public static final MSG_START_SCO:I


# instance fields
.field public mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1038
    const-string/jumbo v0, "SCOStartStop"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 1039
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 1165
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1166
    invoke-virtual {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->quit()Z

    .line 1167
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 1049
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 1054
    new-instance v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask$1;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask$1;-><init>(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    .line 1152
    invoke-static {}, Landroid/os/Looper;->loop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1156
    :goto_0
    return-void

    .line 1153
    :catch_0
    move-exception v0

    .line 1154
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
