.class Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;
.super Ljava/lang/Object;
.source "TTSEngine.java"

# interfaces
.implements Lcom/vlingo/sdk/tts/VLTextToSpeechListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/TTSEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FetchTTSTask"
.end annotation


# instance fields
.field audioFilePath:Ljava/lang/String;

.field final context:Landroid/content/Context;

.field volatile isComplete:Z

.field volatile isSuccess:Z

.field final request:Lcom/vlingo/core/internal/audio/TTSRequest;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/audio/TTSRequest;Landroid/content/Context;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->audioFilePath:Ljava/lang/String;

    .line 248
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isComplete:Z

    .line 249
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isSuccess:Z

    .line 252
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->request:Lcom/vlingo/core/internal/audio/TTSRequest;

    .line 253
    iput-object p2, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->context:Landroid/content/Context;

    .line 254
    return-void
.end method


# virtual methods
.method declared-synchronized isComplete()Z
    .locals 1

    .prologue
    .line 290
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized isSuccess()Z
    .locals 1

    .prologue
    .line 293
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isSuccess:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onError(Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;Ljava/lang/String;)V
    .locals 1
    .param p1, "error"    # Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 302
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isComplete:Z

    .line 303
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isSuccess:Z

    .line 304
    return-void
.end method

.method public onSuccess()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 297
    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isComplete:Z

    .line 298
    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->isSuccess:Z

    .line 299
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 256
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->request:Lcom/vlingo/core/internal/audio/TTSRequest;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/audio/TTSRequest;->getTextToSpeak()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/vlingo/core/internal/audio/TTSEngine;->getDefaultSDKRequest(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
    invoke-static {v4}, Lcom/vlingo/core/internal/audio/TTSEngine;->access$000(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;

    move-result-object v2

    .line 257
    .local v2, "sdkRequest":Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
    const/4 v3, 0x0

    .line 259
    .local v3, "ttsEngine":Lcom/vlingo/sdk/tts/VLTextToSpeech;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/VLSdk;->getTextToSpeech()Lcom/vlingo/sdk/tts/VLTextToSpeech;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 266
    :try_start_1
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->request:Lcom/vlingo/core/internal/audio/TTSRequest;

    const-string/jumbo v5, "network_tts"

    iget-object v6, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->context:Landroid/content/Context;

    invoke-static {v4, v5, v6}, Lcom/vlingo/core/internal/audio/TTSCache;->getTempFile(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 267
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 268
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v4

    if-nez v4, :cond_1

    .line 269
    # getter for: Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/TTSEngine;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "createNewFile failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 288
    .end local v1    # "f":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/TTSEngine;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "Exception: Call to SDK TTS engine, but SDK is not initialized"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 272
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .restart local v1    # "f":Ljava/io/File;
    :cond_1
    :try_start_2
    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v4

    if-nez v4, :cond_2

    .line 273
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/io/File;->setWritable(Z)Z

    move-result v4

    if-nez v4, :cond_2

    .line 274
    # getter for: Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/TTSEngine;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "!canWrite but setWritable failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 283
    .end local v1    # "f":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 284
    .local v0, "e":Ljava/io/FileNotFoundException;
    # getter for: Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/TTSEngine;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "FileNotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 279
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .restart local v1    # "f":Ljava/io/File;
    :cond_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->audioFilePath:Ljava/lang/String;

    .line 280
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->audioFilePath:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 281
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/TTSEngine$FetchTTSTask;->audioFilePath:Ljava/lang/String;

    invoke-interface {v3, v2, v4, p0}, Lcom/vlingo/sdk/tts/VLTextToSpeech;->synthesizeToFile(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;Ljava/lang/String;Lcom/vlingo/sdk/tts/VLTextToSpeechListener;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 285
    .end local v1    # "f":Ljava/io/File;
    :catch_2
    move-exception v0

    .line 286
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/vlingo/core/internal/audio/TTSEngine;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/TTSEngine;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
