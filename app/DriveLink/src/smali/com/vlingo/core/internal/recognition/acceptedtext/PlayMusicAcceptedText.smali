.class public final Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;
.super Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;
.source "PlayMusicAcceptedText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;
    }
.end annotation


# static fields
.field private static instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;


# instance fields
.field private context:Landroid/content/Context;

.field private ids:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private requestedName:Ljava/lang/String;

.field private requestedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

.field private resolvedName:Ljava/lang/String;

.field private resolvedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;-><init>(Ljava/lang/String;)V

    .line 101
    const-string/jumbo v0, "PlayMusicAcceptedText"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "context: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->context:Landroid/content/Context;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->ids:Ljava/util/ArrayList;

    .line 104
    return-void
.end method

.method public static requested(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "requestedName"    # Ljava/lang/String;

    .prologue
    .line 58
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    .line 59
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    iput-object p2, v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requestedName:Ljava/lang/String;

    .line 61
    const-string/jumbo v0, "Music:Title"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->TITLE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    iput-object v1, v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requestedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    .line 72
    :goto_0
    return-void

    .line 63
    :cond_0
    const-string/jumbo v0, "Music:Album"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ALBUM:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    iput-object v1, v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requestedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    goto :goto_0

    .line 65
    :cond_1
    const-string/jumbo v0, "Music:Artist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ARTIST:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    iput-object v1, v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requestedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    goto :goto_0

    .line 67
    :cond_2
    const-string/jumbo v0, "Music:Generic"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 68
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->GENERIC:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    iput-object v1, v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requestedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    goto :goto_0

    .line 70
    :cond_3
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ELSE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    iput-object v1, v0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requestedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    goto :goto_0
.end method

.method public static resolved(Ljava/lang/String;Ljava/lang/String;[J)Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;
    .locals 7
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "resolvedName"    # Ljava/lang/String;
    .param p2, "ids"    # [J

    .prologue
    .line 76
    const/4 v4, 0x0

    .line 77
    .local v4, "tmp":Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;
    sget-object v5, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    if-eqz v5, :cond_3

    .line 78
    sget-object v4, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    .line 79
    const/4 v5, 0x0

    sput-object v5, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    .line 81
    const-string/jumbo v5, "TITLE"

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 82
    sget-object v5, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->TITLE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    iput-object v5, v4, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->resolvedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    .line 91
    :goto_0
    iput-object p1, v4, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->resolvedName:Ljava/lang/String;

    .line 92
    move-object v0, p2

    .local v0, "arr$":[J
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v3, :cond_3

    aget-wide v5, v0, v1

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 93
    .local v2, "id":Ljava/lang/Long;
    iget-object v5, v4, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->ids:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 83
    .end local v0    # "arr$":[J
    .end local v1    # "i$":I
    .end local v2    # "id":Ljava/lang/Long;
    .end local v3    # "len$":I
    :cond_0
    const-string/jumbo v5, "ALBUM"

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 84
    sget-object v5, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ALBUM:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    iput-object v5, v4, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->resolvedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    goto :goto_0

    .line 85
    :cond_1
    const-string/jumbo v5, "ARTIST"

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 86
    sget-object v5, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ARTIST:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    iput-object v5, v4, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->resolvedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    goto :goto_0

    .line 88
    :cond_2
    sget-object v5, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->ELSE:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    iput-object v5, v4, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->resolvedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    goto :goto_0

    .line 96
    :cond_3
    return-object v4
.end method


# virtual methods
.method protected getAcceptedTextXML()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 108
    iget-object v2, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->ids:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/util/MusicUtil;->getPlayCount(Landroid/content/Context;Ljava/util/List;)I

    move-result v0

    .line 110
    .local v0, "playCount":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v2, "<AcceptedText pt=\"music:play%s\">"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requestedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->getPt()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string/jumbo v2, "<Tag u=\"req_%s\">%s</Tag>"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requestedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requestedName:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string/jumbo v2, "<Tag u=\"%s\" c=\"%d\">%s</Tag>"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->resolvedType:Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText$Type;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->resolvedName:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string/jumbo v2, "</AcceptedText>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
