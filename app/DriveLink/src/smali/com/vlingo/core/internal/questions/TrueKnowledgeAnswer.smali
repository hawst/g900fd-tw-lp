.class public Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;
.super Lcom/vlingo/core/internal/questions/AnswerAdaptation;
.source "TrueKnowledgeAnswer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$1;,
        Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/questions/Answer;)V
    .locals 0
    .param p1, "answer"    # Lcom/vlingo/core/internal/questions/Answer;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/questions/AnswerAdaptation;-><init>(Lcom/vlingo/core/internal/questions/Answer;)V

    .line 18
    return-void
.end method


# virtual methods
.method public getSimpleResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    invoke-super {p0}, Lcom/vlingo/core/internal/questions/AnswerAdaptation;->getSimpleResponse()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;->stripText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public is(Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;)Z
    .locals 5
    .param p1, "type"    # Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 27
    const/4 v0, 0x0

    .line 29
    .local v0, "result":Z
    sget-object v3, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$1;->$SwitchMap$com$vlingo$core$internal$questions$TrueKnowledgeAnswer$Type:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 45
    :goto_0
    return v0

    .line 32
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v3

    array-length v3, v3

    if-nez v3, :cond_1

    :cond_0
    move v0, v2

    .line 33
    :goto_1
    goto :goto_0

    :cond_1
    move v0, v1

    .line 32
    goto :goto_1

    .line 35
    :pswitch_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v3

    if-eqz v3, :cond_2

    move v0, v2

    .line 36
    :goto_2
    goto :goto_0

    :cond_2
    move v0, v1

    .line 35
    goto :goto_2

    .line 38
    :pswitch_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v3

    if-eqz v3, :cond_3

    move v0, v2

    .line 39
    :goto_3
    goto :goto_0

    :cond_3
    move v0, v1

    .line 38
    goto :goto_3

    .line 29
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
