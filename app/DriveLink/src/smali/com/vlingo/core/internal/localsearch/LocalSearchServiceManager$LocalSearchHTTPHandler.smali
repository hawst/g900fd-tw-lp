.class Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;
.super Ljava/lang/Object;
.source "LocalSearchServiceManager.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalSearchHTTPHandler"
.end annotation


# instance fields
.field m_currentBusinessItem:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

.field m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

.field m_responseParser:Lcom/vlingo/core/internal/localsearch/LocalSearchParser;

.field private processingDone:Z

.field final synthetic this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;Lcom/vlingo/core/internal/localsearch/LocalSearchParser;)V
    .locals 1
    .param p2, "currentBusinessItem"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .param p3, "listener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;
    .param p4, "parser"    # Lcom/vlingo/core/internal/localsearch/LocalSearchParser;

    .prologue
    const/4 v0, 0x0

    .line 254
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 246
    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_currentBusinessItem:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .line 255
    iput-object p3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 256
    iput-object p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_currentBusinessItem:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .line 257
    iput-object p4, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_responseParser:Lcom/vlingo/core/internal/localsearch/LocalSearchParser;

    .line 258
    return-void
.end method

.method constructor <init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;Lcom/vlingo/core/internal/localsearch/LocalSearchParser;)V
    .locals 1
    .param p2, "listener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;
    .param p3, "parser"    # Lcom/vlingo/core/internal/localsearch/LocalSearchParser;

    .prologue
    const/4 v0, 0x0

    .line 249
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 246
    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_currentBusinessItem:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .line 250
    iput-object p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 251
    iput-object p3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_responseParser:Lcom/vlingo/core/internal/localsearch/LocalSearchParser;

    .line 252
    return-void
.end method


# virtual methods
.method public declared-synchronized onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 333
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->processingDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 341
    :goto_0
    monitor-exit p0

    return-void

    .line 337
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Cancelled"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 338
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->processingDone:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 321
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->processingDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 329
    :goto_0
    monitor-exit p0

    return-void

    .line 325
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Failure"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->processingDone:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 321
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 4
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->processingDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 297
    :goto_0
    monitor-exit p0

    return-void

    .line 264
    :cond_0
    const/4 v0, 0x0

    .line 265
    .local v0, "failureType":Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;
    const/4 v1, 0x0

    .line 266
    .local v1, "results":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;>;"
    :try_start_1
    iget v2, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_6

    .line 269
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_responseParser:Lcom/vlingo/core/internal/localsearch/LocalSearchParser;

    iget-object v3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_currentBusinessItem:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    invoke-interface {v2, p2, v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchParser;->parseLocalSearchResponse(Lcom/vlingo/sdk/internal/http/HttpResponse;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)Landroid/util/Pair;

    move-result-object v1

    .line 270
    if-eqz v1, :cond_5

    .line 271
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v2, :cond_2

    .line 272
    const-string/jumbo v3, "USERCACHEDLOCATION"

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getSelectedLocationType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v3, "DEFAULT"

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getSelectedLocationType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v3, "USERPHONENUMBER"

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getSelectedLocationType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 277
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->BADLOCATIONTYPE:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    .line 283
    :cond_2
    :goto_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->processingDone:Z

    .line 289
    :cond_3
    :goto_2
    if-nez v1, :cond_4

    .line 290
    sget-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->NORESULTS:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    .line 292
    :cond_4
    if-eqz v0, :cond_7

    .line 293
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 261
    .end local v0    # "failureType":Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;
    .end local v1    # "results":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 281
    .restart local v0    # "failureType":Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;
    .restart local v1    # "results":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;>;"
    :cond_5
    :try_start_2
    sget-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->NORESULTS:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    goto :goto_1

    .line 285
    :cond_6
    iget-boolean v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->processingDone:Z

    if-nez v2, :cond_3

    .line 286
    sget-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->BADRESPONSE:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    goto :goto_2

    .line 295
    :cond_7
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const/4 v3, 0x1

    invoke-interface {v2, v3, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestComplete(ZLjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 3
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    const/4 v2, 0x0

    .line 308
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->processingDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 316
    :goto_0
    monitor-exit p0

    return v2

    .line 312
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Timeout"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 313
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->processingDone:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 308
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestScheduled()V

    .line 304
    return-void
.end method
