.class public Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;
.super Ljava/lang/Object;
.source "StateController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/StateController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Rule"
.end annotation


# instance fields
.field private final fieldId:Ljava/lang/String;

.field private final prompt:Ljava/lang/String;

.field private final required:Z

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "prompt"    # Ljava/lang/String;
    .param p3, "required"    # Z

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->fieldId:Ljava/lang/String;

    .line 127
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->prompt:Ljava/lang/String;

    .line 128
    iput-boolean p3, p0, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->required:Z

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->value:Ljava/lang/String;

    .line 130
    return-void
.end method


# virtual methods
.method getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->fieldId:Ljava/lang/String;

    return-object v0
.end method

.method getPrompt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->prompt:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->value:Ljava/lang/String;

    return-object v0
.end method

.method isRequired()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->required:Z

    return v0
.end method

.method setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->value:Ljava/lang/String;

    .line 136
    return-void
.end method
