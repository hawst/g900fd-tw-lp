.class Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;
.super Ljava/lang/Object;
.source "ScheduleUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/schedule/ScheduleUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScheduleEventIndices"
.end annotation


# instance fields
.field final ACCESS_LEVEL_COL:I

.field final ALL_DAY_COL:I

.field final BEGIN_COL:I

.field final DESCRIPTION_COL:I

.field final END_COL:I

.field final EVENT_ID_COL:I

.field final EVENT_LOCATION_COL:I

.field final GUESTS_CAN_MODIFY_COL:I

.field final ORGANIZER_COL:I

.field final OWNER_ACCOUNT_COL:I

.field final TITLE_COL:I


# direct methods
.method constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472
    const-string/jumbo v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->TITLE_COL:I

    .line 473
    const-string/jumbo v0, "eventLocation"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->EVENT_LOCATION_COL:I

    .line 474
    const-string/jumbo v0, "description"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->DESCRIPTION_COL:I

    .line 475
    const-string/jumbo v0, "organizer"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->ORGANIZER_COL:I

    .line 476
    const-string/jumbo v0, "event_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->EVENT_ID_COL:I

    .line 477
    const-string/jumbo v0, "begin"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->BEGIN_COL:I

    .line 478
    const-string/jumbo v0, "end"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->END_COL:I

    .line 479
    const-string/jumbo v0, "allDay"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->ALL_DAY_COL:I

    .line 480
    const-string/jumbo v0, "calendar_access_level"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->ACCESS_LEVEL_COL:I

    .line 481
    const-string/jumbo v0, "ownerAccount"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->OWNER_ACCOUNT_COL:I

    .line 482
    const-string/jumbo v0, "guestsCanModify"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->GUESTS_CAN_MODIFY_COL:I

    .line 483
    return-void
.end method
