.class public Lcom/vlingo/core/internal/audio/MediaAudioRequest;
.super Lcom/vlingo/core/internal/audio/AudioRequest;
.source "MediaAudioRequest.java"


# instance fields
.field protected mFileToPlayResid:I


# direct methods
.method protected constructor <init>(I)V
    .locals 0
    .param p1, "fileToPlayResid"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioRequest;-><init>()V

    .line 13
    iput p1, p0, Lcom/vlingo/core/internal/audio/MediaAudioRequest;->mFileToPlayResid:I

    .line 14
    return-void
.end method

.method public static getRequest(I)Lcom/vlingo/core/internal/audio/MediaAudioRequest;
    .locals 2
    .param p0, "fileToPlayResid"    # I

    .prologue
    .line 17
    new-instance v0, Lcom/vlingo/core/internal/audio/MediaAudioRequest;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/audio/MediaAudioRequest;-><init>(I)V

    .line 18
    .local v0, "request":Lcom/vlingo/core/internal/audio/MediaAudioRequest;
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/vlingo/core/internal/audio/MediaAudioRequest;->requestAudioFocus:Z

    .line 21
    return-object v0
.end method


# virtual methods
.method public prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/AudioPlayer;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "player"    # Lcom/vlingo/core/internal/audio/AudioPlayer;

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method public setDataSource(Landroid/content/Context;Landroid/media/MediaPlayer;Lcom/vlingo/core/internal/audio/AudioPlayer;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaPlayer"    # Landroid/media/MediaPlayer;
    .param p3, "player"    # Lcom/vlingo/core/internal/audio/AudioPlayer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    const/4 v6, 0x0

    .line 33
    .local v6, "afd":Landroid/content/res/AssetFileDescriptor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/audio/MediaAudioRequest;->mFileToPlayResid:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    .line 34
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    move-result-wide v4

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    if-eqz v6, :cond_0

    :try_start_1
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 37
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    :try_start_2
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method
