.class Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$8;
.super Ljava/lang/Object;
.source "DialogTurn.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

.field final synthetic val$displayText:Ljava/lang/String;

.field final synthetic val$spokenText:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 446
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$8;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$8;->val$displayText:Ljava/lang/String;

    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$8;->val$spokenText:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 448
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$8;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$8;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$8;->val$displayText:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$8;->val$spokenText:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    :cond_0
    return-void
.end method
