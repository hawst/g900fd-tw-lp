.class public Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;
.super Ljava/lang/Object;
.source "InternalMdsoUtils.java"


# static fields
.field private static final MDSO_CONFIG_PROVIDER:Ljava/lang/String; = "com.vlingo.content.action.CONFIG_PROVIDER"

.field private static final MDSO_CONTACT_PROVIDER:Ljava/lang/String; = "com.vlingo.content.action.CONTACT_PROVIDER"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static becomeMaster()V
    .locals 3

    .prologue
    .line 111
    const-string/jumbo v1, "multiapp.am_master"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 112
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "language":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->setLanguageToAllNodes(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method private static buildContentValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentValues;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 269
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 270
    .local v0, "contentValues":Landroid/content/ContentValues;
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 271
    check-cast p1, Ljava/lang/String;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-virtual {v0, p0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :cond_0
    :goto_0
    return-object v0

    .line 272
    .restart local p1    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 273
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-virtual {v0, p0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 274
    .restart local p1    # "value":Ljava/lang/Object;
    :cond_2
    instance-of v1, p1, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 275
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-virtual {v0, p0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private static getBooleanSetting(Landroid/content/pm/ProviderInfo;Ljava/lang/String;)Z
    .locals 2
    .param p0, "provider"    # Landroid/content/pm/ProviderInfo;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 242
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getSetting(Landroid/content/pm/ProviderInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "settingValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getConfigProviders()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ProviderInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v5, "com.vlingo.content.action.CONFIG_PROVIDER"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 231
    .local v2, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v2, v6}, Landroid/content/pm/PackageManager;->queryIntentContentProviders(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 232
    .local v4, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 233
    .local v3, "mdsoProviders":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 236
    .local v1, "info":Landroid/content/pm/ResolveInfo;
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->providerInfo:Landroid/content/pm/ProviderInfo;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 238
    .end local v1    # "info":Landroid/content/pm/ResolveInfo;
    :cond_0
    return-object v3
.end method

.method private static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 281
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private static getMasterConfigProvider()Landroid/content/pm/ProviderInfo;
    .locals 5

    .prologue
    .line 209
    const/4 v1, 0x0

    .line 210
    .local v1, "masterConfigProvider":Landroid/content/pm/ProviderInfo;
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getConfigProviders()Ljava/util/List;

    move-result-object v3

    .line 211
    .local v3, "providers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ProviderInfo;

    .line 212
    .local v2, "providerInfo":Landroid/content/pm/ProviderInfo;
    const-string/jumbo v4, "multiapp.am_master"

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getBooleanSetting(Landroid/content/pm/ProviderInfo;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 213
    move-object v1, v2

    .line 217
    .end local v2    # "providerInfo":Landroid/content/pm/ProviderInfo;
    :cond_1
    if-nez v1, :cond_2

    .line 224
    :cond_2
    return-object v1
.end method

.method private static getMasterContactProvider()Landroid/content/pm/ProviderInfo;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 177
    const/4 v1, 0x0

    .line 178
    .local v1, "masterContactProvider":Landroid/content/pm/ProviderInfo;
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getMasterPackage()Ljava/lang/String;

    move-result-object v2

    .line 179
    .local v2, "masterPackage":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 180
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v4, "com.vlingo.content.action.CONTACT_PROVIDER"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v0, v6}, Landroid/content/pm/PackageManager;->queryIntentContentProviders(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 183
    .local v3, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 184
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v1, v4, Landroid/content/pm/ResolveInfo;->providerInfo:Landroid/content/pm/ProviderInfo;

    .line 187
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v3    # "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_0
    if-nez v1, :cond_1

    .line 194
    :cond_1
    return-object v1
.end method

.method public static getMasterContactProviderAuthority()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    const/4 v0, 0x0

    .line 42
    .local v0, "authority":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getMasterContactProvider()Landroid/content/pm/ProviderInfo;

    move-result-object v1

    .line 43
    .local v1, "provider":Landroid/content/pm/ProviderInfo;
    if-eqz v1, :cond_0

    .line 44
    iget-object v0, v1, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    .line 48
    :cond_0
    return-object v0
.end method

.method static getMasterLanguage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 133
    const/4 v0, 0x0

    .line 134
    .local v0, "masterLanguage":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getMasterConfigProvider()Landroid/content/pm/ProviderInfo;

    move-result-object v1

    .line 135
    .local v1, "provider":Landroid/content/pm/ProviderInfo;
    if-eqz v1, :cond_0

    .line 136
    const-string/jumbo v2, "language"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getSetting(Landroid/content/pm/ProviderInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    :cond_0
    return-object v0
.end method

.method private static getMasterPackage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 199
    .local v0, "masterPackage":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getMasterConfigProvider()Landroid/content/pm/ProviderInfo;

    move-result-object v1

    .line 200
    .local v1, "provider":Landroid/content/pm/ProviderInfo;
    if-eqz v1, :cond_0

    .line 201
    iget-object v0, v1, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    .line 205
    :cond_0
    return-object v0
.end method

.method private static getSetting(Landroid/content/pm/ProviderInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "provider"    # Landroid/content/pm/ProviderInfo;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 248
    :try_start_0
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "content://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "SETTINGS/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 250
    .local v0, "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 254
    .end local v0    # "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    :goto_0
    return-object v2

    .line 251
    :catch_0
    move-exception v1

    .line 252
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 254
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static isMaster()Z
    .locals 3

    .prologue
    .line 83
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMdsoApplication()Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    const/4 v0, 0x1

    .line 91
    .local v0, "isMaster":Z
    :goto_0
    return v0

    .line 88
    .end local v0    # "isMaster":Z
    :cond_0
    const-string/jumbo v1, "multiapp.am_master"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 91
    .restart local v0    # "isMaster":Z
    goto :goto_0
.end method

.method public static isMasterWithoutActiveSlave()Z
    .locals 6

    .prologue
    .line 58
    const/4 v4, 0x0

    .line 59
    .local v4, "retVal":Z
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 60
    const/4 v4, 0x1

    .line 61
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getConfigProviders()Ljava/util/List;

    move-result-object v3

    .line 63
    .local v3, "providers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ProviderInfo;

    .line 64
    .local v2, "provider":Landroid/content/pm/ProviderInfo;
    iget-object v5, v2, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "tos_accepted"

    invoke-static {v2, v5}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getBooleanSetting(Landroid/content/pm/ProviderInfo;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 66
    const/4 v4, 0x0

    .line 73
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "packageName":Ljava/lang/String;
    .end local v2    # "provider":Landroid/content/pm/ProviderInfo;
    .end local v3    # "providers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    :cond_1
    return v4
.end method

.method public static isSlave()Z
    .locals 2

    .prologue
    .line 32
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->otherMasterExists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 35
    .local v0, "isSlave":Z
    :goto_0
    return v0

    .line 32
    .end local v0    # "isSlave":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static otherMasterExists()Z
    .locals 4

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 98
    .local v0, "otherMasterExists":Z
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getMasterConfigProvider()Landroid/content/pm/ProviderInfo;

    move-result-object v2

    .line 99
    .local v2, "provider":Landroid/content/pm/ProviderInfo;
    if-eqz v2, :cond_0

    .line 100
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "packageName":Ljava/lang/String;
    iget-object v3, v2, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, 0x1

    .line 105
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 101
    .restart local v1    # "packageName":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static sendEventToMaster(Ljava/lang/String;)Z
    .locals 4
    .param p0, "eventName"    # Ljava/lang/String;

    .prologue
    .line 157
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getMasterPackage()Ljava/lang/String;

    move-result-object v2

    .line 158
    .local v2, "masterPackage":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 161
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "com.vlingo.mdso.action.SLAVE_EVENT"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 162
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "com.vlingo.mdso.extra.EVENT_NAME"

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    const-class v3, Lcom/vlingo/core/facade/lmtt/LmttReceiver;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 165
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 166
    const/4 v3, 0x1

    .line 168
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method static setLanguageToAllNodes(Ljava/lang/String;)V
    .locals 5
    .param p0, "language"    # Ljava/lang/String;

    .prologue
    .line 119
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getConfigProviders()Ljava/util/List;

    move-result-object v3

    .line 121
    .local v3, "providers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ProviderInfo;

    .line 122
    .local v2, "provider":Landroid/content/pm/ProviderInfo;
    iget-object v4, v2, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 125
    const-string/jumbo v4, "language"

    invoke-static {v2, v4, p0}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->setSetting(Landroid/content/pm/ProviderInfo;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    .end local v2    # "provider":Landroid/content/pm/ProviderInfo;
    :cond_1
    return-void
.end method

.method static setMasterSetting(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 146
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getMasterConfigProvider()Landroid/content/pm/ProviderInfo;

    move-result-object v0

    .line 147
    .local v0, "provider":Landroid/content/pm/ProviderInfo;
    if-eqz v0, :cond_0

    .line 150
    invoke-static {v0, p0, p1}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->setSetting(Landroid/content/pm/ProviderInfo;Ljava/lang/String;Ljava/lang/Object;)V

    .line 152
    :cond_0
    return-void
.end method

.method private static setSetting(Landroid/content/pm/ProviderInfo;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 6
    .param p0, "provider"    # Landroid/content/pm/ProviderInfo;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 259
    :try_start_0
    invoke-static {p1, p2}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->buildContentValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentValues;

    move-result-object v0

    .line 260
    .local v0, "contentValues":Landroid/content/ContentValues;
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "content://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "SETTINGS/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 263
    :catch_0
    move-exception v1

    .line 264
    .local v1, "iArgEx":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method
