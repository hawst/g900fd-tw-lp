.class Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;
.super Ljava/lang/Object;
.source "ContactDBSyncUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContactCompareData"
.end annotation


# instance fields
.field private m_lookup:Ljava/lang/String;

.field private m_starred:Ljava/lang/Integer;

.field private m_timesContacted:Ljava/lang/Integer;

.field private m_version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p1, "version"    # Ljava/lang/Integer;
    .param p2, "starred"    # Ljava/lang/Integer;
    .param p3, "timesContacted"    # Ljava/lang/Integer;
    .param p4, "lookup"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->m_version:Ljava/lang/Integer;

    .line 134
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->m_starred:Ljava/lang/Integer;

    .line 135
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->m_timesContacted:Ljava/lang/Integer;

    .line 136
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->m_lookup:Ljava/lang/String;

    .line 137
    return-void
.end method


# virtual methods
.method public getLookup()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->m_lookup:Ljava/lang/String;

    return-object v0
.end method

.method public getStarred()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->m_starred:Ljava/lang/Integer;

    return-object v0
.end method

.method public getTimesContacted()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->m_timesContacted:Ljava/lang/Integer;

    return-object v0
.end method

.method public getVersion()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil$ContactCompareData;->m_version:Ljava/lang/Integer;

    return-object v0
.end method
