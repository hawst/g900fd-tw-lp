.class public Lcom/vlingo/core/internal/util/ApplicationAdapter;
.super Ljava/lang/Object;
.source "ApplicationAdapter.java"

# interfaces
.implements Lcom/vlingo/core/facade/IApplicationAdapter;


# static fields
.field public static VERSION:Ljava/lang/String;

.field protected static instance:Lcom/vlingo/core/internal/util/ApplicationAdapter;


# instance fields
.field protected businessItemCache:Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;

.field context:Landroid/content/Context;

.field theVlingoApplicationInterface:Lcom/vlingo/core/internal/util/VlingoApplicationInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string/jumbo v0, "%VLINGOANDROIDCORE_VERSION%"

    sput-object v0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->VERSION:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->instance:Lcom/vlingo/core/internal/util/ApplicationAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->businessItemCache:Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;

    .line 37
    iput-object v0, p0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->context:Landroid/content/Context;

    .line 38
    iput-object v0, p0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->theVlingoApplicationInterface:Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    return-void
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->instance:Lcom/vlingo/core/internal/util/ApplicationAdapter;

    .line 60
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->instance:Lcom/vlingo/core/internal/util/ApplicationAdapter;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/vlingo/core/internal/util/ApplicationAdapter;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->instance:Lcom/vlingo/core/internal/util/ApplicationAdapter;

    .line 55
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->instance:Lcom/vlingo/core/internal/util/ApplicationAdapter;

    return-object v0
.end method


# virtual methods
.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getBusinessItemCache()Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->businessItemCache:Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;

    invoke-direct {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->businessItemCache:Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->businessItemCache:Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;

    return-object v0
.end method

.method public getConnectionTestFieldID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->theVlingoApplicationInterface:Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    return-object v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 0
    .param p1, "contextParam"    # Landroid/content/Context;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->context:Landroid/content/Context;

    .line 67
    return-void
.end method

.method public isAudioStreamingEnabled()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public setInterface(Lcom/vlingo/core/internal/util/VlingoApplicationInterface;)V
    .locals 0
    .param p1, "vai"    # Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/vlingo/core/internal/util/ApplicationAdapter;->theVlingoApplicationInterface:Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    .line 71
    return-void
.end method

.method public showFatalDialog(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 91
    return-void
.end method
