.class final enum Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;
.super Ljava/lang/Enum;
.source "LMTTItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmtt/LMTTItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ChangeType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

.field public static final enum DELETE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

.field public static final enum INSERT:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

.field public static final enum NOCHANGE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

.field public static final enum UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;


# instance fields
.field private final delta:I

.field private final upType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 25
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    const-string/jumbo v1, "UPDATE"

    sget-object v2, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->UP:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;-><init>(Ljava/lang/String;IILcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    const-string/jumbo v1, "INSERT"

    sget-object v2, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->INS:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;-><init>(Ljava/lang/String;IILcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->INSERT:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    const-string/jumbo v1, "DELETE"

    const/4 v2, -0x1

    sget-object v3, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->DEL:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;-><init>(Ljava/lang/String;IILcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    const-string/jumbo v1, "NOCHANGE"

    sget-object v2, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->NOOP:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    invoke-direct {v0, v1, v7, v4, v2}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;-><init>(Ljava/lang/String;IILcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->NOCHANGE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->INSERT:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->NOCHANGE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->$VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;)V
    .locals 0
    .param p3, "thisDelta"    # I
    .param p4, "upType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->delta:I

    .line 32
    iput-object p4, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->upType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    .line 33
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->$VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    return-object v0
.end method


# virtual methods
.method dbUpdate(Lcom/vlingo/core/internal/lmtt/LMTTItem;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "item"    # Lcom/vlingo/core/internal/lmtt/LMTTItem;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->upType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType$DBUpdateType;->dbUpdate(Lcom/vlingo/core/internal/lmtt/LMTTItem;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 41
    return-void
.end method

.method runningSum(I)I
    .locals 1
    .param p1, "currentSum"    # I

    .prologue
    .line 36
    iget v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->delta:I

    add-int/2addr v0, p1

    return v0
.end method
