.class public Lcom/vlingo/core/internal/dialogmanager/tasks/ActionTask;
.super Lcom/vlingo/core/internal/util/TaskQueue$Task;
.source "ActionTask.java"


# instance fields
.field private final action:Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;)V
    .locals 0
    .param p1, "action"    # Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue$Task;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/ActionTask;->action:Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    .line 21
    return-void
.end method


# virtual methods
.method protected onAborted()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/ActionTask;->action:Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;->abort()V

    .line 35
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/ActionTask;->action:Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;->readyToExec()V

    .line 29
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/ActionTask;->notifyFinished()V

    .line 30
    return-void
.end method
