.class public Lcom/vlingo/core/internal/util/ShakeMotionDetector;
.super Ljava/lang/Object;
.source "ShakeMotionDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/ShakeMotionDetector$ShakeMotionDetectorListener;
    }
.end annotation


# instance fields
.field private isListening:Z

.field private final listener:Lcom/vlingo/core/internal/util/ShakeMotionDetector$ShakeMotionDetectorListener;

.field private final mSensorListener:Landroid/hardware/SensorEventListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private m_LastSensorTime:J

.field private m_LastValues:[F

.field private s_Accelerometer:Landroid/hardware/Sensor;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/util/ShakeMotionDetector$ShakeMotionDetectorListener;Landroid/content/Context;)V
    .locals 2
    .param p1, "listener"    # Lcom/vlingo/core/internal/util/ShakeMotionDetector$ShakeMotionDetectorListener;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;-><init>(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->mSensorListener:Landroid/hardware/SensorEventListener;

    .line 67
    const-string/jumbo v0, "sensor"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->mSensorManager:Landroid/hardware/SensorManager;

    .line 69
    iput-object p1, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->listener:Lcom/vlingo/core/internal/util/ShakeMotionDetector$ShakeMotionDetectorListener;

    .line 70
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->s_Accelerometer:Landroid/hardware/Sensor;

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)Landroid/hardware/Sensor;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->s_Accelerometer:Landroid/hardware/Sensor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)J
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    .prologue
    .line 14
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastSensorTime:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/vlingo/core/internal/util/ShakeMotionDetector;J)J
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/ShakeMotionDetector;
    .param p1, "x1"    # J

    .prologue
    .line 14
    iput-wide p1, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastSensorTime:J

    return-wide p1
.end method

.method static synthetic access$114(Lcom/vlingo/core/internal/util/ShakeMotionDetector;J)J
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/ShakeMotionDetector;
    .param p1, "x1"    # J

    .prologue
    .line 14
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastSensorTime:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastSensorTime:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)Landroid/hardware/SensorManager;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->mSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)[F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastValues:[F

    return-object v0
.end method

.method static synthetic access$302(Lcom/vlingo/core/internal/util/ShakeMotionDetector;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/ShakeMotionDetector;
    .param p1, "x1"    # [F

    .prologue
    .line 14
    iput-object p1, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastValues:[F

    return-object p1
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)Lcom/vlingo/core/internal/util/ShakeMotionDetector$ShakeMotionDetectorListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->listener:Lcom/vlingo/core/internal/util/ShakeMotionDetector$ShakeMotionDetectorListener;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized destroy()V
    .locals 1

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->stopListening()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    monitor-exit p0

    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startListening()V
    .locals 4

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->isListening:Z

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->isListening:Z

    .line 76
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->mSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->s_Accelerometer:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 79
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastSensorTime:J

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastValues:[F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :cond_0
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopListening()V
    .locals 2

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->isListening:Z

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->mSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->isListening:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_0
    monitor-exit p0

    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
