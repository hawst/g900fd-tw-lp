.class public Lcom/vlingo/core/internal/util/SayHello;
.super Ljava/lang/Object;
.source "SayHello.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/SayHello$VVSListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static isSayHelloDone:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/vlingo/core/internal/util/SayHello;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/SayHello;->TAG:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/util/SayHello;->isSayHelloDone:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    return-void
.end method

.method public static sendHello()Z
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 61
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "language":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/sdk/VLSdk;->getVLServices()Lcom/vlingo/sdk/services/VLServices;

    move-result-object v2

    .line 64
    .local v2, "sdkServices":Lcom/vlingo/sdk/services/VLServices;
    invoke-static {v4}, Lcom/vlingo/core/internal/VlingoAndroidCore;->preventSdkReload(Z)V

    .line 65
    new-instance v3, Lcom/vlingo/core/internal/util/SayHello$1;

    invoke-direct {v3}, Lcom/vlingo/core/internal/util/SayHello$1;-><init>()V

    .line 79
    .local v3, "servicesListener":Lcom/vlingo/sdk/services/VLServicesListener;
    const-string/jumbo v6, "hello_request_complete"

    invoke-static {v6, v4}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 81
    :try_start_0
    invoke-interface {v2, v1, v3}, Lcom/vlingo/sdk/services/VLServices;->sendHello(Ljava/lang/String;Lcom/vlingo/sdk/services/VLServicesListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_0
    return v4

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "ex":Ljava/lang/IllegalStateException;
    sget-object v4, Lcom/vlingo/core/internal/util/SayHello;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "did not send Hello, IllegalStateException"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    invoke-static {v5}, Lcom/vlingo/core/internal/VlingoAndroidCore;->preventSdkReload(Z)V

    move v4, v5

    .line 85
    goto :goto_0
.end method
