.class public Lcom/vlingo/core/internal/util/ClientSuppliedValues;
.super Ljava/lang/Object;
.source "ClientSuppliedValues.java"


# static fields
.field private static clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static disableAppCarMode()V
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->disableAppCarMode()V

    .line 96
    return-void
.end method

.method public static disablePhoneDrivingMode()V
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->disablePhoneDrivingMode()V

    .line 108
    return-void
.end method

.method public static enableAppCarMode()V
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->enableAppCarMode()V

    .line 100
    return-void
.end method

.method public static enablePhoneDrivingMode()V
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->enablePhoneDrivingMode()V

    .line 112
    return-void
.end method

.method public static getADMController()Lcom/vlingo/core/internal/util/ADMController;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getADMController()Lcom/vlingo/core/internal/util/ADMController;

    move-result-object v0

    return-object v0
.end method

.method public static getAssets()Landroid/content/res/AssetManager;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    return-object v0
.end method

.method public static getCarModePackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getCarModePackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getClientAudioValues()Lcom/vlingo/core/facade/audio/ClientAudioValues;
    .locals 1

    .prologue
    .line 227
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getClientAudioValues()Lcom/vlingo/core/facade/audio/ClientAudioValues;

    move-result-object v0

    return-object v0
.end method

.method public static getConfiguration()Landroid/content/res/Configuration;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0
.end method

.method public static getContactProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getContactProviderName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getDefaultFieldId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDrivingModeWidgetMax()I
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getDrivingModeWidgetMax()I

    move-result v0

    return v0
.end method

.method public static getFmRadioApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getFmRadioApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v0

    return-object v0
.end method

.method public static getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p0, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 152
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0, p0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 153
    return-void
.end method

.method public static getHomeAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getHomeAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLaunchingClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getLaunchingClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public static getMaxDisplayNumber()I
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getMaxDisplayNumber()I

    move-result v0

    return v0
.end method

.method public static getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v0

    return-object v0
.end method

.method public static getMmsSubject(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 87
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0, p0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getMmsSubject(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRegularWidgetMax()I
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getRegularWidgetMax()I

    move-result v0

    return v0
.end method

.method public static getRelativePriority()B
    .locals 1

    .prologue
    .line 255
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getRelativePriority()B

    move-result v0

    return v0
.end method

.method public static getSafeReaderHandler(ZLjava/util/LinkedList;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    .locals 1
    .param p0, "isSilentHandler"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0, p0, p1}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getSafeReaderHandler(ZLjava/util/LinkedList;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;

    move-result-object v0

    return-object v0
.end method

.method public static getSettingsProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getSettingsProviderName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWakeLockManager()Lcom/vlingo/core/internal/display/WakeLockManager;
    .locals 2

    .prologue
    .line 55
    sget-object v1, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    if-nez v1, :cond_1

    .line 56
    invoke-static {}, Lcom/vlingo/core/internal/display/WakeLockManagerNoop;->getInstance()Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    .line 63
    :cond_0
    :goto_0
    return-object v0

    .line 59
    :cond_1
    sget-object v1, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v1}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->getWakeLockManager()Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    .line 60
    .local v0, "clientWakeLockManager":Lcom/vlingo/core/internal/display/WakeLockManager;
    if-nez v0, :cond_0

    .line 61
    invoke-static {}, Lcom/vlingo/core/internal/display/WakeLockManagerNoop;->getInstance()Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    goto :goto_0
.end method

.method public static isAppCarModeEnabled()Z
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isAppCarModeEnabled()Z

    move-result v0

    return v0
.end method

.method public static isBlockingMode()Z
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isBlockingMode()Z

    move-result v0

    return v0
.end method

.method public static isChinesePhone()Z
    .locals 1

    .prologue
    .line 268
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isChinesePhone()Z

    move-result v0

    return v0
.end method

.method public static isCurrentModelNotExactlySynchronizedWithPlayingTts()Z
    .locals 1

    .prologue
    .line 202
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isCurrentModelNotExactlySynchronizedWithPlayingTts()Z

    move-result v0

    return v0
.end method

.method public static isDSPSeamlessWakeupEnabled()Z
    .locals 1

    .prologue
    .line 264
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isDSPSeamlessWakeupEnabled()Z

    move-result v0

    return v0
.end method

.method public static isEmbeddedBrowserUsedForSearchResults()Z
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isEmbeddedBrowserUsedForSearchResults()Z

    move-result v0

    return v0
.end method

.method public static isEyesFree()Z
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isEyesFree()Z

    move-result v0

    return v0
.end method

.method public static isGearProviderExistOnPhone()Z
    .locals 1

    .prologue
    .line 243
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isGearProviderExistOnPhone()Z

    move-result v0

    return v0
.end method

.method public static isIUXComplete()Z
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isIUXComplete()Z

    move-result v0

    return v0
.end method

.method public static isInDmFlowChanging()Z
    .locals 1

    .prologue
    .line 231
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isInDmFlowChanging()Z

    move-result v0

    return v0
.end method

.method public static isLanguageChangeAllowed()Z
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isLanguageChangeAllowed()Z

    move-result v0

    return v0
.end method

.method public static isMdsoApplication()Z
    .locals 1

    .prologue
    .line 272
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isMdsoApplication()Z

    move-result v0

    return v0
.end method

.method public static isMessageReadbackFlowEnabled()Z
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isMessageReadbackFlowEnabled()Z

    move-result v0

    return v0
.end method

.method public static isMessagingLocked()Z
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isMessagingLocked()Z

    move-result v0

    return v0
.end method

.method public static isMiniTabletDevice()Z
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isMiniTabletDevice()Z

    move-result v0

    return v0
.end method

.method public static isPhoneDrivingModeEnabled()Z
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isPhoneDrivingModeEnabled()Z

    move-result v0

    return v0
.end method

.method public static isReadMessageBodyEnabled()Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method public static isSeamless()Z
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isSeamless()Z

    move-result v0

    return v0
.end method

.method public static isTalkbackOn()Z
    .locals 1

    .prologue
    .line 177
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isTalkbackOn()Z

    move-result v0

    return v0
.end method

.method public static isTutorialMode()Z
    .locals 1

    .prologue
    .line 239
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isTutorialMode()Z

    move-result v0

    return v0
.end method

.method public static isVideoCallingSupported()Z
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isVideoCallingSupported()Z

    move-result v0

    return v0
.end method

.method public static isViewCoverOpened()Z
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->isViewCoverOpened()Z

    move-result v0

    return v0
.end method

.method public static releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p0, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 157
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0, p0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 158
    return-void
.end method

.method public static setInDmFlowChanging(Z)V
    .locals 1
    .param p0, "inDmFlowChanging"    # Z

    .prologue
    .line 235
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0, p0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->setInDmFlowChanging(Z)V

    .line 236
    return-void
.end method

.method public static setInterface(Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;)V
    .locals 0
    .param p0, "csvi"    # Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    .prologue
    .line 30
    sput-object p0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    .line 31
    invoke-static {p0}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->setAllowsLocationInstance(Lcom/vlingo/sdk/util/AllowsLocation;)V

    .line 32
    return-void
.end method

.method public static shouldIncomingMessagesReadout()Z
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->shouldIncomingMessagesReadout()Z

    move-result v0

    return v0
.end method

.method public static shouldSetWideBandSpeechToUse16khzVoice()Z
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->shouldSetWideBandSpeechToUse16khzVoice()Z

    move-result v0

    return v0
.end method

.method public static showViewCoverUi()V
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->showViewCoverUi()V

    .line 124
    return-void
.end method

.method public static supportsSVoiceAssociatedServiceOnly()Z
    .locals 1

    .prologue
    .line 197
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    return v0
.end method

.method public static updateCurrentLocale(Landroid/content/res/Resources;)V
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 139
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0, p0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 140
    return-void
.end method

.method public static useGoogleSearch()Z
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->clientInterface:Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;

    invoke-interface {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;->useGoogleSearch()Z

    move-result v0

    return v0
.end method
