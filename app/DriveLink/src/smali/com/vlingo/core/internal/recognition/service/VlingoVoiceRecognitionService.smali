.class public Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;
.super Landroid/speech/RecognitionService;
.source "VlingoVoiceRecognitionService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$1;,
        Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;,
        Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;
    }
.end annotation


# static fields
.field public static final ACTION_TEST:Ljava/lang/String; = "com.vlingo.core.internal.recognition.service.vlingovoicerecognitionservice.action.TEST"

.field public static final EXTRA_DEBUG_ACTION:Ljava/lang/String; = "com.vlingo.core.internal.recognition.service.vlingovoicerecognitionservice.extra.DEBUG_ACTION"

.field public static final KNOWN_INVALID_DURATION:I = -0x1

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_LISTENING:I = 0x1

.field private static final STATE_THINKING:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

.field private mStateListener:Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;

.field private m_recoMgr:Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

.field private m_state:I

.field private serviceCallbackListener:Landroid/speech/RecognitionService$Callback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/speech/RecognitionService;-><init>()V

    .line 39
    new-instance v0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;-><init>(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->mStateListener:Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I

    .line 52
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_recoMgr:Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    .line 280
    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    .prologue
    .line 31
    iget v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I

    return v0
.end method

.method static synthetic access$202(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;
    .param p1, "x1"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I

    return p1
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V

    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static enable()V
    .locals 5

    .prologue
    .line 59
    :try_start_0
    new-instance v0, Landroid/content/ComponentName;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    .local v0, "comp":Landroid/content/ComponentName;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v1

    .line 64
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private declared-synchronized endRecognition()V
    .locals 2

    .prologue
    .line 193
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I

    .line 197
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->mStateListener:Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->removeRecognitionStateListener(Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :cond_0
    monitor-exit p0

    return-void

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getAudioFocusManager()Lcom/vlingo/core/internal/audio/AudioFocusManager;
    .locals 1

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    if-nez v0, :cond_0

    .line 183
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected onCancel(Landroid/speech/RecognitionService$Callback;)V
    .locals 2
    .param p1, "callback"    # Landroid/speech/RecognitionService$Callback;

    .prologue
    .line 73
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->mStateListener:Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->removeRecognitionStateListener(Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;)V

    .line 75
    monitor-enter p0

    .line 76
    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;

    .line 77
    iget v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I

    if-eqz v0, :cond_0

    .line 81
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->cancelRecognition()V

    .line 82
    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V

    .line 87
    :cond_0
    monitor-exit p0

    .line 88
    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->onCancel(Landroid/speech/RecognitionService$Callback;)V

    .line 96
    invoke-super {p0}, Landroid/speech/RecognitionService;->onDestroy()V

    .line 97
    return-void
.end method

.method protected declared-synchronized onStartListening(Landroid/content/Intent;Landroid/speech/RecognitionService$Callback;)V
    .locals 9
    .param p1, "recognizerIntent"    # Landroid/content/Intent;
    .param p2, "callback"    # Landroid/speech/RecognitionService$Callback;

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    iget v6, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_recoMgr:Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isActive()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-eqz v6, :cond_2

    .line 108
    :cond_0
    const/16 v6, 0x8

    :try_start_1
    invoke-virtual {p2, v6}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 109
    :catch_0
    move-exception v2

    .line 110
    .local v2, "ex":Landroid/os/RemoteException;
    :try_start_2
    sget-object v6, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VLG_EXCEPTION"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 104
    .end local v2    # "ex":Landroid/os/RemoteException;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 115
    :cond_2
    :try_start_3
    iput-object p2, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;

    .line 119
    new-instance v5, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;-><init>(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$1;)V

    .line 120
    .local v5, "myResultsHandler":Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;
    invoke-static {v5}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->setResultDispatcher(Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;)V

    .line 121
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->mStateListener:Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->addRecognitionStateListener(Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;)V

    .line 123
    const-string/jumbo v6, "com.vlingo.client.actions.RECOGNIZE.pcm_wavefile"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 124
    .local v4, "fileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 125
    .local v0, "audioSourceInfo":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    const/4 v1, -0x1

    .line 126
    .local v1, "duration":I
    if-eqz v4, :cond_5

    .line 129
    sget-object v6, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_AUTO:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    invoke-static {v4, v6}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getFileSource(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    .line 140
    :cond_3
    :goto_1
    const-string/jumbo v6, "com.vlingo.client.actions.RECOGNIZE.field_id"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 141
    .local v3, "fieldId":Ljava/lang/String;
    if-nez v3, :cond_4

    .line 142
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v6}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v3

    .line 152
    :cond_4
    invoke-static {p0, v3, v0, v1}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->initiateRecognition(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo;I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 155
    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 158
    const/4 v6, 0x2

    :try_start_4
    invoke-virtual {p2, v6}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 160
    :catch_1
    move-exception v2

    .line 161
    .restart local v2    # "ex":Landroid/os/RemoteException;
    :try_start_5
    sget-object v6, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VLG_EXCEPTION"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 131
    .end local v2    # "ex":Landroid/os/RemoteException;
    .end local v3    # "fieldId":Ljava/lang/String;
    :cond_5
    const-string/jumbo v6, "com.vlingo.client.actions.RECOGNIZE.amr_wavefile"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 132
    if-eqz v4, :cond_3

    .line 135
    const-string/jumbo v6, "com.vlingo.client.actions.RECOGNIZE.duration_ms"

    const/4 v7, -0x1

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 136
    sget-object v6, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->AMR:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    invoke-static {v4, v6}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getFileSource(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    goto :goto_1
.end method

.method protected declared-synchronized onStopListening(Landroid/speech/RecognitionService$Callback;)V
    .locals 2
    .param p1, "callback"    # Landroid/speech/RecognitionService$Callback;

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;

    .line 175
    iget v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 176
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I

    .line 177
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->processRecognition()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    :cond_0
    monitor-exit p0

    return-void

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
