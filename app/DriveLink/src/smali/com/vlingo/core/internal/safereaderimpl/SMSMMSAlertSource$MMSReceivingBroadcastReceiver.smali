.class Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SMSMMSAlertSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MMSReceivingBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)V
    .locals 0
    .param p2, "safeReaderAlertChangeListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    .line 153
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 154
    # setter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderMMSAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    invoke-static {p1, p2}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$102(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    .line 155
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 159
    if-nez p2, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "action":Ljava/lang/String;
    const/4 v2, 0x0

    .line 165
    .local v2, "isMmsRecevied":Z
    const-string/jumbo v4, "com.android.mms.RECEIVED_MSG"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 168
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 169
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_2

    .line 170
    const-string/jumbo v4, "msg_type"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 171
    .local v3, "msgType":Ljava/lang/String;
    const-string/jumbo v4, "mms"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 172
    const/4 v2, 0x1

    .line 177
    .end local v1    # "extras":Landroid/os/Bundle;
    .end local v3    # "msgType":Ljava/lang/String;
    :cond_2
    const-string/jumbo v4, "com.android.mms.MMS_BR_FOR_VLINGO"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    if-eqz v2, :cond_0

    .line 181
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 185
    new-instance v4, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver$1;

    invoke-direct {v4, p0}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver$1;-><init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;)V

    invoke-static {v4}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
