.class Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4$1;
.super Ljava/lang/Object;
.source "PlayTTSTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTts:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->access$500(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 172
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;

    iget-object v1, v1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 173
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->access$602(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;)Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    .line 174
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$4;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->notifyFinished()V
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->access$700(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V

    .line 177
    :cond_0
    return-void
.end method
