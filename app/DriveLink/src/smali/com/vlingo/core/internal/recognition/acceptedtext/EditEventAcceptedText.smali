.class public Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;
.super Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;
.source "EditEventAcceptedText.java"


# instance fields
.field private newdate:Ljava/lang/String;

.field private newduration:Ljava/lang/String;

.field private newendtime:Ljava/lang/String;

.field private newinvitee:Ljava/lang/String;

.field private newinviteetype:Ljava/lang/String;

.field private newloc:Ljava/lang/String;

.field private newtime:Ljava/lang/String;

.field private newtitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "date"    # Ljava/lang/String;
    .param p3, "time"    # Ljava/lang/String;
    .param p4, "endtime"    # Ljava/lang/String;
    .param p5, "duration"    # Ljava/lang/String;
    .param p6, "loc"    # Ljava/lang/String;
    .param p7, "invitee"    # Ljava/lang/String;
    .param p8, "inviteetype"    # Ljava/lang/String;
    .param p9, "newtitle"    # Ljava/lang/String;
    .param p10, "newdate"    # Ljava/lang/String;
    .param p11, "newtime"    # Ljava/lang/String;
    .param p12, "newendtime"    # Ljava/lang/String;
    .param p13, "newduration"    # Ljava/lang/String;
    .param p14, "newloc"    # Ljava/lang/String;
    .param p15, "newinvitee"    # Ljava/lang/String;
    .param p16, "newinviteetype"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct/range {p0 .. p8}, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    iput-object p9, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newtitle:Ljava/lang/String;

    .line 48
    iput-object p10, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newdate:Ljava/lang/String;

    .line 49
    iput-object p11, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newtime:Ljava/lang/String;

    .line 50
    iput-object p12, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newendtime:Ljava/lang/String;

    .line 51
    iput-object p13, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newduration:Ljava/lang/String;

    .line 52
    iput-object p14, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newloc:Ljava/lang/String;

    .line 53
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newinvitee:Ljava/lang/String;

    .line 54
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newinviteetype:Ljava/lang/String;

    .line 55
    return-void
.end method


# virtual methods
.method protected getAcceptedTextXML()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 60
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "<AcceptedText pt=\"event:edit\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 62
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->getBodyElementsXML(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newdate:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 64
    const-string/jumbo v1, "<Tag u=\"newdate\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 65
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newdate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 66
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newtime:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 69
    const-string/jumbo v1, "<Tag u=\"newtime\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 70
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newtime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 71
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 73
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newtitle:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 74
    const-string/jumbo v1, "<Tag u=\"newtitle\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 75
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 76
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newendtime:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 79
    const-string/jumbo v1, "<Tag u=\"newendtime\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 80
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newendtime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 81
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 83
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newloc:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 84
    const-string/jumbo v1, "<Tag u=\"newloc\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newloc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 86
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 88
    :cond_4
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newinvitee:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 89
    const-string/jumbo v1, "<Tag u=\"newinvitee\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 90
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newinvitee:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 91
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 93
    :cond_5
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newinviteetype:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 94
    const-string/jumbo v1, "<Tag u=\"newinviteetype\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 95
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newinviteetype:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 96
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 98
    :cond_6
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newduration:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 99
    const-string/jumbo v1, "<Tag u=\"newduration\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newduration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 103
    :cond_7
    const-string/jumbo v1, "</AcceptedText>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 104
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "EditEventAcceptedText [newtitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", newdate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newdate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", newtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newtime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", newendtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newendtime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", newduration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newduration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", newloc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newloc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", newinvitee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newinvitee:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", newinviteetype="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditEventAcceptedText;->newinviteetype:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
