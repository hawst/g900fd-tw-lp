.class public abstract Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;
.source "VVSActionHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/vlingo/core/internal/dialogmanager/Controller;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "aClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/Controller;

    .line 42
    .local v1, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 44
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-object v1, v0

    .line 45
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/Controller;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 46
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/Controller;->setTurn(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 47
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :cond_0
    :goto_0
    invoke-virtual {p1, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/Controller;

    return-object v3

    .line 49
    :catch_0
    move-exception v2

    .line 50
    .local v2, "ex":Ljava/lang/Exception;
    const-class v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception getting controller: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected throwUnknownActionException(Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 22
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unrecognized action "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " for handler "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
