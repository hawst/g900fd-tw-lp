.class Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;
.super Ljava/lang/Object;
.source "ContactLookupHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/ContactMatchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->additionalMatchContacts()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoAction(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 308
    return-void
.end method

.method public onContactMatchResultsUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 302
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    return-void
.end method

.method public onContactMatchingFailed()V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$900(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 296
    return-void
.end method

.method public onContactMatchingFinished(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 248
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-eqz p1, :cond_0

    .line 249
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_4

    .line 252
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v1, v2, :cond_1

    .line 253
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getEmailNotFoundString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 291
    return-void

    .line 254
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v1, v2, :cond_2

    .line 255
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getAddressNotFoundString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 289
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v1

    .line 256
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v1, v2, :cond_3

    .line 257
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getBirthdayNotFoundString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;Ljava/lang/String;)V

    goto :goto_0

    .line 258
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v1, v2, :cond_0

    .line 259
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getPhoneTypes()[I
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)[I

    move-result-object v1

    array-length v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 261
    const-string/jumbo v0, ""

    .line 262
    .local v0, "currentPhoneType":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getPhoneTypes()[I
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)[I

    move-result-object v1

    const/4 v2, 0x0

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 273
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_other:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 278
    :goto_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    invoke-virtual {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getPhoneNotFoundString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;Ljava/lang/String;)V

    goto :goto_0

    .line 264
    :pswitch_0
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_work:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$400(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 265
    goto :goto_1

    .line 267
    :pswitch_1
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$500(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 268
    goto :goto_1

    .line 270
    :pswitch_2
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_mobile:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$600(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 271
    goto :goto_1

    .line 285
    .end local v0    # "currentPhoneType":Ljava/lang/String;
    :cond_4
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v3, v3, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getContactNotFoundString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 262
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
