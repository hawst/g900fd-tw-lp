.class Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;
.super Ljava/lang/Object;
.source "DialogFlow.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/VLRecognitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VLRecognitionListenerImpl"
.end annotation


# instance fields
.field private reprompted:Z

.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V
    .locals 1

    .prologue
    .line 1455
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1457
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->reprompted:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;

    .prologue
    .line 1455
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    return-void
.end method

.method private getNothingRecognizedRepromptCount()I
    .locals 7

    .prologue
    .line 1501
    const-string/jumbo v4, "nothing_recognized_reprompt.count"

    const-string/jumbo v5, "-1"

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1503
    .local v3, "stringValue":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1504
    .local v1, "intVal":I
    if-lez v1, :cond_0

    .line 1505
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1506
    .local v2, "newValue":Ljava/lang/String;
    const-string/jumbo v4, "nothing_recognized_reprompt.count"

    invoke-static {v4, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "setString() to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1513
    .end local v1    # "intVal":I
    .end local v2    # "newValue":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 1510
    :catch_0
    move-exception v0

    .line 1511
    .local v0, "ex":Ljava/lang/NumberFormatException;
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "NumberFormatException on value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1513
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getNothingRecognizedRepromptMaxValues()I
    .locals 7

    .prologue
    .line 1485
    const-string/jumbo v4, "nothing_recognized_reprompt.max_value"

    const-string/jumbo v5, "-1"

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1487
    .local v3, "stringValue":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1488
    .local v1, "intVal":I
    if-lez v1, :cond_0

    .line 1489
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1490
    .local v2, "newValue":Ljava/lang/String;
    const-string/jumbo v4, "nothing_recognized_reprompt.max_value"

    invoke-static {v4, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1491
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "setString() to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1497
    .end local v1    # "intVal":I
    .end local v2    # "newValue":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 1494
    :catch_0
    move-exception v0

    .line 1495
    .local v0, "ex":Ljava/lang/NumberFormatException;
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "NumberFormatException on value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1497
    const/4 v1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public onASRRecorderClosed()V
    .locals 1

    .prologue
    .line 1611
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1612
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->onASRRecorderClosed()V

    .line 1614
    :cond_0
    return-void
.end method

.method public onASRRecorderOpened()V
    .locals 1

    .prologue
    .line 1619
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1620
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->onASRRecorderOpened()V

    .line 1622
    :cond_0
    return-void
.end method

.method public onCancelled()V
    .locals 2

    .prologue
    .line 1596
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isAboutToStartUserFlowWithMic:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2002(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Z)Z

    .line 1597
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onCancelled()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1598
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    # invokes: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->preserveState(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2400(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 1599
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->onRecoCancelled()V

    .line 1600
    return-void
.end method

.method public onError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 1476
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isAboutToStartUserFlowWithMic:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2002(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Z)Z

    .line 1477
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onError() error="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1481
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    .line 1482
    return-void
.end method

.method public onRecoToneStarting(Z)J
    .locals 2
    .param p1, "startTone"    # Z

    .prologue
    .line 1605
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->onRecoToneStarting(Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public onRecoToneStopped(Z)V
    .locals 1
    .param p1, "startTone"    # Z

    .prologue
    .line 1627
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1628
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->onRecoToneStopped(Z)V

    .line 1630
    :cond_0
    return-void
.end method

.method public onRecognitionResults(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V
    .locals 6
    .param p1, "result"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1543
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onRecognitionResults()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1548
    iget-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->reprompted:Z

    if-nez v1, :cond_0

    .line 1549
    const-string/jumbo v1, "nothing_recognized_reprompt.count"

    const-string/jumbo v2, "0"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    :cond_0
    iput-boolean v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->reprompted:Z

    .line 1553
    const/4 v0, 0x0

    .line 1554
    .local v0, "actionList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    if-eqz p1, :cond_1

    .line 1555
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getActions()Ljava/util/List;

    move-result-object v0

    .line 1557
    :cond_1
    if-nez v0, :cond_3

    .line 1558
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "no actions received"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1559
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->onResultsNoAction()V

    .line 1587
    :cond_2
    :goto_0
    return-void

    .line 1561
    :cond_3
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getResultString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v4, :cond_4

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/recognition/VLAction;

    invoke-interface {v1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "SetTurnParams"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1562
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "no results with setTurnParms"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1563
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->onResultsNoAction()V

    goto :goto_0

    .line 1566
    :cond_4
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "got results"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1570
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v1

    new-instance v2, Lcom/vlingo/core/internal/logging/EventLog;

    invoke-direct {v2, p1}, Lcom/vlingo/core/internal/logging/EventLog;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showReceivedResults(Lcom/vlingo/core/internal/logging/EventLog;)V

    .line 1571
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 1573
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1574
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2200(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v1

    if-nez v1, :cond_5

    .line 1575
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v4}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v5

    invoke-direct {v3, p1, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    # setter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2202(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 1580
    :goto_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    const/4 v3, 0x0

    # setter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    invoke-static {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$902(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 1581
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    const/4 v3, 0x1

    # setter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z
    invoke-static {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$802(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Z)Z

    .line 1582
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v1

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->RECOGNITION_RESULT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v3, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 1583
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1584
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2200(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->processActions()V

    goto/16 :goto_0

    .line 1578
    :cond_5
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2200(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v5

    invoke-direct {v3, v4, p1, v5}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    # setter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2202(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    goto :goto_1

    .line 1583
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onRmsChanged(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 1591
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showRMSChange(I)V

    .line 1592
    return-void
.end method

.method public onStateChanged(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 2
    .param p1, "newState"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    .line 1461
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isAboutToStartUserFlowWithMic:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2002(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Z)Z

    .line 1470
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # setter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->recoState:Lcom/vlingo/sdk/recognition/VLRecognitionStates;
    invoke-static {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2102(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/sdk/recognition/VLRecognitionStates;)Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .line 1471
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showRecoStateChange(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    .line 1472
    return-void
.end method

.method public onWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)V
    .locals 6
    .param p1, "warning"    # Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 1518
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onWarning() warning="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", message="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1522
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->getNothingRecognizedRepromptMaxValues()I

    move-result v0

    .line 1523
    .local v0, "maxNothingRecognizedRepromptMax":I
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->getNothingRecognizedRepromptCount()I

    move-result v1

    .line 1524
    .local v1, "nothingRecognizedRepromptCount":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-lt v1, v0, :cond_1

    sget-object v3, Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;->WARNING_NOTHING_RECOGNIZED:Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;

    if-ne p1, v3, :cond_1

    .line 1527
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2200(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1528
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2200(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->cancel()V

    .line 1539
    :cond_0
    :goto_0
    return-void

    .line 1531
    :cond_1
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v3

    invoke-interface {v3, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;

    move-result-object v2

    .line 1532
    .local v2, "result":Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;->Reprompted:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;

    if-ne v2, v3, :cond_2

    .line 1533
    const-string/jumbo v3, "nothing_recognized_reprompt.count"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1534
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;->reprompted:Z

    goto :goto_0

    .line 1536
    :cond_2
    const-string/jumbo v3, "nothing_recognized_reprompt.count"

    const-string/jumbo v4, "0"

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
