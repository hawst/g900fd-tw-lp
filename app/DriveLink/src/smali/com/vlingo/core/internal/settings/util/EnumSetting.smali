.class public abstract Lcom/vlingo/core/internal/settings/util/EnumSetting;
.super Lcom/vlingo/core/internal/settings/util/Setting;
.source "EnumSetting.java"


# instance fields
.field protected enumeration:[[Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;[[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "enumeration"    # [[Ljava/lang/String;
    .param p3, "defaultValue"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    .line 17
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/vlingo/core/internal/settings/util/Setting;-><init>(Ljava/lang/String;ILjava/lang/Object;Ljava/lang/String;)V

    .line 18
    iput-object p2, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    .line 19
    return-void
.end method

.method private cloneEnum([[Ljava/lang/String;)[[Ljava/lang/String;
    .locals 7
    .param p1, "old"    # [[Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    .line 105
    array-length v3, p1

    filled-new-array {v3, v6}, [I

    move-result-object v3

    const-class v4, Ljava/lang/String;

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[Ljava/lang/String;

    .line 106
    .local v2, "newEnum":[[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_1

    .line 107
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, v6, :cond_0

    .line 108
    aget-object v3, v2, v0

    new-instance v4, Ljava/lang/String;

    aget-object v5, p1, v0

    aget-object v5, v5, v1

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 107
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 106
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    .end local v1    # "j":I
    :cond_1
    return-object v2
.end method

.method private equal([[Ljava/lang/String;[[Ljava/lang/String;)Z
    .locals 5
    .param p1, "oldEnum"    # [[Ljava/lang/String;
    .param p2, "newEnum"    # [[Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 115
    array-length v3, p1

    array-length v4, p2

    if-eq v3, v4, :cond_1

    .line 123
    :cond_0
    :goto_0
    return v2

    .line 117
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, p2

    if-ge v0, v3, :cond_3

    .line 118
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    const/4 v3, 0x2

    if-ge v1, v3, :cond_2

    .line 119
    aget-object v3, p1, v0

    aget-object v3, v3, v1

    aget-object v4, p2, v0

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 117
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 123
    .end local v1    # "j":I
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getEnumerationLength()I
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getIndexOfValue(Ljava/lang/String;)I
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 69
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 70
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->getValueOfIndex(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 69
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getNameForValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->getIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 78
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 79
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->getNameOfIndex(I)Ljava/lang/String;

    move-result-object v1

    .line 81
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNameOfIndex(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 59
    iget-object v1, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 60
    .local v0, "e":[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    return-object v1
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getValueOfIndex(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 64
    iget-object v1, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 65
    .local v0, "e":[Ljava/lang/String;
    const/4 v1, 0x1

    aget-object v1, v0, v1

    return-object v1
.end method

.method public hasValue(Ljava/lang/String;)Z
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 37
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 38
    iget-object v3, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    aget-object v0, v3, v1

    .line 39
    .local v0, "enumType":[Ljava/lang/String;
    aget-object v3, v0, v2

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 42
    .end local v0    # "enumType":[Ljava/lang/String;
    :goto_1
    return v2

    .line 37
    .restart local v0    # "enumType":[Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 42
    .end local v0    # "enumType":[Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public migrate(Lcom/vlingo/core/internal/settings/util/Setting;)V
    .locals 1
    .param p1, "newSetting"    # Lcom/vlingo/core/internal/settings/util/Setting;

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/settings/util/Setting;->migrate(Lcom/vlingo/core/internal/settings/util/Setting;)V

    .line 97
    check-cast p1, Lcom/vlingo/core/internal/settings/util/EnumSetting;

    .end local p1    # "newSetting":Lcom/vlingo/core/internal/settings/util/Setting;
    iget-object v0, p1, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->setEnumeration([[Ljava/lang/String;)V

    .line 98
    return-void
.end method

.method public declared-synchronized onSavedValue(Ljava/lang/Object;)V
    .locals 2
    .param p1, "valueParam"    # Ljava/lang/Object;

    .prologue
    .line 23
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 24
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->getValueOfIndex(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/settings/util/Setting;->onSavedValue(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    :goto_1
    monitor-exit p0

    return-void

    .line 23
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 29
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->setDirty()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 23
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setEnumeration([[Ljava/lang/String;)V
    .locals 1
    .param p1, "newEnumeration"    # [[Ljava/lang/String;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->equal([[Ljava/lang/String;[[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->cloneEnum([[Ljava/lang/String;)[[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->enumeration:[[Ljava/lang/String;

    .line 88
    invoke-virtual {p0}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->setDirty()V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/EnumSetting;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->hasValue(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->setDefault()V

    .line 93
    :cond_1
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 46
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->setValue(Ljava/lang/String;Z)V

    .line 47
    return-void
.end method

.method public setValue(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "setDefaultIfValueNotInEnum"    # Z

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->hasValue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->setValueInternal(Ljava/lang/Object;)V

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    if-eqz p2, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/vlingo/core/internal/settings/util/EnumSetting;->setDefault()V

    goto :goto_0
.end method
