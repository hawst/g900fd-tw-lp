.class public abstract Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
.super Ljava/lang/Object;
.source "ContactRuleSet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract canProcess(Ljava/lang/String;)Z
.end method

.method public abstract generateRules(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;"
        }
    .end annotation
.end method

.method public getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;-><init>(Z)V

    return-object v0
.end method

.method public skipExtraData()Z
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    return v0
.end method
