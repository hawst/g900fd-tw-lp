.class public Lcom/vlingo/core/internal/questions/DownloadableImage;
.super Ljava/lang/Object;
.source "DownloadableImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/questions/DownloadableImage$Notification;,
        Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;
    }
.end annotation


# instance fields
.field private image:Landroid/graphics/drawable/Drawable;

.field private listeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private timedout:Z

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->listeners:Ljava/util/HashSet;

    .line 18
    if-nez p1, :cond_0

    .line 19
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "you MUST provide a \'url\'"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->url:Ljava/lang/String;

    .line 21
    return-void
.end method

.method private notifyListeners(Lcom/vlingo/core/internal/questions/DownloadableImage$Notification;)V
    .locals 3
    .param p1, "notification"    # Lcom/vlingo/core/internal/questions/DownloadableImage$Notification;

    .prologue
    .line 89
    iget-object v2, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->listeners:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;

    .line 90
    .local v1, "listener":Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;
    invoke-interface {p1, v1}, Lcom/vlingo/core/internal/questions/DownloadableImage$Notification;->notify(Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;)V

    goto :goto_0

    .line 91
    .end local v1    # "listener":Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;
    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;)Lcom/vlingo/core/internal/questions/DownloadableImage;
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 80
    return-object p0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->image:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->url:Ljava/lang/String;

    return-object v0
.end method

.method public isDownloaded()Z
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->image:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeListener(Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;)Lcom/vlingo/core/internal/questions/DownloadableImage;
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 85
    return-object p0
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)Lcom/vlingo/core/internal/questions/DownloadableImage;
    .locals 1
    .param p1, "imageParam"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->image:Landroid/graphics/drawable/Drawable;

    .line 53
    new-instance v0, Lcom/vlingo/core/internal/questions/DownloadableImage$2;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/questions/DownloadableImage$2;-><init>(Lcom/vlingo/core/internal/questions/DownloadableImage;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/questions/DownloadableImage;->notifyListeners(Lcom/vlingo/core/internal/questions/DownloadableImage$Notification;)V

    .line 71
    return-object p0
.end method

.method public timedOut(Z)V
    .locals 1
    .param p1, "timedOut"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->timedout:Z

    .line 35
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/DownloadableImage;->isDownloaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/vlingo/core/internal/questions/DownloadableImage$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/questions/DownloadableImage$1;-><init>(Lcom/vlingo/core/internal/questions/DownloadableImage;)V

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/questions/DownloadableImage;->notifyListeners(Lcom/vlingo/core/internal/questions/DownloadableImage$Notification;)V

    .line 41
    :cond_0
    return-void
.end method

.method public timedOut()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->timedout:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/DownloadableImage;->isDownloaded()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "[DONE]"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/questions/DownloadableImage;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "[....]"

    goto :goto_0
.end method
