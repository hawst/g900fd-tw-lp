.class public Lcom/vlingo/core/internal/dialogmanager/tasks/ExecuteTask;
.super Lcom/vlingo/core/internal/util/TaskQueue$Task;
.source "ExecuteTask.java"


# instance fields
.field private mRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue$Task;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/ExecuteTask;->mRunnable:Ljava/lang/Runnable;

    .line 20
    return-void
.end method


# virtual methods
.method public declared-synchronized run()V
    .locals 3

    .prologue
    .line 24
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/ExecuteTask;->mRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 25
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/ExecuteTask;->mRunnable:Ljava/lang/Runnable;

    const-string/jumbo v2, "ExecuteTask"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 26
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 29
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/ExecuteTask;->notifyFinished()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    monitor-exit p0

    return-void

    .line 24
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
