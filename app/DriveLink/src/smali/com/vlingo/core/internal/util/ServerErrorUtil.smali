.class public Lcom/vlingo/core/internal/util/ServerErrorUtil;
.super Ljava/lang/Object;
.source "ServerErrorUtil.java"


# static fields
.field private static final CHILD_ELEMENT_CODE:Ljava/lang/String; = "Code"

.field private static final CHILD_ELEMENT_DETAILS:Ljava/lang/String; = "Details"

.field private static final CHILD_ELEMENT_MESSAGE:Ljava/lang/String; = "Message"

.field private static final ROOT_ELEMENT_ERROR:Ljava/lang/String; = "Error"


# instance fields
.field private errorNodes:Lorg/w3c/dom/NodeList;

.field private root:Lorg/w3c/dom/Element;


# direct methods
.method public constructor <init>(Lorg/w3c/dom/Element;)V
    .locals 1
    .param p1, "root"    # Lorg/w3c/dom/Element;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->errorNodes:Lorg/w3c/dom/NodeList;

    .line 88
    iput-object p1, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->root:Lorg/w3c/dom/Element;

    .line 89
    return-void
.end method

.method public static getErrorMessageFromXML(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 53
    const-string/jumbo v2, ""

    .line 56
    .local v2, "errorMessage":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v4

    .line 57
    .local v4, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v4}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v8

    .line 58
    .local v8, "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v8, p0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 59
    .local v0, "doc":Lorg/w3c/dom/Document;
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v6

    .line 61
    .local v6, "root":Lorg/w3c/dom/Element;
    const-string/jumbo v9, "Error"

    invoke-static {v6, v9}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->getFirstElementNamed(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 62
    .local v1, "error":Lorg/w3c/dom/Element;
    if-eqz v1, :cond_1

    .line 64
    const-string/jumbo v9, "Message"

    invoke-static {v1, v9}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->getFirstElementNamed(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v5

    .line 65
    .local v5, "message":Lorg/w3c/dom/Element;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v5}, Lorg/w3c/dom/Element;->getTextContent()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 66
    if-eqz p1, :cond_0

    .line 67
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Server error: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v1    # "error":Lorg/w3c/dom/Element;
    .end local v4    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v5    # "message":Lorg/w3c/dom/Element;
    .end local v6    # "root":Lorg/w3c/dom/Element;
    .end local v8    # "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    :cond_0
    :goto_0
    move-object v3, v2

    .line 82
    .end local v2    # "errorMessage":Ljava/lang/String;
    .local v3, "errorMessage":Ljava/lang/String;
    :goto_1
    return-object v3

    .line 71
    .end local v3    # "errorMessage":Ljava/lang/String;
    .restart local v0    # "doc":Lorg/w3c/dom/Document;
    .restart local v1    # "error":Lorg/w3c/dom/Element;
    .restart local v2    # "errorMessage":Ljava/lang/String;
    .restart local v4    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v6    # "root":Lorg/w3c/dom/Element;
    .restart local v8    # "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    :cond_1
    new-instance v7, Lcom/vlingo/core/internal/util/ServerErrorUtil;

    invoke-direct {v7, v6}, Lcom/vlingo/core/internal/util/ServerErrorUtil;-><init>(Lorg/w3c/dom/Element;)V

    .line 72
    .local v7, "serverErrorUtil":Lcom/vlingo/core/internal/util/ServerErrorUtil;
    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->hasServerError()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 73
    if-eqz p1, :cond_2

    .line 74
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Server error Code=\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->getCode()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " Code="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->getCode()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 77
    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->logIt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v2

    .line 78
    .end local v2    # "errorMessage":Ljava/lang/String;
    .restart local v3    # "errorMessage":Ljava/lang/String;
    goto :goto_1

    .line 81
    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v1    # "error":Lorg/w3c/dom/Element;
    .end local v3    # "errorMessage":Ljava/lang/String;
    .end local v4    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v6    # "root":Lorg/w3c/dom/Element;
    .end local v7    # "serverErrorUtil":Lcom/vlingo/core/internal/util/ServerErrorUtil;
    .end local v8    # "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v2    # "errorMessage":Ljava/lang/String;
    :catch_0
    move-exception v9

    goto :goto_0
.end method

.method private static getFirstElementNamed(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 2
    .param p0, "parent"    # Lorg/w3c/dom/Element;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 118
    invoke-interface {p0}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    .end local p0    # "parent":Lorg/w3c/dom/Element;
    :goto_0
    return-object p0

    .line 122
    .restart local p0    # "parent":Lorg/w3c/dom/Element;
    :cond_0
    invoke-interface {p0, p1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 123
    .local v0, "children":Lorg/w3c/dom/NodeList;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-lez v1, :cond_1

    .line 124
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Element;

    move-object p0, v1

    goto :goto_0

    .line 126
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 109
    iget-object v2, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->root:Lorg/w3c/dom/Element;

    const-string/jumbo v3, "Code"

    invoke-interface {v2, v3}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 110
    .local v1, "nodeList":Lorg/w3c/dom/NodeList;
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-lez v2, :cond_0

    .line 111
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 112
    .local v0, "node":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v2

    .line 114
    .end local v0    # "node":Lorg/w3c/dom/Node;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public hasServerError()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 92
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->errorNodes:Lorg/w3c/dom/NodeList;

    if-nez v5, :cond_0

    .line 93
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->root:Lorg/w3c/dom/Element;

    const-string/jumbo v6, "Error"

    invoke-interface {v5, v6}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->errorNodes:Lorg/w3c/dom/NodeList;

    .line 95
    :cond_0
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->errorNodes:Lorg/w3c/dom/NodeList;

    if-eqz v5, :cond_7

    .line 96
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->errorNodes:Lorg/w3c/dom/NodeList;

    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-lez v5, :cond_2

    .line 105
    :cond_1
    :goto_0
    return v3

    .line 99
    :cond_2
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->root:Lorg/w3c/dom/Element;

    const-string/jumbo v6, "Code"

    invoke-interface {v5, v6}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-lez v5, :cond_4

    move v0, v3

    .line 100
    .local v0, "hasCode":Z
    :goto_1
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->root:Lorg/w3c/dom/Element;

    const-string/jumbo v6, "Message"

    invoke-interface {v5, v6}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-lez v5, :cond_5

    move v2, v3

    .line 101
    .local v2, "hasMessage":Z
    :goto_2
    iget-object v5, p0, Lcom/vlingo/core/internal/util/ServerErrorUtil;->root:Lorg/w3c/dom/Element;

    const-string/jumbo v6, "Details"

    invoke-interface {v5, v6}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-lez v5, :cond_6

    move v1, v3

    .line 102
    .local v1, "hasDetails":Z
    :goto_3
    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    if-nez v1, :cond_1

    :cond_3
    move v3, v4

    goto :goto_0

    .end local v0    # "hasCode":Z
    .end local v1    # "hasDetails":Z
    .end local v2    # "hasMessage":Z
    :cond_4
    move v0, v4

    .line 99
    goto :goto_1

    .restart local v0    # "hasCode":Z
    :cond_5
    move v2, v4

    .line 100
    goto :goto_2

    .restart local v2    # "hasMessage":Z
    :cond_6
    move v1, v4

    .line 101
    goto :goto_3

    .end local v0    # "hasCode":Z
    .end local v2    # "hasMessage":Z
    :cond_7
    move v3, v4

    .line 105
    goto :goto_0
.end method

.method public logIt()V
    .locals 0

    .prologue
    .line 150
    return-void
.end method
