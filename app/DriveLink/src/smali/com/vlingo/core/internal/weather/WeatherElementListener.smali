.class public Lcom/vlingo/core/internal/weather/WeatherElementListener;
.super Ljava/lang/Object;
.source "WeatherElementListener.java"

# interfaces
.implements Landroid/sax/ElementListener;


# instance fields
.field private element:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private grandParent:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private parent:Lcom/vlingo/core/internal/weather/WeatherElement;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V
    .locals 2
    .param p1, "element"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p2, "parent"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "element OR parent cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_1
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->element:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 30
    iput-object p2, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->parent:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V
    .locals 2
    .param p1, "element"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p2, "parent"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p3, "grandParent"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "element, parent OR grandParent cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_1
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->element:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 41
    iput-object p2, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->parent:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 42
    iput-object p3, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->grandParent:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 43
    return-void
.end method


# virtual methods
.method public end()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->grandParent:Lcom/vlingo/core/internal/weather/WeatherElement;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->parent:Lcom/vlingo/core/internal/weather/WeatherElement;

    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->element:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->clone()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->addChild(Lcom/vlingo/core/internal/weather/WeatherElement;)Z

    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->grandParent:Lcom/vlingo/core/internal/weather/WeatherElement;

    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->parent:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->clone()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->addChild(Lcom/vlingo/core/internal/weather/WeatherElement;)Z

    .line 54
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->parent:Lcom/vlingo/core/internal/weather/WeatherElement;

    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->element:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->clone()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->addChild(Lcom/vlingo/core/internal/weather/WeatherElement;)Z

    goto :goto_0
.end method

.method public start(Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 58
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    invoke-interface {p1}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 59
    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getLocalName(I)Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "key":Ljava/lang/String;
    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/core/internal/weather/WeatherElementListener;->element:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v3, v1, v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->addAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    return-void
.end method
