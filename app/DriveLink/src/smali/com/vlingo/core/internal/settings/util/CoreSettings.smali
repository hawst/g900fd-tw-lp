.class public final Lcom/vlingo/core/internal/settings/util/CoreSettings;
.super Ljava/lang/Object;
.source "CoreSettings.java"


# static fields
.field public static final ALERT_TONES:Ljava/lang/String; = "behavior.alert_tones"

.field public static final ALERT_TONES_ENUM:[[Ljava/lang/String;

.field public static final ALERT_TONES_HIGH:Ljava/lang/String; = "behavior.alert_tones.high"

.field public static final ALERT_TONES_LOW:Ljava/lang/String; = "behavior.alert_tones.low"

.field public static final ALERT_TONES_MEDIUM:Ljava/lang/String; = "behavior.alert_tones.medium"

.field public static final ALERT_TONES_OFF:Ljava/lang/String; = "behavior.alert_tones.off"

.field public static final ALERT_TONES_VIBRATE:Ljava/lang/String; = "behavior.alert_tones.vibrate"

.field public static final ASR_KEEPALIVE:Ljava/lang/String; = "pref.internal.asr_keepalive"

.field public static final ASR_KEY_BEHAVIOR:[[Ljava/lang/String;

.field public static final ASR_KEY_NAMES:[[Ljava/lang/String;

.field public static final AUTO_ENDPOINT_TIMELIMIT_WITHOUT_SPEECH_SEC:Ljava/lang/String; = "behavior.auto_endpointing.timelimit.withoutspeech"

.field public static final AUTO_ENDPOINT_TIMELIMIT_WITHOUT_SPEECH_SEC_CAR:Ljava/lang/String; = "behavior.auto_endpointing.timelimit.withoutspeechcar"

.field public static final AUTO_ENDPOINT_TIMELIMIT_WITH_SPEECH_SEC:Ljava/lang/String; = "behavior.auto_endpointing.timelimit.withspeech"

.field public static final AUTO_ENDPOINT_TIMELIMIT_WITH_SPEECH_SEC_CAR:Ljava/lang/String; = "behavior.auto_endpointing.timelimit.withspeechcar"

.field public static final AUTO_PUNCTUATION:Ljava/lang/String; = "pref.autopunctuation"

.field public static final BLUETOOTH_VIBRATE_TIMING:Ljava/lang/String; = "behavior.bluetooth.vibrate.timing"

.field public static final BROWSER_FORCE_BIS:Ljava/lang/String; = "browser.force_bis"

.field public static final CONNECTION_TYPE:Ljava/lang/String; = "network.connection_type"

.field public static final CONNECTION_TYPE_BIS:Ljava/lang/String; = "network.connection_type.bis"

.field public static final CONNECTION_TYPE_DIRECT_TCP:Ljava/lang/String; = "network.connection_type.directtcp"

.field public static final CONNECTION_TYPE_ENUM:[[Ljava/lang/String;

.field public static final CONNECTION_TYPE_MDS:Ljava/lang/String; = "network.connection_type.mds"

.field public static final DETAILED_TIMINGS:Ljava/lang/String; = "pref.internal.detailed_timings"

.field public static final LANGUAGE:Ljava/lang/String; = "pref.language"

.field public static final LANGUAGE_ENUM:[[Ljava/lang/String;

.field public static final LANGUAGE_EN_AU:Ljava/lang/String; = "pref.language.en_au"

.field public static final LANGUAGE_EN_GB:Ljava/lang/String; = "pref.language.en_gb"

.field public static final LANGUAGE_EN_US:Ljava/lang/String; = "pref.language.en_us"

.field public static final LAST_CONNECTION_TEST_PASSED:Ljava/lang/String; = "appstate.last_conn_test_passed"

.field public static final LED_RECORD:Ljava/lang/String; = "behavior.led"

.field public static final LED_RECORD_BEHAVIOR:[[Ljava/lang/String;

.field public static final LED_RECORD_BLUE_ANIMATED:Ljava/lang/String; = "behavior.led.blue_animated"

.field public static final LED_RECORD_OFF:Ljava/lang/String; = "behavior.led.off"

.field public static final LED_RECORD_RED_SOLID:Ljava/lang/String; = "behavior.led.red_solid"

.field public static final LOCATION_AWARENESS:Ljava/lang/String; = "pref.location.awareness"

.field public static final SETTINGS_VERSION:Ljava/lang/String; = "settings.internal.version"

.field public static final TEST_NETWORK_NAME:Ljava/lang/String; = "appstate.network_name"

.field public static final TTS_PLAYBACK:Ljava/lang/String; = "tts.playback"

.field public static final TTS_PLAYBACK_BRIEF:Ljava/lang/String; = "tts.playback.brief"

.field public static final TTS_PLAYBACK_BT:Ljava/lang/String; = "tts.playback.bt"

.field public static final TTS_PLAYBACK_BT_BRIEF:Ljava/lang/String; = "tts.playback.bt.brief"

.field public static final TTS_PLAYBACK_BT_FULL:Ljava/lang/String; = "tts.playback.bt.full"

.field public static final TTS_PLAYBACK_BT_OFF:Ljava/lang/String; = "tts.playback.bt.off"

.field public static final TTS_PLAYBACK_ENUM:[[Ljava/lang/String;

.field public static final TTS_PLAYBACK_ENUM_BT:[[Ljava/lang/String;

.field public static final TTS_PLAYBACK_FULL:Ljava/lang/String; = "tts.playback.full"

.field public static final TTS_PLAYBACK_OFF:Ljava/lang/String; = "tts.playback.off"

.field public static final TTS_VOICE:Ljava/lang/String; = "tts.voice"

.field public static final TTS_VOICE_ENUM:[[Ljava/lang/String;

.field public static final TTS_VOICE_FEMALE:Ljava/lang/String; = "tts.voice.female"

.field public static final TTS_VOICE_MALE:Ljava/lang/String; = "tts.voice.male"

.field public static final TTS_VOLUME:Ljava/lang/String; = "tts.volume"

.field public static final TTS_VOLUME_ENUM:[[Ljava/lang/String;

.field public static final TTS_VOLUME_LOUD:Ljava/lang/String; = "tts.volume.loud"

.field public static final TTS_VOLUME_NORMAL:Ljava/lang/String; = "tts.volume.normal"

.field public static final TTS_VOLUME_QUIET:Ljava/lang/String; = "tts.volume.quiet"

.field public static final USE_BLUETOOTH:Ljava/lang/String; = "bluetooth.enabled"

.field public static final VOICE_KEY:Ljava/lang/String; = "behavior.voice_key"

.field public static final VOICE_KEY_BEHAVIOR:Ljava/lang/String; = "behavior.asr_key_active"

.field public static final VOICE_KEY_BEHAVIOR_ALWAYS:Ljava/lang/String; = "behavior.asr_key_active.always"

.field public static final VOICE_KEY_BEHAVIOR_WHEN_ACTIVE:Ljava/lang/String; = "behavior.asr_key_active.when_active"

.field public static final VOICE_KEY_LEFT:Ljava/lang/String; = "behavior.voice_key.left"

.field public static final VOICE_KEY_RIGHT:Ljava/lang/String; = "behavior.voice_key.right"

.field public static final WORKING_APN:Ljava/lang/String; = "appstate.working_apn"


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 100
    new-array v0, v7, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "US English"

    aput-object v2, v1, v4

    const-string/jumbo v2, "pref.language.en_us"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "UK English"

    aput-object v2, v1, v4

    const-string/jumbo v2, "pref.language.en_gb"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Australian English"

    aput-object v2, v1, v4

    const-string/jumbo v2, "pref.language.en_au"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/settings/util/CoreSettings;->LANGUAGE_ENUM:[[Ljava/lang/String;

    .line 106
    new-array v0, v7, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Direct TCP"

    aput-object v2, v1, v4

    const-string/jumbo v2, "network.connection_type.directtcp"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "BIS"

    aput-object v2, v1, v4

    const-string/jumbo v2, "network.connection_type.bis"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "MDS"

    aput-object v2, v1, v4

    const-string/jumbo v2, "network.connection_type.mds"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/settings/util/CoreSettings;->CONNECTION_TYPE_ENUM:[[Ljava/lang/String;

    .line 112
    const/4 v0, 0x5

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Off"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.alert_tones.off"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Vibrate"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.alert_tones.vibrate"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Quiet"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.alert_tones.low"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Normal"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.alert_tones.medium"

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "Loud"

    aput-object v3, v2, v4

    const-string/jumbo v3, "behavior.alert_tones.high"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/settings/util/CoreSettings;->ALERT_TONES_ENUM:[[Ljava/lang/String;

    .line 120
    new-array v0, v6, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Left"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.voice_key.left"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Right"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.voice_key.right"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/settings/util/CoreSettings;->ASR_KEY_NAMES:[[Ljava/lang/String;

    .line 125
    new-array v0, v6, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Anywhere"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.asr_key_active.always"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Only On Vlingo Home"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.asr_key_active.when_active"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/settings/util/CoreSettings;->ASR_KEY_BEHAVIOR:[[Ljava/lang/String;

    .line 130
    new-array v0, v7, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Off"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.led.off"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Solid Red"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.led.red_solid"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Animated Blue"

    aput-object v2, v1, v4

    const-string/jumbo v2, "behavior.led.blue_animated"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/settings/util/CoreSettings;->LED_RECORD_BEHAVIOR:[[Ljava/lang/String;

    .line 136
    new-array v0, v7, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Quiet"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.volume.quiet"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Normal"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.volume.normal"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Loud"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.volume.loud"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/settings/util/CoreSettings;->TTS_VOLUME_ENUM:[[Ljava/lang/String;

    .line 142
    new-array v0, v6, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Female"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.voice.female"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Male"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.voice.male"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/settings/util/CoreSettings;->TTS_VOICE_ENUM:[[Ljava/lang/String;

    .line 147
    new-array v0, v7, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Off"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.playback.off"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Brief"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.playback.brief"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Full"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.playback.full"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/settings/util/CoreSettings;->TTS_PLAYBACK_ENUM:[[Ljava/lang/String;

    .line 153
    new-array v0, v7, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Off"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.playback.bt.off"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Brief"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.playback.bt.brief"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "Full"

    aput-object v2, v1, v4

    const-string/jumbo v2, "tts.playback.bt.full"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/settings/util/CoreSettings;->TTS_PLAYBACK_ENUM_BT:[[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getIndexForEnumWithValue([[Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0, "enumeration"    # [[Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 163
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 164
    aget-object v1, p0, v0

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 163
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 167
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
