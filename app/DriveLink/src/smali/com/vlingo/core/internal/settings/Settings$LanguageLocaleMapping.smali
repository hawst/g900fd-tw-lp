.class final enum Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;
.super Ljava/lang/Enum;
.source "Settings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/settings/Settings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "LanguageLocaleMapping"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum BRAZIL:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum CHINA:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum FRANCE:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum GERMANY:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum ITALY:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum JAPAN:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum KOREA:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum RUSSIA:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum SPAIN:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum SPAIN_LATIN:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum SPAIN_US:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum UK:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

.field public static final enum US:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;


# instance fields
.field private final language:Ljava/lang/String;

.field private locale:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1316
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "KOREA"

    const-string/jumbo v2, "ko-KR"

    sget-object v3, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->KOREA:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1317
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "CHINA"

    const-string/jumbo v2, "zh-CN"

    sget-object v3, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->CHINA:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1318
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "GERMANY"

    const-string/jumbo v2, "de-DE"

    sget-object v3, Ljava/util/Locale;->GERMANY:Ljava/util/Locale;

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->GERMANY:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1319
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "FRANCE"

    const-string/jumbo v2, "fr-FR"

    sget-object v3, Ljava/util/Locale;->FRANCE:Ljava/util/Locale;

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->FRANCE:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1320
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "ITALY"

    const-string/jumbo v2, "it-IT"

    sget-object v3, Ljava/util/Locale;->ITALY:Ljava/util/Locale;

    invoke-direct {v0, v1, v11, v2, v3}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->ITALY:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1321
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "JAPAN"

    const/4 v2, 0x5

    const-string/jumbo v3, "ja-JP"

    sget-object v4, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->JAPAN:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1322
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "RUSSIA"

    const/4 v2, 0x6

    const-string/jumbo v3, "ru-RU"

    new-instance v4, Ljava/util/Locale;

    const-string/jumbo v5, "ru"

    const-string/jumbo v6, "RU"

    invoke-direct {v4, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->RUSSIA:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1323
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "UK"

    const/4 v2, 0x7

    const-string/jumbo v3, "en-GB"

    sget-object v4, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->UK:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1324
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "US"

    const/16 v2, 0x8

    const-string/jumbo v3, "en-US"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->US:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1325
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "SPAIN"

    const/16 v2, 0x9

    const-string/jumbo v3, "es-ES"

    new-instance v4, Ljava/util/Locale;

    const-string/jumbo v5, "es"

    const-string/jumbo v6, "ES"

    invoke-direct {v4, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->SPAIN:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1326
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "SPAIN_LATIN"

    const/16 v2, 0xa

    const-string/jumbo v3, "v-es-LA"

    new-instance v4, Ljava/util/Locale;

    const-string/jumbo v5, "es"

    const-string/jumbo v6, "US"

    invoke-direct {v4, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->SPAIN_LATIN:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1327
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "SPAIN_US"

    const/16 v2, 0xb

    const-string/jumbo v3, "v-es-NA"

    new-instance v4, Ljava/util/Locale;

    const-string/jumbo v5, "es"

    const-string/jumbo v6, "US"

    invoke-direct {v4, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->SPAIN_US:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1328
    new-instance v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    const-string/jumbo v1, "BRAZIL"

    const/16 v2, 0xc

    const-string/jumbo v3, "pt-BR"

    new-instance v4, Ljava/util/Locale;

    const-string/jumbo v5, "pt"

    const-string/jumbo v6, "BR"

    invoke-direct {v4, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->BRAZIL:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .line 1315
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->KOREA:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->CHINA:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v1, v0, v8

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->GERMANY:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v1, v0, v9

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->FRANCE:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v1, v0, v10

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->ITALY:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v1, v0, v11

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->JAPAN:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->RUSSIA:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->UK:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->US:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->SPAIN:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->SPAIN_LATIN:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->SPAIN_US:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->BRAZIL:Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->$VALUES:[Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V
    .locals 0
    .param p3, "language"    # Ljava/lang/String;
    .param p4, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1332
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1333
    iput-object p3, p0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->language:Ljava/lang/String;

    .line 1334
    iput-object p4, p0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->locale:Ljava/util/Locale;

    .line 1335
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;)Ljava/util/Locale;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->language:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1315
    const-class v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;
    .locals 1

    .prologue
    .line 1315
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->$VALUES:[Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    return-object v0
.end method
