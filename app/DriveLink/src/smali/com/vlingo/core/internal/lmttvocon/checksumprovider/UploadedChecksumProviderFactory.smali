.class public final Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProviderFactory;
.super Ljava/lang/Object;
.source "UploadedChecksumProviderFactory.java"


# static fields
.field private static overriddenProvider:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProviderFactory;->overriddenProvider:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public static create(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;
    .locals 5
    .param p0, "uploadContext"    # Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .prologue
    .line 21
    sget-object v1, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProviderFactory;->overriddenProvider:Ljava/lang/Class;

    if-eqz v1, :cond_0

    .line 23
    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProviderFactory;->overriddenProvider:Ljava/lang/Class;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 24
    .local v0, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;>;"
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    .end local v0    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;>;"
    :goto_0
    return-object v1

    .line 25
    :catch_0
    move-exception v1

    .line 30
    :cond_0
    new-instance v1, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V

    goto :goto_0
.end method

.method public static override(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;>;"
    sput-object p0, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProviderFactory;->overriddenProvider:Ljava/lang/Class;

    .line 35
    return-void
.end method
