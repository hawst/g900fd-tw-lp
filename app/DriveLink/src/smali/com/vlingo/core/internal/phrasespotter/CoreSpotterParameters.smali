.class public final Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
.super Ljava/lang/Object;
.source "CoreSpotterParameters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$1;,
        Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_DELTA_D:I = 0x0

.field public static final DEFAULT_DELTA_S:I = 0x32

.field public static final DEFAULT_PHRASESPOT_ABSBEAM:F = 40.0f

.field public static final DEFAULT_PHRASESPOT_AOFFSET:F = 0.0f

.field public static final DEFAULT_PHRASESPOT_BEAM:F = 20.0f

.field public static final DEFAULT_PHRASESPOT_DELAY:F


# instance fields
.field private absbeam:F

.field private aoffset:F

.field private beam:F

.field private cgFilename:Ljava/lang/String;

.field private delay:F

.field private deltaD:I

.field private deltaS:I

.field private grammarSpec:Ljava/lang/String;

.field private language:Ljava/lang/String;

.field private pronunList:[Ljava/lang/String;

.field private wakeUpExternalStorage:Ljava/lang/String;

.field private wordList:[Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    return-void
.end method

.method private constructor <init>(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->language:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$100(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->language:Ljava/lang/String;

    .line 126
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->cgFilename:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$200(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->cgFilename:Ljava/lang/String;

    .line 127
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->grammarSpec:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$300(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->grammarSpec:Ljava/lang/String;

    .line 128
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->wordList:[Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$400(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->wordList:[Ljava/lang/String;

    .line 129
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->pronunList:[Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$500(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->pronunList:[Ljava/lang/String;

    .line 130
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->beam:F
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$600(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->beam:F

    .line 131
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->absbeam:F
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$700(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->absbeam:F

    .line 132
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->aoffset:F
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$800(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->aoffset:F

    .line 133
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->delay:F
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$900(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->delay:F

    .line 134
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->deltaD:I
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$1000(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->deltaD:I

    .line 135
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->deltaS:I
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$1100(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->deltaS:I

    .line 136
    # getter for: Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->wakeUpExternalStorage:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->access$1200(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->wakeUpExternalStorage:Ljava/lang/String;

    .line 137
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;
    .param p2, "x1"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;-><init>(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)V

    return-void
.end method


# virtual methods
.method public getAbsbeam()F
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->absbeam:F

    return v0
.end method

.method public getAoffset()F
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->aoffset:F

    return v0
.end method

.method public getBeam()F
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->beam:F

    return v0
.end method

.method public getCGFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->cgFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getDelay()F
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->delay:F

    return v0
.end method

.method public getDeltaD()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->deltaD:I

    return v0
.end method

.method public getDeltaS()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->deltaS:I

    return v0
.end method

.method public getGrammarSpec()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->grammarSpec:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getPronunList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->pronunList:[Ljava/lang/String;

    return-object v0
.end method

.method public getWakeUpExternalStorage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->wakeUpExternalStorage:Ljava/lang/String;

    return-object v0
.end method

.method public getWordList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->wordList:[Ljava/lang/String;

    return-object v0
.end method
