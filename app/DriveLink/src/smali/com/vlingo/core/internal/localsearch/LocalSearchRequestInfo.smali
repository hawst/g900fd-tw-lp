.class public Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;
.super Ljava/lang/Object;
.source "LocalSearchRequestInfo.java"


# instance fields
.field private city:Ljava/lang/String;

.field private query:Ljava/lang/String;

.field private selectedLocationType:Ljava/lang/String;

.field private state:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0, v0, v0, v0, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "city"    # Ljava/lang/String;
    .param p3, "state"    # Ljava/lang/String;
    .param p4, "selectedLocationType"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->setQuery(Ljava/lang/String;)V

    .line 20
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->setCity(Ljava/lang/String;)V

    .line 21
    invoke-virtual {p0, p3}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->setState(Ljava/lang/String;)V

    .line 22
    invoke-virtual {p0, p4}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->setSelectedLocationType(Ljava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalizedCityState()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->city:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->state:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 43
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ko-KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->query:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedLocationType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->selectedLocationType:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->state:Ljava/lang/String;

    return-object v0
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .param p1, "city"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->city:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->query:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setSelectedLocationType(Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedLocationType"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->selectedLocationType:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->state:Ljava/lang/String;

    .line 60
    return-void
.end method
