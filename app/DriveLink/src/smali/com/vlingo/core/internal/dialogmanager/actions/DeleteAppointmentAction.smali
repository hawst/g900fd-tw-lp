.class public Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "DeleteAppointmentAction.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method public event(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;
    .locals 0
    .param p1, "scheduleEvent"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 25
    return-object p0
.end method

.method protected execute()V
    .locals 4

    .prologue
    .line 30
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    if-eqz v1, :cond_0

    .line 32
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getID()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->deleteEvent(Landroid/content/Context;J)V

    .line 33
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_0
    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to delete event: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to delete event: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 40
    .end local v0    # "e":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "No event to delete"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method
