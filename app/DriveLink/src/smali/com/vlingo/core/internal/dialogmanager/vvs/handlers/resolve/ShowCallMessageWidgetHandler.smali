.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallMessageWidgetHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;
.source "ShowCallMessageWidgetHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallMessageWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallMessageWidgetHandler;->messages:Ljava/util/LinkedList;

    .line 17
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallMessageWidgetHandler;->messages:Ljava/util/LinkedList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallMessageWidgetHandler;->messages:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 18
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallMessageWidgetHandler;->messages:Ljava/util/LinkedList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 19
    .local v0, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .local v1, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallMessageWidgetHandler;->forwardRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 22
    .end local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v1    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v2

    return v2
.end method
