.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;
.super Ljava/lang/Object;
.source "PhraseSpotterAudioLogger.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/AudioLogger;


# static fields
.field private static final BUFFER_LENGTH_MS:I = 0x4e20

.field private static smLoggerId:I


# instance fields
.field private mBAOutputStream:Ljava/io/ByteArrayOutputStream;

.field private mBytesLeft:I

.field private mDataOutputStream:Ljava/io/DataOutputStream;

.field private mGotLowByte:Z

.field private mId:I

.field private mIsDone:Z

.field private mLowByte:B

.field private mPStartingSample:I

.field private mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

.field private mSStartingSample:I

.field private mSampleRate:I

.field private mSamplesPerChunk:I

.field private mScore:F

.field private mSpeechSample:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->smLoggerId:I

    return-void
.end method

.method constructor <init>(IIILcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "chunkSize"    # I
    .param p3, "sampleRate"    # I
    .param p4, "psParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mId:I

    .line 44
    iput p2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSamplesPerChunk:I

    .line 45
    iput p3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSampleRate:I

    .line 46
    iput-object p4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .line 47
    mul-int/lit16 v0, p3, 0x4e20

    div-int/lit16 v0, v0, 0x3e8

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    .line 48
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    iget v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBAOutputStream:Ljava/io/ByteArrayOutputStream;

    .line 49
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBAOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mDataOutputStream:Ljava/io/DataOutputStream;

    .line 50
    return-void
.end method

.method static getInstance(IILcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;
    .locals 2
    .param p0, "chunkSize"    # I
    .param p1, "sampleRate"    # I
    .param p2, "psParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .prologue
    .line 39
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    sget v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->smLoggerId:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->smLoggerId:I

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;-><init>(IIILcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized dumpToFile()V
    .locals 14

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    iget-boolean v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mIsDone:Z

    if-nez v12, :cond_1

    .line 125
    new-instance v5, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "/sdcard/PhraseSpotter"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mId:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ".raw"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v5, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 126
    .local v5, "file":Ljava/io/File;
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 127
    .local v6, "fos":Ljava/io/FileOutputStream;
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-direct {v0, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 128
    .local v0, "bos":Ljava/io/BufferedOutputStream;
    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 130
    .local v4, "dos":Ljava/io/DataOutputStream;
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBAOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 131
    .local v1, "byteData":[B
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v12, v1

    if-ge v7, v12, :cond_0

    .line 132
    aget-byte v12, v1, v7

    invoke-virtual {v4, v12}, Ljava/io/DataOutputStream;->write(I)V

    .line 131
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 134
    :cond_0
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V

    .line 135
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    .line 137
    iget v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPStartingSample:I

    mul-int/lit16 v12, v12, 0x3e8

    iget v13, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSampleRate:I

    div-int v8, v12, v13

    .line 138
    .local v8, "pStartingSampleMs":I
    iget v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSStartingSample:I

    mul-int/lit16 v12, v12, 0x3e8

    iget v13, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSampleRate:I

    div-int v9, v12, v13

    .line 139
    .local v9, "sStartingSampleMs":I
    iget v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSpeechSample:I

    mul-int/lit16 v12, v12, 0x3e8

    iget v13, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSampleRate:I

    div-int v11, v12, v13

    .line 141
    .local v11, "speechSampleMs":I
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getDeltaD()I

    move-result v12

    sub-int v3, v8, v12

    .line 142
    .local v3, "dStartingSampleMs":I
    iget v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSampleRate:I

    mul-int/2addr v12, v3

    div-int/lit16 v2, v12, 0x3e8

    .line 144
    .local v2, "dStartingSample":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    .local v10, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v12, "CONFIG\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string/jumbo v12, "---------------\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string/jumbo v12, "CHUNK_LENGTH_MS: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getChunkLength()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 149
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string/jumbo v12, "PREBUFFER_LENGTH_MS: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getPreBufferLength()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 152
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string/jumbo v12, "DELTA_D: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getDeltaD()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 155
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string/jumbo v12, "DELTA_S: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getDeltaS()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 158
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    const-string/jumbo v12, "SEAMLESS_TIMEOUT_MS: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getSeamlessTimeout()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 161
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    const-string/jumbo v12, "\nPHRASESPOTTER PARAMS\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    const-string/jumbo v12, "---------------\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    const-string/jumbo v12, "Language: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getLanguage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    const-string/jumbo v12, "CGFilePath: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getCGFilename()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const-string/jumbo v12, "Beam: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getBeam()F

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 172
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    const-string/jumbo v12, "AbsBeam: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getAbsbeam()F

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 175
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    const-string/jumbo v12, "AOffset: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getAoffset()F

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 178
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    const-string/jumbo v12, "Delay: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    iget-object v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getDelay()F

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 181
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    const-string/jumbo v12, "\nDATA\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string/jumbo v12, "---------------\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string/jumbo v12, "SamplesPerChunk: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    iget v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSamplesPerChunk:I

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 186
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    const-string/jumbo v12, "SampleRate: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    iget v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSampleRate:I

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 189
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    const-string/jumbo v12, "D Sample#: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 192
    const-string/jumbo v12, " ("

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "ms)"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string/jumbo v12, "P Sample#: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    iget v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPStartingSample:I

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 196
    const-string/jumbo v12, " ("

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "ms)"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    const-string/jumbo v12, "S Sample#: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    iget v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSStartingSample:I

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 200
    const-string/jumbo v12, " ("

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "ms)"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    const-string/jumbo v12, "Speech Sample#: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    iget v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSpeechSample:I

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 204
    const-string/jumbo v12, " ("

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "ms)"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    const-string/jumbo v12, "Score: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    iget v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mScore:F

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 208
    const-string/jumbo v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    new-instance v5, Ljava/io/File;

    .end local v5    # "file":Ljava/io/File;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "/sdcard/PhraseSpotter"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mId:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ".txt"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v5, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 211
    .restart local v5    # "file":Ljava/io/File;
    new-instance v6, Ljava/io/FileOutputStream;

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 212
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    new-instance v0, Ljava/io/BufferedOutputStream;

    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    invoke-direct {v0, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 213
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    new-instance v4, Ljava/io/DataOutputStream;

    .end local v4    # "dos":Ljava/io/DataOutputStream;
    invoke-direct {v4, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 214
    .restart local v4    # "dos":Ljava/io/DataOutputStream;
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/io/DataOutputStream;->write([B)V

    .line 216
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V

    .line 217
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 223
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .end local v1    # "byteData":[B
    .end local v2    # "dStartingSample":I
    .end local v3    # "dStartingSampleMs":I
    .end local v4    # "dos":Ljava/io/DataOutputStream;
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v7    # "i":I
    .end local v8    # "pStartingSampleMs":I
    .end local v9    # "sStartingSampleMs":I
    .end local v10    # "sb":Ljava/lang/StringBuilder;
    .end local v11    # "speechSampleMs":I
    :cond_1
    const/4 v12, 0x1

    :try_start_1
    iput-boolean v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mIsDone:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    :goto_1
    monitor-exit p0

    return-void

    .line 220
    :catch_0
    move-exception v12

    .line 223
    const/4 v12, 0x1

    :try_start_2
    iput-boolean v12, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mIsDone:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 124
    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12

    .line 223
    :catchall_1
    move-exception v12

    const/4 v13, 0x1

    :try_start_3
    iput-boolean v13, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mIsDone:Z

    throw v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method declared-synchronized markDecisionSample(Z)V
    .locals 1
    .param p1, "isSeamless"    # Z

    .prologue
    .line 111
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mDataOutputStream:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSpeechSample:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :goto_0
    monitor-exit p0

    return-void

    .line 117
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->dumpToFile()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized markP()V
    .locals 1

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mDataOutputStream:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPStartingSample:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    monitor-exit p0

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized markS()V
    .locals 1

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mDataOutputStream:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSStartingSample:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized setScore(F)V
    .locals 1
    .param p1, "score"    # F

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mScore:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    monitor-exit p0

    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public writeData([BII)V
    .locals 5
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 54
    add-int v2, p2, p3

    .line 56
    .local v2, "indexLimit":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    if-lez v3, :cond_2

    .line 58
    :try_start_0
    iget-boolean v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mGotLowByte:Z

    if-nez v3, :cond_1

    .line 59
    aget-byte v3, p1, v1

    iput-byte v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mLowByte:B

    .line 60
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mGotLowByte:Z

    .line 67
    :goto_1
    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    .line 68
    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    if-nez v3, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->dumpToFile()V

    .line 56
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    :cond_1
    iget-object v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mDataOutputStream:Ljava/io/DataOutputStream;

    aget-byte v4, p1, v1

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 64
    iget-object v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mDataOutputStream:Ljava/io/DataOutputStream;

    iget-byte v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mLowByte:B

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 65
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mGotLowByte:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 77
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    return-void
.end method

.method public writeData([SII)V
    .locals 5
    .param p1, "data"    # [S
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 81
    add-int v2, p2, p3

    .line 83
    .local v2, "indexLimit":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    if-lez v3, :cond_1

    .line 85
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mDataOutputStream:Ljava/io/DataOutputStream;

    aget-short v4, p1, v1

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 86
    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    add-int/lit8 v3, v3, -0x2

    iput v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    .line 87
    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    if-nez v3, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->dumpToFile()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 96
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    return-void
.end method
