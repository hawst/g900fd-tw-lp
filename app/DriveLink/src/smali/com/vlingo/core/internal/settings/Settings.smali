.class public abstract Lcom/vlingo/core/internal/settings/Settings;
.super Ljava/lang/Object;
.source "Settings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;
    }
.end annotation


# static fields
.field public static final ASR_EDITING_ENABLED_LANGUAGES_ALL:Ljava/lang/String; = "All"

.field public static final AS_CONFIG_AUTO_ACTION_REC_CONFIDENCE_THRESHOLD:Ljava/lang/String; = "config.autoaction.rec_confidence_threshold"

.field public static final AS_CONFIG_FIRST_RUN:Ljava/lang/String; = "appstate.first_run.calypso"

.field public static final AS_CONFIG_LAST_UPDATE:Ljava/lang/String; = "appstate.config_last_update"

.field public static final AS_CONFIG_RAW_UTT_XMIT_PERCENT:Ljava/lang/String; = "Config.RawUtt.Xmit.Percent"

.field public static final AS_CONFIG_UPDATE_COUNT:Ljava/lang/String; = "appstate.config_update_count"

.field public static final AUTO_DIAL_VALUE_ALWAYS:Ljava/lang/String; = "always"

.field public static final AUTO_DIAL_VALUE_CONFIDENT:Ljava/lang/String; = "confident"

.field public static final AUTO_ENDPOINTING_DEFAULT:Z = true

.field public static final AUTO_LISTEN:Ljava/lang/String; = "home_auto_listen"

.field public static final CAR_AUTO_LISTEN_ENABLED:Ljava/lang/String; = "car_auto_listen_enabled"

.field public static final CAR_KEYWORD_SPOTTING_DEFAULT:Z = true

.field public static final CMA_APP_ID_DEFAULT:Ljava/lang/String; = "f63d32"

.field public static final CMA_APP_ID_FULL_DEFAULT:Ljava/lang/String; = "f63d329270a44900"

.field public static final CMA_PRIVATE_KEY_DEFAULT:Ljava/lang/String; = "sanx_data_99"

.field public static final CONFIG_DISABLE_FETCH_CONFIG:Ljava/lang/String; = "config.fetch.disable"

.field public static final CONTACTS_USE_OTHER_NAMES:Ljava/lang/String; = "contacts.use_other_names"

.field public static final CONTACTS_USE_OTHER_NAMES_DEFAULT:Z = true

.field public static final DEFAULT_APP_ID:Ljava/lang/String; = "com.vlingo.d2c"

.field public static final DEFAULT_ASR_EDITING_ENABLED_LANGUAGES:Ljava/lang/String; = "All"

.field public static final DEFAULT_ASR_MANAGER:Z

.field public static final DEFAULT_CARRIER:Ljava/lang/String; = ""

.field public static final DEFAULT_CARRIER_COUNTRY:Ljava/lang/String; = ""

.field public static final DEFAULT_CAR_AUTO_START_SPEAKERPHONE:Z = false

.field public static final DEFAULT_CAR_MODE_ENABLED:Z = false

.field public static final DEFAULT_CAR_VOLUME:I = 0x5a

.field public static final DEFAULT_CORE_WEB_SEARCH_ENGINE:Ljava/lang/String; = "Google"

.field public static final DEFAULT_CORE_WEB_SEARCH_URL:Ljava/lang/String; = "http://www.google.com/m?cx=partner-pub-5324388728707269:o6qccq-17aj&amp;q={query}"

.field public static final DEFAULT_DECIDER_ACTIONNAME_TO_DOMAIN_MAPPING:Ljava/lang/String; = "ShowCallWidget=voicedial"

.field public static final DEFAULT_DECIDER_FIELDID_TO_DOMAIN_MAPPING:Ljava/lang/String; = "dm_dial_contact=voicedial,dm_dial_contact_disambig=voicedial,dm_dial_type=voicedial"

.field public static final DEFAULT_DEVICE_MODEL:Ljava/lang/String; = ""

.field public static final DEFAULT_DISABLE_FETCH_CONFIG:Z = false

.field public static final DEFAULT_DOWNLOAD_DELAY:Ljava/lang/String; = "0"

.field public static final DEFAULT_DOWNLOAD_TIMEOUT:Ljava/lang/String; = "30"

.field public static final DEFAULT_ENDPOINT_SILENCE_SPEECH_LONG:I = 0x6d6

.field public static final DEFAULT_ENDPOINT_SILENCE_SPEECH_LONG_LONG:I = 0x8ca

.field public static final DEFAULT_ENDPOINT_SILENCE_SPEECH_MEDIUM:I = 0x2ee

.field public static final DEFAULT_ENDPOINT_SILENCE_SPEECH_MEDIUM_LONG:I = 0x4e2

.field public static final DEFAULT_ENDPOINT_SILENCE_SPEECH_SHORT:I = 0x190

.field public static final DEFAULT_HELLO_REQUEST_COMPLETE:Z = false

.field public static final DEFAULT_IMAGE_OVERLAYS:Z = true

.field public static final DEFAULT_IMAGE_PRELOADS:Z = true

.field public static final DEFAULT_LOCATION_ENABLED:Z = false

.field public static final DEFAULT_MULTI_WIDGET_CLIENT_CAPPED:Z = true

.field public static final DEFAULT_MULTI_WIDGET_CLIENT_COLLAPSE:Z = true

.field public static final DEFAULT_MULTI_WIDGET_CLIENT_SHOWCOUNTS:Z = false

.field public static final DEFAULT_MULTI_WIDGET_CLIENT_SHOWMOREBUTTON:Z = false

.field public static final DEFAULT_MULTI_WIDGET_ITEMS_INITIAL_MAX:Ljava/lang/String; = "6"

.field public static final DEFAULT_MULTI_WIDGET_ITEMS_ULTIMATE_MAX:Ljava/lang/String; = "20"

.field public static final DEFAULT_PHRASESPOT_WAVEFORMLOGGING_ENABLED:Z = false

.field public static final DEFAULT_PROFANITY_FILTER_VALUE:Z = true

.field public static final DEFAULT_ROLLOUT_PERCENTAGE:I = 0x64

.field public static final DEFAULT_SAFEREADER_ALERT_ENABLED:Z = true

.field public static final DEFAULT_SAFEREADER_DELAY:I = 0x0

.field public static final DEFAULT_SAFEREADER_OFF_WHEN_SILENT:Z = false

.field public static final DEFAULT_SAFEREADER_START_ON_BOOT:Z = true

.field public static final DEFAULT_SERVER_RESONSE_FILE:Ljava/lang/String; = "serverReponseFile"

.field public static final DEFAULT_SERVER_RESONSE_LOGGGING:Ljava/lang/String; = "None"

.field public static final DEFAULT_SHOW_ALL_LANGUAGES:Z = false

.field public static final DEFAULT_SPEEX_COMPLEXITY:I = 0x3

.field public static final DEFAULT_SPEEX_QUALITY:I = 0x8

.field public static final DEFAULT_SPEEX_VARIABLE_BITRATE:I = 0x0

.field public static final DEFAULT_SPEEX_VOICE_ACTIVITY_DETECTION:I = 0x0

.field public static final DEFAULT_TTS_CACHING_REQUIRED:Z = false

.field public static final DEFAULT_TTS_LOCAL_FALLBACK_ENGINE:Ljava/lang/String; = "com.google.android.tts"

.field public static final DEFAULT_TTS_LOCAL_FORCE_SPEECH_RATE:F = -1.0f

.field public static final DEFAULT_TTS_LOCAL_IGNORE_USER_SPEECH_RATE:Z = false

.field public static final DEFAULT_TTS_LOCAL_REQUIRED_ENGINE:Z = false

.field public static final DEFAULT_USE_DEFAULT_PHONE:Z = true

.field public static final DEFAULT_WEATHER_USE_VP_LOCATION:Ljava/lang/String; = "All"

.field public static final DRIVING_MODE_WIDGET_DISPLAY_MAX:I = 0x3

.field public static final ENDPOINT_SPEECHDETECT_MIN_VOICE_LEVEL_DEFAULT:F = 57.0f

.field public static final ENDPOINT_SPEECHDETECT_THRESHOLD_DEFAULT:F = 11.0f

.field public static final ENDPOINT_SPEECHDETECT_VOICE_DURATION_DEFAULT:F = 0.08f

.field public static final ENDPOINT_SPEECHDETECT_VOICE_PORTION_DEFAULT:F = 0.02f

.field public static final ENDPOINT_TIME_WITHOUTSPEECH_DEFAULT:I = 0x1388

.field public static final ENDPOINT_TIME_WITHSPEECH_DEFAULT:I = 0x6d6

.field public static final FACEBOOK_APP_ID:Ljava/lang/String; = "facebook_app_id"

.field public static final FACEBOOK_APP_ID_DEFAULT:Ljava/lang/String; = "39010226174"

.field public static final KEY_ACCEPTED_NOTIFICATIONS:Ljava/lang/String; = "accepted_notifications"

.field public static final KEY_ADS_ENABLED:Ljava/lang/String; = "ads.enabled"

.field public static final KEY_ADS_IS_USER_IN_APPROVED_GROUP:Ljava/lang/String; = "ads.user_in_approved_group"

.field public static final KEY_ADS_IS_USER_IN_OPT_OUT_GROUP:Ljava/lang/String; = "ads.user_in_opt_out_group"

.field public static final KEY_ADS_IS_USER_IN_OPT_OUT_GROUP_CHECKED:Ljava/lang/String; = "ads.user_in_opt_out_group.checked"

.field public static final KEY_ADS_PERCENT_OF_USERS_IN_OPT_OUT_GROUP:Ljava/lang/String; = "ads.percent_users_in_ad_opt_out_group"

.field public static final KEY_ALL_NOTIFICATIONS_ACCEPTED:Ljava/lang/String; = "all_notifications_accepted"

.field public static final KEY_ALWAYS_SEND_RAW_UTTS:Ljava/lang/String; = "always_send_raw_utts"

.field public static final KEY_ALWAYS_WARMUP_CONNECTIONS:Ljava/lang/String; = "always_warmup_connections"

.field public static final KEY_AM_MASTER:Ljava/lang/String; = "multiapp.am_master"

.field public static final KEY_ASR_EDITING_ENABLED_LANGUAGES:Ljava/lang/String; = "asr.editing.enabled.languages"

.field public static final KEY_ASR_MANAGER:Ljava/lang/String; = "asr.manager"

.field public static final KEY_ASR_SERVER_HOST:Ljava/lang/String; = "SERVER_NAME"

.field public static final KEY_AUDIO_FILE_LOG_ENABLED:Ljava/lang/String; = "audiofilelog_enabled"

.field public static final KEY_AUDIO_FILE_LOG_PHRASESPOT_INDEX:Ljava/lang/String; = "audiofilelog_index"

.field public static final KEY_AUDIO_TONES:Ljava/lang/String; = "settings.audio.tones"

.field public static final KEY_AUTO_DIAL:Ljava/lang/String; = "auto_dial"

.field public static final KEY_AUTO_ENDPOINTING:Ljava/lang/String; = "auto_endpointing"

.field public static final KEY_AUTO_PUNCTUATION:Ljava/lang/String; = "auto_punctuation"

.field public static final KEY_AUTO_SPEAK_ANSWER:Ljava/lang/String; = "auto_speak_answer"

.field public static final KEY_BARGE_IN_ENABLED:Ljava/lang/String; = "barge_in_enabled"

.field public static final KEY_BILLING_REFRESH_PURCHASES:Ljava/lang/String; = "billing.refresh_purchases"

.field public static final KEY_BLUETOOTH_HEADSET_CONNECTED:Ljava/lang/String; = "bluetooth_headset_connected"

.field public static final KEY_CARRIER:Ljava/lang/String; = "CARRIER"

.field public static final KEY_CARRIER_COUNTRY:Ljava/lang/String; = "CARRIER_COUNTRY"

.field public static final KEY_CAR_ANSWERS_ENABLED:Ljava/lang/String; = "car_vlingo_answers_enabled"

.field public static final KEY_CAR_AUTO_START_SPEAKERPHONE:Ljava/lang/String; = "car_auto_start_speakerphone"

.field public static final KEY_CAR_DO_NOT_REMIND:Ljava/lang/String; = "car_do_not_remind"

.field public static final KEY_CAR_FIRST_RUN:Ljava/lang/String; = "car_first_run_calypso"

.field public static final KEY_CAR_IUX_INTRO_REQUIRED:Ljava/lang/String; = "car_iux_intro_required"

.field public static final KEY_CAR_IUX_TTS_DOWNLOADED:Ljava/lang/String; = "car_iux_tts_downloaded"

.field public static final KEY_CAR_IUX_WORD_SPOT_INTRO_REQUIRED:Ljava/lang/String; = "car_iux_word_spot_intro_required"

.field public static final KEY_CAR_KEYWORD_SPOTTING_ENABLED:Ljava/lang/String; = "car_word_spotter_enabled"

.field public static final KEY_CAR_KEYWORD_SPOTTING_ONLY_WHEN_CHARGING:Ljava/lang/String; = "car_word_spotter_when_charging_only"

.field public static final KEY_CAR_MODE_ENABLED:Ljava/lang/String; = "driving_mode_on"

.field public static final KEY_CAR_NAV_HOME_ADDRESS:Ljava/lang/String; = "car_nav_home_address"

.field public static final KEY_CAR_SMS_LAST_WAS_RECENTS:Ljava/lang/String; = "car_sms_last_was_recents"

.field public static final KEY_CAR_SPEAK_INCOMING_CALLS:Ljava/lang/String; = "tts_carmode_speak_incoming_calls"

.field public static final KEY_CAR_SPEAK_WHEN_STARTED:Ljava/lang/String; = "tts_carmode_speak_car_on_confirmation"

.field public static final KEY_CAR_STARTUP_TTS_PROMPT:Ljava/lang/String; = "tts_carmode_startup_prompt"

.field public static final KEY_CAR_TTS_DOWNLOAD:Ljava/lang/String; = "car_tts_download"

.field public static final KEY_CAR_VD_LAST_WAS_RECENTS:Ljava/lang/String; = "car_vd_last_was_recents"

.field public static final KEY_CMA_APP_ID:Ljava/lang/String; = "cma_app_id"

.field public static final KEY_CMA_APP_ID_FULL:Ljava/lang/String; = "cma_app_id_full"

.field public static final KEY_CMA_PRIVATE_KEY:Ljava/lang/String; = "cma_private_key"

.field public static final KEY_CONFIG_WARMUP_CONN_PERCENT:Ljava/lang/String; = "Config.WarmupConnection.Percent"

.field public static final KEY_COOKIE_DATA:Ljava/lang/String; = "cookie_data"

.field public static final KEY_CUSTOM_TONE_ENCODING:Ljava/lang/String; = "custom_tone_encoding"

.field public static final KEY_DECIDER_ACTIONNAME_TO_DOMAIN_MAPPING:Ljava/lang/String; = "DECIDER_ACTIONNAME_TO_DOMAIN_MAPPING"

.field public static final KEY_DECIDER_FIELDID_TO_DOMAIN_MAPPING:Ljava/lang/String; = "DECIDER_FIELDID_TO_DOMAIN_MAPPING"

.field public static final KEY_DEFAULT_CALENDAR_KEY:Ljava/lang/String; = "calendar.default_calendar_key"

.field public static final KEY_DEVICE_MODEL:Ljava/lang/String; = "DEVICE_MODEL"

.field public static final KEY_DM_AUTO_CONFIRM_TIMEOUT:Ljava/lang/String; = "DIALOG_MANAGER_AUTO_CONFIRM_TIMEOUT"

.field public static final KEY_DM_ENDPOINT_THRESHOLD:Ljava/lang/String; = "DIALOG_MANAGER_ENDPOINT_THRESHOLD"

.field public static final KEY_DM_USERNAME:Ljava/lang/String; = "dm.username"

.field public static final KEY_DM_WORKING_MESSAGES:Ljava/lang/String; = "DIALOG_MANAGER_WORKING_MESSAGES"

.field public static final KEY_DM_WORKING_MESSAGE_INTERVAL:Ljava/lang/String; = "DIALOG_MANAGER_WORKING_MESSAGE_INTERVAL"

.field public static final KEY_DOWNLOAD_DELAY:Ljava/lang/String; = "wa.download.delay"

.field public static final KEY_DOWNLOAD_TIMEOUT:Ljava/lang/String; = "wa.download.timeout"

.field public static final KEY_DRIVING_MODE_AUDIO_FILES:Ljava/lang/String; = "driving_mode_audio_files"

.field public static final KEY_DRIVING_MODE_MESSAGES_NOTIFICATION:Ljava/lang/String; = "driving_mode_message_notification"

.field public static final KEY_DYNAMIC_CONFIG_DISABLED:Ljava/lang/String; = "dynamic_config_disabled"

.field public static final KEY_ENABLE_ASR_EVENT_LOG:Ljava/lang/String; = "asr_event_logging"

.field public static final KEY_ENABLE_NLU_EVENT_LOG:Ljava/lang/String; = "nlu_event_logging"

.field public static final KEY_ENDPOINT_SILENCE_SPEECH_LONG:Ljava/lang/String; = "endpoint.time.speech.long"

.field public static final KEY_ENDPOINT_SILENCE_SPEECH_LONG_LONG:Ljava/lang/String; = "endpoint.time.speech.long.msg"

.field public static final KEY_ENDPOINT_SILENCE_SPEECH_MEDIUM:Ljava/lang/String; = "endpoint.time.speech.medium"

.field public static final KEY_ENDPOINT_SILENCE_SPEECH_MEDIUM_LONG:Ljava/lang/String; = "endpoint.time.speech.medium.long"

.field public static final KEY_ENDPOINT_SILENCE_SPEECH_SHORT:Ljava/lang/String; = "endpoint.time.speech.short"

.field public static final KEY_ENDPOINT_SPEECHDETECT_MIN_VOICE_LEVEL:Ljava/lang/String; = "endpoint.speechdetect_min_voice_level"

.field public static final KEY_ENDPOINT_SPEECHDETECT_THRESHOLD:Ljava/lang/String; = "endpoint.speechdetect_threshold"

.field public static final KEY_ENDPOINT_SPEECHDETECT_VOICE_DURATION:Ljava/lang/String; = "endpoint.speechdetect_voice_duration"

.field public static final KEY_ENDPOINT_SPEECHDETECT_VOICE_PORTION:Ljava/lang/String; = "endpoint.speechdetect_voice_portion"

.field public static final KEY_ENDPOINT_TIME_WITHOUTSPEECH:Ljava/lang/String; = "endpoint.time_withoutspeech"

.field public static final KEY_ENDPOINT_TIME_WITHSPEECH:Ljava/lang/String; = "endpoint.time_withspeech"

.field public static final KEY_FAKE_DEVICE_MODEL:Ljava/lang/String; = "FAKE_DEVICE_MODEL"

.field public static final KEY_FAKE_LAT:Ljava/lang/String; = "FAKE_LAT"

.field public static final KEY_FAKE_LAT_LONG:Ljava/lang/String; = "FAKE_LAT_LONG"

.field public static final KEY_FAKE_LONG:Ljava/lang/String; = "FAKE_LONG"

.field public static final KEY_FIELD_ID:Ljava/lang/String; = "FIELD_ID"

.field public static final KEY_FIRST_UTT_COMPLETE:Ljava/lang/String; = "vlingo.first_utt_complete"

.field public static final KEY_FORCE_NON_DM:Ljava/lang/String; = "FORCE_NON_DM"

.field public static final KEY_FORMER_TOS_ACCEPTANCE_STATE:Ljava/lang/String; = "former_tos_acceptance_state"

.field public static final KEY_HAS_USER_PAID_FOR_ADS:Ljava/lang/String; = "ads.paid"

.field public static final KEY_HELLO_REQUEST_COMPLETE:Ljava/lang/String; = "hello_request_complete"

.field public static final KEY_HELLO_SERVER_HOST:Ljava/lang/String; = "HELLO_HOST_NAME"

.field public static final KEY_HELP_ABOUT:Ljava/lang/String; = "help_about"

.field public static final KEY_HELP_UPDATE:Ljava/lang/String; = "help_update"

.field public static final KEY_HOME_SCREEN_FIRST_DISPLAY:Ljava/lang/String; = "home_screen_first_display_calypso"

.field public static final KEY_HOME_SCREEN_SHOW_INVITE_BAR:Ljava/lang/String; = "home_screen_show_invite_bar"

.field public static final KEY_HTTPS_ASR:Ljava/lang/String; = "https.asr_enabled"

.field public static final KEY_HTTPS_HELLO:Ljava/lang/String; = "https.hello_enabled"

.field public static final KEY_HTTPS_LMTT:Ljava/lang/String; = "https.lmtt_enabled"

.field public static final KEY_HTTPS_LOG:Ljava/lang/String; = "https.log_enabled"

.field public static final KEY_HTTPS_ROLLOUT_GROUPID:Ljava/lang/String; = "https.rollout_groupid"

.field public static final KEY_HTTPS_ROLLOUT_PERCENTAGE:Ljava/lang/String; = "https.rollout_percentage"

.field public static final KEY_HTTPS_TTS:Ljava/lang/String; = "https.tts_enabled"

.field public static final KEY_HTTPS_VCS:Ljava/lang/String; = "https.vcs_enabled"

.field public static final KEY_IMAGE_OVERLAYS:Ljava/lang/String; = "wa.image.overlays"

.field public static final KEY_IMAGE_PRELOADS:Ljava/lang/String; = "wa.image.preloads"

.field public static final KEY_IN_CAR_MODE:Ljava/lang/String; = "in_car_mode"

.field public static final KEY_IUX_COMPLETE:Ljava/lang/String; = "iux_complete"

.field public static final KEY_IUX_STARTED:Ljava/lang/String; = "iux_started"

.field public static final KEY_KOREAN_NAME_SIMILARITY_VALUE_MIN:Ljava/lang/String; = "korean_name_similarity_value_min"

.field public static final KEY_LANGUAGE:Ljava/lang/String; = "language"

.field public static final KEY_LANGUAGE_DE_DE_ENABLE:Ljava/lang/String; = "de-DE_enable"

.field public static final KEY_LANGUAGE_EN_GB_ENABLE:Ljava/lang/String; = "en-GB_enable"

.field public static final KEY_LANGUAGE_EN_US_ENABLE:Ljava/lang/String; = "en-US_enable"

.field public static final KEY_LANGUAGE_ES_ES_ENABLE:Ljava/lang/String; = "es-ES_enable"

.field public static final KEY_LANGUAGE_FR_FR_ENABLE:Ljava/lang/String; = "fr-FR_enable"

.field public static final KEY_LANGUAGE_IT_IT_ENABLE:Ljava/lang/String; = "it-IT_enable"

.field public static final KEY_LANGUAGE_JA_JP_ENABLE:Ljava/lang/String; = "ja-JP_enable"

.field public static final KEY_LANGUAGE_KO_KR_ENABLE:Ljava/lang/String; = "ko-KR_enable"

.field public static final KEY_LANGUAGE_PT_BR_ENABLE:Ljava/lang/String; = "pt-BR_enable"

.field public static final KEY_LANGUAGE_RU_RU_ENABLE:Ljava/lang/String; = "ru-RU_enable"

.field public static final KEY_LANGUAGE_V_ES_LA_ENABLE:Ljava/lang/String; = "v-es-LA_enable"

.field public static final KEY_LANGUAGE_V_ES_NA_ENABLE:Ljava/lang/String; = "v-es-NA_enable"

.field public static final KEY_LANGUAGE_ZH_CN_ENABLE:Ljava/lang/String; = "zh-CN_enable"

.field public static final KEY_LAST_HOME_CONTENT_STATE:Ljava/lang/String; = "home_content_state_last"

.field public static final KEY_LAUNCHED_FOR_TOS_ACCEPT_AS_PART_OF_OTHER_APP:Ljava/lang/String; = "tos_launched_for_tos_other_app"

.field public static final KEY_LAUNCH_CAR_ON_BT_CONNECT:Ljava/lang/String; = "launch_car_on_bt_connect"

.field public static final KEY_LISTEN_OVER_BLUETOOTH:Ljava/lang/String; = "listen_over_bluetooth"

.field public static final KEY_LMTT_CHUNK_DELAY:Ljava/lang/String; = "lmtt.chunk_delay_ms"

.field public static final KEY_LMTT_CHUNK_RETRIES:Ljava/lang/String; = "lmtt.chunk_retries"

.field public static final KEY_LMTT_CHUNK_RETRY_DELAY:Ljava/lang/String; = "lmtt.chunk_retry_delay_ms"

.field public static final KEY_LMTT_CHUNK_SIZE:Ljava/lang/String; = "lmtt.chunk_size"

.field public static final KEY_LMTT_CLIENT_SHIELD_DURATION:Ljava/lang/String; = "lmtt.client_shield_duration_mins"

.field public static final KEY_LMTT_FORCE_FULLUPDATE_ON_START:Ljava/lang/String; = "lmtt.force_fullupdate_on_start"

.field public static final KEY_LMTT_INITIAL_UPLOAD_DONE_PREFIX:Ljava/lang/String; = "lmtt.initial_upload_done."

.field public static final KEY_LMTT_LAST_APP_START_TIME:Ljava/lang/String; = "lmtt.last_app_start_time"

.field public static final KEY_LMTT_LAST_CHUNK_RETRYCOUNT:Ljava/lang/String; = "lmtt.last_chunk_retrycount"

.field public static final KEY_LMTT_LAST_TRANSMIT_TIMESTAMP:Ljava/lang/String; = "lmtt.last_transmit_timestamp"

.field public static final KEY_LMTT_NOACTIVITY_SHUTDOWN_PERIOD:Ljava/lang/String; = "lmtt.no_activity_shutdown_period_mins"

.field public static final KEY_LMTT_SERVER_HOST:Ljava/lang/String; = "LMTT_HOST_NAME"

.field public static final KEY_LMTT_STATUS_PREFIX:Ljava/lang/String; = "lmtt.enable."

.field public static final KEY_LMTT_SYNC_CONTACT:Ljava/lang/String; = "lmtt.sync_status_contact"

.field public static final KEY_LMTT_SYNC_MUSIC:Ljava/lang/String; = "lmtt.sync_status_music"

.field public static final KEY_LMTT_TASK_RETRIES:Ljava/lang/String; = "lmtt.task_retries"

.field public static final KEY_LMTT_UPDATE_VERSION_CURRENT:Ljava/lang/String; = "lmtt.update.version"

.field public static final KEY_LMTT_UPDATE_VERSION_PREVIOUS_SUFFIX:Ljava/lang/String; = ".previous"

.field public static final KEY_LMTT_VOCON_FORCE_FULL_UPDATE_ON_START:Ljava/lang/String; = "lmttvocon.force_start_from_scratch"

.field public static final KEY_LMTT_VOCON_LAST_ACTIVITY_TIME:Ljava/lang/String; = "lmttvocon.last_app_activity_time"

.field public static final KEY_LMTT_VOCON_NO_ACTIVITY_SHUTDOWN_TIMEOUT_MINS:Ljava/lang/String; = "lmttvocon.no_activity_shutdown_timeout"

.field public static final KEY_LOCAL_SEARCH_MAX_LISTINGS:Ljava/lang/String; = "localsearch.max_spon_listing"

.field public static final KEY_LOCATION_ENABLED:Ljava/lang/String; = "location_enabled"

.field public static final KEY_LOCATION_OVERRIDE_ENABLED:Ljava/lang/String; = "location.override.enabled"

.field public static final KEY_LOCATION_OVERRIDE_PRESET:Ljava/lang/String; = "location.override.preset"

.field public static final KEY_LOG_SERVER_HOST:Ljava/lang/String; = "EVENTLOG_HOST_NAME"

.field public static final KEY_MARKET_AVAILABLE:Ljava/lang/String; = "ads.market.available"

.field public static final KEY_MARKET_CHECKED:Ljava/lang/String; = "ads.market.checked"

.field public static final KEY_MAX_AUDIO_TIME:Ljava/lang/String; = "max_audio_time"

.field public static final KEY_MAX_WIDTH:Ljava/lang/String; = "max.width"

.field public static final KEY_MEID:Ljava/lang/String; = "MEID"

.field public static final KEY_MIMIC_MODE:Ljava/lang/String; = "mimic_mode"

.field public static final KEY_MULTI_WIDGET_CLIENT_CAPPED:Ljava/lang/String; = "multi.widget.client.capped"

.field public static final KEY_MULTI_WIDGET_CLIENT_COLLAPSE:Ljava/lang/String; = "multi.widget.client.collapse"

.field public static final KEY_MULTI_WIDGET_CLIENT_SHOWCOUNTS:Ljava/lang/String; = "multi.widget.client.showcounts"

.field public static final KEY_MULTI_WIDGET_CLIENT_SHOWMOREBUTTON:Ljava/lang/String; = "multi.widget.client.showmorebutton"

.field public static final KEY_MULTI_WIDGET_ITEMS_INITIAL_MAX:Ljava/lang/String; = "multi.widget.item.initial.max"

.field public static final KEY_MULTI_WIDGET_ITEMS_ULTIMATE_MAX:Ljava/lang/String; = "multi.widget.item.ultimate.max"

.field public static final KEY_NAME_CALENDAR_PACKAGE:Ljava/lang/String; = "calendar.app_package"

.field public static final KEY_NAME_CALENDAR_PREFERENCE:Ljava/lang/String; = "calendar.preference_filename"

.field public static final KEY_NETWORK_TTS_TIMEOUT:Ljava/lang/String; = "network_tts_timeout"

.field public static final KEY_NEW_CONTACT_MATCH_ALGO:Ljava/lang/String; = "new_contact_match_algo"

.field public static final KEY_NEW_FEATURE_DIALOG_SHOWN:Ljava/lang/String; = "new_feature_dialog_shown"

.field public static final KEY_NOTHING_RECOGNIZED_REPROMPT_COUNT:Ljava/lang/String; = "nothing_recognized_reprompt.count"

.field public static final KEY_NOTHING_RECOGNIZED_REPROMPT_MAX_VALUE:Ljava/lang/String; = "nothing_recognized_reprompt.max_value"

.field public static final KEY_NOTIFICATION_CONTENT:Ljava/lang/String; = "notification_content"

.field public static final KEY_NOTIFICATION_COUNTER:Ljava/lang/String; = "notification_counter"

.field public static final KEY_NOTIFICATION_COUNTER_LOCAL:Ljava/lang/String; = "notification_counter_local"

.field public static final KEY_OBEY_DEVICE_LOCATION_SETTINGS:Ljava/lang/String; = "obey_device_location_settings"

.field public static final KEY_PHRASESPOT_ABSBEAM:Ljava/lang/String; = "phrasespot_absbeam"

.field public static final KEY_PHRASESPOT_AOFFSET:Ljava/lang/String; = "phrasespot_aoffset"

.field public static final KEY_PHRASESPOT_BEAM:Ljava/lang/String; = "phrasespot_beam"

.field public static final KEY_PHRASESPOT_DELAY:Ljava/lang/String; = "phrasespot_delay"

.field public static final KEY_PHRASESPOT_PARAMA:Ljava/lang/String; = "phrasespot_parama"

.field public static final KEY_PHRASESPOT_PARAMB:Ljava/lang/String; = "phrasespot_paramb"

.field public static final KEY_PHRASESPOT_PARAMC:Ljava/lang/String; = "phrasespot_paramc"

.field public static final KEY_PHRASESPOT_WAVEFORMLOGGING_ENABLED:Ljava/lang/String; = "phrasespot_waveformlogging_enabled"

.field public static final KEY_PLOT_WIDTH:Ljava/lang/String; = "plot.width"

.field public static final KEY_PREVIOUS_MASTER:Ljava/lang/String; = "multiapp.previous_master"

.field public static final KEY_PROCESSING_TONE_FADEOUT:Ljava/lang/String; = "processing_tone_fadeout_period"

.field public static final KEY_PROFANITY_FILTER:Ljava/lang/String; = "profanity_filter"

.field public static final KEY_RAW_UTTS_IS_USER_IN_SEND_GROUP:Ljava/lang/String; = "rawutts.user_in_send_group"

.field public static final KEY_RAW_UTTS_IS_USER_IN_SEND_GROUP_CHECKED:Ljava/lang/String; = "rawutts.user_in_send_group.checked"

.field public static final KEY_RAW_UTTS_LAST_XMIT:Ljava/lang/String; = "rawutts.last_xmit_time"

.field public static final KEY_REMOTE_HAS_UIFOCUS:Ljava/lang/String; = "uifocus.remote_has_focus"

.field public static final KEY_RUNNING_TIMER:Ljava/lang/String; = "running_timer"

.field public static final KEY_SAFEREADER_ACCOUNTS_CHANGED:Ljava/lang/String; = "safereader_email_accounts_changed"

.field public static final KEY_SAFEREADER_ALERT_ENABLED:Ljava/lang/String; = "car_safereader_enable_alert"

.field public static final KEY_SAFEREADER_DELAY:Ljava/lang/String; = "safereader.delay"

.field public static final KEY_SAFEREADER_EMAIL_ACCOUNTS:Ljava/lang/String; = "safereader_email_accounts"

.field public static final KEY_SAFEREADER_EMAIL_POLL_INTERVAL:Ljava/lang/String; = "safereader_email_poll_interval"

.field public static final KEY_SAFEREADER_EMAIL_READBACK_SETTING:Ljava/lang/String; = "safereader_email_enabled"

.field public static final KEY_SAFEREADER_INTRO_SHOWN:Ljava/lang/String; = "safereader_shown"

.field public static final KEY_SAFEREADER_LAST_SAFEREAD_TIME:Ljava/lang/String; = "car_safereader_last_saferead_time"

.field public static final KEY_SAFEREADER_OFF_WHEN_SILENT:Ljava/lang/String; = "car_safereader_off_when_silent"

.field public static final KEY_SAFEREADER_RUN_IN_BACKGROUND:Ljava/lang/String; = "car_safereader_run_in_background"

.field public static final KEY_SAFEREADER_START_ON_BOOT:Ljava/lang/String; = "safereader_start_on_boot"

.field public static final KEY_SCREEN_MAG:Ljava/lang/String; = "screen.mag"

.field public static final KEY_SCREEN_WIDTH:Ljava/lang/String; = "screen.width"

.field public static final KEY_SEND_WAKEUP_WORD_HEADERS:Ljava/lang/String; = "SEND_WAKEUP_WORD_HEADERS"

.field public static final KEY_SERVER_RESONSE_FILE:Ljava/lang/String; = "SERVER_RESONSE_FILE"

.field public static final KEY_SERVER_RESONSE_LOGGGING:Ljava/lang/String; = "SERVER_RESONSE_LOGGGING"

.field public static final KEY_SET_AUDIO_VOLUME_FOR_ASSOC_SVC:Ljava/lang/String; = "set_audio_volume_for_assoc_svc"

.field public static final KEY_SHOWING_NOTIFICATIONS:Ljava/lang/String; = "showing_notifications"

.field public static final KEY_SHOW_ALL_LANGUAGES:Ljava/lang/String; = "show_all_languages"

.field public static final KEY_SHOW_UUID_IN_ABOUT_SCREEN:Ljava/lang/String; = "show_uuid_in_about_screen"

.field public static final KEY_SOCIAL_LOGIN_ATTEMPT_FOR_RESUME:Ljava/lang/String; = "key_social_login_attemp_for_resume"

.field public static final KEY_SOCIAL_LOGIN_ATTEMPT_FOR_WINDOW_FOCUS_CHANGED:Ljava/lang/String; = "key_social_login_attemp_for_user_leave_hint"

.field public static final KEY_SPEAKERID:Ljava/lang/String; = "speaker_id"

.field public static final KEY_SPEEX_COMPLEXITY:Ljava/lang/String; = "speex.complexity"

.field public static final KEY_SPEEX_QUALITY:Ljava/lang/String; = "speex.quality"

.field public static final KEY_SPEEX_VARIABLE_BITRATE:Ljava/lang/String; = "speex.variable_bitrate"

.field public static final KEY_SPEEX_VOICE_ACTIVITY_DETECTION:Ljava/lang/String; = "speex.voice_activity_detection"

.field public static final KEY_START_LISTENING_IMMEDIATELY:Ljava/lang/String; = "START_LISTENING_IMMEDIATELY"

.field public static final KEY_SUPER_DIALER_DETAILS_REMINDER_SHOWN:Ljava/lang/String; = "sd_reminder_shown"

.field public static final KEY_TIMINGLOG_ENABLED:Ljava/lang/String; = "timinglog_enabled"

.field public static final KEY_TOS_ACCEPTED:Ljava/lang/String; = "tos_accepted"

.field public static final KEY_TOS_ACCEPTED_DATE:Ljava/lang/String; = "tos_accepted_date"

.field public static final KEY_TOS_ACCEPTED_VERSION:Ljava/lang/String; = "tos_accepted_version"

.field public static final KEY_TOS_NOTIFICATION_CONTENT:Ljava/lang/String; = "tos_notification_content"

.field public static final KEY_TOS_NOTIFICATION_COUNTER:Ljava/lang/String; = "tos_notification_counter"

.field public static final KEY_TOS_NOTIFICATION_COUNTER_LOCAL:Ljava/lang/String; = "tos_notification_counter_local"

.field public static final KEY_TTS_CACHING_REQUIRED:Ljava/lang/String; = "car_iux_tts_cacheing_required"

.field public static final KEY_TTS_CARMODE_CONFIRM:Ljava/lang/String; = "tts_carmode_confirm"

.field public static final KEY_TTS_LANGUAGE:Ljava/lang/String; = "tts_language"

.field public static final KEY_TTS_LOCAL_FALLBACK_ENGINE:Ljava/lang/String; = "tts_local_tts_fallback_engine"

.field public static final KEY_TTS_LOCAL_FORCE_SPEECH_RATE:Ljava/lang/String; = "tts_local_force_speech_rate"

.field public static final KEY_TTS_LOCAL_IGNORE_USER_SPEECH_RATE:Ljava/lang/String; = "tts_local_ignore_use_speech_rate"

.field public static final KEY_TTS_LOCAL_REQUIRED_ENGINE:Ljava/lang/String; = "tts_local_required_engine"

.field public static final KEY_TTS_VOICE:Ljava/lang/String; = "tts_voice"

.field public static final KEY_UNWATERMARKED_VERSIONS:Ljava/lang/String; = "unwatermarked.versions"

.field public static final KEY_UPDATE_INFO:Ljava/lang/String; = "update_info_xml"

.field public static final KEY_USE_AUDIOTRACK_TONE_PLAYER:Ljava/lang/String; = "use_audiotrack_tone_player"

.field public static final KEY_USE_DEFAULT_PHONE:Ljava/lang/String; = "use_default_phone"

.field public static final KEY_USE_EDM:Ljava/lang/String; = "USE_EDM"

.field public static final KEY_USE_HIDDEN_CALENDARS:Ljava/lang/String; = "use_hidden_calendars"

.field public static final KEY_USE_MEDIASYNC_APPROACH:Ljava/lang/String; = "use_mediasync_tone_approach"

.field public static final KEY_USE_NETWORK_TTS:Ljava/lang/String; = "use_network_tts"

.field public static final KEY_USE_SAFEREADER_NOTIFICATIONS:Ljava/lang/String; = "safereader_notifications"

.field public static final KEY_USE_STREAM_BLUETOOTH_SCO_FOR_ASSOC_SVC:Ljava/lang/String; = "use_stream_bluetooth_sco_for_assoc_svc"

.field public static final KEY_USE_VLINGO_TYPED_REQUESTS:Ljava/lang/String; = "use_vlingo_typed_requests"

.field public static final KEY_UUID:Ljava/lang/String; = "uuid"

.field public static final KEY_VALIDATE_LAUNCH_INTENT_VERSION:Ljava/lang/String; = "validate_launch_intent_version"

.field public static final KEY_VCS_SERVER_HOST:Ljava/lang/String; = "SERVICES_HOST_NAME"

.field public static final KEY_VCS_TIMEOUT_MS:Ljava/lang/String; = "vcs.timeout.ms"

.field public static final KEY_VOICE_THRESHOLD_LEVEL:Ljava/lang/String; = "voice_threshold_level"

.field public static final KEY_WEATHER_USE_VP_LOCATION:Ljava/lang/String; = "weather_use_vp_location"

.field public static final KEY_WEATHER_VERSION_TWO_LANGUAGES:Ljava/lang/String; = "weather_version_two_languages"

.field public static final KEY_WEB_SEARCH_BAIDU_DEFAULT:Ljava/lang/String; = "web_search_biadu_default"

.field public static final KEY_WEB_SEARCH_BAIDU_QUERY:Ljava/lang/String; = "web_search_biadu_query"

.field public static final KEY_WEB_SEARCH_BING_DEFAULT:Ljava/lang/String; = "web_search_bing_default"

.field public static final KEY_WEB_SEARCH_BING_QUERY:Ljava/lang/String; = "web_search_bing_query"

.field public static final KEY_WEB_SEARCH_DAUM_DEFAULT:Ljava/lang/String; = "web_search_daum_default"

.field public static final KEY_WEB_SEARCH_DAUM_QUERY:Ljava/lang/String; = "web_search_daum_query"

.field public static final KEY_WEB_SEARCH_DIRECT:Ljava/lang/String; = "web_search_direct"

.field public static final KEY_WEB_SEARCH_ENGINE:Ljava/lang/String; = "web_search_engine"

.field public static final KEY_WEB_SEARCH_GOOGLE_DEFAULT:Ljava/lang/String; = "web_search_google_default"

.field public static final KEY_WEB_SEARCH_GOOGLE_QUERY:Ljava/lang/String; = "web_search_google_query"

.field public static final KEY_WEB_SEARCH_HOME_URL:Ljava/lang/String; = "web_search_home_url"

.field public static final KEY_WEB_SEARCH_NAVER_DEFAULT:Ljava/lang/String; = "web_search_naver_default"

.field public static final KEY_WEB_SEARCH_NAVER_QUERY:Ljava/lang/String; = "web_search_naver_query"

.field public static final KEY_WEB_SEARCH_URL:Ljava/lang/String; = "web_search_url"

.field public static final KEY_WEB_SEARCH_YAHOO_DEFAULT:Ljava/lang/String; = "web_search_yahoo_default"

.field public static final KEY_WEB_SEARCH_YAHOO_QUERY:Ljava/lang/String; = "web_search_yahoo_query"

.field public static final KEY_WELCOME_NOTE_SHOWN:Ljava/lang/String; = "welcome_note_shown"

.field public static final KEY_WIDGET_DISPLAY_MAX:Ljava/lang/String; = "widget_display_max"

.field public static final KNOWN_BAD_SYSTEM_SETTING_BOOLEAN_VALUE:I = -0x63

.field private static final KNOWN_BAD_UUID:Ljava/lang/String; = "0"

.field public static final KOREAN_NAME_SIMILARITY_VALUE_MIN_DEFAULT:F = 0.8f

.field public static final LANGUAGES_ALL:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LANGUAGES_CEFIGS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LANGUAGES_CHINESE_ONLY:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LANGUAGES_CJ:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LANGUAGES_CJK:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LANGUAGES_EFIGS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LANGUAGES_EFIGSJK:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LANGUAGES_ENGLISH:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LANGUAGES_FIGS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LANGUAGES_US_ONLY:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LANGUAGE_DEFAULT:Ljava/lang/String; = "en-US"

.field public static final LANGUAGE_DE_DE:Ljava/lang/String; = "de-DE"

.field public static final LANGUAGE_EN_GB:Ljava/lang/String; = "en-GB"

.field public static final LANGUAGE_EN_US:Ljava/lang/String; = "en-US"

.field public static final LANGUAGE_ES_ES:Ljava/lang/String; = "es-ES"

.field public static final LANGUAGE_ES_US:Ljava/lang/String; = "es-US"

.field public static final LANGUAGE_FR_FR:Ljava/lang/String; = "fr-FR"

.field public static final LANGUAGE_IT_IT:Ljava/lang/String; = "it-IT"

.field public static final LANGUAGE_JA_JP:Ljava/lang/String; = "ja-JP"

.field public static final LANGUAGE_KO_KR:Ljava/lang/String; = "ko-KR"

.field public static final LANGUAGE_PT_BR:Ljava/lang/String; = "pt-BR"

.field public static final LANGUAGE_RU_RU:Ljava/lang/String; = "ru-RU"

.field public static final LANGUAGE_V_ES_LA:Ljava/lang/String; = "v-es-LA"

.field public static final LANGUAGE_V_ES_NA:Ljava/lang/String; = "v-es-NA"

.field public static final LANGUAGE_ZH_CN:Ljava/lang/String; = "zh-CN"

.field public static final LOCAL_TOS_VERSION:I = 0x1

.field public static final LOGIN_FACEBOOK:Ljava/lang/String; = "facebook_account"

.field public static final LOGIN_FACEBOOK_ACCOUNT_NAME:Ljava/lang/String; = "facebook_account_name"

.field public static final LOGIN_FACEBOOK_EXPIRES:Ljava/lang/String; = "facebook_expires"

.field public static final LOGIN_FACEBOOK_PICTURE:Ljava/lang/String; = "facebook_picture"

.field public static final LOGIN_FACEBOOK_PICTURE_URL:Ljava/lang/String; = "facebook_picture_url"

.field public static final LOGIN_FACEBOOK_TOKEN:Ljava/lang/String; = "facebook_token"

.field public static final LOGIN_FOURSQUARE:Ljava/lang/String; = "foursquare_account"

.field public static final LOGIN_FOURSQUARE_ACCOUNT_NAME:Ljava/lang/String; = "foursquare_account_name"

.field public static final LOGIN_FOURSQUARE_DEMO:Ljava/lang/String; = "foursquare_demo"

.field public static final LOGIN_FOURSQUARE_PASSWORD:Ljava/lang/String; = "foursquare_password"

.field public static final LOGIN_FOURSQUARE_PICTURE:Ljava/lang/String; = "foursquare_picture"

.field public static final LOGIN_FOURSQUARE_PICTURE_URL:Ljava/lang/String; = "foursquare_picture_url"

.field public static final LOGIN_FOURSQUARE_USERNAME:Ljava/lang/String; = "foursquare_username"

.field public static final LOGIN_TWITTER:Ljava/lang/String; = "twitter_account"

.field public static final LOGIN_TWITTER_ACCOUNT_NAME:Ljava/lang/String; = "twitter_account_name"

.field public static final LOGIN_TWITTER_DEMO:Ljava/lang/String; = "twitter_demo"

.field public static final LOGIN_TWITTER_PICTURE:Ljava/lang/String; = "twitter_picture"

.field public static final LOGIN_TWITTER_PICTURE_URL:Ljava/lang/String; = "twitter_picture_url"

.field public static final LOGIN_TWITTER_PROMPTED_FOLLOW_VLINGO:Ljava/lang/String; = "twitter_prompted_follow_vlingo"

.field public static final LOGIN_TWITTER_USERNAME:Ljava/lang/String; = "twitter_username"

.field public static final LOGIN_WEIBO:Ljava/lang/String; = "weibo_account"

.field public static final LOGIN_WEIBO_ACCOUNT_NAME:Ljava/lang/String; = "weibo_account_name"

.field public static final LOGIN_WEIBO_EXPIRES_IN:Ljava/lang/String; = "weibo_expires_in"

.field public static final LOGIN_WEIBO_PICTURE:Ljava/lang/String; = "weibo_picture"

.field public static final LOGIN_WEIBO_PICTURE_URL:Ljava/lang/String; = "weibo_picture_url"

.field public static final LOGIN_WEIBO_TOKEN:Ljava/lang/String; = "weibo_token"

.field public static final LOGIN_WEIBO_USER_UID:Ljava/lang/String; = "weibo_user_uid"

.field public static final LOGOUT_SOCIAL_NETWORK:Ljava/lang/String; = "logout_social_network"

.field public static final MAX_AUDIO_TIME_DEFAULT:I = 0x9c40

.field private static final MAX_NOTHING_RECOGNIZED_REPROMPTS_DEFAULT:Ljava/lang/String; = "2"

.field public static final MAX_NOTHING_RECOGNIZED_REPROMPTS_DEFAULT_INT:I = 0x2

.field public static final MAX_NOTHING_RECOGNIZED_REPROMPTS_FOREVER:Ljava/lang/String; = "-1"

.field public static final MAX_NOTHING_RECOGNIZED_REPROMPTS_FOREVER_INT:I = -0x1

.field public static final MESSAGE_READBACK_WORD_COUNT:Ljava/lang/String; = "msg_readback_word_count"

.field public static final MIMIC_MODE_DEFAULT:Ljava/lang/String; = "-1"

.field public static final NETWORK_TTS_TIMEOUT_DEFAULT:I = 0x1388

.field public static final NEW_CONTACT_MATCH_ALGO_DEFAULT:Z = true

.field public static final PLAYBACK_INPUT_FILE:Ljava/lang/String; = "playback_input_file"

.field public static final RECORD_OUTPUT_FILE:Ljava/lang/String; = "record_output_file"

.field public static final REGULAR_WIDGET_DISPLAY_MAX:I = 0x5

.field public static final SERVER_RESONSE_LOGGGING_NONE:Ljava/lang/String; = "None"

.field public static final SHAKE_TO_SKIP:Ljava/lang/String; = "shake_to_skip"

.field public static final SHARED_CRITICAL_SETTINGS_KEY:Ljava/lang/String; = "shared_critical_settings"

.field public static final SOCIAL_LOGIN_ATTEMPT_DEF_VALUE:Z = false

.field public static final SOCIAL_NETWORK_UPDATE_MASK:Ljava/lang/String; = "social_network_update_mask"

.field private static final TAG:Ljava/lang/String;

.field public static final TOS_ACCEPTANCE_STATE_REMINDER_NEEDED:Ljava/lang/String; = "reminder_needed"

.field public static final TOS_ACCEPTANCE_STATE_REMINDER_NOT_NEEDED:Ljava/lang/String; = "reminder_not_needed"

.field public static final TOS_ACCEPTANCE_STATE_REMINDER_SHOWN:Ljava/lang/String; = "reminder_done"

.field public static final TTS_VOICE_FEMALE:Ljava/lang/String; = "Female"

.field public static final TTS_VOICE_MALE:Ljava/lang/String; = "Male"

.field public static final TWITTER_CONSUMER_KEY:Ljava/lang/String; = "twitter_consumer_key"

.field public static final TWITTER_CONSUMER_KEY_DEFAULT:Ljava/lang/String; = "AGv8Ps3AlFKrf2C1YoFkQ"

.field public static final TWITTER_CONSUMER_SECRET:Ljava/lang/String; = "twitter_consumer_secret"

.field public static final TWITTER_CONSUMER_SECRET_DEFAULT:Ljava/lang/String; = "qeX5TCXa9HPDlpNmhPACOT7sUerHPmD91Oq9nYuw6Q"

.field public static final TWITTER_DIALOG_FLAGS:Ljava/lang/String; = "twitter_dialog_flags"

.field public static final TWITTER_REQUEST_SECRET:Ljava/lang/String; = "twitter_request_secret"

.field public static final TWITTER_REQUEST_TOKEN:Ljava/lang/String; = "twitter_request_token"

.field public static final TWITTER_USER_SECRET:Ljava/lang/String; = "twitter_user_secret"

.field public static final TWITTER_USER_TOKEN:Ljava/lang/String; = "twitter_user_token"

.field public static final USE_HIDDEN_CALENDARS_DEFAULT:Z = true

.field public static final USE_MEDIASYNC_APPROACH_DEFAULT:Z = false

.field public static final USE_PHONE_DISAMBIG:Ljava/lang/String; = "USE_PHONE_DISAMBIG"

.field public static final USE_VOICE_PROMPT:Ljava/lang/String; = "use_voice_prompt"

.field public static final USE_VOICE_PROMPT_CONFIRM_WITH_USER:Ljava/lang/String; = "use_voice_prompt_confirm_with_user"

.field public static final USE_VOICE_PROMPT_CONFIRM_WITH_USER_DEFAULT:Z = true

.field public static final USE_VOICE_PROMPT_DEFAULT:Z = true

.field public static final WEATHER_USE_VP_LOCATION_ALL:Ljava/lang/String; = "All"

.field public static final WEB_SEARCH_ENGINE_NAME_BAIDU:Ljava/lang/String; = "Baidu"

.field public static final WEB_SEARCH_ENGINE_NAME_BING:Ljava/lang/String; = "Bing"

.field public static final WEB_SEARCH_ENGINE_NAME_DAUM:Ljava/lang/String; = "Daum"

.field public static final WEB_SEARCH_ENGINE_NAME_GOOGLE:Ljava/lang/String; = "Google"

.field public static final WEB_SEARCH_ENGINE_NAME_NAVER:Ljava/lang/String; = "Naver"

.field public static final WEB_SEARCH_ENGINE_NAME_YAHOO:Ljava/lang/String; = "Yahoo"

.field public static final WEIBO_APP_ID:Ljava/lang/String; = "weibo_app_id"

.field public static final WEIBO_APP_ID_DEFAULT:Ljava/lang/String; = "3328388872"

.field public static final WEIBO_REDIRECT_URL:Ljava/lang/String; = "weibo_redirect_url"

.field public static final WEIBO_REDIRECT_URL_DEFAULT:Ljava/lang/String; = "http://www.nuance.com/"

.field private static albumNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static artistNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static contactNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static currentLanguageApplication:Ljava/lang/String;

.field private static hsBoolean:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static hsFloat:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static hsInt:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static hsLong:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static hsString:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static langIsoToEnable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static languageDescriptions:[Ljava/lang/String;

.field private static languageEnables:[Ljava/lang/String;

.field private static languages:[Ljava/lang/String;

.field private static maxNumLanguages:I

.field private static playlistNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static refCount:I

.field private static sEditor:Landroid/content/SharedPreferences$Editor;

.field private static songNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static startHashSettingEdit:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x0

    .line 72
    const-class v0, Lcom/vlingo/core/internal/settings/Settings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->TAG:Ljava/lang/String;

    .line 328
    sput v4, Lcom/vlingo/core/internal/settings/Settings;->maxNumLanguages:I

    .line 339
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "en-US_enable"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string/jumbo v2, "en-GB_enable"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "de-DE_enable"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "es-ES_enable"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "fr-FR_enable"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "it-IT_enable"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "ko-KR_enable"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "zh-CN_enable"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "ja-JP_enable"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "v-es-LA_enable"

    aput-object v2, v0, v1

    const-string/jumbo v1, "v-es-NA_enable"

    aput-object v1, v0, v4

    const/16 v1, 0xb

    const-string/jumbo v2, "ru-RU_enable"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "pt-BR_enable"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->languageEnables:[Ljava/lang/String;

    .line 355
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    .line 489
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_FIGS:Ljava/util/Set;

    .line 491
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_FIGS:Ljava/util/Set;

    const-string/jumbo v1, "fr-FR"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 492
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_FIGS:Ljava/util/Set;

    const-string/jumbo v1, "it-IT"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 493
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_FIGS:Ljava/util/Set;

    const-string/jumbo v1, "de-DE"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 494
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_FIGS:Ljava/util/Set;

    const-string/jumbo v1, "es-ES"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 497
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_ENGLISH:Ljava/util/Set;

    .line 499
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_ENGLISH:Ljava/util/Set;

    const-string/jumbo v1, "en-US"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 500
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_ENGLISH:Ljava/util/Set;

    const-string/jumbo v1, "en-GB"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 503
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_EFIGS:Ljava/util/Set;

    .line 505
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_EFIGS:Ljava/util/Set;

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_ENGLISH:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 506
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_EFIGS:Ljava/util/Set;

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_FIGS:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 509
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CEFIGS:Ljava/util/Set;

    .line 511
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CEFIGS:Ljava/util/Set;

    const-string/jumbo v1, "zh-CN"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 512
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CEFIGS:Ljava/util/Set;

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_EFIGS:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 515
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CJ:Ljava/util/Set;

    .line 517
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CJ:Ljava/util/Set;

    const-string/jumbo v1, "zh-CN"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 518
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CJ:Ljava/util/Set;

    const-string/jumbo v1, "ja-JP"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 521
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CJK:Ljava/util/Set;

    .line 523
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CJK:Ljava/util/Set;

    const-string/jumbo v1, "zh-CN"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 524
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CJK:Ljava/util/Set;

    const-string/jumbo v1, "ko-KR"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 525
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CJK:Ljava/util/Set;

    const-string/jumbo v1, "ja-JP"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 528
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_US_ONLY:Ljava/util/Set;

    .line 530
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_US_ONLY:Ljava/util/Set;

    const-string/jumbo v1, "en-US"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 533
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CHINESE_ONLY:Ljava/util/Set;

    .line 535
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CHINESE_ONLY:Ljava/util/Set;

    const-string/jumbo v1, "zh-CN"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 538
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_ALL:Ljava/util/Set;

    .line 540
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_ALL:Ljava/util/Set;

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_EFIGS:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 541
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_ALL:Ljava/util/Set;

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_CJK:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 542
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_ALL:Ljava/util/Set;

    const-string/jumbo v1, "ru-RU"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 543
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_ALL:Ljava/util/Set;

    const-string/jumbo v1, "pt-BR"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 546
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_EFIGSJK:Ljava/util/Set;

    .line 548
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_EFIGSJK:Ljava/util/Set;

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_EFIGS:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 549
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_EFIGSJK:Ljava/util/Set;

    const-string/jumbo v1, "ko-KR"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 550
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->LANGUAGES_EFIGSJK:Ljava/util/Set;

    const-string/jumbo v1, "ja-JP"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 589
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->isAsrManager()Z

    move-result v0

    sput-boolean v0, Lcom/vlingo/core/internal/settings/Settings;->DEFAULT_ASR_MANAGER:Z

    .line 922
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->contactNameList:Ljava/util/ArrayList;

    .line 923
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->songNameList:Ljava/util/ArrayList;

    .line 924
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->albumNameList:Ljava/util/ArrayList;

    .line 925
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->artistNameList:Ljava/util/ArrayList;

    .line 926
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->playlistNameList:Ljava/util/ArrayList;

    .line 928
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->hsInt:Ljava/util/HashMap;

    .line 929
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->hsString:Ljava/util/HashMap;

    .line 930
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->hsFloat:Ljava/util/HashMap;

    .line 931
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->hsLong:Ljava/util/HashMap;

    .line 932
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->hsBoolean:Ljava/util/HashMap;

    .line 933
    sput-boolean v3, Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z

    .line 941
    sput v3, Lcom/vlingo/core/internal/settings/Settings;->refCount:I

    .line 942
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->sEditor:Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1315
    return-void
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 67
    sget-boolean v0, Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z

    return v0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 67
    sput-boolean p0, Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z

    return p0
.end method

.method static synthetic access$300()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->hsInt:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->hsString:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$500()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->hsLong:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->hsFloat:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$700()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->hsBoolean:Ljava/util/HashMap;

    return-object v0
.end method

.method private static clearDataValuesDatabase(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1762
    const/4 v1, 0x0

    .line 1764
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string/jumbo v3, "VlingoDataSettings"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v4, v5}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1765
    const-string/jumbo v0, "DROP TABLE IF EXISTS dataValues"

    .line 1766
    .local v0, "createDB":Ljava/lang/String;
    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1772
    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 1774
    .end local v0    # "createDB":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1768
    :catch_0
    move-exception v2

    .line 1769
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/vlingo/core/internal/settings/Settings;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1772
    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_0

    .end local v2    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    :goto_1
    throw v3

    .restart local v0    # "createDB":Ljava/lang/String;
    :catch_2
    move-exception v3

    goto :goto_0

    .end local v0    # "createDB":Ljava/lang/String;
    :catch_3
    move-exception v4

    goto :goto_1
.end method

.method public static declared-synchronized commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V
    .locals 2
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 1842
    const-class v1, Lcom/vlingo/core/internal/settings/Settings;

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/vlingo/core/internal/settings/Settings;->refCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/vlingo/core/internal/settings/Settings;->refCount:I

    .line 1845
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1846
    monitor-exit v1

    return-void

    .line 1842
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static commitHashEditSetting()V
    .locals 2

    .prologue
    .line 1583
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/core/internal/settings/Settings$1;

    invoke-direct {v1}, Lcom/vlingo/core/internal/settings/Settings$1;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1625
    return-void
.end method

.method private static convertToISOLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "vlingoLanguage"    # Ljava/lang/String;

    .prologue
    .line 1304
    const-string/jumbo v0, "v-es-LA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "v-es-NA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1305
    :cond_0
    const-string/jumbo p0, "es-US"

    .line 1307
    .end local p0    # "vlingoLanguage":Ljava/lang/String;
    :cond_1
    return-object p0
.end method

.method public static disableCarMode(Landroid/content/SharedPreferences$Editor;)V
    .locals 2
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 1036
    const-string/jumbo v0, "driving_mode_on"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1037
    return-void
.end method

.method public static enableCarMode(Landroid/content/SharedPreferences$Editor;)V
    .locals 2
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 1026
    const-string/jumbo v0, "driving_mode_on"

    const/4 v1, 0x1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1027
    return-void
.end method

.method public static getAlbumNameList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1923
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->albumNameList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getArtistNameList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1930
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->artistNameList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getBoolean(Ljava/lang/String;Z)Z
    .locals 6
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 1430
    move v2, p1

    .line 1431
    .local v2, "ret":Z
    sget-object v3, Lcom/vlingo/core/internal/settings/Settings;->hsBoolean:Ljava/util/HashMap;

    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 1432
    .local v1, "item":Ljava/lang/Boolean;
    if-eqz v1, :cond_0

    .line 1433
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 1445
    :goto_0
    return v3

    .line 1436
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1437
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3, p0, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    :goto_1
    move v3, v2

    .line 1445
    goto :goto_0

    .line 1439
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/settings/Settings;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getContext() : null; key = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1442
    :catch_0
    move-exception v0

    .line 1443
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/vlingo/core/internal/settings/Settings;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ClassCastException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "; key = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v2

    .line 1445
    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    move v3, v2

    goto :goto_0
.end method

.method public static getContactNameList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1908
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->contactNameList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1944
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public static getCurrentLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 1312
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public static getData(Ljava/lang/String;)[B
    .locals 11
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 1730
    const/4 v0, 0x0

    .line 1731
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 1733
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getDataValuesDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1734
    const-string/jumbo v1, "dataValues"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "setting_data"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setting_key LIKE \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1735
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1736
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 1737
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1742
    if-eqz v8, :cond_0

    .line 1743
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1745
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 1746
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1749
    :cond_1
    :goto_1
    return-object v1

    .line 1742
    :cond_2
    if-eqz v8, :cond_3

    .line 1743
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1745
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    .line 1746
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_4
    :goto_3
    move-object v1, v10

    .line 1749
    goto :goto_1

    .line 1739
    :catch_0
    move-exception v9

    .line 1740
    .local v9, "ex":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1742
    if-eqz v8, :cond_5

    .line 1743
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 1745
    :cond_5
    :goto_4
    if-eqz v0, :cond_4

    .line 1746
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_3

    .line 1742
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_6

    .line 1743
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 1745
    :cond_6
    :goto_5
    if-eqz v0, :cond_7

    .line 1746
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_7
    throw v1

    .line 1743
    :catch_1
    move-exception v2

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    .restart local v9    # "ex":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    goto :goto_4

    .end local v9    # "ex":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    goto :goto_5
.end method

.method private static getDataValuesDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 6

    .prologue
    .line 1753
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1754
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v3, "VlingoDataSettings"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1756
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string/jumbo v1, "CREATE TABLE IF NOT EXISTS dataValues (setting_key VARCHAR(255), setting_data BLOB)"

    .line 1757
    .local v1, "createDB":Ljava/lang/String;
    invoke-virtual {v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1758
    return-object v2
.end method

.method public static getEnum(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 1450
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFloat(Ljava/lang/String;F)F
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultvalue"    # F

    .prologue
    .line 1454
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->hsFloat:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 1455
    .local v0, "item":Ljava/lang/Float;
    if-eqz v0, :cond_0

    .line 1456
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 1458
    :goto_0
    return v1

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    goto :goto_0
.end method

.method public static getISOLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1290
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v0

    .line 1291
    .local v0, "vlingoLanguage":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getISOLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "fullLocaleString"    # Ljava/lang/String;

    .prologue
    .line 1300
    invoke-static {p0}, Lcom/vlingo/core/internal/settings/Settings;->convertToISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 1645
    const/4 v1, 0x0

    .line 1646
    .local v1, "image":Landroid/graphics/Bitmap;
    invoke-static {p0}, Lcom/vlingo/core/internal/settings/Settings;->getData(Ljava/lang/String;)[B

    move-result-object v0

    .line 1647
    .local v0, "bytes":[B
    if-eqz v0, :cond_0

    .line 1648
    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1650
    :cond_0
    return-object v1
.end method

.method public static getInt(Ljava/lang/String;I)I
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 1422
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->hsInt:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1423
    .local v0, "item":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 1424
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1426
    :goto_0
    return v1

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method public static getLanguageApplication()Ljava/lang/String;
    .locals 13

    .prologue
    .line 1347
    const-string/jumbo v11, "language"

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1348
    .local v5, "language":Ljava/lang/String;
    if-nez v5, :cond_2

    .line 1349
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    .line 1350
    .local v2, "defaultLocale":Ljava/util/Locale;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguages()[Ljava/lang/CharSequence;

    move-result-object v10

    .line 1351
    .local v10, "supportedLanguages":[Ljava/lang/CharSequence;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->values()[Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v4, v3

    .end local v0    # "arr$":[Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;
    .end local v3    # "i$":I
    .end local v6    # "len$":I
    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v8, v0, v4

    .line 1352
    .local v8, "mappingItem":Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;
    # getter for: Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->locale:Ljava/util/Locale;
    invoke-static {v8}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->access$000(Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;)Ljava/util/Locale;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1353
    move-object v1, v10

    .local v1, "arr$":[Ljava/lang/CharSequence;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v3, 0x0

    .end local v4    # "i$":I
    .restart local v3    # "i$":I
    :goto_1
    if-ge v3, v7, :cond_1

    aget-object v9, v1, v3

    .line 1354
    .local v9, "supportedLanguage":Ljava/lang/CharSequence;
    # getter for: Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->language:Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->access$100(Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1355
    # getter for: Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->language:Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;->access$100(Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;)Ljava/lang/String;

    move-result-object v5

    .line 1353
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1351
    .end local v1    # "arr$":[Ljava/lang/CharSequence;
    .end local v3    # "i$":I
    .end local v7    # "len$":I
    .end local v9    # "supportedLanguage":Ljava/lang/CharSequence;
    :cond_1
    add-int/lit8 v3, v4, 0x1

    .restart local v3    # "i$":I
    move v4, v3

    .end local v3    # "i$":I
    .restart local v4    # "i$":I
    goto :goto_0

    .line 1361
    .end local v2    # "defaultLocale":Ljava/util/Locale;
    .end local v4    # "i$":I
    .end local v8    # "mappingItem":Lcom/vlingo/core/internal/settings/Settings$LanguageLocaleMapping;
    .end local v10    # "supportedLanguages":[Ljava/lang/CharSequence;
    :cond_2
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 1364
    .end local v5    # "language":Ljava/lang/String;
    :goto_2
    return-object v5

    .restart local v5    # "language":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v11

    invoke-virtual {v11}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/vlingo/core/internal/settings/LanguageDefaulter;->getDefaultLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method public static getLocaleForIsoLanguage()Ljava/util/Locale;
    .locals 2

    .prologue
    .line 1412
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v0

    .line 1413
    .local v0, "isoLanguage":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    return-object v1
.end method

.method public static getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;
    .locals 4
    .param p0, "isoLanguage"    # Ljava/lang/String;

    .prologue
    .line 1402
    const-string/jumbo v2, "-"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1403
    .local v1, "parts":[Ljava/lang/String;
    new-instance v0, Ljava/util/Locale;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-direct {v0, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1404
    .local v0, "locale":Ljava/util/Locale;
    return-object v0
.end method

.method public static getLong(Ljava/lang/String;J)J
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # J

    .prologue
    .line 1470
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->hsLong:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1471
    .local v0, "item":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 1472
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 1474
    :goto_0
    return-wide v1

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    goto :goto_0
.end method

.method public static getMultiWidgetItemsInitialMax()I
    .locals 3

    .prologue
    .line 1878
    const/4 v0, -0x1

    .line 1879
    .local v0, "toReturn":I
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1880
    const-string/jumbo v1, "widget_display_max"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1890
    :goto_0
    return v0

    .line 1885
    :cond_0
    const-string/jumbo v1, "widget_display_max"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static getMultiWidgetItemsUltimateMax()I
    .locals 5

    .prologue
    .line 1894
    const/4 v0, -0x1

    .line 1895
    .local v0, "toReturn":I
    const-string/jumbo v3, "multi.widget.item.ultimate.max"

    const-string/jumbo v4, "20"

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1898
    .local v1, "ultimateMaxString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1899
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1900
    .local v2, "x":I
    if-lez v2, :cond_0

    .line 1901
    move v0, v2

    .line 1904
    .end local v2    # "x":I
    :cond_0
    return v0
.end method

.method public static getPlaylistNameList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1937
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->playlistNameList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 1817
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1818
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    return-object v1
.end method

.method public static getSongNameList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1916
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->songNameList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getSpeakerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1870
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getSpeakerID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 1462
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->hsString:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1463
    .local v0, "item":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1464
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1466
    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1478
    .local p1, "defaultValue":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static getSupportedLanguageDescriptions()[Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 980
    sget v4, Lcom/vlingo/core/internal/settings/Settings;->maxNumLanguages:I

    .line 981
    .local v4, "maxCount":I
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languageEnables:[Ljava/lang/String;

    array-length v6, v6

    if-le v4, v6, :cond_0

    .line 982
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languageEnables:[Ljava/lang/String;

    array-length v4, v6

    .line 984
    :cond_0
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languages:[Ljava/lang/String;

    array-length v6, v6

    if-le v4, v6, :cond_1

    .line 985
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languages:[Ljava/lang/String;

    array-length v4, v6

    .line 987
    :cond_1
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languageDescriptions:[Ljava/lang/String;

    array-length v6, v6

    if-le v4, v6, :cond_2

    .line 988
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languageDescriptions:[Ljava/lang/String;

    array-length v4, v6

    .line 990
    :cond_2
    const-string/jumbo v6, "show_all_languages"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 991
    .local v5, "showBydefault":Z
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 992
    .local v2, "langDescList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    const/4 v1, 0x0

    .local v1, "langCounter":I
    :goto_0
    if-ge v1, v4, :cond_4

    .line 993
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    sget-object v7, Lcom/vlingo/core/internal/settings/Settings;->languages:[Ljava/lang/String;

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 994
    .local v3, "langIndex":I
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languageEnables:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-static {v6, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 995
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languageDescriptions:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 992
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 998
    .end local v3    # "langIndex":I
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    new-array v0, v6, [Ljava/lang/CharSequence;

    .line 999
    .local v0, "langArray":[Ljava/lang/CharSequence;
    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1000
    return-object v0
.end method

.method public static getSupportedLanguages()[Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 952
    sget v4, Lcom/vlingo/core/internal/settings/Settings;->maxNumLanguages:I

    .line 953
    .local v4, "maxCount":I
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languageEnables:[Ljava/lang/String;

    array-length v6, v6

    if-le v4, v6, :cond_0

    .line 954
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languageEnables:[Ljava/lang/String;

    array-length v4, v6

    .line 956
    :cond_0
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languages:[Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 957
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languages:[Ljava/lang/String;

    array-length v6, v6

    if-le v4, v6, :cond_1

    .line 958
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languages:[Ljava/lang/String;

    array-length v4, v6

    .line 961
    :cond_1
    const-string/jumbo v6, "show_all_languages"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 962
    .local v5, "showBydefault":Z
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 963
    .local v3, "langList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    const/4 v1, 0x0

    .local v1, "langCounter":I
    :goto_0
    if-ge v1, v4, :cond_3

    .line 964
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    sget-object v7, Lcom/vlingo/core/internal/settings/Settings;->languages:[Ljava/lang/String;

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 965
    .local v2, "langIndex":I
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languageEnables:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-static {v6, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 966
    sget-object v6, Lcom/vlingo/core/internal/settings/Settings;->languages:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 963
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 974
    .end local v2    # "langIndex":I
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    new-array v0, v6, [Ljava/lang/CharSequence;

    .line 975
    .local v0, "langArray":[Ljava/lang/CharSequence;
    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 976
    return-object v0
.end method

.method public static getUUIDDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1866
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getDeviceID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasSetting(Ljava/lang/String;)Z
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 1417
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1418
    .local v0, "value":Ljava/lang/Object;
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 17
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1048
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->initISO()V

    .line 1051
    const-string/jumbo v15, "appstate.first_run.calypso"

    const/16 v16, 0x1

    invoke-static/range {v15 .. v16}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 1052
    .local v6, "firstRun":Z
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 1060
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    if-nez v6, :cond_0

    sget-object v15, Lcom/vlingo/core/internal/settings/Settings;->languageEnables:[Ljava/lang/String;

    const/16 v16, 0x0

    aget-object v15, v15, v16

    invoke-static {v15}, Lcom/vlingo/core/internal/settings/Settings;->hasSetting(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 1062
    :cond_0
    invoke-static {v5}, Lcom/vlingo/core/internal/settings/Settings;->initAllLanguages(Landroid/content/SharedPreferences$Editor;)V

    .line 1067
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v15

    sget-object v16, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_names:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-interface/range {v15 .. v16}, Lcom/vlingo/core/internal/ResourceIdProvider;->getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;

    move-result-object v2

    .line 1068
    .local v2, "availableLanguageNames":[Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v15

    sget-object v16, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_iso:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-interface/range {v15 .. v16}, Lcom/vlingo/core/internal/ResourceIdProvider;->getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;

    move-result-object v3

    .line 1071
    .local v3, "availableLanguages":[Ljava/lang/String;
    const/4 v15, 0x0

    sput v15, Lcom/vlingo/core/internal/settings/Settings;->maxNumLanguages:I

    .line 1072
    const/4 v4, 0x0

    .line 1073
    .local v4, "availableLanguagesCount":I
    if-eqz v2, :cond_2

    .line 1074
    array-length v15, v2

    sput v15, Lcom/vlingo/core/internal/settings/Settings;->maxNumLanguages:I

    .line 1075
    sget v15, Lcom/vlingo/core/internal/settings/Settings;->maxNumLanguages:I

    new-array v15, v15, [Ljava/lang/String;

    sput-object v15, Lcom/vlingo/core/internal/settings/Settings;->languageDescriptions:[Ljava/lang/String;

    .line 1076
    sget v15, Lcom/vlingo/core/internal/settings/Settings;->maxNumLanguages:I

    new-array v15, v15, [Ljava/lang/String;

    sput-object v15, Lcom/vlingo/core/internal/settings/Settings;->languages:[Ljava/lang/String;

    .line 1080
    :cond_2
    if-eqz v3, :cond_3

    .line 1081
    array-length v4, v3

    .line 1084
    :cond_3
    const/4 v9, 0x0

    .line 1085
    .local v9, "langCount":I
    const/4 v7, 0x0

    .local v7, "i":I
    move v10, v9

    .end local v9    # "langCount":I
    .local v10, "langCount":I
    :goto_0
    if-ge v7, v4, :cond_4

    .line 1086
    aget-object v11, v3, v7

    .line 1087
    .local v11, "langIso":Ljava/lang/String;
    sget-object v15, Lcom/vlingo/core/internal/settings/Settings;->languages:[Ljava/lang/String;

    add-int/lit8 v9, v10, 0x1

    .end local v10    # "langCount":I
    .restart local v9    # "langCount":I
    aput-object v11, v15, v10

    .line 1085
    add-int/lit8 v7, v7, 0x1

    move v10, v9

    .end local v9    # "langCount":I
    .restart local v10    # "langCount":I
    goto :goto_0

    .line 1091
    .end local v11    # "langIso":Ljava/lang/String;
    :cond_4
    const/4 v9, 0x0

    .line 1092
    .end local v10    # "langCount":I
    .restart local v9    # "langCount":I
    if-eqz v2, :cond_6

    .line 1093
    move-object v1, v2

    .local v1, "arr$":[Ljava/lang/String;
    array-length v14, v1

    .local v14, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    move v10, v9

    .end local v9    # "langCount":I
    .restart local v10    # "langCount":I
    :goto_1
    if-ge v8, v14, :cond_5

    aget-object v12, v1, v8

    .line 1094
    .local v12, "langName":Ljava/lang/String;
    sget-object v15, Lcom/vlingo/core/internal/settings/Settings;->languageDescriptions:[Ljava/lang/String;

    add-int/lit8 v9, v10, 0x1

    .end local v10    # "langCount":I
    .restart local v9    # "langCount":I
    aput-object v12, v15, v10

    .line 1093
    add-int/lit8 v8, v8, 0x1

    move v10, v9

    .end local v9    # "langCount":I
    .restart local v10    # "langCount":I
    goto :goto_1

    .end local v12    # "langName":Ljava/lang/String;
    :cond_5
    move v9, v10

    .line 1100
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v8    # "i$":I
    .end local v10    # "langCount":I
    .end local v14    # "len$":I
    .restart local v9    # "langCount":I
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v13

    .line 1101
    .local v13, "language":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v13, v0, v5}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;Landroid/content/SharedPreferences$Editor;)V

    .line 1102
    if-eqz v6, :cond_7

    .line 1103
    invoke-static {v5}, Lcom/vlingo/core/internal/settings/Settings;->initSettingValues(Landroid/content/SharedPreferences$Editor;)V

    .line 1105
    :cond_7
    const-string/jumbo v15, "nothing_recognized_reprompt.count"

    const-string/jumbo v16, "0"

    move-object/from16 v0, v16

    invoke-interface {v5, v15, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1106
    const-string/jumbo v15, "test_resolve_contact"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v5, v15, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1107
    invoke-static {v5}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 1108
    return-void
.end method

.method private static initAllLanguages(Landroid/content/SharedPreferences$Editor;)V
    .locals 2
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    const/4 v1, 0x1

    .line 1185
    const-string/jumbo v0, "en-US_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1186
    const-string/jumbo v0, "en-GB_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1187
    const-string/jumbo v0, "de-DE_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1188
    const-string/jumbo v0, "es-ES_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1189
    const-string/jumbo v0, "v-es-LA_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1190
    const-string/jumbo v0, "v-es-NA_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1191
    const-string/jumbo v0, "fr-FR_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1192
    const-string/jumbo v0, "it-IT_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1193
    const-string/jumbo v0, "ko-KR_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1194
    const-string/jumbo v0, "zh-CN_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1195
    const-string/jumbo v0, "ru-RU_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1196
    const-string/jumbo v0, "pt-BR_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1197
    const-string/jumbo v0, "ja-JP_enable"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1198
    return-void
.end method

.method private static initISO()V
    .locals 3

    .prologue
    .line 1202
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "en-US"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1203
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "en-GB"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1204
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "de-DE"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1205
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "es-ES"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1206
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "fr-FR"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "it-IT"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1208
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "ko-KR"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1209
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "zh-CN"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "ja-JP"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1211
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-LA"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-NA"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1213
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "ru-RU"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1214
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->langIsoToEnable:Ljava/util/HashMap;

    const-string/jumbo v1, "pt-BR"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1215
    return-void
.end method

.method private static initSettingValues(Landroid/content/SharedPreferences$Editor;)V
    .locals 13
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    const/16 v12, 0x1388

    const/4 v11, -0x1

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1111
    const-string/jumbo v6, "appstate.first_run.calypso"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1112
    const-string/jumbo v6, "hello_request_complete"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1113
    const-string/jumbo v6, "config.fetch.disable"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1114
    const-string/jumbo v6, "location_enabled"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1115
    const-string/jumbo v6, "car_safereader_enable_alert"

    invoke-interface {p0, v6, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1116
    const-string/jumbo v6, "endpoint.time_withoutspeech"

    invoke-interface {p0, v6, v12}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1117
    const-string/jumbo v6, "endpoint.time.speech.short"

    const/16 v7, 0x190

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1118
    const-string/jumbo v6, "endpoint.time.speech.medium"

    const/16 v7, 0x2ee

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1119
    const-string/jumbo v6, "endpoint.time.speech.long"

    const/16 v7, 0x6d6

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1120
    const-string/jumbo v6, "nothing_recognized_reprompt.max_value"

    const-string/jumbo v7, "2"

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1122
    const-string/jumbo v6, "multi.widget.client.capped"

    invoke-interface {p0, v6, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1123
    const-string/jumbo v6, "multi.widget.client.showmorebutton"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1124
    const-string/jumbo v6, "multi.widget.client.showcounts"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1125
    const-string/jumbo v6, "multi.widget.client.collapse"

    invoke-interface {p0, v6, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1126
    const-string/jumbo v6, "multi.widget.item.initial.max"

    const-string/jumbo v7, "6"

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1127
    const-string/jumbo v6, "multi.widget.item.ultimate.max"

    const-string/jumbo v7, "20"

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1128
    const-string/jumbo v6, "FIELD_ID"

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getDefaultFieldId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1130
    const-string/jumbo v6, "phrasespot_beam"

    const/high16 v7, 0x41a00000    # 20.0f

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1131
    const-string/jumbo v6, "phrasespot_absbeam"

    const/high16 v7, 0x42200000    # 40.0f

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1132
    const-string/jumbo v6, "phrasespot_aoffset"

    invoke-interface {p0, v6, v10}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1133
    const-string/jumbo v6, "phrasespot_delay"

    const/high16 v7, 0x42c80000    # 100.0f

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1134
    const-string/jumbo v6, "phrasespot_parama"

    invoke-interface {p0, v6, v10}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1135
    const-string/jumbo v6, "phrasespot_paramb"

    const/high16 v7, 0x43a00000    # 320.0f

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1136
    const-string/jumbo v6, "phrasespot_paramc"

    const/high16 v7, 0x43fa0000    # 500.0f

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1137
    const-string/jumbo v6, "phrasespot_waveformlogging_enabled"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1139
    const-string/jumbo v6, "speex.quality"

    const/16 v7, 0x8

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1140
    const-string/jumbo v6, "speex.variable_bitrate"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1141
    const-string/jumbo v6, "speex.voice_activity_detection"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1142
    const-string/jumbo v6, "speex.complexity"

    const/4 v7, 0x3

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1143
    const-string/jumbo v6, "tos_launched_for_tos_other_app"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1152
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1153
    .local v0, "appContext":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    .line 1154
    .local v3, "resourceIdProvider":Lcom/vlingo/core/internal/ResourceIdProvider;
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_NAME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v2

    .line 1155
    .local v2, "defaultWebsearchEngineResourceId":I
    const-string/jumbo v1, "Google"

    .line 1156
    .local v1, "defaultWebsearchEngine":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eq v2, v11, :cond_0

    .line 1157
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1159
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v5

    .line 1160
    .local v5, "webSearchURLDefaultResourceId":I
    const-string/jumbo v4, "http://www.google.com/m?cx=partner-pub-5324388728707269:o6qccq-17aj&amp;q={query}"

    .line 1161
    .local v4, "webSearchURLDefault":Ljava/lang/String;
    if-eqz v5, :cond_1

    if-eq v5, v11, :cond_1

    .line 1162
    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1165
    :cond_1
    const-string/jumbo v6, "web_search_engine"

    invoke-interface {p0, v6, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1166
    const-string/jumbo v6, "web_search_url"

    invoke-interface {p0, v6, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1168
    const-string/jumbo v6, "use_default_phone"

    invoke-interface {p0, v6, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1169
    const-string/jumbo v6, "car_safereader_off_when_silent"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1170
    const-string/jumbo v6, "safereader_start_on_boot"

    invoke-interface {p0, v6, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1172
    const-string/jumbo v6, "tts_local_required_engine"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1173
    const-string/jumbo v6, "tts_local_ignore_use_speech_rate"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1174
    const-string/jumbo v6, "tts_local_force_speech_rate"

    const/high16 v7, -0x40800000    # -1.0f

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1175
    const-string/jumbo v6, "tts_local_tts_fallback_engine"

    const-string/jumbo v7, "com.google.android.tts"

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1177
    const-string/jumbo v6, "car_iux_tts_cacheing_required"

    invoke-interface {p0, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1178
    const-string/jumbo v6, "network_tts_timeout"

    invoke-interface {p0, v6, v12}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1180
    const-string/jumbo v6, "weather_version_two_languages"

    const-string/jumbo v7, "ko-KR"

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1181
    const-string/jumbo v6, "weather_use_vp_location"

    const-string/jumbo v7, "all"

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1182
    return-void
.end method

.method public static isAsrEditingEnabled()Z
    .locals 2

    .prologue
    .line 1368
    const-string/jumbo v0, "asr.editing.enabled.languages"

    const-string/jumbo v1, "All"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "asr.editing.enabled.languages"

    const-string/jumbo v1, "All"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "All"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCarModeEnabled()Z
    .locals 2

    .prologue
    .line 1016
    const-string/jumbo v0, "driving_mode_on"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isLocationEnabled()Z
    .locals 2

    .prologue
    .line 1228
    const-string/jumbo v0, "location_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isNotificationsAccepted(Ljava/lang/String;)Z
    .locals 4
    .param p0, "notificationVersion"    # Ljava/lang/String;

    .prologue
    .line 1948
    const-string/jumbo v2, "accepted_notifications"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    .line 1949
    .local v1, "acceptedNotifications":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 1952
    .local v0, "accepted":Z
    :goto_0
    return v0

    .line 1949
    .end local v0    # "accepted":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isObeyDeviceLocationSettings()Z
    .locals 2

    .prologue
    .line 1232
    const-string/jumbo v0, "obey_device_location_settings"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isSafereaderAlertEnabled()Z
    .locals 3

    .prologue
    .line 1218
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v0

    .line 1219
    .local v0, "enabled":Z
    const-string/jumbo v1, "car_safereader_enable_alert"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    and-int/2addr v0, v1

    .line 1220
    return v0
.end method

.method public static isTtsCachingOn()Z
    .locals 2

    .prologue
    .line 1004
    const-string/jumbo v0, "car_iux_tts_cacheing_required"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static notifySafeReaderEmailAccountsChanged()V
    .locals 3

    .prologue
    .line 948
    const-string/jumbo v0, "safereader_email_accounts_changed"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    return-void
.end method

.method public static declared-synchronized releaseSharedEditor(Landroid/content/SharedPreferences$Editor;)V
    .locals 2
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 1855
    const-class v1, Lcom/vlingo/core/internal/settings/Settings;

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/vlingo/core/internal/settings/Settings;->refCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/vlingo/core/internal/settings/Settings;->refCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1858
    monitor-exit v1

    return-void

    .line 1855
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static setAlbumNameList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1926
    .local p0, "albumNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sput-object p0, Lcom/vlingo/core/internal/settings/Settings;->albumNameList:Ljava/util/ArrayList;

    .line 1927
    return-void
.end method

.method public static setAllNotificationsAccepted(Z)V
    .locals 1
    .param p0, "accepted"    # Z

    .prologue
    .line 1968
    const-string/jumbo v0, "all_notifications_accepted"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1969
    return-void
.end method

.method public static setArtistNameList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1933
    .local p0, "artistNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sput-object p0, Lcom/vlingo/core/internal/settings/Settings;->artistNameList:Ljava/util/ArrayList;

    .line 1934
    return-void
.end method

.method public static setBoolean(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 1813
    invoke-interface {p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1814
    return-void
.end method

.method public static setBoolean(Ljava/lang/String;Z)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Z

    .prologue
    .line 1571
    sget-boolean v1, Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z

    if-eqz v1, :cond_0

    .line 1572
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->hsBoolean:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1580
    :goto_0
    return-void

    .line 1574
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1577
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1578
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public static setContactNameList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1912
    .local p0, "contactNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sput-object p0, Lcom/vlingo/core/internal/settings/Settings;->contactNameList:Ljava/util/ArrayList;

    .line 1913
    return-void
.end method

.method public static setData(Ljava/lang/String;[B)V
    .locals 11
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "data"    # [B

    .prologue
    .line 1683
    if-eqz p1, :cond_6

    array-length v1, p1

    if-lez v1, :cond_6

    .line 1684
    const/4 v0, 0x0

    .line 1685
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 1687
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1688
    .local v9, "cv":Landroid/content/ContentValues;
    const-string/jumbo v1, "setting_data"

    invoke-virtual {v9, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1689
    const-string/jumbo v1, "setting_key"

    invoke-virtual {v9, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getDataValuesDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1691
    const-string/jumbo v1, "dataValues"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "setting_data"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setting_key LIKE \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1692
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    .line 1693
    const-string/jumbo v1, "dataValues"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v9}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1701
    :goto_0
    if-eqz v8, :cond_0

    .line 1702
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 1704
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 1705
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    .line 1721
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v9    # "cv":Landroid/content/ContentValues;
    :cond_1
    :goto_2
    return-void

    .line 1696
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v9    # "cv":Landroid/content/ContentValues;
    :cond_2
    :try_start_3
    const-string/jumbo v1, "dataValues"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setting_key LIKE \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1698
    .end local v9    # "cv":Landroid/content/ContentValues;
    :catch_0
    move-exception v10

    .line 1699
    .local v10, "ex":Landroid/database/SQLException;
    :try_start_4
    invoke-virtual {v10}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1701
    if-eqz v8, :cond_3

    .line 1702
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    .line 1704
    :cond_3
    :goto_3
    if-eqz v0, :cond_1

    .line 1705
    :try_start_6
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_2

    .line 1701
    .end local v10    # "ex":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_4

    .line 1702
    :try_start_7
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8

    .line 1704
    :cond_4
    :goto_4
    if-eqz v0, :cond_5

    .line 1705
    :try_start_8
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_9

    :cond_5
    :goto_5
    throw v1

    .line 1709
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_6
    const/4 v0, 0x0

    .line 1711
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_9
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getDataValuesDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1712
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "DELETE FROM dataValues WHERE setting_key LIKE \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/database/SQLException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1716
    if-eqz v0, :cond_1

    .line 1717
    :try_start_a
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_2

    .line 1713
    :catch_3
    move-exception v10

    .line 1714
    .restart local v10    # "ex":Landroid/database/SQLException;
    :try_start_b
    invoke-virtual {v10}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1716
    if-eqz v0, :cond_1

    .line 1717
    :try_start_c
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_2

    .line 1716
    .end local v10    # "ex":Landroid/database/SQLException;
    :catchall_1
    move-exception v1

    if-eqz v0, :cond_7

    .line 1717
    :try_start_d
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a

    :cond_7
    :goto_6
    throw v1

    .line 1702
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v9    # "cv":Landroid/content/ContentValues;
    :catch_5
    move-exception v1

    goto/16 :goto_1

    .line 1705
    :catch_6
    move-exception v1

    goto/16 :goto_2

    .line 1702
    .end local v9    # "cv":Landroid/content/ContentValues;
    .restart local v10    # "ex":Landroid/database/SQLException;
    :catch_7
    move-exception v1

    goto :goto_3

    .end local v10    # "ex":Landroid/database/SQLException;
    :catch_8
    move-exception v2

    goto :goto_4

    .line 1705
    :catch_9
    move-exception v2

    goto :goto_5

    .line 1717
    .end local v8    # "c":Landroid/database/Cursor;
    :catch_a
    move-exception v2

    goto :goto_6
.end method

.method public static setFloat(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;F)V
    .locals 0
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # F

    .prologue
    .line 1797
    invoke-interface {p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1798
    return-void
.end method

.method public static setFloat(Ljava/lang/String;F)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # F

    .prologue
    .line 1535
    sget-boolean v1, Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z

    if-eqz v1, :cond_0

    .line 1536
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->hsFloat:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1544
    :goto_0
    return-void

    .line 1538
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1541
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1542
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public static setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1658
    if-eqz p1, :cond_0

    .line 1659
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1661
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p1, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1662
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1667
    :try_start_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1671
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-static {p0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setData(Ljava/lang/String;[B)V

    .line 1676
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    :goto_1
    return-void

    .line 1663
    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v1

    .line 1664
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1667
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 1668
    :catch_1
    move-exception v2

    goto :goto_0

    .line 1666
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 1667
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1669
    :goto_2
    throw v2

    .line 1674
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    :cond_0
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setData(Ljava/lang/String;[B)V

    goto :goto_1

    .line 1668
    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_2
    move-exception v2

    goto :goto_0

    :catch_3
    move-exception v3

    goto :goto_2
.end method

.method public static setInt(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;I)V
    .locals 0
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 1789
    invoke-interface {p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1790
    return-void
.end method

.method public static setInt(Ljava/lang/String;I)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # I

    .prologue
    .line 1497
    sget-boolean v1, Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z

    if-eqz v1, :cond_0

    .line 1498
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->hsInt:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1506
    :goto_0
    return-void

    .line 1500
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1503
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1504
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public static setLanguageApplication(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p0, "language"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1280
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1281
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-static {p0, p1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;Landroid/content/SharedPreferences$Editor;)V

    .line 1282
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 1283
    return-void
.end method

.method public static setLanguageApplication(Ljava/lang/String;Landroid/content/Context;Landroid/content/SharedPreferences$Editor;)V
    .locals 3
    .param p0, "language"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 1252
    if-eqz p0, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->currentLanguageApplication:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1270
    :cond_0
    :goto_0
    return-void

    .line 1255
    :cond_1
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;->notifyLanguageChanged(Ljava/lang/String;Landroid/content/Context;)V

    .line 1256
    sput-object p0, Lcom/vlingo/core/internal/settings/Settings;->currentLanguageApplication:Ljava/lang/String;

    .line 1257
    const-string/jumbo v1, "language"

    invoke-interface {p2, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1259
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1260
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->currentLanguageApplication:Ljava/lang/String;

    const-string/jumbo v2, "zh-CN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1261
    const-string/jumbo v0, "Baidu"

    .line 1267
    .local v0, "newWebSearchEngineValue":Ljava/lang/String;
    :goto_1
    const-string/jumbo v1, "web_search_engine"

    invoke-interface {p2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1269
    .end local v0    # "newWebSearchEngineValue":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/settings/Settings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    goto :goto_0

    .line 1262
    :cond_3
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->currentLanguageApplication:Ljava/lang/String;

    const-string/jumbo v2, "ko-KR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1263
    const-string/jumbo v0, "Naver"

    .restart local v0    # "newWebSearchEngineValue":Ljava/lang/String;
    goto :goto_1

    .line 1265
    .end local v0    # "newWebSearchEngineValue":Ljava/lang/String;
    :cond_4
    const-string/jumbo v0, "Google"

    .restart local v0    # "newWebSearchEngineValue":Ljava/lang/String;
    goto :goto_1
.end method

.method public static setLocationEnabled(Z)V
    .locals 1
    .param p0, "settingValue"    # Z

    .prologue
    .line 1236
    const-string/jumbo v0, "location_enabled"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1237
    return-void
.end method

.method public static setLong(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;J)V
    .locals 0
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 1781
    invoke-interface {p0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1782
    return-void
.end method

.method public static setLong(Ljava/lang/String;J)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # J

    .prologue
    .line 1516
    sget-boolean v1, Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z

    if-eqz v1, :cond_0

    .line 1517
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->hsLong:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1525
    :goto_0
    return-void

    .line 1519
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1522
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1523
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public static setPlaylistNameList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1940
    .local p0, "playlistNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sput-object p0, Lcom/vlingo/core/internal/settings/Settings;->playlistNameList:Ljava/util/ArrayList;

    .line 1941
    return-void
.end method

.method public static setSafereaderAlertEnabled(Z)V
    .locals 1
    .param p0, "b"    # Z

    .prologue
    .line 1224
    const-string/jumbo v0, "car_safereader_enable_alert"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1225
    return-void
.end method

.method public static setSongNameList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1919
    .local p0, "songNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sput-object p0, Lcom/vlingo/core/internal/settings/Settings;->songNameList:Ljava/util/ArrayList;

    .line 1920
    return-void
.end method

.method public static setString(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1805
    invoke-interface {p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1806
    return-void
.end method

.method public static setString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1554
    sget-boolean v1, Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z

    if-eqz v1, :cond_0

    .line 1555
    sget-object v1, Lcom/vlingo/core/internal/settings/Settings;->hsString:Ljava/util/HashMap;

    invoke-virtual {v1, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1561
    :goto_0
    return-void

    .line 1557
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1558
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1559
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public static setStringSet(Ljava/lang/String;Ljava/util/Set;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1635
    .local p1, "value":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1636
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 1637
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 1638
    return-void
.end method

.method public static setTOSAccepted(Z)V
    .locals 4
    .param p0, "b"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1956
    const-string/jumbo v0, "tos_accepted"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1957
    const-string/jumbo v1, "tos_accepted"

    if-eqz p0, :cond_1

    new-array v0, v3, [B

    aput-byte v3, v0, v2

    :goto_0
    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setData(Ljava/lang/String;[B)V

    .line 1958
    if-eqz p0, :cond_0

    .line 1959
    const-string/jumbo v0, "tos_accepted_date"

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1963
    :cond_0
    return-void

    .line 1957
    :cond_1
    new-array v0, v3, [B

    aput-byte v2, v0, v2

    goto :goto_0
.end method

.method public static setTtsCachingOff()V
    .locals 2

    .prologue
    .line 1008
    const-string/jumbo v0, "car_iux_tts_cacheing_required"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1009
    return-void
.end method

.method public static setTtsCachingOn()V
    .locals 2

    .prologue
    .line 1012
    const-string/jumbo v0, "car_iux_tts_cacheing_required"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1013
    return-void
.end method

.method public static declared-synchronized startBatchEdit()Landroid/content/SharedPreferences$Editor;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 1828
    const-class v1, Lcom/vlingo/core/internal/settings/Settings;

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/vlingo/core/internal/settings/Settings;->refCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/vlingo/core/internal/settings/Settings;->refCount:I

    .line 1831
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->sEditor:Landroid/content/SharedPreferences$Editor;

    if-nez v0, :cond_0

    .line 1832
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/settings/Settings;->sEditor:Landroid/content/SharedPreferences$Editor;

    .line 1834
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/settings/Settings;->sEditor:Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1828
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static startHashSettingEdit()V
    .locals 1

    .prologue
    .line 1972
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/core/internal/settings/Settings;->startHashSettingEdit:Z

    .line 1973
    return-void
.end method

.method protected static updateCurrentLocale(Landroid/content/res/Resources;)V
    .locals 5
    .param p0, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 1378
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isLanguageChangeAllowed()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1380
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v2

    .line 1381
    .local v2, "language":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v1, v4, Landroid/content/res/Configuration;->fontScale:F

    .line 1384
    .local v1, "fontScale":F
    invoke-static {v2}, Lcom/vlingo/core/internal/settings/Settings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    .line 1385
    .local v3, "locale":Ljava/util/Locale;
    invoke-static {v3}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 1386
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    .line 1387
    .local v0, "config":Landroid/content/res/Configuration;
    iput-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1388
    iput v1, v0, Landroid/content/res/Configuration;->fontScale:F

    .line 1389
    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 1391
    .end local v0    # "config":Landroid/content/res/Configuration;
    .end local v1    # "fontScale":F
    .end local v2    # "language":Ljava/lang/String;
    .end local v3    # "locale":Ljava/util/Locale;
    :cond_0
    return-void
.end method

.method public static useWeatherVpLocation()Z
    .locals 2

    .prologue
    .line 1373
    const-string/jumbo v0, "weather_use_vp_location"

    const-string/jumbo v1, "All"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "weather_use_vp_location"

    const-string/jumbo v1, "All"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "All"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public resetAllSettings(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1040
    invoke-static {p1}, Lcom/vlingo/core/internal/settings/Settings;->clearDataValuesDatabase(Landroid/content/Context;)V

    .line 1041
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1042
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 1043
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 1044
    invoke-static {p1}, Lcom/vlingo/core/internal/settings/Settings;->init(Landroid/content/Context;)V

    .line 1045
    return-void
.end method
