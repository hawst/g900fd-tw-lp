.class Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;
.super Ljava/lang/Object;
.source "LocalSearchHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->showSuccess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 135
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCurrentSpokenLocation()Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, "requestedLocation":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 137
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v4, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->access$100(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 141
    .local v1, "msg":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeWebSearchButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 143
    .local v0, "decos":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->access$400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowLocalSearchWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    invoke-interface {v3, v4, v0, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 144
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->actionSuccess()V

    .line 145
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->access$500(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 146
    return-void

    .line 139
    .end local v0    # "decos":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .end local v1    # "msg":Ljava/lang/String;
    :cond_0
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v4, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->access$200(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "msg":Ljava/lang/String;
    goto :goto_0
.end method
