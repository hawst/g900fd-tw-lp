.class public Lcom/vlingo/core/internal/audio/NullAudioRequest;
.super Lcom/vlingo/core/internal/audio/AudioRequest;
.source "NullAudioRequest.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioRequest;-><init>()V

    .line 10
    return-void
.end method

.method public static getRequest()Lcom/vlingo/core/internal/audio/NullAudioRequest;
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/vlingo/core/internal/audio/NullAudioRequest;

    invoke-direct {v0}, Lcom/vlingo/core/internal/audio/NullAudioRequest;-><init>()V

    return-object v0
.end method


# virtual methods
.method public prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/AudioPlayer;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "player"    # Lcom/vlingo/core/internal/audio/AudioPlayer;

    .prologue
    .line 18
    const/4 v0, 0x1

    return v0
.end method

.method public setDataSource(Landroid/content/Context;Landroid/media/MediaPlayer;Lcom/vlingo/core/internal/audio/AudioPlayer;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaPlayer"    # Landroid/media/MediaPlayer;
    .param p3, "player"    # Lcom/vlingo/core/internal/audio/AudioPlayer;

    .prologue
    .line 23
    return-void
.end method
