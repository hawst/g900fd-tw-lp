.class final Lcom/vlingo/core/internal/weather/WeatherResponseParser$11;
.super Ljava/lang/Object;
.source "WeatherResponseParser.java"

# interfaces
.implements Landroid/sax/EndElementListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/weather/WeatherResponseParser;->createForecastElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$wDaytimeForecast:Lcom/vlingo/core/internal/weather/WeatherElement;

.field final synthetic val$wForecast:Lcom/vlingo/core/internal/weather/WeatherElement;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherResponseParser$11;->val$wForecast:Lcom/vlingo/core/internal/weather/WeatherElement;

    iput-object p2, p0, Lcom/vlingo/core/internal/weather/WeatherResponseParser$11;->val$wDaytimeForecast:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public end()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherResponseParser$11;->val$wForecast:Lcom/vlingo/core/internal/weather/WeatherElement;

    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherResponseParser$11;->val$wDaytimeForecast:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->clone()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->addChild(Lcom/vlingo/core/internal/weather/WeatherElement;)Z

    .line 179
    return-void
.end method
