.class public final Lcom/vlingo/core/internal/debug/UttsProvider;
.super Ljava/lang/Object;
.source "UttsProvider.java"


# static fields
.field private static volatile instance:Lcom/vlingo/core/internal/debug/UttsProvider;


# instance fields
.field private fileDir:Ljava/io/File;

.field private final syncObject:Ljava/lang/Object;

.field private utts:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/debug/actions/Utt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->syncObject:Ljava/lang/Object;

    .line 23
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->utts:Ljava/util/Queue;

    .line 24
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/debug/UttsProvider;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/vlingo/core/internal/debug/UttsProvider;->instance:Lcom/vlingo/core/internal/debug/UttsProvider;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lcom/vlingo/core/internal/debug/UttsProvider;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/UttsProvider;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/debug/UttsProvider;->instance:Lcom/vlingo/core/internal/debug/UttsProvider;

    .line 29
    sget-object v0, Lcom/vlingo/core/internal/debug/UttsProvider;->instance:Lcom/vlingo/core/internal/debug/UttsProvider;

    .line 31
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/debug/UttsProvider;->instance:Lcom/vlingo/core/internal/debug/UttsProvider;

    goto :goto_0
.end method


# virtual methods
.method public addUtt(Lcom/vlingo/core/internal/debug/actions/Utt;)V
    .locals 2
    .param p1, "utt"    # Lcom/vlingo/core/internal/debug/actions/Utt;

    .prologue
    .line 35
    iget-object v1, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 36
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->utts:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 37
    monitor-exit v1

    .line 38
    return-void

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearUtts()V
    .locals 2

    .prologue
    .line 55
    iget-object v1, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->utts:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 57
    monitor-exit v1

    .line 58
    return-void

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getNextUtt()Lcom/vlingo/core/internal/debug/actions/Utt;
    .locals 2

    .prologue
    .line 41
    iget-object v1, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 42
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->utts:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->utts:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/debug/actions/Utt;

    monitor-exit v1

    .line 45
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public initAudioUtts(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "utterances":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->syncObject:Ljava/lang/Object;

    monitor-enter v3

    .line 88
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->utts:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 89
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 90
    .local v0, "audioUtt":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->utts:Ljava/util/Queue;

    new-instance v4, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;

    invoke-direct {v4, v0}, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    .end local v0    # "audioUtt":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    return-void
.end method

.method public initTextUtts(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "utterances":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->syncObject:Ljava/lang/Object;

    monitor-enter v3

    .line 79
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->utts:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    .line 80
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 81
    .local v1, "textUtt":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->utts:Ljava/util/Queue;

    new-instance v4, Lcom/vlingo/core/internal/debug/actions/TextUtt;

    invoke-direct {v4, v1}, Lcom/vlingo/core/internal/debug/actions/TextUtt;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 83
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "textUtt":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    return-void
.end method

.method public isUttsEmpty()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->utts:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public setFileDir(Ljava/lang/String;)Z
    .locals 2
    .param p1, "filesDirectory"    # Ljava/lang/String;

    .prologue
    .line 61
    if-eqz p1, :cond_0

    const-string/jumbo v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->fileDir:Ljava/io/File;

    .line 63
    iget-object v1, p0, Lcom/vlingo/core/internal/debug/UttsProvider;->fileDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    .line 68
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
