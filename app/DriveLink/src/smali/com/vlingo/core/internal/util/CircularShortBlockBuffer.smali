.class public Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;
.super Ljava/lang/Object;
.source "CircularShortBlockBuffer.java"


# instance fields
.field private mBlockSize:I

.field private mBlockWriteIndex:I

.field private mIsBufferFilled:Z

.field private mShortArray:[S


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "blockSize"    # I
    .param p2, "blockCount"    # I

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    mul-int v0, p1, p2

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mShortArray:[S

    .line 17
    iput p1, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockSize:I

    .line 18
    return-void
.end method


# virtual methods
.method public getArray()[S
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 34
    const/4 v1, 0x0

    .line 35
    .local v1, "arrayIndex":I
    iget-boolean v2, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mIsBufferFilled:Z

    if-eqz v2, :cond_0

    .line 36
    iget-object v2, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mShortArray:[S

    array-length v2, v2

    new-array v0, v2, [S

    .line 37
    .local v0, "array":[S
    iget-object v2, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mShortArray:[S

    iget v3, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockWriteIndex:I

    iget-object v4, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mShortArray:[S

    array-length v4, v4

    iget v5, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockWriteIndex:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 38
    iget-object v2, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mShortArray:[S

    array-length v2, v2

    iget v3, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockWriteIndex:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 43
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mShortArray:[S

    iget v3, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockWriteIndex:I

    invoke-static {v2, v6, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 44
    return-object v0

    .line 41
    .end local v0    # "array":[S
    :cond_0
    iget v2, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockWriteIndex:I

    new-array v0, v2, [S

    .restart local v0    # "array":[S
    goto :goto_0
.end method

.method public write([S)V
    .locals 4
    .param p1, "block"    # [S

    .prologue
    const/4 v3, 0x0

    .line 21
    array-length v0, p1

    iget v1, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockSize:I

    if-eq v0, v1, :cond_0

    .line 22
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Wrong size blocksize"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mShortArray:[S

    iget v1, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockWriteIndex:I

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 25
    iget v0, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockWriteIndex:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockWriteIndex:I

    .line 26
    iget v0, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockWriteIndex:I

    iget-object v1, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mShortArray:[S

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 27
    iput v3, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mBlockWriteIndex:I

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->mIsBufferFilled:Z

    .line 30
    :cond_1
    return-void
.end method
