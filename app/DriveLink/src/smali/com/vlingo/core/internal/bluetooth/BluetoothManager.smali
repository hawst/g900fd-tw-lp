.class public final Lcom/vlingo/core/internal/bluetooth/BluetoothManager;
.super Ljava/lang/Object;
.source "BluetoothManager.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;,
        Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;,
        Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;,
        Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;
    }
.end annotation


# static fields
.field private static final LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final SCO_RETRY_DELAY:I = 0x7d0

.field private static final SCO_RETRY_FLAG:Ljava/lang/String; = "sco_retry_flag"

.field private static final SETTING_KEY_BVRA_SUPPORT:Ljava/lang/String; = "supported_bvra"

.field public static final STREAM_BLUETOOTH_SCO:I = 0x6

.field private static final TAG:Ljava/lang/String;

.field private static btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

.field private static cancelTurnTiming:Z

.field private static ignoreBtConnectEvent:Z

.field private static instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

.field private static mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

.field private static mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private static mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

.field private static volatile mIsStartSCOinProgress:Z

.field private static mOnScoConnectedTask:Ljava/lang/Runnable;

.field private static mOnScoConnectedTaskTimeout:J

.field private static mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private static mRightBeforeForeground:Z

.field private static mScoDisconnectingByAudioFocusLoss:Z

.field private static mScoTimeoutTask:Ljava/util/TimerTask;

.field private static mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

.field private static mVolumeFallback:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 94
    const-class v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    .line 99
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    .line 103
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 104
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    .line 106
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    .line 108
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    .line 110
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    .line 112
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 114
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->BT_BUTTON:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .line 115
    sput-boolean v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    .line 116
    sput-boolean v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mRightBeforeForeground:Z

    .line 117
    sput-boolean v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent:Z

    .line 118
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelTurnTiming:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170
    return-void
.end method

.method static synthetic access$100()Landroid/bluetooth/BluetoothHeadset;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-object v0
.end method

.method static synthetic access$1002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    sput-boolean p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z

    return p0
.end method

.method static synthetic access$102(Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;
    .locals 0
    .param p0, "x0"    # Landroid/bluetooth/BluetoothHeadset;

    .prologue
    .line 90
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-object p0
.end method

.method static synthetic access$1102(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;)Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .prologue
    .line 90
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    return-object p0
.end method

.method static synthetic access$1200(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V

    return-void
.end method

.method static synthetic access$1300(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p0, "x0"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 90
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isIgnoreIntentForBDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1402(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    sput-boolean p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    return p0
.end method

.method static synthetic access$200()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$202(Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .locals 0
    .param p0, "x0"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 90
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object p0
.end method

.method static synthetic access$300()Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    return-object v0
.end method

.method static synthetic access$302(Ljava/util/TimerTask;)Ljava/util/TimerTask;
    .locals 0
    .param p0, "x0"    # Ljava/util/TimerTask;

    .prologue
    .line 90
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    return-object p0
.end method

.method static synthetic access$400(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V

    return-void
.end method

.method static synthetic access$500(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO(Z)V

    return-void
.end method

.method static synthetic access$600()V
    .locals 0

    .prologue
    .line 90
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO()V

    return-void
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900()V
    .locals 0

    .prologue
    .line 90
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V

    return-void
.end method

.method public static addListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;

    .prologue
    .line 1369
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1370
    return-void
.end method

.method public static appCloseType()Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;
    .locals 1

    .prologue
    .line 298
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    return-object v0
.end method

.method public static appCloseType(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;)V
    .locals 0
    .param p0, "type"    # Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .prologue
    .line 294
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .line 295
    return-void
.end method

.method public static declared-synchronized bluetoothManagerDestroy()V
    .locals 3

    .prologue
    .line 242
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "==[SVoiceBT, LatencyCheck]BT bluetoothManagerDestroy=="

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->enableWideBandSpeech(Z)V

    .line 244
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v0, v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 249
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v0, v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 251
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 252
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    .line 253
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mRightBeforeForeground:Z

    .line 254
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    if-eqz v0, :cond_1

    .line 255
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->NORMAL:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .line 260
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->closeBtProxy()V

    .line 261
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 262
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    .line 264
    const/4 v0, -0x1

    sput v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mVolumeFallback:I

    .line 266
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 267
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    if-eqz v0, :cond_2

    .line 268
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    :cond_2
    monitor-exit v1

    return-void

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized bluetoothManagerInit()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    const-class v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v3

    const/4 v4, 0x0

    :try_start_0
    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z

    .line 131
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->BT_BUTTON:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    sput-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .line 132
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    .line 133
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent:Z

    .line 134
    const/4 v4, -0x1

    sput v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mVolumeFallback:I

    .line 135
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    if-nez v4, :cond_0

    .line 136
    new-instance v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    invoke-direct {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;-><init>()V

    sput-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    .line 137
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->start()V

    .line 138
    :goto_0
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v4, v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_0

    .line 142
    const-wide/16 v4, 0x0

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143
    :catch_0
    move-exception v4

    goto :goto_0

    .line 147
    :cond_0
    :try_start_2
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    if-nez v4, :cond_2

    .line 148
    new-instance v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;-><init>(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$1;)V

    sput-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    .line 149
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 150
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "com.vlingo.client.app.action.APPLICATION_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 151
    const-string/jumbo v2, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 152
    const-string/jumbo v2, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 153
    const-string/jumbo v2, "android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 154
    const-string/jumbo v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 155
    const-string/jumbo v2, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156
    const-string/jumbo v2, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 157
    const-string/jumbo v2, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 158
    const-string/jumbo v2, "com.vlingo.client.app.action.AUDIO_FOCUS_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    invoke-virtual {v2, v4, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 162
    new-instance v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$1;

    invoke-direct {v2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$1;-><init>()V

    sput-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 214
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v6, 0x1

    invoke-virtual {v2, v4, v5, v6}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 222
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    if-nez v2, :cond_1

    .line 223
    new-instance v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    invoke-direct {v2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;-><init>()V

    sput-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    .line 225
    :cond_1
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 232
    :goto_1
    monitor-exit v3

    return v1

    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_2
    move v1, v2

    goto :goto_1

    .line 130
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    :catchall_0
    move-exception v1

    monitor-exit v3

    throw v1
.end method

.method private static cancelScoConnectedTask()V
    .locals 2

    .prologue
    .line 861
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;

    .line 862
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTaskTimeout:J

    .line 863
    return-void
.end method

.method private static cancelScoTimeoutTask()V
    .locals 1

    .prologue
    .line 852
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 853
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 854
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    .line 856
    :cond_0
    return-void
.end method

.method public static declared-synchronized closeBtProxy()V
    .locals 4

    .prologue
    .line 281
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "====[SVoiceBT, LatencyCheck] closeBtProxy==="

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v0, :cond_0

    .line 283
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V

    .line 287
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const/4 v2, 0x1

    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 288
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    :cond_0
    monitor-exit v1

    return-void

    .line 281
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static considerRightBeforeForeground(Z)V
    .locals 0
    .param p0, "flag"    # Z

    .prologue
    .line 1419
    sput-boolean p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mRightBeforeForeground:Z

    .line 1420
    return-void
.end method

.method public static disconnectScoByIdle()Z
    .locals 1

    .prologue
    .line 1407
    sget-boolean v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    return v0
.end method

.method public static enableWideBandSpeech(Z)V
    .locals 5
    .param p0, "bEnable"    # Z

    .prologue
    .line 688
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldSetWideBandSpeechToUse16khzVoice()Z

    move-result v2

    if-nez v2, :cond_1

    .line 739
    :cond_0
    :goto_0
    return-void

    .line 696
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isBMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 700
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v2, :cond_0

    .line 701
    if-eqz p0, :cond_2

    .line 705
    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string/jumbo v4, "enableWBS"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 706
    .local v1, "m":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 707
    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 709
    .end local v1    # "m":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 711
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 723
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string/jumbo v4, "disableWBS"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 724
    .restart local v1    # "m":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 725
    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 727
    .end local v1    # "m":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v0

    .line 729
    .restart local v0    # "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 734
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v2

    goto :goto_0

    .line 732
    :catch_3
    move-exception v2

    goto :goto_0

    .line 730
    :catch_4
    move-exception v2

    goto :goto_0

    .line 716
    :catch_5
    move-exception v2

    goto :goto_0

    .line 714
    :catch_6
    move-exception v2

    goto :goto_0

    .line 712
    :catch_7
    move-exception v2

    goto :goto_0
.end method

.method public static getBTdevice()Landroid/bluetooth/BluetoothDevice;
    .locals 9

    .prologue
    .line 944
    const/4 v0, 0x0

    .line 945
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    const/4 v4, -0x1

    .line 946
    .local v4, "idx":I
    sget-object v6, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-nez v6, :cond_1

    .line 978
    :cond_0
    :goto_0
    return-object v0

    .line 950
    :cond_1
    sget-object v6, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    .line 951
    .local v1, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    .line 952
    .local v5, "nConnectedDeviceNum":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_3

    .line 953
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/bluetooth/BluetoothDevice;

    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v2

    .line 954
    .local v2, "deviceType":Ljava/lang/String;
    sget-object v6, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "[SVoiceBT] getBTdevice deviceList("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")\'s deviceType:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 955
    if-eqz v2, :cond_2

    .line 956
    const-string/jumbo v6, "WA"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 957
    move v4, v3

    .line 952
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 962
    .end local v2    # "deviceType":Ljava/lang/String;
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 963
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v6

    if-eqz v6, :cond_5

    const/4 v6, -0x1

    if-eq v4, v6, :cond_5

    .line 964
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 974
    .restart local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_4
    if-eqz v0, :cond_0

    .line 975
    sget-object v6, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "[SVoiceBT] getBTdevice device\'s deviceName:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 966
    :cond_5
    const/4 v3, 0x0

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_4

    .line 967
    if-eq v4, v3, :cond_6

    .line 968
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 966
    .restart local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private static getBluetoothHeadsetConnctedDevices()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 921
    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 924
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-object v1

    .line 922
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 923
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 924
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public static getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .locals 6
    .param p0, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 983
    sget-object v5, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->BDeviceUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v5}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v4

    .line 984
    .local v4, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v2, 0x0

    .line 985
    .local v2, "deviceUtil":Lcom/vlingo/core/internal/util/BDeviceUtil;
    const/4 v1, 0x0

    .line 986
    .local v1, "deviceType":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 988
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/vlingo/core/internal/util/BDeviceUtil;

    move-object v2, v0

    .line 989
    if-eqz v2, :cond_0

    .line 990
    sget-object v5, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-interface {v2, v5, p0}, Lcom/vlingo/core/internal/util/BDeviceUtil;->getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 999
    :cond_0
    :goto_0
    return-object v1

    .line 992
    :catch_0
    move-exception v3

    .line 993
    .local v3, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v3}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 994
    .end local v3    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v3

    .line 995
    .local v3, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method public static ignoreBtConnectEvent(Z)V
    .locals 0
    .param p0, "flag"    # Z

    .prologue
    .line 1411
    sput-boolean p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent:Z

    .line 1412
    return-void
.end method

.method public static isBVRASupportDevice()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 347
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 360
    .local v0, "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return v3

    .line 350
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 352
    .restart local v0    # "context":Landroid/content/Context;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "supported_bvra"

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 353
    .local v2, "v":I
    if-nez v2, :cond_0

    move v3, v4

    goto :goto_0

    .line 354
    .end local v2    # "v":I
    :catch_0
    move-exception v1

    .line 356
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move v3, v4

    .line 360
    goto :goto_0
.end method

.method private static isBluetoothAdapterOn()Z
    .locals 2

    .prologue
    .line 343
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBluetoothAudioOn()Z
    .locals 4

    .prologue
    .line 800
    const/4 v1, 0x0

    .line 802
    .local v1, "useHeadsetForRecord":Z
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 803
    const/4 v1, 0x0

    .line 824
    .local v0, "am":Landroid/media/AudioManager;
    :goto_0
    return v1

    .line 806
    .end local v0    # "am":Landroid/media/AudioManager;
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 807
    .restart local v0    # "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v1

    .line 824
    goto :goto_0
.end method

.method public static isBluetoothAudioSupported()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 827
    const-string/jumbo v3, "listen_over_bluetooth"

    invoke-static {v3, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 828
    .local v1, "useBluetoothForAudio":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBVRASupportDevice()Z

    move-result v0

    .line 829
    .local v0, "supportBVRA":Z
    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 832
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isHeadsetConnected()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 319
    const/4 v0, 0x1

    .line 321
    .local v0, "headsetConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v3

    .line 339
    :cond_0
    :goto_0
    return v0

    .line 328
    :cond_1
    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 331
    .local v2, "state":I
    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 332
    const/4 v0, 0x0

    goto :goto_0

    .line 334
    .end local v2    # "state":I
    :catch_0
    move-exception v1

    .line 335
    .local v1, "mex":Ljava/lang/NoSuchMethodError;
    const-string/jumbo v4, "bluetooth_headset_connected"

    invoke-static {v4, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private static isIgnoreIntentForBDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p0, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 373
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    if-eqz p0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 375
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v0

    .line 376
    .local v0, "bDevice":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 377
    const/4 v1, 0x1

    .line 380
    .end local v0    # "bDevice":Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isListenOverBTEnabled()Z
    .locals 2

    .prologue
    .line 368
    const-string/jumbo v0, "listen_over_bluetooth"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isOnlyBDeviceConnected()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 929
    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v3, :cond_0

    .line 930
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getBluetoothHeadsetConnctedDevices()Ljava/util/List;

    move-result-object v0

    .line 931
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 932
    .local v2, "nConnectedDeviceNum":I
    if-ne v2, v4, :cond_0

    .line 933
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    invoke-static {v3}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v1

    .line 934
    .local v1, "deviceType":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string/jumbo v3, "WA"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    .line 940
    .end local v1    # "deviceType":Ljava/lang/String;
    :goto_0
    return v3

    :cond_0
    move v3, v5

    goto :goto_0
.end method

.method public static notifyBluetoothServiceConnectedToListener()V
    .locals 3

    .prologue
    .line 1401
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;

    .line 1402
    .local v1, "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    invoke-interface {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;->onBluetoothServiceConnected()V

    goto :goto_0

    .line 1404
    .end local v1    # "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    :cond_0
    return-void
.end method

.method private static notifyHeadsetStateToListeners(Z)V
    .locals 3
    .param p0, "state"    # Z

    .prologue
    .line 1391
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;

    .line 1392
    .local v1, "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    if-eqz p0, :cond_0

    .line 1393
    invoke-interface {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;->onHeadsetConnected()V

    goto :goto_0

    .line 1395
    :cond_0
    invoke-interface {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;->onHeadsetDisconnected()V

    goto :goto_0

    .line 1398
    .end local v1    # "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    :cond_1
    return-void
.end method

.method private static notifyScoStateToListeners(Z)V
    .locals 3
    .param p0, "state"    # Z

    .prologue
    .line 1381
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;

    .line 1382
    .local v1, "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    if-eqz p0, :cond_0

    .line 1383
    invoke-interface {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;->onScoConnected()V

    goto :goto_0

    .line 1385
    :cond_0
    invoke-interface {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;->onScoDisconnected()V

    goto :goto_0

    .line 1388
    .end local v1    # "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    :cond_1
    return-void
.end method

.method public static declared-synchronized onAppStateChanged(Z)V
    .locals 4
    .param p0, "isShown"    # Z

    .prologue
    .line 412
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT onAppStateChanged(), isShown="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    if-eqz p0, :cond_1

    .line 418
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent(Z)V

    .line 419
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v2, 0xc

    if-ne v0, v2, :cond_0

    .line 420
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 425
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V

    .line 426
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 412
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized onHeadsetStateChanged(Z)V
    .locals 6
    .param p0, "isConnected"    # Z

    .prologue
    .line 435
    const-class v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "[SVoiceBT, LatencyCheck] BT onHeadsetStateChanged(): isConnected= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->updateHeadsetStateSetting(Z)V

    .line 437
    if-eqz p0, :cond_2

    .line 439
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->openBtProxy()V

    .line 442
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 455
    :cond_0
    const-string/jumbo v2, "launch_car_on_bt_connect"

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 456
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->startMainActivity()V

    .line 459
    :cond_1
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyHeadsetStateToListeners(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 492
    :goto_0
    monitor-exit v3

    return-void

    .line 464
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getBluetoothHeadsetConnctedDevices()Ljava/util/List;

    move-result-object v0

    .line 465
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 466
    .local v1, "nConnectedDeviceNum":I
    if-nez v1, :cond_3

    .line 467
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->closeBtProxy()V

    .line 469
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    if-nez v2, :cond_5

    .line 470
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 471
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v2

    sget-object v4, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v5, 0x8ca

    invoke-virtual {v2, v4, v5}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    .line 474
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 477
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V

    .line 490
    :cond_5
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyHeadsetStateToListeners(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 435
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v1    # "nConnectedDeviceNum":I
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized onListenOverBTSettingChanged(Z)V
    .locals 2
    .param p0, "isEnabled"    # Z

    .prologue
    .line 389
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    if-eqz p0, :cond_0

    .line 399
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    :goto_0
    monitor-exit v1

    return-void

    .line 403
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized onScoStateChanged(ZI)V
    .locals 8
    .param p0, "isConnected"    # Z
    .param p1, "priorState"    # I

    .prologue
    const/4 v7, 0x1

    .line 503
    const-class v5, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v5

    :try_start_0
    sget-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mRightBeforeForeground:Z

    if-nez v4, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v4

    if-nez v4, :cond_1

    .line 504
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    .line 507
    if-nez p0, :cond_0

    .line 508
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 612
    :cond_0
    :goto_0
    monitor-exit v5

    return-void

    .line 518
    :cond_1
    if-eqz p0, :cond_4

    .line 521
    :try_start_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 522
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 523
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v4

    sget-object v6, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->MEDIUM_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v7, 0x4e2

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    .line 529
    :cond_2
    :goto_1
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V

    .line 530
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCOConnectedTask()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 503
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    .line 525
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v4

    sget-object v6, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v7, 0xa8c

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    goto :goto_1

    .line 534
    :cond_4
    const/16 v4, 0xb

    if-ne v4, p1, :cond_a

    .line 537
    const/4 v1, 0x0

    .line 538
    .local v1, "retry":Z
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v4, :cond_8

    .line 544
    :try_start_3
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I
    :try_end_3
    .catch Ljava/lang/NoSuchMethodError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    .line 550
    .local v2, "state":I
    :goto_2
    const/4 v4, 0x2

    if-eq v2, v4, :cond_5

    if-ne v2, v7, :cond_6

    .line 551
    :cond_5
    const/4 v1, 0x1

    .line 557
    .end local v2    # "state":I
    :cond_6
    :goto_3
    if-eqz v1, :cond_9

    .line 559
    :try_start_4
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    if-eqz v4, :cond_7

    .line 560
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V

    .line 562
    :cond_7
    new-instance v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {v4, v6, v7}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;-><init>(ZZ)V

    sput-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    .line 564
    const/4 v3, 0x0

    .line 565
    .local v3, "timer":Ljava/util/Timer;
    invoke-static {}, Lcom/vlingo/core/internal/util/TimerSingleton;->getTimer()Ljava/util/Timer;

    move-result-object v3

    .line 566
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    const-wide/16 v6, 0x7d0

    invoke-virtual {v3, v4, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 574
    .end local v3    # "timer":Ljava/util/Timer;
    :goto_4
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    goto :goto_0

    .line 545
    :catch_0
    move-exception v0

    .line 546
    .local v0, "mex":Ljava/lang/NoSuchMethodError;
    const/4 v2, 0x1

    .restart local v2    # "state":I
    goto :goto_2

    .line 555
    .end local v0    # "mex":Ljava/lang/NoSuchMethodError;
    .end local v2    # "state":I
    :cond_8
    const/4 v1, 0x1

    goto :goto_3

    .line 571
    :cond_9
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V

    .line 572
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V

    goto :goto_4

    .line 580
    .end local v1    # "retry":Z
    :cond_a
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v4

    if-nez v4, :cond_b

    .line 581
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 582
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v4

    sget-object v6, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v7, 0x8ca

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    .line 587
    :cond_b
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->enableWideBandSpeech(Z)V

    .line 588
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V

    .line 592
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 593
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    sget-object v6, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->BT_BUTTON:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    if-ne v4, v6, :cond_c

    sget-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    if-nez v4, :cond_c

    .line 610
    :cond_c
    :goto_5
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    goto/16 :goto_0

    .line 606
    :cond_d
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v4

    if-eqz v4, :cond_c

    goto :goto_5
.end method

.method public static declared-synchronized openBtProxy()V
    .locals 5

    .prologue
    .line 273
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "====[SVoiceBT, LatencyCheck] openBtProxy==="

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-nez v0, :cond_0

    .line 275
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerInit()Z

    .line 276
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    :cond_0
    monitor-exit v1

    return-void

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;

    .prologue
    .line 1374
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1375
    return-void
.end method

.method public static resumeListenOverBTSCO()V
    .locals 1

    .prologue
    .line 884
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAdapterOn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 885
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 886
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 903
    :cond_0
    return-void
.end method

.method public static declared-synchronized runTaskOnBluetoothAudioOn(Ljava/lang/Runnable;J)V
    .locals 4
    .param p0, "task"    # Ljava/lang/Runnable;
    .param p1, "maxWait"    # J

    .prologue
    .line 845
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;

    .line 846
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p1

    sput-wide v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTaskTimeout:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 847
    monitor-exit v1

    return-void

    .line 845
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static setCancelTurnTiming(Z)V
    .locals 0
    .param p0, "flag"    # Z

    .prologue
    .line 1415
    sput-boolean p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelTurnTiming:Z

    .line 1416
    return-void
.end method

.method private static declared-synchronized startSCO()V
    .locals 2

    .prologue
    .line 661
    const-class v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 662
    monitor-exit v0

    return-void

    .line 661
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static declared-synchronized startSCO(Z)V
    .locals 5
    .param p0, "retry"    # Z

    .prologue
    .line 618
    const-class v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT startSCO()"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBVRASupportDevice()Z

    move-result v2

    if-nez v2, :cond_1

    .line 621
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT Ignoring startSCO() (device doesn\'t support BVRA)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 658
    :cond_0
    :goto_0
    monitor-exit v3

    return-void

    .line 625
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 626
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT Ignoring startSCO() (ListenOverBluetooth setting is OFF)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 618
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 630
    :cond_2
    :try_start_2
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAdapterOn()Z

    move-result v2

    if-nez v2, :cond_3

    .line 631
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT Ignoring startSCO() (Bluetooth Adapter is OFF)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 641
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 642
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT Ignoring startSCO() (ONLY B-Device is connected on H-Device Mode)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 646
    :cond_4
    sget-boolean v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z

    if-nez v2, :cond_5

    sget-boolean v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent:Z

    if-nez v2, :cond_5

    .line 647
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 648
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v2, "sco_retry_flag"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 649
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v2, v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 650
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v2, v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 651
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 652
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v2, v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 656
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_5
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT Ignoring startSCO() as ANOTHER in progress"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static startSCOConnectedTask()V
    .locals 3

    .prologue
    .line 784
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V

    .line 785
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 788
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;

    const-wide/16 v1, 0xc8

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 790
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoConnectedTask()V

    .line 791
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V

    .line 792
    return-void
.end method

.method public static declared-synchronized startScoOnStartRecognition()V
    .locals 5

    .prologue
    .line 665
    const-class v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] startScoOnStartRecognition"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    const/4 v1, 0x0

    sput-boolean v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    .line 667
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent(Z)V

    .line 668
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO(Z)V

    .line 669
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent(Z)V

    .line 671
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelTurnTiming:Z

    if-eqz v1, :cond_0

    .line 672
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 673
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 675
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 676
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    const/4 v3, 0x6

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 679
    :cond_0
    monitor-exit v2

    return-void

    .line 665
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static declared-synchronized stopSCO(Z)V
    .locals 5
    .param p0, "ignoreListenOverBTSetting"    # Z

    .prologue
    .line 746
    const-class v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT stopSCO()"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->enableWideBandSpeech(Z)V

    .line 748
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p0, :cond_0

    .line 749
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT Ignoring stopSCO() (ListenOverBluetooth setting is OFF)"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 778
    :goto_0
    monitor-exit v2

    return-void

    .line 753
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->remoteHasUiFocus()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v1

    if-nez v1, :cond_1

    .line 754
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT Ignoring stopSCO() remote has ui focus"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 746
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 758
    :cond_1
    :try_start_2
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBVRASupportDevice()Z

    move-result v1

    if-nez v1, :cond_2

    .line 759
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT Ignoring stopSCO() (device doesn\'t support BVRA)"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 763
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-boolean v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z

    if-nez v1, :cond_3

    .line 764
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT Ignoring stopSCO() bluetooth Audio off"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 768
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V

    .line 770
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_4

    .line 771
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothHeadset;->stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    .line 772
    .local v0, "result":Z
    const/4 v1, 0x0

    sput-boolean v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z

    .line 773
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT StartScoThread:run() stopVoiceRecognition() result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 776
    .end local v0    # "result":Z
    :cond_4
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT stopSCO() mBluetoothHeadset is NULL"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public static declared-synchronized stopScoOnIdle()V
    .locals 3

    .prologue
    .line 682
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[SVoiceBT, LatencyCheck] stopScoOnIdle"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    .line 684
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 685
    monitor-exit v1

    return-void

    .line 682
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static suspendListenOverBTSCO()V
    .locals 1

    .prologue
    .line 871
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAdapterOn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 872
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 877
    :cond_0
    return-void
.end method

.method public static declared-synchronized updateHeadsetStateSetting(Z)V
    .locals 2
    .param p0, "isConnected"    # Z

    .prologue
    .line 310
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    const-string/jumbo v0, "bluetooth_headset_connected"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    monitor-exit v1

    return-void

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 1360
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1363
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->suspendListenOverBTSCO()V

    .line 1365
    :cond_0
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 1338
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1341
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->suspendListenOverBTSCO()V

    .line 1343
    :cond_0
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 1349
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1352
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->suspendListenOverBTSCO()V

    .line 1354
    :cond_0
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 1325
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->resumeListenOverBTSCO()V

    .line 1327
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1328
    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/audio/AudioRequest;->setStream(I)V

    .line 1332
    :goto_0
    return-void

    .line 1330
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/audio/AudioRequest;->setStream(I)V

    goto :goto_0
.end method
