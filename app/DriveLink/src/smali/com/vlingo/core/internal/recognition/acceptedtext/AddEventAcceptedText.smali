.class public Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;
.super Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;
.source "AddEventAcceptedText.java"


# instance fields
.field private date:Ljava/lang/String;

.field private duration:Ljava/lang/String;

.field private endtime:Ljava/lang/String;

.field private invitee:Ljava/lang/String;

.field private inviteetype:Ljava/lang/String;

.field private loc:Ljava/lang/String;

.field private time:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "date"    # Ljava/lang/String;
    .param p3, "time"    # Ljava/lang/String;
    .param p4, "endtime"    # Ljava/lang/String;
    .param p5, "duration"    # Ljava/lang/String;
    .param p6, "loc"    # Ljava/lang/String;
    .param p7, "invitee"    # Ljava/lang/String;
    .param p8, "inviteetype"    # Ljava/lang/String;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;-><init>(Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->title:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->date:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->time:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->endtime:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->duration:Ljava/lang/String;

    .line 40
    iput-object p6, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->loc:Ljava/lang/String;

    .line 41
    iput-object p7, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->invitee:Ljava/lang/String;

    .line 42
    iput-object p8, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->inviteetype:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method protected getAcceptedTextXML()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 48
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "<AcceptedText pt=\"event:add\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 49
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->getBodyElementsXML(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 50
    const-string/jumbo v1, "</AcceptedText>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 51
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected getBodyElementsXML(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "buffer"    # Ljava/lang/StringBuffer;

    .prologue
    .line 59
    if-nez p1, :cond_1

    .line 60
    const/4 p1, 0x0

    .line 102
    .end local p1    # "buffer":Ljava/lang/StringBuffer;
    :cond_0
    :goto_0
    return-object p1

    .line 62
    .restart local p1    # "buffer":Ljava/lang/StringBuffer;
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->date:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 63
    const-string/jumbo v0, "<Tag u=\"date\">"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 64
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->date:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 65
    const-string/jumbo v0, "</Tag>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 67
    :cond_2
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->time:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 68
    const-string/jumbo v0, "<Tag u=\"time\">"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 69
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->time:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 70
    const-string/jumbo v0, "</Tag>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 72
    :cond_3
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->title:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 73
    const-string/jumbo v0, "<Tag u=\"title\">"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 74
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 75
    const-string/jumbo v0, "</Tag>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 77
    :cond_4
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->endtime:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 78
    const-string/jumbo v0, "<Tag u=\"endtime\">"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 79
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->endtime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 80
    const-string/jumbo v0, "</Tag>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 82
    :cond_5
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->loc:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 83
    const-string/jumbo v0, "<Tag u=\"loc\">"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 84
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->loc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    const-string/jumbo v0, "</Tag>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 87
    :cond_6
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->invitee:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 88
    const-string/jumbo v0, "<Tag u=\"invitee\">"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 89
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->invitee:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 90
    const-string/jumbo v0, "</Tag>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 92
    :cond_7
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->inviteetype:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 93
    const-string/jumbo v0, "<Tag u=\"inviteetype\">"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 94
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->inviteetype:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 95
    const-string/jumbo v0, "</Tag>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    :cond_8
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->duration:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 98
    const-string/jumbo v0, "<Tag u=\"duration\">"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 99
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->duration:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    const-string/jumbo v0, "</Tag>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "AddEventAcceptedText [title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->time:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", endtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->endtime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", loc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->loc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", invitee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->invitee:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", inviteetype="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;->inviteetype:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
