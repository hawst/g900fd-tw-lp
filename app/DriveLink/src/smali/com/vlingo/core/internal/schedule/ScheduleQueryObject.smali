.class public Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;
.super Lcom/vlingo/core/internal/schedule/ScheduleEventBase;
.source "ScheduleQueryObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$1;,
        Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;
    }
.end annotation


# instance fields
.field private attendee:Ljava/lang/String;

.field private attendeeMatches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private count:I

.field private countFromEnd:Z

.field private includeAllDay:Z

.field private type:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;-><init>()V

    .line 24
    sget-object v0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->BEGIN:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->type:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->includeAllDay:Z

    .line 47
    return-void
.end method

.method private matchTime(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Z
    .locals 5
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    const/4 v0, 0x0

    .line 212
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAllDay()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->includeAllDay:Z

    if-nez v1, :cond_1

    .line 248
    :cond_0
    :goto_0
    return v0

    .line 220
    :cond_1
    sget-object v1, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$1;->$SwitchMap$com$vlingo$core$internal$schedule$ScheduleQueryObject$Type:[I

    iget-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->type:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 248
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 222
    :pswitch_0
    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAllDay()Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 231
    :pswitch_1
    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAllDay()Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 243
    :pswitch_2
    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    goto :goto_0

    .line 220
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private normalize()V
    .locals 6

    .prologue
    .line 253
    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendee:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendeeMatches:Ljava/util/List;

    if-nez v1, :cond_0

    .line 254
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v1

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendee:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->getExactContactMatch(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    .line 255
    .local v0, "exactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v0, :cond_1

    .line 256
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendeeMatches:Ljava/util/List;

    .line 257
    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendeeMatches:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    .end local v0    # "exactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_0
    :goto_0
    return-void

    .line 259
    .restart local v0    # "exactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v1

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendee:Ljava/lang/String;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactLookupType;->EMAIL_ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendeeMatches:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public containsString(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Z
    .locals 11
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 134
    invoke-direct {p0}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->normalize()V

    .line 136
    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->title:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->title:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/util/StringUtils;->containsString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v5

    .line 143
    :cond_1
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->matchTime(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 147
    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->location:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->location:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/util/StringUtils;->containsString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 153
    :cond_2
    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendeeMatches:Ljava/util/List;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendeeMatches:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_5

    .line 156
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const/16 v8, 0x19

    invoke-static {v7, p1, v8}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getEventAttendees(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;I)Ljava/util/List;

    move-result-object v2

    .line 157
    .local v2, "eventAttendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 158
    .local v1, "eventAttendee":Ljava/lang/String;
    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendeeMatches:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 159
    .local v0, "cm":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v7, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-static {v7, v1}, Lcom/vlingo/core/internal/util/StringUtils;->containsString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    move v5, v6

    .line 160
    goto :goto_0

    .line 165
    .end local v0    # "cm":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v1    # "eventAttendee":Ljava/lang/String;
    .end local v2    # "eventAttendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendee:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 167
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEventID()J

    move-result-wide v8

    iget-object v10, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendee:Ljava/lang/String;

    invoke-static {v7, v8, v9, v10}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->isAttendeeContainInEvent(Landroid/content/Context;JLjava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v5, v6

    .line 169
    goto :goto_0

    :cond_6
    move v5, v6

    .line 174
    goto :goto_0
.end method

.method public containsWord(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Z
    .locals 6
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 178
    invoke-direct {p0}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->normalize()V

    .line 180
    iget-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->title:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->title:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/util/StringUtils;->containsWord(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 207
    :cond_0
    :goto_0
    return v0

    .line 187
    :cond_1
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->matchTime(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 191
    iget-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->location:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->location:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/util/StringUtils;->containsWord(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 198
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendee:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 200
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEventID()J

    move-result-wide v3

    iget-object v5, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendee:Ljava/lang/String;

    invoke-static {v2, v3, v4, v5}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->isAttendeeContainInEvent(Landroid/content/Context;JLjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 202
    goto :goto_0

    :cond_3
    move v0, v1

    .line 207
    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->count:I

    return v0
.end method

.method public matches(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Z
    .locals 11
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 90
    invoke-direct {p0}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->normalize()V

    .line 92
    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->title:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->title:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v5

    .line 99
    :cond_1
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->matchTime(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 103
    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->location:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->location:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 109
    :cond_2
    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendeeMatches:Ljava/util/List;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendeeMatches:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_5

    .line 112
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const/16 v8, 0x19

    invoke-static {v7, p1, v8}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getEventAttendees(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;I)Ljava/util/List;

    move-result-object v2

    .line 113
    .local v2, "eventAttendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 114
    .local v1, "eventAttendee":Ljava/lang/String;
    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendeeMatches:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 115
    .local v0, "cm":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v7, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    move v5, v6

    .line 116
    goto :goto_0

    .line 121
    .end local v0    # "cm":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v1    # "eventAttendee":Ljava/lang/String;
    .end local v2    # "eventAttendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-object v7, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendee:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 123
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEventID()J

    move-result-wide v8

    iget-object v10, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendee:Ljava/lang/String;

    invoke-static {v7, v8, v9, v10}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->isAttendeeContainInEvent(Landroid/content/Context;JLjava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v5, v6

    .line 125
    goto :goto_0

    :cond_6
    move v5, v6

    .line 130
    goto :goto_0
.end method

.method public setAttendee(Ljava/lang/String;)V
    .locals 0
    .param p1, "attendee"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->attendee:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 66
    if-gez p1, :cond_0

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->countFromEnd:Z

    .line 69
    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->count:I

    .line 70
    return-void
.end method

.method protected setEndTime()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/schedule/DateUtil;->endOfGivenDay(J)J

    move-result-wide v1

    invoke-direct {v0, p0, v1, v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    .line 87
    :cond_0
    return-void
.end method

.method public setIncludeAllDay(Z)V
    .locals 0
    .param p1, "iad"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->includeAllDay:Z

    .line 55
    return-void
.end method

.method public setType(Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;)V
    .locals 0
    .param p1, "type"    # Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->type:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    .line 63
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-static {p1}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;->of(Ljava/lang/String;)Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->type:Lcom/vlingo/core/internal/schedule/ScheduleQueryObject$Type;

    .line 59
    return-void
.end method

.method public shouldCountFromEnd()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->countFromEnd:Z

    return v0
.end method
