.class public final Lcom/vlingo/core/internal/audio/AMRUtil;
.super Ljava/lang/Object;
.source "AMRUtil.java"


# static fields
.field private static final AMR_HEADER:[B

.field private static final FRAME_HEADER:I = 0x4

.field private static final FRAME_SIZE:I = 0xd

.field private static final FRAME_TIME:I = 0x14


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x6

    new-array v0, v0, [B

    sput-object v0, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    .line 25
    sget-object v0, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x0

    const/16 v2, 0x23

    aput-byte v2, v0, v1

    .line 26
    sget-object v0, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x1

    const/16 v2, 0x21

    aput-byte v2, v0, v1

    .line 27
    sget-object v0, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x2

    const/16 v2, 0x41

    aput-byte v2, v0, v1

    .line 28
    sget-object v0, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x3

    const/16 v2, 0x4d

    aput-byte v2, v0, v1

    .line 29
    sget-object v0, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x4

    const/16 v2, 0x52

    aput-byte v2, v0, v1

    .line 30
    sget-object v0, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x5

    const/16 v2, 0xa

    aput-byte v2, v0, v1

    .line 31
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addPaddingToAMR([BIII)[B
    .locals 8
    .param p0, "data"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I
    .param p3, "paddingMillis"    # I

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 51
    invoke-static {p0, p1, p2}, Lcom/vlingo/core/internal/audio/AMRUtil;->isAMRAudioOK([BII)Z

    move-result v0

    .line 52
    .local v0, "amrAudioOK":Z
    if-nez v0, :cond_0

    .line 72
    .end local p0    # "data":[B
    :goto_0
    return-object p0

    .line 58
    .restart local p0    # "data":[B
    :cond_0
    mul-int/lit8 v4, p3, 0xd

    div-int/lit8 v2, v4, 0x14

    .line 59
    .local v2, "iPaddedFrames":I
    mul-int/lit8 v4, v2, 0x2

    add-int/2addr v4, p2

    new-array v3, v4, [B

    .line 60
    .local v3, "paddedAMR":[B
    sget-object v4, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    sget-object v5, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v5, v5

    invoke-static {v4, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 62
    sget-object v4, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v1, v4

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 63
    aput-byte v7, v3, v1

    .line 62
    add-int/lit8 v1, v1, 0xd

    goto :goto_1

    .line 66
    :cond_1
    sget-object v4, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v4, v4

    add-int/2addr v4, p1

    sget-object v5, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v5, v5

    add-int/2addr v5, v2

    sget-object v6, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v6, v6

    sub-int v6, p2, v6

    invoke-static {p0, v4, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 68
    sget-object v4, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v4, v4

    add-int/2addr v4, v2

    add-int v1, v4, p2

    :goto_2
    array-length v4, v3

    if-ge v1, v4, :cond_2

    .line 69
    aput-byte v7, v3, v1

    .line 68
    add-int/lit8 v1, v1, 0xd

    goto :goto_2

    :cond_2
    move-object p0, v3

    .line 72
    goto :goto_0
.end method

.method public static isAMRAudioOK([BII)Z
    .locals 4
    .param p0, "data"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I

    .prologue
    .line 36
    if-eqz p0, :cond_0

    const/16 v2, 0xa

    if-ge p2, v2, :cond_2

    .line 37
    :cond_0
    const/4 v0, 0x0

    .line 47
    :cond_1
    :goto_0
    return v0

    .line 40
    :cond_2
    const/4 v0, 0x1

    .line 41
    .local v0, "containsHeader":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v2, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 42
    sget-object v2, Lcom/vlingo/core/internal/audio/AMRUtil;->AMR_HEADER:[B

    aget-byte v2, v2, v1

    add-int v3, v1, p1

    aget-byte v3, p0, v3

    if-eq v2, v3, :cond_3

    .line 43
    const/4 v0, 0x0

    .line 44
    goto :goto_0

    .line 41
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
