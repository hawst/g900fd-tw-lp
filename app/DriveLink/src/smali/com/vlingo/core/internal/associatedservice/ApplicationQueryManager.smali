.class public Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
.super Ljava/lang/Object;
.source "ApplicationQueryManager.java"

# interfaces
.implements Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManager;


# static fields
.field private static final MANAGERS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManager;",
            ">;"
        }
    .end annotation
.end field

.field private static final PROXIES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManagerProxy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected applicationContentProviderBase:Ljava/lang/String;

.field private contentProviderVersion:I

.field private contentResolver:Landroid/content/ContentResolver;

.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->MANAGERS:Ljava/util/Map;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->PROXIES:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "applicationContentProviderBase"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->applicationContentProviderBase:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->context:Landroid/content/Context;

    .line 29
    iput-object v0, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->contentResolver:Landroid/content/ContentResolver;

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->contentProviderVersion:I

    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "context must be passed non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_0
    invoke-static {p2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "applicationContentProviderBase must be passed with valid value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_1
    iput-object p2, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->applicationContentProviderBase:Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->context:Landroid/content/Context;

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->contentResolver:Landroid/content/ContentResolver;

    .line 44
    return-void
.end method

.method private getCursor(Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 162
    const/4 v7, 0x0

    .line 163
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 164
    .local v8, "gotCursor":Z
    const/4 v10, 0x5

    .line 165
    .local v10, "maxAttempts":I
    const/16 v9, 0xc8

    .line 166
    .local v9, "interTrySleepMs":I
    const/4 v6, 0x0

    .line 167
    .local v6, "attempts":I
    :cond_0
    :goto_0
    if-nez v8, :cond_1

    const/4 v0, 0x5

    if-ge v6, v0, :cond_1

    .line 169
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->contentResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 170
    const/4 v8, 0x1

    .line 175
    :goto_1
    add-int/lit8 v6, v6, 0x1

    .line 176
    if-nez v8, :cond_0

    .line 178
    const-wide/16 v0, 0xc8

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    goto :goto_0

    .line 186
    :cond_1
    return-object v7

    .line 171
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;

    .prologue
    .line 48
    sget-object v1, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->MANAGERS:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    sget-object v1, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->MANAGERS:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManager;

    .line 53
    :goto_0
    return-object v1

    .line 51
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 52
    .local v0, "manager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    sget-object v1, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->MANAGERS:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 53
    goto :goto_0
.end method

.method public static registerProxy(Ljava/lang/String;Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManagerProxy;)V
    .locals 1
    .param p0, "applicationContentProviderBase"    # Ljava/lang/String;
    .param p1, "proxy"    # Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManagerProxy;

    .prologue
    .line 57
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->PROXIES:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    return-void
.end method


# virtual methods
.method public getApplicationContentProviderBase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->applicationContentProviderBase:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderVersion()I
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 76
    iget v3, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->contentProviderVersion:I

    if-gez v3, :cond_0

    .line 77
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->applicationContentProviderBase:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "content_provider_version"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 78
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValueFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "sValue":Ljava/lang/String;
    if-nez v1, :cond_1

    move v3, v4

    :goto_0
    :try_start_0
    iput v3, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->contentProviderVersion:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .end local v1    # "sValue":Ljava/lang/String;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_1
    iget v3, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->contentProviderVersion:I

    return v3

    .line 80
    .restart local v1    # "sValue":Ljava/lang/String;
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_1
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v3

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/NumberFormatException;
    iput v4, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->contentProviderVersion:I

    goto :goto_1
.end method

.method public getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 102
    if-nez p1, :cond_0

    .line 103
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "name parameter must be passed with valid value"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 105
    :cond_0
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "name must be passed with valid value"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 108
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->providerBaseIsWorking()Z

    move-result v1

    if-nez v1, :cond_2

    .line 109
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "applicationContentProviderBase not valid"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 112
    :cond_2
    sget-object v1, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->PROXIES:Ljava/util/Map;

    iget-object v2, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->applicationContentProviderBase:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 113
    sget-object v1, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->PROXIES:Ljava/util/Map;

    iget-object v2, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->applicationContentProviderBase:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManagerProxy;

    invoke-interface {v1, p0, p1}, Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManagerProxy;->getValue(Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 117
    :goto_0
    return-object v1

    .line 116
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->applicationContentProviderBase:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 117
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValueFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getValueFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    .line 122
    const-string/jumbo v3, "value"

    .line 124
    .local v3, "valueColumnName":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getCursor(Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    .line 125
    .local v0, "cur":Landroid/database/Cursor;
    if-nez v0, :cond_0

    move-object v1, v4

    .line 150
    :goto_0
    return-object v1

    .line 131
    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 132
    const-string/jumbo v5, "value"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 133
    .local v2, "valueColumnIndex":I
    const/4 v5, -0x1

    if-eq v5, v2, :cond_1

    .line 134
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "val":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 136
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v1    # "val":Ljava/lang/String;
    .end local v2    # "valueColumnIndex":I
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v1, v4

    .line 150
    goto :goto_0

    .line 148
    :catchall_0
    move-exception v4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v4
.end method

.method protected providerBaseIsWorking()Z
    .locals 4

    .prologue
    .line 61
    iget-object v3, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->applicationContentProviderBase:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 62
    .local v2, "uri":Landroid/net/Uri;
    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getCursor(Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    .line 63
    .local v0, "cur":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 64
    .local v1, "providerBaseIsWorking":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 65
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 67
    :cond_0
    return v1

    .line 63
    .end local v1    # "providerBaseIsWorking":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
