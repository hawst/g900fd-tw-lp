.class public Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;
.super Ljava/lang/Object;
.source "PendingCallback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;
    }
.end annotation


# static fields
.field public static final NEVER_EXPIRE_DELAY:J = -0x1L

.field private static final NS_IN_MS:J = 0xf4240L


# instance fields
.field private final callback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;

.field private callbackDelayNs:J

.field private cancelled:Z

.field private final handler:Landroid/os/Handler;

.field private final maxCallbackDelayNs:J

.field private resetTimeNs:J

.field private final startTimeNs:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;J)V
    .locals 7
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "callback"    # Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;
    .param p3, "callbackDelayMs"    # J

    .prologue
    .line 18
    const-wide/16 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;-><init>(Landroid/os/Handler;Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;JJ)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;JJ)V
    .locals 6
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "callback"    # Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;
    .param p3, "callbackDelayMs"    # J
    .param p5, "maxCallbackDelayMs"    # J

    .prologue
    const-wide/16 v0, -0x1

    const-wide/32 v4, 0xf4240

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->handler:Landroid/os/Handler;

    .line 23
    iput-object p2, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->callback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;

    .line 24
    mul-long v2, p3, v4

    iput-wide v2, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->callbackDelayNs:J

    .line 25
    cmp-long v2, v0, p5

    if-nez v2, :cond_0

    :goto_0
    iput-wide v0, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->maxCallbackDelayNs:J

    .line 26
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->startTimeNs:J

    .line 27
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->resetTimeNs:J

    .line 28
    iget-wide v0, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->callbackDelayNs:J

    div-long/2addr v0, v4

    invoke-virtual {p1, p0, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 29
    return-void

    .line 25
    :cond_0
    mul-long v0, p5, v4

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->cancelled:Z

    .line 61
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 62
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 56
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->resetTimeNs:J

    .line 57
    return-void
.end method

.method public run()V
    .locals 11

    .prologue
    const-wide/16 v9, -0x1

    const/4 v8, 0x1

    .line 33
    iget-boolean v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->cancelled:Z

    if-eqz v4, :cond_1

    .line 34
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->handler:Landroid/os/Handler;

    invoke-virtual {v4, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 38
    .local v2, "currentTimeNs":J
    iget-wide v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->maxCallbackDelayNs:J

    cmp-long v4, v9, v4

    if-eqz v4, :cond_2

    iget-wide v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->startTimeNs:J

    sub-long v4, v2, v4

    iget-wide v6, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->maxCallbackDelayNs:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2

    .line 39
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->callback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;

    if-eqz v4, :cond_0

    .line 40
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->callback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;

    invoke-interface {v4}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;->executeCallback()V

    .line 41
    iput-boolean v8, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->cancelled:Z

    goto :goto_0

    .line 43
    :cond_2
    iget-wide v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->resetTimeNs:J

    sub-long v4, v2, v4

    iget-wide v6, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->callbackDelayNs:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_3

    .line 44
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->callback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;

    if-eqz v4, :cond_0

    .line 45
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->callback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;

    invoke-interface {v4}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;->executeCallback()V

    .line 46
    iput-boolean v8, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->cancelled:Z

    goto :goto_0

    .line 49
    :cond_3
    iget-wide v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->resetTimeNs:J

    iget-wide v6, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->callbackDelayNs:J

    add-long/2addr v4, v6

    sub-long v6, v4, v2

    iget-wide v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->maxCallbackDelayNs:J

    cmp-long v4, v9, v4

    if-nez v4, :cond_4

    const-wide v4, 0x7fffffffffffffffL

    :goto_1
    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 51
    .local v0, "additionalDelay":J
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->handler:Landroid/os/Handler;

    const-wide/32 v5, 0xf4240

    div-long v5, v0, v5

    invoke-virtual {v4, p0, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 49
    .end local v0    # "additionalDelay":J
    :cond_4
    iget-wide v4, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->startTimeNs:J

    iget-wide v8, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->maxCallbackDelayNs:J

    add-long/2addr v4, v8

    sub-long/2addr v4, v2

    goto :goto_1
.end method

.method public updateCallbackDelay(J)V
    .locals 2
    .param p1, "newCallbackDelayMs"    # J

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 66
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p1

    iput-wide v0, p0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->callbackDelayNs:J

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->run()V

    .line 68
    return-void
.end method
