.class Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;
.super Landroid/os/Handler;
.source "TonePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/TonePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NotificationHandler"
.end annotation


# static fields
.field private static final STARTED:I = 0x1

.field private static final STOPPED:I = 0x2


# instance fields
.field private listener:Lcom/vlingo/core/internal/audio/TonePlayer$Listener;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/audio/TonePlayer$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/core/internal/audio/TonePlayer$Listener;

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;->listener:Lcom/vlingo/core/internal/audio/TonePlayer$Listener;

    .line 54
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;->listener:Lcom/vlingo/core/internal/audio/TonePlayer$Listener;

    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 61
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 63
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;->listener:Lcom/vlingo/core/internal/audio/TonePlayer$Listener;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/audio/TonePlayer$Listener;->onStarted(I)V

    goto :goto_0

    .line 66
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;->listener:Lcom/vlingo/core/internal/audio/TonePlayer$Listener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/audio/TonePlayer$Listener;->onStopped()V

    goto :goto_0

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
