.class Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;
.super Ljava/lang/Object;
.source "StringUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/StringUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LookupTranslator"
.end annotation


# instance fields
.field private final lookupMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final mLongest:I

.field private final mShortest:I


# direct methods
.method public varargs constructor <init>([[Ljava/lang/CharSequence;)V
    .locals 11
    .param p1, "lookup"    # [[Ljava/lang/CharSequence;

    .prologue
    const/4 v10, 0x0

    .line 727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 728
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;->lookupMap:Ljava/util/HashMap;

    .line 729
    const v5, 0x7fffffff

    .line 730
    .local v5, "shortest":I
    const/4 v3, 0x0

    .line 731
    .local v3, "longest":I
    if-eqz p1, :cond_2

    .line 732
    move-object v0, p1

    .local v0, "arr$":[[Ljava/lang/CharSequence;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v4, v0, v1

    .line 733
    .local v4, "seq":[Ljava/lang/CharSequence;
    iget-object v7, p0, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;->lookupMap:Ljava/util/HashMap;

    aget-object v8, v4, v10

    const/4 v9, 0x1

    aget-object v9, v4, v9

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 734
    aget-object v7, v4, v10

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v6

    .line 735
    .local v6, "sz":I
    if-ge v6, v5, :cond_0

    .line 736
    move v5, v6

    .line 738
    :cond_0
    if-le v6, v3, :cond_1

    .line 739
    move v3, v6

    .line 732
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 743
    .end local v0    # "arr$":[[Ljava/lang/CharSequence;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "seq":[Ljava/lang/CharSequence;
    .end local v6    # "sz":I
    :cond_2
    iput v5, p0, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;->mShortest:I

    .line 744
    iput v3, p0, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;->mLongest:I

    .line 745
    return-void
.end method


# virtual methods
.method public translate(Ljava/lang/CharSequence;ILjava/io/Writer;)I
    .locals 6
    .param p1, "input"    # Ljava/lang/CharSequence;
    .param p2, "index"    # I
    .param p3, "out"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 748
    iget v1, p0, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;->mLongest:I

    .line 749
    .local v1, "max":I
    iget v4, p0, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;->mLongest:I

    add-int/2addr v4, p2

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-le v4, v5, :cond_0

    .line 750
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    sub-int v1, v4, p2

    .line 753
    :cond_0
    move v0, v1

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;->mShortest:I

    if-lt v0, v4, :cond_2

    .line 754
    add-int v4, p2, v0

    invoke-interface {p1, p2, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    .line 755
    .local v3, "subSeq":Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;->lookupMap:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 756
    .local v2, "result":Ljava/lang/CharSequence;
    if-eqz v2, :cond_1

    .line 757
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 761
    .end local v0    # "i":I
    .end local v2    # "result":Ljava/lang/CharSequence;
    .end local v3    # "subSeq":Ljava/lang/CharSequence;
    :goto_1
    return v0

    .line 753
    .restart local v0    # "i":I
    .restart local v2    # "result":Ljava/lang/CharSequence;
    .restart local v3    # "subSeq":Ljava/lang/CharSequence;
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 761
    .end local v2    # "result":Ljava/lang/CharSequence;
    .end local v3    # "subSeq":Ljava/lang/CharSequence;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final translate(Ljava/lang/CharSequence;Ljava/io/Writer;)V
    .locals 7
    .param p1, "input"    # Ljava/lang/CharSequence;
    .param p2, "out"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 772
    if-nez p2, :cond_0

    .line 773
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "The Writer must not be null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 775
    :cond_0
    if-nez p1, :cond_2

    .line 794
    :cond_1
    return-void

    .line 778
    :cond_2
    const/4 v3, 0x0

    .line 779
    .local v3, "pos":I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 780
    .local v2, "len":I
    :cond_3
    :goto_0
    if-ge v3, v2, :cond_1

    .line 781
    invoke-virtual {p0, p1, v3, p2}, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;->translate(Ljava/lang/CharSequence;ILjava/io/Writer;)I

    move-result v1

    .line 782
    .local v1, "consumed":I
    if-nez v1, :cond_4

    .line 783
    invoke-static {p1, v3}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v0

    .line 784
    .local v0, "c":[C
    invoke-virtual {p2, v0}, Ljava/io/Writer;->write([C)V

    .line 785
    array-length v5, v0

    add-int/2addr v3, v5

    .line 786
    goto :goto_0

    .line 790
    .end local v0    # "c":[C
    :cond_4
    const/4 v4, 0x0

    .local v4, "pt":I
    :goto_1
    if-ge v4, v1, :cond_3

    .line 791
    invoke-static {p1, v3}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->charCount(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 790
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
