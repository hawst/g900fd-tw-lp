.class public Lcom/vlingo/core/internal/contacts/scoring/FullyNormalizedSetMatchContactScore;
.super Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;
.source "FullyNormalizedSetMatchContactScore.java"


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "score"    # I
    .param p2, "backOff"    # I

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;-><init>(II)V

    .line 8
    return-void
.end method


# virtual methods
.method protected fourthPathSuits(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p3, "qw"    # Ljava/lang/String;
    .param p4, "mw"    # Ljava/lang/String;
    .param p5, "nqw"    # Ljava/lang/String;
    .param p6, "nmw"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "queryMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p2, "matchMatchedSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p2, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p4, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p6, p5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getScoreDown(IF[Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1, "matches"    # I
    .param p2, "partialMatches"    # F
    .param p3, "rawQueryWords"    # [Ljava/lang/String;
    .param p4, "rawMatchWords"    # [Ljava/lang/String;

    .prologue
    .line 19
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;->getScoreDown(IF[Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 20
    .local v0, "scoreDown":I
    if-lez v0, :cond_0

    .line 23
    int-to-float v1, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, p2, v2

    int-to-float v3, p1

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/scoring/FullyNormalizedSetMatchContactScore;->getBackOff()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v0, v1

    .line 25
    :cond_0
    return v0
.end method
