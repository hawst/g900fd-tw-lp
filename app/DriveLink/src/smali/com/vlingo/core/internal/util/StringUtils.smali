.class public abstract Lcom/vlingo/core/internal/util/StringUtils;
.super Ljava/lang/Object;
.source "StringUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;
    }
.end annotation


# static fields
.field public static final CHINESE_UNICODE_BLOCKS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Character$UnicodeBlock;",
            ">;"
        }
    .end annotation
.end field

.field public static final CJK_UNICODE_BLOCKS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Character$UnicodeBlock;",
            ">;"
        }
    .end annotation
.end field

.field private static final KOREAN_HONORFIC_STRIP:Ljava/util/regex/Pattern;

.field public static final LATIN_CHARACTER_UNICODE_BLOCKS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Character$UnicodeBlock;",
            ">;"
        }
    .end annotation
.end field

.field public static final NON_ASCII_WITH_CASE_UNICODE_BLOCKS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Character$UnicodeBlock;",
            ">;"
        }
    .end annotation
.end field

.field private static final VAR_PATTERN:Ljava/util/regex/Pattern;

.field private static final XML_ESCAPE:[[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 36
    const-string/jumbo v0, "\ub2d8$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/StringUtils;->KOREAN_HONORFIC_STRIP:Ljava/util/regex/Pattern;

    .line 38
    const/4 v0, 0x5

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "&quot;"

    aput-object v2, v1, v4

    const-string/jumbo v2, "\""

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "&amp;"

    aput-object v2, v1, v4

    const-string/jumbo v2, "&"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "&lt;"

    aput-object v2, v1, v4

    const-string/jumbo v2, "<"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    const/4 v1, 0x3

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "&gt;"

    aput-object v3, v2, v4

    const-string/jumbo v3, ">"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "&apos;"

    aput-object v3, v2, v4

    const-string/jumbo v3, "\'"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/util/StringUtils;->XML_ESCAPE:[[Ljava/lang/String;

    .line 46
    const-string/jumbo v0, "\\$\\[(.+?)\\]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/StringUtils;->VAR_PATTERN:Ljava/util/regex/Pattern;

    .line 55
    new-instance v0, Lcom/vlingo/core/internal/util/StringUtils$1;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/StringUtils$1;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/StringUtils;->CHINESE_UNICODE_BLOCKS:Ljava/util/Set;

    .line 72
    new-instance v0, Lcom/vlingo/core/internal/util/StringUtils$2;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/StringUtils$2;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/StringUtils;->CJK_UNICODE_BLOCKS:Ljava/util/Set;

    .line 90
    new-instance v0, Lcom/vlingo/core/internal/util/StringUtils$3;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/StringUtils$3;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/StringUtils;->NON_ASCII_WITH_CASE_UNICODE_BLOCKS:Ljava/util/Set;

    .line 95
    new-instance v0, Lcom/vlingo/core/internal/util/StringUtils$4;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/StringUtils$4;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/StringUtils;->LATIN_CHARACTER_UNICODE_BLOCKS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 716
    return-void
.end method

.method public static arePhoneNumbersTheSame(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "n1"    # Ljava/lang/String;
    .param p1, "n2"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 283
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 293
    :goto_0
    return v0

    .line 285
    :cond_1
    const-string/jumbo v0, "+"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 287
    :cond_2
    const-string/jumbo v0, "+"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 288
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 289
    :cond_3
    const-string/jumbo v0, "1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 290
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 291
    :cond_4
    const-string/jumbo v0, "1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 292
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 293
    :cond_5
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static byteArrayToString([B)Ljava/lang/String;
    .locals 4
    .param p0, "arr"    # [B

    .prologue
    .line 213
    new-instance v2, Ljava/lang/StringBuffer;

    array-length v3, p0

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 214
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_0

    .line 215
    aget-byte v0, p0, v1

    .line 216
    .local v0, "b":B
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 214
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    .end local v0    # "b":B
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static canTTS(Ljava/lang/String;I)Z
    .locals 2
    .param p0, "tts"    # Ljava/lang/String;
    .param p1, "i"    # I

    .prologue
    .line 267
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 269
    .local v0, "c":C
    sparse-switch v0, :sswitch_data_0

    .line 278
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 276
    :sswitch_0
    const/4 v1, 0x0

    goto :goto_0

    .line 269
    nop

    :sswitch_data_0
    .sparse-switch
        0x28 -> :sswitch_0
        0x29 -> :sswitch_0
        0x5b -> :sswitch_0
        0x5d -> :sswitch_0
        0x7b -> :sswitch_0
        0x7c -> :sswitch_0
        0x7d -> :sswitch_0
    .end sparse-switch
.end method

.method public static capitalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 806
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 811
    .end local p0    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 809
    .restart local p0    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 810
    .local v0, "stringArray":[C
    aget-char v1, v0, v2

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    aput-char v1, v0, v2

    .line 811
    new-instance p0, Ljava/lang/String;

    .end local p0    # "value":Ljava/lang/String;
    invoke-direct {p0, v0}, Ljava/lang/String;-><init>([C)V

    goto :goto_0
.end method

.method public static cleanPhoneNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    .line 234
    if-nez p0, :cond_0

    .line 243
    .end local p0    # "number":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 237
    .restart local p0    # "number":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 238
    .local v1, "cleaned":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 239
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 240
    .local v0, "c":C
    const/16 v3, 0x30

    if-lt v0, v3, :cond_1

    const/16 v3, 0x39

    if-gt v0, v3, :cond_1

    .line 241
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 238
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 243
    .end local v0    # "c":C
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static cleanTts(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "tts"    # Ljava/lang/String;

    .prologue
    .line 247
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 248
    .local v1, "cleaned":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 249
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 251
    .local v0, "c":C
    invoke-static {p0, v2}, Lcom/vlingo/core/internal/util/StringUtils;->canTTS(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 252
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 248
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 255
    .end local v0    # "c":C
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static compareVersions(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p0, "version1"    # Ljava/lang/String;
    .param p1, "version2"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x2e

    const/4 v6, 0x0

    .line 448
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 462
    :cond_0
    :goto_0
    return v6

    .line 451
    :cond_1
    invoke-static {p0, v8}, Lcom/vlingo/core/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v2

    .line 452
    .local v2, "v1":[Ljava/lang/String;
    invoke-static {p1, v8}, Lcom/vlingo/core/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v3

    .line 454
    .local v3, "v2":[Ljava/lang/String;
    array-length v7, v2

    array-length v8, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 455
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 456
    const/4 v4, 0x0

    .local v4, "vc1":I
    const/4 v5, 0x0

    .line 457
    .local v5, "vc2":I
    array-length v7, v2

    if-ge v0, v7, :cond_2

    aget-object v7, v2, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 458
    :cond_2
    array-length v7, v3

    if-ge v0, v7, :cond_3

    aget-object v7, v3, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 459
    :cond_3
    if-le v4, v5, :cond_4

    const/4 v6, 0x1

    goto :goto_0

    .line 460
    :cond_4
    if-ge v4, v5, :cond_5

    const/4 v6, -0x1

    goto :goto_0

    .line 455
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static conditionWithoutSpaces(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "nameField"    # Ljava/lang/String;

    .prologue
    .line 1119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "REPLACE(`"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "`,\' \',\'\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static containsString(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "searchQuery"    # Ljava/lang/String;
    .param p1, "candidateTitle"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 632
    if-eqz p0, :cond_0

    if-nez p1, :cond_3

    .line 633
    :cond_0
    if-nez p0, :cond_2

    if-nez p1, :cond_2

    .line 643
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 633
    goto :goto_0

    .line 636
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 637
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 639
    invoke-virtual {p1, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 643
    goto :goto_0
.end method

.method public static containsWord(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p0, "searchQuery"    # Ljava/lang/String;
    .param p1, "candidateTitle"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 648
    if-eqz p0, :cond_0

    if-nez p1, :cond_3

    .line 649
    :cond_0
    if-nez p0, :cond_2

    if-nez p1, :cond_2

    .line 662
    :cond_1
    :goto_0
    return v5

    :cond_2
    move v5, v6

    .line 649
    goto :goto_0

    .line 652
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 653
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 654
    const-string/jumbo v7, "\\s+"

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 656
    .local v4, "words":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    .line 657
    .local v3, "word":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 656
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v3    # "word":Ljava/lang/String;
    :cond_4
    move v5, v6

    .line 662
    goto :goto_0
.end method

.method public static final convertBytesToString([B)Ljava/lang/String;
    .locals 3
    .param p0, "bytes"    # [B

    .prologue
    .line 120
    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-object v1

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "ex":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public static convertDayOfWeekToInt(Ljava/lang/String;)I
    .locals 1
    .param p0, "weekDay"    # Ljava/lang/String;

    .prologue
    .line 694
    const-string/jumbo v0, "sunday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "sun"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 695
    :cond_0
    const/4 v0, 0x0

    .line 711
    :goto_0
    return v0

    .line 696
    :cond_1
    const-string/jumbo v0, "monday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "mon"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 697
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 698
    :cond_3
    const-string/jumbo v0, "tuesday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "tue"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 699
    :cond_4
    const/4 v0, 0x2

    goto :goto_0

    .line 700
    :cond_5
    const-string/jumbo v0, "wednesday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string/jumbo v0, "wed"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 701
    :cond_6
    const/4 v0, 0x3

    goto :goto_0

    .line 702
    :cond_7
    const-string/jumbo v0, "thursday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "thu"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 703
    :cond_8
    const/4 v0, 0x4

    goto :goto_0

    .line 704
    :cond_9
    const-string/jumbo v0, "friday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string/jumbo v0, "fri"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 705
    :cond_a
    const/4 v0, 0x5

    goto :goto_0

    .line 706
    :cond_b
    const-string/jumbo v0, "saturday"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    const-string/jumbo v0, "sat"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 707
    :cond_c
    const/4 v0, 0x6

    goto :goto_0

    .line 711
    :cond_d
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static final convertStringToBytes(Ljava/lang/String;)[B
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 128
    :try_start_0
    const-string/jumbo v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 130
    :goto_0
    return-object v1

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "ex":Ljava/io/UnsupportedEncodingException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static final formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "contactName"    # Ljava/lang/String;

    .prologue
    .line 170
    sget-object v0, Lcom/vlingo/core/internal/util/StringUtils;->KOREAN_HONORFIC_STRIP:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    .line 297
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumber(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatPhoneNumber(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 12
    .param p0, "number"    # Ljava/lang/String;
    .param p1, "showParens"    # Z

    .prologue
    const/4 v5, 0x1

    const/16 v11, 0xa

    const/4 v10, 0x6

    const/4 v6, 0x0

    const/4 v9, 0x3

    .line 302
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_2

    :cond_0
    const-string/jumbo p0, ""

    .line 374
    .end local p0    # "number":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p0

    .line 304
    .restart local p0    # "number":Ljava/lang/String;
    :cond_2
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->cleanPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 305
    .local v0, "c":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    .line 306
    .local v3, "clen":I
    if-nez v3, :cond_3

    const-string/jumbo p0, ""

    goto :goto_0

    .line 307
    :cond_3
    new-instance v2, Ljava/lang/StringBuffer;

    add-int/lit8 v7, v3, 0x10

    invoke-direct {v2, v7}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 308
    .local v2, "cleaned":Ljava/lang/StringBuffer;
    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x31

    if-ne v7, v8, :cond_5

    move v4, v5

    .line 309
    .local v4, "startsWithOne":Z
    :goto_1
    const-string/jumbo v7, "+"

    invoke-virtual {p0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 310
    const-string/jumbo v7, "+"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 312
    :cond_4
    if-eqz v4, :cond_f

    .line 313
    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 314
    add-int/lit8 v3, v3, -0x1

    .line 316
    if-lt v3, v9, :cond_8

    if-gt v3, v10, :cond_8

    .line 317
    if-eqz p1, :cond_6

    const-string/jumbo v5, "1 ("

    :goto_2
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 318
    invoke-virtual {v0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 319
    if-eqz p1, :cond_7

    const-string/jumbo v5, ") "

    :goto_3
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 320
    invoke-virtual {v0, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 321
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .end local v4    # "startsWithOne":Z
    :cond_5
    move v4, v6

    .line 308
    goto :goto_1

    .line 317
    .restart local v4    # "startsWithOne":Z
    :cond_6
    const-string/jumbo v5, "1-"

    goto :goto_2

    .line 319
    :cond_7
    const-string/jumbo v5, "-"

    goto :goto_3

    .line 323
    :cond_8
    const/4 v5, 0x7

    if-lt v3, v5, :cond_b

    if-gt v3, v11, :cond_b

    .line 324
    if-eqz p1, :cond_9

    const-string/jumbo v5, "1 ("

    :goto_4
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 325
    invoke-virtual {v0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 326
    if-eqz p1, :cond_a

    const-string/jumbo v5, ") "

    :goto_5
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 327
    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 328
    const-string/jumbo v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 329
    invoke-virtual {v0, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 330
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 324
    :cond_9
    const-string/jumbo v5, "1-"

    goto :goto_4

    .line 326
    :cond_a
    const-string/jumbo v5, "-"

    goto :goto_5

    .line 332
    :cond_b
    if-le v3, v11, :cond_1

    .line 333
    add-int/lit8 v1, v3, -0xa

    .line 334
    .local v1, "cidx":I
    if-eqz p1, :cond_c

    const-string/jumbo v5, "1 "

    :goto_6
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 335
    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 336
    if-eqz p1, :cond_d

    const-string/jumbo v5, " ("

    :goto_7
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 337
    add-int/lit8 v5, v1, 0x3

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 338
    if-eqz p1, :cond_e

    const-string/jumbo v5, ") "

    :goto_8
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 339
    add-int/lit8 v5, v1, 0x3

    add-int/lit8 v6, v1, 0x6

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 340
    const-string/jumbo v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 341
    add-int/lit8 v5, v1, 0x6

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 342
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 343
    goto/16 :goto_0

    .line 334
    :cond_c
    const-string/jumbo v5, "1-"

    goto :goto_6

    .line 336
    :cond_d
    const-string/jumbo v5, "-"

    goto :goto_7

    .line 338
    :cond_e
    const-string/jumbo v5, "-"

    goto :goto_8

    .line 346
    .end local v1    # "cidx":I
    :cond_f
    const/4 v5, 0x4

    if-lt v3, v5, :cond_10

    const/4 v5, 0x7

    if-gt v3, v5, :cond_10

    .line 347
    invoke-virtual {v0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 348
    const-string/jumbo v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 349
    invoke-virtual {v0, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 350
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 352
    :cond_10
    const/16 v5, 0x8

    if-lt v3, v5, :cond_13

    if-gt v3, v11, :cond_13

    .line 353
    if-eqz p1, :cond_11

    const-string/jumbo v5, "("

    :goto_9
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 354
    invoke-virtual {v0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 355
    if-eqz p1, :cond_12

    const-string/jumbo v5, ") "

    :goto_a
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 356
    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 357
    const-string/jumbo v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 358
    invoke-virtual {v0, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 359
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 353
    :cond_11
    const-string/jumbo v5, ""

    goto :goto_9

    .line 355
    :cond_12
    const-string/jumbo v5, "-"

    goto :goto_a

    .line 361
    :cond_13
    if-le v3, v11, :cond_1

    .line 362
    add-int/lit8 v1, v3, -0xa

    .line 363
    .restart local v1    # "cidx":I
    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 364
    if-eqz p1, :cond_14

    const-string/jumbo v5, " ("

    :goto_b
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 365
    add-int/lit8 v5, v1, 0x3

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 366
    if-eqz p1, :cond_15

    const-string/jumbo v5, ") "

    :goto_c
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 367
    add-int/lit8 v5, v1, 0x3

    add-int/lit8 v6, v1, 0x6

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 368
    const-string/jumbo v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 369
    add-int/lit8 v5, v1, 0x6

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 370
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 364
    :cond_14
    const-string/jumbo v5, "-"

    goto :goto_b

    .line 366
    :cond_15
    const-string/jumbo v5, "-"

    goto :goto_c
.end method

.method public static final formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "[0-9()\\+\\-\\s]+"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const-string/jumbo v0, "\\D"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 164
    const-string/jumbo v0, "([0-9])"

    const-string/jumbo v1, "$1 "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 166
    :cond_0
    return-object p0
.end method

.method public static getHostname(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 381
    const-string/jumbo v2, "://"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 382
    .local v1, "slashPos":I
    if-ne v1, v4, :cond_0

    .line 383
    const-string/jumbo v2, ""

    .line 390
    :goto_0
    return-object v2

    .line 384
    :cond_0
    const-string/jumbo v2, "/"

    add-int/lit8 v3, v1, 0x3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 385
    .local v0, "endSlashPos":I
    if-ne v0, v4, :cond_1

    .line 386
    const-string/jumbo v2, ";"

    add-int/lit8 v3, v1, 0x3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 387
    if-ne v0, v4, :cond_1

    .line 388
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 390
    :cond_1
    add-int/lit8 v2, v1, 0x3

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static getRandomEmail()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x5f

    const/16 v2, 0x40

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/core/internal/util/StringUtils;->getRandomString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/StringUtils;->getRandomString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".com"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getRandomPhoneNumber()Ljava/lang/String;
    .locals 7

    .prologue
    .line 569
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 570
    .local v2, "random":Ljava/util/Random;
    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    const-wide/16 v5, 0x1

    add-long v0, v3, v5

    .line 571
    .local v0, "number":J
    const-wide v3, 0x2540be400L

    rem-long/2addr v0, v3

    .line 572
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static getRandomPhoneNumberType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 564
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 565
    .local v0, "random":Ljava/util/Random;
    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v1

    rem-int/lit8 v1, v1, 0x8

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getRandomString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 580
    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    .line 581
    .local v4, "random":Ljava/util/Random;
    const-string/jumbo v3, ""

    .line 582
    .local v3, "rand":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v5

    rem-int/lit8 v5, v5, 0x9

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    add-int/lit8 v2, v5, 0x1

    .line 583
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 584
    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v5

    int-to-char v0, v5

    .line 585
    .local v0, "c":C
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 583
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 587
    .end local v0    # "c":C
    :cond_0
    return-object v3
.end method

.method private static getRandomUID()Ljava/lang/String;
    .locals 7

    .prologue
    .line 557
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 558
    .local v2, "random":Ljava/util/Random;
    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    const-wide/16 v5, 0x1

    add-long v0, v3, v5

    .line 559
    .local v0, "number":J
    const-wide v3, 0x2540be400L

    rem-long/2addr v0, v3

    .line 560
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getSubstring(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "startDelimiter"    # Ljava/lang/String;
    .param p2, "endDelimiter"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 530
    if-nez p0, :cond_1

    .line 549
    :cond_0
    :goto_0
    return-object v2

    .line 533
    :cond_1
    const/4 v1, 0x0

    .line 534
    .local v1, "startIndex":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 536
    .local v0, "endIndex":I
    if-eqz p1, :cond_2

    .line 537
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 538
    if-ltz v1, :cond_0

    .line 540
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    .line 543
    :cond_2
    if-eqz p2, :cond_3

    .line 544
    invoke-virtual {p0, p2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 545
    if-ltz v0, :cond_0

    .line 549
    :cond_3
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static final getWordAtCursor(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p0, "phrase"    # Ljava/lang/String;
    .param p1, "cursorPosition"    # I

    .prologue
    .line 103
    const-string/jumbo v1, ""

    .line 104
    .local v1, "word":Ljava/lang/String;
    if-ltz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge p1, v4, :cond_1

    .line 105
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x20

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v3

    .line 106
    .local v3, "words":[Ljava/lang/String;
    const/4 v4, 0x0

    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    .line 107
    .local v0, "pos":I
    const/4 v2, 0x0

    .line 108
    .local v2, "wordindex":I
    :goto_0
    if-le p1, v0, :cond_0

    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_0

    .line 109
    add-int/lit8 v2, v2, 0x1

    aget-object v4, v3, v2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    .line 111
    :cond_0
    array-length v4, v3

    if-gt v2, v4, :cond_1

    .line 112
    aget-object v1, v3, v2

    .line 115
    .end local v0    # "pos":I
    .end local v2    # "wordindex":I
    .end local v3    # "words":[Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method public static isBlank(Ljava/lang/String;)Z
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 154
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isCJKString(Ljava/lang/String;)Z
    .locals 3
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 892
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 893
    sget-object v1, Lcom/vlingo/core/internal/util/StringUtils;->CJK_UNICODE_BLOCKS:Ljava/util/Set;

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 894
    const/4 v1, 0x0

    .line 897
    :goto_1
    return v1

    .line 892
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 897
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static isChineseString(Ljava/lang/String;)Z
    .locals 2
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 868
    sget-object v0, Lcom/vlingo/core/internal/util/StringUtils;->CHINESE_UNICODE_BLOCKS:Ljava/util/Set;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isDigit(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 259
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 260
    const/4 v0, 0x1

    .line 263
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "s1"    # Ljava/lang/String;
    .param p1, "s2"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 520
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 521
    const/4 v0, 0x1

    .line 526
    :cond_0
    :goto_0
    return v0

    .line 522
    :cond_1
    if-eqz p0, :cond_2

    if-eqz p1, :cond_0

    .line 524
    :cond_2
    if-nez p0, :cond_3

    if-nez p1, :cond_0

    .line 526
    :cond_3
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isIPAddress(Ljava/lang/String;)Z
    .locals 13
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, -0x1

    const/16 v9, 0x2e

    const/4 v7, 0x0

    .line 474
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ne v8, v10, :cond_1

    .line 516
    :cond_0
    :goto_0
    return v7

    .line 478
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 480
    const/4 v1, 0x0

    .local v1, "dotCount":I
    new-array v2, v12, [I

    .line 483
    .local v2, "dotPos":[I
    aput v10, v2, v7

    .line 484
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    aput v8, v2, v11

    .line 486
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v4, v8, :cond_4

    if-ge v1, v11, :cond_4

    .line 487
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 488
    .local v0, "ch":C
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v8

    if-nez v8, :cond_2

    if-ne v0, v9, :cond_0

    .line 490
    :cond_2
    if-ne v0, v9, :cond_3

    .line 491
    add-int/lit8 v1, v1, 0x1

    aput v4, v2, v1

    .line 486
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 495
    .end local v0    # "ch":C
    :cond_4
    const/4 v8, 0x3

    if-ne v1, v8, :cond_0

    .line 503
    const/4 v1, 0x1

    :goto_2
    if-ge v1, v12, :cond_5

    .line 504
    add-int/lit8 v8, v1, -0x1

    :try_start_0
    aget v8, v2, v8

    add-int/lit8 v4, v8, 0x1

    .line 505
    aget v8, v2, v1

    invoke-virtual {p0, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 506
    .local v5, "num":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 508
    .local v6, "number":I
    if-ltz v6, :cond_0

    const/16 v8, 0xff

    if-gt v6, v8, :cond_0

    .line 503
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 512
    .end local v5    # "num":Ljava/lang/String;
    .end local v6    # "number":I
    :catch_0
    move-exception v3

    .line 513
    .local v3, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 516
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_5
    const/4 v7, 0x1

    goto :goto_0
.end method

.method public static isJapaneseString(Ljava/lang/String;)Z
    .locals 2
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 881
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->symbolIsJapanese(C)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static isKorean(Ljava/lang/String;)Z
    .locals 3
    .param p0, "args"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1070
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->removeSpecialChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1071
    .local v0, "query":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1072
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1073
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->symbolIsKorean(C)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static isLatinCharacterOnlyString(Ljava/lang/String;)Z
    .locals 3
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 850
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 851
    sget-object v1, Lcom/vlingo/core/internal/util/StringUtils;->LATIN_CHARACTER_UNICODE_BLOCKS:Ljava/util/Set;

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 852
    const/4 v1, 0x0

    .line 855
    :goto_1
    return v1

    .line 850
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 855
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static isNullOrWhiteSpace(Ljava/lang/String;)Z
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 146
    if-eqz p0, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    :cond_0
    const/4 v0, 0x1

    .line 149
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPhoneNumber(Ljava/lang/String;)Z
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 222
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 223
    .local v2, "len":I
    if-nez v2, :cond_1

    .line 230
    :cond_0
    :goto_0
    return v3

    .line 225
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_4

    .line 226
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 227
    .local v0, "ch":C
    const/16 v4, 0x30

    if-lt v0, v4, :cond_2

    const/16 v4, 0x39

    if-le v0, v4, :cond_3

    :cond_2
    const/16 v4, 0x20

    if-eq v0, v4, :cond_3

    const/16 v4, 0x2d

    if-eq v0, v4, :cond_3

    const/16 v4, 0x2b

    if-eq v0, v4, :cond_3

    const/16 v4, 0x28

    if-eq v0, v4, :cond_3

    const/16 v4, 0x29

    if-ne v0, v4, :cond_0

    .line 225
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 230
    .end local v0    # "ch":C
    :cond_4
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static isPresent(Ljava/lang/String;)Z
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 158
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isRussian(Ljava/lang/String;)Z
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1044
    if-nez p0, :cond_1

    const-string/jumbo v0, ""

    .line 1046
    .local v0, "trimmed":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ljava/lang/Character$UnicodeBlock;->CYRILLIC:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 1044
    .end local v0    # "trimmed":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static isVersionAtLeast(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "version"    # Ljava/lang/String;
    .param p1, "atLeast"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 437
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 440
    :cond_0
    :goto_0
    return v1

    .line 439
    :cond_1
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/util/StringUtils;->compareVersions(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 440
    .local v0, "res":I
    if-eqz v0, :cond_2

    if-ne v0, v2, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public static join(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "list"    # Ljava/util/List;
    .param p1, "delimiter"    # Ljava/lang/String;

    .prologue
    .line 815
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 817
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .local v0, "iter":Ljava/util/ListIterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 818
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 819
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 820
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 824
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static matchesKoreanNamePattern(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 917
    new-instance v1, Ljava/util/HashSet;

    const/16 v6, 0x11

    new-array v6, v6, [Ljava/lang/String;

    const-string/jumbo v7, "\uacfc"

    aput-object v7, v6, v4

    const-string/jumbo v7, "\ub791"

    aput-object v7, v6, v5

    const-string/jumbo v7, "\uc774"

    aput-object v7, v6, v9

    const-string/jumbo v7, "\ub97c"

    aput-object v7, v6, v10

    const-string/jumbo v7, "\uc744"

    aput-object v7, v6, v11

    const/4 v7, 0x5

    const-string/jumbo v8, "\uac00"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string/jumbo v8, "\uc528"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const-string/jumbo v8, "\uc640"

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string/jumbo v8, "\uc758"

    aput-object v8, v6, v7

    const/16 v7, 0x9

    const-string/jumbo v8, "\uc5d0"

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-string/jumbo v8, "\uc591"

    aput-object v8, v6, v7

    const/16 v7, 0xb

    const-string/jumbo v8, "\ub85c"

    aput-object v8, v6, v7

    const/16 v7, 0xc

    const-string/jumbo v8, "\uad70"

    aput-object v8, v6, v7

    const/16 v7, 0xd

    const-string/jumbo v8, "\ub3c4"

    aput-object v8, v6, v7

    const/16 v7, 0xe

    const-string/jumbo v8, "\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0xf

    const-string/jumbo v8, "\uc544"

    aput-object v8, v6, v7

    const/16 v7, 0x10

    const-string/jumbo v8, "\uc57c"

    aput-object v8, v6, v7

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 936
    .local v1, "familiarityList":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashSet;

    const/16 v6, 0x44

    new-array v6, v6, [Ljava/lang/String;

    const-string/jumbo v7, "\uc0ac\uc6d0"

    aput-object v7, v6, v4

    const-string/jumbo v7, "\ub300\ub9ac"

    aput-object v7, v6, v5

    const-string/jumbo v7, "\uacfc\uc7a5"

    aput-object v7, v6, v9

    const-string/jumbo v7, "\ucc28\uc7a5"

    aput-object v7, v6, v10

    const-string/jumbo v7, "\ubd80\uc7a5"

    aput-object v7, v6, v11

    const/4 v7, 0x5

    const-string/jumbo v8, "\uc120\uc784"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string/jumbo v8, "\ucc45\uc784"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const-string/jumbo v8, "\uc218\uc11d"

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string/jumbo v8, "\uc774\uc0ac"

    aput-object v8, v6, v7

    const/16 v7, 0x9

    const-string/jumbo v8, "\uc0c1\ubb34"

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-string/jumbo v8, "\uc804\ubb34"

    aput-object v8, v6, v7

    const/16 v7, 0xb

    const-string/jumbo v8, "\ubd80\uc0ac\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0xc

    const-string/jumbo v8, "\uc0ac\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0xd

    const-string/jumbo v8, "\ubd80\ud68c\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0xe

    const-string/jumbo v8, "\ud68c\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0xf

    const-string/jumbo v8, "\uc9c0\uc0ac\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x10

    const-string/jumbo v8, "\uac10\uc0ac"

    aput-object v8, v6, v7

    const/16 v7, 0x11

    const-string/jumbo v8, "\uace0\ubb38"

    aput-object v8, v6, v7

    const/16 v7, 0x12

    const-string/jumbo v8, "\uc790\ubb38"

    aput-object v8, v6, v7

    const/16 v7, 0x13

    const-string/jumbo v8, "\uc8fc\uc784"

    aput-object v8, v6, v7

    const/16 v7, 0x14

    const-string/jumbo v8, "\uc0ac\uc5c5\ubd80\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x15

    const-string/jumbo v8, "\ubcf8\ubd80\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x16

    const-string/jumbo v8, "\ubd80\ubcf8\ubd80\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x17

    const-string/jumbo v8, "\uad00\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x18

    const-string/jumbo v8, "\uad6d\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x19

    const-string/jumbo v8, "\uc18c\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x1a

    const-string/jumbo v8, "\uc9c0\uc810\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x1b

    const-string/jumbo v8, "\uc2e4\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x1c

    const-string/jumbo v8, "\ud300\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x1d

    const-string/jumbo v8, "\uacc4\uc7a5"

    aput-object v8, v6, v7

    const/16 v7, 0x1e

    const-string/jumbo v8, "\uc804\ubb38"

    aput-object v8, v6, v7

    const/16 v7, 0x1f

    const-string/jumbo v8, "\ud504\ub85c"

    aput-object v8, v6, v7

    const/16 v7, 0x20

    const-string/jumbo v8, "\uac10\ub3c5"

    aput-object v8, v6, v7

    const/16 v7, 0x21

    const-string/jumbo v8, "\ucf54\uce58"

    aput-object v8, v6, v7

    const/16 v7, 0x22

    const-string/jumbo v8, "\uc0ac\uc6d0\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x23

    const-string/jumbo v8, "\ub300\ub9ac\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x24

    const-string/jumbo v8, "\uacfc\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x25

    const-string/jumbo v8, "\ucc28\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x26

    const-string/jumbo v8, "\ubd80\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x27

    const-string/jumbo v8, "\uc120\uc784\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x28

    const-string/jumbo v8, "\ucc45\uc784\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x29

    const-string/jumbo v8, "\uc218\uc11d\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x2a

    const-string/jumbo v8, "\uc774\uc0ac\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x2b

    const-string/jumbo v8, "\uc0c1\ubb34\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x2c

    const-string/jumbo v8, "\uc804\ubb34\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x2d

    const-string/jumbo v8, "\ubd80\uc0ac\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x2e

    const-string/jumbo v8, "\uc0ac\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x2f

    const-string/jumbo v8, "\ubd80\ud68c\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x30

    const-string/jumbo v8, "\ud68c\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x31

    const-string/jumbo v8, "\uc9c0\uc0ac\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x32

    const-string/jumbo v8, "\uac10\uc0ac\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x33

    const-string/jumbo v8, "\uace0\ubb38\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x34

    const-string/jumbo v8, "\uc790\ubb38\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x35

    const-string/jumbo v8, "\uc0ac\uc5c5\ubd80\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x36

    const-string/jumbo v8, "\ubcf8\ubd80\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x37

    const-string/jumbo v8, "\ubd80\ubcf8\ubd80\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x38

    const-string/jumbo v8, "\uad00\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x39

    const-string/jumbo v8, "\uad6d\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x3a

    const-string/jumbo v8, "\uc18c\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x3b

    const-string/jumbo v8, "\uc9c0\uc810\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x3c

    const-string/jumbo v8, "\uc2e4\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x3d

    const-string/jumbo v8, "\ud300\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x3e

    const-string/jumbo v8, "\uacc4\uc7a5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x3f

    const-string/jumbo v8, "\uc8fc\uc784\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x40

    const-string/jumbo v8, "\uc804\ubb38\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x41

    const-string/jumbo v8, "\ud504\ub85c\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x42

    const-string/jumbo v8, "\uac10\ub3c5\ub2d8"

    aput-object v8, v6, v7

    const/16 v7, 0x43

    const-string/jumbo v8, "\ucf54\uce58\ub2d8"

    aput-object v8, v6, v7

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1007
    .local v0, "commonTitles":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    .line 1009
    const-string/jumbo v6, "\\s+"

    const-string/jumbo v7, ""

    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1012
    :cond_0
    if-eqz p0, :cond_6

    if-eqz p1, :cond_6

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v9, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v9, :cond_6

    invoke-virtual {p1, p0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 1018
    invoke-virtual {p1, p0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1020
    .local v2, "nameStartPos":I
    if-gez v2, :cond_1

    move v2, v4

    .line 1021
    :cond_1
    add-int/lit8 v6, v2, 0x2

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1022
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1040
    .end local v2    # "nameStartPos":I
    .end local v3    # "title":Ljava/lang/String;
    :cond_2
    :goto_0
    return v5

    .line 1025
    .restart local v2    # "nameStartPos":I
    .restart local v3    # "title":Ljava/lang/String;
    :cond_3
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1028
    invoke-virtual {p1, p0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x3

    if-ne v6, v7, :cond_5

    .line 1029
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->symbolIsKorean(C)Z

    move-result v6

    if-nez v6, :cond_4

    move v4, v5

    :cond_4
    move v5, v4

    goto :goto_0

    :cond_5
    move v5, v4

    .line 1031
    goto :goto_0

    .line 1033
    .end local v2    # "nameStartPos":I
    .end local v3    # "title":Ljava/lang/String;
    :cond_6
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1, p0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v5, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v5, :cond_2

    move v5, v4

    .line 1038
    goto :goto_0
.end method

.method public static nameIsJapanese(Ljava/lang/String;)Z
    .locals 4
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 1092
    const/4 v1, 0x0

    .line 1093
    .local v1, "retVal":Z
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    .line 1102
    .end local v1    # "retVal":Z
    :cond_0
    :goto_0
    return v1

    .line 1097
    .restart local v1    # "retVal":Z
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 1098
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->symbolIsJapanese(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1099
    const/4 v1, 0x1

    goto :goto_0

    .line 1097
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1102
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static nameIsKorean(Ljava/lang/String;)Z
    .locals 4
    .param p0, "args"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1057
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->removeSpecialChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1058
    .local v0, "query":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1059
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1060
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->symbolIsKorean(C)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static partialStringMatch(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 14
    .param p0, "s1"    # Ljava/lang/String;
    .param p1, "s2"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 606
    if-eqz p0, :cond_0

    if-nez p1, :cond_3

    .line 607
    :cond_0
    if-nez p0, :cond_2

    if-nez p1, :cond_2

    .line 627
    :cond_1
    :goto_0
    return v10

    :cond_2
    move v10, v11

    .line 607
    goto :goto_0

    .line 610
    :cond_3
    const-string/jumbo v12, "\\s+"

    invoke-virtual {p0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 611
    .local v8, "s1a":[Ljava/lang/String;
    const-string/jumbo v12, "\\s+"

    invoke-virtual {p1, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 613
    .local v9, "s2a":[Ljava/lang/String;
    move-object v1, v8

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v5, v4

    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_7

    aget-object v0, v1, v5

    .line 614
    .local v0, "a":Ljava/lang/String;
    move-object v2, v9

    .local v2, "arr$":[Ljava/lang/String;
    array-length v7, v2

    .local v7, "len$":I
    const/4 v4, 0x0

    .end local v5    # "i$":I
    .restart local v4    # "i$":I
    :goto_2
    if-ge v4, v7, :cond_6

    aget-object v3, v2, v4

    .line 615
    .local v3, "b":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v13

    if-ge v12, v13, :cond_5

    .line 616
    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 614
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 620
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    goto :goto_0

    .line 613
    .end local v3    # "b":Ljava/lang/String;
    :cond_6
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    .end local v4    # "i$":I
    .restart local v5    # "i$":I
    goto :goto_1

    .end local v0    # "a":Ljava/lang/String;
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v7    # "len$":I
    :cond_7
    move v10, v11

    .line 627
    goto :goto_0
.end method

.method public static removeFromEnd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "strToRemove"    # Ljava/lang/String;

    .prologue
    .line 466
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 467
    invoke-virtual {p0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 469
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 470
    return-object p0
.end method

.method public static removePossessives(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 666
    if-nez p0, :cond_0

    .line 669
    .end local p0    # "name":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "name":Ljava/lang/String;
    :cond_0
    const-string/jumbo v0, "\'s"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "s\'"

    const-string/jumbo v2, "s"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static removeSpecialChar(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 1123
    if-eqz p0, :cond_0

    .line 1124
    const-string/jumbo v0, "[\\u003a-\\u0040]"

    const-string/jumbo v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1125
    const-string/jumbo v0, "[\\u0021-\\u002f]"

    const-string/jumbo v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1126
    const-string/jumbo v0, "[\\u005b-\\u0060]"

    const-string/jumbo v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1127
    const-string/jumbo v0, "[\\u007b-\\u00bf]"

    const-string/jumbo v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1128
    const-string/jumbo v0, "[\\u2000-\\u27ff]"

    const-string/jumbo v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1129
    const-string/jumbo v0, "[\\u3000-\\u3040]"

    const-string/jumbo v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1130
    const-string/jumbo v0, "[\\u00d7]"

    const-string/jumbo v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1131
    const-string/jumbo v0, "[\\u00f7]"

    const-string/jumbo v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1132
    const-string/jumbo v0, "^\\s+"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1133
    const-string/jumbo v0, "\\s+$"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1135
    :cond_0
    return-object p0
.end method

.method public static final removeTopChoice([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p0, "choices"    # [Ljava/lang/String;

    .prologue
    .line 135
    if-eqz p0, :cond_0

    array-length v3, p0

    if-nez v3, :cond_2

    .line 136
    :cond_0
    const/4 v1, 0x0

    .line 142
    :cond_1
    return-object v1

    .line 137
    :cond_2
    array-length v3, p0

    add-int/lit8 v2, v3, -0x1

    .line 138
    .local v2, "size":I
    new-array v1, v2, [Ljava/lang/String;

    .line 139
    .local v1, "ret":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 140
    add-int/lit8 v3, v0, 0x1

    aget-object v3, p0, v3

    aput-object v3, v1, v0

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "searchStr"    # Ljava/lang/String;
    .param p2, "replacementStr"    # Ljava/lang/String;

    .prologue
    .line 191
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 194
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 195
    .local v2, "searchStringPos":I
    const/4 v3, 0x0

    .line 196
    .local v3, "startPos":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 199
    .local v1, "searchStringLength":I
    :goto_0
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 200
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 201
    add-int v3, v2, v1

    .line 202
    invoke-virtual {p0, p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    .line 206
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 208
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static replaceHostname(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "newHostname"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    .line 398
    const-string/jumbo v3, "://"

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 399
    .local v2, "slashPos":I
    if-ne v2, v5, :cond_0

    .line 400
    const-string/jumbo v3, ""

    .line 411
    :goto_0
    return-object v3

    .line 401
    :cond_0
    const-string/jumbo v3, "/"

    add-int/lit8 v4, v2, 0x3

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 402
    .local v1, "endSlashPos":I
    if-ne v1, v5, :cond_1

    .line 403
    const-string/jumbo v3, ";"

    add-int/lit8 v4, v2, 0x3

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 404
    if-ne v1, v5, :cond_1

    .line 405
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 408
    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 409
    .local v0, "buff":Ljava/lang/StringBuffer;
    add-int/lit8 v3, v2, 0x3

    invoke-virtual {v0, v3, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 410
    add-int/lit8 v3, v2, 0x3

    invoke-virtual {v0, v3, p1}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 411
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static replaceTokens(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 4
    .param p0, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 675
    .local p1, "replacements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v3, Lcom/vlingo/core/internal/util/StringUtils;->VAR_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 676
    .local v1, "matcher":Ljava/util/regex/Matcher;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 677
    .local v0, "buffer":Ljava/lang/StringBuffer;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 678
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 679
    .local v2, "replacement":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 680
    const-string/jumbo v3, ""

    invoke-virtual {v1, v0, v3}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 681
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 684
    .end local v2    # "replacement":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 685
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static split(Ljava/lang/String;C)[Ljava/lang/String;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "delimiter"    # C

    .prologue
    .line 418
    if-nez p0, :cond_0

    .line 419
    const/4 v0, 0x0

    .line 433
    :goto_0
    return-object v0

    .line 420
    :cond_0
    const/4 v1, 0x0

    .line 421
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 422
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, p1, :cond_1

    .line 423
    add-int/lit8 v1, v1, 0x1

    .line 421
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 425
    :cond_2
    add-int/lit8 v5, v1, 0x1

    new-array v0, v5, [Ljava/lang/String;

    .line 426
    .local v0, "arr":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 427
    .local v3, "index":I
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v1, :cond_3

    .line 428
    invoke-virtual {p0, p1, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 429
    .local v4, "pos":I
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    .line 430
    add-int/lit8 v3, v4, 0x1

    .line 427
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 432
    .end local v4    # "pos":I
    :cond_3
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v1

    goto :goto_0
.end method

.method public static stringHasLatinSymbols(Ljava/lang/String;)Z
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 1115
    const-string/jumbo v0, ".*[a-zA-Z].*"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static stringIsNonAsciiWithCase(Ljava/lang/String;)Z
    .locals 3
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 835
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 836
    sget-object v1, Lcom/vlingo/core/internal/util/StringUtils;->NON_ASCII_WITH_CASE_UNICODE_BLOCKS:Ljava/util/Set;

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 837
    const/4 v1, 0x1

    .line 840
    :goto_1
    return v1

    .line 835
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 840
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static symbolIsJapanese(C)Z
    .locals 2
    .param p0, "ch"    # C

    .prologue
    .line 1111
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HIRAGANA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v1

    if-eq v0, v1, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->KATAKANA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static symbolIsKorean(C)Z
    .locals 2
    .param p0, "ch"    # C

    .prologue
    .line 1082
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HANGUL_SYLLABLES:Ljava/lang/Character$UnicodeBlock;

    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static unescapeXml(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 591
    if-nez p0, :cond_0

    .line 592
    const/4 v3, 0x0

    .line 598
    :goto_0
    return-object v3

    .line 595
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/StringWriter;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/io/StringWriter;-><init>(I)V

    .line 596
    .local v2, "writer":Ljava/io/StringWriter;
    new-instance v1, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;

    sget-object v3, Lcom/vlingo/core/internal/util/StringUtils;->XML_ESCAPE:[[Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;-><init>([[Ljava/lang/CharSequence;)V

    .line 597
    .local v1, "translator":Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;
    invoke-virtual {v1, p0, v2}, Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;->translate(Ljava/lang/CharSequence;Ljava/io/Writer;)V

    .line 598
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 599
    .end local v1    # "translator":Lcom/vlingo/core/internal/util/StringUtils$LookupTranslator;
    .end local v2    # "writer":Ljava/io/StringWriter;
    :catch_0
    move-exception v0

    .line 601
    .local v0, "ioe":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method
