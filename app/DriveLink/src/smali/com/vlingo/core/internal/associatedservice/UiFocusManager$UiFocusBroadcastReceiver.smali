.class public Lcom/vlingo/core/internal/associatedservice/UiFocusManager$UiFocusBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UiFocusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UiFocusBroadcastReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 315
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v3, "id"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 317
    .local v2, "remoteId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 319
    .local v1, "localId":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "UiFocusBroadcastReceiver() remoteId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", localId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", action="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", HasUiFocus="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->hasUiFocus()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v3

    if-nez v3, :cond_4

    .line 327
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 330
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v3

    if-nez v3, :cond_4

    .line 331
    const-string/jumbo v3, "com.vlingo.core.action.REQUEST_UIFOCUS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 332
    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    invoke-static {p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$000(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v3

    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->setRemoteHasUiFocus(Landroid/content/Context;Z)V
    invoke-static {v3, p1, v6}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$100(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Landroid/content/Context;Z)V

    goto :goto_0

    .line 334
    :cond_2
    const-string/jumbo v3, "com.vlingo.core.action.ABANDONED_UIFOCUS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 335
    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    invoke-static {p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$000(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v3

    const/4 v4, 0x0

    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->setRemoteHasUiFocus(Landroid/content/Context;Z)V
    invoke-static {v3, p1, v4}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$100(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 337
    :cond_3
    const-string/jumbo v3, "com.vlingo.core.action.ACQUIRED_UIFOCUS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 338
    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    invoke-static {p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$000(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v3

    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->setRemoteHasUiFocus(Landroid/content/Context;Z)V
    invoke-static {v3, p1, v6}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$100(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 344
    :cond_4
    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    invoke-static {p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$000(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v3

    # setter for: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mRemoteId:Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$202(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Ljava/lang/String;)Ljava/lang/String;

    .line 345
    const-string/jumbo v3, "com.vlingo.core.action.REQUEST_UIFOCUS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 346
    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    invoke-static {p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$000(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v3

    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->onRemoteUiFocusRequest(Landroid/content/Context;Ljava/lang/String;)V
    invoke-static {v3, p1, v2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$300(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 348
    :cond_5
    const-string/jumbo v3, "com.vlingo.core.action.ABANDONED_UIFOCUS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 349
    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    invoke-static {p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$000(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v3

    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->onRemoteUiFocusAbandoned(Landroid/content/Context;Ljava/lang/String;)V
    invoke-static {v3, p1, v2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$400(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 351
    :cond_6
    const-string/jumbo v3, "com.vlingo.core.action.ACQUIRED_UIFOCUS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 352
    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    invoke-static {p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$000(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v3

    # invokes: Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->onRemoteUiFocusAcquired(Landroid/content/Context;Ljava/lang/String;)V
    invoke-static {v3, p1, v2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->access$500(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
