.class public Lcom/vlingo/core/internal/phrasespotter/ForwardingPhraseSpotterListener;
.super Ljava/lang/Object;
.source "ForwardingPhraseSpotterListener.java"

# interfaces
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;


# instance fields
.field private target:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/ForwardingPhraseSpotterListener;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseDetected(Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/ForwardingPhraseSpotterListener;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotted(Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method public onPhraseSpotterStarted()V
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/ForwardingPhraseSpotterListener;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotterStarted()V

    .line 12
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/ForwardingPhraseSpotterListener;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotterStopped()V

    .line 16
    return-void
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/ForwardingPhraseSpotterListener;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    .line 28
    return-void
.end method

.method public target(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)Lcom/vlingo/core/internal/phrasespotter/ForwardingPhraseSpotterListener;
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/ForwardingPhraseSpotterListener;->target:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .line 32
    return-object p0
.end method

.method public target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/ForwardingPhraseSpotterListener;->target:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    return-object v0
.end method
