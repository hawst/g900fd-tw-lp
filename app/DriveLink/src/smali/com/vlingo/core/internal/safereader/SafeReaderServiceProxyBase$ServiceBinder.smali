.class public Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;
.super Ljava/lang/Object;
.source "SafeReaderServiceProxyBase.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ServiceBinder"
.end annotation


# instance fields
.field mCallback:Landroid/content/ServiceConnection;


# direct methods
.method constructor <init>(Landroid/content/ServiceConnection;)V
    .locals 0
    .param p1, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    iput-object p1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    .line 280
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 287
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 292
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 295
    :cond_0
    return-void
.end method
