.class public final Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
.super Ljava/lang/Object;
.source "PhraseSpotter.java"

# interfaces
.implements Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSSharedPreferenceChangeListener;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;
    }
.end annotation


# static fields
.field private static final BATTERY_CHANGED_FILTER:Landroid/content/IntentFilter;

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;


# instance fields
.field private volatile audioPlaying:Z

.field private isCharging:Z

.field private listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

.field private final phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

.field private phraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

.field private final psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

.field private final psSharedPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private volatile recoInProgress:Z

.field private volatile restartWhenStopped:Z

.field private volatile spottingRequested:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    const-class v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    .line 48
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->BATTERY_CHANGED_FILTER:Landroid/content/IntentFilter;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    .line 64
    new-instance v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-direct {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .line 66
    new-instance v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSSharedPreferenceChangeListener;

    invoke-direct {v1, p0, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSSharedPreferenceChangeListener;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psSharedPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 68
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psSharedPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 71
    new-instance v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    invoke-direct {v1, p0, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    .line 72
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->register(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 73
    .local v0, "i":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 74
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging(Landroid/content/Intent;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging:Z

    .line 80
    :cond_0
    return-void
.end method

.method static synthetic access$1000()Landroid/content/IntentFilter;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->BATTERY_CHANGED_FILTER:Landroid/content/IntentFilter;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Landroid/content/Intent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updatePhraseSpotterState()V

    return-void
.end method

.method static synthetic access$1302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->audioPlaying:Z

    return p1
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->spottingRequested:Z

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging:Z

    return v0
.end method

.method static synthetic access$402(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging:Z

    return p1
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->recoInProgress:Z

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->startPhraseSpottingInternal(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpottingInternal()V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    return v0
.end method

.method static synthetic access$802(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    return p1
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    return-object v0
.end method

.method private declared-synchronized asyncStopPhraseSpottingWithResumingControl(Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Z
    .locals 2
    .param p1, "control"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 280
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getState()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;->IDLE:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->is(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$State;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->disableSpottingRequest()V

    .line 284
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stop(Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    const/4 v0, 0x1

    .line 287
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 280
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized destroy()V
    .locals 4

    .prologue
    .line 97
    const-class v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    if-eqz v0, :cond_0

    .line 101
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 102
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    iget-object v2, v2, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psSharedPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 103
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    sget-object v3, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 104
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :cond_0
    monitor-exit v1

    return-void

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private disableSpottingRequest()V
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->spottingRequested:Z

    .line 276
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setDialogFlowTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 277
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .locals 2

    .prologue
    .line 57
    const-class v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-direct {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .line 60
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isCharging(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 307
    const-string/jumbo v1, "plugged"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 308
    .local v0, "plugged":I
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private startPhraseSpottingInternal(Z)V
    .locals 4
    .param p1, "registerTaskRegulator"    # Z

    .prologue
    .line 244
    sget-object v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startPhraseSpottingInternal()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isIUXComplete()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 248
    if-eqz p1, :cond_0

    .line 249
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v1, v2, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->registerTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 251
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v1, v2, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_1
    :goto_0
    return-void

    .line 253
    :catch_0
    move-exception v0

    .line 254
    .local v0, "e":Ljava/lang/Exception;
    const-class v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error starting PhraseSpotterControl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private stopPhraseSpottingInternal()V
    .locals 2

    .prologue
    .line 261
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopPhraseSpottingInternal()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stop()V

    .line 264
    return-void
.end method

.method private updatePhraseSpotterState()V
    .locals 1

    .prologue
    .line 214
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 241
    return-void
.end method


# virtual methods
.method public declared-synchronized init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 1
    .param p1, "spotterParameters"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "spotterListener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .line 85
    iput-object p2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    monitor-exit p0

    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isListening()Z
    .locals 1

    .prologue
    .line 185
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isSpotting()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 343
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$3;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$3;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 349
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 353
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$4;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$4;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 359
    return-void
.end method

.method public onPhraseSpotterStarted()V
    .locals 2

    .prologue
    .line 315
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    if-nez v0, :cond_0

    .line 316
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPhraseSpotterStarted: restartWhenStopped==false, so calling listener."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotterStarted()V

    .line 319
    :cond_0
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 1

    .prologue
    .line 325
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 339
    return-void
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 363
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 369
    return-void
.end method

.method public onTaskWaitingToStart(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    .locals 2
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;
    .param p2, "control"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 293
    const/4 v0, 0x1

    .line 294
    .local v0, "resumeControlImmediately":Z
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->asyncStopPhraseSpottingWithResumingControl(Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 296
    const/4 v0, 0x0

    .line 300
    :cond_0
    if-eqz v0, :cond_1

    .line 301
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 303
    :cond_1
    return-void
.end method

.method public declared-synchronized startPhraseSpotting()V
    .locals 2

    .prologue
    .line 143
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startPhraseSpotting()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->supportsSVoiceAssociatedServiceOnly()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    :goto_0
    monitor-exit p0

    return-void

    .line 148
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->spottingRequested:Z

    .line 149
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setStopPhraseSpotterRightNow(Z)V

    .line 155
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    .line 158
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updatePhraseSpotterState()V

    .line 161
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->register(Landroid/content/Context;)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopPhraseSpotting()V
    .locals 2

    .prologue
    .line 172
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopPhraseSpotting()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    .line 176
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->unregister(Landroid/content/Context;)V

    .line 179
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->disableSpottingRequest()V

    .line 180
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpottingInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    monitor-exit p0

    return-void

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public stopPhraseSpottingRightNow()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setStopPhraseSpotterRightNow(Z)V

    .line 272
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PS: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; control=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; adapter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized updateAudioSettings(Z)V
    .locals 1
    .param p1, "forceSpotterToRun"    # Z

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->spottingRequested:Z

    if-eqz v0, :cond_1

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    .line 124
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpottingInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 126
    :cond_1
    if-eqz p1, :cond_0

    .line 127
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->startPhraseSpotting()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized updateParameters(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)V
    .locals 1
    .param p1, "spotterParameters"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updateAudioSettings(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    monitor-exit p0

    return-void

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
