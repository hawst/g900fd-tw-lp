.class Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$3;
.super Ljava/lang/Object;
.source "LocalSearchHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->showFailure()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$3;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 155
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$3;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCurrentSpokenLocation()Ljava/lang/String;

    move-result-object v1

    .line 156
    .local v1, "search":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 157
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$3;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->access$600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 159
    :cond_0
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->access$700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 160
    .local v0, "prompt":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$3;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_MAIN_WSEARCH_PROMPT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 161
    return-void
.end method
