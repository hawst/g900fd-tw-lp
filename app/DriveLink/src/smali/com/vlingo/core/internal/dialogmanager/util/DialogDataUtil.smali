.class public final Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;
.super Ljava/lang/Object;
.source "DialogDataUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;
    }
.end annotation


# static fields
.field public static final LIST_POSITION_CHANGE_PARAM:Ljava/lang/String; = "ListPositionChange"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static displayScheduleEventList(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/sdk/recognition/VLAction;Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 6
    .param p0, "vvsListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p3, "widgetListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 212
    .local p2, "allEvents":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    const-string/jumbo v3, "choices"

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 213
    const/4 v0, 0x0

    .line 215
    .local v0, "choices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :try_start_0
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getChoicesAsInts(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 219
    :goto_0
    if-nez v0, :cond_0

    .line 228
    .end local v0    # "choices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_1
    return-void

    .line 216
    .restart local v0    # "choices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catch_0
    move-exception v1

    .line 217
    .local v1, "e":Lorg/json/JSONException;
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "displayScheduleEventList: JSONException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 222
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_0
    invoke-static {p2, v0}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->extractByPosition(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 223
    .local v2, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    if-eqz v2, :cond_1

    .line 224
    move-object p2, v2

    .line 227
    .end local v0    # "choices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v2    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEventChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v4, 0x0

    invoke-interface {p0, v3, v4, p2, p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_1
.end method

.method public static extractByPosition(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 185
    .local p0, "objects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "order":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 186
    .local v1, "extractedObjects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez p0, :cond_1

    .line 187
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "objects array is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    return-object v1

    .line 190
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 192
    .local v3, "position":Ljava/lang/Integer;
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Server/Client state mismatch in extractByPosition"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getChoicesAsInts(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    .locals 7
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 98
    const-string/jumbo v6, "choices"

    invoke-interface {p0, v6}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "choicesText":Ljava/lang/String;
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 100
    .local v4, "json":Lorg/json/JSONObject;
    const-string/jumbo v6, "id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 101
    .local v0, "choiceArray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v5

    .line 102
    .local v5, "len":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 103
    .local v1, "choices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_0

    .line 104
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 106
    :cond_0
    return-object v1
.end method

.method public static getChoicesAsStrings(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    .locals 7
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 110
    const-string/jumbo v6, "choices"

    invoke-interface {p0, v6}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 111
    .local v2, "choicesText":Ljava/lang/String;
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 113
    .local v4, "json":Lorg/json/JSONObject;
    const-string/jumbo v6, "id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 114
    .local v0, "choiceArray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v5

    .line 115
    .local v5, "len":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 116
    .local v1, "choices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_0

    .line 117
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 119
    :cond_0
    return-object v1
.end method

.method public static getContactDataFromId(Ljava/util/List;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;
    .locals 7
    .param p1, "contactAndAddressIndex"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/vlingo/core/internal/contacts/ContactData;"
        }
    .end annotation

    .prologue
    .local p0, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v6, 0x0

    .line 46
    if-nez p0, :cond_0

    move-object v5, v6

    .line 60
    :goto_0
    return-object v5

    .line 48
    :cond_0
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->splitAddressFromContactId(Ljava/lang/String;)[I

    move-result-object v3

    .line 49
    .local v3, "ids":[I
    const/4 v5, 0x0

    aget v1, v3, v5

    .line 50
    .local v1, "contactIndex":I
    const/4 v5, 0x1

    aget v0, v3, v5

    .line 51
    .local v0, "addressIndex":I
    invoke-static {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getContactMatchFromId(Ljava/util/List;I)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v4

    .line 52
    .local v4, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v4, :cond_1

    .line 54
    :try_start_0
    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactData;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 56
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    move-object v5, v6

    .line 57
    goto :goto_0

    .end local v2    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_1
    move-object v5, v6

    .line 60
    goto :goto_0
.end method

.method public static getContactMatchFromId(Ljava/util/List;I)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 4
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;I)",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;"
        }
    .end annotation

    .prologue
    .local p0, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v2, 0x0

    .line 65
    if-nez p0, :cond_0

    move-object v1, v2

    .line 75
    :goto_0
    return-object v1

    .line 69
    :cond_0
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactMatch;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 73
    const/4 v1, 0x0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 75
    goto :goto_0
.end method

.method public static getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;
    .locals 7
    .param p0, "from"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 146
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-object v5

    .line 148
    :cond_1
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 151
    .local v3, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string/jumbo v6, "numbers"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 157
    .local v0, "choiceArray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 158
    .local v4, "len":I
    new-instance v5, Ljava/util/LinkedHashSet;

    invoke-direct {v5, v4}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 159
    .local v5, "recipients":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_0

    .line 160
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 159
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 153
    .end local v0    # "choiceArray":Lorg/json/JSONArray;
    .end local v2    # "i":I
    .end local v4    # "len":I
    .end local v5    # "recipients":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 154
    .local v1, "e":Lorg/json/JSONException;
    goto :goto_0
.end method

.method public static getMessageRecipientContacts(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;
    .locals 8
    .param p1, "from"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .local p0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v6, 0x0

    .line 123
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-object v6

    .line 125
    :cond_1
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 128
    .local v4, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string/jumbo v7, "contacts"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 134
    .local v0, "choiceArray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v5

    .line 135
    .local v5, "len":I
    new-instance v6, Ljava/util/LinkedHashSet;

    invoke-direct {v6, v5}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 136
    .local v6, "recipients":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_0

    .line 137
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getContactDataFromId(Ljava/util/List;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v1

    .line 138
    .local v1, "data":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v1, :cond_2

    .line 139
    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 130
    .end local v0    # "choiceArray":Lorg/json/JSONArray;
    .end local v1    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v3    # "i":I
    .end local v5    # "len":I
    .end local v6    # "recipients":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :catch_0
    move-exception v2

    .line 131
    .local v2, "e":Lorg/json/JSONException;
    goto :goto_0
.end method

.method public static getPrunedList(Ljava/util/List;IZ)Ljava/util/List;
    .locals 4
    .param p1, "number"    # I
    .param p2, "fromBack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;IZ)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 166
    .local p0, "from":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-eqz p0, :cond_0

    const/4 v3, 0x1

    if-ge p1, v3, :cond_2

    :cond_0
    const/4 p0, 0x0

    .line 180
    .end local p0    # "from":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_1
    :goto_0
    return-object p0

    .line 168
    .restart local p0    # "from":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 169
    .local v1, "length":I
    if-ge p1, v1, :cond_1

    .line 171
    const/4 v2, 0x0

    .line 172
    .local v2, "start":I
    move v0, v1

    .line 173
    .local v0, "end":I
    if-eqz p2, :cond_3

    .line 174
    sub-int v2, v0, p1

    .line 180
    :goto_1
    invoke-interface {p0, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 177
    :cond_3
    move v0, p1

    goto :goto_1
.end method

.method public static splitAddressFromContactId(Ljava/lang/String;)[I
    .locals 6
    .param p0, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 80
    const/16 v3, 0x2e

    invoke-static {p0, v3}, Lcom/vlingo/core/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "ids":[Ljava/lang/String;
    array-length v3, v1

    if-eq v3, v4, :cond_0

    .line 82
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Expected Contact Address id to be of form <contact_id>.<address_id>: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    new-instance v3, Ljava/security/InvalidParameterException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Expected Contact Address id to be of form <contact_id>.<address_id>: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 85
    :cond_0
    new-array v2, v4, [I

    .line 87
    .local v2, "result":[I
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    aget-object v4, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v2, v3

    .line 88
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v2, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    return-object v2

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Expected Contact Address id to be of form <contact_id>.<address_id>: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    new-instance v3, Ljava/security/InvalidParameterException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Expected Contact Address id to be of form <contact_id>.<address_id>: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
