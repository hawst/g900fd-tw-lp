.class public final enum Lcom/vlingo/core/internal/audio/AudioType;
.super Ljava/lang/Enum;
.source "AudioType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/audio/AudioType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/audio/AudioType;

.field public static final enum PCM_22k:Lcom/vlingo/core/internal/audio/AudioType;

.field public static final enum PCM_44k:Lcom/vlingo/core/internal/audio/AudioType;


# instance fields
.field public final bytesPerSample:I

.field public final channel:I

.field public final encoding:I

.field public final frequency:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v5, 0x4

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 6
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioType;

    const-string/jumbo v1, "PCM_22k"

    const/16 v3, 0x5622

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/audio/AudioType;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioType;->PCM_22k:Lcom/vlingo/core/internal/audio/AudioType;

    .line 7
    new-instance v6, Lcom/vlingo/core/internal/audio/AudioType;

    const-string/jumbo v7, "PCM_44k"

    const v9, 0xac44

    move v10, v4

    move v11, v5

    move v12, v4

    invoke-direct/range {v6 .. v12}, Lcom/vlingo/core/internal/audio/AudioType;-><init>(Ljava/lang/String;IIIII)V

    sput-object v6, Lcom/vlingo/core/internal/audio/AudioType;->PCM_44k:Lcom/vlingo/core/internal/audio/AudioType;

    .line 5
    new-array v0, v4, [Lcom/vlingo/core/internal/audio/AudioType;

    sget-object v1, Lcom/vlingo/core/internal/audio/AudioType;->PCM_22k:Lcom/vlingo/core/internal/audio/AudioType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/audio/AudioType;->PCM_44k:Lcom/vlingo/core/internal/audio/AudioType;

    aput-object v1, v0, v8

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioType;->$VALUES:[Lcom/vlingo/core/internal/audio/AudioType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0
    .param p3, "frequency"    # I
    .param p4, "encoding"    # I
    .param p5, "channel"    # I
    .param p6, "bytesPerSample"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII)V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 16
    iput p3, p0, Lcom/vlingo/core/internal/audio/AudioType;->frequency:I

    .line 17
    iput p4, p0, Lcom/vlingo/core/internal/audio/AudioType;->encoding:I

    .line 18
    iput p5, p0, Lcom/vlingo/core/internal/audio/AudioType;->channel:I

    .line 19
    iput p6, p0, Lcom/vlingo/core/internal/audio/AudioType;->bytesPerSample:I

    .line 20
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/AudioType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/vlingo/core/internal/audio/AudioType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/audio/AudioType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioType;->$VALUES:[Lcom/vlingo/core/internal/audio/AudioType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/audio/AudioType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/audio/AudioType;

    return-object v0
.end method
