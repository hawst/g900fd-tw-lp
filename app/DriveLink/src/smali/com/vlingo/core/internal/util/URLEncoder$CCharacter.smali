.class Lcom/vlingo/core/internal/util/URLEncoder$CCharacter;
.super Ljava/lang/Object;
.source "URLEncoder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/URLEncoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CCharacter"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static forDigit(II)C
    .locals 2
    .param p0, "digit"    # I
    .param p1, "radix"    # I

    .prologue
    const/4 v0, 0x0

    .line 115
    if-ge p0, p1, :cond_0

    if-gez p0, :cond_1

    .line 124
    :cond_0
    :goto_0
    return v0

    .line 118
    :cond_1
    const/4 v1, 0x2

    if-lt p1, v1, :cond_0

    const/16 v1, 0x24

    if-gt p1, v1, :cond_0

    .line 121
    const/16 v0, 0xa

    if-ge p0, v0, :cond_2

    .line 122
    add-int/lit8 v0, p0, 0x30

    int-to-char v0, v0

    goto :goto_0

    .line 124
    :cond_2
    add-int/lit8 v0, p0, 0x57

    int-to-char v0, v0

    goto :goto_0
.end method
