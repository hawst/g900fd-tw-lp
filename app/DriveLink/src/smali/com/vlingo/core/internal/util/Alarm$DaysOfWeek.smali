.class public final enum Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;
.super Ljava/lang/Enum;
.source "Alarm.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/Alarm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DaysOfWeek"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

.field public static final enum FRIDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

.field public static final enum MONDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

.field public static final enum SATURDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

.field public static final enum SUNDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

.field public static final enum THURSDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

.field public static final enum TUESDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

.field public static final enum WEDNESDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 82
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    const-string/jumbo v1, "SUNDAY"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->SUNDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    .line 83
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    const-string/jumbo v1, "MONDAY"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->MONDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    .line 84
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    const-string/jumbo v1, "TUESDAY"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->TUESDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    .line 85
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    const-string/jumbo v1, "WEDNESDAY"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->WEDNESDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    .line 86
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    const-string/jumbo v1, "THURSDAY"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->THURSDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    .line 87
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    const-string/jumbo v1, "FRIDAY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->FRIDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    .line 88
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    const-string/jumbo v1, "SATURDAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->SATURDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    .line 81
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->SUNDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->MONDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->TUESDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->WEDNESDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->THURSDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->FRIDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->SATURDAY:Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->$VALUES:[Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    const-class v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->$VALUES:[Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/util/Alarm$DaysOfWeek;

    return-object v0
.end method
