.class public Lcom/vlingo/core/internal/util/DialUtil;
.super Ljava/lang/Object;
.source "DialUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/DialUtil$ContactDataComparator;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static bluetoothOn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/util/DialUtil;->bluetoothOn:Z

    .line 52
    const-class v0, Lcom/vlingo/core/internal/util/DialUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/DialUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 276
    return-void
.end method

.method public static dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .param p3, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 153
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Z)V

    .line 154
    return-void
.end method

.method public static dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .param p3, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p4, "isVideoCall"    # Z

    .prologue
    .line 133
    if-eqz p4, :cond_0

    .line 134
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VIDEO_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;->address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;->setContext(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setListener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setActionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->queue()V

    .line 150
    :goto_0
    return-void

    .line 143
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VOICE_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;->address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;->setContext(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setListener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setActionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->queue()V

    goto :goto_0
.end method

.method public static filterContactDataByType(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    .local p0, "contactInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 219
    .local v2, "filteredResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const-string/jumbo v4, "call"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 231
    .end local p0    # "contactInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :goto_0
    return-object p0

    .line 223
    .restart local p0    # "contactInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_0
    const-string/jumbo v4, "\\d"

    const-string/jumbo v5, ""

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 225
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 226
    .local v0, "data":Lcom/vlingo/core/internal/contacts/ContactData;
    iget v4, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-static {v4}, Lcom/vlingo/core/internal/util/ContactUtil;->getTypeStringEN(I)Ljava/lang/String;

    move-result-object v1

    .line 227
    .local v1, "dataType":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 228
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .end local v0    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v1    # "dataType":Ljava/lang/String;
    :cond_2
    move-object p0, v2

    .line 231
    goto :goto_0
.end method

.method private static getContactId(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "phoneNumber"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 95
    const/4 v6, 0x0

    .line 96
    .local v6, "contactId":Ljava/lang/String;
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v2, v6

    .line 129
    :cond_0
    :goto_0
    return-object v2

    .line 101
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 104
    .local v0, "mResolver":Landroid/content/ContentResolver;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 107
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 110
    .local v1, "uri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 112
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x2

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 114
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 116
    :cond_2
    const-string/jumbo v2, "_id"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 118
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 125
    :cond_3
    if-eqz v7, :cond_4

    .line 126
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v2, v6

    .line 129
    goto :goto_0

    .line 120
    :catch_0
    move-exception v8

    .line 125
    .local v8, "iae":Ljava/lang/IllegalArgumentException;
    if-eqz v7, :cond_5

    .line 126
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v2, v6

    goto :goto_0

    .line 125
    .end local v8    # "iae":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v2

    if-eqz v7, :cond_6

    .line 126
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v2
.end method

.method public static getDefaultNumber(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 207
    .local p0, "contactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v0, 0x0

    .line 208
    .local v0, "defaultNumber":Ljava/lang/String;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 209
    .local v2, "phoneInfo":Lcom/vlingo/core/internal/contacts/ContactData;
    iget v3, v2, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    if-lez v3, :cond_0

    .line 210
    iget-object v0, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    goto :goto_0

    .line 213
    .end local v2    # "phoneInfo":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_1
    return-object v0
.end method

.method public static getDialIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p0, "address"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 68
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 70
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.CALL"

    const-string/jumbo v3, "tel"

    const/4 v4, 0x0

    invoke-static {v3, p0, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 71
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 72
    const-string/jumbo v2, "call_from_svoice"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 74
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 75
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v2

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/vlingo/core/internal/util/DialUtil;->bluetoothOn:Z

    if-nez v2, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-le v2, v3, :cond_0

    .line 76
    const-string/jumbo v2, "android.phone.extra.speakeron"

    const-string/jumbo v3, "car_auto_start_speakerphone"

    invoke-static {v3, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 79
    :cond_0
    invoke-static {p0}, Lcom/vlingo/core/internal/util/DialUtil;->isPhoneExistsInContacts(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80
    const-string/jumbo v2, "origin"

    const-string/jumbo v3, "from_contact"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    :cond_1
    sput-boolean v5, Lcom/vlingo/core/internal/util/DialUtil;->bluetoothOn:Z

    .line 84
    return-object v1
.end method

.method public static getDialIntent(Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 1
    .param p0, "address"    # Ljava/lang/String;
    .param p1, "btOn"    # Z

    .prologue
    .line 63
    sput-boolean p1, Lcom/vlingo/core/internal/util/DialUtil;->bluetoothOn:Z

    .line 64
    invoke-static {p0}, Lcom/vlingo/core/internal/util/DialUtil;->getDialIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static getPhoneType(Lcom/vlingo/core/internal/contacts/ContactType;Ljava/lang/String;)I
    .locals 4
    .param p0, "capability"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p1, "phoneType"    # Ljava/lang/String;

    .prologue
    const/4 v0, -0x1

    .line 157
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    :goto_0
    return v0

    .line 160
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne p0, v1, :cond_7

    .line 161
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "work"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162
    const/4 v0, 0x3

    goto :goto_0

    .line 163
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "mobile"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 164
    const/4 v0, 0x2

    goto :goto_0

    .line 165
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "home"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 166
    const/4 v0, 0x1

    goto :goto_0

    .line 167
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "other"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 168
    const/4 v0, 0x7

    goto :goto_0

    .line 169
    :cond_4
    const-string/jumbo v1, "main"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 170
    const/16 v0, 0xc

    goto :goto_0

    .line 171
    :cond_5
    const-string/jumbo v1, "pager"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 172
    const/4 v0, 0x6

    goto :goto_0

    .line 174
    :cond_6
    sget-object v1, Lcom/vlingo/core/internal/util/DialUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "phone type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " unrecognized as a phone type."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_7
    sget-object v1, Lcom/vlingo/core/internal/util/DialUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "phone type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " capability "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " is not call."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static getPhoneTypeMap(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;
    .locals 12
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p2, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "cds":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v11, 0x1

    .line 235
    new-instance v7, Ljava/util/LinkedHashMap;

    invoke-direct {v7}, Ljava/util/LinkedHashMap;-><init>()V

    .line 236
    .local v7, "phoneTypeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v1, Lcom/vlingo/core/internal/util/SparseArrayMap;

    invoke-direct {v1}, Lcom/vlingo/core/internal/util/SparseArrayMap;-><init>()V

    .line 239
    .local v1, "contactTypeCounts":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<Ljava/lang/Integer;>;"
    invoke-static {p1, p2}, Lcom/vlingo/core/internal/util/DialUtil;->filterContactDataByType(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 241
    new-instance v8, Lcom/vlingo/core/internal/util/DialUtil$ContactDataComparator;

    invoke-direct {v8}, Lcom/vlingo/core/internal/util/DialUtil$ContactDataComparator;-><init>()V

    invoke-static {p1, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 243
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 244
    .local v0, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    iget v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/vlingo/core/internal/util/SparseArrayMap;->containsKey(Ljava/lang/Integer;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 245
    iget v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-virtual {v1, v8}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 246
    .local v2, "count":Ljava/lang/Integer;
    iget v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/vlingo/core/internal/util/SparseArrayMap;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 248
    .end local v2    # "count":Ljava/lang/Integer;
    :cond_0
    iget v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/vlingo/core/internal/util/SparseArrayMap;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 251
    .end local v0    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_1
    const/4 v3, 0x1

    .line 252
    .local v3, "counter":I
    const/4 v4, 0x0

    .line 253
    .local v4, "currentType":Ljava/lang/Integer;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 254
    .restart local v0    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    iget v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/vlingo/core/internal/util/SparseArrayMap;->containsKey(Ljava/lang/Integer;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 255
    if-nez v4, :cond_3

    .line 256
    iget v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 258
    :cond_3
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget v9, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-eq v8, v9, :cond_4

    .line 259
    const/4 v3, 0x1

    .line 260
    iget v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 262
    :cond_4
    iget v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-virtual {v1, v8}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 263
    .restart local v2    # "count":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-le v8, v11, :cond_5

    .line 264
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    iget-object v10, v0, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    invoke-static {p0, v8, v10}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 265
    .local v6, "phoneType":Ljava/lang/String;
    invoke-interface {v7, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    add-int/lit8 v3, v3, 0x1

    .line 267
    goto :goto_1

    .line 268
    .end local v6    # "phoneType":Ljava/lang/String;
    :cond_5
    iget v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    iget-object v9, v0, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    invoke-static {p0, v8, v9}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 269
    .restart local v6    # "phoneType":Ljava/lang/String;
    invoke-interface {v7, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 273
    .end local v0    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v2    # "count":Ljava/lang/Integer;
    .end local v6    # "phoneType":Ljava/lang/String;
    :cond_6
    return-object v7
.end method

.method public static getPhoneTypeOrder(Lcom/vlingo/core/internal/contacts/ContactType;Ljava/lang/String;)I
    .locals 6
    .param p0, "capability"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p1, "phoneType"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 182
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v2

    .line 185
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne p0, v3, :cond_4

    .line 186
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 187
    const-string/jumbo v3, "work"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "mobile"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "home"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "other"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 191
    :cond_2
    const-string/jumbo v3, "\\d+$"

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 192
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 193
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 194
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 199
    .end local v0    # "matcher":Ljava/util/regex/Matcher;
    .end local v1    # "pattern":Ljava/util/regex/Pattern;
    :cond_3
    sget-object v3, Lcom/vlingo/core/internal/util/DialUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "phone type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " unrecognized as a phone type."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_4
    sget-object v3, Lcom/vlingo/core/internal/util/DialUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "phone type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " capability "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " is not call."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static getTTSForAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "address"    # Ljava/lang/String;

    .prologue
    .line 372
    const-string/jumbo v3, ""

    .line 373
    .local v3, "ttsMsg":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 374
    const/4 v0, -0x1

    .line 376
    .local v0, "digit":I
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 383
    :cond_0
    :goto_1
    if-ltz v0, :cond_1

    const/16 v4, 0x9

    if-le v0, v4, :cond_2

    .line 373
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 377
    :catch_0
    move-exception v1

    .line 378
    .local v1, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2d

    if-ne v4, v5, :cond_0

    .line 379
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 385
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 388
    .end local v0    # "digit":I
    :cond_3
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_4

    .line 389
    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 391
    .end local p0    # "address":Ljava/lang/String;
    :cond_4
    return-object p0
.end method

.method public static getVideoDialIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p0, "address"    # Ljava/lang/String;

    .prologue
    .line 55
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.CALL"

    const-string/jumbo v2, "tel"

    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 56
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "videocall"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 57
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 59
    return-object v0
.end method

.method private static isPhoneExistsInContacts(Ljava/lang/String;)Z
    .locals 1
    .param p0, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/vlingo/core/internal/util/DialUtil;->getContactId(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
