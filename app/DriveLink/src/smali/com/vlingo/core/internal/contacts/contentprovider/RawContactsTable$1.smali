.class final Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;
.super Ljava/util/LinkedHashMap;
.source "RawContactsTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 30
    const-string/jumbo v0, "contact_id"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->LONG:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    const-string/jumbo v0, "aggregation_mode"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const-string/jumbo v0, "deleted"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const-string/jumbo v0, "times_contacted"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const-string/jumbo v0, "last_time_contacted"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->LONG:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    const-string/jumbo v0, "custom_ringtone"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->STRING:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const-string/jumbo v0, "send_to_voicemail"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const-string/jumbo v0, "send_to_voicemail"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->LONG:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    const-string/jumbo v0, "account_name"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->STRING:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    const-string/jumbo v0, "account_type"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->STRING:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    const-string/jumbo v0, "data_set"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->STRING:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const-string/jumbo v0, "sourceid"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->LONG:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string/jumbo v0, "version"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    const-string/jumbo v0, "dirty"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const-string/jumbo v0, "original_id"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->LONG:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/RawContactsTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    return-void
.end method
