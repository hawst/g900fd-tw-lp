.class public Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
.super Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
.source "ActionConfirmedEvent.java"


# instance fields
.field private final NAME:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;-><init>(ZZZ)V

    .line 17
    const-string/jumbo v0, "action-confirmed"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;->NAME:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    const-string/jumbo v0, "action-confirmed"

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v1, "action-confirmed"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 31
    .local v0, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v1

    return-object v1
.end method
