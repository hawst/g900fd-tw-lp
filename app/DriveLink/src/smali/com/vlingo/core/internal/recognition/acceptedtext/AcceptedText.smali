.class public abstract Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;
.super Ljava/lang/Object;
.source "AcceptedText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;,
        Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;
    }
.end annotation


# instance fields
.field protected GUttId:Ljava/lang/String;

.field protected dialogGuid:Ljava/lang/String;

.field protected dialogTurn:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "gUttId"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogGuid:Ljava/lang/String;

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogTurn:I

    .line 41
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->GUttId:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "dialogGuid"    # Ljava/lang/String;
    .param p2, "dialogTurn"    # I
    .param p3, "gUttId"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogGuid:Ljava/lang/String;

    .line 30
    iput p2, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogTurn:I

    .line 31
    iput-object p3, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->GUttId:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method protected abstract getAcceptedTextXML()Ljava/lang/String;
.end method

.method public getDialogGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogTurn()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogTurn:I

    return v0
.end method

.method public getDialogXML()Ljava/lang/String;
    .locals 3

    .prologue
    .line 62
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogGuid:Ljava/lang/String;

    if-nez v1, :cond_0

    iget v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogTurn:I

    if-gez v1, :cond_0

    .line 63
    const-string/jumbo v1, ""

    .line 74
    :goto_0
    return-object v1

    .line 66
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    const-string/jumbo v1, "<Dialog "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 67
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogGuid:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 68
    const-string/jumbo v1, " dialogGuid=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogGuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\" "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 70
    :cond_1
    iget v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogTurn:I

    const/4 v2, -0x1

    if-le v1, v2, :cond_2

    .line 71
    const-string/jumbo v1, " dialogTurn=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogTurn:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\" "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 73
    :cond_2
    const-string/jumbo v1, " />"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 74
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getGUttId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->GUttId:Ljava/lang/String;

    return-object v0
.end method

.method public getXMLString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->getAcceptedTextXML()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 52
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->getDialogXML()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public setDialogGuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "dialogGuid"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogGuid:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setDialogTurn(I)V
    .locals 0
    .param p1, "dialogTurn"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogTurn:I

    .line 95
    return-void
.end method

.method public setGUttId(Ljava/lang/String;)V
    .locals 0
    .param p1, "gUttId"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->GUttId:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "AcceptedText [dialogGuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogGuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", dialogTurn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->dialogTurn:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", GUttId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->GUttId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
