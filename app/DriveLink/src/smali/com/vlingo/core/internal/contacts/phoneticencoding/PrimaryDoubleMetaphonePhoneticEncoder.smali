.class public Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;
.super Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
.source "PrimaryDoubleMetaphonePhoneticEncoder.java"


# static fields
.field private static final DATA_KINDS:[Ljava/lang/String;


# instance fields
.field private final mimeTypeExtension:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "vnd.android.cursor.item/name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "vnd.android.cursor.item/nickname"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "vnd.android.cursor.item/organization"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;->DATA_KINDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;-><init>()V

    .line 18
    const-string/jumbo v0, "_svoice_dmetaphone_primary_encoding"

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;->mimeTypeExtension:Ljava/lang/String;

    return-void
.end method

.method private static getPossibleCombinations(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    .local p0, "encodedWords":Ljava/util/List;, "Ljava/util/List<Ljava/util/HashSet<Ljava/lang/String;>;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {p0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashSet;

    .line 113
    .local v1, "currentWordEncodings":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v0, "currentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_1

    .line 115
    const/4 v7, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {p0, v7, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v7

    invoke-static {v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;->getPossibleCombinations(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v6

    .line 116
    .local v6, "prevList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 117
    .local v5, "prev":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 118
    .local v2, "encodings":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "*"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 123
    .end local v2    # "encodings":Ljava/lang/String;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "prev":Ljava/lang/String;
    .end local v6    # "prevList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 124
    .restart local v2    # "encodings":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "*"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 126
    .end local v2    # "encodings":Ljava/lang/String;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    return-object v0
.end method


# virtual methods
.method public canEncode(Ljava/lang/String;)Z
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isLatinCharacterOnlyString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 42
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;->cacheContainsValue(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 43
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;->getCachedValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 57
    :goto_0
    return-object v3

    .line 45
    :cond_0
    new-instance v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;-><init>()V

    .line 46
    .local v1, "dm":Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;
    const-string/jumbo v8, "[\\s+.,-]"

    invoke-virtual {p1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 47
    .local v7, "substrings":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 48
    .local v2, "encoded":Ljava/lang/StringBuffer;
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v6, v0, v4

    .line 49
    .local v6, "substring":Ljava/lang/String;
    invoke-virtual {v1, v6, v9}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->doubleMetaphone(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 50
    invoke-virtual {v1, v6, v9}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->doubleMetaphone(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 48
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 55
    .end local v6    # "substring":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 56
    .local v3, "encodedString":Ljava/lang/String;
    invoke-virtual {p0, p1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;->addCachedValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getDataColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "data3"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "data4"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "data5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "data6"

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected getDataKinds()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;->DATA_KINDS:[Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "_svoice_dmetaphone_primary_encoding"

    return-object v0
.end method

.method public varargs getNameQueryPart(Ljava/util/List;[Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p2, "fields"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    .local v7, "namePartWhere":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v1, "encodedWords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashSet<Ljava/lang/String;>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 84
    .local v9, "word":Ljava/lang/String;
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 85
    .local v11, "wordEncodings":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {p0, v9}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 88
    .end local v9    # "word":Ljava/lang/String;
    .end local v11    # "wordEncodings":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_0
    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;->getPossibleCombinations(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v8

    .line 90
    .local v8, "possibleCombinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string/jumbo v12, "("

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    const/4 v3, 0x1

    .line 92
    .local v3, "first":Z
    move-object/from16 v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v5, v4

    .end local v4    # "i$":I
    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_4

    aget-object v2, v0, v5

    .line 93
    .local v2, "field":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .end local v5    # "i$":I
    .local v4, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 94
    .local v10, "wordEncodings":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 95
    const-string/jumbo v12, " OR "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    :cond_1
    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "*"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 98
    const-string/jumbo v12, "(0=1)"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    :goto_3
    const/4 v3, 0x0

    goto :goto_2

    .line 100
    :cond_2
    const-string/jumbo v12, "("

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " GLOB "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\'*"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 92
    .end local v10    # "wordEncodings":Ljava/lang/String;
    :cond_3
    add-int/lit8 v4, v5, 0x1

    .local v4, "i$":I
    move v5, v4

    .end local v4    # "i$":I
    .restart local v5    # "i$":I
    goto :goto_1

    .line 106
    .end local v2    # "field":Ljava/lang/String;
    :cond_4
    const-string/jumbo v12, ")"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    return-object v12
.end method
