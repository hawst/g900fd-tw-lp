.class public final enum Lcom/vlingo/core/internal/ResourceIdProvider$id;
.super Ljava/lang/Enum;
.source "ResourceIdProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/ResourceIdProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "id"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/ResourceIdProvider$id;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$id;

.field public static final enum core_btn_skip:Lcom/vlingo/core/internal/ResourceIdProvider$id;

.field public static final enum core_icon:Lcom/vlingo/core/internal/ResourceIdProvider$id;

.field public static final enum core_text:Lcom/vlingo/core/internal/ResourceIdProvider$id;

.field public static final enum core_title:Lcom/vlingo/core/internal/ResourceIdProvider$id;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;

    const-string/jumbo v1, "core_btn_skip"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;->core_btn_skip:Lcom/vlingo/core/internal/ResourceIdProvider$id;

    .line 50
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;

    const-string/jumbo v1, "core_icon"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/ResourceIdProvider$id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;->core_icon:Lcom/vlingo/core/internal/ResourceIdProvider$id;

    .line 51
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;

    const-string/jumbo v1, "core_text"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/ResourceIdProvider$id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;->core_text:Lcom/vlingo/core/internal/ResourceIdProvider$id;

    .line 52
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;

    const-string/jumbo v1, "core_title"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/ResourceIdProvider$id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;->core_title:Lcom/vlingo/core/internal/ResourceIdProvider$id;

    .line 48
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/core/internal/ResourceIdProvider$id;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$id;->core_btn_skip:Lcom/vlingo/core/internal/ResourceIdProvider$id;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$id;->core_icon:Lcom/vlingo/core/internal/ResourceIdProvider$id;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$id;->core_text:Lcom/vlingo/core/internal/ResourceIdProvider$id;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$id;->core_title:Lcom/vlingo/core/internal/ResourceIdProvider$id;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$id;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/ResourceIdProvider$id;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    const-class v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/ResourceIdProvider$id;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$id;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/ResourceIdProvider$id;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/ResourceIdProvider$id;

    return-object v0
.end method
