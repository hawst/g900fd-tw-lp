.class public abstract Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;
.super Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;
.source "PlayAudioTask.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# instance fields
.field protected mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field private mFileToPlayResid:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;-><init>()V

    return-void
.end method


# virtual methods
.method public isSystemTts()Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method public onCancelled()V
    .locals 0

    .prologue
    .line 36
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stop()V

    .line 37
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->notifyFinished()V

    .line 38
    return-void
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->notifyFinished()V

    .line 74
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->notifyFinished()V

    .line 58
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->notifyFinished()V

    .line 66
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 50
    :cond_0
    return-void
.end method

.method public abstract run()V
.end method
