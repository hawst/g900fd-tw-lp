.class Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/bluetooth/BluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BTStateBroadcastReceiver"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1170
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/bluetooth/BluetoothManager$1;

    .prologue
    .line 1170
    invoke-direct {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1173
    if-nez p2, :cond_1

    .line 1318
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1177
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1178
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$700()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "[SVoiceBT, LatencyCheck] BTStateBroadcastReceiver Received intent: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1179
    const-string/jumbo v6, "com.vlingo.client.app.action.APPLICATION_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1180
    const-string/jumbo v6, "com.vlingo.client.app.extra.STATE"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 1184
    .local v4, "newState":I
    if-nez v4, :cond_2

    .line 1185
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1186
    const/4 v6, 0x0

    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->onAppStateChanged(Z)V

    goto :goto_0

    .line 1191
    :cond_2
    const/4 v6, 0x1

    if-ne v4, v6, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v6

    if-eqz v6, :cond_0

    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$800()Ljava/lang/Runnable;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1195
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1196
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCOConnectedTask()V

    goto :goto_0

    .line 1201
    .end local v4    # "newState":I
    :cond_3
    const-string/jumbo v6, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1202
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->updateCurrentLocale(Landroid/content/res/Resources;)V

    goto :goto_0

    .line 1204
    :cond_4
    const-string/jumbo v6, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1205
    const-string/jumbo v6, "android.bluetooth.adapter.extra.STATE"

    const/high16 v7, -0x80000000

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    goto :goto_0

    .line 1229
    :cond_5
    const-string/jumbo v6, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1244
    const-string/jumbo v6, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1245
    const-string/jumbo v6, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 1246
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    # invokes: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isIgnoreIntentForBDevice(Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$1300(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1250
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0xe

    if-lt v6, v7, :cond_0

    .line 1251
    const-string/jumbo v6, "android.bluetooth.profile.extra.STATE"

    const/4 v7, 0x2

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 1252
    .restart local v4    # "newState":I
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$700()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "[SVoiceBT, LatencyCheck] BT BluetoothHeadset Connection state change: newState = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    const/4 v6, 0x2

    if-eq v4, v6, :cond_6

    if-nez v4, :cond_0

    .line 1254
    :cond_6
    const/4 v6, 0x2

    if-ne v4, v6, :cond_7

    const/4 v6, 0x1

    :goto_1
    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->onHeadsetStateChanged(Z)V

    goto/16 :goto_0

    :cond_7
    const/4 v6, 0x0

    goto :goto_1

    .line 1258
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v4    # "newState":I
    :cond_8
    const-string/jumbo v6, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1259
    const/4 v6, 0x0

    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z
    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$1002(Z)Z

    .line 1260
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0xe

    if-lt v6, v7, :cond_0

    .line 1261
    const-string/jumbo v6, "android.bluetooth.profile.extra.STATE"

    const/16 v7, 0xa

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 1262
    .restart local v4    # "newState":I
    const-string/jumbo v6, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    const/16 v7, 0xa

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 1263
    .local v5, "priorState":I
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$700()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "[SVoiceBT, LatencyCheck] BT BluetoothHeadset Audio state change: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "->"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1264
    const/16 v6, 0xc

    if-eq v4, v6, :cond_9

    const/16 v6, 0xa

    if-ne v4, v6, :cond_0

    .line 1265
    :cond_9
    const/16 v6, 0xc

    if-ne v4, v6, :cond_a

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6, v5}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->onScoStateChanged(ZI)V

    goto/16 :goto_0

    :cond_a
    const/4 v6, 0x0

    goto :goto_2

    .line 1269
    .end local v4    # "newState":I
    .end local v5    # "priorState":I
    :cond_b
    const-string/jumbo v6, "com.vlingo.client.app.action.AUDIO_FOCUS_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1270
    const-string/jumbo v6, "com.vlingo.client.app.extra.FOCUS_CHANGE"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1271
    .local v2, "focusChange":I
    const-string/jumbo v6, "com.vlingo.client.app.extra.FOCUS_CHANGE_FROM"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1272
    .local v3, "focusChangeFrom":Ljava/lang/String;
    packed-switch v2, :pswitch_data_0

    :pswitch_1
    goto/16 :goto_0

    .line 1292
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v6

    if-nez v6, :cond_c

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isCarMode()Z

    move-result v6

    if-nez v6, :cond_c

    .line 1293
    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_c

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1295
    const/4 v6, 0x1

    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z
    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$1402(Z)Z

    .line 1296
    const/4 v6, 0x0

    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->onAppStateChanged(Z)V

    .line 1306
    :cond_c
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isAboutToStartUserFlowWithMic()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1307
    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1311
    const/4 v6, 0x1

    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z
    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$1402(Z)Z

    .line 1312
    const/4 v6, 0x0

    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->onAppStateChanged(Z)V

    goto/16 :goto_0

    .line 1272
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
