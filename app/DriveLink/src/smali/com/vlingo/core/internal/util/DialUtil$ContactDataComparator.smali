.class Lcom/vlingo/core/internal/util/DialUtil$ContactDataComparator;
.super Ljava/lang/Object;
.source "DialUtil.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/DialUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContactDataComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/vlingo/core/internal/contacts/ContactData;Lcom/vlingo/core/internal/contacts/ContactData;)I
    .locals 2
    .param p1, "lhs"    # Lcom/vlingo/core/internal/contacts/ContactData;
    .param p2, "rhs"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 282
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v0

    invoke-virtual {p2}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 283
    const/4 v0, -0x1

    .line 287
    :goto_0
    return v0

    .line 284
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v0

    invoke-virtual {p2}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 285
    const/4 v0, 0x1

    goto :goto_0

    .line 287
    :cond_1
    iget v0, p1, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    iget v1, p2, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 276
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/util/DialUtil$ContactDataComparator;->compare(Lcom/vlingo/core/internal/contacts/ContactData;Lcom/vlingo/core/internal/contacts/ContactData;)I

    move-result v0

    return v0
.end method
