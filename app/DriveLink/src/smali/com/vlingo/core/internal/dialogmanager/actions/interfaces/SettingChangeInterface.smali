.class public interface abstract Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
.super Ljava/lang/Object;
.source "SettingChangeInterface.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;


# virtual methods
.method public abstract alreadySet(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
.end method

.method public abstract confirmOff(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
.end method

.method public abstract confirmOffTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
.end method

.method public abstract confirmOn(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
.end method

.method public abstract confirmOnTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
.end method

.method public abstract name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
.end method

.method public abstract state(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
.end method

.method public abstract vvsListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
.end method
