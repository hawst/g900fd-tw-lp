.class abstract Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;
.super Ljava/lang/Object;
.source "BaseContentChangesObserver.java"

# interfaces
.implements Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;
.implements Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;


# static fields
.field private static final INITIAL_DELAY_COEFFICIENT:F = 1.2f

.field private static final KEY_LMTT_VOCON_CONTENT_CHANGES_OBSERVER_COEFFICIENT:Ljava/lang/String; = "com.vlingo.core.internal.lmttvocon.conentobserver.content_observer_coefficient_"

.field public static MAX_SYNC_UP_DELAY_MS:J = 0x0L

.field private static final MS_IN_S:J = 0x3e8L

.field public static final SYNC_UP_DELAY_MS:J = 0x3a98L


# instance fields
.field private final context:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

.field private listener:Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;

.field private pendingCallback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    const-wide v0, 0x141dd76000L

    sput-wide v0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->MAX_SYNC_UP_DELAY_MS:J

    return-void
.end method

.method protected constructor <init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V
    .locals 0
    .param p1, "context"    # Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->context:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .line 26
    return-void
.end method

.method private getCurrentSyncUpDelayMs()J
    .locals 3

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->getSyncUpDelayCoefficientKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->getInitialDelayCoefficient()F

    move-result v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 105
    .local v0, "coefficient":F
    const v1, 0x466a6000    # 15000.0f

    mul-float/2addr v1, v0

    float-to-long v1, v1

    return-wide v1
.end method

.method private getSyncUpDelayCoefficientKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "com.vlingo.core.internal.lmttvocon.conentobserver.content_observer_coefficient_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private increaseSyncUpDelayCoefficient()V
    .locals 3

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->getSyncUpDelayCoefficientKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->getInitialDelayCoefficient()F

    move-result v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->getInitialDelayCoefficient()F

    move-result v2

    mul-float v0, v1, v2

    .line 112
    .local v0, "newSyncUpDelayCoefficient":F
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->getSyncUpDelayCoefficientKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setFloat(Ljava/lang/String;F)V

    .line 113
    return-void
.end method

.method private resetSyncUpDelayCoefficient()V
    .locals 2

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->getSyncUpDelayCoefficientKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->getInitialDelayCoefficient()F

    move-result v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setFloat(Ljava/lang/String;F)V

    .line 119
    return-void
.end method


# virtual methods
.method public declared-synchronized executeCallback()V
    .locals 1

    .prologue
    .line 92
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->pendingCallback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    .line 93
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->listener:Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->listener:Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;->onContentChanges()V

    .line 96
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->increaseSyncUpDelayCoefficient()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    monitor-exit p0

    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getInitialDelayCoefficient()F
    .locals 1

    .prologue
    .line 100
    const v0, 0x3f99999a    # 1.2f

    return v0
.end method

.method protected final onContentChanged()V
    .locals 7

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->listener:Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->pendingCallback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->pendingCallback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->reset()V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->getCurrentSyncUpDelayMs()J

    move-result-wide v3

    .line 83
    .local v3, "syncUpDelayMs":J
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->context:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->getMainHandler()Landroid/os/Handler;

    move-result-object v1

    sget-wide v5, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->MAX_SYNC_UP_DELAY_MS:J

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;-><init>(Landroid/os/Handler;Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;JJ)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->pendingCallback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    goto :goto_0
.end method

.method protected abstract registerContentObserver(Landroid/content/Context;)V
.end method

.method public resetDelay()V
    .locals 3

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->resetSyncUpDelayCoefficient()V

    .line 63
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->pendingCallback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    if-eqz v2, :cond_0

    .line 64
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->getCurrentSyncUpDelayMs()J

    move-result-wide v0

    .line 67
    .local v0, "syncUpDelayMs":J
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->pendingCallback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    invoke-virtual {v2, v0, v1}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->updateCallbackDelay(J)V

    .line 69
    .end local v0    # "syncUpDelayMs":J
    :cond_0
    return-void
.end method

.method public final declared-synchronized stopWatchingForContentUpdates()V
    .locals 1

    .prologue
    .line 30
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->pendingCallback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->pendingCallback:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->cancel()V

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->listener:Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;

    if-eqz v0, :cond_1

    .line 36
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->context:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->unregisterContentObserver(Landroid/content/Context;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->listener:Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    :cond_1
    monitor-exit p0

    return-void

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract unregisterContentObserver(Landroid/content/Context;)V
.end method

.method public final declared-synchronized watchForContentUpdates(Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;)V
    .locals 1
    .param p1, "listenerParam"    # Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->listener:Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->stopWatchingForContentUpdates()V

    .line 48
    :cond_0
    if-eqz p1, :cond_1

    .line 51
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->listener:Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;

    .line 52
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->context:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;->registerContentObserver(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :cond_1
    monitor-exit p0

    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
