.class public Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;
.super Landroid/database/CursorWrapper;
.source "CursorUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/CursorUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CursorGuard"
.end annotation


# instance fields
.field private cursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 98
    iput-object p1, p0, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->cursor:Landroid/database/Cursor;

    .line 99
    return-void
.end method

.method private cursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->cursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public static isValid(Landroid/database/Cursor;)Z
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 102
    invoke-static {p0}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->cursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->cursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->isValid(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public moveToFirst()Z
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->cursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToLast()Z
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->cursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToNext()Z
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->cursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToPosition(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->cursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToPrevious()Z
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CursorUtil$CursorGuard;->cursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
