.class Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;
.super Ljava/lang/Object;
.source "AudioPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/AudioPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaPlayerListener"
.end annotation


# instance fields
.field request:Lcom/vlingo/core/internal/audio/AudioRequest;

.field final synthetic this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/audio/AudioPlayer;Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p2, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 539
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540
    iput-object p2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->request:Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 541
    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v3, 0x1

    .line 545
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->request:Lcom/vlingo/core/internal/audio/AudioRequest;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v1, v1, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    if-ne v0, v1, :cond_0

    .line 546
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->request:Lcom/vlingo/core/internal/audio/AudioRequest;

    instance-of v0, v0, Lcom/vlingo/core/internal/audio/ToneAudioRequest;

    if-eqz v0, :cond_1

    .line 566
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isCurrentModelNotExactlySynchronizedWithPlayingTts()Z

    move-result v0

    if-nez v0, :cond_2

    .line 557
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v1, v1, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->request:Lcom/vlingo/core/internal/audio/AudioRequest;

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 559
    :cond_2
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v1, v1, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->request:Lcom/vlingo/core/internal/audio/AudioRequest;

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v2, 0x0

    .line 573
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->request:Lcom/vlingo/core/internal/audio/AudioRequest;

    if-ne v0, v1, :cond_1

    .line 574
    if-eqz p1, :cond_0

    .line 575
    invoke-virtual {p1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 576
    invoke-virtual {p1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v1, v1, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    const/16 v2, 0x69

    iget-object v3, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;->request:Lcom/vlingo/core/internal/audio/AudioRequest;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 581
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
