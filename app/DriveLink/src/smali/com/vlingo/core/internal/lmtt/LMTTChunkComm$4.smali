.class Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;
.super Ljava/lang/Object;
.source "LMTTChunkComm.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->onError(Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

.field final synthetic val$error:Lcom/vlingo/sdk/training/VLTrainerErrors;

.field final synthetic val$message:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    iput-object p2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;->val$error:Lcom/vlingo/sdk/training/VLTrainerErrors;

    iput-object p3, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;->val$message:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 294
    # invokes: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->updateTransmitTime()V
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$500()V

    .line 296
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mActiveChunk:Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    invoke-static {v1}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$300(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->updateState(Z)Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    move-result-object v0

    .line 298
    .local v0, "nextState":Lcom/vlingo/core/internal/lmtt/LMTTChunkState;
    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->ERRORED_OUT_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    if-eq v0, v1, :cond_0

    .line 302
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mChunkQueue:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$800(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mActiveChunk:Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$300(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 303
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # invokes: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->handleNextChunk()V
    invoke-static {v1}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$000(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)V

    .line 312
    :goto_0
    return-void

    .line 310
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mListener:Lcom/vlingo/sdk/training/VLTrainerListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$200(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Lcom/vlingo/sdk/training/VLTrainerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;->val$error:Lcom/vlingo/sdk/training/VLTrainerErrors;

    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$4;->val$message:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/vlingo/sdk/training/VLTrainerListener;->onError(Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V

    goto :goto_0
.end method
