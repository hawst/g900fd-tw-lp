.class public abstract Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LanguageChangeReceiver.java"


# static fields
.field public static final EXTRA_LANGUAGE:Ljava/lang/String; = "com.vlingo.client.settings.LANGUAGE"

.field public static final NOTIFY_LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.client.settings.LANGUAGE_CHANGED"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static notifyLanguageChanged(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p0, "newLanguage"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.vlingo.client.settings.LANGUAGE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 33
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.settings.LANGUAGE"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 34
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 35
    return-void
.end method

.method public static register(Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;Landroid/content/Context;)V
    .locals 2
    .param p0, "receiver"    # Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 23
    .local v0, "f":Landroid/content/IntentFilter;
    const-string/jumbo v1, "com.vlingo.client.settings.LANGUAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 24
    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 25
    return-void
.end method

.method public static unregister(Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;Landroid/content/Context;)V
    .locals 0
    .param p0, "receiver"    # Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 29
    return-void
.end method


# virtual methods
.method public abstract onLanguageChanged(Ljava/lang/String;)V
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 39
    const-string/jumbo v1, "com.vlingo.client.settings.LANGUAGE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    const-string/jumbo v1, "com.vlingo.client.settings.LANGUAGE"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "newLanguage":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;->onLanguageChanged(Ljava/lang/String;)V

    .line 43
    .end local v0    # "newLanguage":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public register(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;->register(Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;Landroid/content/Context;)V

    .line 15
    return-void
.end method

.method public unregister(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;->unregister(Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;Landroid/content/Context;)V

    .line 19
    return-void
.end method
