.class public abstract Lcom/vlingo/core/internal/lmtt/LMTTItem;
.super Ljava/lang/Object;
.source "LMTTItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmtt/LMTTItem$1;,
        Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;,
        Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    }
.end annotation


# instance fields
.field changeType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

.field final type:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

.field final uid:J


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;JLcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 0
    .param p1, "type"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .param p2, "uid"    # J
    .param p4, "changeType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem;->type:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    .line 75
    iput-wide p2, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    .line 76
    iput-object p4, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem;->changeType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .line 77
    return-void
.end method


# virtual methods
.method setChangeType(Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 0
    .param p1, "changeType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem;->changeType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    .line 81
    return-void
.end method
