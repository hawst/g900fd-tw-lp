.class public Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;
.super Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;
.source "ContactFetchAndSortRequest.java"


# static fields
.field private static final LOG:Lcom/vlingo/core/internal/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->LOG:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;[ILcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p4, "requestedTypes"    # [I
    .param p5, "callback"    # Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    .prologue
    .line 16
    invoke-direct/range {p0 .. p5}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;[ILcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;)V

    .line 17
    return-void
.end method

.method private breakAllWords(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 228
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 229
    .local v1, "newName":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 230
    sget-object v2, Lcom/vlingo/core/internal/util/StringUtils;->CHINESE_UNICODE_BLOCKS:Ljava/util/Set;

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 231
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 229
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 236
    :cond_1
    return-object v1
.end method

.method private breakAtPosition(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 240
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 241
    .local v1, "newName":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 242
    if-ne v0, p2, :cond_0

    .line 243
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 241
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 248
    :cond_1
    return-object v1
.end method

.method private computeChineseScore(FLcom/vlingo/core/internal/contacts/ContactMatch;)F
    .locals 10
    .param p1, "score"    # F
    .param p2, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/high16 v9, 0x41a00000    # 20.0f

    .line 196
    iget-object v4, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 197
    .local v4, "name":Ljava/lang/String;
    move v0, p1

    .line 198
    .local v0, "chineseScore":F
    cmpl-float v7, v0, v9

    if-ltz v7, :cond_0

    move v1, v0

    .line 224
    .end local v0    # "chineseScore":F
    .local v1, "chineseScore":F
    :goto_0
    return v1

    .line 202
    .end local v1    # "chineseScore":F
    .restart local v0    # "chineseScore":F
    :cond_0
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->breakAllWords(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 203
    .local v5, "newName":Ljava/lang/String;
    invoke-direct {p0, p2, v5}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v6

    .line 204
    .local v6, "newScore":F
    cmpl-float v7, v6, v0

    if-lez v7, :cond_1

    .line 205
    move v0, v6

    .line 207
    :cond_1
    cmpg-float v7, v0, v9

    if-gez v7, :cond_5

    .line 208
    const-string/jumbo v7, " "

    const-string/jumbo v8, ""

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 209
    invoke-direct {p0, p2, v5}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v6

    .line 210
    cmpl-float v7, v6, v0

    if-lez v7, :cond_2

    .line 211
    move v0, v6

    .line 213
    :cond_2
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x4

    if-le v7, v8, :cond_4

    const/4 v3, 0x3

    .line 214
    .local v3, "maxBreakPosition":I
    :goto_1
    move v2, v3

    .local v2, "i":I
    :goto_2
    if-ltz v2, :cond_5

    .line 215
    cmpg-float v7, v0, v9

    if-gez v7, :cond_3

    .line 216
    invoke-direct {p0, v4, v2}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->breakAtPosition(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 217
    invoke-direct {p0, p2, v5}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v6

    .line 218
    cmpl-float v7, v6, v0

    if-lez v7, :cond_3

    .line 219
    move v0, v6

    .line 214
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 213
    .end local v2    # "i":I
    .end local v3    # "maxBreakPosition":I
    :cond_4
    const/4 v3, 0x1

    goto :goto_1

    :cond_5
    move v1, v0

    .line 224
    .end local v0    # "chineseScore":F
    .restart local v1    # "chineseScore":F
    goto :goto_0
.end method

.method private computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;)F
    .locals 4
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 72
    iget-object v0, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 73
    .local v0, "name":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v1

    .line 74
    .local v1, "score":F
    sget-object v2, Lcom/vlingo/core/internal/util/StringUtils;->CHINESE_UNICODE_BLOCKS:Ljava/util/Set;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    invoke-direct {p0, v1, p1}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->computeChineseScore(FLcom/vlingo/core/internal/contacts/ContactMatch;)F

    move-result v1

    .line 77
    :cond_0
    return v1
.end method

.method private computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F
    .locals 8
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, ", &/+.-"

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "contactWords":[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->computeScore([Ljava/lang/String;)F

    move-result v5

    .line 86
    .local v5, "score":F
    const-string/jumbo v6, "contacts.use_other_names"

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 88
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasExtraNames()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 89
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getExtraNames()Ljava/util/List;

    move-result-object v3

    .line 90
    .local v3, "extraNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 92
    .local v1, "extraName":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, ", &/+."

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->computeScore([Ljava/lang/String;)F

    move-result v2

    .line 94
    .local v2, "extraNameScore":F
    const/high16 v6, -0x40000000    # -2.0f

    add-float/2addr v2, v6

    .line 95
    cmpg-float v6, v5, v2

    if-gez v6, :cond_0

    .line 96
    move v5, v2

    .line 97
    iput-object v1, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNameUsed:Ljava/lang/String;

    goto :goto_0

    .line 102
    .end local v1    # "extraName":Ljava/lang/String;
    .end local v2    # "extraNameScore":F
    .end local v3    # "extraNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    return v5
.end method

.method private computeScore([Ljava/lang/String;)F
    .locals 13
    .param p1, "contactWords"    # [Ljava/lang/String;

    .prologue
    const/high16 v12, -0x40800000    # -1.0f

    .line 106
    const/4 v9, 0x0

    .line 111
    .local v9, "score":F
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getMatchString()Ljava/util/List;

    move-result-object v7

    .line 112
    .local v7, "matchString":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    new-array v10, v11, [Z

    .line 113
    .local v10, "wasWordUsed":[Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-ge v3, v11, :cond_0

    .line 114
    const/4 v11, 0x0

    aput-boolean v11, v10, v3

    .line 113
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 116
    :cond_0
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    array-length v11, p1

    if-ge v5, v11, :cond_d

    .line 117
    const/4 v1, 0x0

    .line 118
    .local v1, "bestMatchScore":I
    const/4 v0, -0x1

    .line 119
    .local v0, "bestMatchIndex":I
    aget-object v2, p1, v5

    .line 120
    .local v2, "contactWord":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_1

    .line 116
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 122
    :cond_1
    const/4 v3, 0x0

    :goto_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-ge v3, v11, :cond_b

    .line 123
    aget-boolean v11, v10, v3

    if-nez v11, :cond_5

    .line 124
    const/4 v4, 0x0

    .line 125
    .local v4, "isMatchingPosition":Z
    if-nez v3, :cond_2

    if-eqz v5, :cond_3

    :cond_2
    if-eqz v3, :cond_4

    if-eqz v5, :cond_4

    .line 126
    :cond_3
    const/4 v4, 0x1

    .line 128
    :cond_4
    const/4 v6, 0x0

    .line 129
    .local v6, "matchScore":I
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 130
    .local v8, "matchWord":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_6

    .line 122
    .end local v4    # "isMatchingPosition":Z
    .end local v6    # "matchScore":I
    .end local v8    # "matchWord":Ljava/lang/String;
    :cond_5
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 133
    .restart local v4    # "isMatchingPosition":Z
    .restart local v6    # "matchScore":I
    .restart local v8    # "matchWord":Ljava/lang/String;
    :cond_6
    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 134
    if-eqz v4, :cond_8

    .line 135
    const/16 v6, 0x14

    .line 146
    :cond_7
    :goto_5
    if-le v6, v1, :cond_5

    .line 147
    move v1, v6

    .line 148
    move v0, v3

    goto :goto_4

    .line 137
    :cond_8
    const/16 v6, 0x12

    goto :goto_5

    .line 139
    :cond_9
    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 140
    if-eqz v4, :cond_a

    .line 141
    const/16 v6, 0xa

    goto :goto_5

    .line 143
    :cond_a
    const/16 v6, 0x8

    goto :goto_5

    .line 152
    .end local v4    # "isMatchingPosition":Z
    .end local v6    # "matchScore":I
    .end local v8    # "matchWord":Ljava/lang/String;
    :cond_b
    if-ltz v0, :cond_c

    .line 153
    int-to-float v11, v1

    add-float/2addr v9, v11

    .line 154
    const/4 v11, 0x1

    aput-boolean v11, v10, v0

    goto :goto_2

    .line 157
    :cond_c
    add-float/2addr v9, v12

    goto :goto_2

    .line 160
    .end local v0    # "bestMatchIndex":I
    .end local v1    # "bestMatchScore":I
    .end local v2    # "contactWord":Ljava/lang/String;
    :cond_d
    const/4 v3, 0x0

    :goto_6
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-ge v3, v11, :cond_f

    .line 161
    aget-boolean v11, v10, v3

    if-nez v11, :cond_e

    .line 162
    add-float/2addr v9, v12

    .line 160
    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 164
    :cond_f
    return v9
.end method

.method private static split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 9
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "delimiters"    # Ljava/lang/String;

    .prologue
    .line 168
    const/4 v1, 0x0

    .line 169
    .local v1, "count":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    .line 170
    .local v7, "len":I
    const/4 v6, 0x1

    .line 171
    .local v6, "lastWasDelimiter":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_2

    .line 172
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ltz v8, :cond_0

    .line 173
    const/4 v6, 0x1

    .line 171
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 175
    :cond_0
    if-eqz v6, :cond_1

    add-int/lit8 v1, v1, 0x1

    .line 176
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 179
    :cond_2
    new-array v0, v1, [Ljava/lang/String;

    .line 180
    .local v0, "arr":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 181
    .local v3, "index":I
    const/4 v5, -0x1

    .line 182
    .local v5, "lastStart":I
    const/4 v2, 0x0

    move v4, v3

    .end local v3    # "index":I
    .local v4, "index":I
    :goto_2
    if-gt v2, v7, :cond_5

    .line 183
    if-eq v2, v7, :cond_3

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ltz v8, :cond_4

    .line 184
    :cond_3
    if-ltz v5, :cond_7

    .line 185
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "index":I
    .restart local v3    # "index":I
    invoke-virtual {p0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v4

    .line 187
    :goto_3
    const/4 v5, -0x1

    .line 182
    :goto_4
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    .end local v3    # "index":I
    .restart local v4    # "index":I
    goto :goto_2

    .line 188
    :cond_4
    if-gez v5, :cond_6

    .line 189
    move v5, v2

    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_4

    .line 192
    .end local v3    # "index":I
    .restart local v4    # "index":I
    :cond_5
    return-object v0

    :cond_6
    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_4

    .end local v3    # "index":I
    .restart local v4    # "index":I
    :cond_7
    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_3
.end method


# virtual methods
.method protected getContactMatches(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;)Ljava/util/List;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "queryParam"    # Ljava/lang/String;
    .param p3, "contactTypeParam"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p3}, Lcom/vlingo/core/internal/contacts/ContactType;->getLookupTypes()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {p3}, Lcom/vlingo/core/internal/contacts/ContactType;->keepUnrelatedContacts()Z

    move-result v3

    invoke-interface {v0, v1, p2, v2, v3}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected scoreContacts()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 26
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getContactType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactType;->keepUnrelatedContacts()Z

    move-result v5

    .line 27
    .local v5, "mixedForPhone":Z
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getBestContact()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    .line 28
    .local v0, "bestContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getMatches()Ljava/util/List;

    move-result-object v4

    .line 29
    .local v4, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-eqz v4, :cond_8

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_8

    .line 30
    const/4 v2, 0x0

    .line 31
    .local v2, "foundBest":Z
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 32
    .local v1, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const-string/jumbo v6, "new_contact_match_algo"

    invoke-static {v6, v8}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-nez v6, :cond_1

    .line 33
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;)F

    move-result v6

    iput v6, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 35
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getContactType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactType;->getPreferredTarget()I

    move-result v6

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getRequestedTypes()[I

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeScores(I[I)V

    .line 39
    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_2

    .line 40
    iget v6, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    const v7, 0x3dcccccd    # 0.1f

    mul-float/2addr v6, v7

    iput v6, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 45
    :cond_2
    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    iget v6, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget v7, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_0

    .line 46
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getMru()Lcom/vlingo/core/internal/contacts/mru/MRU;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getContactType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeMRUScore(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 47
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getMru()Lcom/vlingo/core/internal/contacts/mru/MRU;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getContactType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeMRUScoresForData(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 49
    if-eqz v2, :cond_4

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->compareTo(Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v6

    if-ne v6, v8, :cond_0

    .line 50
    :cond_4
    move-object v0, v1

    .line 51
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->sortDetails()V

    .line 52
    const/4 v2, 0x1

    goto :goto_0

    .line 59
    .end local v1    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 60
    .restart local v1    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget v6, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget v7, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getPruningThreshold()F

    move-result v8

    sub-float/2addr v7, v8

    cmpl-float v6, v6, v7

    if-lez v6, :cond_6

    .line 63
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getSortedContacts()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 66
    .end local v1    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_7
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->setBestContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->matchExtraSortedContacts()V

    .line 69
    .end local v2    # "foundBest":Z
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_8
    return-void
.end method
