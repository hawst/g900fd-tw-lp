.class public Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;
.super Ljava/lang/Object;
.source "AndroidRecognitionResults.java"


# instance fields
.field private errorMessageString:Ljava/lang/String;

.field private recognitionResultPhrase:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "errorMessageString"    # Ljava/lang/String;
    .param p2, "recognitionResultPhrase"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;->errorMessageString:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;->recognitionResultPhrase:Ljava/lang/String;

    .line 16
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;->errorMessageString:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;->recognitionResultPhrase:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getErrorMessageString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;->errorMessageString:Ljava/lang/String;

    return-object v0
.end method

.method public getRecognitionResultPhrase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;->recognitionResultPhrase:Ljava/lang/String;

    return-object v0
.end method

.method public hasError()Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;->errorMessageString:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResultPhrase()Z
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;->recognitionResultPhrase:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
