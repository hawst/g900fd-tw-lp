.class public Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;
.super Ljava/lang/Object;
.source "NotificationPopUpContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;
    }
.end annotation


# instance fields
.field LOG_TAG:Ljava/lang/String;

.field protected context:Landroid/content/Context;

.field protected factory:Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

.field protected incomingNotifications:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/notification/NotificationPopUp;",
            ">;"
        }
    .end annotation
.end field

.field protected incomingTOSNotification:Lcom/vlingo/core/internal/notification/NotificationPopUp;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V
    .locals 1
    .param p1, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string/jumbo v0, "NotificationPopUpContainer"

    iput-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->LOG_TAG:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->factory:Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->incomingNotifications:Ljava/util/ArrayList;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->incomingTOSNotification:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .line 29
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->context:Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->readContent()V

    .line 31
    return-void
.end method

.method private checkForAcceptedNotifications(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/notification/NotificationPopUp;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/notification/NotificationPopUp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "receivedNotifications":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/notification/NotificationPopUp;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 57
    .local v2, "newAcceptedList":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .local v3, "notAcceptedNotifications":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/notification/NotificationPopUp;>;"
    const/4 v0, 0x1

    .line 59
    .local v0, "allAccepted":Z
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .line 60
    .local v4, "ntfc":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getVersion()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 61
    .local v5, "version":Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/core/internal/settings/Settings;->isNotificationsAccepted(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 62
    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    const/4 v0, 0x0

    goto :goto_0

    .line 72
    .end local v4    # "ntfc":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    .end local v5    # "version":Ljava/lang/String;
    :cond_1
    const-string/jumbo v6, "accepted_notifications"

    invoke-static {v6, v2}, Lcom/vlingo/core/internal/settings/Settings;->setStringSet(Ljava/lang/String;Ljava/util/Set;)V

    .line 73
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->setAllNotificationsAccepted(Z)V

    .line 74
    return-object v3
.end method

.method private checkForTOSNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp;)Lcom/vlingo/core/internal/notification/NotificationPopUp;
    .locals 3
    .param p1, "recievedTOSNotification"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .prologue
    const/4 v2, 0x0

    .line 81
    if-eqz p1, :cond_0

    .line 84
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getVersion()I

    move-result v0

    const-string/jumbo v1, "tos_accepted_version"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-le v0, v1, :cond_0

    .line 87
    invoke-static {v2}, Lcom/vlingo/core/internal/settings/Settings;->setTOSAccepted(Z)V

    .line 93
    .end local p1    # "recievedTOSNotification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    :goto_0
    return-object p1

    .restart local p1    # "recievedTOSNotification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private readContent()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 37
    const-string/jumbo v1, "notification_counter"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v2, "notification_counter_local"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 40
    new-instance v0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;

    iget-object v1, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->factory:Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    .line 41
    .local v0, "parser":Lcom/vlingo/core/internal/notification/NotificationPopUpParser;
    const-string/jumbo v1, "notification_content"

    const-string/jumbo v2, ""

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->parseXML(Ljava/lang/String;)V

    .line 42
    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->getNotifications()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->checkForAcceptedNotifications(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->incomingNotifications:Ljava/util/ArrayList;

    .line 44
    .end local v0    # "parser":Lcom/vlingo/core/internal/notification/NotificationPopUpParser;
    :cond_0
    const-string/jumbo v1, "tos_notification_counter"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v2, "tos_notification_counter_local"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 47
    new-instance v0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;

    iget-object v1, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->factory:Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    .line 48
    .restart local v0    # "parser":Lcom/vlingo/core/internal/notification/NotificationPopUpParser;
    const-string/jumbo v1, "tos_notification_content"

    const-string/jumbo v2, ""

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->parseXML(Ljava/lang/String;)V

    .line 49
    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->getFirstNotification()Lcom/vlingo/core/internal/notification/NotificationPopUp;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->checkForTOSNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp;)Lcom/vlingo/core/internal/notification/NotificationPopUp;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;->incomingTOSNotification:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .line 51
    .end local v0    # "parser":Lcom/vlingo/core/internal/notification/NotificationPopUpParser;
    :cond_1
    return-void
.end method
