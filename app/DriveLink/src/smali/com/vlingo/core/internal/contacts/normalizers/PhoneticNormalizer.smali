.class public Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;
.super Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
.source "PhoneticNormalizer.java"


# instance fields
.field context:Landroid/content/Context;

.field tools:Lcom/vlingo/core/internal/contacts/ContactPinyinTools;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;-><init>()V

    return-void
.end method


# virtual methods
.method public canNormalize(Ljava/lang/String;)Z
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 32
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    const-string/jumbo v2, "_"

    const-string/jumbo v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isChineseString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "zh-CN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 36
    :cond_0
    :goto_0
    return v1

    .line 34
    :catch_0
    move-exception v0

    .line 36
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public cleanTool()V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->tools:Lcom/vlingo/core/internal/contacts/ContactPinyinTools;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->tools:Lcom/vlingo/core/internal/contacts/ContactPinyinTools;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->destroyPinyinTranscriptionList()V

    .line 26
    :cond_0
    return-void
.end method

.method public normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 43
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->context:Landroid/content/Context;

    move-object/from16 v18, v0

    if-nez v18, :cond_0

    .line 44
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->context:Landroid/content/Context;

    .line 46
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->tools:Lcom/vlingo/core/internal/contacts/ContactPinyinTools;

    move-object/from16 v18, v0

    if-nez v18, :cond_1

    .line 47
    new-instance v18, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->tools:Lcom/vlingo/core/internal/contacts/ContactPinyinTools;

    .line 50
    :cond_1
    const/16 v16, 0x1

    .line 51
    .local v16, "numberOfVariations":I
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .local v12, "nameCharsPhoneticList":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/Character;>;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v14

    .line 54
    .local v14, "nameInChars":[C
    move-object v2, v14

    .local v2, "arr$":[C
    array-length v9, v2

    .local v9, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v9, :cond_2

    aget-char v13, v2, v5

    .line 55
    .local v13, "nameInChar":C
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->tools:Lcom/vlingo/core/internal/contacts/ContactPinyinTools;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->findNormalizedCharacterList(C)Ljava/util/List;

    move-result-object v17

    .line 56
    .local v17, "samePhoneticList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    move-object/from16 v0, v17

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    mul-int v16, v16, v18

    .line 54
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 59
    .end local v13    # "nameInChar":C
    .end local v17    # "samePhoneticList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    :cond_2
    const/16 v18, 0x1

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 60
    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v18, ";"

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 61
    .local v15, "normalizedName":Ljava/lang/StringBuilder;
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    .line 62
    .local v10, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 64
    .end local v10    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    :cond_3
    const-string/jumbo v18, ";"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 91
    .end local v5    # "i$":Ljava/util/Iterator;
    :goto_2
    return-object v18

    .line 67
    .end local v15    # "normalizedName":Ljava/lang/StringBuilder;
    .local v5, "i$":I
    :cond_4
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v18

    add-int/lit8 v18, v18, 0x1

    mul-int v18, v18, v16

    add-int/lit8 v18, v18, 0x2

    move/from16 v0, v18

    invoke-direct {v15, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 68
    .restart local v15    # "normalizedName":Ljava/lang/StringBuilder;
    const-string/jumbo v18, ";"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v11, v0, [I

    .line 70
    .local v11, "matrix":[I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    array-length v0, v11

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v4, v0, :cond_5

    .line 71
    const/16 v18, 0x0

    aput v18, v11, v4

    .line 70
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 73
    :cond_5
    const/4 v6, 0x0

    .line 74
    .local v6, "indexToIncrease":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_4
    move/from16 v0, v16

    if-ge v7, v0, :cond_a

    .line 75
    const/4 v4, 0x0

    :goto_5
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v4, v0, :cond_6

    .line 76
    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/List;

    aget v19, v11, v4

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 75
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 78
    :cond_6
    const/16 v18, 0x3b

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 79
    move v8, v6

    .local v8, "k":I
    :goto_6
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v8, v0, :cond_8

    .line 80
    aget v18, v11, v8

    add-int/lit8 v19, v18, 0x1

    invoke-interface {v12, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/List;

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v19

    move/from16 v1, v18

    if-eq v0, v1, :cond_9

    .line 81
    move v6, v8

    .line 82
    aget v18, v11, v6

    add-int/lit8 v18, v18, 0x1

    aput v18, v11, v6

    .line 83
    const/4 v3, 0x0

    .local v3, "h":I
    :goto_7
    if-ge v3, v6, :cond_7

    .line 84
    const/16 v18, 0x0

    aput v18, v11, v3

    .line 83
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 86
    :cond_7
    const/4 v6, 0x0

    .line 74
    .end local v3    # "h":I
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 79
    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 91
    .end local v8    # "k":I
    :cond_a
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_2
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->context:Landroid/content/Context;

    .line 20
    return-void
.end method
