.class public Lcom/vlingo/core/internal/questions/parser/SubsectionElement;
.super Lcom/vlingo/core/internal/questions/parser/ResponseElement;
.source "SubsectionElement.java"

# interfaces
.implements Lcom/vlingo/core/internal/questions/Answer$Subsection;


# instance fields
.field private mImage:Lcom/vlingo/core/internal/questions/DownloadableImage;

.field private mImages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/questions/parser/ImageElement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/questions/parser/ResponseElement;-><init>(Ljava/lang/String;)V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->mImages:Ljava/util/ArrayList;

    .line 20
    return-void
.end method


# virtual methods
.method public add(Lcom/vlingo/core/internal/questions/parser/ImageElement;)V
    .locals 1
    .param p1, "image"    # Lcom/vlingo/core/internal/questions/parser/ImageElement;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    return-void
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 30
    :try_start_0
    const-string/jumbo v0, "Height"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 35
    :goto_0
    return v0

    .line 31
    :catch_0
    move-exception v0

    .line 35
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImage()Lcom/vlingo/core/internal/questions/DownloadableImage;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->mImage:Lcom/vlingo/core/internal/questions/DownloadableImage;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    const-string/jumbo v0, "Src"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string/jumbo v0, "Text"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 40
    :try_start_0
    const-string/jumbo v0, "Width"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 45
    :goto_0
    return v0

    .line 41
    :catch_0
    move-exception v0

    .line 45
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasImage()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->mImage:Lcom/vlingo/core/internal/questions/DownloadableImage;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setImage(Lcom/vlingo/core/internal/questions/DownloadableImage;)V
    .locals 0
    .param p1, "image"    # Lcom/vlingo/core/internal/questions/DownloadableImage;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->mImage:Lcom/vlingo/core/internal/questions/DownloadableImage;

    .line 50
    return-void
.end method
