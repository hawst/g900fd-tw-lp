.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetTurnParamsHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "SetTurnParamsHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v4, 0x0

    .line 27
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 28
    const-string/jumbo v3, "fieldid"

    invoke-static {p1, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 29
    .local v1, "fieldId":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 30
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 32
    :cond_0
    const-string/jumbo v3, "ActionName"

    invoke-static {p1, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "actionName":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 34
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v3, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 36
    :cond_1
    const-string/jumbo v3, "ParseType"

    invoke-static {p1, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 37
    .local v2, "parseType":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 38
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PARSE_TYPE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v3, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 40
    :cond_2
    return v4
.end method
