.class public Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;
.super Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;
.source "KoreanPhoneticEncoder.java"


# static fields
.field private static final CHOSEONG:[C

.field private static final DATA_KINDS:[Ljava/lang/String;

.field private static final FISRT_NAME_RULES:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final JAEUM_CODES:[C

.field private static final JONGSEONG:[C

.field private static final JUNGSEONG:[C

.field private static final LAST_NAME_RULES:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MIMETYPE:Ljava/lang/String; = "_svoice_korean_encoding"

.field private static final MOEUM_CODES:[C

.field private static final NCHOSEONG:I

.field private static final NJONGSEONG:I

.field private static final NJUNGSEONG:I

.field private static final NULL_CHAR:C

.field private static final ZONE:[C


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x15

    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "vnd.android.cursor.item/name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->DATA_KINDS:[Ljava/lang/String;

    .line 22
    const/16 v0, 0x13

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->CHOSEONG:[C

    .line 25
    new-array v0, v3, [C

    fill-array-data v0, :array_1

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JUNGSEONG:[C

    .line 28
    const/16 v0, 0x1c

    new-array v0, v0, [C

    fill-array-data v0, :array_2

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JONGSEONG:[C

    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [C

    fill-array-data v0, :array_3

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->ZONE:[C

    .line 34
    new-instance v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder$1;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder$1;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->FISRT_NAME_RULES:Ljava/util/LinkedHashMap;

    .line 55
    new-instance v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder$2;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder$2;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->LAST_NAME_RULES:Ljava/util/LinkedHashMap;

    .line 59
    const/16 v0, 0x24

    new-array v0, v0, [C

    fill-array-data v0, :array_4

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JAEUM_CODES:[C

    .line 68
    new-array v0, v3, [C

    fill-array-data v0, :array_5

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->MOEUM_CODES:[C

    .line 76
    sget-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->CHOSEONG:[C

    array-length v0, v0

    sput v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->NCHOSEONG:I

    .line 77
    sget-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JUNGSEONG:[C

    array-length v0, v0

    sput v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->NJUNGSEONG:I

    .line 78
    sget-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JONGSEONG:[C

    array-length v0, v0

    sput v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->NJONGSEONG:I

    return-void

    .line 22
    :array_0
    .array-data 2
        0x3131s
        0x3132s
        0x3134s
        0x3137s
        0x3138s
        0x3139s
        0x3141s
        0x3142s
        0x3143s
        0x3145s
        0x3146s
        0x3147s
        0x3148s
        0x3149s
        0x314as
        0x314bs
        0x314cs
        0x314ds
        0x314es
    .end array-data

    .line 25
    nop

    :array_1
    .array-data 2
        0x314fs
        0x3150s
        0x3151s
        0x3152s
        0x3153s
        0x3154s
        0x3155s
        0x3156s
        0x3157s
        0x3158s
        0x3159s
        0x315as
        0x315bs
        0x315cs
        0x315ds
        0x315es
        0x315fs
        0x3160s
        0x3161s
        0x3162s
        0x3163s
    .end array-data

    .line 28
    nop

    :array_2
    .array-data 2
        0x0s
        0x3131s
        0x3132s
        0x3133s
        0x3134s
        0x3135s
        0x3136s
        0x3137s
        0x3139s
        0x313as
        0x313bs
        0x313cs
        0x313ds
        0x313es
        0x313fs
        0x3140s
        0x3141s
        0x3142s
        0x3144s
        0x3145s
        0x3146s
        0x3147s
        0x3148s
        0x314as
        0x314bs
        0x314cs
        0x314ds
        0x314es
    .end array-data

    .line 32
    :array_3
    .array-data 2
        -0x5400s
        -0x285ds
    .end array-data

    .line 59
    :array_4
    .array-data 2
        0x3131s
        0x3132s
        0x3133s
        0x3134s
        0x3135s
        0x3136s
        0x3137s
        0x3138s
        0x3139s
        0x313as
        0x313bs
        0x313cs
        0x313ds
        0x313es
        0x313fs
        0x3140s
        0x3141s
        0x3142s
        0x3143s
        0x3144s
        0x3145s
        0x3134s
        0x3135s
        0x3136s
        0x3131s
        0x3132s
        0x3133s
        0x3146s
        0x3147s
        0x3148s
        0x3149s
        0x314as
        0x314bs
        0x314cs
        0x314ds
        0x314es
    .end array-data

    .line 68
    :array_5
    .array-data 2
        0x314fs
        0x3150s
        0x3151s
        0x3152s
        0x3153s
        0x3154s
        0x3155s
        0x3156s
        0x3157s
        0x3158s
        0x3159s
        0x315as
        0x315bs
        0x315cs
        0x315ds
        0x315es
        0x315fs
        0x3160s
        0x3161s
        0x3162s
        0x3163s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;-><init>()V

    return-void
.end method

.method private static codeInJaeum(C)Z
    .locals 2
    .param p0, "code"    # C

    .prologue
    .line 234
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JAEUM_CODES:[C

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 235
    sget-object v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JAEUM_CODES:[C

    aget-char v1, v1, v0

    if-ne p0, v1, :cond_0

    .line 236
    const/4 v1, 0x1

    .line 240
    :goto_1
    return v1

    .line 234
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static codeInMoeum(C)Z
    .locals 2
    .param p0, "code"    # C

    .prologue
    .line 244
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->MOEUM_CODES:[C

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 245
    sget-object v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->MOEUM_CODES:[C

    aget-char v1, v1, v0

    if-ne p0, v1, :cond_0

    .line 246
    const/4 v1, 0x1

    .line 250
    :goto_1
    return v1

    .line 244
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getDistanceBetweenNames(Ljava/lang/String;Ljava/lang/String;)D
    .locals 7
    .param p0, "name1"    # Ljava/lang/String;
    .param p1, "name2"    # Ljava/lang/String;

    .prologue
    .line 277
    invoke-static {p0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->getSoundex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 278
    .local v1, "soundex1":Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->getSoundex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 279
    .local v2, "soundex2":Ljava/lang/String;
    const/4 v0, 0x1

    .line 281
    .local v0, "method":S
    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;->nlevenshtein(Ljava/lang/String;Ljava/lang/String;S)D

    move-result-wide v5

    sub-double/2addr v3, v5

    return-wide v3
.end method

.method private getSoudnex(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "firstName"    # Ljava/lang/String;
    .param p2, "lastName"    # Ljava/lang/String;

    .prologue
    .line 149
    sget-object v3, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->LAST_NAME_RULES:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, p2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 150
    sget-object v3, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->LAST_NAME_RULES:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .end local p2    # "lastName":Ljava/lang/String;
    check-cast p2, Ljava/lang/String;

    .line 153
    .restart local p2    # "lastName":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "fullName":Ljava/lang/String;
    sget-object v3, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->FISRT_NAME_RULES:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 155
    .local v0, "firstNameKey":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 156
    sget-object v3, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->FISRT_NAME_RULES:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 159
    .end local v0    # "firstNameKey":Ljava/lang/String;
    :cond_2
    return-object v1
.end method

.method private static getSoundex(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 189
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    .line 194
    .local v8, "length":I
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 195
    .local v1, "familyNameJaso":Ljava/lang/StringBuffer;
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "\\s+"

    const-string/jumbo v11, " "

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->splitJaso(Ljava/lang/String;)[C

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 196
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "\\s+"

    const-string/jumbo v11, " "

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "familyName":Ljava/lang/String;
    sget-object v9, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->LAST_NAME_RULES:Ljava/util/LinkedHashMap;

    invoke-virtual {v9}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 198
    .local v2, "familyNameKey":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 199
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    sget-object v9, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->LAST_NAME_RULES:Ljava/util/LinkedHashMap;

    invoke-virtual {v9, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/CharSequence;

    invoke-virtual {v10, v2, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 200
    const-string/jumbo v9, "\\s+"

    const-string/jumbo v10, " "

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 204
    .end local v2    # "familyNameKey":Ljava/lang/String;
    :cond_1
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 205
    .local v4, "firstNameJaso":Ljava/lang/StringBuffer;
    const/4 v6, 0x1

    .local v6, "i":I
    :goto_1
    if-ge v6, v8, :cond_2

    .line 206
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "\\s+"

    const-string/jumbo v11, " "

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->splitJaso(Ljava/lang/String;)[C

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 205
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 209
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "\\s+"

    const-string/jumbo v11, " "

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 210
    .local v3, "firstName":Ljava/lang/String;
    sget-object v9, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->FISRT_NAME_RULES:Ljava/util/LinkedHashMap;

    invoke-virtual {v9}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 211
    .local v5, "firstNameKey":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 212
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    sget-object v9, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->FISRT_NAME_RULES:Ljava/util/LinkedHashMap;

    invoke-virtual {v9, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/CharSequence;

    invoke-virtual {v10, v5, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 213
    const-string/jumbo v9, "\\s+"

    const-string/jumbo v10, " "

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 217
    .end local v5    # "firstNameKey":Ljava/lang/String;
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method

.method public static isHangul(I)Z
    .locals 3
    .param p0, "charCode"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 137
    sget-object v2, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->ZONE:[C

    aget-char v2, v2, v1

    if-gt v2, p0, :cond_1

    sget-object v2, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->ZONE:[C

    aget-char v2, v2, v0

    if-gt p0, v2, :cond_1

    .line 145
    :cond_0
    :goto_0
    return v0

    .line 139
    :cond_1
    int-to-char v2, p0

    invoke-static {v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->codeInJaeum(C)Z

    move-result v2

    if-nez v2, :cond_0

    .line 141
    int-to-char v2, p0

    invoke-static {v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->codeInMoeum(C)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 145
    goto :goto_0
.end method

.method private static splitJaso(Ljava/lang/String;)[C
    .locals 13
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 254
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 255
    .local v5, "length":I
    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 256
    .local v4, "code":C
    if-ne v5, v10, :cond_0

    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->isHangul(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 264
    :cond_0
    const v6, 0xac00

    sub-int v0, v4, v6

    .line 265
    .local v0, "asciiCode":I
    sget-object v6, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->CHOSEONG:[C

    sget v7, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->NJUNGSEONG:I

    sget v8, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->NJONGSEONG:I

    mul-int/2addr v7, v8

    div-int v7, v0, v7

    aget-char v1, v6, v7

    .line 266
    .local v1, "char1":C
    sget-object v6, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JUNGSEONG:[C

    sget v7, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->NJONGSEONG:I

    div-int v7, v0, v7

    sget v8, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->NJUNGSEONG:I

    rem-int/2addr v7, v8

    aget-char v2, v6, v7

    .line 267
    .local v2, "char2":C
    sget-object v6, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JONGSEONG:[C

    sget v7, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->NJONGSEONG:I

    rem-int v7, v0, v7

    aget-char v3, v6, v7

    .line 268
    .local v3, "char3":C
    if-nez v3, :cond_3

    .line 269
    new-array v6, v11, [C

    aput-char v1, v6, v9

    aput-char v2, v6, v10

    .line 272
    .end local v0    # "asciiCode":I
    .end local v1    # "char1":C
    .end local v2    # "char2":C
    .end local v3    # "char3":C
    :goto_0
    return-object v6

    .line 258
    :cond_1
    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->codeInJaeum(C)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 259
    new-array v6, v12, [C

    aput-char v4, v6, v9

    aput-char v9, v6, v10

    aput-char v9, v6, v11

    goto :goto_0

    .line 260
    :cond_2
    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->codeInMoeum(C)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 261
    new-array v6, v12, [C

    aput-char v9, v6, v9

    aput-char v4, v6, v10

    aput-char v9, v6, v11

    goto :goto_0

    .line 272
    .restart local v0    # "asciiCode":I
    .restart local v1    # "char1":C
    .restart local v2    # "char2":C
    .restart local v3    # "char3":C
    :cond_3
    new-array v6, v12, [C

    aput-char v1, v6, v9

    aput-char v2, v6, v10

    aput-char v3, v6, v11

    goto :goto_0
.end method


# virtual methods
.method public canEncode(Ljava/lang/String;)Z
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isKorean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 15
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 93
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->removeSpecialChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 95
    .local v11, "value":Ljava/lang/String;
    invoke-virtual {p0, v11}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->cacheContainsValue(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 96
    invoke-virtual {p0, v11}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->getCachedValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 132
    :goto_0
    return-object v4

    .line 98
    :cond_0
    const-string/jumbo v12, " "

    const-string/jumbo v13, ""

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    .line 99
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 100
    .local v5, "firstName":Ljava/lang/StringBuffer;
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    .line 101
    .local v9, "lastName":Ljava/lang/StringBuffer;
    const/4 v6, 0x0

    .line 102
    .local v6, "i":I
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v10, v0

    .local v10, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_1
    if-ge v7, v10, :cond_7

    aget-char v1, v0, v7

    .line 103
    .local v1, "character":C
    const/4 v12, 0x1

    new-array v12, v12, [[C

    const/4 v13, 0x0

    sget-object v14, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->CHOSEONG:[C

    aput-object v14, v12, v13

    invoke-static {v12}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    const/4 v12, 0x1

    new-array v12, v12, [[C

    const/4 v13, 0x0

    sget-object v14, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JUNGSEONG:[C

    aput-object v14, v12, v13

    invoke-static {v12}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    const/4 v12, 0x1

    new-array v12, v12, [[C

    const/4 v13, 0x0

    sget-object v14, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JONGSEONG:[C

    aput-object v14, v12, v13

    invoke-static {v12}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 104
    :cond_1
    if-nez v6, :cond_3

    .line 105
    invoke-virtual {v9, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 126
    :cond_2
    :goto_2
    add-int/lit8 v6, v6, 0x1

    .line 102
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 107
    :cond_3
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 110
    :cond_4
    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->isHangul(I)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 111
    const v12, 0xac00

    sub-int v2, v1, v12

    .line 112
    .local v2, "characterCode":I
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 113
    .local v3, "encodedChar":Ljava/lang/StringBuffer;
    sget-object v12, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->CHOSEONG:[C

    sget-object v13, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JUNGSEONG:[C

    array-length v13, v13

    sget-object v14, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JONGSEONG:[C

    array-length v14, v14

    mul-int/2addr v13, v14

    div-int v13, v2, v13

    aget-char v12, v12, v13

    invoke-static {v12}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v12

    invoke-virtual {v3, v12}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 114
    sget-object v12, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JUNGSEONG:[C

    sget-object v13, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JONGSEONG:[C

    array-length v13, v13

    div-int v13, v2, v13

    sget-object v14, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JUNGSEONG:[C

    array-length v14, v14

    rem-int/2addr v13, v14

    aget-char v12, v12, v13

    invoke-static {v12}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v12

    invoke-virtual {v3, v12}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 115
    sget-object v12, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JONGSEONG:[C

    array-length v12, v12

    rem-int v8, v2, v12

    .line 116
    .local v8, "jongseongCharIndex":I
    if-eqz v8, :cond_5

    .line 117
    sget-object v12, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JONGSEONG:[C

    sget-object v13, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->JONGSEONG:[C

    array-length v13, v13

    rem-int v13, v2, v13

    aget-char v12, v12, v13

    invoke-static {v12}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v12

    invoke-virtual {v3, v12}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 119
    :cond_5
    if-nez v6, :cond_6

    .line 120
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 122
    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 128
    .end local v1    # "character":C
    .end local v2    # "characterCode":I
    .end local v3    # "encodedChar":Ljava/lang/StringBuffer;
    .end local v8    # "jongseongCharIndex":I
    :cond_7
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v12, v13}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->getSoudnex(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 131
    .local v4, "encodedString":Ljava/lang/String;
    invoke-virtual {p0, v11, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->addCachedValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getDataColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 164
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "data1"

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected getDataKinds()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    sget-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->DATA_KINDS:[Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    const-string/jumbo v0, "_svoice_korean_encoding"

    return-object v0
.end method

.method public varargs getNameQueryPart(Ljava/util/List;[Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p2, "fields"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 175
    .local p1, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Ljava/lang/StringBuffer;

    const-string/jumbo v6, " ( "

    invoke-direct {v5, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 176
    .local v5, "namePart":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 177
    .local v1, "counter":I
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    .line 178
    .local v2, "field":Ljava/lang/String;
    invoke-virtual {v5, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, " = \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const/4 v6, 0x0

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 179
    add-int/lit8 v1, v1, 0x1

    .line 180
    array-length v6, p2

    if-eq v1, v6, :cond_0

    .line 181
    const-string/jumbo v6, " OR "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 177
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 184
    .end local v2    # "field":Ljava/lang/String;
    :cond_1
    const-string/jumbo v6, ") "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 185
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method
