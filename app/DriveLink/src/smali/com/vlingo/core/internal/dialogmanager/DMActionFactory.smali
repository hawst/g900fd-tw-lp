.class public final Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;
.super Ljava/lang/Object;
.source "DMActionFactory.java"


# static fields
.field private static final ACTIONS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DMActionType;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/DMAction;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->ACTIONS:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 5
    .param p0, "actionType"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .prologue
    const/4 v3, 0x0

    .line 98
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->ACTIONS:Ljava/util/Map;

    invoke-interface {v4, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 99
    .local v1, "actionClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/DMAction;>;"
    if-nez v1, :cond_0

    move-object v0, v3

    .line 112
    :goto_0
    return-object v0

    .line 102
    :cond_0
    const/4 v0, 0x0

    .line 104
    .local v0, "action":Lcom/vlingo/core/internal/dialogmanager/DMAction;
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "action":Lcom/vlingo/core/internal/dialogmanager/DMAction;
    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DMAction;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .restart local v0    # "action":Lcom/vlingo/core/internal/dialogmanager/DMAction;
    goto :goto_0

    .line 105
    .end local v0    # "action":Lcom/vlingo/core/internal/dialogmanager/DMAction;
    :catch_0
    move-exception v2

    .line 106
    .local v2, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v2}, Ljava/lang/InstantiationException;->printStackTrace()V

    move-object v0, v3

    .line 107
    goto :goto_0

    .line 108
    .end local v2    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v2

    .line 109
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    move-object v0, v3

    .line 110
    goto :goto_0
.end method

.method public static getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 1
    .param p0, "actionType"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .param p3, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 153
    invoke-static {p0}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    .line 154
    .local v0, "action":Lcom/vlingo/core/internal/dialogmanager/DMAction;
    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setContext(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 156
    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setListener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 157
    invoke-virtual {v0, p3}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setActionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 159
    :cond_0
    return-object v0
.end method

.method public static getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 1
    .param p0, "actionType"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/vlingo/core/internal/dialogmanager/DMAction;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/DMActionType;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "classType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p0}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DMAction;

    return-object v0
.end method

.method public static registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V
    .locals 1
    .param p0, "key"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/DMActionType;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/DMAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "clz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/DMAction;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->ACTIONS:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    return-void
.end method

.method public static setupStandardMappings()V
    .locals 2

    .prologue
    .line 63
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/DeleteAppointmentAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 64
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 65
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 66
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 67
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_VIEW:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 68
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_EDIT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 69
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 70
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_EMAIL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 71
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 72
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/SetAlarmAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 73
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SET_TIMER:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 74
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/SettingChangeAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 75
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VIEW_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/ViewContactAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 76
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VIDEO_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 77
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VOICE_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 78
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 79
    return-void
.end method
