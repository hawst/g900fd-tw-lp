.class public Lcom/vlingo/core/internal/util/TaskQueue;
.super Ljava/lang/Object;
.source "TaskQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;,
        Lcom/vlingo/core/internal/util/TaskQueue$Task;
    }
.end annotation


# instance fields
.field private mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

.field private mDisableProcessingOnResume:Z

.field private mHandler:Landroid/os/Handler;

.field private mIsPaused:Z

.field private mListener:Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;

.field private mTaskList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/TaskQueue$Task;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .line 39
    iput-object p1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mHandler:Landroid/os/Handler;

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "listener"    # Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .line 46
    iput-object p1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mHandler:Landroid/os/Handler;

    .line 47
    iput-object p2, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mListener:Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;

    .line 48
    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/util/TaskQueue;)Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/TaskQueue;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mListener:Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/util/TaskQueue;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/TaskQueue;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue;->onCurrentTaskDone()V

    return-void
.end method

.method private notifyQueueCancelled()V
    .locals 1

    .prologue
    .line 236
    new-instance v0, Lcom/vlingo/core/internal/util/TaskQueue$2;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/util/TaskQueue$2;-><init>(Lcom/vlingo/core/internal/util/TaskQueue;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 243
    return-void
.end method

.method private notifyQueueDone()V
    .locals 1

    .prologue
    .line 226
    new-instance v0, Lcom/vlingo/core/internal/util/TaskQueue$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/util/TaskQueue$1;-><init>(Lcom/vlingo/core/internal/util/TaskQueue;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 233
    return-void
.end method

.method private notifyTaskStarting(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V
    .locals 1
    .param p1, "task"    # Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mListener:Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mListener:Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;->onTaskStarting(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 217
    :cond_0
    return-void
.end method

.method private declared-synchronized onCurrentTaskDone()V
    .locals 2

    .prologue
    .line 156
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/TaskQueue$Task;->isCancelled()Z

    move-result v0

    .line 157
    .local v0, "currentTaskWasCancelled":Z
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .line 158
    iget-object v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 159
    iget-object v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mListener:Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;

    if-eqz v1, :cond_0

    .line 160
    if-eqz v0, :cond_1

    .line 161
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue;->notifyQueueCancelled()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 164
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue;->notifyQueueDone()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 156
    .end local v0    # "currentTaskWasCancelled":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 169
    .restart local v0    # "currentTaskWasCancelled":Z
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue;->processNextTask()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized processNextTask()V
    .locals 3

    .prologue
    .line 177
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mIsPaused:Z

    if-nez v1, :cond_0

    .line 178
    iget-object v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .line 184
    .local v0, "taskToRun":Lcom/vlingo/core/internal/util/TaskQueue$Task;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mDisableProcessingOnResume:Z

    .line 185
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/util/TaskQueue;->notifyTaskStarting(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 186
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mDisableProcessingOnResume:Z

    .line 189
    iget-boolean v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mIsPaused:Z

    if-nez v1, :cond_0

    .line 190
    iput-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .line 191
    iget-object v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 192
    iget-object v1, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    .end local v0    # "taskToRun":Lcom/vlingo/core/internal/util/TaskQueue$Task;
    :cond_0
    monitor-exit p0

    return-void

    .line 177
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public declared-synchronized cancel()V
    .locals 3

    .prologue
    .line 143
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .line 144
    .local v1, "t":Lcom/vlingo/core/internal/util/TaskQueue$Task;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/TaskQueue$Task;->onAborted()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 143
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "t":Lcom/vlingo/core/internal/util/TaskQueue$Task;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 146
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 147
    iget-object v2, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    if-eqz v2, :cond_1

    .line 148
    iget-object v2, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    # invokes: Lcom/vlingo/core/internal/util/TaskQueue$Task;->cancel()V
    invoke-static {v2}, Lcom/vlingo/core/internal/util/TaskQueue$Task;->access$100(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized deleteQueuedTaskOfType(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/util/TaskQueue$Task;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/util/TaskQueue$Task;>;"
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/util/TaskQueue$Task;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .line 109
    .local v1, "t":Lcom/vlingo/core/internal/util/TaskQueue$Task;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 110
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 107
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/util/TaskQueue$Task;>;"
    .end local v1    # "t":Lcom/vlingo/core/internal/util/TaskQueue$Task;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 113
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/util/TaskQueue$Task;>;"
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized hasQueuedOrRunningTaskOfType(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/util/TaskQueue$Task;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/util/TaskQueue$Task;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 85
    const/4 v0, 0x1

    .line 88
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/util/TaskQueue;->isQueuedTask(Ljava/lang/Class;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasQueuedTask()Z
    .locals 1

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasQueuedTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)Z
    .locals 3
    .param p1, "task"    # Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/TaskQueue$Task;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    .local v1, "t":Lcom/vlingo/core/internal/util/TaskQueue$Task;
    if-ne v1, p1, :cond_0

    .line 130
    const/4 v2, 0x1

    .line 133
    .end local v1    # "t":Lcom/vlingo/core/internal/util/TaskQueue$Task;
    :goto_0
    monitor-exit p0

    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 128
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized isQueuedTask(Ljava/lang/Class;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/util/TaskQueue$Task;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/util/TaskQueue$Task;>;"
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .line 96
    .local v1, "t":Lcom/vlingo/core/internal/util/TaskQueue$Task;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 97
    const/4 v2, 0x1

    .line 100
    .end local v1    # "t":Lcom/vlingo/core/internal/util/TaskQueue$Task;
    :goto_0
    monitor-exit p0

    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 95
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized isRunningTask()Z
    .locals 1

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isRunningTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)Z
    .locals 1
    .param p1, "task"    # Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mCurrentTask:Lcom/vlingo/core/internal/util/TaskQueue$Task;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized pause()V
    .locals 1

    .prologue
    .line 54
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mIsPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    monitor-exit p0

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V
    .locals 1
    .param p1, "task"    # Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mTaskList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    # setter for: Lcom/vlingo/core/internal/util/TaskQueue$Task;->mParentQueue:Lcom/vlingo/core/internal/util/TaskQueue;
    invoke-static {p1, p0}, Lcom/vlingo/core/internal/util/TaskQueue$Task;->access$002(Lcom/vlingo/core/internal/util/TaskQueue$Task;Lcom/vlingo/core/internal/util/TaskQueue;)Lcom/vlingo/core/internal/util/TaskQueue;

    .line 79
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue;->processNextTask()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    monitor-exit p0

    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized resume()V
    .locals 1

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mIsPaused:Z

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mIsPaused:Z

    .line 63
    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/TaskQueue;->mDisableProcessingOnResume:Z

    if-nez v0, :cond_0

    .line 64
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue;->processNextTask()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :cond_0
    monitor-exit p0

    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
