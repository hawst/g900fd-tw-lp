.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/VolumeHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "VolumeHandler.java"


# static fields
.field public static final SUBCOMMAND_PARAM_KEY:Ljava/lang/String; = "Action"

.field public static final VOLUME_DOWN:Ljava/lang/String; = "down"

.field public static final VOLUME_MUTE:Ljava/lang/String; = "mute"

.field public static final VOLUME_UNMUTE:Ljava/lang/String; = "unmute"

.field public static final VOLUME_UP:Ljava/lang/String; = "up"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x0

    .line 23
    const-string/jumbo v4, "Action"

    invoke-interface {p1, v4}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24
    .local v0, "actionVal":Ljava/lang/String;
    const/4 v1, 0x0

    .line 26
    .local v1, "msg":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v2, 0x6

    .line 29
    .local v2, "stream":I
    :goto_0
    const/4 v3, 0x1

    .line 32
    .local v3, "success":Z
    if-eqz v0, :cond_4

    const-string/jumbo v4, "down"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 33
    const/4 v4, -0x1

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/util/PhoneUtil;->incrementStreamVolume(II)Z

    move-result v4

    if-nez v4, :cond_3

    .line 34
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_at_minimum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/VolumeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 57
    :cond_0
    :goto_1
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 58
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/VolumeHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoText(Ljava/lang/String;)V

    .line 61
    :cond_1
    return v6

    .line 26
    .end local v2    # "stream":I
    .end local v3    # "success":Z
    :cond_2
    const/4 v2, 0x3

    goto :goto_0

    .line 36
    .restart local v2    # "stream":I
    .restart local v3    # "success":Z
    :cond_3
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_down:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/VolumeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 39
    :cond_4
    if-eqz v0, :cond_6

    const-string/jumbo v4, "up"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 40
    const/4 v4, 0x1

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/util/PhoneUtil;->incrementStreamVolume(II)Z

    move-result v4

    if-nez v4, :cond_5

    .line 41
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_at_maximum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/VolumeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 43
    :cond_5
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_up:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/VolumeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 46
    :cond_6
    if-eqz v0, :cond_7

    const-string/jumbo v4, "mute"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 47
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->muteCurrentStream()Z

    move-result v4

    if-nez v4, :cond_0

    .line 48
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_already_muted:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/VolumeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 51
    :cond_7
    if-eqz v0, :cond_0

    const-string/jumbo v4, "unmute"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 52
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->unmuteCurrentStream()Z

    move-result v4

    if-nez v4, :cond_0

    .line 53
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_already_unmuted:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/VolumeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
