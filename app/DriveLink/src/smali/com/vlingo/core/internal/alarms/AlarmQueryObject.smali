.class public Lcom/vlingo/core/internal/alarms/AlarmQueryObject;
.super Ljava/lang/Object;
.source "AlarmQueryObject.java"


# instance fields
.field private active:Ljava/lang/Boolean;

.field protected begin:J

.field protected beginTime:Ljava/lang/String;

.field private count:I

.field protected dayMask:Ljava/lang/Integer;

.field private daysMatchType:Lcom/vlingo/core/internal/util/Alarm$MatchType;

.field protected end:J

.field protected endTime:Ljava/lang/String;

.field private isRepeating:Ljava/lang/Boolean;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->name:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->active:Ljava/lang/Boolean;

    .line 29
    iput-wide v1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->begin:J

    .line 30
    iput-wide v1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->end:J

    .line 33
    iput-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->beginTime:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->endTime:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->dayMask:Ljava/lang/Integer;

    .line 37
    iput-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->isRepeating:Ljava/lang/Boolean;

    .line 39
    sget-object v0, Lcom/vlingo/core/internal/util/Alarm$MatchType;->LOOSE:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    iput-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->daysMatchType:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->count:I

    return-void
.end method

.method public static isDayMaskMatches(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/vlingo/core/internal/util/Alarm$MatchType;)Z
    .locals 4
    .param p0, "maskToLookup"    # Ljava/lang/Integer;
    .param p1, "alarmMask"    # Ljava/lang/Integer;
    .param p2, "matchType"    # Lcom/vlingo/core/internal/util/Alarm$MatchType;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 93
    if-nez p0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v0

    .line 94
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 96
    sget-object v2, Lcom/vlingo/core/internal/util/Alarm$MatchType;->EXACT:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    invoke-virtual {v2, p2}, Lcom/vlingo/core/internal/util/Alarm$MatchType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 97
    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    or-int/2addr v2, v3

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 100
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, 0x111110

    if-ne v2, v3, :cond_3

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    and-int/2addr v2, v3

    if-ge v2, v0, :cond_0

    .line 101
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, 0x1000001

    if-ne v2, v3, :cond_4

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    and-int/2addr v2, v3

    if-ge v2, v0, :cond_0

    :cond_4
    move v0, v1

    .line 102
    goto :goto_0
.end method

.method private stringCanonicalLong(Ljava/lang/String;)J
    .locals 5
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 127
    :try_start_0
    const-string/jumbo v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 128
    .local v2, "t":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 129
    .local v0, "hours":I
    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 130
    .local v1, "minutes":I
    mul-int/lit8 v3, v0, 0x64

    add-int/2addr v3, v1

    int-to-long v3, v3

    .line 135
    .end local v0    # "hours":I
    .end local v1    # "minutes":I
    .end local v2    # "t":[Ljava/lang/String;
    :goto_0
    return-wide v3

    .line 131
    :catch_0
    move-exception v3

    .line 135
    const-wide/16 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getActive()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->active:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getBeginCanonical()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->beginTime:Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->count:I

    return v0
.end method

.method public getDayMask()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->dayMask:Ljava/lang/Integer;

    return-object v0
.end method

.method public getDaysMatchType()Lcom/vlingo/core/internal/util/Alarm$MatchType;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->daysMatchType:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->name:Ljava/lang/String;

    return-object v0
.end method

.method public isRepeating()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->isRepeating:Ljava/lang/Boolean;

    return-object v0
.end method

.method public matches(Lcom/vlingo/core/internal/util/Alarm;)Z
    .locals 6
    .param p1, "alarm"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 56
    invoke-virtual {p0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->normalize()V

    .line 58
    iget-object v2, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->beginTime:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->endTime:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->begin:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    iget-wide v2, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->end:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 59
    iget-wide v2, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->begin:J

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    iget-wide v2, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->end:J

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->getActive()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->getActive()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getAlarmStatus()Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    move-result-object v2

    sget-object v4, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    if-eq v2, v4, :cond_5

    move v2, v1

    :goto_1
    if-ne v3, v2, :cond_0

    .line 71
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->getDayMask()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getDayMask()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->getDaysMatchType()Lcom/vlingo/core/internal/util/Alarm$MatchType;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->isDayMaskMatches(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/vlingo/core/internal/util/Alarm$MatchType;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->isRepeating()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->isRepeating()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->isWeeklyRepeating()Z

    move-result v3

    if-ne v2, v3, :cond_0

    :cond_4
    move v0, v1

    .line 89
    goto :goto_0

    :cond_5
    move v2, v0

    .line 64
    goto :goto_1
.end method

.method public normalize()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->beginTime:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->beginTime:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->stringCanonicalLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->begin:J

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->endTime:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->endTime:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->stringCanonicalLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->end:J

    .line 145
    :cond_1
    return-void
.end method

.method public setActive(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "active"    # Ljava/lang/Boolean;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->active:Ljava/lang/Boolean;

    .line 123
    return-void
.end method

.method public setBegin(J)V
    .locals 0
    .param p1, "begin"    # J

    .prologue
    .line 148
    iput-wide p1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->begin:J

    .line 149
    return-void
.end method

.method public setBegin(Ljava/lang/String;)V
    .locals 0
    .param p1, "canonical"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->setBeginTime(Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method public setBeginTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "beginTime"    # Ljava/lang/String;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->beginTime:Ljava/lang/String;

    .line 166
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->count:I

    .line 45
    return-void
.end method

.method public setDayMask(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "dayMask"    # Ljava/lang/Integer;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->dayMask:Ljava/lang/Integer;

    .line 191
    return-void
.end method

.method public setDaysMatchType(Lcom/vlingo/core/internal/util/Alarm$MatchType;)V
    .locals 0
    .param p1, "daysMatchType"    # Lcom/vlingo/core/internal/util/Alarm$MatchType;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->daysMatchType:Lcom/vlingo/core/internal/util/Alarm$MatchType;

    .line 218
    return-void
.end method

.method public setEnd(J)V
    .locals 0
    .param p1, "end"    # J

    .prologue
    .line 169
    iput-wide p1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->end:J

    .line 170
    return-void
.end method

.method public setEnd(Ljava/lang/String;)V
    .locals 0
    .param p1, "canonical"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->setEndTime(Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public setEndTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "endTime"    # Ljava/lang/String;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->endTime:Ljava/lang/String;

    .line 178
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->name:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setRepeating(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "repeating"    # Ljava/lang/Boolean;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->isRepeating:Ljava/lang/Boolean;

    .line 205
    return-void
.end method
