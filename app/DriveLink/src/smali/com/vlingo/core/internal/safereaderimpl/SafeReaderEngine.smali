.class public final Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;
.super Ljava/lang/Object;
.source "SafeReaderEngine.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;
.implements Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
.implements Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;
.implements Lcom/vlingo/core/internal/util/ADMFeatureListener;


# static fields
.field private static instance:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;


# instance fields
.field private final SAFEREADER_NOTIFICATION_ID:I

.field private alertQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field private final alertSources:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;",
            ">;"
        }
    .end annotation
.end field

.field protected audioManager:Landroid/media/AudioManager;

.field protected final context:Landroid/content/Context;

.field private final intentReceiver:Landroid/content/BroadcastReceiver;

.field protected isAudioPlaying:Z

.field protected isPaused:Z

.field protected isSafeReaderOn:Z

.field protected isSilenced:Z

.field private final notificationManager:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;

.field protected final phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

.field public final remoteMsgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;",
            ">;"
        }
    .end annotation
.end field

.field private smsmmmsAlertSource:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

.field protected telephonyManager:Landroid/telephony/TelephonyManager;

.field protected useDelay:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v3, 0x7d0

    const/4 v2, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->SAFEREADER_NOTIFICATION_ID:I

    .line 50
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertQueue:Ljava/util/LinkedList;

    .line 52
    iput-boolean v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSafeReaderOn:Z

    .line 53
    iput-boolean v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isPaused:Z

    .line 54
    iput-boolean v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isAudioPlaying:Z

    .line 55
    iput-boolean v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSilenced:Z

    .line 56
    iput-boolean v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->useDelay:Z

    .line 66
    new-instance v1, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$1;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$1;-><init>(Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->intentReceiver:Landroid/content/BroadcastReceiver;

    .line 89
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->context:Landroid/content/Context;

    .line 90
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->remoteMsgListeners:Ljava/util/List;

    .line 91
    const-string/jumbo v1, "phone"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 92
    const-string/jumbo v1, "audio"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->audioManager:Landroid/media/AudioManager;

    .line 93
    new-instance v1, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;

    iget-boolean v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSilenced:Z

    invoke-direct {v1, p1, v3, v2}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;-><init>(Landroid/content/Context;IZ)V

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->notificationManager:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertSources:Ljava/util/ArrayList;

    .line 96
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getPhoneStateListener()Landroid/telephony/PhoneStateListener;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    .line 97
    new-instance v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    invoke-direct {v1}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->smsmmmsAlertSource:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    .line 98
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertSources:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->smsmmmsAlertSource:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 100
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 101
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->intentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 102
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/content/Intent;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->handleIntent(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertQueue:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;Ljava/util/LinkedList;)Ljava/util/LinkedList;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;
    .param p1, "x1"    # Ljava/util/LinkedList;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertQueue:Ljava/util/LinkedList;

    return-object p1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    sget-object v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->instance:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->instance:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    .line 78
    sget-object v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->instance:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    .line 80
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->instance:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    goto :goto_0
.end method

.method private handleIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "contextParam"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 318
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "com.vlingo.client.safereader.ACTION_SAFEREADER_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 326
    :cond_0
    return-void
.end method


# virtual methods
.method public broadcastStatusUpdate()V
    .locals 0

    .prologue
    .line 199
    return-void
.end method

.method public doesSupportNotifications()Z
    .locals 2

    .prologue
    .line 242
    const-string/jumbo v0, "safereader_notifications"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getAlertSources()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 354
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertSources:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNotification()Landroid/app/Notification;
    .locals 4

    .prologue
    .line 237
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->notificationManager:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;

    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->context:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isAudioPlaying:Z

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->getNotification(Landroid/content/Context;ZZ)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationId()I
    .locals 1

    .prologue
    .line 247
    const/16 v0, 0x7d0

    return v0
.end method

.method public isSafeReaderOn()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSafeReaderOn:Z

    return v0
.end method

.method public isSilenced()Z
    .locals 1

    .prologue
    .line 349
    iget-boolean v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSilenced:Z

    return v0
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 221
    packed-switch p1, :pswitch_data_0

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 225
    :pswitch_0
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertQueue:Ljava/util/LinkedList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 226
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->useDelay:Z

    .line 227
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 228
    .local v0, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 229
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->onNewSafeReaderAlert(Ljava/util/LinkedList;)V

    goto :goto_0

    .line 221
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onNewSafeReaderAlert(Ljava/util/LinkedList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 252
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine$2;-><init>(Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;Ljava/util/LinkedList;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 315
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "isEnabled"    # Z

    .prologue
    .line 361
    sget-object v0, Lcom/vlingo/core/internal/util/ADMController;->SAFEREADER:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    if-eqz p2, :cond_1

    .line 365
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->startSafeReading()V

    .line 373
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->stopSafeReading()V

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSafeReaderOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isPaused:Z

    .line 165
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stop()V

    .line 167
    :cond_0
    return-void
.end method

.method public registerAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 184
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->remoteMsgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->remoteMsgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSafeReaderOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isPaused:Z

    .line 174
    :cond_0
    return-void
.end method

.method public safeReaderDeinit()V
    .locals 1

    .prologue
    .line 335
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getADMController()Lcom/vlingo/core/internal/util/ADMController;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/util/ADMController;->removeListener(Lcom/vlingo/core/internal/util/ADMFeatureListener;)V

    .line 336
    return-void
.end method

.method public safeReaderInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V
    .locals 4
    .param p1, "engine"    # Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    .prologue
    .line 330
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getADMController()Lcom/vlingo/core/internal/util/ADMController;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/vlingo/core/internal/util/ADMController;->SAFEREADER:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/vlingo/core/internal/util/ADMController;->addListener(Lcom/vlingo/core/internal/util/ADMFeatureListener;[Ljava/lang/String;)V

    .line 331
    return-void
.end method

.method public setPriority(Z)V
    .locals 1
    .param p1, "high"    # Z

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->smsmmmsAlertSource:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->setPriority(Z)V

    .line 86
    return-void
.end method

.method public setSilenced(Z)V
    .locals 1
    .param p1, "silenced"    # Z

    .prologue
    .line 344
    iput-boolean p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSilenced:Z

    .line 345
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->notificationManager:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->setSilent(Z)V

    .line 346
    return-void
.end method

.method public skipCurrentlyPlayingItem()V
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSafeReaderOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    :cond_0
    return-void
.end method

.method public startSafeReading()V
    .locals 3

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSafeReaderOn()Z

    move-result v2

    if-nez v2, :cond_1

    .line 110
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isPaused:Z

    .line 111
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSafeReaderOn:Z

    .line 115
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    if-eqz v2, :cond_0

    .line 116
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    invoke-virtual {v2, p0}, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->addListener(Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;)V

    .line 119
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertSources:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;

    .line 120
    .local v1, "source":Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->context:Landroid/content/Context;

    invoke-interface {v1, v2, p0}, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;->onStart(Landroid/content/Context;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)V

    goto :goto_0

    .line 123
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "source":Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;
    :cond_1
    return-void
.end method

.method public stopSafeReading()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 127
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSafeReaderOn()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 130
    iput-boolean v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isAudioPlaying:Z

    .line 131
    iput-boolean v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isSafeReaderOn:Z

    .line 135
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    if-eqz v2, :cond_0

    .line 136
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    invoke-virtual {v2, p0}, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->removeListener(Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;)V

    .line 139
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->alertSources:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;

    .line 140
    .local v1, "source":Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->context:Landroid/content/Context;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;->onStop(Landroid/content/Context;)V

    goto :goto_0

    .line 143
    .end local v1    # "source":Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->isAppInForeground()Z

    move-result v2

    if-nez v2, :cond_2

    .line 144
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stop()V

    .line 147
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->intentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    return-void

    .line 148
    .restart local v0    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public unregisterAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 191
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->remoteMsgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->remoteMsgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 194
    :cond_0
    return-void
.end method

.method public updateNotification()V
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->notificationManager:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;

    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->context:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->isAudioPlaying:Z

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->updateNotification(Landroid/content/Context;Z)V

    .line 341
    return-void
.end method
