.class public abstract Lcom/vlingo/core/internal/settings/util/DataSetting;
.super Lcom/vlingo/core/internal/settings/util/Setting;
.source "DataSetting.java"


# direct methods
.method protected constructor <init>(Ljava/lang/String;[BLjava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "data"    # [B
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 20
    const/4 v0, 0x6

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/vlingo/core/internal/settings/util/Setting;-><init>(Ljava/lang/String;ILjava/lang/Object;Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public getValue()[B
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/DataSetting;->value:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public setValue([B)V
    .locals 0
    .param p1, "value"    # [B

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/settings/util/DataSetting;->setValueInternal(Ljava/lang/Object;)V

    .line 25
    return-void
.end method
