.class public final Lcom/vlingo/core/internal/associatedservice/ApplicationQueryNames;
.super Ljava/lang/Object;
.source "ApplicationQueryNames.java"


# static fields
.field public static final QUERY_CONTENT_PROVIDER_VERSION:Ljava/lang/String; = "content_provider_version"

.field public static final QUERY_DELIMETER:Ljava/lang/String; = "/"

.field public static final QUERY_LMTT_SERVICE_STARTED:Ljava/lang/String; = "lmtt_started"

.field public static final QUERY_NAME_APP_CHANNEL:Ljava/lang/String; = "app_channel"

.field public static final QUERY_NAME_APP_ID:Ljava/lang/String; = "app_id"

.field public static final QUERY_NAME_APP_NAME:Ljava/lang/String; = "app_name"

.field public static final QUERY_NAME_APP_VERSION:Ljava/lang/String; = "app_version"

.field public static final QUERY_NAME_ECHO_CANCELLATION:Ljava/lang/String; = "using_echo_cancellation"

.field public static final QUERY_NAME_HOST_ASR:Ljava/lang/String; = "server_host_asr"

.field public static final QUERY_NAME_HOST_HELLO:Ljava/lang/String; = "server_host_hello"

.field public static final QUERY_NAME_HOST_LOG:Ljava/lang/String; = "server_host_log"

.field public static final QUERY_NAME_HOST_VCS:Ljava/lang/String; = "server_host_vcs"

.field public static final QUERY_NAME_READBACK_ENABLED:Ljava/lang/String; = "readback_enabled"

.field public static final QUERY_NAME_TOS_ACCEPTED_AND_SAFEREADING:Ljava/lang/String; = "tos_accepted"

.field public static final QUERY_SALES_CODE:Ljava/lang/String; = "sales_code"

.field public static final QUERY_SETTINGS_PATH_PREFIX:Ljava/lang/String; = "SETTINGS/"

.field public static final QUERY_UIFOCUS_MANAGER_VERSION:Ljava/lang/String; = "content_uifocus_manager_version"

.field public static final QUERY_UIX_COMPLETE:Ljava/lang/String; = "uix_complete"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
