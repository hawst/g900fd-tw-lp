.class public Lcom/vlingo/core/internal/dialogmanager/actions/WeatherLookupAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "WeatherLookupAction.java"


# instance fields
.field private weatherResp:Lcom/vlingo/core/internal/weather/WeatherElement;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 3

    .prologue
    .line 32
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/WeatherLookupAction;->weatherResp:Lcom/vlingo/core/internal/weather/WeatherElement;

    if-eqz v1, :cond_0

    .line 33
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 37
    .local v0, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/WeatherLookupAction;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/WeatherLookupAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 47
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/WeatherLookupAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "Empty Response"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1

    .line 39
    .restart local v0    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public weather(Lcom/vlingo/core/internal/weather/WeatherElement;)Lcom/vlingo/core/internal/dialogmanager/actions/WeatherLookupAction;
    .locals 0
    .param p1, "weather"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/WeatherLookupAction;->weatherResp:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 25
    return-object p0
.end method
