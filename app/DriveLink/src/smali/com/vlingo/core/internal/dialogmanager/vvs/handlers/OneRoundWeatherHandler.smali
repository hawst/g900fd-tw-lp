.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/OneRoundWeatherHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;
.source "OneRoundWeatherHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v8, 0x0

    .line 29
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/OneRoundWeatherHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 31
    const-string/jumbo v7, "RawVCSResponse"

    invoke-static {p1, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 32
    .local v2, "rawVcsResponse":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 33
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v8, "RawVCSResponse parameter shouldn\'t be empty"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 36
    :cond_0
    const-string/jumbo v7, "Location"

    invoke-static {p1, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, "location":Ljava/lang/String;
    const-string/jumbo v7, "Date"

    invoke-static {p1, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "dateStr":Ljava/lang/String;
    const-string/jumbo v7, "SpokenDate"

    invoke-static {p1, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, "spokenDataStr":Ljava/lang/String;
    const-string/jumbo v7, "time"

    invoke-static {p1, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 41
    .local v5, "time":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 42
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v7, "yyyy-MM-dd"

    invoke-direct {v3, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 43
    .local v3, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .end local v3    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_1
    const-string/jumbo v7, "location_enabled"

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_2

    if-eqz v1, :cond_4

    .line 47
    :cond_2
    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/OneRoundWeatherHandler;->getWeatherElement(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v6

    .line 48
    .local v6, "weatherElement":Lcom/vlingo/core/internal/weather/WeatherElement;
    if-eqz v6, :cond_3

    .line 49
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/OneRoundWeatherHandler;->getAdaptor()Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v7

    invoke-virtual {v7, v0, v1, v4, v6}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->runWithoutRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    .line 55
    .end local v6    # "weatherElement":Lcom/vlingo/core/internal/weather/WeatherElement;
    :cond_3
    :goto_0
    const/4 v7, 0x1

    return v7

    .line 52
    :cond_4
    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p0, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/OneRoundWeatherHandler;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0
.end method

.method public getWeatherElement(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;
    .locals 6
    .param p1, "weatherElementString"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 59
    const/4 v3, 0x0

    .line 60
    .local v3, "result":Lcom/vlingo/core/internal/weather/WeatherElement;
    const/4 v1, 0x0

    .line 62
    .local v1, "isXml":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/ByteArrayInputStream;

    const-string/jumbo v5, "UTF-8"

    invoke-virtual {p1, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .end local v1    # "isXml":Ljava/io/InputStream;
    .local v2, "isXml":Ljava/io/InputStream;
    invoke-static {v2}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->parse(Ljava/io/InputStream;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    .line 72
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getErrorMessage()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    move-object v1, v2

    .end local v2    # "isXml":Ljava/io/InputStream;
    .restart local v1    # "isXml":Ljava/io/InputStream;
    move-object v4, v3

    .line 78
    :goto_0
    return-object v4

    .line 63
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/OneRoundWeatherHandler;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 77
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v1    # "isXml":Ljava/io/InputStream;
    .restart local v2    # "isXml":Ljava/io/InputStream;
    :cond_0
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->showFailure()V

    move-object v1, v2

    .line 78
    .end local v2    # "isXml":Ljava/io/InputStream;
    .restart local v1    # "isXml":Ljava/io/InputStream;
    goto :goto_0
.end method
