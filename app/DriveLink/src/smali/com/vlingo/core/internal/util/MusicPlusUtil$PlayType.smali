.class public final enum Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;
.super Ljava/lang/Enum;
.source "MusicPlusUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/MusicPlusUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlayType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

.field public static final enum ALBUM:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

.field public static final enum ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

.field public static final enum ARTIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

.field public static final enum GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

.field public static final enum PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    const-string/jumbo v1, "GENERAL"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    new-instance v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    const-string/jumbo v1, "ARTIST"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ARTIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    new-instance v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    const-string/jumbo v1, "ALBUM"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUM:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    new-instance v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    const-string/jumbo v1, "PLAYLIST"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    new-instance v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    const-string/jumbo v1, "ALBUMART"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ARTIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUM:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->$VALUES:[Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->$VALUES:[Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    return-object v0
.end method
