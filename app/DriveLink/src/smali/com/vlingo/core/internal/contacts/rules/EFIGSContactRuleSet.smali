.class public Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;
.super Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
.source "EFIGSContactRuleSet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;-><init>()V

    return-void
.end method

.method private isEnglishInSettings()Z
    .locals 2

    .prologue
    .line 70
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "en-GB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "en-US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public canProcess(Ljava/lang/String;)Z
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "fr-FR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public generateRules(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 46
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 47
    const/4 v3, 0x0

    .line 66
    :goto_0
    return-object v3

    .line 50
    :cond_0
    const/4 v5, 0x3

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v5, "vnd.android.cursor.item/name"

    aput-object v5, v1, v10

    const-string/jumbo v5, "vnd.android.cursor.item/nickname"

    aput-object v5, v1, v11

    const/4 v5, 0x2

    const-string/jumbo v6, "vnd.android.cursor.item/organization"

    aput-object v6, v1, v5

    .line 51
    .local v1, "mimeType":[Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .local v3, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v6, "Full Match"

    const-string/jumbo v7, "vnd.android.cursor.item/name"

    new-array v8, v11, [Ljava/lang/String;

    const-string/jumbo v9, "data1"

    aput-object v9, v8, v10

    invoke-static {p1, v7, v8}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithTwoWayNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;->getFullMatchScorer()Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    const-string/jumbo v5, " "

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "rawWords":[Ljava/lang/String;
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 58
    .local v4, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v6, "SetMatchContactScore"

    const-string/jumbo v7, "data1"

    invoke-static {v4, v7, v1}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->appendBasicMatchToWhereClauseOnlyWithNorm(Ljava/util/List;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;->getPartialMatchScorer()Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;->isEnglishInSettings()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v0, "encoderClasses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Class<+Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;>;>;"
    const-class v5, Lcom/vlingo/core/internal/contacts/phoneticencoding/PrimaryDoubleMetaphonePhoneticEncoder;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v6, "DoubleMetaphoneContactSearch"

    new-array v7, v11, [Ljava/lang/String;

    const-string/jumbo v8, "data1"

    aput-object v8, v7, v10

    invoke-static {v4, v0, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->queryToWhereClauseWithEncoding(Ljava/util/List;Ljava/util/List;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;->getDoubleMetaphoneScore()Lcom/vlingo/core/internal/contacts/scoring/ContactScore;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    .end local v0    # "encoderClasses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Class<+Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;>;>;"
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->clearEncodersCachedValues()V

    goto/16 :goto_0
.end method

.method protected getDoubleMetaphoneScore()Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    .locals 3

    .prologue
    .line 82
    new-instance v0, Lcom/vlingo/core/internal/contacts/scoring/DoubleMetaphoneScore;

    const/16 v1, 0x64

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/scoring/DoubleMetaphoneScore;-><init>(II)V

    return-object v0
.end method

.method public getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;

    const/16 v1, 0x19

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;-><init>(II)V

    return-object v0
.end method

.method protected getFullMatchScorer()Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    return-object v0
.end method

.method protected getPartialMatchScorer()Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;

    const/16 v1, 0x5f

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/scoring/SetMatchContactScore;-><init>(II)V

    return-object v0
.end method

.method public skipExtraData()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method
