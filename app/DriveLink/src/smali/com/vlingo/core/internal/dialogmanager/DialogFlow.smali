.class public final Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
.super Ljava/lang/Object;
.source "DialogFlow.java"

# interfaces
.implements Lcom/vlingo/core/facade/dialogflow/IDialogFlow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/DialogFlow$6;,
        Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;,
        Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;,
        Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;,
        Lcom/vlingo/core/internal/dialogmanager/DialogFlow$LapTimer;,
        Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;
    }
.end annotation


# static fields
.field private static final EMBEDDED_APPS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;


# instance fields
.field private DelayProcessingActions:Z

.field private activityContext:Landroid/content/Context;

.field private alerts:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field private currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

.field private dialogData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DialogDataType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

.field private dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

.field private dialogState:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

.field private final dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

.field private isAboutToStartUserFlowWithMic:Z

.field private isProcessingActions:Z

.field private isSilentMode:Z

.field private lastEmbeddedAppWidgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field protected mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field private mResponseTimer:Lcom/vlingo/core/internal/util/PrecisionTimer;

.field private pendingClientTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

.field private pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

.field private pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

.field private preservedState:Ljava/lang/Object;

.field private recoState:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

.field private final taskQueueListener:Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;

.field private taskQueueMutex:Ljava/lang/Object;

.field private taskRegulators:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;",
            ">;>;"
        }
    .end annotation
.end field

.field private userProperties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final vlRecognitionListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

.field private wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

.field private wakeUpContext:Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

.field private widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

.field private final widgetSpecificProperties:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    .line 159
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->EMBEDDED_APPS:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    .line 135
    iput-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->DelayProcessingActions:Z

    .line 137
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    .line 141
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    .line 147
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$VLRecognitionListenerImpl;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->vlRecognitionListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 148
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueListener:Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;

    .line 149
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    .line 152
    iput-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isSilentMode:Z

    .line 153
    iput-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isAboutToStartUserFlowWithMic:Z

    .line 156
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogState:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    .line 158
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->lastEmbeddedAppWidgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 164
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetSpecificProperties:Ljava/util/EnumMap;

    .line 1688
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 178
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DefaultDialogFlowListener;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/DefaultDialogFlowListener;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    .line 179
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskRegulators:Ljava/util/Map;

    .line 180
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getWakeLockManager()Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    .line 181
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogData:Ljava/util/Map;

    .line 182
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->initQueue()V

    .line 183
    return-void
.end method

.method static synthetic access$1000(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->resetState()V

    return-void
.end method

.method static synthetic access$1100(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p1, "x1"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->listen(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishTurn()V

    return-void
.end method

.method static synthetic access$1300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogData:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    return-object v0
.end method

.method static synthetic access$1600()Ljava/util/Map;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->EMBEDDED_APPS:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->setLastEmbeddedAppWidgetKey(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->sendTextRequest(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isAboutToStartUserFlowWithMic:Z

    return p1
.end method

.method static synthetic access$2102(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/sdk/recognition/VLRecognitionStates;)Lcom/vlingo/sdk/recognition/VLRecognitionStates;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->recoState:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->preserveState(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->notifyDialogBusy()V

    return-void
.end method

.method static synthetic access$2600(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->handleIdle(Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/display/WakeLockManager;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishActionListProcessing()V

    return-void
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/util/TaskQueue;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->createDMTransactionTask()V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    return v0
.end method

.method static synthetic access$802(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    return p1
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    return-object v0
.end method

.method static synthetic access$902(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    return-object p1
.end method

.method private clearPreservedFieldID()V
    .locals 3

    .prologue
    .line 403
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PREVIOUS_FIELD_ID:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 404
    return-void
.end method

.method private cloneState()Ljava/util/HashMap;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DialogDataType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1200
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1201
    .local v1, "clone":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;>;"
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogData:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1202
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 1203
    .local v5, "key":Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    .line 1232
    .local v6, "value":Ljava/lang/Object;
    if-eqz v6, :cond_0

    .line 1233
    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$6;->$SwitchMap$com$vlingo$core$internal$dialogmanager$DialogDataType:[I

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 1258
    .end local v6    # "value":Ljava/lang/Object;
    :goto_1
    invoke-virtual {v1, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1236
    .restart local v6    # "value":Ljava/lang/Object;
    :pswitch_0
    :try_start_0
    move-object v0, v6

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-object v7, v0

    invoke-virtual {v7}, Lcom/vlingo/core/internal/dialogmanager/Controller;->clone()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .local v6, "value":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;
    goto :goto_1

    .line 1237
    .local v6, "value":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 1238
    .local v2, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v2}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto :goto_1

    .line 1242
    .end local v2    # "e":Ljava/lang/CloneNotSupportedException;
    :pswitch_1
    check-cast v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local v6    # "value":Ljava/lang/Object;
    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;->clone()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v6

    .line 1243
    .local v6, "value":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto :goto_1

    .line 1245
    .local v6, "value":Ljava/lang/Object;
    :pswitch_2
    check-cast v6, Ljava/util/List;

    .end local v6    # "value":Ljava/lang/Object;
    invoke-static {v6}, Lcom/vlingo/core/internal/util/Alarm;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 1246
    .local v6, "value":Ljava/util/List;
    goto :goto_1

    .line 1248
    .local v6, "value":Ljava/lang/Object;
    :pswitch_3
    check-cast v6, Ljava/util/List;

    .end local v6    # "value":Ljava/lang/Object;
    invoke-static {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 1249
    .local v6, "value":Ljava/util/List;
    goto :goto_1

    .line 1251
    .local v6, "value":Ljava/lang/Object;
    :pswitch_4
    check-cast v6, Ljava/util/List;

    .end local v6    # "value":Ljava/lang/Object;
    invoke-static {v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 1252
    .local v6, "value":Ljava/util/List;
    goto :goto_1

    .line 1254
    .local v6, "value":Ljava/lang/Object;
    :pswitch_5
    check-cast v6, Ljava/util/List;

    .end local v6    # "value":Ljava/lang/Object;
    invoke-static {v6}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .local v6, "value":Ljava/util/List;
    goto :goto_1

    .line 1261
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;>;"
    .end local v5    # "key":Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .end local v6    # "value":Ljava/util/List;
    :cond_1
    return-object v1

    .line 1233
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private createDMTransactionTask()V
    .locals 4

    .prologue
    .line 1075
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    if-nez v0, :cond_0

    .line 1076
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->vlRecognitionListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getAllUserProperties()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/sdk/recognition/VLRecognitionListener;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 1078
    :cond_0
    return-void
.end method

.method private finishActionListProcessing()V
    .locals 3

    .prologue
    .line 1010
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 1011
    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "finishActionListProcessing()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->preserveState(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 1015
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    .line 1016
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    if-eqz v0, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 1018
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 1025
    :goto_0
    monitor-exit v1

    .line 1026
    return-void

    .line 1023
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->handleIdle(Z)V

    goto :goto_0

    .line 1025
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private finishTurn()V
    .locals 2

    .prologue
    .line 820
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "finishTurn()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    .line 827
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->handleIdle(Z)V

    .line 828
    return-void
.end method

.method private getAllUserProperties()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1859
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1860
    .local v0, "allProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userProperties:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 1861
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userProperties:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1863
    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeUpContext:Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    if-eqz v3, :cond_1

    .line 1864
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeUpContext:Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;->getConfigurationHeaders()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1867
    :cond_1
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->lastEmbeddedAppWidgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetSpecificProperties:Ljava/util/EnumMap;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->lastEmbeddedAppWidgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v3, v4}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1868
    const-string/jumbo v3, "LastEmbeddedAppWidget"

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->EMBEDDED_APPS:Ljava/util/Map;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->lastEmbeddedAppWidgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1871
    :cond_2
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->EMBEDDED_APPS:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 1872
    .local v2, "key":Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetSpecificProperties:Ljava/util/EnumMap;

    invoke-virtual {v3, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1873
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetSpecificProperties:Ljava/util/EnumMap;

    invoke-virtual {v3, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0

    .line 1877
    .end local v2    # "key":Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    :cond_4
    return-object v0
.end method

.method private getAudioMode()I
    .locals 3

    .prologue
    .line 1803
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1804
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    return v1
.end method

.method public static getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .locals 1

    .prologue
    .line 168
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->instance:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    if-nez v0, :cond_0

    .line 169
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->instance:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .line 171
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->instance:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    return-object v0
.end method

.method private getTelephonyMode()I
    .locals 3

    .prologue
    .line 1808
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1809
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    return v1
.end method

.method private handleIdle(Z)V
    .locals 2
    .param p1, "force"    # Z

    .prologue
    .line 1081
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "handleIdle"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    .line 1084
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_2

    .line 1085
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->startSafeReaderFlow()V

    .line 1093
    :cond_1
    :goto_0
    return-void

    .line 1087
    :cond_2
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "reporting idle..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1089
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->onDialogIdle()V

    .line 1090
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->notifyDialogIdle()V

    goto :goto_0
.end method

.method private initDMTransationTask(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 3
    .param p1, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 963
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->createDMTransactionTask()V

    .line 965
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    .line 966
    .local v0, "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->clearPreservedFieldID()V

    .line 967
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getAllUserProperties()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v0, p1, v2}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performReco(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/audio/MicrophoneStream;Ljava/util/Map;)V

    .line 968
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeUpContext:Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    .line 969
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "userProperties sent to reco task"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    return-void
.end method

.method private initDMTransationTaskForTests()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 973
    invoke-static {}, Lcom/vlingo/core/internal/debug/UttsProvider;->getInstance()Lcom/vlingo/core/internal/debug/UttsProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/debug/UttsProvider;->getNextUtt()Lcom/vlingo/core/internal/debug/actions/Utt;

    move-result-object v3

    .line 974
    .local v3, "utt":Lcom/vlingo/core/internal/debug/actions/Utt;
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "initDMTransationTaskForTests() utt type="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/vlingo/core/internal/debug/actions/Utt;->getType()Lcom/vlingo/core/internal/debug/actions/Utt$Type;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    .line 976
    .local v1, "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$6;->$SwitchMap$com$vlingo$core$internal$debug$actions$Utt$Type:[I

    invoke-virtual {v3}, Lcom/vlingo/core/internal/debug/actions/Utt;->getType()Lcom/vlingo/core/internal/debug/actions/Utt$Type;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/debug/actions/Utt$Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 998
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "Invalid utt type"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 978
    :pswitch_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->createDMTransactionTask()V

    move-object v0, v3

    .line 980
    check-cast v0, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;

    .line 981
    .local v0, "audioFileUtt":Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;->getFileName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;->getFileName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 984
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "Audio file is not provided for automation test"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 986
    :cond_1
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;->getAudioType()Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    move-result-object v6

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getAllUserProperties()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v4, v1, v5, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performAudioFileReco(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;Ljava/util/Map;)V

    .line 987
    iput-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeUpContext:Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    .line 1001
    .end local v0    # "audioFileUtt":Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;
    :goto_0
    return-void

    .line 990
    :pswitch_1
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->createDMTransactionTask()V

    .line 992
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->clearPreservedFieldID()V

    move-object v2, v3

    .line 993
    check-cast v2, Lcom/vlingo/core/internal/debug/actions/TextUtt;

    .line 994
    .local v2, "textUtt":Lcom/vlingo/core/internal/debug/actions/TextUtt;
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/debug/actions/TextUtt;->getUtt()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getAllUserProperties()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v4, v1, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendTextRequest(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Ljava/lang/String;Ljava/util/Map;)V

    .line 995
    iput-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeUpContext:Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    goto :goto_0

    .line 976
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initQueue()V
    .locals 3

    .prologue
    .line 1064
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "initQueue()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    new-instance v0, Lcom/vlingo/core/internal/util/TaskQueue;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueListener:Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/util/TaskQueue;-><init>(Landroid/os/Handler;Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    .line 1067
    return-void
.end method

.method private listen(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z
    .locals 7
    .param p1, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 911
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "listen()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->onInterceptStartReco()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 959
    :cond_0
    :goto_0
    return v3

    .line 916
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v5

    if-nez v5, :cond_2

    .line 918
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "listen() return false because supportsSVoiceAssociatedServiceOnly and an app"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 923
    :cond_2
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v0

    .line 924
    .local v0, "checkPhoneInUse":Z
    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getTelephonyMode()I

    move-result v5

    if-eqz v5, :cond_6

    .line 925
    :cond_3
    if-eqz v0, :cond_4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 928
    .local v2, "msg":Ljava/lang/String;
    :goto_1
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    if-eqz v5, :cond_5

    .line 929
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    invoke-static {v5, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 925
    .end local v2    # "msg":Ljava/lang/String;
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_mic_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 930
    .restart local v2    # "msg":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getTelephonyMode()I

    move-result v5

    if-eqz v5, :cond_0

    .line 932
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 940
    .end local v2    # "msg":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;

    move-result-object v1

    .line 944
    .local v1, "grammarContext":Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v5

    .line 945
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/debug/TestWrapper;->isExecuteFromAutomationTests()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-static {}, Lcom/vlingo/core/internal/debug/UttsProvider;->getInstance()Lcom/vlingo/core/internal/debug/UttsProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/debug/UttsProvider;->isUttsEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 946
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->initDMTransationTaskForTests()V

    .line 951
    :goto_2
    iget-boolean v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    if-nez v3, :cond_8

    .line 952
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "listen: queueing task ..."

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 953
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-virtual {v3, v6}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 954
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 958
    :goto_3
    monitor-exit v5

    move v3, v4

    .line 959
    goto/16 :goto_0

    .line 948
    :cond_7
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->initDMTransationTask(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    goto :goto_2

    .line 958
    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 956
    :cond_8
    :try_start_1
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "listen: actions in process, task not queued"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method private notifyDialogBusy()V
    .locals 3

    .prologue
    .line 373
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogState:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    if-ne v0, v1, :cond_0

    .line 374
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$2;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$2;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 379
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->BUSY:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogState:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    .line 383
    :goto_0
    return-void

    .line 381
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Only transition Dialog Flow to busy if idle, not: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogState:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private notifyDialogIdle()V
    .locals 3

    .prologue
    .line 386
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogState:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->BUSY:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    if-ne v0, v1, :cond_1

    .line 387
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V

    .line 388
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogState:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    .line 390
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isBMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Only transition Dialog Flow to idle if busy, not "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogState:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private preserveState(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 4
    .param p1, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 1187
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cloneState()Ljava/util/HashMap;

    move-result-object v0

    .line 1188
    .local v0, "clone":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;>;"
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PREVIOUS_FIELD_ID:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1189
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->preservedState:Ljava/lang/Object;

    .line 1190
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "preserved state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1191
    return-void
.end method

.method private resetState()V
    .locals 1

    .prologue
    .line 1070
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogData:Ljava/util/Map;

    .line 1071
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->preservedState:Ljava/lang/Object;

    .line 1072
    return-void
.end method

.method private restoreState(Ljava/lang/Object;)V
    .locals 3
    .param p1, "state"    # Ljava/lang/Object;

    .prologue
    .line 1180
    if-eqz p1, :cond_0

    .line 1181
    check-cast p1, Ljava/util/HashMap;

    .end local p1    # "state":Ljava/lang/Object;
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogData:Ljava/util/Map;

    .line 1182
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "restored state to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1184
    :cond_0
    return-void
.end method

.method private sendAudioFileRequest(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Z
    .locals 6
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "format"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .prologue
    const/4 v2, 0x1

    .line 881
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "sendAudioFileRequest()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 884
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 885
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 887
    .local v1, "msg":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    invoke-static {v3, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 888
    const/4 v2, 0x0

    .line 907
    .end local v1    # "msg":Ljava/lang/String;
    :goto_0
    return v2

    .line 891
    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v3

    .line 892
    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->createDMTransactionTask()V

    .line 894
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    .line 895
    .local v0, "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->clearPreservedFieldID()V

    .line 896
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getAllUserProperties()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v4, v0, p1, p2, v5}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->performAudioFileReco(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;Ljava/util/Map;)V

    .line 897
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeUpContext:Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    .line 899
    iget-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    if-nez v4, :cond_1

    .line 900
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "sendTextTaggingRequest: queueing task ..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 902
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 906
    :goto_1
    monitor-exit v3

    goto :goto_0

    .end local v0    # "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 904
    .restart local v0    # "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    :cond_1
    :try_start_1
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "sendTextTaggingRequest: actions in process, task not queued"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private sendTextRequest(Ljava/lang/String;)Z
    .locals 6
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 850
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "sendTextRequest()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 854
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 856
    .local v1, "msg":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    invoke-static {v3, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 857
    const/4 v2, 0x0

    .line 876
    .end local v1    # "msg":Ljava/lang/String;
    :goto_0
    return v2

    .line 860
    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v3

    .line 861
    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->createDMTransactionTask()V

    .line 863
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    .line 864
    .local v0, "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->clearPreservedFieldID()V

    .line 865
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getAllUserProperties()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v4, v0, p1, v5}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->sendTextRequest(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Ljava/lang/String;Ljava/util/Map;)V

    .line 866
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeUpContext:Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    .line 868
    iget-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    if-nez v4, :cond_1

    .line 869
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "sendTextTaggingRequest: queueing task ..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 871
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 875
    :goto_1
    monitor-exit v3

    goto :goto_0

    .end local v0    # "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 873
    .restart local v0    # "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    :cond_1
    :try_start_1
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "sendTextTaggingRequest: actions in process, task not queued"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private setLastEmbeddedAppWidgetKey(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V
    .locals 0
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .prologue
    .line 1821
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->lastEmbeddedAppWidgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 1822
    return-void
.end method


# virtual methods
.method public addUserProperties(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 585
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userProperties:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userProperties:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 587
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "userProperties updated with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    :goto_0
    return-void

    .line 589
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " failed on adding to userProperties"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p2, "propName"    # Ljava/lang/String;
    .param p3, "propValue"    # Ljava/lang/String;

    .prologue
    .line 1834
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetSpecificProperties:Ljava/util/EnumMap;

    invoke-virtual {v1, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1835
    .local v0, "propMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v0, :cond_0

    .line 1836
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "propMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1837
    .restart local v0    # "propMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetSpecificProperties:Ljava/util/EnumMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1839
    :cond_0
    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1840
    return-void
.end method

.method public cancelAudio()V
    .locals 2

    .prologue
    .line 727
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "cancelAudio()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stop()V

    .line 730
    return-void
.end method

.method public cancelDialog()V
    .locals 2

    .prologue
    .line 770
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "cancelDialog()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 773
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishDialog()V

    .line 774
    return-void
.end method

.method public cancelTTS()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 722
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 723
    return-void
.end method

.method public cancelTurn()V
    .locals 1

    .prologue
    .line 781
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn(Z)V

    .line 782
    return-void
.end method

.method public cancelTurn(Z)V
    .locals 5
    .param p1, "skipRestoreState"    # Z

    .prologue
    .line 786
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "cancelTurn()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 789
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    .line 790
    .local v0, "isIdle":Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    .line 791
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 792
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v1, :cond_0

    .line 793
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->cancel()V

    .line 796
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getPreservedState()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    if-nez v0, :cond_0

    .line 798
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getPreservedState()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->restoreState(Ljava/lang/Object;)V

    .line 799
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->RECOGNITION_RESULT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-virtual {v3, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->setResult(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    .line 802
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 804
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishTurn()V

    .line 805
    return-void

    .line 802
    .end local v0    # "isIdle":Z
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public clearPendingSafeReaderTurn()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1172
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 1173
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    .line 1174
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 1175
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    .line 1177
    :cond_0
    return-void
.end method

.method public deleteQueuedAudioTasks()V
    .locals 2

    .prologue
    .line 748
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/TaskQueue;->deleteQueuedTaskOfType(Ljava/lang/Class;)V

    .line 749
    return-void
.end method

.method public deleteQueuedTtsTasks()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 744
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->deleteQueuedAudioTasks()V

    .line 745
    return-void
.end method

.method public endpointReco()V
    .locals 2

    .prologue
    .line 839
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "endpointReco()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 845
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognizer;->stopRecognition()V

    .line 847
    :cond_0
    return-void
.end method

.method public fakeResults(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V
    .locals 1
    .param p1, "results"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .prologue
    .line 1102
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->vlRecognitionListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onRecognitionResults(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    .line 1103
    return-void
.end method

.method public finishDialog()V
    .locals 2

    .prologue
    .line 832
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "finishDialog()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 834
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->resetState()V

    .line 835
    return-void
.end method

.method public getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 4

    .prologue
    .line 352
    const/4 v0, 0x0

    .line 353
    .local v0, "fieldID":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PREVIOUS_FIELD_ID:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "fieldID":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .line 354
    .restart local v0    # "fieldID":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    if-nez v0, :cond_1

    .line 355
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-nez v2, :cond_2

    .line 356
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getDefaultFieldId()Ljava/lang/String;

    move-result-object v1

    .line 357
    .local v1, "fieldIdString":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 358
    invoke-static {v1}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    .line 361
    :cond_0
    if-nez v0, :cond_1

    .line 362
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    .line 368
    .end local v1    # "fieldIdString":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v0

    .line 365
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreservedState()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1195
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "getPreservedState"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1196
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->preservedState:Ljava/lang/Object;

    return-object v0
.end method

.method public getRecoState()Lcom/vlingo/sdk/recognition/VLRecognitionStates;
    .locals 1

    .prologue
    .line 1817
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->recoState:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    return-object v0
.end method

.method public getTaskRegulators(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;)Ljava/util/Set;
    .locals 1
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 618
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskRegulators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method getWidgetFactory()Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;
    .locals 1

    .prologue
    .line 1097
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    return-object v0
.end method

.method public getWidgetSpecificProperties(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)Ljava/util/Map;
    .locals 2
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1849
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetSpecificProperties:Ljava/util/EnumMap;

    invoke-virtual {v1, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1850
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    return-object v0
.end method

.method public handleAlert(Ljava/util/LinkedList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1110
    .local p1, "alertsParam":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 1111
    :try_start_0
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "handleAlert()"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1112
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "alerts.isEmpty()="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1113
    move-object v1, p1

    .line 1114
    .local v1, "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1115
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 1116
    .local v0, "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1117
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Adding alert with id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1118
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1134
    .end local v0    # "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    .end local v1    # "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1122
    .restart local v1    # "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    :cond_1
    :try_start_1
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    .line 1124
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1125
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v6}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    invoke-direct {v3, v5, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;-><init>(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 1126
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$3;

    invoke-direct {v3, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$3;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    invoke-static {v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 1134
    :cond_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1135
    return-void
.end method

.method public handleAlert(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 8
    .param p2, "fieldId"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1143
    .local p1, "alertsParam":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 1144
    :try_start_0
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "handleAlert()"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "alerts.isEmpty()="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    move-object v1, p1

    .line 1147
    .local v1, "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1148
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 1149
    .local v0, "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1150
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Adding alert with id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1167
    .end local v0    # "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    .end local v1    # "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1155
    .restart local v1    # "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    :cond_1
    :try_start_1
    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    .line 1157
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1158
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    invoke-direct {v3, v5, p2, v6}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;-><init>(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 1159
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$4;

    invoke-direct {v3, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$4;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    invoke-static {v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 1167
    :cond_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1168
    return-void
.end method

.method public initFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V
    .locals 2
    .param p1, "activityContextParam"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    .param p4, "widgetFactoryParam"    # Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 212
    .local p3, "userPropertiesParam":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "initFlow()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    if-nez p1, :cond_0

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "activityContext cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_0
    if-nez p2, :cond_1

    .line 217
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    if-eq v0, p2, :cond_3

    .line 221
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 222
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    .line 223
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    .line 226
    :cond_3
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userProperties:Ljava/util/Map;

    .line 227
    iput-object p4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .line 228
    return-void
.end method

.method public interruptTurn()V
    .locals 2

    .prologue
    .line 810
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "interruptTurn()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 812
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/TaskQueue;->cancel()V

    .line 813
    monitor-exit v1

    .line 814
    return-void

    .line 813
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isAboutToStartUserFlowWithMic()Z
    .locals 1

    .prologue
    .line 348
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isAboutToStartUserFlowWithMic:Z

    return v0
.end method

.method public isIdle()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 648
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 649
    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "isIdle(), isProcessingActions="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", isRunningTask="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/TaskQueue;->isRunningTask()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", hasQueuedTask="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/TaskQueue;->hasQueuedTask()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->DelayProcessingActions:Z

    if-eqz v2, :cond_0

    .line 651
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->DelayProcessingActions:Z

    .line 652
    monitor-exit v1

    .line 657
    :goto_0
    return v0

    .line 654
    :cond_0
    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/TaskQueue;->isRunningTask()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/TaskQueue;->hasQueuedTask()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 655
    :cond_1
    monitor-exit v1

    goto :goto_0

    .line 658
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 657
    :cond_2
    const/4 v0, 0x1

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public isQueuedAudioTask()Z
    .locals 2

    .prologue
    .line 739
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/TaskQueue;->isQueuedTask(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public isQueuedTtsTask()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 734
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/TaskQueue;->isQueuedTask(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 1795
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isSilentMode:Z

    return v0
.end method

.method public manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 4
    .param p1, "fieldId"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    const/4 v3, 0x0

    .line 523
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v1, :cond_0

    .line 524
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->cancel()V

    .line 526
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 527
    const/4 v0, 0x0

    .line 528
    .local v0, "result":Lcom/vlingo/sdk/recognition/VLRecognitionResult;
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    invoke-direct {v1, v0, p1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingClientTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 530
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    invoke-interface {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showReceivedResults(Lcom/vlingo/core/internal/logging/EventLog;)V

    .line 532
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 533
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingClientTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 534
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingClientTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 535
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 536
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    .line 537
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->processClientFlow()V

    .line 539
    return-void

    .line 537
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public playMedia(I)V
    .locals 3
    .param p1, "fileResId"    # I

    .prologue
    .line 704
    if-lez p1, :cond_0

    .line 705
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-direct {v0, v1, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;-><init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;I)V

    .line 706
    .local v0, "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 707
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 708
    monitor-exit v2

    .line 710
    .end local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;
    :cond_0
    return-void

    .line 708
    .restart local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public queuePauseableTask(Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;)V
    .locals 2
    .param p1, "task"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;

    .prologue
    .line 714
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->vlRecognitionListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;->setVlRecognitionListener(Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V

    .line 715
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 716
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 717
    monitor-exit v1

    .line 718
    return-void

    .line 717
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public readoutDelay()J
    .locals 2

    .prologue
    .line 1733
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public registerTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V
    .locals 3
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;
    .param p2, "regulator"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    .prologue
    .line 543
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "registerTaskRegulator(), type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", regulator="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskRegulators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskRegulators:Ljava/util/Map;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskRegulators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 552
    return-void
.end method

.method public releaseFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    .prologue
    const/4 v2, 0x0

    .line 623
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "releaseFlow()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    if-eq p1, v0, :cond_1

    .line 626
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "ignoring releaseFlow() call, not current listener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    :cond_0
    :goto_0
    return-void

    .line 631
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 632
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->initQueue()V

    .line 634
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    .line 635
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DefaultDialogFlowListener;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/DefaultDialogFlowListener;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    .line 636
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userProperties:Ljava/util/Map;

    .line 639
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->lastEmbeddedAppWidgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 641
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 642
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognizer;->destroy()V

    goto :goto_0
.end method

.method public removeUserProperties(Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 571
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userProperties:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userProperties:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " removed from userProperties"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    :goto_0
    return-void

    .line 575
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " failed on removing from userProperties"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .prologue
    .line 1854
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetSpecificProperties:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1855
    return-void
.end method

.method public sendPendingEvents()V
    .locals 2

    .prologue
    .line 753
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sendPendingEvents()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getPendingEvents()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 756
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->createDMTransactionTask()V

    .line 757
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    if-nez v0, :cond_0

    .line 758
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 759
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 762
    :cond_0
    return-void
.end method

.method public setProcessingActions()V
    .locals 1

    .prologue
    .line 1813
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->DelayProcessingActions:Z

    .line 1814
    return-void
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "silentMode"    # Z

    .prologue
    .line 1800
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isSilentMode:Z

    .line 1801
    return-void
.end method

.method public setUserProperties(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 599
    .local p1, "newUserProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userProperties:Ljava/util/Map;

    .line 600
    return-void
.end method

.method public setWakeUpContext(Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;)V
    .locals 2
    .param p1, "wakeUpContext"    # Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    .prologue
    .line 604
    const-string/jumbo v0, "SEND_WAKEUP_WORD_HEADERS"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605
    if-eqz p1, :cond_0

    .line 608
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeUpContext:Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    .line 614
    :cond_0
    return-void
.end method

.method public startReplyFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V
    .locals 5
    .param p1, "fieldId"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .param p2, "message"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    const/4 v4, 0x0

    .line 478
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startSafeReaderFlow()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v1, :cond_0

    .line 484
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->cancel()V

    .line 487
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 489
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 490
    .local v0, "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 492
    if-eqz p1, :cond_1

    .line 493
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    invoke-direct {v1, v0, p1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;-><init>(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 498
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 499
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 500
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 501
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 502
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    .line 503
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 505
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isSilentMode()Z

    move-result v2

    invoke-virtual {v1, p2, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->processReplyMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Z)V

    .line 506
    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    .line 507
    return-void

    .line 495
    :cond_1
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    invoke-direct {v1, v0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;-><init>(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    goto :goto_0

    .line 503
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public startSafeReaderFlow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 411
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startSafeReaderFlow()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->cancel()V

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_1

    .line 421
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 424
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showReceivedResults(Lcom/vlingo/core/internal/logging/EventLog;)V

    .line 426
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 427
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 428
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 430
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    .line 431
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isSilentMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->processSafeReaderMessage(Z)V

    .line 433
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    .line 435
    :cond_1
    return-void

    .line 431
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public startSafeReaderFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V
    .locals 5
    .param p1, "fieldId"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .param p2, "message"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    const/4 v4, 0x0

    .line 444
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startSafeReaderFlow()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v1, :cond_0

    .line 450
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->cancel()V

    .line 453
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 455
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 456
    .local v0, "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 458
    if-eqz p1, :cond_1

    .line 459
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    invoke-direct {v1, v0, p1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;-><init>(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 464
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 465
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 466
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .line 467
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 468
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z

    .line 469
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isSilentMode()Z

    move-result v2

    invoke-virtual {v1, p2, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->processSafeReaderMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Z)V

    .line 472
    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->alerts:Ljava/util/LinkedList;

    .line 473
    return-void

    .line 461
    :cond_1
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    invoke-direct {v1, v0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;-><init>(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingSafeReaderTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    goto :goto_0

    .line 469
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z
    .locals 7
    .param p1, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    const/4 v6, 0x2

    .line 257
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isCarMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 258
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    .line 259
    .local v0, "baseContext":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    .line 260
    .local v2, "isBtHeadsetConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v1

    .line 261
    .local v1, "isBluetoothAudioSupported":Z
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 262
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v4, v5, v6}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 263
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startScoOnStartRecognition()V

    .line 269
    .end local v0    # "baseContext":Landroid/content/Context;
    .end local v1    # "isBluetoothAudioSupported":Z
    .end local v2    # "isBtHeadsetConnected":Z
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 270
    .local v3, "result":Z
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isAboutToStartUserFlowWithMic:Z

    .line 272
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "[LatencyCheck] startUserFlow() micStream="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v4, :cond_1

    .line 279
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->cancel()V

    .line 281
    :cond_1
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->listen(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    move-result v3

    .line 282
    iput-boolean v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isAboutToStartUserFlowWithMic:Z

    .line 283
    return v3

    .line 265
    .end local v3    # "result":Z
    .restart local v0    # "baseContext":Landroid/content/Context;
    .restart local v1    # "isBluetoothAudioSupported":Z
    .restart local v2    # "isBtHeadsetConnected":Z
    :cond_2
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v4, v5, v6}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    goto :goto_0
.end method

.method public startUserFlow(Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;Ljava/lang/String;)Z
    .locals 3
    .param p1, "fileFormat"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 326
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[LatencyCheck] startUserFlow() fileName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " format="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->cancel()V

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_1

    .line 336
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->RECOGNITION_RESULT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->setResult(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    .line 339
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->preserveState(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 342
    invoke-direct {p0, p2, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->sendAudioFileRequest(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Z

    move-result v0

    return v0
.end method

.method public startUserFlow(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "dialogStateParam"    # Ljava/lang/Object;

    .prologue
    .line 295
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[LatencyCheck] startUserFlow() text="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->cancel()V

    .line 303
    :cond_0
    if-eqz p2, :cond_1

    .line 304
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->restoreState(Ljava/lang/Object;)V

    .line 307
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-eqz v0, :cond_2

    .line 308
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->currentTurn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->RECOGNITION_RESULT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->setResult(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    .line 311
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->preserveState(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 314
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->sendTextRequest(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public stealFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V
    .locals 2
    .param p1, "activityContextParam"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    .param p4, "widgetFactoryParam"    # Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 231
    .local p3, "userPropertiesParam":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stealFlow()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    if-nez p1, :cond_0

    .line 234
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "activityContext cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_0
    if-nez p2, :cond_1

    .line 236
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    if-eq v0, p2, :cond_3

    .line 240
    :cond_2
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;

    .line 241
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    .line 244
    :cond_3
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userProperties:Ljava/util/Map;

    .line 245
    iput-object p4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .line 246
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1742
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogData:Ljava/util/Map;

    if-nez v7, :cond_0

    .line 1743
    const-string/jumbo v7, ""

    .line 1790
    :goto_0
    return-object v7

    .line 1746
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1748
    .local v5, "sb":Ljava/lang/StringBuilder;
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogData:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1749
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;>;"
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-nez v7, :cond_2

    .line 1750
    const-string/jumbo v7, "{"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1754
    :goto_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 1755
    .local v3, "key":Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    .line 1756
    .local v6, "value":Ljava/lang/Object;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1757
    if-eqz v6, :cond_3

    .line 1758
    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$6;->$SwitchMap$com$vlingo$core$internal$dialogmanager$DialogDataType:[I

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 1780
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1752
    .end local v3    # "key":Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .end local v6    # "value":Ljava/lang/Object;
    :cond_2
    const-string/jumbo v7, ","

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1760
    .restart local v3    # "key":Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .restart local v6    # "value":Ljava/lang/Object;
    :pswitch_0
    check-cast v6, Ljava/util/List;

    .end local v6    # "value":Ljava/lang/Object;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/util/Alarm;

    .line 1761
    .local v4, "o":Lcom/vlingo/core/internal/util/Alarm;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/Alarm;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1765
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "o":Lcom/vlingo/core/internal/util/Alarm;
    .restart local v6    # "value":Ljava/lang/Object;
    :pswitch_1
    check-cast v6, Ljava/util/List;

    .end local v6    # "value":Ljava/lang/Object;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 1766
    .local v4, "o":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1770
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "o":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .restart local v6    # "value":Ljava/lang/Object;
    :pswitch_2
    check-cast v6, Ljava/util/List;

    .end local v6    # "value":Ljava/lang/Object;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1771
    .local v4, "o":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 1775
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "o":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v6    # "value":Ljava/lang/Object;
    :pswitch_3
    check-cast v6, Ljava/util/List;

    .end local v6    # "value":Ljava/lang/Object;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 1776
    .local v4, "o":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1783
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "o":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .restart local v6    # "value":Ljava/lang/Object;
    :cond_3
    const-string/jumbo v7, "null"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 1786
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;>;"
    .end local v3    # "key":Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .end local v6    # "value":Ljava/lang/Object;
    :cond_4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-eqz v7, :cond_5

    .line 1787
    const-string/jumbo v7, "}"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1790
    :cond_5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1758
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public tts(Ljava/lang/String;)V
    .locals 3
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 663
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 664
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-direct {v0, v1, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;-><init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Ljava/lang/String;)V

    .line 665
    .local v0, "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 666
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 667
    monitor-exit v2

    .line 669
    .end local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    :cond_0
    return-void

    .line 667
    .restart local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 3
    .param p1, "tts"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 673
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 674
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    invoke-direct {v0, p2, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;-><init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Ljava/lang/String;)V

    .line 675
    .local v0, "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 676
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 677
    monitor-exit v2

    .line 679
    .end local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    :cond_0
    return-void

    .line 677
    .restart local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public ttsAnyway(Ljava/lang/String;)V
    .locals 3
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 683
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 684
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-direct {v0, v1, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;-><init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Ljava/lang/String;)V

    .line 685
    .local v0, "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->setTTSAnyway(Z)V

    .line 686
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 687
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 688
    monitor-exit v2

    .line 690
    .end local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    :cond_0
    return-void

    .line 688
    .restart local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public ttsAnyway(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 3
    .param p1, "tts"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 693
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 694
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    invoke-direct {v0, p2, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;-><init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Ljava/lang/String;)V

    .line 695
    .local v0, "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->setTTSAnyway(Z)V

    .line 696
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 697
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 698
    monitor-exit v2

    .line 700
    .end local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    :cond_0
    return-void

    .line 698
    .restart local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V
    .locals 3
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;
    .param p2, "regulator"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    .prologue
    .line 556
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterTaskFlowRegulator(), type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskRegulators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    if-eqz p2, :cond_0

    .line 560
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskRegulators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 563
    :cond_0
    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 1738
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->userCancel()V

    .line 1739
    return-void
.end method
