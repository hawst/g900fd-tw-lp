.class public abstract Lcom/vlingo/core/internal/BaseResourceIdProvider;
.super Ljava/lang/Object;
.source "BaseResourceIdProvider.java"

# interfaces
.implements Lcom/vlingo/core/internal/ResourceIdProvider;


# static fields
.field private static final NOT_FOUND_ID:I = -0x1


# instance fields
.field private arrayMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/ResourceIdProvider$array;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private drawableMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/ResourceIdProvider$drawable;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private idMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/ResourceIdProvider$id;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private layoutMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/ResourceIdProvider$layout;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private rawMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/ResourceIdProvider$raw;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private stringMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/ResourceIdProvider$string;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-static {}, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->values()[Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->drawableMap:Ljava/util/Map;

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-static {}, Lcom/vlingo/core/internal/ResourceIdProvider$id;->values()[Lcom/vlingo/core/internal/ResourceIdProvider$id;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->idMap:Ljava/util/Map;

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-static {}, Lcom/vlingo/core/internal/ResourceIdProvider$layout;->values()[Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->layoutMap:Ljava/util/Map;

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-static {}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->values()[Lcom/vlingo/core/internal/ResourceIdProvider$string;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->stringMap:Ljava/util/Map;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-static {}, Lcom/vlingo/core/internal/ResourceIdProvider$array;->values()[Lcom/vlingo/core/internal/ResourceIdProvider$array;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->arrayMap:Ljava/util/Map;

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-static {}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->values()[Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->rawMap:Ljava/util/Map;

    .line 32
    invoke-virtual {p0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->initDrawableMap()V

    .line 33
    invoke-virtual {p0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->initIdMap()V

    .line 34
    invoke-virtual {p0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->initLayoutMap()V

    .line 35
    invoke-virtual {p0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->initStringMap()V

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->initArrayMap()V

    .line 37
    invoke-virtual {p0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->initRawMap()V

    .line 38
    return-void
.end method

.method private getIntValue(Ljava/lang/Integer;)I
    .locals 1
    .param p1, "value"    # Ljava/lang/Integer;

    .prologue
    .line 203
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public getDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)I

    move-result v0

    .line 123
    .local v0, "resourceId":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 124
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 126
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$array;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->arrayMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->getIntValue(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public final getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->drawableMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->getIntValue(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public final getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$id;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$id;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->idMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->getIntValue(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public final getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$layout;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->layoutMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->getIntValue(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public final getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->rawMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->getIntValue(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public final getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->stringMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->getIntValue(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public final getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 102
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v0

    .line 103
    .local v0, "resourceId":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 104
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 106
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public varargs getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 112
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v0

    .line 113
    .local v0, "resourceId":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 114
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 116
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/BaseResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$array;)I

    move-result v0

    .line 133
    .local v0, "resourceId":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 134
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 136
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected abstract initArrayMap()V
.end method

.method protected abstract initDrawableMap()V
.end method

.method protected abstract initIdMap()V
.end method

.method protected abstract initLayoutMap()V
.end method

.method protected abstract initRawMap()V
.end method

.method protected abstract initStringMap()V
.end method

.method protected final putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$array;
    .param p2, "value"    # Ljava/lang/Integer;

    .prologue
    .line 189
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->arrayMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    return-void
.end method

.method protected final putDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$drawable;
    .param p2, "value"    # Ljava/lang/Integer;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->drawableMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    return-void
.end method

.method protected final putId(Lcom/vlingo/core/internal/ResourceIdProvider$id;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$id;
    .param p2, "value"    # Ljava/lang/Integer;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->idMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    return-void
.end method

.method protected final putLayout(Lcom/vlingo/core/internal/ResourceIdProvider$layout;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$layout;
    .param p2, "value"    # Ljava/lang/Integer;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->layoutMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    return-void
.end method

.method protected final putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$raw;
    .param p2, "value"    # Ljava/lang/Integer;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->rawMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    return-void
.end method

.method protected final putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p2, "value"    # Ljava/lang/Integer;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/vlingo/core/internal/BaseResourceIdProvider;->stringMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    return-void
.end method
