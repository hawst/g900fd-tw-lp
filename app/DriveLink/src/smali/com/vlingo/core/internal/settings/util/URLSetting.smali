.class public abstract Lcom/vlingo/core/internal/settings/util/URLSetting;
.super Lcom/vlingo/core/internal/settings/util/StringSetting;
.source "URLSetting.java"


# instance fields
.field url:Lcom/vlingo/core/internal/http/URL;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/settings/util/StringSetting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/settings/util/URLSetting;->url:Lcom/vlingo/core/internal/http/URL;

    .line 23
    return-void
.end method

.method private updateDetails()V
    .locals 2

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/vlingo/core/internal/settings/util/URLSetting;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/settings/util/URLSetting;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/http/URL;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/settings/util/URLSetting;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/http/URL;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/settings/util/URLSetting;->url:Lcom/vlingo/core/internal/http/URL;

    .line 28
    :cond_0
    return-void
.end method


# virtual methods
.method public getURL()Lcom/vlingo/core/internal/http/URL;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/URLSetting;->url:Lcom/vlingo/core/internal/http/URL;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/core/internal/settings/util/URLSetting;->updateDetails()V

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/URLSetting;->url:Lcom/vlingo/core/internal/http/URL;

    return-object v0
.end method

.method public setValue(Lcom/vlingo/core/internal/http/URL;)V
    .locals 1
    .param p1, "details"    # Lcom/vlingo/core/internal/http/URL;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/core/internal/settings/util/URLSetting;->url:Lcom/vlingo/core/internal/http/URL;

    .line 37
    invoke-virtual {p1}, Lcom/vlingo/core/internal/http/URL;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/vlingo/core/internal/settings/util/StringSetting;->setValueInternal(Ljava/lang/Object;)V

    .line 38
    return-void
.end method
