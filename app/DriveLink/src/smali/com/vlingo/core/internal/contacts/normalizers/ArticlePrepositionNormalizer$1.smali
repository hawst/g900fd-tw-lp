.class final Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;
.super Ljava/util/HashMap;
.source "ArticlePrepositionNormalizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/util/regex/Pattern;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 15
    const-string/jumbo v0, "mon"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    const-string/jumbo v0, "ma"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    const-string/jumbo v0, "la"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    const-string/jumbo v0, "el"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    const-string/jumbo v0, "de"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    const-string/jumbo v0, "en"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    const-string/jumbo v0, "lea"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    const-string/jumbo v0, "les"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    const-string/jumbo v0, "le"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    const-string/jumbo v0, "au"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    const-string/jumbo v0, "d\'"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    const-string/jumbo v0, "l\'"

    # invokes: Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    return-void
.end method
