.class public abstract Lcom/vlingo/midas/provider/BasicConfigProvider;
.super Landroid/content/ContentProvider;
.source "BasicConfigProvider.java"


# static fields
.field protected static final AUTHORITY:Ljava/lang/String;

.field public static final CONTENT_URI:Ljava/lang/String;

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final VALUE:Ljava/lang/String; = "value"

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/vlingo/midas/provider/BasicConfigProvider;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/provider/BasicConfigProvider;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 17
    sget-object v0, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_AUTHORITY:Ljava/lang/String;

    sput-object v0, Lcom/vlingo/midas/provider/BasicConfigProvider;->AUTHORITY:Ljava/lang/String;

    .line 18
    sget-object v0, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_URI:Ljava/lang/String;

    sput-object v0, Lcom/vlingo/midas/provider/BasicConfigProvider;->CONTENT_URI:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 26
    sget-object v2, Lcom/vlingo/midas/provider/BasicConfigProvider;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "VlingoConfigProvider.update(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 27
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "/SETTINGS/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 28
    invoke-virtual {p2}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 43
    :cond_1
    const/4 v2, 0x0

    return v2

    .line 28
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 29
    .local v0, "key":Ljava/lang/String;
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 30
    .local v1, "value":Ljava/lang/Object;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    if-nez v3, :cond_3

    .line 31
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/midas/provider/BasicConfigProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->init(Landroid/content/Context;)V

    .line 34
    :cond_3
    instance-of v3, v1, Ljava/lang/Boolean;

    if-eqz v3, :cond_4

    .line 35
    check-cast v1, Ljava/lang/Boolean;

    .end local v1    # "value":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 36
    .restart local v1    # "value":Ljava/lang/Object;
    :cond_4
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 37
    check-cast v1, Ljava/lang/String;

    .end local v1    # "value":Ljava/lang/Object;
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 38
    .restart local v1    # "value":Ljava/lang/Object;
    :cond_5
    instance-of v3, v1, Ljava/lang/Integer;

    if-eqz v3, :cond_0

    .line 39
    check-cast v1, Ljava/lang/Integer;

    .end local v1    # "value":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_0
.end method
