.class public Lcom/vlingo/midas/naver/NaverAdaptor;
.super Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
.source "NaverAdaptor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;
    }
.end annotation


# static fields
.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field private content:Ljava/lang/String;

.field private currentRequest:Ljava/lang/String;

.field private failReason:Ljava/lang/String;

.field private isRequestComplete:Z

.field private isRequestFailed:Z

.field private final lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

.field m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

.field private naverXML:Lcom/vlingo/midas/naver/NaverXMLParser;

.field private responseParser:Lcom/vlingo/core/internal/naver/NaverResponseParser;

.field private type:Ljava/lang/String;

.field private vvsActionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field private widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;-><init>()V

    .line 24
    iput-boolean v1, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestComplete:Z

    .line 25
    iput-boolean v1, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestFailed:Z

    .line 26
    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->failReason:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 34
    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    .line 39
    iput-boolean v1, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestComplete:Z

    .line 40
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->currentRequest:Ljava/lang/String;

    .line 41
    return-void
.end method

.method static synthetic access$0()Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-object v0
.end method


# virtual methods
.method cancelCurrentRequest()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->cancel()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 95
    :cond_0
    return-void
.end method

.method public executeSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "search"    # Ljava/lang/String;
    .param p2, "spokenLocation"    # Ljava/lang/String;
    .param p3, "region"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/midas/naver/NaverAdaptor;->sendNaverRequest(Ljava/lang/String;Z)V

    .line 49
    return-void
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getFailReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->failReason:Ljava/lang/String;

    return-object v0
.end method

.method public getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->naverXML:Lcom/vlingo/midas/naver/NaverXMLParser;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getVVSActionHandlerListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->vvsActionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    return-object v0
.end method

.method public getWidgetListener()Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    return-object v0
.end method

.method public isRequestComplete()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestComplete:Z

    return v0
.end method

.method public isRequestFailed()Z
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestFailed:Z

    return v0
.end method

.method public onRequestComplete(ZLjava/lang/Object;)V
    .locals 2
    .param p1, "success"    # Z
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestComplete:Z

    .line 100
    if-eqz p1, :cond_2

    .line 102
    sget-object v0, Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v1, "Local Search Request Successful!"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 103
    instance-of v0, p2, Lcom/vlingo/core/internal/naver/NaverResponseParser;

    if-eqz v0, :cond_0

    .line 104
    check-cast p2, Lcom/vlingo/core/internal/naver/NaverResponseParser;

    .end local p2    # "item":Ljava/lang/Object;
    iput-object p2, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->responseParser:Lcom/vlingo/core/internal/naver/NaverResponseParser;

    .line 105
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->responseParser:Lcom/vlingo/core/internal/naver/NaverResponseParser;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->getRawResponseXML()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->unescapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverAdaptor;->setContent(Ljava/lang/String;)V

    .line 106
    new-instance v0, Lcom/vlingo/midas/naver/NaverXMLParser;

    invoke-direct {v0}, Lcom/vlingo/midas/naver/NaverXMLParser;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->naverXML:Lcom/vlingo/midas/naver/NaverXMLParser;

    .line 107
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->naverXML:Lcom/vlingo/midas/naver/NaverXMLParser;

    iget-object v1, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->responseParser:Lcom/vlingo/core/internal/naver/NaverResponseParser;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->getRawResponseXML()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/naver/NaverXMLParser;->parse(Ljava/lang/String;)V

    .line 115
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onResponseReceived()V

    .line 118
    :cond_1
    return-void

    .line 111
    .restart local p2    # "item":Ljava/lang/Object;
    :cond_2
    sget-object v0, Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v1, "Local Search Request failed "

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    goto :goto_0
.end method

.method public onRequestFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "failedReason"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 122
    iput-boolean v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestComplete:Z

    .line 123
    iput-boolean v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestFailed:Z

    .line 124
    iput-object p1, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->failReason:Ljava/lang/String;

    .line 126
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    .line 129
    :cond_0
    return-void
.end method

.method public onRequestScheduled()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 133
    iput-boolean v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestComplete:Z

    .line 134
    iput-boolean v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestFailed:Z

    .line 136
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestScheduled()V

    .line 139
    :cond_0
    return-void
.end method

.method public declared-synchronized sendNaverRequest(Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "requestListener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/naver/NaverAdaptor;->cancelCurrentRequest()V

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0xc8

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 75
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    const-string/jumbo v3, "<NaverPassThroughRequest Query=\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string/jumbo v3, "\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    const-string/jumbo v3, "/>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getServerInfo()Lcom/vlingo/core/internal/util/CoreServerInfo;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getVcsUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v2

    .line 81
    .local v2, "url":Ljava/lang/String;
    new-instance v1, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "/localsearch/localsearch"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    .line 85
    .local v1, "sm_vcsUrl":Lcom/vlingo/sdk/internal/http/URL;
    sget-object v3, Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Request = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 86
    const-string/jumbo v3, "NaverRequest-Request"

    new-instance v4, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;

    invoke-direct {v4, p0, p2}, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;-><init>(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v1, v5}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 87
    iget-object v3, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/http/HttpRequest;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    monitor-exit p0

    return-void

    .line 73
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    .end local v1    # "sm_vcsUrl":Lcom/vlingo/sdk/internal/http/URL;
    .end local v2    # "url":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public sendNaverRequest(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "req"    # Ljava/lang/String;
    .param p2, "force"    # Z

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->isRequestComplete:Z

    .line 55
    if-nez p1, :cond_0

    .line 56
    const-string/jumbo p1, ""

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->currentRequest:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 58
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->currentRequest:Ljava/lang/String;

    .line 61
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->currentRequest:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_3

    .line 62
    :cond_2
    iput-object p1, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->currentRequest:Ljava/lang/String;

    .line 63
    invoke-virtual {p0, p1, p0}, Lcom/vlingo/midas/naver/NaverAdaptor;->sendNaverRequest(Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    .line 65
    :cond_3
    return-void
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->content:Ljava/lang/String;

    .line 188
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->type:Ljava/lang/String;

    .line 204
    return-void
.end method

.method public setVVSActionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->vvsActionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 196
    return-void
.end method

.method public setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 0
    .param p1, "widgetListener"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/vlingo/midas/naver/NaverAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 155
    return-void
.end method
