.class Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;
.super Ljava/lang/Object;
.source "NaverAdaptor.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/naver/NaverAdaptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NaverHTTPHandler"
.end annotation


# instance fields
.field m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

.field final synthetic this$0:Lcom/vlingo/midas/naver/NaverAdaptor;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 1
    .param p2, "listener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;->this$0:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 210
    iput-object p2, p0, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 211
    return-void
.end method


# virtual methods
.method public onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Cancelled"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 257
    # getter for: Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/naver/NaverAdaptor;->access$0()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    const-string/jumbo v1, "Request Cancelled "

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 258
    return-void
.end method

.method public onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 248
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Failure"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 250
    # getter for: Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/naver/NaverAdaptor;->access$0()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    const-string/jumbo v1, "request failed"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 251
    return-void
.end method

.method public onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 215
    const/4 v0, 0x0

    .line 216
    .local v0, "result":Lcom/vlingo/core/internal/naver/NaverResponseParser;
    iget v2, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    .line 218
    # getter for: Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/naver/NaverAdaptor;->access$0()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Got valid response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 219
    new-instance v0, Lcom/vlingo/core/internal/naver/NaverResponseParser;

    .end local v0    # "result":Lcom/vlingo/core/internal/naver/NaverResponseParser;
    invoke-direct {v0}, Lcom/vlingo/core/internal/naver/NaverResponseParser;-><init>()V

    .line 220
    .restart local v0    # "result":Lcom/vlingo/core/internal/naver/NaverResponseParser;
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->parse(Ljava/lang/String;)V

    .line 221
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->getError()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 223
    .local v1, "success":Z
    :goto_0
    # getter for: Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/naver/NaverAdaptor;->access$0()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "success: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 224
    iget-object v2, p0, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    invoke-interface {v2, v1, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestComplete(ZLjava/lang/Object;)V

    .line 229
    .end local v1    # "success":Z
    :goto_1
    return-void

    .line 221
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 226
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "responseCode="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Timeout"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 242
    # getter for: Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/naver/NaverAdaptor;->access$0()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    const-string/jumbo v1, " Time out! "

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 243
    const/4 v0, 0x0

    return v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 234
    # getter for: Lcom/vlingo/midas/naver/NaverAdaptor;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/naver/NaverAdaptor;->access$0()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    const-string/jumbo v1, " will execute! "

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverAdaptor$NaverHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestScheduled()V

    .line 236
    return-void
.end method
