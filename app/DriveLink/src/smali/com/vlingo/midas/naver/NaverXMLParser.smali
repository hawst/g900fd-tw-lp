.class public Lcom/vlingo/midas/naver/NaverXMLParser;
.super Ljava/lang/Object;
.source "NaverXMLParser.java"


# instance fields
.field public ErrorCode:I

.field public answer:Ljava/lang/String;

.field public domain:Ljava/lang/String;

.field public itemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public message:Ljava/lang/String;

.field private final ns:Ljava/lang/String;

.field public propertyList:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public query:Ljava/lang/String;

.field public svoiceQuery:Ljava/lang/String;

.field public ttsText:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string/jumbo v0, "nhncorp.naverkoreaservice.search.searchcommon.searchsystem.svoice"

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->ns:Ljava/lang/String;

    .line 35
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->propertyList:Ljava/util/Hashtable;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->itemList:Ljava/util/List;

    .line 37
    return-void
.end method

.method private getResult(Lorg/xmlpull/v1/XmlPullParser;)Lcom/vlingo/midas/naver/NaverXMLParser;
    .locals 5
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 92
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    if-eq v2, v4, :cond_0

    .line 93
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    .line 95
    :cond_0
    const/4 v1, 0x0

    .line 97
    .local v1, "resStatus":Lcom/vlingo/midas/naver/NaverXMLParser;
    const-string/jumbo v2, "nhncorp.naverkoreaservice.search.searchcommon.searchsystem.svoice"

    const-string/jumbo v3, "result"

    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_1
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 117
    return-object v1

    .line 99
    :cond_2
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    if-ne v2, v4, :cond_1

    .line 102
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "name":Ljava/lang/String;
    const-string/jumbo v2, "query"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 105
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readCDATA(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->query:Ljava/lang/String;

    goto :goto_0

    .line 106
    :cond_3
    const-string/jumbo v2, "domain"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 107
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readCDATA(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->domain:Ljava/lang/String;

    goto :goto_0

    .line 108
    :cond_4
    const-string/jumbo v2, "data"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 109
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readData(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/List;

    goto :goto_0

    .line 110
    :cond_5
    const-string/jumbo v2, "status"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 111
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->getStatus(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 113
    :cond_6
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0
.end method

.method private getStatus(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 121
    const-string/jumbo v0, "status"

    invoke-interface {p1, v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 123
    const-string/jumbo v0, "code"

    invoke-interface {p1, v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->ErrorCode:I

    .line 125
    const-string/jumbo v0, "code"

    invoke-interface {p1, v3, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 127
    const-string/jumbo v0, "message"

    invoke-interface {p1, v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->message:Ljava/lang/String;

    .line 129
    const-string/jumbo v0, "message"

    invoke-interface {p1, v3, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 131
    const-string/jumbo v0, "status"

    invoke-interface {p1, v3, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method private readCDATA(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 1
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 170
    const-string/jumbo v0, ""

    .line 172
    .local v0, "result":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 173
    return-object v0
.end method

.method private readData(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/List;
    .locals 7
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    .line 177
    const/4 v2, 0x0

    .line 178
    .local v2, "result":Ljava/util/List;
    const/4 v3, 0x0

    const-string/jumbo v4, "data"

    invoke-interface {p1, v5, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    if-ne v3, v6, :cond_1

    .line 209
    iput-object v2, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->itemList:Ljava/util/List;

    .line 210
    return-object v2

    .line 180
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 183
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "name":Ljava/lang/String;
    const-string/jumbo v3, "ttsText"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 188
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->ttsText:Ljava/lang/String;

    goto :goto_0

    .line 189
    :cond_2
    const-string/jumbo v3, "svoiceQuery"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 190
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->svoiceQuery:Ljava/lang/String;

    goto :goto_0

    .line 191
    :cond_3
    const-string/jumbo v3, "url"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 192
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->url:Ljava/lang/String;

    goto :goto_0

    .line 193
    :cond_4
    const-string/jumbo v3, "answer"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 194
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->answer:Ljava/lang/String;

    goto :goto_0

    .line 195
    :cond_5
    const-string/jumbo v3, "itemList"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 196
    if-nez v2, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "result":Ljava/util/List;
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 197
    .restart local v2    # "result":Ljava/util/List;
    :cond_6
    :goto_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    if-eq v3, v6, :cond_0

    .line 198
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "item"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 199
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readEntry(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/Hashtable;

    move-result-object v1

    .line 200
    .local v1, "node":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 203
    .end local v1    # "node":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_7
    iget-object v3, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->propertyList:Ljava/util/Hashtable;

    if-nez v3, :cond_8

    new-instance v3, Ljava/util/Hashtable;

    invoke-direct {v3}, Ljava/util/Hashtable;-><init>()V

    iput-object v3, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->propertyList:Ljava/util/Hashtable;

    .line 204
    :cond_8
    iget-object v3, p0, Lcom/vlingo/midas/naver/NaverXMLParser;->propertyList:Ljava/util/Hashtable;

    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readCDATA(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private readEntry(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/Hashtable;
    .locals 6
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 137
    const/4 v3, 0x0

    const-string/jumbo v4, "item"

    invoke-interface {p1, v5, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 151
    const/4 v0, 0x0

    .line 153
    .local v0, "items":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    .line 166
    return-object v0

    .line 154
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 157
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "name":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->readCDATA(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    .line 159
    .local v2, "value":Ljava/lang/String;
    if-nez v0, :cond_2

    new-instance v0, Ljava/util/Hashtable;

    .end local v0    # "items":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 161
    .restart local v0    # "items":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 164
    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private readNaverXML(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 5
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 59
    const/4 v1, 0x0

    .line 60
    .local v1, "ret":Lcom/vlingo/midas/naver/NaverXMLParser;
    const/4 v2, 0x0

    const-string/jumbo v3, "message"

    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 61
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 73
    return-void

    .line 62
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 65
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "name":Ljava/lang/String;
    const-string/jumbo v2, "result"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 68
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->getResult(Lorg/xmlpull/v1/XmlPullParser;)Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v1

    .line 69
    goto :goto_0

    .line 70
    :cond_2
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverXMLParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0
.end method

.method private readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 3
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 215
    const-string/jumbo v0, ""

    .line 216
    .local v0, "result":Ljava/lang/String;
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 217
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v0

    .line 218
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 220
    :cond_0
    return-object v0
.end method

.method private skip(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 77
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 79
    :cond_0
    const/4 v0, 0x1

    .line 80
    .local v0, "depth":I
    :goto_0
    if-nez v0, :cond_1

    .line 90
    return-void

    .line 81
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 86
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    .line 84
    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public parse(Ljava/lang/String;)V
    .locals 5
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 42
    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 43
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v1, 0x0

    .line 44
    .local v1, "is":Ljava/io/InputStream;
    new-instance v1, Ljava/io/ByteArrayInputStream;

    .end local v1    # "is":Ljava/io/InputStream;
    const-string/jumbo v3, "UTF-8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 45
    .restart local v1    # "is":Ljava/io/InputStream;
    const-string/jumbo v3, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V

    .line 46
    const-string/jumbo v3, "UTF-8"

    invoke-interface {v2, v1, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 47
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 48
    invoke-direct {p0, v2}, Lcom/vlingo/midas/naver/NaverXMLParser;->readNaverXML(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 50
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    .end local v1    # "is":Ljava/io/InputStream;
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 53
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 55
    throw v3
.end method
