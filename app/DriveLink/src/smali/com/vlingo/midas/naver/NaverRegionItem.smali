.class public Lcom/vlingo/midas/naver/NaverRegionItem;
.super Lcom/vlingo/midas/naver/NaverLocalItem;
.source "NaverRegionItem.java"


# direct methods
.method public constructor <init>(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 8
    .local p1, "input":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverLocalItem;-><init>(Ljava/util/Hashtable;)V

    .line 10
    return-void
.end method


# virtual methods
.method public getMatchAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    const-string/jumbo v0, "matchAddress"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverRegionItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getZipCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    const-string/jumbo v0, "zipCode"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverRegionItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
