.class Lcom/vlingo/midas/uifocus/UiFocusUtil$OnUiFocusChangeListenerImpl;
.super Ljava/lang/Object;
.source "UiFocusUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/uifocus/UiFocusUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OnUiFocusChangeListenerImpl"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/vlingo/midas/uifocus/UiFocusUtil$OnUiFocusChangeListenerImpl;->mContext:Landroid/content/Context;

    .line 87
    return-void
.end method


# virtual methods
.method public onUiFocusChange(I)V
    .locals 1
    .param p1, "focusChange"    # I

    .prologue
    .line 91
    sget-object v0, Lcom/vlingo/midas/uifocus/UiFocusUtil;->smListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    if-eq v0, p0, :cond_1

    .line 109
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 95
    :cond_1
    sget-boolean v0, Lcom/vlingo/midas/uifocus/UiFocusUtil;->associatedService:Z

    if-nez v0, :cond_0

    .line 99
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 106
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusUtil$OnUiFocusChangeListenerImpl;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finishAffinity()V

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
