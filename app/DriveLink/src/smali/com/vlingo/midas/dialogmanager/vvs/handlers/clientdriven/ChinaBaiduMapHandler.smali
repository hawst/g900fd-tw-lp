.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ChinaBaiduMapHandler.java"


# static fields
.field private static final NAVIGATE_URI:Ljava/lang/String; = "bdapp://map/direction?origin=%s,%s&destination=%s&mode=driving"

.field private static final NAVIGATION_TYPE:Ljava/lang/String; = "NAVIGATE"

.field public static final PACKAGE:Ljava/lang/String; = "com.baidu.BaiduMap"

.field public static final PACKAGE_H_DEVICES:Ljava/lang/String; = "com.baidu.BaiduMap.samsung"

.field public static final PACKAGE_TABLETS:Ljava/lang/String; = "com.baidu.BaiduMap.pad"

.field private static final PARAM_NAVTYPE:Ljava/lang/String; = "Navtype"

.field private static final PARAM_QUERY:Ljava/lang/String; = "Query"

.field private static final SHOWMAP_URI:Ljava/lang/String; = "bdapp://map/place/search?query=%s"

.field private static final SHOW_MARKER_URI:Ljava/lang/String; = "bdapp://map/marker?location=%s,%s&title=%s&content=%s"

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method public static getCurrentMapIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "desc"    # Ljava/lang/String;

    .prologue
    .line 85
    const-string/jumbo v1, "bdapp://map/marker?location=%s,%s&title=%s&content=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getUserLat()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 86
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getUserLong()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p0, v2, v3

    const/4 v3, 0x3

    aput-object p1, v2, v3

    .line 85
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "uri":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private static getIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 96
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 97
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.baidu.BaiduMap.samsung"

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    const-string/jumbo v1, "com.baidu.BaiduMap.samsung"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    :goto_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 106
    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "BaiduMaps Intent : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 107
    return-object v0

    .line 99
    :cond_0
    const-string/jumbo v1, "com.baidu.BaiduMap.pad"

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    const-string/jumbo v1, "com.baidu.BaiduMap.pad"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 102
    :cond_1
    const-string/jumbo v1, "com.baidu.BaiduMap"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static getMapIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 80
    const-string/jumbo v1, "bdapp://map/place/search?query=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "uri":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static getMarkerMapIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "latitude"    # Ljava/lang/String;
    .param p1, "longitude"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "desc"    # Ljava/lang/String;

    .prologue
    .line 91
    const-string/jumbo v1, "bdapp://map/marker?location=%s,%s&title=%s&content=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    const/4 v3, 0x3

    aput-object p3, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "uri":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static getNaviagateIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 74
    const-string/jumbo v1, "bdapp://map/direction?origin=%s,%s&destination=%s&mode=driving"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getUserLat()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 75
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getUserLong()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p0, v2, v3

    .line 74
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "uri":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static getNaviagateIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "latitude"    # Ljava/lang/String;
    .param p1, "longitude"    # Ljava/lang/String;

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getNaviagateIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 40
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 42
    const-string/jumbo v4, "Navtype"

    invoke-static {p1, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "navType":Ljava/lang/String;
    const-string/jumbo v4, "Query"

    invoke-static {p1, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 44
    .local v3, "query":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 45
    .local v2, "prompt":Ljava/lang/String;
    const/4 v0, 0x0

    .line 46
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "NAVIGATE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 47
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NAVIGATE_TO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 48
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getNaviagateIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 59
    :cond_0
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v4

    const-string/jumbo v5, "navigate"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 60
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 61
    invoke-interface {p2, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_1
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v5, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    .line 64
    invoke-virtual {v4, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v4

    .line 65
    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 66
    return v6

    .line 50
    :cond_2
    const-string/jumbo v4, "CurrentLocation"

    invoke-static {p1, v4, v6, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 51
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 52
    const-string/jumbo v4, ""

    invoke-static {v2, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getCurrentMapIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 53
    goto :goto_0

    :cond_3
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 54
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MAP_OF:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 55
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getMapIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
