.class public Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;
.source "SamsungVVSActionKey.java"


# static fields
.field public static CAR_LOCATE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static CAR_MODE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static CHATBOT:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static CHATBOT_SING:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static CHINA_BAIDU_MAP:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static CHINA_NAV:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static CHINA_NAV_ENAVI:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static CONFIRM:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static EMERGENCY_CALL:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static LOCATION_SHARE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static NAVER_CONTENT:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static NAVIGATE_LOCAL:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static PLAY_MUSIC_BY_CHARACTERISTIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static PROCESS_COMMAND:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static RECORD_VOICE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static RESOLVE_APPOINTMENT:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static RESOLVE_MESSAGE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static RESOLVE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_APPOINTMENTS_CHOICES_WIDGET:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_APPOINTMENTS_WIDGET:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_BATTERY:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_CREATE_APPOINTMENT_WIDGET:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_CREATE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_DELETE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_EDIT_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_NAVIGATION:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_PLAY_ALBUM:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_PLAY_ARTIST:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_PLAY_GENERIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_PLAY_MUSIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_PLAY_TITLE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_TASKS:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_TASK_CHOICES:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_UNREAD_MESSAGES:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static SHOW_WCIS:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

.field public static UNSUPPORTED_DOMAIN:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ChinaNav"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHINA_NAV:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 11
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ChinaBaiduMap"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHINA_BAIDU_MAP:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 12
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ChatbotSing"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHATBOT_SING:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 13
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ChatbotWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHATBOT:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 14
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "NaverContent"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->NAVER_CONTENT:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 15
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "NavLocal"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->NAVIGATE_LOCAL:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 16
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "RecordVoice"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RECORD_VOICE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 17
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowWCISWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_WCIS:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 18
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowPlayTitleWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_TITLE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 19
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowPlayArtistWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_ARTIST:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 20
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowPlayAlbumWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_ALBUM:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 21
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowPlayMusicWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_MUSIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 22
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowPlayGenericWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_GENERIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 23
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowUnreadMessagesWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_UNREAD_MESSAGES:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 24
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ResolveTask"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RESOLVE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 25
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowCreateTaskWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_CREATE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 26
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowDeleteTaskWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_DELETE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 27
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowEditTaskWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_EDIT_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 28
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowTasksWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_TASKS:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 29
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowTaskChoicesWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_TASK_CHOICES:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 30
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "Task"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 31
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "PlayMusicByCharacteristic"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->PLAY_MUSIC_BY_CHARACTERISTIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 32
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ConfirmHandler"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CONFIRM:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 33
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ResolveMessage"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RESOLVE_MESSAGE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 34
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowNavWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_NAVIGATION:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 35
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ResolveAppointment"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RESOLVE_APPOINTMENT:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 36
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowAppointmentsWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_APPOINTMENTS_WIDGET:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 37
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowAppointmentChoicesWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_APPOINTMENTS_CHOICES_WIDGET:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 38
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowCreateAppointmentWidget"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_CREATE_APPOINTMENT_WIDGET:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 39
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "EmergencyCall"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->EMERGENCY_CALL:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 40
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ProcessCommand"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->PROCESS_COMMAND:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 41
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "CarLocate"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CAR_LOCATE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 42
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ENaviNavigate"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHINA_NAV_ENAVI:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 43
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "ShowBattery"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_BATTERY:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 44
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "EmergencyLocationSharing"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->LOCATION_SHARE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 45
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "CarMode"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CAR_MODE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 48
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-string/jumbo v1, "HandleUnsupportedRequest"

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->UNSUPPORTED_DOMAIN:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    .line 52
    return-void
.end method
