.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;
.super Ljava/lang/Object;
.source "ResolveAlarmHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AlarmQueryRunnable"
.end annotation


# instance fields
.field action:Lcom/vlingo/sdk/recognition/VLAction;

.field resolveAlarmHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 0
    .param p1, "resolveAlarmHandler"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;
    .param p2, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;->resolveAlarmHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;

    .line 80
    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;->action:Lcom/vlingo/sdk/recognition/VLAction;

    .line 81
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 85
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;->resolveAlarmHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;->action:Lcom/vlingo/sdk/recognition/VLAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getAlarmsByQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->access$0(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v0

    .line 87
    .local v0, "queriedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    if-eqz v0, :cond_0

    .line 88
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;->resolveAlarmHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getStoredAlarms()Ljava/util/List;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->access$1(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;)Ljava/util/List;

    move-result-object v1

    .line 90
    .local v1, "storedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;->resolveAlarmHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->sendAlarmResolvedEvent(Ljava/util/List;I)V
    invoke-static {v2, v0, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->access$2(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;Ljava/util/List;I)V

    .line 91
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;->resolveAlarmHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->addQueriedAlarmsToStore(Ljava/util/List;Ljava/util/List;)V
    invoke-static {v2, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->access$3(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;Ljava/util/List;Ljava/util/List;)V

    .line 94
    .end local v1    # "storedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;->resolveAlarmHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->access$4(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 96
    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;->resolveAlarmHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;

    .line 97
    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;->action:Lcom/vlingo/sdk/recognition/VLAction;

    .line 98
    return-void
.end method
