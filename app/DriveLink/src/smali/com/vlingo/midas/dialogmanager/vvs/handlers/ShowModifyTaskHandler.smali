.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowModifyTaskHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field private modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

.field private originalTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private getTask(I)Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .locals 5
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TASK_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 62
    .local v1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    :try_start_0
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 63
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->log:Lcom/vlingo/core/internal/logging/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Task Index passed in is not in the cache"

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    new-instance v2, Ljava/security/InvalidParameterException;

    const-string/jumbo v3, "Task Index passed in is not in the cache"

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private storeTask()V
    .locals 3

    .prologue
    .line 72
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 73
    .local v0, "storedEvents":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TASK_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 75
    return-void
.end method

.method private updateTask()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 78
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyTaskInterface;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyTaskInterface;

    .line 79
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->originalTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyTaskInterface;->original(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyTaskInterface;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyTaskInterface;->modified(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyTaskInterface;

    move-result-object v0

    .line 81
    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyTaskInterface;->queue()V

    .line 82
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    if-eqz v0, :cond_0

    .line 83
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/EditTaskAcceptedText;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBeginDate()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/vlingo/core/internal/recognition/acceptedtext/EditTaskAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 85
    :cond_0
    return-void
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 100
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 101
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 102
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 93
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 94
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 95
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->storeTask()V

    .line 96
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 46
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 47
    const-string/jumbo v1, "id"

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-static {p1, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v0

    .line 48
    .local v0, "index":I
    invoke-direct {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->getTask(I)Lcom/vlingo/core/internal/schedule/ScheduleTask;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->originalTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 49
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->originalTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-static {p1, v1}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->mergeChanges(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/core/internal/schedule/ScheduleTask;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 50
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-interface {p2, v1, v2, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 52
    const-string/jumbo v1, "execute"

    invoke-static {p1, v1, v4, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->updateTask()V

    .line 55
    :cond_0
    return v4
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;-><init>()V

    .line 113
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 120
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;-><init>()V

    .line 116
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 118
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method
