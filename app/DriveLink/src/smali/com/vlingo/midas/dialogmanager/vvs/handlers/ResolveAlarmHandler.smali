.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;
.source "ResolveAlarmHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getAlarmsByQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;)Ljava/util/List;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getStoredAlarms()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->sendAlarmResolvedEvent(Ljava/util/List;I)V

    return-void
.end method

.method static synthetic access$3(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->addQueriedAlarmsToStore(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$4(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method private addQueriedAlarmsToStore(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "queriedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    .local p2, "storedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    invoke-interface {p2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 65
    return-void
.end method

.method private clearCacheOnRequest(Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v1, 0x0

    .line 35
    const-string/jumbo v0, "clear.cache"

    invoke-static {p1, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 38
    :cond_0
    return-void
.end method

.method private getAlarmsByQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {p1}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->extractAlarmQuery(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/alarms/AlarmQueryObject;

    move-result-object v0

    .line 42
    .local v0, "criteria":Lcom/vlingo/core/internal/alarms/AlarmQueryObject;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->getAlarms(Landroid/content/Context;Lcom/vlingo/core/internal/alarms/AlarmQueryObject;)Ljava/util/List;

    move-result-object v1

    .line 43
    .local v1, "newAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    return-object v1
.end method

.method private getStoredAlarms()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 51
    .local v0, "storedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    if-nez v0, :cond_0

    .line 52
    new-instance v0, Ljava/util/LinkedList;

    .end local v0    # "storedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 54
    .restart local v0    # "storedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    :cond_0
    return-object v0
.end method

.method private sendAlarmResolvedEvent(Ljava/util/List;I)V
    .locals 3
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "queriedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, p1, p2, v1}, Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;-><init>(Ljava/util/List;II)V

    .line 59
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 60
    return-void
.end method


# virtual methods
.method public executeQuery(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->clearCacheOnRequest(Lcom/vlingo/sdk/recognition/VLAction;)V

    .line 29
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler$AlarmQueryRunnable;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;Lcom/vlingo/sdk/recognition/VLAction;)V

    const-string/jumbo v2, "AlarmQueryRunnable"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 31
    const/4 v0, 0x1

    return v0
.end method

.method protected sendEmptyEvent()V
    .locals 3

    .prologue
    .line 69
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;-><init>()V

    .line 70
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/AlarmResolvedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 71
    return-void
.end method
