.class public Lcom/vlingo/midas/CommonConstants;
.super Ljava/lang/Object;
.source "CommonConstants.java"


# static fields
.field public static final ACTION_NAME_TOS_ACCEPTED:Ljava/lang/String; = "com.vlingo.midas.action.TOS_ACCEPTED"

.field public static final EXTRA_EXIT_AFTER_TERMS_OF_SERVICE_ACCEPTANCE:Ljava/lang/String; = "drivelink"

.field public static final EXTRA_LAUNCH_AFTER_TERMS_OF_SERVICE_ACCEPTANCE_ACTIVITY:Ljava/lang/String; = "launch_aafter_terms_of_service_acceptance_activity"

.field public static final EXTRA_LAUNCH_AFTER_TERMS_OF_SERVICE_ACCEPTANCE_PACKAGE:Ljava/lang/String; = "launch_after_terms_of_service_acceptance_package"

.field public static final SVOICE_ACTIVITY:Ljava/lang/String; = "com.vlingo.midas.gui.ConversationActivity"

.field public static final SVOICE_PACKAGE:Ljava/lang/String; = "com.vlingo.midas"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
