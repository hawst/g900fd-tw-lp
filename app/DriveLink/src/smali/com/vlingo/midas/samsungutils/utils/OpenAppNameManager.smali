.class public final Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;
.super Ljava/lang/Object;
.source "OpenAppNameManager.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/OpenAppUtil$IOpenAppNameManager;


# static fields
.field private static instance:Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;

.field private static mHandler:Landroid/os/Handler;


# instance fields
.field BriefingSamsungList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field MessengerList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field NavigatorList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field PlayNewsStandList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field SmartRemoteList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field TMobileMyAccountList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field UberList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field VisualVoicemailList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field astroFileList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field bandList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field betweenList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field cgvList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field cjOneCardList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field facebookList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field flashLightList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field grouponList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field hangOutList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field instagramList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final log:Lcom/vlingo/core/internal/logging/Logger;

.field mocaList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field nDriveList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field ollehNaviList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field oneKmList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field pocketList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field seoulBusList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field smartTouchList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field smartWalletList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field tvingList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field uwaList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field v3MobileList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field zooMoneyList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-class v0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 123
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v8, [Ljava/lang/String;

    .line 125
    const-string/jumbo v2, "ollehnavi"

    aput-object v2, v1, v4

    .line 126
    const-string/jumbo v2, "\uc62c\ub808navi"

    aput-object v2, v1, v5

    .line 127
    const-string/jumbo v2, "\uc62c\ub808\ub0b4\ube44"

    aput-object v2, v1, v6

    .line 128
    const-string/jumbo v2, "\uc62c\ub808\ub124\ube44"

    aput-object v2, v1, v7

    .line 124
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->ollehNaviList:Ljava/util/HashSet;

    .line 133
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    .line 135
    const-string/jumbo v2, "facebook"

    aput-object v2, v1, v4

    .line 136
    const-string/jumbo v2, "\ud398\uc774\uc2a4\ubd81"

    aput-object v2, v1, v5

    .line 137
    const-string/jumbo v2, "\ud328\uc774\uc2a4\ubd81"

    aput-object v2, v1, v6

    .line 138
    const-string/jumbo v2, "\ud328\ubd81"

    aput-object v2, v1, v7

    .line 139
    const-string/jumbo v2, "\ud398\ubd81"

    aput-object v2, v1, v8

    .line 134
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->facebookList:Ljava/util/HashSet;

    .line 143
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    .line 145
    const-string/jumbo v2, "v3 mobile"

    aput-object v2, v1, v4

    .line 146
    const-string/jumbo v2, "v3 \ubaa8\ubc14\uc77c"

    aput-object v2, v1, v5

    .line 147
    const-string/jumbo v2, "v3 \ubaa8\ubc14\uc77c"

    aput-object v2, v1, v6

    .line 148
    const-string/jumbo v2, "v3\ubaa8\ubc14\uc77c"

    aput-object v2, v1, v7

    .line 149
    const-string/jumbo v2, "v3mobile"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    .line 150
    const-string/jumbo v3, "v3 mobile"

    aput-object v3, v1, v2

    .line 144
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->v3MobileList:Ljava/util/HashSet;

    .line 154
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    .line 156
    const-string/jumbo v2, "\ud589\uc544\uc6c3"

    aput-object v2, v1, v4

    .line 157
    const-string/jumbo v2, "hang out"

    aput-object v2, v1, v5

    .line 158
    const-string/jumbo v2, "hang outs"

    aput-object v2, v1, v6

    .line 159
    const-string/jumbo v2, "hangout"

    aput-object v2, v1, v7

    .line 160
    const-string/jumbo v2, "hangouts"

    aput-object v2, v1, v8

    .line 155
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->hangOutList:Ljava/util/HashSet;

    .line 164
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/String;

    .line 166
    const-string/jumbo v2, "smarttouch"

    aput-object v2, v1, v4

    .line 167
    const-string/jumbo v2, "smart touch"

    aput-object v2, v1, v5

    .line 168
    const-string/jumbo v2, "\uc2a4\ub9c8\ud2b8\ud130\uce58"

    aput-object v2, v1, v6

    .line 165
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->smartTouchList:Ljava/util/HashSet;

    .line 172
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/String;

    .line 174
    const-string/jumbo v2, "\uc2a4\ub9c8\ud2b8\uc6d4\ub81b"

    aput-object v2, v1, v4

    .line 175
    const-string/jumbo v2, "smart wallet"

    aput-object v2, v1, v5

    .line 176
    const-string/jumbo v2, "smartwallet"

    aput-object v2, v1, v6

    .line 173
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->smartWalletList:Ljava/util/HashSet;

    .line 180
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 182
    const-string/jumbo v2, "moca"

    aput-object v2, v1, v4

    .line 183
    const-string/jumbo v2, "\ubaa8\uce74"

    aput-object v2, v1, v5

    .line 181
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->mocaList:Ljava/util/HashSet;

    .line 187
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 189
    const-string/jumbo v2, "zoomoney"

    aput-object v2, v1, v4

    .line 190
    const-string/jumbo v2, "\uc8fc\uba38\ub2c8"

    aput-object v2, v1, v5

    .line 188
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->zooMoneyList:Ljava/util/HashSet;

    .line 194
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/String;

    .line 196
    const-string/jumbo v2, "uwa"

    aput-object v2, v1, v4

    .line 197
    const-string/jumbo v2, "\uc6b0\uc640"

    aput-object v2, v1, v5

    .line 198
    const-string/jumbo v2, "\uc720\ud654"

    aput-object v2, v1, v6

    .line 195
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->uwaList:Ljava/util/HashSet;

    .line 202
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    .line 204
    const-string/jumbo v2, "n\ub4dc\ub77c\uc774\ube0c"

    aput-object v2, v1, v4

    .line 205
    const-string/jumbo v2, "\uc564\ub4dc\ub77c\uc774\ube0c"

    aput-object v2, v1, v5

    .line 206
    const-string/jumbo v2, "\uc5d4\ub4dc\ub77c\uc774\ube0c"

    aput-object v2, v1, v6

    .line 207
    const-string/jumbo v2, "n \ub4dc\ub77c\uc774\ube0c"

    aput-object v2, v1, v7

    .line 208
    const-string/jumbo v2, "\uc564 \ub4dc\ub77c\uc774\ube0c"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    .line 209
    const-string/jumbo v3, "\uc5d4 \ub4dc\ub77c\uc774\ube0c"

    aput-object v3, v1, v2

    .line 203
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->nDriveList:Ljava/util/HashSet;

    .line 213
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/String;

    .line 215
    const-string/jumbo v2, "seoulbus"

    aput-object v2, v1, v4

    .line 216
    const-string/jumbo v2, "\uc11c\uc6b8 \ubc84\uc2a4"

    aput-object v2, v1, v5

    .line 217
    const-string/jumbo v2, "\uc11c\uc6b8\ubc84\uc2a4"

    aput-object v2, v1, v6

    .line 214
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->seoulBusList:Ljava/util/HashSet;

    .line 221
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/String;

    .line 223
    const-string/jumbo v2, "\ud50c\ub798\uc26c\ub77c\uc774\ud2b8"

    aput-object v2, v1, v4

    .line 224
    const-string/jumbo v2, "Flash light"

    aput-object v2, v1, v5

    .line 225
    const-string/jumbo v2, "\ud50c\ub798\uc2dc\ub77c\uc774\ud2b8"

    aput-object v2, v1, v6

    .line 222
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->flashLightList:Ljava/util/HashSet;

    .line 229
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/String;

    .line 231
    const-string/jumbo v2, "ASTRO \ud30c\uc77c\uad00\ub9ac\uc790"

    aput-object v2, v1, v4

    .line 232
    const-string/jumbo v2, "\uc544\uc2a4\ud2b8\ub85c \ud30c\uc77c \uad00\ub9ac\uc790"

    aput-object v2, v1, v5

    .line 233
    const-string/jumbo v2, "\uc544\uc2a4\ud2b8\ub85c\ud30c\uc77c\uad00\ub9ac\uc790"

    aput-object v2, v1, v6

    .line 230
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->astroFileList:Ljava/util/HashSet;

    .line 237
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 239
    const-string/jumbo v2, "between"

    aput-object v2, v1, v4

    .line 240
    const-string/jumbo v2, "\ube44\ud2b8\uc708"

    aput-object v2, v1, v5

    .line 238
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->betweenList:Ljava/util/HashSet;

    .line 244
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 246
    const-string/jumbo v2, "BAND"

    aput-object v2, v1, v4

    .line 247
    const-string/jumbo v2, "\ubc34\ub4dc"

    aput-object v2, v1, v5

    .line 245
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->bandList:Ljava/util/HashSet;

    .line 251
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 253
    const-string/jumbo v2, "instagram"

    aput-object v2, v1, v4

    .line 254
    const-string/jumbo v2, "\uc778\uc2a4\ud0c0\uadf8\ub7a8"

    aput-object v2, v1, v5

    .line 252
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->instagramList:Ljava/util/HashSet;

    .line 258
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/String;

    .line 260
    const-string/jumbo v2, "1km"

    aput-object v2, v1, v4

    .line 261
    const-string/jumbo v2, "\uc77c \ud0a4\ub85c\ubbf8\ud130"

    aput-object v2, v1, v5

    .line 262
    const-string/jumbo v2, "\uc77c\ud0a4\ub85c\ubbf8\ud130"

    aput-object v2, v1, v6

    .line 259
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->oneKmList:Ljava/util/HashSet;

    .line 266
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 268
    const-string/jumbo v2, "pocket"

    aput-object v2, v1, v4

    .line 269
    const-string/jumbo v2, "\ud3ec\ucf13"

    aput-object v2, v1, v5

    .line 267
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->pocketList:Ljava/util/HashSet;

    .line 273
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 275
    const-string/jumbo v2, "tving"

    aput-object v2, v1, v4

    .line 276
    const-string/jumbo v2, "\ud2f0\ube59"

    aput-object v2, v1, v5

    .line 274
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->tvingList:Ljava/util/HashSet;

    .line 280
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 282
    const-string/jumbo v2, "cgv"

    aput-object v2, v1, v4

    .line 283
    const-string/jumbo v2, "\uc528\uc9c0\ube44"

    aput-object v2, v1, v5

    .line 281
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->cgvList:Ljava/util/HashSet;

    .line 287
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    .line 289
    const-string/jumbo v2, "cj one card"

    aput-object v2, v1, v4

    .line 290
    const-string/jumbo v2, "cj one \uce74\ub4dc"

    aput-object v2, v1, v5

    .line 291
    const-string/jumbo v2, "cj \uc6d0\uce74\ub4dc"

    aput-object v2, v1, v6

    .line 292
    const-string/jumbo v2, "cjone\uce74\ub4dc"

    aput-object v2, v1, v7

    .line 293
    const-string/jumbo v2, "cj\uc6d0\uce74\ub4dc"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    .line 294
    const-string/jumbo v3, "\uc528\uc81c\uc774\uc6d0\uce74\ub4dc"

    aput-object v3, v1, v2

    .line 288
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->cjOneCardList:Ljava/util/HashSet;

    .line 298
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 300
    const-string/jumbo v2, "\uadf8\ub8e8\ud3f0"

    aput-object v2, v1, v4

    .line 301
    const-string/jumbo v2, "groupon"

    aput-object v2, v1, v5

    .line 299
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->grouponList:Ljava/util/HashSet;

    .line 305
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 307
    const-string/jumbo v2, "\ufeffBriefing Samsung"

    aput-object v2, v1, v4

    .line 308
    const-string/jumbo v2, "\ube0c\ub9ac\ud551 \uc0bc\uc131"

    aput-object v2, v1, v5

    .line 306
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->BriefingSamsungList:Ljava/util/HashSet;

    .line 312
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 314
    const-string/jumbo v2, "\ufeffPlay \ub274\uc2a4\uc2a4\ud0e0\ub4dc"

    aput-object v2, v1, v4

    .line 315
    const-string/jumbo v2, "\ud50c\ub808\uc774 \ub274\uc2a4\uc2a4\ud0e0\ub4dc"

    aput-object v2, v1, v5

    .line 313
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->PlayNewsStandList:Ljava/util/HashSet;

    .line 319
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 321
    const-string/jumbo v2, "Smart Remote"

    aput-object v2, v1, v4

    .line 322
    const-string/jumbo v2, "\uc2a4\ub9c8\ud2b8 \ub9ac\ubaa8\ud2b8"

    aput-object v2, v1, v5

    .line 320
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->SmartRemoteList:Ljava/util/HashSet;

    .line 326
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 328
    const-string/jumbo v2, "Messenger"

    aput-object v2, v1, v4

    .line 329
    const-string/jumbo v2, "\uba54\uc2e0\uc800"

    aput-object v2, v1, v5

    .line 327
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->MessengerList:Ljava/util/HashSet;

    .line 333
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 335
    const-string/jumbo v2, "Visual Voicemail"

    aput-object v2, v1, v4

    .line 336
    const-string/jumbo v2, "\ube44\uc8fc\uc5bc voice mail"

    aput-object v2, v1, v5

    .line 334
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->VisualVoicemailList:Ljava/util/HashSet;

    .line 340
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 342
    const-string/jumbo v2, "Uber"

    aput-object v2, v1, v4

    .line 343
    const-string/jumbo v2, "over"

    aput-object v2, v1, v5

    .line 341
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->UberList:Ljava/util/HashSet;

    .line 347
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 349
    const-string/jumbo v2, "T-Mobile My Account"

    aput-object v2, v1, v4

    .line 350
    const-string/jumbo v2, "\ud2f0\ubaa8\ubc14\uc77c myaccount"

    aput-object v2, v1, v5

    .line 348
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->TMobileMyAccountList:Ljava/util/HashSet;

    .line 354
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    .line 356
    const-string/jumbo v2, "Navigator"

    aput-object v2, v1, v4

    .line 357
    const-string/jumbo v2, "\ub124\ube44\uac8c\uc774\ud130"

    aput-object v2, v1, v5

    .line 355
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->NavigatorList:Ljava/util/HashSet;

    .line 30
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->context:Landroid/content/Context;

    .line 31
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->instance:Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->instance:Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;

    .line 26
    :cond_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->instance:Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;

    return-object v0
.end method

.method private getResultAppName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "searchStr"    # Ljava/lang/String;

    .prologue
    .line 49
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->ollehNaviList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    const-string/jumbo v0, "ollehnavi"

    .line 119
    .local v0, "resultStr":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 51
    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->facebookList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    const-string/jumbo v0, "facebook"

    .line 53
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->v3MobileList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 54
    const-string/jumbo v0, "V3 Mobile"

    .line 55
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->hangOutList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 56
    const-string/jumbo v0, "\ud589\uc544\uc6c3"

    .line 57
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->smartTouchList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 58
    const-string/jumbo v0, "SmartTouch"

    .line 59
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->smartWalletList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 60
    const-string/jumbo v0, "\uc2a4\ub9c8\ud2b8\uc6d4\ub81b"

    .line 61
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->mocaList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 62
    const-string/jumbo v0, "Moca"

    .line 63
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->zooMoneyList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 64
    const-string/jumbo v0, "ZooMoney"

    .line 65
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_7
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->uwaList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 66
    const-string/jumbo v0, "Uwa"

    .line 67
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_8
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->nDriveList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 68
    const-string/jumbo v0, "n\ub4dc\ub77c\uc774\ube0c"

    .line 69
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_9
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->seoulBusList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 70
    const-string/jumbo v0, "seoulbus"

    .line 71
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_a
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->flashLightList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 72
    const-string/jumbo v0, "\ud50c\ub798\uc26c\ub77c\uc774\ud2b8"

    .line 73
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_b
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->astroFileList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 74
    const-string/jumbo v0, "astro\ud30c\uc77c\uad00\ub9ac\uc790"

    .line 75
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_c
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->betweenList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 76
    const-string/jumbo v0, "between"

    .line 77
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_d
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->bandList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 78
    const-string/jumbo v0, "band"

    .line 79
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_e
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->instagramList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 80
    const-string/jumbo v0, "instagram"

    .line 81
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_f
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->oneKmList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 82
    const-string/jumbo v0, "1km"

    .line 83
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_10
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->pocketList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 84
    const-string/jumbo v0, "pocket"

    .line 85
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_11
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->tvingList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 86
    const-string/jumbo v0, "tving"

    .line 87
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_12
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->cgvList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 88
    const-string/jumbo v0, "CGV"

    .line 89
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_13
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->cjOneCardList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 90
    const-string/jumbo v0, "cjonecard"

    .line 91
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_14
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->BriefingSamsungList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 92
    const-string/jumbo v0, "\ufeffBriefing Samsung"

    .line 93
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_15
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->PlayNewsStandList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 94
    const-string/jumbo v0, "Play \ub274\uc2a4\uc2a4\ud0e0\ub4dc"

    .line 95
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_16
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->SmartRemoteList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 96
    const-string/jumbo v0, "Smart Remote"

    .line 97
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_17
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->MessengerList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 98
    const-string/jumbo v0, "Messenger"

    .line 99
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_18
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->VisualVoicemailList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 100
    const-string/jumbo v0, "Visual Voicemail"

    .line 101
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_19
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->UberList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 102
    const-string/jumbo v0, "Uber"

    .line 103
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_1a
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->TMobileMyAccountList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 104
    const-string/jumbo v0, "T-Mobile My Account"

    .line 105
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_1b
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->NavigatorList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 106
    const-string/jumbo v0, "Navigator"

    .line 107
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .line 110
    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_1c
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->grouponList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 111
    const-string/jumbo v0, "\uadf8\ub8e8\ud3f0"

    .line 112
    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0

    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_1d
    invoke-static {}, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-static {}, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 113
    move-object v0, p1

    .line 114
    .restart local v0    # "resultStr":Ljava/lang/String;
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x10

    const-wide/16 v3, 0xbb8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 117
    .end local v0    # "resultStr":Ljava/lang/String;
    :cond_1e
    move-object v0, p1

    .restart local v0    # "resultStr":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public static registerHandler(Landroid/os/Handler;)V
    .locals 0
    .param p0, "handler"    # Landroid/os/Handler;

    .prologue
    .line 34
    sput-object p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->mHandler:Landroid/os/Handler;

    .line 35
    return-void
.end method


# virtual methods
.method public normalizeOpenAppName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "searchStr"    # Ljava/lang/String;

    .prologue
    .line 38
    const-string/jumbo v0, ""

    .line 40
    .local v0, "resultStr":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->getResultAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "resultStr:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 44
    return-object v0
.end method
