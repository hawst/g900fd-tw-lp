.class public Lcom/vlingo/midas/samsungutils/utils/ConfigurationUtility;
.super Ljava/lang/Object;
.source "ConfigurationUtility.java"


# static fields
.field public static final CMCC_SALES_CODE:Ljava/lang/String; = "CHM"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static supportsSVoiceAssociatedServiceOnly(Ljava/lang/String;)Z
    .locals 2
    .param p0, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 28
    const/4 v0, 0x0

    .line 29
    .local v0, "toReturn":Z
    if-eqz p0, :cond_0

    .line 34
    const-string/jumbo v1, "CHM"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    const/4 v0, 0x1

    .line 38
    :cond_0
    return v0
.end method

.method public static usingNoiseCancellation()Z
    .locals 2

    .prologue
    .line 13
    const/4 v0, 0x0

    .line 15
    .local v0, "voiceEngine":Lcom/samsung/voiceshell/VoiceEngine;
    :try_start_0
    invoke-static {}, Lcom/samsung/voiceshell/VoiceEngineWrapper;->getInstance()Lcom/samsung/voiceshell/VoiceEngine;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 19
    :goto_0
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 16
    :catch_0
    move-exception v1

    goto :goto_0
.end method
