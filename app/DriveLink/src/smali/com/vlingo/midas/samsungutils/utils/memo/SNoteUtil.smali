.class public final Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;
.super Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;
.source "SNoteUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/memo/IMemoUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;
    }
.end annotation


# static fields
.field private static final KEY_DATE:Ljava/lang/String; = "ModifiedTime"

.field private static final KEY_HAS_FAVORITES:Ljava/lang/String; = "HasFavorites"

.field private static final KEY_HAS_VOICERECORDER:Ljava/lang/String; = "HasVoiceRecord"

.field private static final KEY_ID:Ljava/lang/String; = "_id"

.field private static final KEY_NAME:Ljava/lang/String; = "name"

.field private static final KEY_PATH:Ljava/lang/String; = "path"

.field private static final KEY_TAG:Ljava/lang/String; = "HasTag"

.field private static final KEY_TEMPLATE_TIME:Ljava/lang/String; = "TemplateType"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SORT_ORDER:Ljava/lang/String; = "ModifiedTime DESC"

.field private static instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;

.field private static loggedMemoSchema:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 31
    const-class v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 50
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    .line 51
    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    .line 52
    const-string/jumbo v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 53
    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 54
    const-string/jumbo v2, "ModifiedTime"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 55
    const-string/jumbo v2, "HasFavorites"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 56
    const-string/jumbo v2, "HasVoiceRecord"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 57
    const-string/jumbo v2, "HasTag"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 58
    const-string/jumbo v2, "TemplateType"

    aput-object v2, v0, v1

    .line 50
    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->PROJECTION:[Ljava/lang/String;

    .line 61
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    .line 67
    sput-boolean v3, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->loggedMemoSchema:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;-><init>()V

    .line 64
    return-void
.end method

.method static synthetic access$0()Z
    .locals 1

    .prologue
    .line 67
    sget-boolean v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->loggedMemoSchema:Z

    return v0
.end method

.method static synthetic access$1()Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-object v0
.end method

.method static synthetic access$2(Z)V
    .locals 0

    .prologue
    .line 67
    sput-boolean p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->loggedMemoSchema:Z

    return-void
.end method

.method private getFilePathFromId(Landroid/content/Context;J)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J

    .prologue
    .line 131
    const/4 v7, 0x0

    .line 132
    .local v7, "path":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 133
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-direct {p0, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v1

    .line 134
    .local v1, "itemUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 136
    .local v6, "cur":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 137
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    const-string/jumbo v2, "path"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 141
    :cond_0
    if-eqz v6, :cond_1

    .line 142
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 145
    :cond_1
    return-object v7

    .line 140
    :catchall_0
    move-exception v2

    .line 141
    if-eqz v6, :cond_2

    .line 142
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 144
    :cond_2
    throw v2
.end method

.method public static getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;

    invoke-direct {v0}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    .line 105
    :cond_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    return-object v0
.end method

.method private getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "indices"    # Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;

    .prologue
    .line 81
    new-instance v0, Lcom/vlingo/core/internal/memo/Memo;

    invoke-direct {v0}, Lcom/vlingo/core/internal/memo/Memo;-><init>()V

    .line 82
    .local v0, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iget v2, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;->KEY_NAME_COL:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->truncateDateFromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setTitle(Ljava/lang/String;)V

    .line 84
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setContent(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setText(Ljava/lang/String;)V

    .line 86
    iget v2, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;->KEY_ID_COL:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/memo/Memo;->setId(I)V

    .line 87
    iget v2, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;->KEY_DATE_COL:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/memo/Memo;->setDate(Ljava/lang/String;)V

    .line 89
    return-object v0
.end method

.method private getUriFromId(J)Landroid/net/Uri;
    .locals 4
    .param p1, "id"    # J

    .prologue
    .line 149
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getUpdateContentProviderUri()Landroid/net/Uri;

    move-result-object v0

    .line 151
    .local v0, "contentUri":Landroid/net/Uri;
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "using "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 153
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static isInstalled()Z
    .locals 2

    .prologue
    .line 98
    const-string/jumbo v0, "com.sec.android.app.snotebook"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static truncateDateFromString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "title"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    .end local p0    # "title":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "title":Ljava/lang/String;
    :cond_0
    const-string/jumbo v0, ".snb$"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\\_\\d{6,10}\\_\\d{6,10}$"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public deleteMemo(Landroid/content/Context;J)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 165
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 166
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    invoke-direct/range {p0 .. p3}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->getFilePathFromId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v17

    .line 167
    .local v17, "strFilePath":Ljava/lang/String;
    const/4 v7, 0x0

    .line 169
    .local v7, "bDeleted":Z
    if-nez v17, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 174
    .local v12, "file":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 175
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v7

    .line 178
    :cond_2
    if-eqz v7, :cond_0

    .line 182
    const-string/jumbo v5, "content://com.infraware.provider.SNoteProvider/fileMgr"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 183
    .local v2, "ofileMgrUri":Landroid/net/Uri;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "path = \""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 184
    .local v4, "strWhere":Ljava/lang/String;
    const/16 v18, 0x0

    .line 185
    .local v18, "strSCloudType":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v3, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "account_type"

    aput-object v6, v3, v5

    .line 187
    .local v3, "columns":[Ljava/lang/String;
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 188
    .local v15, "oCursor":Landroid/database/Cursor;
    if-eqz v15, :cond_3

    .line 189
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-gtz v5, :cond_5

    .line 190
    const-string/jumbo v18, ""

    .line 196
    :goto_1
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 199
    :cond_3
    if-nez v18, :cond_4

    .line 200
    const-string/jumbo v18, ""

    .line 203
    :cond_4
    const-string/jumbo v5, "com.osp.app.signin"

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 205
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 206
    .local v16, "oValue":Landroid/content/ContentValues;
    const-string/jumbo v5, "deleted"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 207
    const-string/jumbo v5, "dirty"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 208
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 209
    .local v8, "correctedTime":J
    const-wide/16 v13, 0x0

    .line 211
    .local v13, "mtimeDifference":J
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "TIME_DIFFERENCE"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v13

    .line 216
    :goto_2
    sub-long/2addr v8, v13

    .line 217
    :try_start_2
    const-string/jumbo v5, "sync2"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 218
    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v1, v2, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 223
    .end local v1    # "contentResolver":Landroid/content/ContentResolver;
    .end local v2    # "ofileMgrUri":Landroid/net/Uri;
    .end local v3    # "columns":[Ljava/lang/String;
    .end local v4    # "strWhere":Ljava/lang/String;
    .end local v7    # "bDeleted":Z
    .end local v8    # "correctedTime":J
    .end local v12    # "file":Ljava/io/File;
    .end local v13    # "mtimeDifference":J
    .end local v15    # "oCursor":Landroid/database/Cursor;
    .end local v16    # "oValue":Landroid/content/ContentValues;
    .end local v17    # "strFilePath":Ljava/lang/String;
    .end local v18    # "strSCloudType":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 224
    .local v11, "excetion":Ljava/lang/Exception;
    const-string/jumbo v5, "MemoUtilException:"

    invoke-virtual {v11}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    .line 226
    new-instance v5, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v6, "Error in deleting a memo."

    invoke-direct {v5, v6}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 192
    .end local v11    # "excetion":Ljava/lang/Exception;
    .restart local v1    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v2    # "ofileMgrUri":Landroid/net/Uri;
    .restart local v3    # "columns":[Ljava/lang/String;
    .restart local v4    # "strWhere":Ljava/lang/String;
    .restart local v7    # "bDeleted":Z
    .restart local v12    # "file":Ljava/io/File;
    .restart local v15    # "oCursor":Landroid/database/Cursor;
    .restart local v17    # "strFilePath":Ljava/lang/String;
    .restart local v18    # "strSCloudType":Ljava/lang/String;
    :cond_5
    :try_start_3
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    .line 193
    const/4 v5, 0x0

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    goto :goto_1

    .line 212
    .restart local v8    # "correctedTime":J
    .restart local v13    # "mtimeDifference":J
    .restart local v16    # "oValue":Landroid/content/ContentValues;
    :catch_1
    move-exception v10

    .line 214
    .local v10, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-wide/16 v13, 0x0

    goto :goto_2

    .line 221
    .end local v8    # "correctedTime":J
    .end local v10    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .end local v13    # "mtimeDifference":J
    .end local v16    # "oValue":Landroid/content/ContentValues;
    :cond_6
    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method

.method public getCreateMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    const-string/jumbo v0, "android.intent.action.VOICETALK_NEW_SMEMO"

    return-object v0
.end method

.method protected getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 93
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;-><init>(Landroid/database/Cursor;)V

    .line 94
    .local v0, "indices":Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method public getMemos(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    const/4 v6, 0x0

    .line 241
    .local v6, "cur":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 244
    .local v7, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getContentProviderUri()Landroid/net/Uri;

    move-result-object v1

    .line 245
    .local v1, "contentUri":Landroid/net/Uri;
    const/4 v3, 0x0

    .line 246
    .local v3, "SELECTION":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 247
    const-string/jumbo v3, "IsFolder=\'0\' and Islocked=\'0\' and deleted=\'0\'"

    .line 256
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 257
    if-nez v6, :cond_3

    .line 259
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Cannot resolve provider for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    :goto_1
    if-eqz v6, :cond_0

    .line 271
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 274
    :cond_0
    return-object v7

    .line 249
    :cond_1
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 250
    const-string/jumbo v3, "IsFolder=\'0\' and Islocked=\'0\' and deleted=\'0\'"

    .line 251
    goto :goto_0

    .line 252
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " and "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "IsFolder=\'0\' and Islocked=\'0\' and deleted=\'0\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 260
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_5

    .line 262
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v2, "No enabled memos"

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 269
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v3    # "SELECTION":Ljava/lang/String;
    :catchall_0
    move-exception v0

    .line 270
    :goto_2
    if-eqz v6, :cond_4

    .line 271
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 273
    :cond_4
    throw v0

    .line 264
    .restart local v1    # "contentUri":Landroid/net/Uri;
    .restart local v3    # "SELECTION":Ljava/lang/String;
    :cond_5
    :try_start_2
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 266
    .end local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .local v8, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :cond_6
    :try_start_3
    invoke-virtual {p0, v6}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v0

    if-nez v0, :cond_6

    move-object v7, v8

    .line 269
    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_1

    .end local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :catchall_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_2
.end method

.method public getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 233
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    .line 234
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "ModifiedTime DESC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public getViewMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    const-string/jumbo v0, "android.intent.action.VOICETALK_VIEW_SMEMO"

    return-object v0
.end method

.method public searchMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    .line 159
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "ModifiedTime DESC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->searchMemos(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public updateMemo(Landroid/content/Context;Lcom/vlingo/core/internal/memo/Memo;Lcom/vlingo/core/internal/memo/Memo;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "originalMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .param p3, "changedMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 120
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 121
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "name"

    invoke-virtual {p3}, Lcom/vlingo/core/internal/memo/Memo;->getContent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p2}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v3

    int-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v1

    .line 124
    .local v1, "updateUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 125
    .local v0, "rowsUpdated":I
    const/4 v3, 0x1

    if-eq v0, v3, :cond_0

    .line 126
    new-instance v3, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v4, "Error in updating memo."

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 128
    :cond_0
    return-void
.end method
