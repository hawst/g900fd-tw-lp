.class Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction$1;
.super Ljava/lang/Object;
.source "SaveMemoAction.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->execute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction$1;->this$0:Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 46
    # getter for: Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->access$0()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v1

    const-string/jumbo v2, "Memo: saving ..."

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/MemoManager;->getMemoUtil()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v0

    .line 49
    .local v0, "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    # getter for: Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->access$0()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "using "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to save  \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction$1;->this$0:Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;

    # getter for: Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->memo:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->access$1(Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 50
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction$1;->this$0:Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;

    # invokes: Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->access$2(Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction$1;->this$0:Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;

    # getter for: Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->memo:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->access$1(Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/memo/IMemoUtil;->saveMemoData(Landroid/content/Context;Ljava/lang/String;)V

    .line 51
    return-void
.end method
