.class public Lcom/vlingo/midas/samsungutils/utils/TalkbackUtils;
.super Ljava/lang/Object;
.source "TalkbackUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isTalkbackEnabled(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 11
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "accessibility_enabled"

    .line 10
    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-ne v3, v0, :cond_0

    .line 15
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 10
    goto :goto_0

    .line 12
    :catch_0
    move-exception v1

    .local v1, "ex":Landroid/provider/Settings$SettingNotFoundException;
    move v0, v2

    .line 13
    goto :goto_0
.end method
