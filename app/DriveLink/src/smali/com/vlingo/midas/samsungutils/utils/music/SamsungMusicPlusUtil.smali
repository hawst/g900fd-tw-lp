.class public Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;
.super Ljava/lang/Object;
.source "SamsungMusicPlusUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/MusicPlusUtil;


# static fields
.field private static synthetic $SWITCH_TABLE$com$vlingo$core$internal$util$MusicPlusUtil$PlayType:[I = null

.field private static final DEFAULT_ALBUM_ART_URI_PREFIX:Ljava/lang/String; = "content://media/external/audio/albumart/"

.field private static final DEFAULT_MUSIC_SQUARE_URL:Ljava/lang/String; = "content://com.sec.music/music_square/music_square/"

.field private static final LOCAL_MUSICPLUS_ALBUM_ART_URI:Ljava/lang/String; = "content://com.samsung.musicplus/audio/albumart/"

.field private static final LOCAL_MUSICPLUS_ALBUM_URI:Ljava/lang/String; = "content://com.samsung.musicplus/audio/album"

.field private static final LOCAL_MUSICPLUS_ARTIST_URI:Ljava/lang/String; = "content://com.samsung.musicplus/audio/artists"

.field private static final LOCAL_MUSICPLUS_GENERAL_URI:Ljava/lang/String; = "content://com.samsung.musicplus/audio/media"

.field private static final LOCAL_MUSICPLUS_PLAYLIST_URI:Ljava/lang/String; = "content://com.samsung.musicplus/audio/playlists"

.field private static final MUSICPLUS_SQUARE_CONTENT_URI:Ljava/lang/String; = "content://com.samsung.musicplus/mood_"

.field public static final NONMOOD:Z = false

.field private static final SAMSUNG_MUSICPLUS_AUTHORITY:Ljava/lang/String; = "com.samsung.musicplus"

.field public static final USEMOOD:Z = true

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# direct methods
.method static synthetic $SWITCH_TABLE$com$vlingo$core$internal$util$MusicPlusUtil$PlayType()[I
    .locals 3

    .prologue
    .line 19
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->$SWITCH_TABLE$com$vlingo$core$internal$util$MusicPlusUtil$PlayType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->values()[Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUM:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ARTIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->$SWITCH_TABLE$com$vlingo$core$internal$util$MusicPlusUtil$PlayType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 35
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFavoriteMusicIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 136
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.music.intent.action.PLAY_VIA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 138
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->getMusicAppVersion()I

    move-result v1

    if-ge v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->usingLocalMusicDB(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    const-string/jumbo v1, "extra_type"

    const-string/jumbo v2, "favorite_playlist"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    const-string/jumbo v1, "launchMusicPlayer"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 150
    :goto_0
    return-object v0

    .line 142
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->getMusicAppVersion()I

    move-result v1

    if-ge v1, v2, :cond_1

    .line 143
    const-string/jumbo v1, "extra_type"

    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAYLIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 144
    const-string/jumbo v1, "extra_name"

    const-string/jumbo v2, "Quick list"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 147
    :cond_1
    const-string/jumbo v1, "extra_type"

    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAYLIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 148
    const-string/jumbo v1, "extra_name"

    const-string/jumbo v2, "FavoriteList#328795!432@1341"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public getMusicAppVersion()I
    .locals 7

    .prologue
    .line 119
    const/4 v1, 0x0

    .line 122
    .local v1, "musicAppVersion":I
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 123
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const-string/jumbo v4, "com.sec.android.app.music"

    const/16 v5, 0x4000

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 125
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Package verion = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 126
    iget v1, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v1

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 129
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMusicFavorite()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 100
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 101
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "com.samsung.musicplus"

    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 102
    .local v0, "provider":Landroid/content/ContentProviderClient;
    if-eqz v0, :cond_1

    move v1, v2

    .line 104
    .local v1, "ret":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 105
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 107
    :cond_0
    if-nez v1, :cond_2

    .line 108
    const-string/jumbo v2, "name like \'%Quick list%\'"

    .line 114
    :goto_1
    return-object v2

    .end local v1    # "ret":Z
    :cond_1
    move v1, v3

    .line 102
    goto :goto_0

    .line 110
    .restart local v1    # "ret":Z
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->getMusicAppVersion()I

    move-result v4

    const/16 v5, 0xb

    if-ge v4, v5, :cond_3

    move v1, v2

    .line 112
    :goto_2
    if-eqz v1, :cond_4

    .line 113
    const-string/jumbo v2, "is_favorite =1"

    goto :goto_1

    :cond_3
    move v1, v3

    .line 110
    goto :goto_2

    .line 114
    :cond_4
    const-string/jumbo v2, "name like \'FavoriteList#328795!432@1341\'"

    goto :goto_1
.end method

.method public getMusicMoodUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "mood"    # Ljava/lang/String;

    .prologue
    .line 85
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->usingLocalMusicDB(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "content://com.samsung.musicplus/mood_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "content://com.sec.music/music_square/music_square/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicPlaylistUri(J)Landroid/net/Uri;
    .locals 2
    .param p1, "playlist_id"    # J

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->usingLocalMusicDB(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "content://com.samsung.musicplus/audio/playlists/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/members"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "external"

    invoke-static {v0, p1, p2}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;
    .locals 3
    .param p1, "Music"    # Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    .prologue
    .line 56
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->usingLocalMusicDB(Z)Z

    move-result v0

    .line 57
    .local v0, "usingLocalMusicDB":Z
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->$SWITCH_TABLE$com$vlingo$core$internal$util$MusicPlusUtil$PlayType()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 79
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 59
    :pswitch_0
    if-eqz v0, :cond_0

    .line 60
    const-string/jumbo v1, "content://com.samsung.musicplus/audio/media"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 61
    :cond_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 63
    :pswitch_1
    if-eqz v0, :cond_1

    .line 64
    const-string/jumbo v1, "content://com.samsung.musicplus/audio/artists"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 65
    :cond_1
    sget-object v1, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 67
    :pswitch_2
    if-eqz v0, :cond_2

    .line 68
    const-string/jumbo v1, "content://com.samsung.musicplus/audio/album"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 69
    :cond_2
    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 71
    :pswitch_3
    if-eqz v0, :cond_3

    .line 72
    const-string/jumbo v1, "content://com.samsung.musicplus/audio/playlists"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 73
    :cond_3
    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 75
    :pswitch_4
    if-eqz v0, :cond_4

    .line 76
    const-string/jumbo v1, "content://com.samsung.musicplus/audio/albumart/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 77
    :cond_4
    const-string/jumbo v1, "content://media/external/audio/albumart/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public usingLocalMusicDB(Z)Z
    .locals 6
    .param p1, "usingMood"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 39
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 40
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "com.samsung.musicplus"

    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 41
    .local v0, "provider":Landroid/content/ContentProviderClient;
    if-eqz v0, :cond_1

    move v1, v2

    .line 42
    .local v1, "ret":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 43
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 46
    :cond_0
    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    .line 51
    :goto_1
    return v2

    .end local v1    # "ret":Z
    :cond_1
    move v1, v3

    .line 41
    goto :goto_0

    .line 49
    .restart local v1    # "ret":Z
    :cond_2
    if-eqz v1, :cond_3

    .line 50
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;->getMusicAppVersion()I

    move-result v4

    const/16 v5, 0xb

    if-ge v4, v5, :cond_4

    move v1, v2

    :cond_3
    :goto_2
    move v2, v1

    .line 51
    goto :goto_1

    :cond_4
    move v1, v3

    .line 50
    goto :goto_2
.end method
