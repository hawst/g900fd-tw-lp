.class public Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;
.super Ljava/lang/Object;
.source "KMemoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "MemoIndices"
.end annotation


# instance fields
.field final KEY_CONTENT_COL:I

.field public final KEY_DATE_COL:I

.field final KEY_ID_COL:I

.field public final KEY_TEXT_COL:I

.field final KEY_TITLE_COL:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 5
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    # getter for: Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->loggedMemoSchema:Z
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->access$2()Z

    move-result v2

    if-nez v2, :cond_0

    .line 195
    # getter for: Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->access$3()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "column names for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getContentProviderUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 196
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    .line 197
    .local v0, "columnCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 204
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->access$4(Z)V

    .line 208
    .end local v0    # "columnCount":I
    .end local v1    # "i":I
    :cond_0
    const-string/jumbo v2, "_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_ID_COL:I

    .line 209
    const-string/jumbo v2, "title"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_TITLE_COL:I

    .line 210
    const-string/jumbo v2, "strippedContent"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_CONTENT_COL:I

    .line 211
    const-string/jumbo v2, "strippedContent"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_TEXT_COL:I

    .line 212
    const-string/jumbo v2, "lastModifiedAt"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_DATE_COL:I

    .line 213
    return-void

    .line 198
    .restart local v0    # "columnCount":I
    .restart local v1    # "i":I
    :cond_1
    if-nez v1, :cond_2

    .line 199
    # getter for: Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->access$3()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 197
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 201
    :cond_2
    # getter for: Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->access$3()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1
.end method
