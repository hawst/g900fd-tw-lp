.class public Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;
.super Lcom/vlingo/core/internal/util/AlarmUtil;
.source "SamsungAlarmUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;
    }
.end annotation


# static fields
.field private static final ALARM_ACTIVE:Ljava/lang/String; = "active"

.field private static final ALARM_ALARMTIME:Ljava/lang/String; = "alarmtime"

.field private static final ALARM_CREATETIME:Ljava/lang/String; = "createtime"

.field private static final ALARM_ID:Ljava/lang/String; = "_id"

.field private static final ALARM_NAME:Ljava/lang/String; = "name"

.field private static final ALARM_PROJECTION:[Ljava/lang/String;

.field private static final ALARM_REPEATTYPE:Ljava/lang/String; = "repeattype"

.field private static final ALARM_URI:Landroid/net/Uri;

.field private static final AUTHORITY:Ljava/lang/String; = "com.samsung.sec.android.clockpackage"

.field private static final REPEATING_INCLUSION_MASK:I = 0xf

.field private static final REPEATING_REMOVAL_MASK:I = -0x10

.field private static final TABLE_NAME:Ljava/lang/String; = "alarm"

.field private static final TAG:Ljava/lang/String;

.field private static final WEEKLY_NO_REPEAT_MASK:I = 0x1

.field private static final WEEKLY_REPEAT_MASK:I = 0x5


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->TAG:Ljava/lang/String;

    .line 32
    const-string/jumbo v0, "content://com.samsung.sec.android.clockpackage/alarm"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->ALARM_URI:Landroid/net/Uri;

    .line 63
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 64
    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 65
    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 66
    const-string/jumbo v2, "active"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 67
    const-string/jumbo v2, "alarmtime"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 68
    const-string/jumbo v2, "repeattype"

    aput-object v2, v0, v1

    .line 63
    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->ALARM_PROJECTION:[Ljava/lang/String;

    .line 69
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/AlarmUtil;-><init>()V

    return-void
.end method

.method public static deleteAlarm(Landroid/content/Context;J)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    .line 141
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getAlarm(Landroid/content/Context;J)Lcom/vlingo/core/internal/util/Alarm;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    .line 77
    const/4 v7, 0x0

    .line 80
    .local v7, "cur":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 82
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->ALARM_URI:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 85
    .local v1, "myAlarm":Landroid/net/Uri;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->ALARM_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 87
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 88
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 90
    new-instance v8, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;

    invoke-direct {v8, v7}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;-><init>(Landroid/database/Cursor;)V

    .line 91
    .local v8, "indices":Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;
    invoke-static {v7, v8}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->getAlarm(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;)Lcom/vlingo/core/internal/util/Alarm;
    :try_end_0
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 102
    .local v6, "alarm":Lcom/vlingo/core/internal/util/Alarm;
    if-eqz v7, :cond_0

    .line 103
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 93
    :cond_0
    return-object v6

    .line 95
    .end local v6    # "alarm":Lcom/vlingo/core/internal/util/Alarm;
    .end local v8    # "indices":Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;
    :cond_1
    :try_start_1
    new-instance v2, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v3, "Error in getting alarm."

    invoke-direct {v2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "myAlarm":Landroid/net/Uri;
    :catch_0
    move-exception v9

    .line 98
    .local v9, "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    :try_start_2
    const-string/jumbo v2, "ScheduleUtilException:"

    invoke-virtual {v9}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {v9}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->printStackTrace()V

    .line 100
    throw v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 101
    .end local v9    # "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    :catchall_0
    move-exception v2

    .line 102
    if-eqz v7, :cond_2

    .line 103
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 105
    :cond_2
    throw v2
.end method

.method private static getAlarm(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;)Lcom/vlingo/core/internal/util/Alarm;
    .locals 3
    .param p0, "c"    # Landroid/database/Cursor;
    .param p1, "indices"    # Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;

    .prologue
    .line 154
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/Alarm;-><init>()V

    .line 155
    .local v0, "alarm":Lcom/vlingo/core/internal/util/Alarm;
    iget v1, p1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_NAME_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setName(Ljava/lang/String;)V

    .line 156
    iget v1, p1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_ACTIVE_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->fromInt(I)Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    .line 157
    iget v1, p1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_ALARMTIME_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/util/Alarm;->setTime(J)V

    .line 158
    iget v1, p1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_ID_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setId(I)V

    .line 159
    iget v1, p1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_REPEATTYPE_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->getDayMask(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setDayMask(I)V

    .line 160
    iget v1, p1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_REPEATTYPE_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->isRepeating(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/Alarm;->setWeeklyRepeating(Z)V

    .line 161
    return-object v0
.end method

.method public static getAlarms(Landroid/content/Context;)Ljava/util/List;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 185
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 186
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->ALARM_URI:Landroid/net/Uri;

    .line 187
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->ALARM_PROJECTION:[Ljava/lang/String;

    .line 190
    const-string/jumbo v5, "alarmtime ASC"

    move-object v4, v3

    .line 185
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 192
    .local v7, "cur":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 194
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Cannot resolve provider for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->ALARM_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :cond_0
    :goto_0
    return-object v3

    .line 198
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 200
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "No enabled alarms"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    if-eqz v7, :cond_0

    .line 203
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 208
    :cond_2
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 210
    .local v6, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    new-instance v8, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;

    invoke-direct {v8, v7}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;-><init>(Landroid/database/Cursor;)V

    .line 213
    .local v8, "indices":Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;
    :cond_3
    invoke-static {v7, v8}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->getAlarm(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;)Lcom/vlingo/core/internal/util/Alarm;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 215
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v3, v6

    .line 216
    goto :goto_0
.end method

.method public static getAlarms(Landroid/content/Context;Lcom/vlingo/core/internal/alarms/AlarmQueryObject;)Ljava/util/List;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "queryObject"    # Lcom/vlingo/core/internal/alarms/AlarmQueryObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/alarms/AlarmQueryObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    invoke-static {p0}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->getAlarms(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 111
    .local v2, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 113
    .local v4, "results":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    invoke-virtual {p1}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->getCount()I

    move-result v3

    .line 114
    .local v3, "maxCount":I
    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 115
    const-string/jumbo v5, "widget_display_max"

    .line 116
    const/4 v6, 0x5

    .line 115
    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 118
    :cond_0
    const/4 v0, 0x0

    .line 120
    .local v0, "addedCount":I
    if-eqz v2, :cond_2

    .line 121
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 131
    :cond_2
    return-object v4

    .line 121
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/Alarm;

    .line 122
    .local v1, "alarm":Lcom/vlingo/core/internal/util/Alarm;
    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/alarms/AlarmQueryObject;->matches(Lcom/vlingo/core/internal/util/Alarm;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 123
    if-ge v0, v3, :cond_1

    .line 125
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static getColumnTypes(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;)V
    .locals 7
    .param p0, "c"    # Landroid/database/Cursor;
    .param p1, "indices"    # Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;

    .prologue
    .line 146
    iget v4, p1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_ID_COL:I

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getType(I)I

    move-result v1

    .line 147
    .local v1, "column_id_type":I
    iget v4, p1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_NAME_COL:I

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getType(I)I

    move-result v2

    .line 148
    .local v2, "column_name_type":I
    iget v4, p1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_ACTIVE_COL:I

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    .line 149
    .local v0, "column_active_type":I
    iget v4, p1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_ALARMTIME_COL:I

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getType(I)I

    move-result v3

    .line 150
    .local v3, "column_time_type":I
    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "foo"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 151
    return-void
.end method

.method private static getDayMask(I)I
    .locals 1
    .param p0, "repeatColumnValue"    # I

    .prologue
    .line 181
    and-int/lit8 v0, p0, -0x10

    shr-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public static getLatestAlarms(Landroid/content/Context;)Ljava/util/List;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 221
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 222
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->ALARM_URI:Landroid/net/Uri;

    .line 223
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->ALARM_PROJECTION:[Ljava/lang/String;

    .line 226
    const-string/jumbo v5, "createtime DESC"

    move-object v4, v3

    .line 221
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 228
    .local v7, "cur":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 230
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Cannot resolve provider for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->ALARM_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :goto_0
    return-object v3

    .line 234
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 236
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "No enabled alarms"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 241
    :cond_1
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 243
    .local v6, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    new-instance v8, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;

    invoke-direct {v8, v7}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;-><init>(Landroid/database/Cursor;)V

    .line 246
    .local v8, "indices":Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;
    :cond_2
    invoke-static {v7, v8}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->getAlarm(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;)Lcom/vlingo/core/internal/util/Alarm;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 248
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v3, v6

    .line 249
    goto :goto_0
.end method

.method private static isRepeating(I)Z
    .locals 2
    .param p0, "repeatColumnValue"    # I

    .prologue
    .line 170
    and-int/lit8 v0, p0, 0xf

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static updateAlarm(Landroid/content/Context;Lcom/vlingo/core/internal/util/Alarm;Lcom/vlingo/core/internal/util/Alarm;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "originalAlarm"    # Lcom/vlingo/core/internal/util/Alarm;
    .param p2, "changedAlarm"    # Lcom/vlingo/core/internal/util/Alarm;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    .line 136
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
