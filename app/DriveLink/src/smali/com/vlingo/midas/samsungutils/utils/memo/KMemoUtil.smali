.class public final Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;
.super Lcom/vlingo/core/internal/memo/MemoUtil;
.source "KMemoUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/memo/IMemoUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;,
        Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;
    }
.end annotation


# static fields
.field private static final KEY_CONTENT:Ljava/lang/String; = "content"

.field private static final KEY_TITLE:Ljava/lang/String; = "title"

.field public static final MEMO_BASE_URI:Landroid/net/Uri;

.field private static final MEMO_DATA:Ljava/lang/String; = "_data"

.field private static final MEMO_ID:Ljava/lang/String; = "_id"

.field private static final MEMO_LASTMODIFIEDAT:Ljava/lang/String; = "lastModifiedAt"

.field private static final MEMO_PROJECTION:[Ljava/lang/String;

.field public static final MEMO_SERVICE_ACTION:Ljava/lang/String; = "com.samsung.android.intent.action.MEMO_SERVICE"

.field private static final MEMO_STRIPPEDCONTENT:Ljava/lang/String; = "strippedContent"

.field private static final MEMO_TITLE:Ljava/lang/String; = "title"

.field private static final MEMO_VRFILEUUID:Ljava/lang/String; = "vrfileuuid"

.field private static final SORT_ORDER:Ljava/lang/String; = "lastModifiedAt ASC"

.field private static instance:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;

.field private static loggedMemoSchema:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHolder:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

.field private mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

.field private mSvcConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    const-class v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 39
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    invoke-direct {v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->instance:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    .line 42
    sput-boolean v2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->loggedMemoSchema:Z

    .line 225
    const-string/jumbo v0, "content://com.samsung.android.memo/memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_BASE_URI:Landroid/net/Uri;

    .line 238
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    .line 239
    const-string/jumbo v1, "_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    .line 240
    const-string/jumbo v2, "lastModifiedAt"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 241
    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 242
    const-string/jumbo v2, "strippedContent"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 243
    const-string/jumbo v2, "vrfileuuid"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 244
    const-string/jumbo v2, "_data"

    aput-object v2, v0, v1

    .line 238
    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_PROJECTION:[Ljava/lang/String;

    .line 253
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Lcom/vlingo/core/internal/memo/MemoUtil;-><init>()V

    .line 255
    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    .line 256
    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mContext:Landroid/content/Context;

    .line 257
    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    .line 259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    .line 261
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$1;-><init>(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mSvcConnection:Landroid/content/ServiceConnection;

    .line 47
    return-void
.end method

.method static synthetic access$0(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;Lcom/samsung/android/app/memo/MemoServiceIF;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    return-void
.end method

.method static synthetic access$1(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;)V
    .locals 0

    .prologue
    .line 297
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->createPendingMemos()V

    return-void
.end method

.method static synthetic access$2()Z
    .locals 1

    .prologue
    .line 42
    sget-boolean v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->loggedMemoSchema:Z

    return v0
.end method

.method static synthetic access$3()Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-object v0
.end method

.method static synthetic access$4(Z)V
    .locals 0

    .prologue
    .line 42
    sput-boolean p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->loggedMemoSchema:Z

    return-void
.end method

.method private createPendingMemos()V
    .locals 7

    .prologue
    .line 298
    iget-object v5, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    monitor-enter v5

    .line 300
    :try_start_0
    iget-object v4, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 311
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->unbindService()V

    .line 298
    monitor-exit v5

    .line 313
    return-void

    .line 300
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 301
    .local v2, "holder":Landroid/os/Bundle;
    const-string/jumbo v6, "title"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 302
    .local v3, "title":Ljava/lang/String;
    const-string/jumbo v6, "content"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 304
    .local v0, "content":Ljava/lang/String;
    :try_start_1
    invoke-direct {p0, v3, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->newMemo1(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    iget-object v6, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 306
    :catch_0
    move-exception v1

    .line 307
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_2
    iget-object v6, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    if-eqz v6, :cond_0

    .line 308
    iget-object v6, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    invoke-interface {v6, v3, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;->onError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    .end local v0    # "content":Ljava/lang/String;
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v2    # "holder":Landroid/os/Bundle;
    .end local v3    # "title":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4
.end method

.method private getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->instance:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    return-object v0
.end method

.method private getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 4
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "indices"    # Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;

    .prologue
    .line 162
    new-instance v0, Lcom/vlingo/core/internal/memo/Memo;

    invoke-direct {v0}, Lcom/vlingo/core/internal/memo/Memo;-><init>()V

    .line 163
    .local v0, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_TITLE_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setTitle(Ljava/lang/String;)V

    .line 164
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_CONTENT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setContent(Ljava/lang/String;)V

    .line 165
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_TEXT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setText(Ljava/lang/String;)V

    .line 167
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "KMemoUtil.getMemo setting memo text to \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/vlingo/core/internal/memo/Memo;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\' from persistence"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 168
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_ID_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setId(I)V

    .line 169
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_DATE_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setDate(Ljava/lang/String;)V

    .line 171
    return-object v0
.end method

.method private getUriFromId(J)Landroid/net/Uri;
    .locals 4
    .param p1, "id"    # J

    .prologue
    .line 175
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getUpdateContentProviderUri()Landroid/net/Uri;

    move-result-object v0

    .line 177
    .local v0, "contentUri":Landroid/net/Uri;
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "using "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 179
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static isInstalled()Z
    .locals 4

    .prologue
    .line 54
    const/4 v1, 0x0

    .line 55
    .local v1, "ret":Z
    const-string/jumbo v2, "com.samsung.android.app.memo"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v0

    .line 57
    .local v0, "kmemo_installed":Z
    if-eqz v0, :cond_0

    .line 59
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v3, "Kmemo is installed."

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 60
    const/4 v1, 0x1

    .line 63
    :cond_0
    return v1
.end method

.method private newMemo1(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 286
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/MemoServiceIF;->createNew()Ljava/lang/String;

    .line 287
    if-eqz p1, :cond_0

    .line 288
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/memo/MemoServiceIF;->setTitle(Ljava/lang/String;)V

    .line 289
    :cond_0
    if-eqz p2, :cond_1

    .line 290
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v0, p2}, Lcom/samsung/android/app/memo/MemoServiceIF;->appendText(Ljava/lang/String;)V

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/MemoServiceIF;->saveCurrent()J

    .line 292
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    if-eqz v0, :cond_2

    .line 293
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/MemoServiceIF;->getUriWithId()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;->onCreated(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_2
    return-void
.end method


# virtual methods
.method public bindService()V
    .locals 4

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->isServiceBinded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 321
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.samsung.android.intent.action.MEMO_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 322
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mSvcConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 326
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 324
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->createPendingMemos()V

    goto :goto_0
.end method

.method public deleteMemo(Landroid/content/Context;J)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 125
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 126
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    invoke-direct {p0, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 127
    invoke-direct {p0, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 128
    .local v1, "rowsDeleted":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    .line 129
    new-instance v3, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v4, "Error in deleting a memo."

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lcom/vlingo/core/internal/memo/MemoUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "rowsDeleted":I
    :catch_0
    move-exception v2

    .line 135
    .local v2, "sux":Lcom/vlingo/core/internal/memo/MemoUtilException;
    const-string/jumbo v3, "MemoUtilException:"

    invoke-virtual {v2}, Lcom/vlingo/core/internal/memo/MemoUtilException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    invoke-virtual {v2}, Lcom/vlingo/core/internal/memo/MemoUtilException;->printStackTrace()V

    .line 137
    throw v2

    .line 139
    .end local v2    # "sux":Lcom/vlingo/core/internal/memo/MemoUtilException;
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    :cond_0
    return-void
.end method

.method public getCreateMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const-string/jumbo v0, "android.intent.action.VIEW"

    return-object v0
.end method

.method public getListener()Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    return-object v0
.end method

.method protected getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 150
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;-><init>(Landroid/database/Cursor;)V

    .line 151
    .local v0, "indices":Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method public getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->listRecentMemos()Landroid/database/Cursor;

    move-result-object v0

    .line 100
    .local v0, "cursorMemo":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 101
    .local v1, "memo":Lcom/vlingo/core/internal/memo/Memo;
    if-eqz v0, :cond_0

    .line 103
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 104
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    .line 105
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 107
    :cond_0
    return-object v1
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public getViewMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string/jumbo v0, "android.intent.action.VIEW"

    return-object v0
.end method

.method public isServiceBinded()Z
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public listRecentMemos()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 350
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_BASE_URI:Landroid/net/Uri;

    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v5, "lastModifiedAt DESC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public newMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 275
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 276
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string/jumbo v1, "content"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v2, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    monitor-enter v2

    .line 279
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 278
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->bindService()V

    .line 283
    return-void

    .line 278
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public openMemo(J)V
    .locals 2
    .param p1, "_id"    # J

    .prologue
    .line 354
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 355
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_BASE_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 356
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 357
    return-void
.end method

.method public saveMemoData(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->setContext(Landroid/content/Context;)V

    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->newMemo(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public searchMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "content"

    aput-object v2, v0, v1

    .line 158
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "lastModifiedAt ASC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->searchMemos(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 338
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mContext:Landroid/content/Context;

    .line 339
    return-void
.end method

.method public setListener(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    .prologue
    .line 346
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    .line 347
    return-void
.end method

.method public unbindService()V
    .locals 2

    .prologue
    .line 329
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    .line 330
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mSvcConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 331
    return-void
.end method

.method public updateMemo(Landroid/content/Context;Lcom/vlingo/core/internal/memo/Memo;Lcom/vlingo/core/internal/memo/Memo;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "originalMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .param p3, "changedMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 117
    return-void
.end method
