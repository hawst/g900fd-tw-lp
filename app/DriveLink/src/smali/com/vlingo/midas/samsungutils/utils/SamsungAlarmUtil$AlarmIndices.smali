.class Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;
.super Ljava/lang/Object;
.source "SamsungAlarmUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AlarmIndices"
.end annotation


# instance fields
.field final ALARM_ACTIVE_COL:I

.field final ALARM_ALARMTIME_COL:I

.field final ALARM_ID_COL:I

.field final ALARM_NAME_COL:I

.field final ALARM_REPEATTYPE_COL:I


# direct methods
.method constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    const-string/jumbo v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_ID_COL:I

    .line 261
    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_NAME_COL:I

    .line 262
    const-string/jumbo v0, "active"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_ACTIVE_COL:I

    .line 263
    const-string/jumbo v0, "alarmtime"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_ALARMTIME_COL:I

    .line 264
    const-string/jumbo v0, "repeattype"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil$AlarmIndices;->ALARM_REPEATTYPE_COL:I

    .line 265
    return-void
.end method
