.class public final Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;
.super Ljava/lang/Object;
.source "SVoxPronunciationManager.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/TTSEngine$PronunciationManager;


# static fields
.field private static final SEARCH_AROUND_MONTH_LENGTH:I = 0x7

.field private static final TAG:Ljava/lang/String;

.field private static final smBigNumbers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static smInstance:Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;

.field private static final smLongPrice:Ljava/util/regex/Pattern;

.field private static final smLongPricePatternDollar:Ljava/util/regex/Pattern;

.field private static final smLongPricePatternEuro:Ljava/util/regex/Pattern;

.field private static final smLongPricePatternPound:Ljava/util/regex/Pattern;

.field private static final smLongPricePatternPoundLb:Ljava/util/regex/Pattern;

.field private static final smLongPricePatternPoundLbs:Ljava/util/regex/Pattern;

.field private static final smLongPricePatternPoundPriceLbs:Ljava/util/regex/Pattern;

.field private static final smLongWeightPatternG:Ljava/util/regex/Pattern;

.field private static final smLongWeightPatternKg:Ljava/util/regex/Pattern;

.field private static final smLongWeightPatternL:Ljava/util/regex/Pattern;

.field private static final smLongWeightPatternOz:Ljava/util/regex/Pattern;

.field private static final smMemoName:Ljava/util/regex/Pattern;

.field private static final smPhoneNumberPattern:Ljava/util/regex/Pattern;

.field private static final smPhoneNumberWithSpacePattern:Ljava/util/regex/Pattern;

.field private static final smPhoneticWords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final smSpecialDomains:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final smSpecialSymbols:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final smSpecialWords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final smUrl:Ljava/util/regex/Pattern;


# instance fields
.field private final log:Lcom/vlingo/core/internal/logging/Logger;

.field monthes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->TAG:Ljava/lang/String;

    .line 32
    const-string/jumbo v0, "(\\d+\\-)+\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smPhoneNumberPattern:Ljava/util/regex/Pattern;

    .line 33
    const-string/jumbo v0, "(\\d(\\s\\d)*\\s-\\s)+\\d(\\s\\d)*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smPhoneNumberWithSpacePattern:Ljava/util/regex/Pattern;

    .line 34
    const-string/jumbo v0, "((\\$\\s*)|(\\\u00a3\\s*)|(\\\u20ac\\s*))(\\d+)([.,]?(\\d{3}))+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPrice:Ljava/util/regex/Pattern;

    .line 35
    const-string/jumbo v0, "(\\$\\s*)(\\d+)[.,]?(\\d\\d*)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternDollar:Ljava/util/regex/Pattern;

    .line 36
    const-string/jumbo v0, "(\\\u00a3\\s*)(\\d+)[.,]?(\\d\\d*)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternPound:Ljava/util/regex/Pattern;

    .line 37
    const-string/jumbo v0, "(\\\u20ac\\s*)(\\d+)[.,]?(\\d\\d*)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternEuro:Ljava/util/regex/Pattern;

    .line 40
    const-string/jumbo v0, "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smUrl:Ljava/util/regex/Pattern;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialSymbols:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialWords:Ljava/util/Map;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialDomains:Ljava/util/Map;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smPhoneticWords:Ljava/util/Map;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smBigNumbers:Ljava/util/Map;

    .line 49
    const-string/jumbo v0, "(\\d+)[.,]?(\\d\\d*)?\\s*((\\u004C\\u0062\\u0073)|(\\u004C\\u0062)|(\\u006C\\u0062\\u0073)|(\\u006C\\u0062))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternPoundPriceLbs:Ljava/util/regex/Pattern;

    .line 51
    const-string/jumbo v0, "(\\s+((\\u004C\\u0062\\u0073)|(\\u006C\\u0062\\u0073))\\s+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternPoundLbs:Ljava/util/regex/Pattern;

    .line 52
    const-string/jumbo v0, "(\\s+((\\u004C\\u0062)|(\\u006C\\u0062))\\s+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternPoundLb:Ljava/util/regex/Pattern;

    .line 53
    const-string/jumbo v0, "(\\d+)[.,]?(\\d\\d*)?\\s*(\\u006b\\u0067)([ \\ta-zA-Z]*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongWeightPatternKg:Ljava/util/regex/Pattern;

    .line 54
    const-string/jumbo v0, "(\\d+)[.,]?(\\d\\d*)?\\s*(\\u006f\\u007a)([ \\ta-zA-Z]*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongWeightPatternOz:Ljava/util/regex/Pattern;

    .line 55
    const-string/jumbo v0, "(\\d+)[.,]?(\\d\\d*)?\\s*(\\u0067)([ \\ta-zA-Z]*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongWeightPatternG:Ljava/util/regex/Pattern;

    .line 56
    const-string/jumbo v0, "(\\d+)[.,]?(\\d\\d*)?\\s*(\\u006c|\\u004c)([ \\ta-zA-Z]*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongWeightPatternL:Ljava/util/regex/Pattern;

    .line 58
    const-string/jumbo v0, "(((\\d*)[_](\\d*))+)[.](\\u0073\\u006e\\u0062)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smMemoName:Ljava/util/regex/Pattern;

    .line 62
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-class v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 73
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->initSpecialWords()V

    .line 74
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->initDomainNameFragments()V

    .line 75
    invoke-direct {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->initSpecialSymbols(Landroid/content/Context;)V

    .line 77
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->initPhoneticWords()V

    .line 78
    invoke-direct {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->initBigNumbers(Landroid/content/Context;)V

    .line 79
    invoke-direct {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->initMonthMatchers(Landroid/content/Context;)V

    .line 80
    return-void
.end method

.method private checkPounds(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 611
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternPound:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 612
    .local v2, "m":Ljava/util/regex/Matcher;
    const-string/jumbo v3, " pound"

    const-string/jumbo v4, " pounds"

    const-string/jumbo v5, " penny"

    const-string/jumbo v6, " pence"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->correctPriceString(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 614
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternPoundPriceLbs:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 615
    const-string/jumbo v3, " pound"

    const-string/jumbo v4, " pounds"

    const-string/jumbo v5, " penny"

    const-string/jumbo v6, " pence"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->correctPriceStringSymbolAfterString(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 617
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternPoundLbs:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 618
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-nez v0, :cond_0

    .line 622
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternPoundLb:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 623
    :goto_1
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-nez v0, :cond_1

    .line 627
    return-object p1

    .line 619
    :cond_0
    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " pounds"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 624
    :cond_1
    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " pound"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method private correctKoreanPriceString(Ljava/lang/String;Ljava/util/regex/Matcher;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "m"    # Ljava/util/regex/Matcher;

    .prologue
    const/4 v7, 0x0

    .line 516
    :goto_0
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-nez v4, :cond_0

    .line 523
    return-object p1

    .line 517
    :cond_0
    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 518
    .local v3, "startIndex":I
    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int v1, v3, v4

    .line 519
    .local v1, "lastIndex":I
    const/4 v4, 0x2

    invoke-virtual {p2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "."

    const-string/jumbo v6, ","

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 520
    .local v0, "amount":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " won"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 521
    .local v2, "replacement":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p1, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p1, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private correctPriceString(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "m"    # Ljava/util/regex/Matcher;
    .param p3, "singular"    # Ljava/lang/String;
    .param p4, "plural"    # Ljava/lang/String;
    .param p5, "singularFractional"    # Ljava/lang/String;
    .param p6, "pluralFractional"    # Ljava/lang/String;

    .prologue
    .line 480
    :goto_0
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-nez v7, :cond_0

    .line 512
    return-object p1

    .line 481
    :cond_0
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 482
    .local v2, "oldPrice":Ljava/lang/String;
    const/4 v7, 0x1

    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 483
    .local v4, "sign":Ljava/lang/String;
    const/4 v7, 0x2

    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 484
    .local v0, "firstPart":Ljava/lang/String;
    const/4 v7, 0x3

    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 485
    .local v1, "lastPart":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    .line 486
    .local v6, "value":Ljava/lang/Float;
    invoke-virtual {v6}, Ljava/lang/Float;->intValue()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 487
    move-object v4, p3

    .line 492
    :goto_1
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 493
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 510
    .local v3, "price":Ljava/lang/String;
    :goto_2
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 490
    .end local v3    # "price":Ljava/lang/String;
    :cond_1
    move-object v4, p4

    goto :goto_1

    .line 495
    :cond_2
    const-string/jumbo v7, "^0*"

    const-string/jumbo v8, ""

    invoke-virtual {v1, v7, v8}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 496
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 497
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 498
    .restart local v3    # "price":Ljava/lang/String;
    goto :goto_2

    .line 499
    .end local v3    # "price":Ljava/lang/String;
    :cond_3
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    .line 500
    invoke-virtual {v6}, Ljava/lang/Float;->intValue()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_4

    .line 501
    move-object v5, p5

    .line 507
    .local v5, "signFractional":Ljava/lang/String;
    :goto_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "price":Ljava/lang/String;
    goto :goto_2

    .line 504
    .end local v3    # "price":Ljava/lang/String;
    .end local v5    # "signFractional":Ljava/lang/String;
    :cond_4
    move-object v5, p6

    .restart local v5    # "signFractional":Ljava/lang/String;
    goto :goto_3
.end method

.method private correctPriceStringSymbolAfterString(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "m"    # Ljava/util/regex/Matcher;
    .param p3, "singular"    # Ljava/lang/String;
    .param p4, "plural"    # Ljava/lang/String;
    .param p5, "singularFractional"    # Ljava/lang/String;
    .param p6, "pluralFractional"    # Ljava/lang/String;

    .prologue
    .line 448
    :goto_0
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-nez v7, :cond_0

    .line 474
    return-object p1

    .line 449
    :cond_0
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 450
    .local v2, "oldPrice":Ljava/lang/String;
    const/4 v7, 0x3

    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 451
    .local v4, "sign":Ljava/lang/String;
    const/4 v7, 0x1

    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 452
    .local v0, "firstPart":Ljava/lang/String;
    const/4 v7, 0x2

    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 453
    .local v1, "lastPart":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    .line 454
    .local v6, "value":Ljava/lang/Float;
    invoke-virtual {v6}, Ljava/lang/Float;->intValue()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 455
    move-object v4, p3

    .line 460
    :goto_1
    if-nez v1, :cond_2

    .line 461
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 472
    .local v3, "price":Ljava/lang/String;
    :goto_2
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 458
    .end local v3    # "price":Ljava/lang/String;
    :cond_1
    move-object v4, p4

    goto :goto_1

    .line 463
    :cond_2
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    .line 464
    invoke-virtual {v6}, Ljava/lang/Float;->intValue()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_3

    .line 465
    move-object v5, p5

    .line 470
    .local v5, "signFractional":Ljava/lang/String;
    :goto_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "price":Ljava/lang/String;
    goto :goto_2

    .line 468
    .end local v3    # "price":Ljava/lang/String;
    .end local v5    # "signFractional":Ljava/lang/String;
    :cond_3
    move-object v5, p6

    .restart local v5    # "signFractional":Ljava/lang/String;
    goto :goto_3
.end method

.method private correctWeightStringSymbol(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "m"    # Ljava/util/regex/Matcher;
    .param p3, "singular"    # Ljava/lang/String;
    .param p4, "plural"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    .line 416
    :cond_0
    :goto_0
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-nez v7, :cond_1

    .line 442
    return-object p1

    .line 417
    :cond_1
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 418
    .local v2, "oldPrice":Ljava/lang/String;
    const/4 v7, 0x3

    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 419
    .local v4, "sign":Ljava/lang/String;
    invoke-virtual {p2, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 420
    .local v0, "firstPart":Ljava/lang/String;
    const/4 v7, 0x2

    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 421
    .local v1, "lastPart":Ljava/lang/String;
    const/4 v7, 0x4

    invoke-virtual {p2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 422
    .local v5, "space":Ljava/lang/String;
    const-string/jumbo v7, " "

    const-string/jumbo v8, ""

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 423
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    .line 424
    .local v6, "value":Ljava/lang/Float;
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_0

    .line 426
    :cond_2
    invoke-virtual {v6}, Ljava/lang/Float;->intValue()I

    move-result v7

    if-ne v7, v9, :cond_3

    .line 427
    move-object v4, p3

    .line 432
    :goto_1
    if-eqz v1, :cond_4

    .line 433
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 437
    .local v3, "price":Ljava/lang/String;
    :goto_2
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 430
    .end local v3    # "price":Ljava/lang/String;
    :cond_3
    move-object v4, p4

    goto :goto_1

    .line 435
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "price":Ljava/lang/String;
    goto :goto_2
.end method

.method private findAndFormatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 293
    iget-object v6, p0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "findAndFormatPhoneNumber"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 295
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 296
    .local v4, "patterns":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/util/regex/Pattern;>;"
    const-string/jumbo v6, "[Mm]essage to (\\d+)"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 297
    const-string/jumbo v6, "[Cc]alling (\\d+)"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 298
    const-string/jumbo v6, "[Dd]o you want to call (\\d+)"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 300
    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 323
    iget-object v6, p0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "findAndFormatPhoneNumber result: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 325
    return-object p1

    .line 300
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/regex/Pattern;

    .line 301
    .local v3, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 302
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 303
    invoke-virtual {v1, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 305
    .local v2, "number":Ljava/lang/String;
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 306
    .local v5, "spannableStringBuilder":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 307
    invoke-virtual {v5, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 308
    invoke-static {v5, v10}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Landroid/text/Editable;I)V

    .line 310
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 311
    .local v0, "formattedNumber":Ljava/lang/String;
    const-string/jumbo v7, "-"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 312
    const-string/jumbo v7, ""

    const-string/jumbo v8, " "

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 316
    :cond_2
    iget-object v7, p0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "find "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", replacing with "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 318
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smInstance:Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smInstance:Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;

    .line 69
    :cond_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smInstance:Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;

    return-object v0
.end method

.method private initBigNumbers(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 154
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/vlingo/midas/samsungutils/R$array;->svox_big_numbers:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 155
    .local v2, "values":[Ljava/lang/String;
    array-length v5, v2

    move v3, v4

    :goto_0
    if-lt v3, v5, :cond_0

    .line 159
    return-void

    .line 155
    :cond_0
    aget-object v1, v2, v3

    .line 156
    .local v1, "value":Ljava/lang/String;
    const-string/jumbo v6, "/"

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "parts":[Ljava/lang/String;
    sget-object v6, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smBigNumbers:Ljava/util/Map;

    aget-object v7, v0, v4

    const/4 v8, 0x1

    aget-object v8, v0, v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private initDomainNameFragments()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method private initDomainNameSpecialWordsFull()V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method private initMonthMatchers(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 162
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/samsungutils/R$array;->monthMatchers:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 163
    .local v2, "monthMatchers":[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->monthes:Ljava/util/Set;

    .line 164
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-lt v0, v4, :cond_0

    .line 170
    return-void

    .line 165
    :cond_0
    aget-object v4, v2, v0

    const-string/jumbo v5, "\\|"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 166
    .local v3, "splitMonthes":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    array-length v4, v3

    if-lt v1, v4, :cond_1

    .line 164
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 167
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->monthes:Ljava/util/Set;

    aget-object v5, v3, v1

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private initPhoneticWords()V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method private initSpecialSymbols(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/vlingo/midas/samsungutils/R$array;->svox_symbols:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "values":[Ljava/lang/String;
    array-length v5, v2

    move v3, v4

    :goto_0
    if-lt v3, v5, :cond_0

    .line 88
    return-void

    .line 84
    :cond_0
    aget-object v1, v2, v3

    .line 85
    .local v1, "value":Ljava/lang/String;
    const-string/jumbo v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "parts":[Ljava/lang/String;
    sget-object v6, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialSymbols:Ljava/util/Map;

    aget-object v7, v0, v4

    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v8, v0, v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private initSpecialWords()V
    .locals 3

    .prologue
    .line 139
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialWords:Ljava/util/Map;

    const-string/jumbo v1, "\u2103"

    const-string/jumbo v2, "\u00b0C"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialWords:Ljava/util/Map;

    const-string/jumbo v1, "\u2109"

    const-string/jumbo v2, "\u00b0F"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialWords:Ljava/util/Map;

    const-string/jumbo v1, "\\b1\\s*\u00b0[^C]"

    const-string/jumbo v2, "1 degree"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->initDomainNameSpecialWordsFull()V

    .line 143
    return-void
.end method

.method private processBillions(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 580
    const-string/jumbo v7, "\\d{10,12}"

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 584
    .local v5, "smBillion":Ljava/util/regex/Pattern;
    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 585
    .local v4, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 587
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    .line 588
    .local v1, "currentText":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v2, v7, -0x9

    .line 589
    .local v2, "first":I
    invoke-virtual {v1, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 590
    .local v0, "billionPart":Ljava/lang/String;
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 591
    .local v3, "lookForZero":Ljava/lang/String;
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_2

    .line 602
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, " billions "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v1, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 605
    .end local v0    # "billionPart":Ljava/lang/String;
    .end local v1    # "currentText":Ljava/lang/String;
    .end local v2    # "first":I
    .end local v3    # "lookForZero":Ljava/lang/String;
    :cond_1
    return-object p1

    .line 593
    .restart local v0    # "billionPart":Ljava/lang/String;
    .restart local v1    # "currentText":Ljava/lang/String;
    .restart local v2    # "first":I
    .restart local v3    # "lookForZero":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 594
    .local v6, "zero":Ljava/lang/String;
    const-string/jumbo v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 596
    invoke-virtual {v3, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private processHeight(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 254
    const-string/jumbo v0, "(\\d+)\'(\\d+)\""

    const-string/jumbo v1, "$1 feet $2 inches"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 255
    const-string/jumbo v0, "(\\d+)\' (\\d+)\""

    const-string/jumbo v1, "$1 feet $2 inches"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 256
    return-object p1
.end method


# virtual methods
.method public prepareText(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 178
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "language":Ljava/lang/String;
    const-string/jumbo v1, "en-US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "en-GB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    :cond_0
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "prepareText-start: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processDates(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 191
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processSpecialSymbols(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 192
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processWeather(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 197
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processSpecialWords(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 199
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processSpecialDomainNames(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 200
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processPhoneNumbers(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 201
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processTime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 202
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processSpecialPrices(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 203
    invoke-direct {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processBillions(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 204
    invoke-direct {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processHeight(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 205
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processPhonetic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 206
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processBigNumbers(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 207
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processMarks(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 208
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 210
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "prepareText-end: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_1
    return-object p1
.end method

.method protected processBigNumbers(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 237
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smBigNumbers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 248
    return-object p1

    .line 237
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 238
    .local v1, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 239
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int v0, v3, v2

    .line 240
    .local v0, "index":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_2

    .line 243
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v6, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "th"

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 244
    goto :goto_0

    .line 241
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 245
    .end local v0    # "index":I
    :cond_3
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0
.end method

.method protected processDateMonth(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 695
    iget-object v8, p0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->monthes:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 697
    .local v7, "monthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 724
    return-object p1

    .line 698
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 699
    .local v6, "month":Ljava/lang/String;
    const-string/jumbo v8, "-"

    invoke-virtual {p1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 701
    iget-object v8, p0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "processDateMonth before: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 703
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 704
    .local v0, "firstMonthIndex":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 706
    .local v4, "lastMonthIndex":I
    const/4 v1, 0x0

    .line 707
    .local v1, "firstSubIndex":I
    move v5, v4

    .line 709
    .local v5, "lastSubIndex":I
    add-int/lit8 v8, v0, -0x7

    if-ltz v8, :cond_2

    .line 710
    add-int/lit8 v1, v0, -0x7

    .line 712
    :cond_2
    add-int/lit8 v8, v4, 0x7

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-gt v8, v9, :cond_3

    .line 713
    add-int/lit8 v5, v4, 0x7

    .line 716
    :cond_3
    invoke-virtual {p1, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 717
    .local v2, "formatSubString":Ljava/lang/String;
    const-string/jumbo v8, "([^\\d\\-]|^)(\\d\\d?)-(\\d\\d?)([^\\d\\-]|$)"

    const-string/jumbo v9, "$1$2 to $3$4"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 718
    .local v3, "formattedString":Ljava/lang/String;
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 721
    iget-object v8, p0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "processDateMonth result: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected processDates(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 230
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->processDateMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 232
    const-string/jumbo v0, "\\b(\\d\\d?)(st|nd|rd|th)\\s?-\\s?(\\d\\d?)(st|nd|rd|th)\\b"

    const-string/jumbo v1, "$1$2 to $3$4"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 233
    const-string/jumbo v1, "(\\d\\d?:\\d\\d)\\s*-\\s*(\\d\\d?:\\d\\d)"

    const-string/jumbo v2, "$1 to $2"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    return-object v0
.end method

.method protected processMarks(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 677
    const-string/jumbo v0, "!+"

    const-string/jumbo v1, "!"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 678
    const-string/jumbo v0, "\\?+"

    const-string/jumbo v1, "?"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 679
    const-string/jumbo v0, "[!\\?]+"

    const-string/jumbo v1, "!?"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 680
    const-string/jumbo v0, "\\.\\.\\.\\.+"

    const-string/jumbo v1, "..."

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 681
    return-object p1
.end method

.method protected processPhoneNumbers(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x2d

    const/16 v5, 0x2c

    .line 274
    invoke-direct {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->findAndFormatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 275
    sget-object v3, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smPhoneNumberPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 276
    .local v0, "m":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-nez v3, :cond_0

    .line 282
    sget-object v3, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smPhoneNumberWithSpacePattern:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 283
    .local v1, "mathcerWithSpace":Ljava/util/regex/Matcher;
    :goto_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-nez v3, :cond_1

    .line 288
    return-object p1

    .line 277
    .end local v1    # "mathcerWithSpace":Ljava/util/regex/Matcher;
    :cond_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 278
    .local v2, "message":Ljava/lang/String;
    invoke-virtual {v2, v6, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    .line 279
    const-string/jumbo v3, "(\\d{1})"

    const-string/jumbo v4, " $1"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 280
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 284
    .end local v2    # "message":Ljava/lang/String;
    .restart local v1    # "mathcerWithSpace":Ljava/util/regex/Matcher;
    :cond_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 285
    .restart local v2    # "message":Ljava/lang/String;
    invoke-virtual {v2, v6, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    .line 286
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method protected processPhonetic(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 407
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smPhoneticWords:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 410
    return-object p1

    .line 407
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 408
    .local v0, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method protected processSpecialDomainNames(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 267
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialDomains:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 270
    return-object p1

    .line 267
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 268
    .local v0, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method protected processSpecialPrices(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    .line 529
    const-string/jumbo v0, "([\\$\u00a3\u20ac\u20a9])\\s*(\\d+)[Kk]"

    const-string/jumbo v1, "$1$2000"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 532
    const-string/jumbo v0, "([\\$\u00a3\u20ac\u20a9])\\s*(\\d+[,.]?\\d*)\\s*([+-])\\s*(\\d+[,.]?\\d*)"

    const-string/jumbo v1, "$1$2 $3 $1$4"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 534
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPrice:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 535
    .local v2, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    const-string/jumbo v0, "[,.]?"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 540
    :cond_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternDollar:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 541
    const-string/jumbo v3, " dollar"

    const-string/jumbo v4, " dollars"

    const-string/jumbo v5, " cent"

    const-string/jumbo v6, " cents"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->correctPriceString(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 543
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongPricePatternEuro:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 544
    const-string/jumbo v3, " euro"

    const-string/jumbo v4, " euros"

    const-string/jumbo v5, " cent"

    const-string/jumbo v6, " cents"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->correctPriceString(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 546
    invoke-direct {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->checkPounds(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 548
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongWeightPatternKg:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 549
    const-string/jumbo v0, " kilogram "

    const-string/jumbo v1, " kilograms "

    invoke-direct {p0, p1, v2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->correctWeightStringSymbol(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 551
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongWeightPatternG:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 552
    const-string/jumbo v0, " gram "

    const-string/jumbo v1, "  grams "

    invoke-direct {p0, p1, v2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->correctWeightStringSymbol(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 555
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongWeightPatternL:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 556
    const-string/jumbo v0, " liter "

    const-string/jumbo v1, " liters "

    invoke-direct {p0, p1, v2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->correctWeightStringSymbol(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 558
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smLongWeightPatternOz:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 559
    const-string/jumbo v0, " ounce "

    const-string/jumbo v1, " ounces "

    invoke-direct {p0, p1, v2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->correctWeightStringSymbol(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 566
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smMemoName:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 567
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-nez v0, :cond_1

    .line 576
    return-object p1

    .line 568
    :cond_1
    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    .line 569
    .local v7, "figures":Ljava/lang/String;
    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 571
    .local v8, "oldFigures":Ljava/lang/String;
    const-string/jumbo v0, "_"

    const-string/jumbo v1, ""

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 572
    const-string/jumbo v0, ""

    const-string/jumbo v1, " "

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 573
    invoke-virtual {p1, v8, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method protected processSpecialSymbols(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 91
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v2, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    const/4 v4, 0x0

    .line 94
    .local v4, "textLength":I
    if-eqz p1, :cond_0

    .line 95
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 97
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v4, :cond_1

    .line 104
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 108
    const-string/jumbo v5, "\\u03BC\\u006C"

    const-string/jumbo v6, "micro liter"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 109
    const-string/jumbo v5, "\\u00B5\\u006C"

    const-string/jumbo v6, "micro liter"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 110
    const-string/jumbo v5, "\\u03BC\\u0067"

    const-string/jumbo v6, "micro gram"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 111
    const-string/jumbo v5, "\\u00B5\\u0067"

    const-string/jumbo v6, "micro gram"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 112
    const-string/jumbo v5, "\\u03BC\\u0041"

    const-string/jumbo v6, "micro amps"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 113
    const-string/jumbo v5, "\\u00B5\\u0041"

    const-string/jumbo v6, "micro amps"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 114
    const-string/jumbo v5, "\\u03BC\\u0046"

    const-string/jumbo v6, "micro farads"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 115
    const-string/jumbo v5, "\\u00B5\\u0046"

    const-string/jumbo v6, "micro farads"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 116
    const-string/jumbo v5, "\\u0040\\u0067\\u006D\\u0061\\u0069\\u006C"

    const-string/jumbo v6, " @g mail"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 119
    .end local v1    # "i":I
    :cond_0
    return-object p1

    .line 98
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 99
    .local v3, "symbol":C
    sget-object v5, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialSymbols:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 100
    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    .end local v3    # "symbol":C
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 105
    .local v0, "character":Ljava/lang/Character;
    invoke-virtual {v0}, Ljava/lang/Character;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v5, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialSymbols:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {p1, v7, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1
.end method

.method protected processSpecialWords(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 260
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smSpecialWords:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 263
    return-object p1

    .line 260
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 261
    .local v0, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method protected processTime(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 354
    const-string/jumbo v1, "\\bhrs\\b"

    const-string/jumbo v2, "hours"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 355
    const-string/jumbo v1, "(\\d+)(am)"

    const-string/jumbo v2, "$1 A M"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 356
    const-string/jumbo v1, "(\\d+)(pm)"

    const-string/jumbo v2, "$1 P M"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 357
    const-string/jumbo v1, "P M"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "A M"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, " AM"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, " PM"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, " am"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, " pm"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 358
    .local v0, "hasAmPm":Z
    :goto_0
    const-string/jumbo v1, ".* (00:00).*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, ".* (0:00).*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 360
    :cond_0
    const-string/jumbo v1, " (00:00 AM)"

    const-string/jumbo v2, " midnight "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 361
    const-string/jumbo v1, " (0:00 AM)"

    const-string/jumbo v2, " midnight "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 362
    const-string/jumbo v1, " (00:00 A M)"

    const-string/jumbo v2, " midnight "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 363
    const-string/jumbo v1, " (0:00 A M)"

    const-string/jumbo v2, " midnight "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 364
    const-string/jumbo v1, " (00:00)"

    const-string/jumbo v2, " midnight "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 365
    const-string/jumbo v1, " (0:00)"

    const-string/jumbo v2, " midnight "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 403
    :goto_1
    return-object p1

    .line 357
    .end local v0    # "hasAmPm":Z
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 366
    .restart local v0    # "hasAmPm":Z
    :cond_2
    const-string/jumbo v1, ".* (12:00).*AM.*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, ".* (12:00).*A M.*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, ".* (12:00).*am.*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, ".* (12:00).*a m.*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 368
    :cond_3
    const-string/jumbo v1, " (12:00 AM)"

    const-string/jumbo v2, " midnight "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 369
    const-string/jumbo v1, " (12:00 A M)"

    const-string/jumbo v2, " midnight "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 370
    goto :goto_1

    :cond_4
    const-string/jumbo v1, ".* (12:00).*PM.*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, ".* (12:00).*P M.*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, ".* (12:00).*pm.*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, ".* (12:00).*p m.*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 372
    :cond_5
    const-string/jumbo v1, " (12:00 PM)"

    const-string/jumbo v2, " noon "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 373
    const-string/jumbo v1, " (12:00 P M)"

    const-string/jumbo v2, " noon "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 374
    goto :goto_1

    :cond_6
    const-string/jumbo v1, ".* (12:00).*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 376
    const-string/jumbo v1, " (12:00 PM)"

    const-string/jumbo v2, " noon "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 377
    const-string/jumbo v1, " (12:00 P M)"

    const-string/jumbo v2, " noon "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 378
    goto/16 :goto_1

    :cond_7
    const-string/jumbo v1, ".* (12:00).*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 380
    const-string/jumbo v1, " (12:00 PM)"

    const-string/jumbo v2, " noon "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 381
    const-string/jumbo v1, " (12:00 P M)"

    const-string/jumbo v2, " noon "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 382
    goto/16 :goto_1

    :cond_8
    const-string/jumbo v1, ".*(\\d+\\d+):(00).*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    if-nez v0, :cond_9

    .line 386
    const-string/jumbo v1, "(\\d+\\d+):(00)"

    const-string/jumbo v2, "$1 hundred "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 387
    goto/16 :goto_1

    :cond_9
    const-string/jumbo v1, ".*(\\d+\\d+):(00).*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    if-eqz v0, :cond_a

    .line 389
    const-string/jumbo v1, "(\\d+\\d+):(00)"

    const-string/jumbo v2, "$1 "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 390
    goto/16 :goto_1

    :cond_a
    const-string/jumbo v1, ".*(\\d+):(00).*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    if-nez v0, :cond_b

    .line 394
    const-string/jumbo v1, "(\\d+):(00)"

    const-string/jumbo v2, "$1 hundred "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 395
    goto/16 :goto_1

    :cond_b
    const-string/jumbo v1, ".*(\\d+):(00).*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    if-eqz v0, :cond_c

    .line 398
    const-string/jumbo v1, "(\\d+):(00)"

    const-string/jumbo v2, "$1 "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 399
    goto/16 :goto_1

    .line 401
    :cond_c
    const-string/jumbo v1, "(\\d+):(0)(\\d)"

    const-string/jumbo v2, "$1 O $3 "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1
.end method

.method protected processUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 685
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->smUrl:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 686
    .local v0, "m":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-nez v2, :cond_0

    .line 691
    return-object p1

    .line 687
    :cond_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    .line 688
    .local v1, "message":Ljava/lang/String;
    const-string/jumbo v2, "-"

    const-string/jumbo v3, " hyphen "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 689
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method protected processWeather(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 217
    const-string/jumbo v0, "(\\d+)\\s*(\\u00B0[\\u0046)])\\s*-\\s*(\\d+)\\s*(\\u00B0[\\u0046)])\\b"

    const-string/jumbo v1, "$1\u00b0 Fahrenheit to $3\u00b0 Fahrenheit"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 218
    const-string/jumbo v0, "(\\d+)\\s*(\\u00B0[\\u0043)])\\s*-\\s*(\\d+)\\s*(\\u00B0[\\u0043)])\\b"

    const-string/jumbo v1, "$1\u00b0 Celsius to $3\u00b0 Celsius"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 219
    const-string/jumbo v0, "(\\d+)\\s*(\\u0046)\\s*-\\s*(\\d+)\\s*(\\u0046)"

    const-string/jumbo v1, "$1 Fahrenheit to $3 Fahrenheit"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 220
    const-string/jumbo v0, "(\\d+)\\s*(\\u0043)\\s*-\\s*(\\d+)\\s*(\\u0043)"

    const-string/jumbo v1, "$1 Celsius to $3 Celsius"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 221
    const-string/jumbo v0, "(\\d+)\\s*(\\u0046)\\b"

    const-string/jumbo v1, "$1 Fahrenheit"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 222
    const-string/jumbo v0, "(\\d+)\\s*(\\u0043)\\b"

    const-string/jumbo v1, "$1 Celsius"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 223
    const-string/jumbo v0, "(\\d+)\\s*(\\u00B0[\\u0043])\\b"

    const-string/jumbo v1, "$1\u00b0 Celsius"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 224
    const-string/jumbo v0, "(\\d+)\\s*(\\u00B0[\\u0046])\\b"

    const-string/jumbo v1, "$1\u00b0 Fahrenheit"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 225
    return-object p1
.end method
