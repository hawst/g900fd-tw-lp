.class public final Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;
.super Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;
.source "SNote2Util.java"

# interfaces
.implements Lcom/vlingo/core/internal/memo/IMemoUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;
    }
.end annotation


# static fields
.field private static final KEY_CONTENT:Ljava/lang/String; = "content"

.field private static final KEY_DATE:Ljava/lang/String; = "ModifiedTime"

.field private static final KEY_HAS_VOICERECORDER:Ljava/lang/String; = "HasVoiceRecord"

.field private static final KEY_ID:Ljava/lang/String; = "_id"

.field private static final KEY_NAME:Ljava/lang/String; = "name"

.field private static final KEY_TAG_CONTENT:Ljava/lang/String; = "Tag_Content"

.field private static final KEY_TEMPLATE_TIME:Ljava/lang/String; = "TemplateType"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SORT_ORDER:Ljava/lang/String; = "ModifiedTime DESC"

.field private static instance:Lcom/vlingo/core/internal/memo/IMemoUtil; = null

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;

.field private static loggedMemoSchema:Z = false

.field private static final strAudio:Ljava/lang/String; = "voice"

.field private static final strContent:Ljava/lang/String; = "content"

.field private static final strId:Ljava/lang/String; = "id"

.field private static final strTitle:Ljava/lang/String; = "title"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 36
    const-class v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 51
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    .line 52
    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    .line 53
    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 54
    const-string/jumbo v2, "ModifiedTime"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 55
    const-string/jumbo v2, "content"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 56
    const-string/jumbo v2, "Tag_Content"

    aput-object v2, v0, v1

    .line 51
    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->PROJECTION:[Ljava/lang/String;

    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    .line 65
    sput-boolean v3, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->loggedMemoSchema:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;-><init>()V

    .line 62
    return-void
.end method

.method static synthetic access$0()Z
    .locals 1

    .prologue
    .line 65
    sget-boolean v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->loggedMemoSchema:Z

    return v0
.end method

.method static synthetic access$1()Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-object v0
.end method

.method static synthetic access$2(Z)V
    .locals 0

    .prologue
    .line 65
    sput-boolean p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->loggedMemoSchema:Z

    return-void
.end method

.method private getFilePathFromId(Landroid/content/Context;J)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J

    .prologue
    .line 176
    const/4 v7, 0x0

    .line 177
    .local v7, "path":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 178
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-direct {p0, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v1

    .line 179
    .local v1, "itemUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 181
    .local v6, "cur":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 182
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 183
    const-string/jumbo v2, "path"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 186
    :cond_0
    if-eqz v6, :cond_1

    .line 187
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 190
    :cond_1
    return-object v7

    .line 185
    :catchall_0
    move-exception v2

    .line 186
    if-eqz v6, :cond_2

    .line 187
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 189
    :cond_2
    throw v2
.end method

.method public static getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    if-nez v0, :cond_0

    .line 129
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;

    invoke-direct {v0}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    .line 131
    :cond_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    return-object v0
.end method

.method private getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 4
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "indices"    # Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;

    .prologue
    .line 72
    new-instance v0, Lcom/vlingo/core/internal/memo/Memo;

    invoke-direct {v0}, Lcom/vlingo/core/internal/memo/Memo;-><init>()V

    .line 73
    .local v0, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;->KEY_NAME_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setTitle(Ljava/lang/String;)V

    .line 74
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;->KEY_CONTENT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setContent(Ljava/lang/String;)V

    .line 75
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;->KEY_CONTENT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setText(Ljava/lang/String;)V

    .line 77
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "SMemo2Util.getMemo setting memo text to \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/vlingo/core/internal/memo/Memo;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\' from persistence"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 78
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;->KEY_ID_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setId(I)V

    .line 79
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;->KEY_DATE_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setDate(Ljava/lang/String;)V

    .line 81
    return-object v0
.end method

.method public static getPackageVersionCode(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 121
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 122
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v1, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    .end local v0    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v1

    .line 123
    :catch_0
    move-exception v1

    .line 125
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private getUriFromId(J)Landroid/net/Uri;
    .locals 4
    .param p1, "id"    # J

    .prologue
    .line 194
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getUpdateContentProviderUri()Landroid/net/Uri;

    move-result-object v0

    .line 196
    .local v0, "contentUri":Landroid/net/Uri;
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "using "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 198
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static isAppInstalled(Ljava/lang/String;I)Z
    .locals 12
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "minVersionCode"    # I

    .prologue
    const/4 v8, 0x0

    .line 99
    sget-object v9, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v10, "isAppInstalled"

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 100
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 101
    .local v2, "ctxt":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 102
    .local v6, "pm":Landroid/content/pm/PackageManager;
    new-instance v5, Landroid/content/Intent;

    const-string/jumbo v9, "android.intent.action.MAIN"

    invoke-direct {v5, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 103
    .local v5, "intent":Landroid/content/Intent;
    const-string/jumbo v9, "android.intent.category.LAUNCHER"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    invoke-virtual {v6, v5, v8}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 105
    .local v1, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 107
    .local v0, "actList":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/pm/ResolveInfo;>;"
    sget-object v9, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Looking for packageName: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " minVersionCode: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 108
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 116
    :goto_0
    return v8

    .line 109
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    .line 110
    .local v7, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    .line 111
    .local v3, "flag1":Z
    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 112
    .local v4, "flag2":Z
    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    if-eqz v9, :cond_0

    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 113
    const/4 v8, 0x1

    goto :goto_0
.end method

.method public static isInstalled()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 90
    const-string/jumbo v2, "com.samsung.android.snote"

    invoke-static {v2, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v0

    .line 91
    .local v0, "snote2_installed":Z
    if-eqz v0, :cond_0

    .line 94
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public deleteMemo(Landroid/content/Context;J)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 209
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getIntentNameDelete()Ljava/lang/String;

    move-result-object v0

    .line 211
    .local v0, "action":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "deleteMemo() using "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 212
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 213
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "id"

    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 215
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->isBroadcast()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 216
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 220
    :goto_0
    return-void

    .line 218
    :cond_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public getCreateMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    const-string/jumbo v0, "com.sec.android.memo.CREATE_TMEMO"

    return-object v0
.end method

.method protected getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 85
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;-><init>(Landroid/database/Cursor;)V

    .line 86
    .local v0, "indices":Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method public getMemos(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    const/4 v6, 0x0

    .line 233
    .local v6, "cur":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 236
    .local v8, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getContentProviderUri()Landroid/net/Uri;

    move-result-object v1

    .line 237
    .local v1, "contentUri":Landroid/net/Uri;
    const/4 v3, 0x0

    .line 238
    .local v3, "SELECTION":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 239
    const-string/jumbo v3, "IsFolder=\'0\' and Islocked=\'0\' and deleted=\'0\'"

    .line 248
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 249
    if-nez v6, :cond_3

    .line 251
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Cannot resolve provider for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    :goto_1
    if-eqz v6, :cond_0

    .line 266
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 269
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v3    # "SELECTION":Ljava/lang/String;
    :cond_0
    :goto_2
    return-object v8

    .line 241
    .restart local v1    # "contentUri":Landroid/net/Uri;
    .restart local v3    # "SELECTION":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 242
    const-string/jumbo v3, "IsFolder=\'0\' and Islocked=\'0\' and deleted=\'0\'"

    .line 243
    goto :goto_0

    .line 244
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " and "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "IsFolder=\'0\' and Islocked=\'0\' and deleted=\'0\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 252
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_4

    .line 254
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v2, "No enabled memos"

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 261
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v3    # "SELECTION":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 263
    .local v7, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_2
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Exception when getting memos: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 265
    if-eqz v6, :cond_0

    .line 266
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 256
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v1    # "contentUri":Landroid/net/Uri;
    .restart local v3    # "SELECTION":Ljava/lang/String;
    :cond_4
    :try_start_3
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 258
    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .local v9, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :cond_5
    :try_start_4
    invoke-virtual {p0, v6}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    if-nez v0, :cond_5

    move-object v8, v9

    .line 261
    .end local v9    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_1

    .line 264
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v3    # "SELECTION":Ljava/lang/String;
    :catchall_0
    move-exception v0

    .line 265
    :goto_4
    if-eqz v6, :cond_6

    .line 266
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 268
    :cond_6
    throw v0

    .line 264
    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v1    # "contentUri":Landroid/net/Uri;
    .restart local v3    # "SELECTION":Ljava/lang/String;
    .restart local v9    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :catchall_1
    move-exception v0

    move-object v8, v9

    .end local v9    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_4

    .line 261
    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v9    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :catch_1
    move-exception v7

    move-object v8, v9

    .end local v9    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_3
.end method

.method public getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 225
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    .line 226
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "ModifiedTime DESC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public getViewMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    const-string/jumbo v0, "com.sec.android.memo.OPEN_ID"

    return-object v0
.end method

.method public saveMemoData(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 146
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getIntentNameCreate()Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "action":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "saveMemoData() using "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 149
    const-string/jumbo v2, "SNote2Util"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "saveMemoData() using "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 151
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "title"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string/jumbo v2, "content"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->isBroadcast()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 155
    const-string/jumbo v2, "SNote2Util"

    const-string/jumbo v3, "sendBroadcast(intentForMemo)"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 161
    :goto_0
    return-void

    .line 158
    :cond_0
    const-string/jumbo v2, "SNote2Util"

    const-string/jumbo v3, "startActivity(intentForMemo)"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public searchMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    .line 204
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "ModifiedTime DESC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->searchMemos(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public updateMemo(Landroid/content/Context;Lcom/vlingo/core/internal/memo/Memo;Lcom/vlingo/core/internal/memo/Memo;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "originalMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .param p3, "changedMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 165
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 166
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "name"

    invoke-virtual {p3}, Lcom/vlingo/core/internal/memo/Memo;->getContent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-virtual {p2}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v3

    int-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v1

    .line 169
    .local v1, "updateUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 170
    .local v0, "rowsUpdated":I
    const/4 v3, 0x1

    if-eq v0, v3, :cond_0

    .line 171
    new-instance v3, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v4, "Error in updating memo."

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 173
    :cond_0
    return-void
.end method
