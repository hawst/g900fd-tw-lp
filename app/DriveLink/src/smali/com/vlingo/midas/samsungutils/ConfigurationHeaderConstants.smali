.class public Lcom/vlingo/midas/samsungutils/ConfigurationHeaderConstants;
.super Ljava/lang/Object;
.source "ConfigurationHeaderConstants.java"


# static fields
.field public static final APP_TYPE_ACCESSORY:Ljava/lang/String; = "forAccessory"

.field public static final APP_TYPE_FULL:Ljava/lang/String; = "full"

.field public static final X_VLCONF_APPLICATION_TYPE:Ljava/lang/String; = "applicationType"

.field public static final X_VLCONF_APP_DRIVING_MODE:Ljava/lang/String; = "isInDrivingMode"

.field public static final X_VLCONF_EMERGENCY_MODE_SUPPORTED:Ljava/lang/String; = "emergencyModeSupported"

.field public static final X_VLCONF_EYES_FREE:Ljava/lang/String; = "isEyesFree"

.field public static final X_VLCONF_HAS_APP_CAR_MODE:Ljava/lang/String; = "hasCarModeApp"

.field public static final X_VLCONF_OFFER_APP_CAR_MODE:Ljava/lang/String; = "offerAppCarMode"

.field public static final X_VLCONF_OFFER_PHONE_CAR_MODE:Ljava/lang/String; = "offerPhoneCarMode"

.field public static final X_VLCONF_PHONE_DRIVING_MODE:Ljava/lang/String; = "isPhoneInDrivingMode"

.field public static final X_VLCONF_SALES_CODE:Ljava/lang/String; = "salesCode"

.field public static final X_VLCONF_VIDEO_CALL_SUPPORTED:Ljava/lang/String; = "videoCallSupported"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
