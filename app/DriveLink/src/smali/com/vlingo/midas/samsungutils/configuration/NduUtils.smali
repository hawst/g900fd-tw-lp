.class public Lcom/vlingo/midas/samsungutils/configuration/NduUtils;
.super Ljava/lang/Object;
.source "NduUtils.java"


# static fields
.field private static final HOST_SETTINGS_UTILS:[Ljava/lang/String;

.field private static final SHARED_PREFS_NAME:Ljava/lang/String; = "lmttutility"

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 13
    const-class v0, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 15
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "com.nuance.nuancedebugutil"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "com.vlingo.midas.lmttutility"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->HOST_SETTINGS_UTILS:[Ljava/lang/String;

    .line 16
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getNduContext()Landroid/content/Context;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 38
    .local v0, "context":Landroid/content/Context;
    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->HOST_SETTINGS_UTILS:[Ljava/lang/String;

    array-length v5, v4

    :goto_0
    if-lt v3, v5, :cond_0

    .line 46
    const/4 v3, 0x0

    :goto_1
    return-object v3

    .line 38
    :cond_0
    aget-object v2, v4, v3

    .line 40
    .local v2, "packageName":Ljava/lang/String;
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v0, v2, v6}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_1

    .line 41
    :catch_0
    move-exception v1

    .line 43
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v6, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Couldn\'t create "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "package context for host settings utility"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 38
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static getNduHost(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "hostKey"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 19
    move-object v0, p1

    .line 21
    .local v0, "host":Ljava/lang/String;
    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "getNduHost() hostKey="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", defaultValue="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 22
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->getNduContext()Landroid/content/Context;

    move-result-object v1

    .line 23
    .local v1, "nduContext":Landroid/content/Context;
    if-eqz v1, :cond_0

    .line 24
    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "  found NDU app: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 25
    const-string/jumbo v3, "lmttutility"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, p0, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 26
    .local v2, "nduHost":Ljava/lang/String;
    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "  nduHost="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 27
    invoke-static {v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 28
    move-object v0, v2

    .line 31
    .end local v2    # "nduHost":Ljava/lang/String;
    :cond_0
    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "  returning: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 33
    return-object v0
.end method
