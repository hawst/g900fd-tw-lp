.class final enum Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;
.super Ljava/lang/Enum;
.source "SettingsInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FOLLOW_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum APPLICATION:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

.field private static final synthetic ENUM$VALUES:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

.field public static final enum SERVICE:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    const-string/jumbo v1, "APPLICATION"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->APPLICATION:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    .line 30
    new-instance v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    const-string/jumbo v1, "SERVICE"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->SERVICE:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->APPLICATION:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->SERVICE:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->ENUM$VALUES:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->ENUM$VALUES:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
