.class public final Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;
.super Lcom/vlingo/core/internal/memo/MemoUtil;
.source "SMemoUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/memo/IMemoUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;
    }
.end annotation


# static fields
.field private static final KEY_CONTENT:Ljava/lang/String; = "Content"

.field private static final KEY_DATE:Ljava/lang/String; = "Date"

.field private static final KEY_ID:Ljava/lang/String; = "_id"

.field private static final KEY_TEXT:Ljava/lang/String; = "Text"

.field private static final KEY_TITLE:Ljava/lang/String; = "Title"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SORT_ORDER:Ljava/lang/String; = "Date DESC"

.field private static instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;

.field private static loggedMemoSchema:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 31
    const-class v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 47
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    .line 48
    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    .line 49
    const-string/jumbo v2, "Title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 50
    const-string/jumbo v2, "Text"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 51
    const-string/jumbo v2, "Content"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 52
    const-string/jumbo v2, "Date"

    aput-object v2, v0, v1

    .line 47
    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->PROJECTION:[Ljava/lang/String;

    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    .line 61
    sput-boolean v3, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->loggedMemoSchema:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/vlingo/core/internal/memo/MemoUtil;-><init>()V

    .line 58
    return-void
.end method

.method static synthetic access$0()Z
    .locals 1

    .prologue
    .line 61
    sget-boolean v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->loggedMemoSchema:Z

    return v0
.end method

.method static synthetic access$1()Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-object v0
.end method

.method static synthetic access$2(Z)V
    .locals 0

    .prologue
    .line 61
    sput-boolean p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->loggedMemoSchema:Z

    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    if-nez v0, :cond_0

    .line 91
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;

    invoke-direct {v0}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    .line 93
    :cond_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    return-object v0
.end method

.method private getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 4
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "indices"    # Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;

    .prologue
    .line 68
    new-instance v0, Lcom/vlingo/core/internal/memo/Memo;

    invoke-direct {v0}, Lcom/vlingo/core/internal/memo/Memo;-><init>()V

    .line 69
    .local v0, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;->KEY_TITLE_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setTitle(Ljava/lang/String;)V

    .line 70
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;->KEY_CONTENT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setContent(Ljava/lang/String;)V

    .line 71
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;->KEY_TEXT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setText(Ljava/lang/String;)V

    .line 73
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "SMemoUtil.getMemo setting memo text to \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/vlingo/core/internal/memo/Memo;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\' from persistence"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 74
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;->KEY_ID_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setId(I)V

    .line 75
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;->KEY_DATE_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setDate(Ljava/lang/String;)V

    .line 77
    return-object v0
.end method

.method private getUriFromId(J)Landroid/net/Uri;
    .locals 4
    .param p1, "id"    # J

    .prologue
    .line 120
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getUpdateContentProviderUri()Landroid/net/Uri;

    move-result-object v0

    .line 122
    .local v0, "contentUri":Landroid/net/Uri;
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "using "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 124
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static isInstalled()Z
    .locals 2

    .prologue
    .line 86
    const-string/jumbo v0, "com.sec.android.widgetapp.diotek.smemo"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addAdditionalExtra(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 229
    return-void
.end method

.method public deleteMemo(Landroid/content/Context;J)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 174
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 175
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    invoke-direct {p0, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 176
    invoke-direct {p0, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 177
    .local v1, "rowsDeleted":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    .line 178
    new-instance v3, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v4, "Error in deleting a memo."

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lcom/vlingo/core/internal/memo/MemoUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "rowsDeleted":I
    :catch_0
    move-exception v2

    .line 183
    .local v2, "sux":Lcom/vlingo/core/internal/memo/MemoUtilException;
    const-string/jumbo v3, "MemoUtilException:"

    invoke-virtual {v2}, Lcom/vlingo/core/internal/memo/MemoUtilException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v2}, Lcom/vlingo/core/internal/memo/MemoUtilException;->printStackTrace()V

    .line 185
    throw v2

    .line 188
    .end local v2    # "sux":Lcom/vlingo/core/internal/memo/MemoUtilException;
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    :cond_0
    return-void
.end method

.method public getCreateMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    const-string/jumbo v0, "android.intent.action.VOICETALK_NEW_SMEMO"

    return-object v0
.end method

.method protected getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 81
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;-><init>(Landroid/database/Cursor;)V

    .line 82
    .local v0, "indices":Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method public getMemos(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    const/4 v6, 0x0

    .line 136
    .local v6, "cur":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 139
    .local v7, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    const/4 v3, 0x0

    .line 140
    .local v3, "SELECTION":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 141
    :try_start_0
    const-string/jumbo v3, " IsLock != 1 and deleted=\'0\'"

    .line 149
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getContentProviderUri()Landroid/net/Uri;

    move-result-object v1

    .line 150
    .local v1, "contentUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 151
    if-nez v6, :cond_3

    .line 153
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Cannot resolve provider for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    :goto_1
    if-eqz v6, :cond_0

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 168
    :cond_0
    return-object v7

    .line 143
    .end local v1    # "contentUri":Landroid/net/Uri;
    :cond_1
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 144
    const-string/jumbo v3, " IsLock != 1 and deleted=\'0\'"

    .line 145
    goto :goto_0

    .line 146
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ") and "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "IsLock != 1 and deleted=\'0\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 154
    .restart local v1    # "contentUri":Landroid/net/Uri;
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_5

    .line 156
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v2, "No enabled memos"

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 163
    .end local v1    # "contentUri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    .line 164
    :goto_2
    if-eqz v6, :cond_4

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 167
    :cond_4
    throw v0

    .line 158
    .restart local v1    # "contentUri":Landroid/net/Uri;
    :cond_5
    :try_start_2
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 160
    .end local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .local v8, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :cond_6
    :try_start_3
    invoke-virtual {p0, v6}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v0

    if-nez v0, :cond_6

    move-object v7, v8

    .line 163
    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_1

    .end local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :catchall_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_2
.end method

.method public getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 193
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "Title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "Text"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "Content"

    aput-object v2, v0, v1

    .line 194
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "Date DESC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public getViewMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    const-string/jumbo v0, "android.intent.action.VOICETALK_VIEW_SMEMO"

    return-object v0
.end method

.method public searchMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "Title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "Text"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "Content"

    aput-object v2, v0, v1

    .line 130
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "Date DESC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->searchMemos(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public updateMemo(Landroid/content/Context;Lcom/vlingo/core/internal/memo/Memo;Lcom/vlingo/core/internal/memo/Memo;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "originalMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .param p3, "changedMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 108
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 109
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "Title"

    invoke-virtual {p3}, Lcom/vlingo/core/internal/memo/Memo;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string/jumbo v3, "Content"

    invoke-virtual {p3}, Lcom/vlingo/core/internal/memo/Memo;->getContent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p2}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v3

    int-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v1

    .line 113
    .local v1, "updateUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 114
    .local v0, "rowsUpdated":I
    const/4 v3, 0x1

    if-eq v0, v3, :cond_0

    .line 115
    new-instance v3, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v4, "Error in updating memo."

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 117
    :cond_0
    return-void
.end method
