.class public Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;
.super Ljava/lang/Object;
.source "SearchMusic.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$vlingo$midas$samsungutils$utils$music$PlayMusicType:[I = null

.field private static final FAVORITE:Ljava/lang/String; = "Favorite"

.field private static final GB_FAVORITE:Ljava/lang/String; = "Favourite"

.field private static MAX_COUNT:I = 0x0

.field private static final QUICK_LIST_DEFAULT_VALUE:Ljava/lang/String; = "Quick list"

.field private static final QUICK_LIST_MUSIC_PLUS_VALUE:Ljava/lang/String; = "FavoriteList#328795!432@1341"

.field private static isDefaultFavorite:Z

.field private static log:Lcom/vlingo/core/internal/logging/Logger;


# direct methods
.method static synthetic $SWITCH_TABLE$com$vlingo$midas$samsungutils$utils$music$PlayMusicType()[I
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->$SWITCH_TABLE$com$vlingo$midas$samsungutils$utils$music$PlayMusicType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->values()[Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ALBUM:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_1
    :try_start_1
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ARTIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_2
    :try_start_2
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->MOOD:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_3
    :try_start_3
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->NEXT:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_4
    :try_start_4
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PAUSE:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_5
    :try_start_5
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAY:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_6
    :try_start_6
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAYLIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_7
    :try_start_7
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PREVIOUS:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_8
    :try_start_8
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->SONGLIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_9
    :try_start_9
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->TITLE:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_a
    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->$SWITCH_TABLE$com$vlingo$midas$samsungutils$utils$music$PlayMusicType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_a

    :catch_1
    move-exception v1

    goto :goto_9

    :catch_2
    move-exception v1

    goto :goto_8

    :catch_3
    move-exception v1

    goto :goto_7

    :catch_4
    move-exception v1

    goto :goto_6

    :catch_5
    move-exception v1

    goto :goto_5

    :catch_6
    move-exception v1

    goto :goto_4

    :catch_7
    move-exception v1

    goto :goto_3

    :catch_8
    move-exception v1

    goto :goto_2

    :catch_9
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 39
    const/4 v0, 0x5

    sput v0, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->MAX_COUNT:I

    .line 40
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->isDefaultFavorite:Z

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static augmentQuery(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p0, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1001
    .local p1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1002
    .local v1, "words":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1003
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1011
    :goto_1
    return-object v1

    .line 1003
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1004
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/util/StringUtils;->containsString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1005
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1009
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static augmentWords([Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p0, "words"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1015
    .local p1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1016
    .local v0, "augmentedWords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1017
    array-length v3, p0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 1022
    .end local v0    # "augmentedWords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1
    return-object v0

    .line 1017
    .restart local v0    # "augmentedWords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    aget-object v1, p0, v2

    .line 1018
    .local v1, "word":Ljava/lang/String;
    invoke-static {v1, p1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->augmentQuery(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1017
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1022
    .end local v1    # "word":Ljava/lang/String;
    :cond_1
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method

.method private static buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;
    .locals 7
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .param p3, "searchType"    # Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    .prologue
    .line 835
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->stringIsNonAsciiWithCase(Ljava/lang/String;)Z

    move-result v1

    .line 836
    .local v1, "isNonAsciiWithCase":Z
    const/4 v2, 0x0

    .line 838
    .local v2, "nameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 839
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->$SWITCH_TABLE$com$vlingo$midas$samsungutils$utils$music$PlayMusicType()[I

    move-result-object v5

    invoke-virtual {p3}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 857
    :pswitch_0
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "nameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 858
    .restart local v2    # "nameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 864
    :cond_0
    :goto_0
    if-eqz p2, :cond_3

    .line 865
    if-eqz v1, :cond_2

    .line 866
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v5, v2}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->augmentWords([Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 867
    .local v4, "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 868
    invoke-static {p0, v4}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    .line 885
    .end local v4    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    return-object v5

    .line 841
    :pswitch_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getAlbumNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 842
    goto :goto_0

    .line 845
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getArtistNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 846
    goto :goto_0

    .line 849
    :pswitch_3
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getPlaylistNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 850
    goto :goto_0

    .line 853
    :pswitch_4
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSongNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 854
    goto :goto_0

    .line 870
    .restart local v4    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 873
    .end local v4    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 876
    :cond_3
    const-string/jumbo v5, " "

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 877
    .local v3, "parts":[Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 878
    invoke-static {v3, v2}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->augmentWords([Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 879
    .local v0, "augmentedParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 880
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 882
    :cond_4
    invoke-static {p0, v3}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 885
    .end local v0    # "augmentedParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    invoke-static {p0, v3}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 839
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static byAlbum(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumName"    # Ljava/lang/String;
    .param p2, "max"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, p2}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getAlbumList(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/util/List;

    move-result-object v0

    .line 243
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "album"

    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ALBUM:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-static {v0, p0, p1, v1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static byArtist(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistName"    # Ljava/lang/String;
    .param p2, "max"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, p2}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getArtistList(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/util/List;

    move-result-object v0

    .line 228
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "artist"

    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ARTIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-static {v0, p0, p1, v1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static byEverything(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "isLastAttempt"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    const/4 v11, 0x0

    .line 151
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v15, 0x0

    .line 152
    .local v15, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const/4 v2, 0x5

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v6, "album_id"

    aput-object v6, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v6, "artist"

    aput-object v6, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v6, "title"

    aput-object v6, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v6, "album"

    aput-object v6, v4, v2

    .line 153
    .local v4, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "artist"

    const/4 v7, 0x1

    sget-object v20, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ARTIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v6, v0, v7, v1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, " OR "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "title"

    const/4 v7, 0x1

    sget-object v20, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->TITLE:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v6, v0, v7, v1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, " OR "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 154
    const-string/jumbo v6, "album"

    const/4 v7, 0x1

    sget-object v20, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ALBUM:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v6, v0, v7, v1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 153
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 155
    .local v5, "where":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v13

    .line 156
    .local v13, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v17, 0x0

    .line 157
    .local v17, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v13, :cond_0

    .line 159
    :try_start_0
    invoke-virtual {v13}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v17, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 169
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 170
    .local v3, "musicUri":Landroid/net/Uri;
    if-eqz v17, :cond_6

    .line 171
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v3

    .line 174
    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const-string/jumbo v7, "title_key"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 176
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .local v16, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    if-eqz v11, :cond_2

    :try_start_2
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 179
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "** cursor.size()---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 180
    const-string/jumbo v9, ""

    .line 181
    .local v9, "albumUri":Ljava/lang/String;
    if-eqz v17, :cond_7

    .line 182
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 186
    :cond_1
    :goto_2
    const-string/jumbo v2, "_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    .line 187
    .local v18, "songId":Ljava/lang/Long;
    const-string/jumbo v2, "album_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 188
    .local v8, "albumId":Ljava/lang/String;
    const-string/jumbo v2, "artist"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 189
    .local v10, "artist":Ljava/lang/String;
    const-string/jumbo v2, "title"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 190
    .local v19, "title":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 191
    .local v14, "imageUri":Landroid/net/Uri;
    new-instance v2, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v2, v0, v1, v10, v14}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Artist---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 194
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Title ---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 195
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "ALBUM_ID ---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 198
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-nez v2, :cond_1

    .line 205
    .end local v8    # "albumId":Ljava/lang/String;
    .end local v9    # "albumUri":Ljava/lang/String;
    .end local v10    # "artist":Ljava/lang/String;
    .end local v14    # "imageUri":Landroid/net/Uri;
    .end local v18    # "songId":Ljava/lang/Long;
    .end local v19    # "title":Ljava/lang/String;
    :cond_2
    if-eqz v11, :cond_3

    .line 206
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object/from16 v15, v16

    .line 210
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_4
    :goto_3
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_5

    .line 214
    :cond_5
    return-object v15

    .line 160
    .end local v3    # "musicUri":Landroid/net/Uri;
    :catch_0
    move-exception v12

    .line 162
    .local v12, "e":Ljava/lang/InstantiationException;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v6, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v12}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 163
    .end local v12    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v12

    .line 165
    .local v12, "e":Ljava/lang/IllegalAccessException;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v6, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v12}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 173
    .end local v12    # "e":Ljava/lang/IllegalAccessException;
    .restart local v3    # "musicUri":Landroid/net/Uri;
    :cond_6
    :try_start_3
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 184
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v9    # "albumUri":Ljava/lang/String;
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_7
    :try_start_4
    const-string/jumbo v9, "content://media/external/audio/albumart/"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_2

    .line 201
    .end local v9    # "albumUri":Ljava/lang/String;
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_2
    move-exception v12

    .line 203
    .local v12, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_5
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v6, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 205
    if-eqz v11, :cond_4

    .line 206
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 204
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 205
    :goto_5
    if-eqz v11, :cond_8

    .line 206
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 208
    :cond_8
    throw v2

    .line 204
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object/from16 v15, v16

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_5

    .line 201
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_3
    move-exception v12

    move-object/from16 v15, v16

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_4
.end method

.method public static byPlaylist(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistName"    # Ljava/lang/String;
    .param p2, "max"    # I
    .param p3, "defaultQuickListName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getPlaylistList(Landroid/content/Context;Ljava/lang/String;ZILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 269
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->searchSongForPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static byTitle(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleName"    # Ljava/lang/String;
    .param p2, "max"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255
    const/4 v0, 0x1

    invoke-static {p0, p1, v0, p2}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getTitleList(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getAlbumList(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/util/List;
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .param p3, "max"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "ZI)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 445
    const/4 v14, 0x0

    .line 446
    .local v14, "cursor":Landroid/database/Cursor;
    const/16 v17, 0x0

    .line 447
    .local v17, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v6, "album"

    aput-object v6, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v6, "artist"

    aput-object v6, v4, v2

    .line 448
    .local v4, "projection":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v5, 0x0

    .line 449
    .local v5, "where":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v6, "ko-KR"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 450
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "("

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ") OR ("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "REPLACE(`album`,\' \',\'\')"

    sget-object v7, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ALBUM:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v6, v0, v1, v7}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ")"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 452
    :cond_0
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v16

    .line 453
    .local v16, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v19, 0x0

    .line 454
    .local v19, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v16, :cond_1

    .line 456
    :try_start_0
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v19, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 464
    :cond_1
    :goto_1
    const/4 v3, 0x0

    .line 465
    .local v3, "albumUri":Landroid/net/Uri;
    if-eqz v19, :cond_6

    .line 466
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUM:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v3

    .line 470
    :goto_2
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "TableName : + "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 472
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const-string/jumbo v7, "album_key"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 474
    if-eqz v14, :cond_3

    .line 476
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "** cursor.size()---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 478
    const-string/jumbo v9, ""

    .line 479
    .local v9, "albumArtUristring":Ljava/lang/String;
    if-eqz v19, :cond_7

    .line 480
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 483
    :goto_3
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    .line 484
    const/4 v13, 0x0

    .line 485
    .local v13, "count":I
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 487
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .local v18, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_2
    :try_start_2
    const-string/jumbo v2, "_id"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    .line 488
    .local v11, "albumId":Ljava/lang/Long;
    const-string/jumbo v2, "album"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 489
    .local v12, "albumName":Ljava/lang/String;
    const-string/jumbo v2, "artist"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 490
    .local v10, "albumArtist":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 492
    .local v8, "albumArtUri":Landroid/net/Uri;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Album ID---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 493
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Album Name ---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 495
    new-instance v2, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    invoke-direct {v2, v11, v12, v10, v8}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 496
    add-int/lit8 v13, v13, 0x1

    .line 497
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-eqz v2, :cond_9

    .line 486
    move/from16 v0, p3

    if-lt v13, v0, :cond_2

    move-object/from16 v17, v18

    .line 503
    .end local v8    # "albumArtUri":Landroid/net/Uri;
    .end local v9    # "albumArtUristring":Ljava/lang/String;
    .end local v10    # "albumArtist":Ljava/lang/String;
    .end local v11    # "albumId":Ljava/lang/Long;
    .end local v12    # "albumName":Ljava/lang/String;
    .end local v13    # "count":I
    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_3
    :goto_4
    if-eqz v14, :cond_4

    .line 504
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 508
    :cond_4
    :goto_5
    return-object v17

    .line 448
    .end local v3    # "albumUri":Landroid/net/Uri;
    .end local v5    # "where":Ljava/lang/String;
    .end local v16    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v19    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :cond_5
    const-string/jumbo v2, "album"

    sget-object v6, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ALBUM:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v2, v0, v1, v6}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 457
    .restart local v5    # "where":Ljava/lang/String;
    .restart local v16    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v19    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_0
    move-exception v15

    .line 458
    .local v15, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v15}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_1

    .line 459
    .end local v15    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v15

    .line 460
    .local v15, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v15}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_1

    .line 468
    .end local v15    # "e":Ljava/lang/IllegalAccessException;
    .restart local v3    # "albumUri":Landroid/net/Uri;
    :cond_6
    :try_start_3
    sget-object v3, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto/16 :goto_2

    .line 482
    .restart local v9    # "albumArtUristring":Ljava/lang/String;
    :cond_7
    const-string/jumbo v9, "content://media/external/audio/albumart/"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 499
    .end local v9    # "albumArtUristring":Ljava/lang/String;
    :catch_2
    move-exception v15

    .line 501
    .local v15, "e":Ljava/lang/Exception;
    :goto_6
    :try_start_4
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v6, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v15}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 503
    if-eqz v14, :cond_4

    .line 504
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 502
    .end local v15    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 503
    :goto_7
    if-eqz v14, :cond_8

    .line 504
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 506
    :cond_8
    throw v2

    .line 502
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v9    # "albumArtUristring":Ljava/lang/String;
    .restart local v13    # "count":I
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object/from16 v17, v18

    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_7

    .line 499
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_3
    move-exception v15

    move-object/from16 v17, v18

    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_6

    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v8    # "albumArtUri":Landroid/net/Uri;
    .restart local v10    # "albumArtist":Ljava/lang/String;
    .restart local v11    # "albumId":Ljava/lang/Long;
    .restart local v12    # "albumName":Ljava/lang/String;
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_9
    move-object/from16 v17, v18

    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_4
.end method

.method public static getArtistList(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/util/List;
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .param p3, "max"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "ZI)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    const/16 v18, 0x0

    .line 357
    .local v18, "cursor":Landroid/database/Cursor;
    const/16 v19, 0x0

    .line 358
    .local v19, "cursorAlbumArt":Landroid/database/Cursor;
    const/16 v23, 0x0

    .line 359
    .local v23, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v6, "artist"

    aput-object v6, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v6, "artist_key"

    aput-object v6, v4, v2

    .line 360
    .local v4, "projection":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v5, 0x0

    .line 361
    .local v5, "where":Ljava/lang/String;
    :goto_0
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v21

    .line 362
    .local v21, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v25, 0x0

    .line 363
    .local v25, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v21, :cond_0

    .line 365
    :try_start_0
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v25, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 372
    :cond_0
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v6, "ko-KR"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 373
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "("

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ") OR ("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "REPLACE(`artist`,\' \',\'\')"

    sget-object v9, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ARTIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v6, v0, v1, v9}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ")"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 376
    :cond_1
    const/4 v3, 0x0

    .line 377
    .local v3, "artistUri":Landroid/net/Uri;
    if-eqz v25, :cond_9

    .line 378
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ARTIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v3

    .line 381
    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const-string/jumbo v7, "artist_key"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 383
    if-eqz v18, :cond_5

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 385
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "** cursor.size()---"

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 387
    const/16 v17, 0x0

    .line 388
    .local v17, "count":I
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .local v24, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :try_start_2
    const-string/jumbo v13, ""

    .line 390
    .local v13, "albumUri":Ljava/lang/String;
    if-eqz v25, :cond_a

    .line 391
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    .line 395
    :cond_2
    :goto_3
    const-string/jumbo v2, "_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 396
    .local v15, "artistId":Ljava/lang/Long;
    const-string/jumbo v2, "artist"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 397
    .local v14, "artist":Ljava/lang/String;
    const-string/jumbo v2, "artist_key"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 400
    .local v16, "artistKey":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Artist ID---"

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 401
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Artist Name ---"

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 404
    const/4 v2, 0x1

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "album_id"

    aput-object v6, v8, v2

    .line 405
    .local v8, "projectionAlbumArt":[Ljava/lang/String;
    const-string/jumbo v2, "artist_key"

    const/4 v6, 0x1

    sget-object v9, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ARTIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, v16

    invoke-static {v2, v0, v6, v9}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    .line 406
    sget-object v22, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 407
    .local v22, "imageUri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 408
    .local v7, "musicUri":Landroid/net/Uri;
    if-eqz v25, :cond_b

    .line 409
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v7

    .line 412
    :goto_4
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 413
    const/4 v10, 0x0

    const-string/jumbo v11, "artist_key"

    move-object v9, v5

    .line 412
    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 414
    if-eqz v19, :cond_4

    .line 415
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 416
    const-string/jumbo v2, "album_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 417
    .local v12, "albumId":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    .line 420
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "ALBUM_ID ---"

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 422
    .end local v12    # "albumId":Ljava/lang/String;
    :cond_3
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 425
    :cond_4
    new-instance v2, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    move-object/from16 v0, v22

    invoke-direct {v2, v15, v14, v14, v0}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    add-int/lit8 v17, v17, 0x1

    .line 427
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-eqz v2, :cond_f

    .line 394
    move/from16 v0, v17

    move/from16 v1, p3

    if-lt v0, v1, :cond_2

    move-object/from16 v23, v24

    .line 433
    .end local v7    # "musicUri":Landroid/net/Uri;
    .end local v8    # "projectionAlbumArt":[Ljava/lang/String;
    .end local v13    # "albumUri":Ljava/lang/String;
    .end local v14    # "artist":Ljava/lang/String;
    .end local v15    # "artistId":Ljava/lang/Long;
    .end local v16    # "artistKey":Ljava/lang/String;
    .end local v17    # "count":I
    .end local v22    # "imageUri":Landroid/net/Uri;
    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_5
    :goto_5
    if-eqz v18, :cond_6

    .line 434
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 436
    :cond_6
    if-eqz v19, :cond_7

    .line 437
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 441
    :cond_7
    :goto_6
    return-object v23

    .line 360
    .end local v3    # "artistUri":Landroid/net/Uri;
    .end local v5    # "where":Ljava/lang/String;
    .end local v21    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v25    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :cond_8
    const-string/jumbo v2, "artist"

    sget-object v6, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ARTIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v2, v0, v1, v6}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 366
    .restart local v5    # "where":Ljava/lang/String;
    .restart local v21    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v25    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_0
    move-exception v20

    .line 367
    .local v20, "e":Ljava/lang/InstantiationException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_1

    .line 368
    .end local v20    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v20

    .line 369
    .local v20, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_1

    .line 380
    .end local v20    # "e":Ljava/lang/IllegalAccessException;
    .restart local v3    # "artistUri":Landroid/net/Uri;
    :cond_9
    :try_start_3
    sget-object v3, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 393
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v13    # "albumUri":Ljava/lang/String;
    .restart local v17    # "count":I
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_a
    :try_start_4
    const-string/jumbo v13, "content://media/external/audio/albumart/"

    goto/16 :goto_3

    .line 411
    .restart local v7    # "musicUri":Landroid/net/Uri;
    .restart local v8    # "projectionAlbumArt":[Ljava/lang/String;
    .restart local v14    # "artist":Ljava/lang/String;
    .restart local v15    # "artistId":Ljava/lang/Long;
    .restart local v16    # "artistKey":Ljava/lang/String;
    .restart local v22    # "imageUri":Landroid/net/Uri;
    :cond_b
    sget-object v7, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_4

    .line 429
    .end local v7    # "musicUri":Landroid/net/Uri;
    .end local v8    # "projectionAlbumArt":[Ljava/lang/String;
    .end local v13    # "albumUri":Ljava/lang/String;
    .end local v14    # "artist":Ljava/lang/String;
    .end local v15    # "artistId":Ljava/lang/Long;
    .end local v16    # "artistKey":Ljava/lang/String;
    .end local v17    # "count":I
    .end local v22    # "imageUri":Landroid/net/Uri;
    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_2
    move-exception v20

    .line 431
    .local v20, "e":Ljava/lang/Exception;
    :goto_7
    :try_start_5
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v6, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v6, v9}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 433
    if-eqz v18, :cond_c

    .line 434
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 436
    :cond_c
    if-eqz v19, :cond_7

    .line 437
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto :goto_6

    .line 432
    .end local v20    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 433
    :goto_8
    if-eqz v18, :cond_d

    .line 434
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 436
    :cond_d
    if-eqz v19, :cond_e

    .line 437
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 439
    :cond_e
    throw v2

    .line 432
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "count":I
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object/from16 v23, v24

    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_8

    .line 429
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_3
    move-exception v20

    move-object/from16 v23, v24

    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_7

    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v7    # "musicUri":Landroid/net/Uri;
    .restart local v8    # "projectionAlbumArt":[Ljava/lang/String;
    .restart local v13    # "albumUri":Ljava/lang/String;
    .restart local v14    # "artist":Ljava/lang/String;
    .restart local v15    # "artistId":Ljava/lang/Long;
    .restart local v16    # "artistKey":Ljava/lang/String;
    .restart local v22    # "imageUri":Landroid/net/Uri;
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_f
    move-object/from16 v23, v24

    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_5
.end method

.method private static getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/util/List;
    .locals 6
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchCondition"    # Ljava/lang/String;
    .param p3, "searchType"    # Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 274
    invoke-static {p0, p1, p2, v5, p3}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    .line 275
    .local v0, "firstResult":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 287
    .end local v0    # "firstResult":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :goto_0
    return-object v0

    .line 279
    .restart local v0    # "firstResult":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_0
    const-string/jumbo v3, " "

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 280
    .local v1, "parts":[Ljava/lang/String;
    array-length v3, v1

    if-le v3, v5, :cond_1

    .line 281
    const-string/jumbo v3, " "

    const-string/jumbo v4, "%"

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 282
    invoke-static {p0, p1, p2, v5, p3}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/util/List;

    move-result-object v2

    .line 283
    .local v2, "secondResult":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    move-object v0, v2

    .line 284
    goto :goto_0

    .line 287
    .end local v2    # "secondResult":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_1
    const/4 v3, 0x0

    invoke-static {p0, p1, p2, v3, p3}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/util/List;
    .locals 22
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchCondition"    # Ljava/lang/String;
    .param p3, "exactSearch"    # Z
    .param p4, "searchType"    # Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291
    const/4 v13, 0x0

    .line 292
    .local v13, "cursor":Landroid/database/Cursor;
    const/16 v17, 0x0

    .line 293
    .local v17, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const/4 v4, 0x4

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v8, "_id"

    aput-object v8, v6, v4

    const/4 v4, 0x1

    const-string/jumbo v8, "album_id"

    aput-object v8, v6, v4

    const/4 v4, 0x2

    const-string/jumbo v8, "artist"

    aput-object v8, v6, v4

    const/4 v4, 0x3

    const-string/jumbo v8, "title"

    aput-object v8, v6, v4

    .line 294
    .local v6, "projection":[Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p4

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v7

    .line 295
    .local v7, "where":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v4}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v15

    .line 296
    .local v15, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v19, 0x0

    .line 297
    .local v19, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v15, :cond_0

    .line 299
    :try_start_0
    invoke-virtual {v15}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v19, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 307
    :cond_0
    :goto_0
    const/4 v5, 0x0

    .line 308
    .local v5, "musicUri":Landroid/net/Uri;
    if-eqz v19, :cond_5

    .line 309
    :try_start_1
    sget-object v4, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v5

    .line 312
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v8, 0x0

    const-string/jumbo v9, "title_key"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 314
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .local v18, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    if-eqz v13, :cond_2

    :try_start_2
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 317
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "** cursor.size()---"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 318
    const-string/jumbo v11, ""

    .line 319
    .local v11, "albumUri":Ljava/lang/String;
    if-eqz v19, :cond_6

    .line 320
    sget-object v4, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 324
    :cond_1
    :goto_2
    const-string/jumbo v4, "_id"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    .line 325
    .local v20, "songId":Ljava/lang/Long;
    const-string/jumbo v4, "album_id"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 326
    .local v10, "albumId":Ljava/lang/String;
    const-string/jumbo v4, "artist"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 327
    .local v12, "artist":Ljava/lang/String;
    const-string/jumbo v4, "title"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 328
    .local v21, "title":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 329
    .local v16, "imageUri":Landroid/net/Uri;
    new-instance v4, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v16

    invoke-direct {v4, v0, v1, v12, v2}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 331
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Artist---"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 332
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Title ---"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 333
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "ALBUM_ID ---"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 336
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v4

    if-nez v4, :cond_1

    .line 343
    .end local v10    # "albumId":Ljava/lang/String;
    .end local v11    # "albumUri":Ljava/lang/String;
    .end local v12    # "artist":Ljava/lang/String;
    .end local v16    # "imageUri":Landroid/net/Uri;
    .end local v20    # "songId":Ljava/lang/Long;
    .end local v21    # "title":Ljava/lang/String;
    :cond_2
    if-eqz v13, :cond_3

    .line 344
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object/from16 v17, v18

    .line 347
    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_4
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    .line 352
    return-object v17

    .line 300
    .end local v5    # "musicUri":Landroid/net/Uri;
    :catch_0
    move-exception v14

    .line 301
    .local v14, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v14}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_0

    .line 302
    .end local v14    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v14

    .line 303
    .local v14, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v14}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_0

    .line 311
    .end local v14    # "e":Ljava/lang/IllegalAccessException;
    .restart local v5    # "musicUri":Landroid/net/Uri;
    :cond_5
    :try_start_3
    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 322
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v11    # "albumUri":Ljava/lang/String;
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_6
    :try_start_4
    const-string/jumbo v11, "content://media/external/audio/albumart/"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_2

    .line 339
    .end local v11    # "albumUri":Ljava/lang/String;
    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_2
    move-exception v14

    .line 341
    .local v14, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_5
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v8, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v8}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v14}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 343
    if-eqz v13, :cond_4

    .line 344
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 342
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 343
    :goto_5
    if-eqz v13, :cond_7

    .line 344
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 346
    :cond_7
    throw v4

    .line 342
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catchall_1
    move-exception v4

    move-object/from16 v17, v18

    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_5

    .line 339
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_3
    move-exception v14

    move-object/from16 v17, v18

    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_4
.end method

.method private static getFavoriteMusicList(Landroid/content/Context;Ljava/lang/Long;)Ljava/util/List;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "favoriteId"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1047
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1048
    .local v15, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const/4 v2, 0x5

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v5, "_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    .line 1049
    const-string/jumbo v5, "artist"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    .line 1050
    const-string/jumbo v5, "title"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    .line 1051
    const-string/jumbo v5, "audio_id"

    aput-object v5, v4, v2

    const/4 v2, 0x4

    .line 1052
    const-string/jumbo v5, "album_id"

    aput-object v5, v4, v2

    .line 1054
    .local v4, "projection":[Ljava/lang/String;
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v13

    .line 1055
    .local v13, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v16, 0x0

    .line 1056
    .local v16, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v13, :cond_0

    .line 1058
    :try_start_0
    invoke-virtual {v13}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v16, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1065
    :cond_0
    :goto_0
    const/4 v11, 0x0

    .line 1067
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v16, :cond_2

    .line 1068
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5, v6}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicPlaylistUri(J)Landroid/net/Uri;

    move-result-object v3

    .line 1069
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v5, "is_music != 0 "

    const/4 v6, 0x0

    const-string/jumbo v7, "play_order"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1070
    if-eqz v11, :cond_2

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1071
    const-string/jumbo v9, ""

    .line 1072
    .local v9, "albumUri":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1074
    :cond_1
    const-string/jumbo v2, "audio_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 1075
    .local v17, "songId":Ljava/lang/Long;
    const-string/jumbo v2, "album_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1076
    .local v8, "albumId":Ljava/lang/String;
    const-string/jumbo v2, "artist"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1077
    .local v10, "artist":Ljava/lang/String;
    const-string/jumbo v2, "title"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 1078
    .local v18, "title":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 1079
    .local v14, "imageUri":Landroid/net/Uri;
    new-instance v2, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v2, v0, v1, v10, v14}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1081
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Artist---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 1082
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Title ---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 1083
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "ALBUM_ID ---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 1085
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 1092
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v8    # "albumId":Ljava/lang/String;
    .end local v9    # "albumUri":Ljava/lang/String;
    .end local v10    # "artist":Ljava/lang/String;
    .end local v14    # "imageUri":Landroid/net/Uri;
    .end local v17    # "songId":Ljava/lang/Long;
    .end local v18    # "title":Ljava/lang/String;
    :cond_2
    if-eqz v11, :cond_3

    .line 1093
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1096
    :cond_3
    :goto_1
    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1097
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->setIsFavorite()V

    .line 1098
    :cond_4
    return-object v15

    .line 1059
    .end local v11    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v12

    .line 1060
    .local v12, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v12}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_0

    .line 1061
    .end local v12    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v12

    .line 1062
    .local v12, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v12}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_0

    .line 1088
    .end local v12    # "e":Ljava/lang/IllegalAccessException;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    :catch_2
    move-exception v12

    .line 1090
    .local v12, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v5, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1092
    if-eqz v11, :cond_3

    .line 1093
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1091
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 1092
    if-eqz v11, :cond_5

    .line 1093
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1095
    :cond_5
    throw v2
.end method

.method public static getFavoritePlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 27
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 665
    const/16 v16, 0x0

    .line 666
    .local v16, "cursor":Landroid/database/Cursor;
    const/16 v22, 0x0

    .line 667
    .local v22, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const/16 v21, 0x0

    .line 669
    .local v21, "musicAppVersion":I
    const/4 v3, 0x4

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v5, "_id"

    aput-object v5, v7, v3

    const/4 v3, 0x1

    .line 670
    const-string/jumbo v5, "album_id"

    aput-object v5, v7, v3

    const/4 v3, 0x2

    .line 671
    const-string/jumbo v5, "artist"

    aput-object v5, v7, v3

    const/4 v3, 0x3

    .line 672
    const-string/jumbo v5, "title"

    aput-object v5, v7, v3

    .line 673
    .local v7, "projection":[Ljava/lang/String;
    const-string/jumbo v11, ""

    .line 675
    .local v11, "where":Ljava/lang/String;
    const/16 v24, 0x0

    .line 676
    .local v24, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    const-string/jumbo v13, "LENGTH(name)"

    .line 678
    .local v13, "sortOrder":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->isPartialFavoriteMatch(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 679
    const/4 v3, 0x0

    .line 753
    .end local v7    # "projection":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v3

    .line 681
    .restart local v7    # "projection":[Ljava/lang/String;
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v3}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v19

    .line 682
    .local v19, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    if-eqz v19, :cond_2

    .line 684
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v24, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 691
    :cond_2
    :goto_1
    if-nez v24, :cond_4

    .line 692
    const-string/jumbo v11, "name like \'%Quick list%\'"

    .line 699
    :goto_2
    if-eqz v24, :cond_a

    .line 700
    const/16 v3, 0xa

    move/from16 v0, v21

    if-le v0, v3, :cond_5

    :try_start_1
    invoke-static/range {p1 .. p1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->isPartialFavoriteMatch(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 701
    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v5, "secFilter"

    const-string/jumbo v6, "include"

    invoke-virtual {v3, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 702
    .local v4, "playListUri":Landroid/net/Uri;
    if-eqz v4, :cond_3

    .line 703
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v8, "_id"

    aput-object v8, v5, v6

    const-string/jumbo v6, "name= ?"

    const/4 v8, 0x1

    new-array v7, v8, [Ljava/lang/String;

    .end local v7    # "projection":[Ljava/lang/String;
    const/4 v8, 0x0

    const-string/jumbo v9, "FavoriteList#328795!432@1341"

    aput-object v9, v7, v8

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 704
    :cond_3
    if-eqz v16, :cond_a

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 705
    const-string/jumbo v3, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    .line 706
    .local v18, "favoriteId":Ljava/lang/Long;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getFavoriteMusicList(Landroid/content/Context;Ljava/lang/Long;)Ljava/util/List;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 750
    if-eqz v16, :cond_0

    .line 751
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 685
    .end local v4    # "playListUri":Landroid/net/Uri;
    .end local v18    # "favoriteId":Ljava/lang/Long;
    .restart local v7    # "projection":[Ljava/lang/String;
    :catch_0
    move-exception v17

    .line 686
    .local v17, "e":Ljava/lang/InstantiationException;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1

    .line 687
    .end local v17    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v17

    .line 688
    .local v17, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 694
    .end local v17    # "e":Ljava/lang/IllegalAccessException;
    :cond_4
    invoke-interface/range {v24 .. v24}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicAppVersion()I

    move-result v21

    .line 695
    invoke-interface/range {v24 .. v24}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicFavorite()Ljava/lang/String;

    move-result-object v11

    goto :goto_2

    .line 710
    :cond_5
    const/16 v3, 0xb

    move/from16 v0, v21

    if-ge v0, v3, :cond_a

    const/4 v3, 0x0

    :try_start_2
    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->usingLocalMusicDB(Z)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-static/range {p1 .. p1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->isPartialFavoriteMatch(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 711
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v3, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v6

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v8, v11

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 712
    if-eqz v16, :cond_7

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 713
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 714
    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .local v23, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :try_start_3
    sget-object v3, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    .line 716
    .local v14, "albumUri":Ljava/lang/String;
    :cond_6
    const-string/jumbo v3, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    .line 717
    .local v25, "songId":Ljava/lang/Long;
    const-string/jumbo v3, "artist"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 718
    .local v15, "artist":Ljava/lang/String;
    const-string/jumbo v3, "title"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 719
    .local v26, "title":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    .line 720
    .local v20, "imageUri":Landroid/net/Uri;
    new-instance v3, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v20

    invoke-direct {v3, v0, v1, v15, v2}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 722
    sget-object v3, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "_ID---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 723
    sget-object v3, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Artist---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 724
    sget-object v3, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Title ---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 725
    sget-object v3, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "ALBUM_ID ---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 728
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v3

    if-nez v3, :cond_6

    move-object/from16 v22, v23

    .line 730
    .end local v14    # "albumUri":Ljava/lang/String;
    .end local v15    # "artist":Ljava/lang/String;
    .end local v20    # "imageUri":Landroid/net/Uri;
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .end local v25    # "songId":Ljava/lang/Long;
    .end local v26    # "title":Ljava/lang/String;
    .restart local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_7
    if-eqz v22, :cond_9

    :try_start_4
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 731
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->setIsFavorite()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 750
    :goto_3
    if-eqz v16, :cond_8

    .line 751
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_8
    move-object/from16 v3, v22

    .line 734
    goto/16 :goto_0

    .line 733
    :cond_9
    const/16 v22, 0x0

    goto :goto_3

    .line 738
    .end local v7    # "projection":[Ljava/lang/String;
    :cond_a
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v10, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 739
    if-eqz v16, :cond_d

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 741
    sget-object v3, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "** cursor.size()---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 742
    const-string/jumbo v3, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    .line 743
    .restart local v18    # "favoriteId":Ljava/lang/Long;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getFavoriteMusicList(Landroid/content/Context;Ljava/lang/Long;)Ljava/util/List;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v3

    .line 750
    if-eqz v16, :cond_0

    .line 751
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 746
    .end local v18    # "favoriteId":Ljava/lang/Long;
    :catch_2
    move-exception v17

    .line 748
    .local v17, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_6
    sget-object v3, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v5, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 750
    if-eqz v16, :cond_b

    .line 751
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .end local v17    # "e":Ljava/lang/Exception;
    :cond_b
    :goto_5
    move-object/from16 v3, v22

    .line 753
    goto/16 :goto_0

    .line 749
    :catchall_0
    move-exception v3

    .line 750
    :goto_6
    if-eqz v16, :cond_c

    .line 751
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 752
    :cond_c
    throw v3

    .line 750
    :cond_d
    if-eqz v16, :cond_b

    .line 751
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 749
    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v7    # "projection":[Ljava/lang/String;
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catchall_1
    move-exception v3

    move-object/from16 v22, v23

    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_6

    .line 746
    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_3
    move-exception v17

    move-object/from16 v22, v23

    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_4
.end method

.method public static getGenericList(Landroid/content/Context;Ljava/lang/String;ZILjava/lang/String;)Landroid/util/Pair;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .param p3, "max"    # I
    .param p4, "defaultQuickListName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "ZI",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 84
    invoke-static {p0, p1, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getArtistList(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/util/List;

    move-result-object v0

    .line 85
    .local v0, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ARTIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 103
    :goto_0
    return-object v1

    .line 90
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getTitleList(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/util/List;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 92
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->TITLE:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 96
    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getAlbumList(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/util/List;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 98
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ALBUM:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :cond_2
    invoke-static {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getPlaylistList(Landroid/content/Context;Ljava/lang/String;ZILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 103
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAYLIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static getIsFavorite()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1105
    sget-boolean v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->isDefaultFavorite:Z

    if-eqz v1, :cond_0

    .line 1106
    sput-boolean v0, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->isDefaultFavorite:Z

    .line 1107
    const/4 v0, 0x1

    .line 1109
    :cond_0
    return v0
.end method

.method public static getPlaylistList(Landroid/content/Context;Ljava/lang/String;ZILjava/lang/String;)Ljava/util/List;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .param p3, "max"    # I
    .param p4, "defaultQuickListName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "ZI",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 757
    const/4 v9, 0x0

    .line 758
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 759
    .local v12, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v6, "name"

    aput-object v6, v4, v2

    .line 760
    .local v4, "projection":[Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 762
    .local v5, "where":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v11

    .line 763
    .local v11, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v13, 0x0

    .line 764
    .local v13, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v11, :cond_0

    .line 766
    :try_start_0
    invoke-virtual {v11}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v13, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 774
    :cond_0
    :goto_0
    if-eqz v13, :cond_6

    invoke-static/range {p1 .. p1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->isPartialFavoriteMatch(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v6, "dm_main"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 775
    invoke-interface {v13}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicFavorite()Ljava/lang/String;

    move-result-object v5

    .line 787
    :goto_1
    :try_start_1
    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 788
    .local v3, "playListUri":Landroid/net/Uri;
    if-eqz v3, :cond_1

    .line 789
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 790
    :cond_1
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 792
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "** cursor.size()---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 794
    const/4 v8, 0x0

    .line 796
    .local v8, "count":I
    :cond_2
    const-string/jumbo v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 797
    .local v14, "playlistId":Ljava/lang/Long;
    const-string/jumbo v2, "name"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 800
    .local v15, "playlistName":Ljava/lang/String;
    invoke-interface {v13}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicAppVersion()I

    move-result v2

    const/16 v6, 0xb

    if-ge v2, v6, :cond_a

    const-string/jumbo v2, "Quick list"

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 801
    move-object/from16 v16, p4

    .line 802
    .local v16, "playlistNameLocalized":Ljava/lang/String;
    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 812
    :goto_2
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Playlist ID---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 813
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Playlist Name ---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 816
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->searchSongForPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v17

    .line 817
    .local v17, "songList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    if-eqz v17, :cond_3

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_3

    .line 818
    new-instance v2, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    const-string/jumbo v6, ""

    sget-object v7, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    move-object/from16 v0, v16

    invoke-direct {v2, v14, v0, v6, v7}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 819
    add-int/lit8 v8, v8, 0x1

    .line 821
    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_4

    .line 795
    move/from16 v0, p3

    if-lt v8, v0, :cond_2

    .line 828
    .end local v8    # "count":I
    .end local v14    # "playlistId":Ljava/lang/Long;
    .end local v15    # "playlistName":Ljava/lang/String;
    .end local v16    # "playlistNameLocalized":Ljava/lang/String;
    .end local v17    # "songList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_4
    if-eqz v9, :cond_5

    .line 829
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 831
    .end local v3    # "playListUri":Landroid/net/Uri;
    :cond_5
    :goto_3
    return-object v12

    .line 767
    :catch_0
    move-exception v10

    .line 768
    .local v10, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v10}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_0

    .line 769
    .end local v10    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v10

    .line 770
    .local v10, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v10}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_0

    .line 777
    .end local v10    # "e":Ljava/lang/IllegalAccessException;
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v6, "ko-KR"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 778
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "("

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ") OR ("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "REPLACE(`name`,\' \',\'\')"

    sget-object v7, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAYLIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v6, v0, v1, v7}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ")"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 779
    goto/16 :goto_1

    .line 781
    :cond_7
    if-eqz p1, :cond_8

    const-string/jumbo v2, ""

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    const/4 v5, 0x0

    :goto_4
    goto/16 :goto_1

    :cond_9
    const-string/jumbo v2, "name"

    sget-object v6, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAYLIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v2, v0, v1, v6}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 804
    .restart local v3    # "playListUri":Landroid/net/Uri;
    .restart local v8    # "count":I
    .restart local v14    # "playlistId":Ljava/lang/Long;
    .restart local v15    # "playlistName":Ljava/lang/String;
    :cond_a
    :try_start_2
    invoke-interface {v13}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicAppVersion()I

    move-result v2

    const/16 v6, 0xa

    if-le v2, v6, :cond_b

    const-string/jumbo v2, "FavoriteList#328795!432@1341"

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 805
    move-object/from16 v16, p4

    .line 806
    .restart local v16    # "playlistNameLocalized":Ljava/lang/String;
    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v14

    .line 807
    goto/16 :goto_2

    .line 809
    .end local v16    # "playlistNameLocalized":Ljava/lang/String;
    :cond_b
    move-object/from16 v16, v15

    .restart local v16    # "playlistNameLocalized":Ljava/lang/String;
    goto/16 :goto_2

    .line 824
    .end local v3    # "playListUri":Landroid/net/Uri;
    .end local v8    # "count":I
    .end local v14    # "playlistId":Ljava/lang/Long;
    .end local v15    # "playlistName":Ljava/lang/String;
    .end local v16    # "playlistNameLocalized":Ljava/lang/String;
    :catch_2
    move-exception v10

    .line 826
    .local v10, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v6, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 828
    if-eqz v9, :cond_5

    .line 829
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    .line 827
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 828
    if-eqz v9, :cond_c

    .line 829
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 830
    :cond_c
    throw v2
.end method

.method public static getTitleList(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/util/List;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleName"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .param p3, "max"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "ZI)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 512
    const/4 v12, 0x0

    .line 513
    .local v12, "cursor":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 514
    .local v16, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v6, "album_id"

    aput-object v6, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v6, "artist"

    aput-object v6, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v6, "title"

    aput-object v6, v4, v2

    .line 516
    .local v4, "projection":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v6, ""

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 517
    const-string/jumbo v5, "is_music=1"

    .line 524
    .local v5, "where":Ljava/lang/String;
    :cond_0
    :goto_0
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v14

    .line 525
    .local v14, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v18, 0x0

    .line 526
    .local v18, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v14, :cond_1

    .line 528
    :try_start_0
    invoke-virtual {v14}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v18, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 536
    :cond_1
    :goto_1
    const/4 v3, 0x0

    .line 537
    .local v3, "musicUri":Landroid/net/Uri;
    if-eqz v18, :cond_7

    .line 538
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v3

    .line 541
    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const-string/jumbo v7, "title_key"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 543
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 544
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .local v17, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    if-eqz v12, :cond_3

    :try_start_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 546
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "** cursor.size()---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 547
    const-string/jumbo v9, ""

    .line 548
    .local v9, "albumUri":Ljava/lang/String;
    if-eqz v18, :cond_8

    .line 549
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 552
    :goto_3
    const/4 v11, 0x0

    .line 554
    .local v11, "count":I
    :cond_2
    const-string/jumbo v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    .line 555
    .local v19, "songId":Ljava/lang/Long;
    const-string/jumbo v2, "album_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 556
    .local v8, "albumId":Ljava/lang/String;
    const-string/jumbo v2, "artist"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 557
    .local v10, "artist":Ljava/lang/String;
    const-string/jumbo v2, "title"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 558
    .local v20, "title":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    .line 561
    .local v15, "imageUri":Landroid/net/Uri;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Artist---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 562
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Title ---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 563
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "ALBUM_ID ---"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 565
    new-instance v2, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v2, v0, v1, v10, v15}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 566
    add-int/lit8 v11, v11, 0x1

    .line 567
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-eqz v2, :cond_3

    .line 553
    move/from16 v0, p3

    if-lt v11, v0, :cond_2

    .line 573
    .end local v8    # "albumId":Ljava/lang/String;
    .end local v9    # "albumUri":Ljava/lang/String;
    .end local v10    # "artist":Ljava/lang/String;
    .end local v11    # "count":I
    .end local v15    # "imageUri":Landroid/net/Uri;
    .end local v19    # "songId":Ljava/lang/Long;
    .end local v20    # "title":Ljava/lang/String;
    :cond_3
    if-eqz v12, :cond_4

    .line 574
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object/from16 v16, v17

    .line 577
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_5
    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    .line 581
    return-object v16

    .line 519
    .end local v3    # "musicUri":Landroid/net/Uri;
    .end local v5    # "where":Ljava/lang/String;
    .end local v14    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v18    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "title"

    sget-object v7, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->TITLE:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v6, v0, v1, v7}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, " and "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "is_music"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "=1"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 520
    .restart local v5    # "where":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v6, "ko-KR"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 521
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "("

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ") OR ("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "REPLACE(`title`,\' \',\'\')"

    sget-object v7, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->TITLE:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v6, v0, v1, v7}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, " and "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "is_music"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "=1)"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 529
    .restart local v14    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v18    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_0
    move-exception v13

    .line 530
    .local v13, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v13}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_1

    .line 531
    .end local v13    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v13

    .line 532
    .local v13, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v13}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_1

    .line 540
    .end local v13    # "e":Ljava/lang/IllegalAccessException;
    .restart local v3    # "musicUri":Landroid/net/Uri;
    :cond_7
    :try_start_3
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 551
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v9    # "albumUri":Ljava/lang/String;
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_8
    :try_start_4
    const-string/jumbo v9, "content://media/external/audio/albumart/"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_3

    .line 569
    .end local v9    # "albumUri":Ljava/lang/String;
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_2
    move-exception v13

    .line 571
    .local v13, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_5
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v6, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 573
    if-eqz v12, :cond_5

    .line 574
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_4

    .line 572
    .end local v13    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 573
    :goto_6
    if-eqz v12, :cond_9

    .line 574
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 576
    :cond_9
    throw v2

    .line 572
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object/from16 v16, v17

    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_6

    .line 569
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_3
    move-exception v13

    move-object/from16 v16, v17

    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_5
.end method

.method public static isAnyMusic(Landroid/content/Context;)Z
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 108
    const/4 v1, 0x5

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v3, v11

    const-string/jumbo v1, "album_id"

    aput-object v1, v3, v12

    const/4 v1, 0x2

    const-string/jumbo v5, "artist"

    aput-object v5, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v5, "title"

    aput-object v5, v3, v1

    const/4 v1, 0x4

    const-string/jumbo v5, "album"

    aput-object v5, v3, v1

    .line 109
    .local v3, "projection":[Ljava/lang/String;
    const-string/jumbo v4, "is_music != 0 "

    .line 110
    .local v4, "selection":Ljava/lang/String;
    const/4 v7, 0x0

    .line 111
    .local v7, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v9

    .line 112
    .local v9, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v10, 0x0

    .line 113
    .local v10, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v9, :cond_0

    .line 115
    :try_start_0
    invoke-virtual {v9}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v10, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 123
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 124
    .local v2, "musicUri":Landroid/net/Uri;
    if-eqz v10, :cond_1

    .line 125
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v10, v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    .line 129
    :goto_1
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "musicUri = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v5, 0x0

    const-string/jumbo v6, "title_key"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v7

    .line 131
    if-nez v7, :cond_2

    move v1, v11

    .line 146
    :goto_2
    return v1

    .line 116
    .end local v2    # "musicUri":Landroid/net/Uri;
    :catch_0
    move-exception v8

    .line 117
    .local v8, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v8}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 118
    .end local v8    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v8

    .line 119
    .local v8, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v8}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 127
    .end local v8    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "musicUri":Landroid/net/Uri;
    :cond_1
    :try_start_2
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 133
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v1, v12, :cond_3

    .line 134
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move v1, v11

    .line 135
    goto :goto_2

    .line 137
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move v1, v12

    .line 138
    goto :goto_2

    .line 140
    :catch_2
    move-exception v8

    .line 142
    .local v8, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v5, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    if-eqz v7, :cond_4

    .line 144
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    move v1, v11

    .line 146
    goto :goto_2
.end method

.method public static isPartialFavoriteMatch(Ljava/lang/String;)Z
    .locals 5
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 1027
    const-string/jumbo v0, ""

    .line 1029
    .local v0, "fullName":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 1043
    :cond_0
    :goto_0
    return v1

    .line 1032
    :cond_1
    const-string/jumbo v2, "en-GB"

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1033
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->playlists_quicklist_default_value1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 1037
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_3

    .line 1038
    invoke-virtual {p0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 1035
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->playlists_quicklist_default_value:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1040
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1041
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static populateMusicMappings(Landroid/content/Context;)V
    .locals 17
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 890
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSongNameList()Ljava/util/ArrayList;

    move-result-object v16

    .line 891
    .local v16, "songNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getAlbumNameList()Ljava/util/ArrayList;

    move-result-object v8

    .line 892
    .local v8, "albumNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getArtistNameList()Ljava/util/ArrayList;

    move-result-object v10

    .line 894
    .local v10, "artistNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v4, "artist"

    aput-object v4, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v4, "title"

    aput-object v4, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v4, "album"

    aput-object v4, v3, v1

    .line 896
    .local v3, "projection":[Ljava/lang/String;
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v13

    .line 897
    .local v13, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v14, 0x0

    .line 898
    .local v14, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v13, :cond_0

    .line 900
    :try_start_0
    invoke-virtual {v13}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v14, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 908
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 909
    .local v2, "musicUri":Landroid/net/Uri;
    if-eqz v14, :cond_6

    .line 910
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v14, v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    .line 913
    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string/jumbo v6, "title_key"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 915
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_7

    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 917
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "** cursor.size()---"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 920
    :cond_1
    const-string/jumbo v1, "title"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 921
    .local v15, "songName":Ljava/lang/String;
    const-string/jumbo v1, "album"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 922
    .local v7, "albumName":Ljava/lang/String;
    const-string/jumbo v1, "artist"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 924
    .local v9, "artistName":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 926
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Adding song name --- "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 927
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 929
    :cond_2
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 931
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Adding album name --- "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 932
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 934
    :cond_3
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 936
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Adding artist name --- "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 937
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 939
    :cond_4
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 945
    .end local v7    # "albumName":Ljava/lang/String;
    .end local v9    # "artistName":Ljava/lang/String;
    .end local v15    # "songName":Ljava/lang/String;
    :goto_2
    if-eqz v11, :cond_5

    .line 946
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 947
    const/4 v11, 0x0

    .line 951
    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->populatePlaylistMappings(Landroid/content/Context;)V

    .line 952
    return-void

    .line 901
    .end local v2    # "musicUri":Landroid/net/Uri;
    .end local v11    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v12

    .line 902
    .local v12, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v12}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_0

    .line 903
    .end local v12    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v12

    .line 904
    .local v12, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v12}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_0

    .line 912
    .end local v12    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "musicUri":Landroid/net/Uri;
    :cond_6
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto/16 :goto_1

    .line 942
    .restart local v11    # "cursor":Landroid/database/Cursor;
    :cond_7
    :try_start_2
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v4, "No songs in library."

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 944
    :catchall_0
    move-exception v1

    .line 945
    if-eqz v11, :cond_8

    .line 946
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 947
    const/4 v11, 0x0

    .line 949
    :cond_8
    throw v1
.end method

.method private static populatePlaylistMappings(Landroid/content/Context;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 955
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getPlaylistNameList()Ljava/util/ArrayList;

    move-result-object v12

    .line 957
    .local v12, "playlistNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v5, "name"

    aput-object v5, v3, v1

    .line 958
    .local v3, "projection":[Ljava/lang/String;
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v9

    .line 959
    .local v9, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v10, 0x0

    .line 960
    .local v10, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v9, :cond_0

    .line 962
    :try_start_0
    invoke-virtual {v9}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v10, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 969
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 970
    .local v2, "playlistUri":Landroid/net/Uri;
    if-eqz v10, :cond_4

    .line 971
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v10, v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    .line 974
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v6, "name"

    move-object v5, v4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 976
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_5

    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 978
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "** cursor.size()---"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 981
    :cond_1
    const-string/jumbo v1, "name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 983
    .local v11, "playlistName":Ljava/lang/String;
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 985
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Adding playlist --- "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 986
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 988
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 994
    .end local v11    # "playlistName":Ljava/lang/String;
    :goto_2
    if-eqz v7, :cond_3

    .line 995
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 996
    :cond_3
    const/4 v7, 0x0

    .line 998
    return-void

    .line 963
    .end local v2    # "playlistUri":Landroid/net/Uri;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 964
    .local v8, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v8}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 965
    .end local v8    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v8

    .line 966
    .local v8, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v8}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 973
    .end local v8    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "playlistUri":Landroid/net/Uri;
    :cond_4
    sget-object v2, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 991
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :cond_5
    :try_start_2
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v4, "No playlists in library."

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 993
    :catchall_0
    move-exception v1

    .line 994
    if-eqz v7, :cond_6

    .line 995
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 996
    :cond_6
    const/4 v7, 0x0

    .line 997
    throw v1
.end method

.method private static searchSongForPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 30
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 585
    const/16 v19, 0x0

    .line 586
    .local v19, "cursor":Landroid/database/Cursor;
    const/16 v23, 0x0

    .line 587
    .local v23, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const-string/jumbo v4, "name"

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 588
    .local v7, "where":Ljava/lang/String;
    const-string/jumbo v9, "LENGTH(name)"

    .line 590
    .local v9, "sortOrder":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v4}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v21

    .line 591
    .local v21, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v25, 0x0

    .line 592
    .local v25, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v21, :cond_0

    .line 594
    :try_start_0
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v25, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 601
    :cond_0
    :goto_0
    const/16 v26, 0x0

    .line 602
    .local v26, "playlistUri":Landroid/net/Uri;
    if-eqz v25, :cond_5

    .line 603
    :try_start_1
    sget-object v4, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v26

    .line 606
    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 607
    if-eqz v19, :cond_3

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 609
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "** cursor.size()---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 610
    const-string/jumbo v4, "_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    .line 612
    .local v27, "playlist_id":Ljava/lang/Long;
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "playlist_id --"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 613
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 614
    const/4 v4, 0x5

    new-array v12, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "_id"

    aput-object v5, v12, v4

    const/4 v4, 0x1

    .line 615
    const-string/jumbo v5, "artist"

    aput-object v5, v12, v4

    const/4 v4, 0x2

    .line 616
    const-string/jumbo v5, "title"

    aput-object v5, v12, v4

    const/4 v4, 0x3

    .line 617
    const-string/jumbo v5, "audio_id"

    aput-object v5, v12, v4

    const/4 v4, 0x4

    .line 618
    const-string/jumbo v5, "album_id"

    aput-object v5, v12, v4

    .line 619
    .local v12, "projection":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 620
    .local v11, "uriTest":Landroid/net/Uri;
    if-eqz v25, :cond_6

    .line 621
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4, v5}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicPlaylistUri(J)Landroid/net/Uri;

    move-result-object v11

    .line 625
    :goto_2
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "uriTest = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 626
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v13, "is_music != 0 "

    const/4 v14, 0x0

    const-string/jumbo v15, "play_order"

    invoke-virtual/range {v10 .. v15}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 627
    if-eqz v19, :cond_1

    .line 629
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Size of songs in playlist *** *** ---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 631
    :cond_1
    if-eqz v19, :cond_3

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 632
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 633
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .local v24, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :try_start_2
    const-string/jumbo v17, ""

    .line 634
    .local v17, "albumUri":Ljava/lang/String;
    if-eqz v25, :cond_7

    .line 635
    sget-object v4, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v17

    .line 639
    :cond_2
    :goto_3
    const-string/jumbo v4, "audio_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v28

    .line 640
    .local v28, "songId":Ljava/lang/Long;
    const-string/jumbo v4, "album_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 641
    .local v16, "albumId":Ljava/lang/String;
    const-string/jumbo v4, "artist"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 642
    .local v18, "artist":Ljava/lang/String;
    const-string/jumbo v4, "title"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 643
    .local v29, "title":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    .line 644
    .local v22, "imageUri":Landroid/net/Uri;
    new-instance v4, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v18

    move-object/from16 v3, v22

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Artist---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 647
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Title ---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 648
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "ALBUM_ID ---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/logging/Logger;->info(Ljava/lang/String;)V

    .line 650
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v23, v24

    .line 657
    .end local v11    # "uriTest":Landroid/net/Uri;
    .end local v12    # "projection":[Ljava/lang/String;
    .end local v16    # "albumId":Ljava/lang/String;
    .end local v17    # "albumUri":Ljava/lang/String;
    .end local v18    # "artist":Ljava/lang/String;
    .end local v22    # "imageUri":Landroid/net/Uri;
    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .end local v27    # "playlist_id":Ljava/lang/Long;
    .end local v28    # "songId":Ljava/lang/Long;
    .end local v29    # "title":Ljava/lang/String;
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_3
    if-eqz v19, :cond_4

    .line 658
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 661
    :cond_4
    :goto_4
    return-object v23

    .line 595
    .end local v26    # "playlistUri":Landroid/net/Uri;
    :catch_0
    move-exception v20

    .line 597
    .local v20, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v5, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 605
    .end local v20    # "e":Ljava/lang/Exception;
    .restart local v26    # "playlistUri":Landroid/net/Uri;
    :cond_5
    :try_start_3
    const-string/jumbo v4, "content://com.samsung.musicplus/audio/playlists/"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v26

    goto/16 :goto_1

    .line 623
    .restart local v11    # "uriTest":Landroid/net/Uri;
    .restart local v12    # "projection":[Ljava/lang/String;
    .restart local v27    # "playlist_id":Ljava/lang/Long;
    :cond_6
    const-string/jumbo v4, "external"

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v11

    goto/16 :goto_2

    .line 637
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v17    # "albumUri":Ljava/lang/String;
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_7
    :try_start_4
    const-string/jumbo v17, "content://media/external/audio/albumart/"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_3

    .line 653
    .end local v11    # "uriTest":Landroid/net/Uri;
    .end local v12    # "projection":[Ljava/lang/String;
    .end local v17    # "albumUri":Ljava/lang/String;
    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .end local v27    # "playlist_id":Ljava/lang/Long;
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_1
    move-exception v20

    .line 655
    .restart local v20    # "e":Ljava/lang/Exception;
    :goto_5
    :try_start_5
    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-class v5, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 657
    if-eqz v19, :cond_4

    .line 658
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 656
    .end local v20    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 657
    :goto_6
    if-eqz v19, :cond_8

    .line 658
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 660
    :cond_8
    throw v4

    .line 656
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v11    # "uriTest":Landroid/net/Uri;
    .restart local v12    # "projection":[Ljava/lang/String;
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v27    # "playlist_id":Ljava/lang/Long;
    :catchall_1
    move-exception v4

    move-object/from16 v23, v24

    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_6

    .line 653
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :catch_2
    move-exception v20

    move-object/from16 v23, v24

    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    goto :goto_5
.end method

.method public static setIsFavorite()V
    .locals 1

    .prologue
    .line 1101
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->isDefaultFavorite:Z

    .line 1102
    return-void
.end method
