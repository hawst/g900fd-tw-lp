.class public final Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;
.super Ljava/lang/Object;
.source "SettingsInitializer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;,
        Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;,
        Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$vlingo$midas$samsungutils$configuration$SettingsInitializer$SETTING_TYPE:[I

.field private static final SETTINGS_FOR_APPLICATION:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

.field private static final SETTINGS_FOR_APPLICATION_PROPOSED:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

.field private static final SETTINGS_FOR_SERVICE:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

.field private static final TAG:Ljava/lang/String;

.field private static followType:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

.field private static settingsInUse:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;


# direct methods
.method static synthetic $SWITCH_TABLE$com$vlingo$midas$samsungutils$configuration$SettingsInitializer$SETTING_TYPE()[I
    .locals 3

    .prologue
    .line 26
    sget-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->$SWITCH_TABLE$com$vlingo$midas$samsungutils$configuration$SettingsInitializer$SETTING_TYPE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->values()[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->FLOAT:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->LONG:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->$SWITCH_TABLE$com$vlingo$midas$samsungutils$configuration$SettingsInitializer$SETTING_TYPE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 27
    const-class v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    .line 52
    const/16 v0, 0x54

    new-array v0, v0, [Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .line 58
    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "language"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v5

    .line 59
    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "audiofilelog_enabled"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v6

    .line 60
    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "phrasespot_waveformlogging_enabled"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v7

    .line 61
    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "driving_mode_audio_files"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v8

    .line 62
    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "location_enabled"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v9

    const/4 v1, 0x5

    .line 63
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "custom_tone_encoding"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 64
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "calendar.default_calendar_key"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 65
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "dm.username"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 66
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "FORCE_NON_DM"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 68
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_voice_prompt_confirm_with_user"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 69
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "contacts.use_other_names"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 70
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "auto_dial"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 71
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "auto_endpointing"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 72
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "CARRIER"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 73
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "CARRIER_COUNTRY"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 76
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVER_NAME"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 77
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVICES_HOST_NAME"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 78
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "EVENTLOG_HOST_NAME"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 79
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "HELLO_HOST_NAME"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 80
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "LMTT_HOST_NAME"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 82
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.asr_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 83
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.hello_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 84
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.lmtt_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 85
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.log_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 86
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.tts_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 87
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.vcs_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 88
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.rollout_groupid"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 89
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.rollout_percentage"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 91
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "calendar.app_package"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 92
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "calendar.preference_filename"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 93
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "new_contact_match_algo"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 94
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "processing_tone_fadeout_period"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 95
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "profanity_filter"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 97
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tos_accepted"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 98
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tos_accepted_date"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 99
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tos_accepted_version"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 100
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "iux_complete"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 101
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "samsung_disclaimer_accepted"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 103
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "car_iux_tts_cacheing_required"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 104
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_tts_fallback_engine"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 105
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_ignore_use_speech_rate"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 106
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_required_engine"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 108
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_audiotrack_tone_player"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 109
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_default_phone"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    .line 110
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_hidden_calendars"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 111
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_mediasync_tone_approach"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    .line 112
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_network_tts"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    .line 113
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_non_j_audio_sources"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x30

    .line 114
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "validate_launch_intent_version"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x31

    .line 115
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "vcs.timeout.ms"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x32

    .line 116
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "weather_use_vp_location"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x33

    .line 117
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_voice_prompt"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x34

    .line 118
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "auto_punctuation"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x35

    .line 119
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "car_nav_home_address"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x36

    .line 120
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tos_launched_for_tos_other_app"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x37

    .line 121
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "former_tos_acceptance_state"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x38

    .line 128
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "uuid"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x39

    .line 129
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "zip_timestamp_"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    .line 131
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "asr.manager"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    .line 132
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "asr.http.keep_alive"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    .line 133
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "asr.http.timeout.connect_ms"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    .line 134
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "asr.http.timeout.read_ms"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    .line 136
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "stats.enable"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    .line 137
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "acceptedtext.enable"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x40

    .line 138
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "activitylog.enable"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x41

    .line 145
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "appstate.first_run.calypso"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x42

    .line 146
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "DEVICE_MODEL"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x43

    .line 147
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "dynamic_config_disabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x44

    .line 148
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time.speech.long"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x45

    .line 149
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time.speech.long.msg"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x46

    .line 150
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time.speech.medium"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x47

    .line 151
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time.speech.short"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x48

    .line 152
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time_withoutspeech"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x49

    .line 153
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time_withspeech"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    .line 154
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "hello_request_complete"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    .line 155
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "max_audio_time"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    .line 156
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "nothing_recognized_reprompt.count"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    .line 157
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "nothing_recognized_reprompt.max_value"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    .line 158
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVER_RESONSE_FILE"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    .line 159
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVER_RESONSE_LOGGGING"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x50

    .line 160
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "speex.complexity"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x51

    .line 161
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "speex.quality"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x52

    .line 162
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "speex.variable_bitrate"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x53

    .line 163
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "speex.voice_activity_detection"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    .line 52
    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_SERVICE:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .line 166
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .line 174
    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "language"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v5

    .line 175
    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "iux_complete"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v6

    .line 176
    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "tos_accepted"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v7

    .line 177
    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "tos_accepted_date"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v8

    .line 178
    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "tos_accepted_version"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v9

    const/4 v1, 0x5

    .line 190
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "processing_tone_fadeout_period"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 191
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_audiotrack_tone_player"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 192
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_voice_prompt_confirm_with_user"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 193
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "new_contact_match_algo"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 194
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "car_iux_tts_cacheing_required"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 195
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_tts_fallback_engine"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 196
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_ignore_use_speech_rate"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 197
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_required_engine"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 198
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_default_phone"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 211
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "audiofilelog_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 212
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "phrasespot_waveformlogging_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 213
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "driving_mode_audio_files"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 216
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "CARRIER"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 217
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "CARRIER_COUNTRY"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 252
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "profanity_filter"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 253
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "car_nav_home_address"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 312
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "uuid"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 333
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "DEVICE_MODEL"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 334
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVER_RESONSE_FILE"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 335
    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVER_RESONSE_LOGGGING"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    .line 166
    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_APPLICATION_PROPOSED:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .line 338
    sget-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_APPLICATION_PROPOSED:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_APPLICATION:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V
    .locals 0

    .prologue
    .line 448
    invoke-static {p0, p1, p2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->setValue(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V

    return-void
.end method

.method public static initSettings(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;

    .prologue
    .line 347
    const/4 v0, 0x1

    .line 348
    .local v0, "amAService":Z
    invoke-static {p0, p1, v0}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->initSettings(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 349
    return-void
.end method

.method public static initSettings(Landroid/content/Context;Ljava/lang/String;IZ)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;
    .param p2, "providerVersion"    # I
    .param p3, "amAService"    # Z

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    .line 395
    if-eqz p3, :cond_0

    .line 396
    sget-object v6, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->SERVICE:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    sput-object v6, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->followType:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    .line 400
    :goto_0
    const-string/jumbo v6, "appstate.first_run.calypso"

    invoke-static {v6, v11}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 401
    .local v1, "firstRun":Z
    const-string/jumbo v6, "appstate.first_run.calypso"

    invoke-static {v6, v5}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 402
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 403
    .local v0, "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    sget-object v6, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->followType:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    sget-object v7, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->SERVICE:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    if-ne v6, v7, :cond_1

    .line 404
    sget-object v6, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_SERVICE:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    sput-object v6, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->settingsInUse:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .line 408
    :goto_1
    sget-object v6, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->settingsInUse:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    array-length v7, v6

    :goto_2
    if-lt v5, v7, :cond_2

    .line 434
    return-void

    .line 398
    .end local v0    # "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    .end local v1    # "firstRun":Z
    :cond_0
    sget-object v6, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->APPLICATION:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    sput-object v6, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->followType:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    goto :goto_0

    .line 406
    .restart local v0    # "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    .restart local v1    # "firstRun":Z
    :cond_1
    sget-object v6, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_APPLICATION:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    sput-object v6, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->settingsInUse:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    goto :goto_1

    .line 408
    :cond_2
    aget-object v3, v6, v5

    .line 409
    .local v3, "setting":Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    if-eqz v1, :cond_3

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->firstRunInitOnly:Z
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$0(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 410
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentProviderPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$1(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 411
    .local v2, "newSettingValue":Ljava/lang/String;
    sget-object v8, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "First run initializes "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$2(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$2(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v8

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->type:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$3(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    move-result-object v9

    invoke-static {v8, v2, v9}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->setValue(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V

    .line 408
    .end local v2    # "newSettingValue":Ljava/lang/String;
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 413
    :cond_3
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->firstRunInitOnly:Z
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$0(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 414
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentProviderPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$1(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 415
    .restart local v2    # "newSettingValue":Ljava/lang/String;
    sget-object v8, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Initializing "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$2(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " and adding content observer."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$2(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v8

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->type:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$3(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    move-result-object v9

    invoke-static {v8, v2, v9}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->setValue(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V

    .line 417
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$4(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 418
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$4(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 421
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentProviderPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$1(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 422
    .local v4, "uri":Landroid/net/Uri;
    new-instance v8, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;

    new-instance v9, Landroid/os/Handler;

    invoke-direct {v9}, Landroid/os/Handler;-><init>()V

    invoke-direct {v8, v9, v3, v0, p1}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;-><init>(Landroid/os/Handler;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;Ljava/lang/String;)V

    invoke-static {v3, v8}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$5(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;Landroid/database/ContentObserver;)V

    .line 429
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$4(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;

    move-result-object v9

    invoke-virtual {v8, v4, v11, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto/16 :goto_3

    .line 431
    .end local v2    # "newSettingValue":Ljava/lang/String;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_5
    sget-object v8, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Setting "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$2(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " is skipped since it\'s updated on the first run only."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method public static initSettings(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;
    .param p2, "amAService"    # Z

    .prologue
    .line 360
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->initSettings(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 361
    return-void
.end method

.method public static isReady(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 482
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 483
    .local v0, "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    const-string/jumbo v4, "SETTINGS/tos_accepted"

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 484
    .local v2, "tosAcceptedStringValue":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 485
    const-string/jumbo v1, "applicationQueryManager returned null, ConfigProvider not ready?"

    .line 486
    .local v1, "msg":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    .end local v1    # "msg":Ljava/lang/String;
    :goto_0
    return v3

    .line 489
    :cond_0
    const-string/jumbo v4, "true"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 490
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "TOS not yet accepted, value="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 491
    .restart local v1    # "msg":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 494
    .end local v1    # "msg":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static isTosAccepted(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 501
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 502
    .local v0, "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    const-string/jumbo v4, "SETTINGS/tos_accepted"

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 503
    .local v2, "tosAcceptedStringValue":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 504
    const-string/jumbo v1, "applicationQueryManager returned null, ConfigProvider not ready?"

    .line 505
    .local v1, "msg":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    .end local v1    # "msg":Ljava/lang/String;
    :goto_0
    return v3

    .line 508
    :cond_0
    const-string/jumbo v4, "true"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 509
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "TOS not yet accepted, value="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 510
    .restart local v1    # "msg":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 513
    .end local v1    # "msg":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private static setValue(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V
    .locals 4
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    .prologue
    .line 449
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Copying value for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": Value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 452
    :try_start_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->$SWITCH_TABLE$com$vlingo$midas$samsungutils$configuration$SettingsInitializer$SETTING_TYPE()[I

    move-result-object v1

    invoke-virtual {p2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 466
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    :goto_0
    return-void

    .line 454
    :pswitch_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {p0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 469
    :catch_0
    move-exception v0

    .line 470
    .local v0, "ex":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 457
    .end local v0    # "ex":Ljava/lang/NumberFormatException;
    :pswitch_1
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 460
    :pswitch_2
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    goto :goto_0

    .line 463
    :pswitch_3
    invoke-static {p1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {p0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setFloat(Ljava/lang/String;F)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 473
    :cond_0
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Failed to update from master "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 452
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static uninitSettings(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 440
    sget-object v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->settingsInUse:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 446
    return-void

    .line 440
    :cond_0
    aget-object v0, v2, v1

    .line 441
    .local v0, "setting":Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v0}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$4(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 442
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v0}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$4(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 443
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$5(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;Landroid/database/ContentObserver;)V

    .line 440
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
