.class public Lcom/vlingo/midas/settings/SettingKeys;
.super Ljava/lang/Object;
.source "SettingKeys.java"


# static fields
.field public static final KEY_CAR_MOTION:Ljava/lang/String; = "car_word_spotter_motion"

.field public static final KEY_CAR_NAV_OFFICE_ADDRESS:Ljava/lang/String; = "car_nav_office_address"

.field public static final KEY_CLEAN_ACCEPTED_NOTIFICATIONS_INFO:Ljava/lang/String; = "clean_accepted_notifications_info"

.field public static final KEY_CLIENT_VERSION:Ljava/lang/String; = "CLIENT_VERSION"

.field public static final KEY_CUSTOMIZE_WAKE_UP_COMMAND:Ljava/lang/String; = "customize_wake_up_command"

.field public static final KEY_DETAILED_TTS_FEEDBACK:Ljava/lang/String; = "detailed_tts_feedback"

.field public static final KEY_DRIVING_MODE_FROM_MYPLACE:Ljava/lang/String; = "temp_driving_mode_from_myplace"

.field public static final KEY_ENABLE_AUTONAVI:Ljava/lang/String; = "CHINESE_NAVIGATION_AUTONAVI"

.field public static final KEY_ENABLE_BAIDUMAPS:Ljava/lang/String; = "CHINESE_NAVIGATION_BAIDU"

.field public static final KEY_ENABLE_ENAVI:Ljava/lang/String; = "CHINESE_NAVIGATION_ENAVI"

.field public static final KEY_FAKED_SALES_CODE_STRING_VALUE:Ljava/lang/String; = "faked_sales_code_value"

.field public static final KEY_FAKE_CHINESE_NAVIGATION_APP:Ljava/lang/String; = "FAKE_CHINESE_NAVIGATION_APP"

.field public static final KEY_FAKE_SALES_CODE_BOOLEAN:Ljava/lang/String; = "fake_sales_code"

.field public static final KEY_FAKE_VIDEO_CALLING:Ljava/lang/String; = "fake_video_calling"

.field public static final KEY_FORCE_CONTACT_ENCODING:Ljava/lang/String; = "force_contact_encoding"

.field public static final KEY_FORCE_CONTACT_NORMALIZATION:Ljava/lang/String; = "force_contact_normalization"

.field public static final KEY_HAS_CAR_MODE_APP:Ljava/lang/String; = "has_car_mode_app"

.field public static final KEY_HEADSET_MODE:Ljava/lang/String; = "headset_mode"

.field public static final KEY_HELP_VISIBLE:Ljava/lang/String; = "key_help_visible"

.field public static final KEY_HELP_VOICE_TALK:Ljava/lang/String; = "help_voice_talk"

.field public static final KEY_INPUT_VOICE_CONTROL:Ljava/lang/String; = "temp_input_voice_control"

.field public static final KEY_IN_CAR_MODE:Ljava/lang/String; = "in_car_mode"

.field public static final KEY_IS_EYES_FREE_MODE:Ljava/lang/String; = "is_eyes_free_mode"

.field public static final KEY_LAST_ASR_RESULT:Ljava/lang/String; = "last_asr_result"

.field public static final KEY_LAUNCH_CAR_ON_CAR_DOCK:Ljava/lang/String; = "launch_car_on_car_dock"

.field public static final KEY_LAUNCH_VOICETALK:Ljava/lang/String; = "launch_voicetalk"

.field public static final KEY_MANUALLY_TURNED_ON_PROMPT_IN_TALKBACK:Ljava/lang/String; = "manually_prompt_on_in_talkback"

.field public static final KEY_MIDAS_DOMAIN_LIST:Ljava/lang/String; = "midas_supported_domain_list"

.field public static final KEY_MIDAS_FIRST_RUN:Ljava/lang/String; = "key_midas_first_run"

.field public static final KEY_MIDAS_MODEL_NAME:Ljava/lang/String; = "midas_model_name"

.field public static final KEY_MULTI_WINDOW:Ljava/lang/String; = "temp_multi_window"

.field public static final KEY_OFFER_APP_CAR_MODE:Ljava/lang/String; = "offer_app_car_mode"

.field public static final KEY_OFFER_PHONE_CAR_MODE:Ljava/lang/String; = "offer_phone_car_mode"

.field public static final KEY_POPUP_WINDOW_OPENED:Ljava/lang/String; = "key_popup_window_opened"

.field public static final KEY_SAMSUNG_MULTI_ENGINE_ENABLE:Ljava/lang/String; = "samsung_multi_engine_enable"

.field public static final KEY_SAMSUNG_WAKEUP_DATA_ENABLE:Ljava/lang/String; = "samsung_wakeup_data_enable"

.field public static final KEY_SAMSUNG_WAKEUP_ENGINE_ENABLE:Ljava/lang/String; = "samsung_wakeup_engine_enable"

.field public static final KEY_SAY_WAKE_UP_COMMAND:Ljava/lang/String; = "say_wake_up_command"

.field public static final KEY_SEAMLESS_WAKEUP:Ljava/lang/String; = "seamless_wakeup"

.field public static final KEY_SET_WAKE_UP_COMMAND:Ljava/lang/String; = "key_set_wake_up_command"

.field public static final KEY_SHOW_USER_TURN_IN_DRIVING_MODE:Ljava/lang/String; = "show_user_turn_in_driving_mode"

.field public static final KEY_SOCIAL_SETTINGS:Ljava/lang/String; = "social_settings"

.field public static final KEY_SOCIAL_SETTINGS_CATEGORY:Ljava/lang/String; = "social_settings_category"

.field public static final KEY_START_FROM_OPTION_MENU:Ljava/lang/String; = "is_start_option_menu"

.field public static final KEY_SVOICE_FOR_GEAR:Ljava/lang/String; = "is_svoice_for_gear"

.field public static final KEY_TTS_NETWORK_TIMEOUT:Ljava/lang/String; = "tts_network_timeout"

.field public static final KEY_USE_ECHO_CANCEL_FOR_SPOTTER:Ljava/lang/String; = "use_echo_cancel_for_spotter"

.field public static final KEY_USE_HIDDEN_CALENDARS:Ljava/lang/String; = "use_hidden_calendars"

.field public static final KEY_USE_NON_J_AUDIO_SOURCES:Ljava/lang/String; = "use_non_j_audio_sources"

.field public static final KEY_VIDEO_CALLING_SUPPORTED_VALUE:Ljava/lang/String; = "video_calling_supported_value"

.field public static final KEY_VLINGO_CAR_NAV_SETTINGS:Ljava/lang/String; = "car_nav_settings"

.field public static final KEY_VLINGO_CAR_SETTINGS:Ljava/lang/String; = "vlingo_car_settings"

.field public static final KEY_VLINGO_FACEBOOK:Ljava/lang/String; = "facebook_preference_category"

.field public static final KEY_VLINGO_SETTINGS:Ljava/lang/String; = "vlingo_settings"

.field public static final KEY_VLINGO_TWITTER:Ljava/lang/String; = "twitter_preference_category"

.field public static final KEY_VLINGO_VOICE_COMMAND:Ljava/lang/String; = "voicecommand"

.field public static final KEY_VOICE_WAKE_UP:Ljava/lang/String; = "voice_wake_up"

.field public static final KEY_WAKE_UP_AND_AUTOFUNCTION1:Ljava/lang/String; = "kew_wake_up_and_auto_function1"

.field public static final KEY_WAKE_UP_AND_AUTOFUNCTION2:Ljava/lang/String; = "kew_wake_up_and_auto_function2"

.field public static final KEY_WAKE_UP_AND_AUTOFUNCTION3:Ljava/lang/String; = "kew_wake_up_and_auto_function3"

.field public static final KEY_WAKE_UP_AND_AUTOFUNCTION4:Ljava/lang/String; = "kew_wake_up_and_auto_function4"

.field public static final KEY_WAKE_UP_DEFAULT_ENGINE:Ljava/lang/String; = "wake_up_default_engine"

.field public static final KEY_WAKE_UP_IN_SECURED_LOCK:Ljava/lang/String; = "secure_voice_wake_up"

.field public static final KEY_WAKE_UP_LOCK_SCREEN:Ljava/lang/String; = "wake_up_lock_screen"

.field public static final KEY_WAKE_UP_TIME:Ljava/lang/String; = "wake_up_time"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
