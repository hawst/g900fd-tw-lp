.class public Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;
.super Ljava/lang/Object;
.source "DeleteUserDataCallback.java"

# interfaces
.implements Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;
    }
.end annotation


# instance fields
.field private listener:Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;

.field private final log:Lcom/vlingo/core/internal/logging/Logger;

.field private final mContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private userDataDeletionConfirmServerFailed:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userDataDeletionConfirmServerFailed"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-class v0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 31
    iput-object p2, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->userDataDeletionConfirmServerFailed:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->mContext:Ljava/lang/ref/WeakReference;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userDataDeletionConfirmServerFailed"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-class v0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 37
    iput-object p2, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->userDataDeletionConfirmServerFailed:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->mContext:Ljava/lang/ref/WeakReference;

    .line 39
    iput-object p3, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->listener:Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;

    .line 40
    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;)V
    .locals 4
    .param p1, "failureInfo"    # Ljava/lang/String;

    .prologue
    .line 65
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "DeleteUserDataCallback.onFailure: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->warn(Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 68
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 71
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v2, "DeleteUserDataCallback.onFailure: context is null; returning!"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->warn(Ljava/lang/String;)V

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->listener:Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;

    invoke-interface {v1}, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;->onFailure()V

    goto :goto_0
.end method

.method public onSuccess(Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;)V
    .locals 3
    .param p1, "callback"    # Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;

    .prologue
    .line 44
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 45
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 48
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v2, "DeleteUserDataCallback.onSuccess: context is null; returning!"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->warn(Ljava/lang/String;)V

    .line 56
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v2, "DeleteUserDataCallback.onSuccess: clearing our client data."

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 54
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->deleteLocalApplicationData()V

    .line 55
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->listener:Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;

    invoke-interface {v1, p1}, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;->onSuccess(Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;)V

    goto :goto_0
.end method
