.class Lcom/vlingo/midas/datadeletion/ConfirmDialog$2;
.super Ljava/lang/Object;
.source "ConfirmDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/datadeletion/ConfirmDialog;->showConfirmDialog(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/datadeletion/ConfirmDialog;

.field private final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/datadeletion/ConfirmDialog;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog$2;->this$0:Lcom/vlingo/midas/datadeletion/ConfirmDialog;

    iput-object p2, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog$2;->val$context:Landroid/content/Context;

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog$2;->this$0:Lcom/vlingo/midas/datadeletion/ConfirmDialog;

    # getter for: Lcom/vlingo/midas/datadeletion/ConfirmDialog;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {v0}, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->access$0(Lcom/vlingo/midas/datadeletion/ConfirmDialog;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    const-string/jumbo v1, "positive button clicked"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 59
    invoke-static {}, Lcom/vlingo/core/facade/lmtt/MdsoUtils;->isMasterWithoutActiveSlave()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog$2;->this$0:Lcom/vlingo/midas/datadeletion/ConfirmDialog;

    # getter for: Lcom/vlingo/midas/datadeletion/ConfirmDialog;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {v0}, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->access$0(Lcom/vlingo/midas/datadeletion/ConfirmDialog;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    const-string/jumbo v1, "UDD isSetToMaster so calling deleteAllPersonalData"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 62
    const/4 v0, 0x1

    new-instance v1, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;

    iget-object v2, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog$2;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog$2;->this$0:Lcom/vlingo/midas/datadeletion/ConfirmDialog;

    # getter for: Lcom/vlingo/midas/datadeletion/ConfirmDialog;->serverFailedMessage:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->access$1(Lcom/vlingo/midas/datadeletion/ConfirmDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/VlingoAndroidCore;->deleteAllPersonalData(ZLcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;)V

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog$2;->this$0:Lcom/vlingo/midas/datadeletion/ConfirmDialog;

    # getter for: Lcom/vlingo/midas/datadeletion/ConfirmDialog;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {v0}, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->access$0(Lcom/vlingo/midas/datadeletion/ConfirmDialog;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    const-string/jumbo v1, "UDD DeleteUserDataCallback.onSuccess: clearing our client data."

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 66
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->deleteLocalApplicationData()V

    goto :goto_0
.end method
