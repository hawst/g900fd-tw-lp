.class public Lcom/vlingo/midas/datadeletion/DataDeletionUtil;
.super Ljava/lang/Object;
.source "DataDeletionUtil.java"


# static fields
.field private static final CLEAR_DATA_INTENT:Ljava/lang/String; = "com.vlingo.midas.datadeletion.ClearAppData"

.field private static final CLEAR_DATA_INTENT_SUFFIX:Ljava/lang/String; = ".datadeletion.ClearAppData"

.field private static final CLEAR_DATA_PERMISSION:Ljava/lang/String; = "com.vlingo.midas.permission.Clearappdata"

.field private static final KEY_RELATED_APP_NAMES_FOR_DATA_DELETOING:Ljava/lang/String; = "com.vlingo.midas.datadeletion.RelatedAppNames"

.field private static log:Lcom/vlingo/core/internal/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vlingo/midas/datadeletion/DataDeletionUtil;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/datadeletion/DataDeletionUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 31
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
