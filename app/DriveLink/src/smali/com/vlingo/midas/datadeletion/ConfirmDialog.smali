.class public Lcom/vlingo/midas/datadeletion/ConfirmDialog;
.super Ljava/lang/Object;
.source "ConfirmDialog.java"


# instance fields
.field private final log:Lcom/vlingo/core/internal/logging/Logger;

.field private message:Ljava/lang/String;

.field private negative:Ljava/lang/String;

.field private positive:Ljava/lang/String;

.field private serverFailedMessage:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "alertTitle"    # Ljava/lang/String;
    .param p2, "alertMessage"    # Ljava/lang/String;
    .param p3, "negativeButtonLabel"    # Ljava/lang/String;
    .param p4, "positiveButtonLabel"    # Ljava/lang/String;
    .param p5, "serverFailedMessage"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-class v0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 31
    iput-object p1, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->title:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->message:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->negative:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->positive:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->serverFailedMessage:Ljava/lang/String;

    .line 36
    return-void
.end method

.method static synthetic access$0(Lcom/vlingo/midas/datadeletion/ConfirmDialog;)Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-object v0
.end method

.method static synthetic access$1(Lcom/vlingo/midas/datadeletion/ConfirmDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->serverFailedMessage:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public showConfirmDialog(Landroid/content/Context;)V
    .locals 4
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 39
    move-object v1, p1

    .line 44
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 45
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->title:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 46
    iget-object v2, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->message:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 48
    iget-object v2, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->negative:Ljava/lang/String;

    new-instance v3, Lcom/vlingo/midas/datadeletion/ConfirmDialog$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/datadeletion/ConfirmDialog$1;-><init>(Lcom/vlingo/midas/datadeletion/ConfirmDialog;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 55
    iget-object v2, p0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->positive:Ljava/lang/String;

    new-instance v3, Lcom/vlingo/midas/datadeletion/ConfirmDialog$2;

    invoke-direct {v3, p0, v1}, Lcom/vlingo/midas/datadeletion/ConfirmDialog$2;-><init>(Lcom/vlingo/midas/datadeletion/ConfirmDialog;Landroid/content/Context;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 70
    new-instance v2, Lcom/vlingo/midas/datadeletion/ConfirmDialog$3;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/datadeletion/ConfirmDialog$3;-><init>(Lcom/vlingo/midas/datadeletion/ConfirmDialog;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 76
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 77
    return-void
.end method
