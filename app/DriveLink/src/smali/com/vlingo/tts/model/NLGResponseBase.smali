.class public Lcom/vlingo/tts/model/NLGResponseBase;
.super Ljava/lang/Object;
.source "NLGResponseBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_DisplayForm:Ljava/lang/String; = "DisplayForm"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_SpokenForm:Ljava/lang/String; = "SpokenForm"


# instance fields
.field private DisplayForm:Lcom/vlingo/tts/model/NLGSegment;

.field private ID:J

.field private SpokenForm:Lcom/vlingo/tts/model/NLGSegment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 37
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/tts/model/NLGResponse;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 41
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/tts/model/NLGResponse;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayForm()Lcom/vlingo/tts/model/NLGSegment;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/tts/model/NLGResponseBase;->DisplayForm:Lcom/vlingo/tts/model/NLGSegment;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/vlingo/tts/model/NLGResponseBase;->ID:J

    return-wide v0
.end method

.method public getSpokenForm()Lcom/vlingo/tts/model/NLGSegment;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/tts/model/NLGResponseBase;->SpokenForm:Lcom/vlingo/tts/model/NLGSegment;

    return-object v0
.end method

.method public setDisplayForm(Lcom/vlingo/tts/model/NLGSegment;)V
    .locals 0
    .param p1, "val"    # Lcom/vlingo/tts/model/NLGSegment;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vlingo/tts/model/NLGResponseBase;->DisplayForm:Lcom/vlingo/tts/model/NLGSegment;

    .line 26
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/vlingo/tts/model/NLGResponseBase;->ID:J

    .line 33
    return-void
.end method

.method public setSpokenForm(Lcom/vlingo/tts/model/NLGSegment;)V
    .locals 0
    .param p1, "val"    # Lcom/vlingo/tts/model/NLGSegment;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/vlingo/tts/model/NLGResponseBase;->SpokenForm:Lcom/vlingo/tts/model/NLGSegment;

    .line 19
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
