.class public Lcom/vlingo/voicepad/config/EDMConfig;
.super Ljava/lang/Object;
.source "EDMConfig.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x4894ff2892da260L


# instance fields
.field private final ivDMCLoader:Lcom/vlingo/voicepad/config/DMConfigLoader;

.field private final ivGTAGenerator:Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "config"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;-><init>(Ljava/io/File;Lcom/vlingo/voicepad/config/ResourceManager;)V

    iput-object v0, p0, Lcom/vlingo/voicepad/config/EDMConfig;->ivGTAGenerator:Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

    .line 25
    new-instance v0, Lcom/vlingo/voicepad/config/DMConfigLoader;

    invoke-direct {v0, p1}, Lcom/vlingo/voicepad/config/DMConfigLoader;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/vlingo/voicepad/config/EDMConfig;->ivDMCLoader:Lcom/vlingo/voicepad/config/DMConfigLoader;

    .line 26
    return-void
.end method

.method public static loadPromptsForLanguage(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGenerator;
    .locals 5
    .param p0, "config"    # Ljava/io/File;
    .param p1, "project"    # Ljava/lang/String;
    .param p2, "language"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    const-string/jumbo v3, "_"

    const-string/jumbo v4, "-"

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 33
    invoke-static {p1, p2}, Lcom/vlingo/voicepad/config/EDMConfig;->promptFileForLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 34
    .local v2, "languageFile":Ljava/lang/String;
    new-instance v1, Ljava/util/jar/JarFile;

    invoke-direct {v1, p0}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V

    .line 36
    .local v1, "jarFile":Ljava/util/jar/JarFile;
    :try_start_0
    invoke-virtual {v1, v2}, Ljava/util/jar/JarFile;->getJarEntry(Ljava/lang/String;)Ljava/util/jar/JarEntry;

    move-result-object v3

    if-nez v3, :cond_0

    .line 37
    new-instance v3, Lcom/vlingo/common/util/VLocale;

    invoke-direct {v3, p2}, Lcom/vlingo/common/util/VLocale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/vlingo/common/util/VLocale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "altLanguage":Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/vlingo/voicepad/config/EDMConfig;->promptFileForLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 39
    invoke-virtual {v1, v2}, Ljava/util/jar/JarFile;->getJarEntry(Ljava/lang/String;)Ljava/util/jar/JarEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-nez v3, :cond_0

    .line 40
    const/4 v3, 0x0

    .line 44
    invoke-virtual {v1}, Ljava/util/jar/JarFile;->close()V

    .line 47
    .end local v0    # "altLanguage":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 44
    :cond_0
    invoke-virtual {v1}, Ljava/util/jar/JarFile;->close()V

    .line 47
    invoke-static {p0, v2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->loadFromJar(Ljava/io/File;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;

    move-result-object v3

    goto :goto_0

    .line 44
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Ljava/util/jar/JarFile;->close()V

    throw v3
.end method

.method private static promptFileForLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "project"    # Ljava/lang/String;
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "dm-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .param p1, "ois"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 56
    iget-object v0, p0, Lcom/vlingo/voicepad/config/EDMConfig;->ivGTAGenerator:Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->setResourceManager(Lcom/vlingo/voicepad/config/ResourceManager;)V

    .line 57
    return-void
.end method


# virtual methods
.method public getDMCLoader()Lcom/vlingo/voicepad/config/DMConfigLoader;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/voicepad/config/EDMConfig;->ivDMCLoader:Lcom/vlingo/voicepad/config/DMConfigLoader;

    return-object v0
.end method

.method public getGTAGenerator()Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vlingo/voicepad/config/EDMConfig;->ivGTAGenerator:Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

    return-object v0
.end method
