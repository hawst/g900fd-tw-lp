.class public Lcom/vlingo/voicepad/config/DMConfigLoader;
.super Ljava/lang/Object;
.source "DMConfigLoader.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final DM_CONFIG_REGEX:Ljava/lang/String; = "DMConfig\\..*\\.xml"

.field public static final DM_CONFIG_ROOT_FILE:Ljava/lang/String; = "DMConfig.main.xml"

.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;

.field private static final serialVersionUID:J = -0x1202f09d1db3e34bL


# instance fields
.field private ivConfigManager:Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;

.field private ivVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/vlingo/voicepad/config/DMConfigLoader;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method private constructor <init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;)V
    .locals 2
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string/jumbo v0, "unknown"

    iput-object v0, p0, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivVersion:Ljava/lang/String;

    .line 52
    :try_start_0
    invoke-direct {p0, p1}, Lcom/vlingo/voicepad/config/DMConfigLoader;->init(Lcom/vlingo/mda/cfgloader/ConfigLoader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :try_start_1
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 60
    :goto_0
    return-void

    .line 54
    :catchall_0
    move-exception v0

    .line 55
    :try_start_2
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 58
    :goto_1
    throw v0

    .line 56
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "config"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;

    const-class v1, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-direct {v0, p1, v1}, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lcom/vlingo/voicepad/config/DMConfigLoader;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;)V

    .line 40
    return-void
.end method

.method private init(Lcom/vlingo/mda/cfgloader/ConfigLoader;)V
    .locals 4
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->getVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivVersion:Ljava/lang/String;

    .line 67
    const-string/jumbo v1, "DMConfig.main.xml"

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->matchingPaths(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 68
    new-instance v1, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;

    const-string/jumbo v2, "DMConfig.main.xml"

    invoke-direct {v1, p1, v2}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivConfigManager:Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;

    .line 76
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivConfigManager:Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;

    if-nez v1, :cond_2

    .line 77
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v2, "could not find DMConfig.main.xml or a single DMConfig\\..*\\.xml match"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 70
    :cond_1
    const-string/jumbo v1, "DMConfig\\..*\\.xml"

    invoke-interface {p1, v1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->matchingPaths(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    .line 71
    .local v0, "dmConfigFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    sget-object v1, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " matching DMConfig files"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 72
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 73
    new-instance v2, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, p1, v1}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivConfigManager:Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;

    goto :goto_0

    .line 79
    .end local v0    # "dmConfigFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_2
    sget-object v1, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Initialized DMConfigLoader from configuration version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method public static loadFromDirectory(Ljava/io/File;)Lcom/vlingo/voicepad/config/DMConfigLoader;
    .locals 3
    .param p0, "dir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Lcom/vlingo/voicepad/config/DMConfigLoader;

    new-instance v1, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;

    const-class v2, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Lcom/vlingo/voicepad/config/DMConfigLoader;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;)V

    return-object v0
.end method

.method public static loadFromJar(Ljava/io/File;)Lcom/vlingo/voicepad/config/DMConfigLoader;
    .locals 3
    .param p0, "config"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Lcom/vlingo/voicepad/config/DMConfigLoader;

    new-instance v1, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;

    const-class v2, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Lcom/vlingo/voicepad/config/DMConfigLoader;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;)V

    return-object v0
.end method


# virtual methods
.method public getConfig(Lcom/vlingo/workflow/task/TaskEnv;)Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/workflow/task/TaskEnv",
            "<",
            "Lcom/vlingo/voicepad/vvs/VVEnv;",
            ">;)",
            "Lcom/vlingo/dialog/manager/model/DialogManagerConfig;"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "taskEnv":Lcom/vlingo/workflow/task/TaskEnv;, "Lcom/vlingo/workflow/task/TaskEnv<Lcom/vlingo/voicepad/vvs/VVEnv;>;"
    sget-object v1, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v2, "Getting Config..."

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 84
    iget-object v1, p0, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivConfigManager:Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;

    invoke-virtual {p1}, Lcom/vlingo/workflow/task/TaskEnv;->getRequest()Lcom/vlingo/workflow/model/VRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->findDMConfigForRequest(Lcom/vlingo/workflow/model/VRequest;)Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-result-object v0

    .line 85
    .local v0, "dmc":Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    if-nez v0, :cond_0

    .line 86
    sget-object v1, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "DM Config not found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/message/model/response/GeneralError;->getTaskFailed(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 89
    :cond_0
    return-object v0
.end method

.method public getSingleConfig()Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vlingo/voicepad/config/DMConfigLoader;->ivConfigManager:Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;

    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->findSingleDMConfig()Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-result-object v0

    return-object v0
.end method
