.class public final enum Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;
.super Ljava/lang/Enum;
.source "TagToActionErrorCode.java"

# interfaces
.implements Lcom/vlingo/common/message/ErrorCode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;",
        ">;",
        "Lcom/vlingo/common/message/ErrorCode;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

.field public static final enum BadMeta:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

.field public static final enum InvalidRecognitionReference:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

.field public static final enum InvalidVariableReference:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

.field public static final enum InvalidXML:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

.field public static final enum MissingMeta:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

.field public static final enum NoMatchingFeature:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

.field public static final enum NoMatchingFeatureMap:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;


# instance fields
.field private ivDoc:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 6
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    const-string/jumbo v1, "MissingMeta"

    const-string/jumbo v2, "null"

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->MissingMeta:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    .line 7
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    const-string/jumbo v1, "InvalidXML"

    const-string/jumbo v2, "null"

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->InvalidXML:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    .line 8
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    const-string/jumbo v1, "BadMeta"

    const-string/jumbo v2, "null"

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->BadMeta:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    .line 9
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    const-string/jumbo v1, "InvalidRecognitionReference"

    const-string/jumbo v2, "null"

    invoke-direct {v0, v1, v7, v2}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->InvalidRecognitionReference:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    .line 10
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    const-string/jumbo v1, "InvalidVariableReference"

    const-string/jumbo v2, "null"

    invoke-direct {v0, v1, v8, v2}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->InvalidVariableReference:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    .line 11
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    const-string/jumbo v1, "NoMatchingFeatureMap"

    const/4 v2, 0x5

    const-string/jumbo v3, "null"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->NoMatchingFeatureMap:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    .line 12
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    const-string/jumbo v1, "NoMatchingFeature"

    const/4 v2, 0x6

    const-string/jumbo v3, "null"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->NoMatchingFeature:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    .line 4
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->MissingMeta:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->InvalidXML:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->BadMeta:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->InvalidRecognitionReference:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->InvalidVariableReference:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->NoMatchingFeatureMap:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->NoMatchingFeature:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->$VALUES:[Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "doc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 16
    iput-object p3, p0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->ivDoc:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->$VALUES:[Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    invoke-virtual {v0}, [Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    return-object v0
.end method


# virtual methods
.method public getDoc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->ivDoc:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/vlingo/common/message/MessageCodeType;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/vlingo/common/message/MessageCodeType;->ERROR:Lcom/vlingo/common/message/MessageCodeType;

    return-object v0
.end method
