.class public final enum Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;
.super Ljava/lang/Enum;
.source "VVMessageCode.java"

# interfaces
.implements Lcom/vlingo/common/message/StatusCode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;",
        ">;",
        "Lcom/vlingo/common/message/StatusCode;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;

.field public static final enum SubscriptionCancelled:Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;


# instance fields
.field private ivDoc:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;

    const-string/jumbo v1, "SubscriptionCancelled"

    const-string/jumbo v2, "null"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;->SubscriptionCancelled:Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;

    .line 4
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;

    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;->SubscriptionCancelled:Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;->$VALUES:[Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "doc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput-object p3, p0, Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;->ivDoc:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;->$VALUES:[Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;

    invoke-virtual {v0}, [Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;

    return-object v0
.end method


# virtual methods
.method public getDoc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/VVMessageCode;->ivDoc:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/vlingo/common/message/MessageCodeType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/vlingo/common/message/MessageCodeType;->STATUS:Lcom/vlingo/common/message/MessageCodeType;

    return-object v0
.end method
