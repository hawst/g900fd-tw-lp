.class public Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;
.super Ljava/lang/Object;
.source "FeatureSetBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Extends:Ljava/lang/String; = "Extends"

.field public static final PROP_Features:Ljava/lang/String; = "Features"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Name:Ljava/lang/String; = "Name"


# instance fields
.field private Extends:Ljava/lang/String;

.field private Features:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/Feature;",
            ">;"
        }
    .end annotation
.end field

.field private ID:J

.field private Name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;->Features:Ljava/util/Set;

    .line 15
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 41
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 45
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getExtends()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;->Extends:Ljava/lang/String;

    return-object v0
.end method

.method public getFeatures()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/Feature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;->Features:Ljava/util/Set;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;->ID:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public setExtends(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;->Extends:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;->ID:J

    .line 37
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;->Name:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
