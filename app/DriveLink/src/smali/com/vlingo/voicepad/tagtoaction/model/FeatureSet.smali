.class public Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
.super Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;
.source "FeatureSet.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private final ivParents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSetBase;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->ivParents:Ljava/util/List;

    return-void
.end method

.method private findMatchFromParents(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;Lcom/vlingo/common/message/MNode;Ljava/util/Map;)Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    .locals 10
    .param p1, "responseParseType"    # Ljava/lang/String;
    .param p2, "requestFieldID"    # Ljava/lang/String;
    .param p3, "requestFieldContext"    # Ljava/lang/String;
    .param p4, "license"    # Lcom/vlingo/voicepad/model/LicenseInfo;
    .param p5, "request"    # Lcom/vlingo/workflow/model/VRequest;
    .param p6, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vlingo/voicepad/model/LicenseInfo;",
            "Lcom/vlingo/workflow/model/VRequest;",
            "Lcom/vlingo/common/message/MNode;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/vlingo/voicepad/tagtoaction/model/Feature;"
        }
    .end annotation

    .prologue
    .line 38
    .local p7, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getParents()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    .local v0, "parent":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    .line 39
    invoke-virtual/range {v0 .. v7}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->findFirstMatchingFeature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;Lcom/vlingo/common/message/MNode;Ljava/util/Map;)Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    move-result-object v9

    .line 40
    .local v9, "matchingFeature":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    if-eqz v9, :cond_0

    .line 45
    .end local v0    # "parent":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    .end local v9    # "matchingFeature":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    :goto_0
    return-object v9

    :cond_1
    const/4 v9, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addParent(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;)V
    .locals 1
    .param p1, "parent"    # Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->ivParents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    return-void
.end method

.method public findFirstMatchingFeature(Lcom/vlingo/dialog/goal/model/Goal;)Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    .locals 3
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getFeatures()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    .line 67
    .local v0, "feature":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    invoke-virtual {v0, p1}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->isMatch(Lcom/vlingo/dialog/goal/model/Goal;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    .end local v0    # "feature":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findFirstMatchingFeature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;Lcom/vlingo/common/message/MNode;Ljava/util/Map;)Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    .locals 9
    .param p1, "responseParseType"    # Ljava/lang/String;
    .param p2, "requestFieldID"    # Ljava/lang/String;
    .param p3, "requestFieldContext"    # Ljava/lang/String;
    .param p4, "license"    # Lcom/vlingo/voicepad/model/LicenseInfo;
    .param p5, "request"    # Lcom/vlingo/workflow/model/VRequest;
    .param p6, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vlingo/voicepad/model/LicenseInfo;",
            "Lcom/vlingo/workflow/model/VRequest;",
            "Lcom/vlingo/common/message/MNode;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/vlingo/voicepad/tagtoaction/model/Feature;"
        }
    .end annotation

    .prologue
    .line 50
    .local p7, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "checking FeatureSet \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\' for feature matching parsetype="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " requestFieldID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " requestFieldContext="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 52
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getFeatures()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    .local v0, "feature":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    .line 54
    invoke-virtual/range {v0 .. v7}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->isMatch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;Lcom/vlingo/common/message/MNode;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    .end local v0    # "feature":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    :goto_0
    return-object v0

    :cond_1
    invoke-direct/range {p0 .. p7}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->findMatchFromParents(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;Lcom/vlingo/common/message/MNode;Ljava/util/Map;)Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    move-result-object v0

    goto :goto_0
.end method

.method public getParents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->ivParents:Ljava/util/List;

    return-object v0
.end method
