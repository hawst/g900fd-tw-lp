.class public Lcom/vlingo/voicepad/tagtoaction/GoalToActionJexlFunctions;
.super Ljava/lang/Object;
.source "GoalToActionJexlFunctions.java"


# static fields
.field public static final JEXL_PREFIX:Ljava/lang/String; = "gta"

.field private static final NUMBER_PATTERN:Ljava/util/regex/Pattern;

.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionJexlFunctions;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionJexlFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 30
    const-string/jumbo v0, "\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionJexlFunctions;->NUMBER_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static genAlarmChoicesJSON(Ljava/util/Collection;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/dialog/event/model/Alarm;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 164
    .local p0, "alarms":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/dialog/event/model/Alarm;>;"
    new-instance v2, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    invoke-direct {v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;-><init>()V

    .line 165
    .local v2, "alarmId":Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 166
    .local v1, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Alarm;

    .line 167
    .local v0, "a":Lcom/vlingo/dialog/event/model/Alarm;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 169
    .end local v0    # "a":Lcom/vlingo/dialog/event/model/Alarm;
    :cond_0
    const-string/jumbo v4, "id"

    invoke-virtual {v2, v4, v1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 170
    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static genAppointmentChoicesJSON(Ljava/util/Collection;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/dialog/event/model/Appointment;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 139
    .local p0, "appointments":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/dialog/event/model/Appointment;>;"
    new-instance v2, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    invoke-direct {v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;-><init>()V

    .line 142
    .local v2, "appointmentId":Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 144
    .local v1, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Appointment;

    .line 145
    .local v0, "a":Lcom/vlingo/dialog/event/model/Appointment;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 148
    .end local v0    # "a":Lcom/vlingo/dialog/event/model/Appointment;
    :cond_0
    const-string/jumbo v4, "id"

    invoke-virtual {v2, v4, v1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 150
    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static genAppointmentEditInvitees(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "typeId"    # Ljava/lang/String;

    .prologue
    .line 125
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 126
    :cond_0
    const/4 v1, 0x0

    .line 135
    :goto_0
    return-object v1

    .line 129
    :cond_1
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    invoke-direct {v0}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;-><init>()V

    .line 131
    .local v0, "invitees":Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;
    const-string/jumbo v1, "id"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 133
    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/GoalToActionJexlFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "**** Genning Invitees List: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 135
    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static genAppointmentInvitees(Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 8
    .param p0, "inviteesForm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 102
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v2, "idList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/dialog/model/IForm;

    .line 104
    .local v3, "inviteeForm":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v5, "typeId"

    invoke-interface {v3, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 105
    .local v1, "id":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 106
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    .end local v1    # "id":Ljava/lang/String;
    .end local v3    # "inviteeForm":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_2

    .line 110
    const/4 v5, 0x0

    .line 119
    :goto_1
    return-object v5

    .line 113
    :cond_2
    new-instance v4, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    invoke-direct {v4}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;-><init>()V

    .line 115
    .local v4, "invitees":Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;
    const-string/jumbo v5, "id"

    invoke-virtual {v4, v5, v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 117
    sget-object v5, Lcom/vlingo/voicepad/tagtoaction/GoalToActionJexlFunctions;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "**** Genning Invitees List: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 119
    invoke-virtual {v4}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method public static genContactNameChoicesJSON(Ljava/util/Collection;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/dialog/event/model/Contact;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "contacts":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/dialog/event/model/Contact;>;"
    new-instance v2, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    invoke-direct {v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;-><init>()V

    .line 76
    .local v2, "choices":Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v0, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/event/model/Contact;

    .line 79
    .local v1, "c":Lcom/vlingo/dialog/event/model/Contact;
    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/Contact;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    .end local v1    # "c":Lcom/vlingo/dialog/event/model/Contact;
    :cond_0
    const-string/jumbo v4, "id"

    invoke-virtual {v2, v4, v0}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 83
    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static genContactTypeChoicesJSON(Ljava/util/Collection;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/dialog/event/model/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 87
    .local p0, "addresses":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/dialog/event/model/Address;>;"
    new-instance v2, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    invoke-direct {v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;-><init>()V

    .line 89
    .local v2, "choices":Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v1, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Address;

    .line 92
    .local v0, "a":Lcom/vlingo/dialog/event/model/Address;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Address;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 95
    .end local v0    # "a":Lcom/vlingo/dialog/event/model/Address;
    :cond_0
    const-string/jumbo v4, "id"

    invoke-virtual {v2, v4, v1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 98
    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static genDateTime(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "dateSlot"    # Ljava/lang/String;
    .param p2, "timeSlot"    # Ljava/lang/String;

    .prologue
    .line 182
    invoke-interface {p0, p1}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 183
    .local v0, "date":Ljava/lang/String;
    invoke-interface {p0, p2}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 184
    .local v1, "time":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 185
    :cond_0
    const/4 v2, 0x0

    .line 187
    :goto_0
    return-object v2

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static genDateTimeFromDate(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 174
    if-nez p0, :cond_0

    .line 175
    const/4 v0, 0x0

    .line 177
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " 00:00"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static genRecipientJSON(Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 3
    .param p0, "recipientForm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 62
    invoke-interface {p0}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/model/Form;

    .line 63
    .local v0, "newRecipientForm":Lcom/vlingo/dialog/model/Form;
    const-string/jumbo v2, "0"

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 65
    new-instance v1, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v1}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 66
    .local v1, "recipientListForm":Lcom/vlingo/dialog/model/Form;
    const-string/jumbo v2, "recipient_list"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 67
    invoke-virtual {v1, v0}, Lcom/vlingo/dialog/model/Form;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 69
    invoke-static {v1}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionJexlFunctions;->genRecipientsJSON(Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static genRecipientsJSON(Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 9
    .param p0, "recipientListForm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v1, "contactList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .local v5, "numberList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/dialog/model/IForm;

    .line 37
    .local v6, "recipient":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v6}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "name":Ljava/lang/String;
    sget-object v8, Lcom/vlingo/voicepad/tagtoaction/GoalToActionJexlFunctions;->NUMBER_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 39
    const-string/jumbo v8, "typeId"

    invoke-interface {v6, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "contact":Ljava/lang/String;
    const-string/jumbo v8, "address"

    invoke-interface {v6, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 41
    .local v4, "number":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 42
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 43
    :cond_1
    if-eqz v4, :cond_0

    .line 44
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 49
    .end local v0    # "contact":Ljava/lang/String;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "number":Ljava/lang/String;
    .end local v6    # "recipient":Lcom/vlingo/dialog/model/IForm;
    :cond_2
    new-instance v7, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    invoke-direct {v7}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;-><init>()V

    .line 50
    .local v7, "recipients":Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_3

    .line 51
    const-string/jumbo v8, "contacts"

    invoke-virtual {v7, v8, v1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 53
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_4

    .line 54
    const-string/jumbo v8, "numbers"

    invoke-virtual {v7, v8, v5}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 57
    :cond_4
    invoke-virtual {v7}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public static genTaskChoicesJSON(Ljava/util/Collection;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/dialog/event/model/Task;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 154
    .local p0, "tasks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/dialog/event/model/Task;>;"
    new-instance v3, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    invoke-direct {v3}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;-><init>()V

    .line 155
    .local v3, "taskId":Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v0, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/event/model/Task;

    .line 157
    .local v2, "t":Lcom/vlingo/dialog/event/model/Task;
    invoke-virtual {v2}, Lcom/vlingo/dialog/event/model/Task;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 159
    .end local v2    # "t":Lcom/vlingo/dialog/event/model/Task;
    :cond_0
    const-string/jumbo v4, "id"

    invoke-virtual {v3, v4, v0}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    invoke-virtual {v3}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
