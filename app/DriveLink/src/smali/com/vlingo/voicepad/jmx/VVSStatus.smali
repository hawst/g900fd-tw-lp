.class public final enum Lcom/vlingo/voicepad/jmx/VVSStatus;
.super Ljava/lang/Enum;
.source "VVSStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/voicepad/jmx/VVSStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/voicepad/jmx/VVSStatus;

.field public static final enum RUNNING:Lcom/vlingo/voicepad/jmx/VVSStatus;

.field public static final enum SHUTTING_DOWN:Lcom/vlingo/voicepad/jmx/VVSStatus;

.field public static final enum STARTING:Lcom/vlingo/voicepad/jmx/VVSStatus;

.field public static final enum STARTUP_FAILED:Lcom/vlingo/voicepad/jmx/VVSStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/vlingo/voicepad/jmx/VVSStatus;

    const-string/jumbo v1, "STARTING"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/voicepad/jmx/VVSStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/voicepad/jmx/VVSStatus;->STARTING:Lcom/vlingo/voicepad/jmx/VVSStatus;

    new-instance v0, Lcom/vlingo/voicepad/jmx/VVSStatus;

    const-string/jumbo v1, "SHUTTING_DOWN"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/voicepad/jmx/VVSStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/voicepad/jmx/VVSStatus;->SHUTTING_DOWN:Lcom/vlingo/voicepad/jmx/VVSStatus;

    new-instance v0, Lcom/vlingo/voicepad/jmx/VVSStatus;

    const-string/jumbo v1, "RUNNING"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/voicepad/jmx/VVSStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/voicepad/jmx/VVSStatus;->RUNNING:Lcom/vlingo/voicepad/jmx/VVSStatus;

    new-instance v0, Lcom/vlingo/voicepad/jmx/VVSStatus;

    const-string/jumbo v1, "STARTUP_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/voicepad/jmx/VVSStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/voicepad/jmx/VVSStatus;->STARTUP_FAILED:Lcom/vlingo/voicepad/jmx/VVSStatus;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/voicepad/jmx/VVSStatus;

    sget-object v1, Lcom/vlingo/voicepad/jmx/VVSStatus;->STARTING:Lcom/vlingo/voicepad/jmx/VVSStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/voicepad/jmx/VVSStatus;->SHUTTING_DOWN:Lcom/vlingo/voicepad/jmx/VVSStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/voicepad/jmx/VVSStatus;->RUNNING:Lcom/vlingo/voicepad/jmx/VVSStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/voicepad/jmx/VVSStatus;->STARTUP_FAILED:Lcom/vlingo/voicepad/jmx/VVSStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/voicepad/jmx/VVSStatus;->$VALUES:[Lcom/vlingo/voicepad/jmx/VVSStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method
