.class public Lcom/vlingo/voicepad/workflow/WorkflowManager;
.super Ljava/lang/Object;
.source "WorkflowManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/voicepad/workflow/WorkflowManager$PropertyChangeAdapter;
    }
.end annotation


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private ivChangeListener:Lcom/vlingo/voicepad/workflow/WorkflowManager$PropertyChangeAdapter;

.field private ivFactoryCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/voicepad/workflow/model/Workflow;",
            "Lcom/vlingo/voicepad/workflow/VLTaskFactory;",
            ">;"
        }
    .end annotation
.end field

.field private ivPackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/vlingo/mda/model/PackageMeta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/vlingo/voicepad/workflow/WorkflowManager;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/workflow/WorkflowManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/vlingo/voicepad/workflow/WorkflowManager;->ivFactoryCache:Ljava/util/Map;

    .line 45
    new-instance v1, Lcom/vlingo/voicepad/workflow/WorkflowManager$PropertyChangeAdapter;

    invoke-direct {v1, p0}, Lcom/vlingo/voicepad/workflow/WorkflowManager$PropertyChangeAdapter;-><init>(Lcom/vlingo/voicepad/workflow/WorkflowManager;)V

    iput-object v1, p0, Lcom/vlingo/voicepad/workflow/WorkflowManager;->ivChangeListener:Lcom/vlingo/voicepad/workflow/WorkflowManager$PropertyChangeAdapter;

    .line 47
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/vlingo/voicepad/workflow/WorkflowManager;->ivPackages:Ljava/util/HashSet;

    .line 51
    invoke-static {}, Lcom/vlingo/voicepad/jmx/VVSManager;->getInstance()Lcom/vlingo/voicepad/jmx/VVSManager;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/voicepad/workflow/WorkflowManager;->ivChangeListener:Lcom/vlingo/voicepad/workflow/WorkflowManager$PropertyChangeAdapter;

    invoke-virtual {v1, v2}, Lcom/vlingo/voicepad/jmx/VVSManager;->addPropertyChangeListener(Lcom/bzbyte/event/PropertyChangeListener;)V

    .line 53
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/voicepad/workflow/WorkflowManager;->ivPackages:Ljava/util/HashSet;

    const-class v1, Lcom/vlingo/workflow/model/TaskParam;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/workflow/model/TaskParam;

    invoke-virtual {v1}, Lcom/vlingo/workflow/model/TaskParam;->getClassMeta()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/mda/model/ClassMeta;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error instantiating com.vlingo.workflow.model.TaskParam: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static compareVersions(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p0, "version1"    # Ljava/lang/String;
    .param p1, "version2"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 159
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 173
    :cond_0
    :goto_0
    return v6

    .line 162
    :cond_1
    const-string/jumbo v7, "\\."

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 163
    .local v2, "v1":[Ljava/lang/String;
    const-string/jumbo v7, "\\."

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 165
    .local v3, "v2":[Ljava/lang/String;
    array-length v7, v2

    array-length v8, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 166
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 167
    const/4 v4, 0x0

    .local v4, "vc1":I
    const/4 v5, 0x0

    .line 168
    .local v5, "vc2":I
    array-length v7, v2

    if-ge v0, v7, :cond_2

    aget-object v7, v2, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 169
    :cond_2
    array-length v7, v3

    if-ge v0, v7, :cond_3

    aget-object v7, v3, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 170
    :cond_3
    if-le v4, v5, :cond_4

    const/4 v6, 0x1

    goto :goto_0

    .line 171
    :cond_4
    if-ge v4, v5, :cond_5

    const/4 v6, -0x1

    goto :goto_0

    .line 166
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public finalize()V
    .locals 2

    .prologue
    .line 68
    invoke-static {}, Lcom/vlingo/voicepad/jmx/VVSManager;->getInstance()Lcom/vlingo/voicepad/jmx/VVSManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/voicepad/workflow/WorkflowManager;->ivChangeListener:Lcom/vlingo/voicepad/workflow/WorkflowManager$PropertyChangeAdapter;

    invoke-virtual {v0, v1}, Lcom/vlingo/voicepad/jmx/VVSManager;->removePropertyChangeListener(Lcom/bzbyte/event/PropertyChangeListener;)V

    .line 69
    return-void
.end method
