.class public Lcom/vlingo/voicepad/workflow/model/MinVvsVersionEvaluator;
.super Ljava/lang/Object;
.source "MinVvsVersionEvaluator.java"

# interfaces
.implements Lcom/vlingo/workflow/model/EvaluatorInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final ivLogger:Lorg/apache/log4j/Logger;

.field private static final serialVersionUID:J = -0x442d8fa8c01d322cL


# instance fields
.field private condition:Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/vlingo/voicepad/workflow/model/MinVvsVersionEvaluator;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/workflow/model/MinVvsVersionEvaluator;->ivLogger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/workflow/model/RequestCondition;)V
    .locals 0
    .param p1, "condition"    # Lcom/vlingo/workflow/model/RequestCondition;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    check-cast p1, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;

    .end local p1    # "condition":Lcom/vlingo/workflow/model/RequestCondition;
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/MinVvsVersionEvaluator;->condition:Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;

    .line 25
    return-void
.end method


# virtual methods
.method public evaluate(Ljava/lang/String;Lcom/vlingo/workflow/model/VRequest;)Z
    .locals 4
    .param p1, "servletName"    # Ljava/lang/String;
    .param p2, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    .line 28
    iget-object v1, p0, Lcom/vlingo/voicepad/workflow/model/MinVvsVersionEvaluator;->condition:Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;

    invoke-virtual {v1}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getMinVVSVersion()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 29
    invoke-static {p2}, Lcom/vlingo/voicepad/vvs/VVServletUtil;->getVVSRequestVersion(Lcom/vlingo/workflow/model/VRequest;)Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "vvsVersion":Ljava/lang/String;
    sget-object v1, Lcom/vlingo/voicepad/workflow/model/MinVvsVersionEvaluator;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Evaluating MinVVSVersion: expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/voicepad/workflow/model/MinVvsVersionEvaluator;->condition:Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;

    invoke-virtual {v3}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getMinVVSVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " or higher, found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/log4j/Logger;->trace(Ljava/lang/Object;)V

    .line 31
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/vlingo/voicepad/workflow/model/MinVvsVersionEvaluator;->condition:Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;

    invoke-virtual {v1}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getMinVVSVersion()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/workflow/WorkflowManager;->compareVersions(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_1

    .line 32
    :cond_0
    const/4 v1, 0x0

    .line 34
    .end local v0    # "vvsVersion":Ljava/lang/String;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
