.class public Lcom/vlingo/voicepad/model/LicenseHistory;
.super Ljava/lang/Object;
.source "LicenseHistory.java"


# instance fields
.field private ivCurrent:Lcom/vlingo/voicepad/model/LicenseInfo;

.field private ivPrevious:Lcom/vlingo/voicepad/model/LicenseInfo;


# direct methods
.method public constructor <init>(Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/voicepad/model/LicenseInfo;)V
    .locals 0
    .param p1, "previous"    # Lcom/vlingo/voicepad/model/LicenseInfo;
    .param p2, "current"    # Lcom/vlingo/voicepad/model/LicenseInfo;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivPrevious:Lcom/vlingo/voicepad/model/LicenseInfo;

    .line 24
    iput-object p2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivCurrent:Lcom/vlingo/voicepad/model/LicenseInfo;

    .line 25
    return-void
.end method


# virtual methods
.method public getCurrent()Lcom/vlingo/voicepad/model/LicenseInfo;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivCurrent:Lcom/vlingo/voicepad/model/LicenseInfo;

    return-object v0
.end method

.method public getPrevious()Lcom/vlingo/voicepad/model/LicenseInfo;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivPrevious:Lcom/vlingo/voicepad/model/LicenseInfo;

    return-object v0
.end method

.method public hasLicenseChanged()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 44
    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivPrevious:Lcom/vlingo/voicepad/model/LicenseInfo;

    if-nez v2, :cond_2

    .line 45
    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivCurrent:Lcom/vlingo/voicepad/model/LicenseInfo;

    if-eqz v2, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 45
    goto :goto_0

    .line 47
    :cond_2
    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivCurrent:Lcom/vlingo/voicepad/model/LicenseInfo;

    if-eqz v2, :cond_0

    .line 50
    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivCurrent:Lcom/vlingo/voicepad/model/LicenseInfo;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseClass()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    .line 52
    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivPrevious:Lcom/vlingo/voicepad/model/LicenseInfo;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseClass()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 62
    :cond_3
    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivCurrent:Lcom/vlingo/voicepad/model/LicenseInfo;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseState()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_6

    .line 64
    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivPrevious:Lcom/vlingo/voicepad/model/LicenseInfo;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseState()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    .line 73
    goto :goto_0

    .line 57
    :cond_5
    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivPrevious:Lcom/vlingo/voicepad/model/LicenseInfo;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseClass()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivCurrent:Lcom/vlingo/voicepad/model/LicenseInfo;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseClass()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivPrevious:Lcom/vlingo/voicepad/model/LicenseInfo;

    invoke-virtual {v3}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseClass()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    .line 69
    :cond_6
    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivPrevious:Lcom/vlingo/voicepad/model/LicenseInfo;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseState()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivCurrent:Lcom/vlingo/voicepad/model/LicenseInfo;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseState()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivPrevious:Lcom/vlingo/voicepad/model/LicenseInfo;

    invoke-virtual {v3}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_0
.end method

.method public setCurrent(Lcom/vlingo/voicepad/model/LicenseInfo;)V
    .locals 0
    .param p1, "current"    # Lcom/vlingo/voicepad/model/LicenseInfo;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/vlingo/voicepad/model/LicenseHistory;->ivCurrent:Lcom/vlingo/voicepad/model/LicenseInfo;

    .line 40
    return-void
.end method
