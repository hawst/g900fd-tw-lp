.class public Lcom/vlingo/voicepad/model/LicenseInfo;
.super Ljava/lang/Object;
.source "LicenseInfo.java"


# instance fields
.field private ivFeatureLevel:I

.field private ivLicenseClass:Ljava/lang/String;

.field private ivLicenseID:Ljava/lang/String;

.field private ivLicenseState:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "licenseID"    # Ljava/lang/String;
    .param p2, "licenseClass"    # Ljava/lang/String;
    .param p3, "licenseState"    # Ljava/lang/String;
    .param p4, "featureLevel"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p2, p0, Lcom/vlingo/voicepad/model/LicenseInfo;->ivLicenseClass:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/vlingo/voicepad/model/LicenseInfo;->ivLicenseID:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/vlingo/voicepad/model/LicenseInfo;->ivLicenseState:Ljava/lang/String;

    .line 28
    iput p4, p0, Lcom/vlingo/voicepad/model/LicenseInfo;->ivFeatureLevel:I

    .line 29
    return-void
.end method


# virtual methods
.method public getEffectiveFeatureLevel()I
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/vlingo/voicepad/model/LicenseInfo;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/vlingo/voicepad/model/LicenseInfo;->getFeatureLevel()I

    move-result v0

    .line 83
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEffectiveLicenseClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/vlingo/voicepad/model/LicenseInfo;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseClass()Ljava/lang/String;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "Free"

    goto :goto_0
.end method

.method public getFeatureLevel()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/vlingo/voicepad/model/LicenseInfo;->ivFeatureLevel:I

    return v0
.end method

.method public getLicenseClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/voicepad/model/LicenseInfo;->ivLicenseClass:Ljava/lang/String;

    return-object v0
.end method

.method public getLicenseID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/voicepad/model/LicenseInfo;->ivLicenseID:Ljava/lang/String;

    return-object v0
.end method

.method public getLicenseState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/voicepad/model/LicenseInfo;->ivLicenseState:Ljava/lang/String;

    return-object v0
.end method

.method public isValid()Z
    .locals 2

    .prologue
    .line 33
    sget-object v0, Lcom/vlingo/voicepad/bls/LicenseState;->VALID:Lcom/vlingo/voicepad/bls/LicenseState;

    invoke-virtual {v0}, Lcom/vlingo/voicepad/bls/LicenseState;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/voicepad/model/LicenseInfo;->ivLicenseState:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setFeatureLevel(I)V
    .locals 0
    .param p1, "featureLevel"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/vlingo/voicepad/model/LicenseInfo;->ivFeatureLevel:I

    .line 44
    return-void
.end method
