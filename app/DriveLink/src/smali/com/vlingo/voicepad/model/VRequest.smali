.class public interface abstract Lcom/vlingo/voicepad/model/VRequest;
.super Ljava/lang/Object;
.source "VRequest.java"

# interfaces
.implements Lcom/vlingo/message/util/HeaderProvider;


# static fields
.field public static final HEADER_XVL_CLIENT:Ljava/lang/String; = "x-vlclient"

.field public static final HEADER_XVL_CONFIGURATION:Ljava/lang/String; = "x-vlconfiguration"

.field public static final HEADER_XVL_DM:Ljava/lang/String; = "x-vldm"

.field public static final HEADER_XVL_LOCATION:Ljava/lang/String; = "x-vllocation"

.field public static final HEADER_XVL_PROTOCOL:Ljava/lang/String; = "x-vlprotocol"

.field public static final HEADER_XVL_REQUEST:Ljava/lang/String; = "x-vlrequest"

.field public static final HEADER_XVL_SOFTWARE:Ljava/lang/String; = "x-vlsoftware"

.field public static final HEADER_XVL_SR:Ljava/lang/String; = "x-vlsr"

.field public static final HEADER_XVL_VVS:Ljava/lang/String; = "x-vvs"


# virtual methods
.method public abstract destroy()V
.end method

.method public abstract findCookie(Ljava/lang/String;)Ljavax/servlet/http/Cookie;
.end method

.method public abstract getCookies()[Ljavax/servlet/http/Cookie;
.end method

.method public abstract getHeaderNameList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getHeaderValueList(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getHeaderValues(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getMethod()Ljava/lang/String;
.end method

.method public abstract getRemoteAddress()Ljava/lang/String;
.end method

.method public abstract getRequestId()Ljava/lang/String;
.end method

.method public abstract getRequestURI()Ljava/lang/String;
.end method

.method public abstract getSessionID()Ljava/lang/String;
.end method

.method public abstract newInputStream()Ljava/io/InputStream;
.end method

.method public abstract parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract readBodyAsTree()Lcom/vlingo/common/message/MNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
