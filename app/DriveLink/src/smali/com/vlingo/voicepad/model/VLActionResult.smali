.class public interface abstract Lcom/vlingo/voicepad/model/VLActionResult;
.super Ljava/lang/Object;
.source "VLActionResult.java"

# interfaces
.implements Lcom/vlingo/voicepad/model/VLResult;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/voicepad/model/VLActionResult$ActionType;
    }
.end annotation


# virtual methods
.method public abstract getAction()Lcom/vlingo/voicepad/tagtoaction/model/Action;
.end method

.method public abstract getActionType()Lcom/vlingo/voicepad/model/VLActionResult$ActionType;
.end method

.method public abstract getElseClause()Ljava/lang/String;
.end method

.method public abstract getIfClause()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getParameters()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
