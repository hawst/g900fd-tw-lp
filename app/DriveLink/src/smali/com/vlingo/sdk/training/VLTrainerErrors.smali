.class public final enum Lcom/vlingo/sdk/training/VLTrainerErrors;
.super Ljava/lang/Enum;
.source "VLTrainerErrors.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/training/VLTrainerErrors;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/training/VLTrainerErrors;

.field public static final enum ERROR_NETWORK:Lcom/vlingo/sdk/training/VLTrainerErrors;

.field public static final enum ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/training/VLTrainerErrors;

.field public static final enum ERROR_RATE_LIMIT:Lcom/vlingo/sdk/training/VLTrainerErrors;

.field public static final enum ERROR_SERVER:Lcom/vlingo/sdk/training/VLTrainerErrors;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/vlingo/sdk/training/VLTrainerErrors;

    const-string/jumbo v1, "ERROR_NETWORK_TIMEOUT"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/training/VLTrainerErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/training/VLTrainerErrors;

    .line 16
    new-instance v0, Lcom/vlingo/sdk/training/VLTrainerErrors;

    const-string/jumbo v1, "ERROR_NETWORK"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/training/VLTrainerErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_NETWORK:Lcom/vlingo/sdk/training/VLTrainerErrors;

    .line 19
    new-instance v0, Lcom/vlingo/sdk/training/VLTrainerErrors;

    const-string/jumbo v1, "ERROR_SERVER"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/training/VLTrainerErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_SERVER:Lcom/vlingo/sdk/training/VLTrainerErrors;

    .line 22
    new-instance v0, Lcom/vlingo/sdk/training/VLTrainerErrors;

    const-string/jumbo v1, "ERROR_RATE_LIMIT"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/sdk/training/VLTrainerErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_RATE_LIMIT:Lcom/vlingo/sdk/training/VLTrainerErrors;

    .line 11
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/sdk/training/VLTrainerErrors;

    sget-object v1, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/training/VLTrainerErrors;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_NETWORK:Lcom/vlingo/sdk/training/VLTrainerErrors;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_SERVER:Lcom/vlingo/sdk/training/VLTrainerErrors;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_RATE_LIMIT:Lcom/vlingo/sdk/training/VLTrainerErrors;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/sdk/training/VLTrainerErrors;->$VALUES:[Lcom/vlingo/sdk/training/VLTrainerErrors;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/training/VLTrainerErrors;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/vlingo/sdk/training/VLTrainerErrors;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/training/VLTrainerErrors;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/training/VLTrainerErrors;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/vlingo/sdk/training/VLTrainerErrors;->$VALUES:[Lcom/vlingo/sdk/training/VLTrainerErrors;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/training/VLTrainerErrors;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/training/VLTrainerErrors;

    return-object v0
.end method
