.class public abstract Lcom/vlingo/sdk/training/VLTrainerUpdateList;
.super Ljava/lang/Object;
.source "VLTrainerUpdateList.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createList()Lcom/vlingo/sdk/training/VLTrainerUpdateList;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public abstract addContact(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract addPlaylist(JLjava/lang/String;)V
.end method

.method public abstract addSong(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract removeContact(J)V
.end method

.method public abstract removePlayList(J)V
.end method

.method public abstract removeSong(J)V
.end method
