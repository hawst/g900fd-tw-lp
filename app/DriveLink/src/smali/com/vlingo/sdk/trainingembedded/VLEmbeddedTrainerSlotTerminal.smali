.class public Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;
.super Ljava/lang/Object;
.source "VLEmbeddedTrainerSlotTerminal.java"


# instance fields
.field private final id:J

.field private final orthography:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "orthography"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-wide p1, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;->id:J

    .line 18
    iput-object p3, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;->orthography:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public final getId()J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;->id:J

    return-wide v0
.end method

.method public final getOrthography()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;->orthography:Ljava/lang/String;

    return-object v0
.end method
