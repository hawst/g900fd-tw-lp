.class public final enum Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
.super Ljava/lang/Enum;
.source "VLRecognitionErrors.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/recognition/VLRecognitionErrors;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

.field public static final enum ERROR_AUDIO:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

.field public static final enum ERROR_CLIENT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

.field public static final enum ERROR_INSUFFICENT_PERMISSIONS:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

.field public static final enum ERROR_NETWORK:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

.field public static final enum ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

.field public static final enum ERROR_NO_MATCH:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

.field public static final enum ERROR_RECOGNIZER_BUSY:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

.field public static final enum ERROR_SERVER:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

.field public static final enum ERROR_SPEECH_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v1, "ERROR_NETWORK_TIMEOUT"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 16
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v1, "ERROR_NETWORK"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NETWORK:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 19
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v1, "ERROR_AUDIO"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_AUDIO:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 22
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v1, "ERROR_SERVER"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SERVER:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 25
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v1, "ERROR_CLIENT"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_CLIENT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 28
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v1, "ERROR_SPEECH_TIMEOUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SPEECH_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 31
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v1, "ERROR_NO_MATCH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NO_MATCH:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 34
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v1, "ERROR_RECOGNIZER_BUSY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_RECOGNIZER_BUSY:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 37
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v1, "ERROR_INSUFFICENT_PERMISSIONS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_INSUFFICENT_PERMISSIONS:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 11
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NETWORK:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_AUDIO:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SERVER:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_CLIENT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SPEECH_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NO_MATCH:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_RECOGNIZER_BUSY:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_INSUFFICENT_PERMISSIONS:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->$VALUES:[Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->$VALUES:[Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    return-object v0
.end method
