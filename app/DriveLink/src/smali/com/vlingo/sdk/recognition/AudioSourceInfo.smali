.class public final Lcom/vlingo/sdk/recognition/AudioSourceInfo;
.super Ljava/lang/Object;
.source "AudioSourceInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;,
        Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;
    }
.end annotation


# instance fields
.field private mFilename:Ljava/lang/String;

.field private mFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

.field private mInputStream:Ljava/io/InputStream;

.field private mText:Ljava/lang/String;

.field private mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mFilename:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mInputStream:Ljava/io/InputStream;

    .line 51
    iput-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mText:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .line 53
    iput-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    .line 55
    return-void
.end method

.method public static getDataBufferSource(Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 3
    .param p0, "format"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .prologue
    .line 108
    if-nez p0, :cond_0

    .line 109
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "format parameter cannot be null!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 111
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    invoke-direct {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;-><init>()V

    .line 112
    .local v0, "asi":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    sget-object v1, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;->BUFFER:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    iput-object v1, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    .line 113
    iput-object p0, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .line 114
    return-object v0
.end method

.method public static final getFileSource(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "format"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .prologue
    .line 66
    invoke-static {p0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "filename cannot be null or empty!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 69
    :cond_0
    if-nez p1, :cond_1

    .line 70
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "format parameter cannot be null!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 72
    :cond_1
    new-instance v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    invoke-direct {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;-><init>()V

    .line 73
    .local v0, "asi":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    sget-object v1, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;->FILE:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    iput-object v1, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    .line 74
    iput-object p1, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .line 75
    iput-object p0, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mFilename:Ljava/lang/String;

    .line 76
    return-object v0
.end method

.method public static getStreamSource(Ljava/io/InputStream;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 3
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "format"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .prologue
    .line 88
    if-nez p0, :cond_0

    .line 89
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "inputstream parameter cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 91
    :cond_0
    if-nez p1, :cond_1

    .line 92
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "format parameter cannot be null!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 94
    :cond_1
    new-instance v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    invoke-direct {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;-><init>()V

    .line 95
    .local v0, "asi":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    sget-object v1, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;->STREAM:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    iput-object v1, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    .line 96
    iput-object p0, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mInputStream:Ljava/io/InputStream;

    .line 97
    iput-object p1, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .line 98
    return-object v0
.end method

.method public static final getStringSource(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 3
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-static {p0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "text cannot be null or empty!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 128
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    invoke-direct {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;-><init>()V

    .line 129
    .local v0, "asi":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    sget-object v1, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;->STRING:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    iput-object v1, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    .line 130
    sget-object v1, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PLAIN_TEXT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    iput-object v1, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .line 131
    iput-object p0, v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mText:Ljava/lang/String;

    .line 132
    return-object v0
.end method


# virtual methods
.method public getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getFormat()Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mInputStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    return-object v0
.end method

.method public isAMR()Z
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mFormat:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    sget-object v1, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->AMR:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDataBuffer()Z
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    sget-object v1, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;->BUFFER:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFile()Z
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    sget-object v1, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;->FILE:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStream()Z
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    sget-object v1, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;->STREAM:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isString()Z
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->mType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    sget-object v1, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;->STRING:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
