.class public final Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
.super Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
.source "VLDialogEventFieldGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldGroupName"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 13
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;-><init>(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$1;)V

    return-object v0
.end method
