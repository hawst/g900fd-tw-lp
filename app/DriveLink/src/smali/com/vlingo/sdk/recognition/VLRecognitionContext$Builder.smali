.class public Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
.super Ljava/lang/Object;
.source "VLRecognitionContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/recognition/VLRecognitionContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private appName:Ljava/lang/String;

.field private audioFormatChannelConfig:I

.field private audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

.field private autoEndpointing:Z

.field private autoPunctuation:Z

.field private capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

.field private controlName:Ljava/lang/String;

.field private currentText:Ljava/lang/String;

.field private cursorPosition:I

.field private fieldID:Ljava/lang/String;

.field private language:Ljava/lang/String;

.field private maxAudioTime:I

.field private minVoiceDuration:F

.field private minVoiceLevel:F

.field private nospeechEndpointTimeout:I

.field private profanityFilter:Z

.field private recoMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

.field private screenName:Ljava/lang/String;

.field private silenceThreshold:F

.field private speechEndpointTimeout:I

.field private speexComplexity:I

.field private speexQuality:I

.field private speexVariableBitrate:I

.field private speexVoiceActivityDetection:I

.field private voicePortion:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    const-string/jumbo v0, "en-US"

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->language:Ljava/lang/String;

    .line 94
    const v0, 0x9c40

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->maxAudioTime:I

    .line 95
    iput-boolean v2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoEndpointing:Z

    .line 96
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_SPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speechEndpointTimeout:I

    .line 97
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_NOSPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->nospeechEndpointTimeout:I

    .line 98
    const/high16 v0, 0x41300000    # 11.0f

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->silenceThreshold:F

    .line 99
    const v0, 0x3da3d70a    # 0.08f

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->minVoiceDuration:F

    .line 100
    const v0, 0x3ca3d70a    # 0.02f

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->voicePortion:F

    .line 101
    const/high16 v0, 0x42640000    # 57.0f

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->minVoiceLevel:F

    .line 102
    const/16 v0, 0x8

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexQuality:I

    .line 103
    iput v2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexVariableBitrate:I

    .line 104
    iput v2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexVoiceActivityDetection:I

    .line 105
    const/4 v0, 0x3

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexComplexity:I

    .line 107
    const-string/jumbo v0, "vp_car_main"

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->fieldID:Ljava/lang/String;

    .line 108
    iput-object v1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->appName:Ljava/lang/String;

    .line 109
    iput-object v1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->screenName:Ljava/lang/String;

    .line 110
    iput-object v1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->controlName:Ljava/lang/String;

    .line 111
    iput-boolean v3, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoPunctuation:Z

    .line 112
    sget-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;->DEFAULT:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .line 113
    iput-object v1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->currentText:Ljava/lang/String;

    .line 114
    iput v2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->cursorPosition:I

    .line 115
    iput-boolean v3, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->profanityFilter:Z

    .line 116
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioFormatChannelConfig:I

    .line 117
    sget-object v0, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->recoMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    .line 131
    iput-object v1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 132
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    const-string/jumbo v0, "en-US"

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->language:Ljava/lang/String;

    .line 94
    const v0, 0x9c40

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->maxAudioTime:I

    .line 95
    iput-boolean v1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoEndpointing:Z

    .line 96
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_SPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speechEndpointTimeout:I

    .line 97
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_NOSPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->nospeechEndpointTimeout:I

    .line 98
    const/high16 v0, 0x41300000    # 11.0f

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->silenceThreshold:F

    .line 99
    const v0, 0x3da3d70a    # 0.08f

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->minVoiceDuration:F

    .line 100
    const v0, 0x3ca3d70a    # 0.02f

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->voicePortion:F

    .line 101
    const/high16 v0, 0x42640000    # 57.0f

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->minVoiceLevel:F

    .line 102
    const/16 v0, 0x8

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexQuality:I

    .line 103
    iput v1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexVariableBitrate:I

    .line 104
    iput v1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexVoiceActivityDetection:I

    .line 105
    const/4 v0, 0x3

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexComplexity:I

    .line 107
    const-string/jumbo v0, "vp_car_main"

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->fieldID:Ljava/lang/String;

    .line 108
    iput-object v2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->appName:Ljava/lang/String;

    .line 109
    iput-object v2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->screenName:Ljava/lang/String;

    .line 110
    iput-object v2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->controlName:Ljava/lang/String;

    .line 111
    iput-boolean v3, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoPunctuation:Z

    .line 112
    sget-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;->DEFAULT:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .line 113
    iput-object v2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->currentText:Ljava/lang/String;

    .line 114
    iput v1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->cursorPosition:I

    .line 115
    iput-boolean v3, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->profanityFilter:Z

    .line 116
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioFormatChannelConfig:I

    .line 117
    sget-object v0, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->recoMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    .line 123
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->validateDefined(Ljava/lang/Object;)V

    .line 124
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 125
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->language:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->maxAudioTime:I

    return v0
.end method

.method static synthetic access$1000(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexVariableBitrate:I

    return v0
.end method

.method static synthetic access$1100(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexVoiceActivityDetection:I

    return v0
.end method

.method static synthetic access$1200(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexComplexity:I

    return v0
.end method

.method static synthetic access$1300(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoPunctuation:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->fieldID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->appName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->screenName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->controlName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoEndpointing:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->currentText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->cursorPosition:I

    return v0
.end method

.method static synthetic access$2200(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->profanityFilter:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioFormatChannelConfig:I

    return v0
.end method

.method static synthetic access$2400(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Lcom/vlingo/sdk/recognition/RecognitionMode;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->recoMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speechEndpointTimeout:I

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->nospeechEndpointTimeout:I

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->silenceThreshold:F

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->minVoiceDuration:F

    return v0
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->voicePortion:F

    return v0
.end method

.method static synthetic access$800(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->minVoiceLevel:F

    return v0
.end method

.method static synthetic access$900(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 92
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexQuality:I

    return v0
.end method

.method private validateDefined(Ljava/lang/Object;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 317
    if-nez p1, :cond_0

    .line 318
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Value cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_0
    return-void
.end method

.method private validateIntBoundary(III)V
    .locals 3
    .param p1, "val"    # I
    .param p2, "max"    # I
    .param p3, "min"    # I

    .prologue
    .line 311
    if-gt p1, p2, :cond_0

    if-ge p1, p3, :cond_1

    .line 312
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Value must be between "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_1
    return-void
.end method

.method private validateStringNotEmpty(Ljava/lang/String;)V
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 323
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Value cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :cond_0
    return-void
.end method


# virtual methods
.method public appInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "screenName"    # Ljava/lang/String;
    .param p3, "controlName"    # Ljava/lang/String;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->appName:Ljava/lang/String;

    .line 186
    iput-object p2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->screenName:Ljava/lang/String;

    .line 187
    iput-object p3, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->controlName:Ljava/lang/String;

    .line 188
    return-object p0
.end method

.method public audioFormatChannelConfig(I)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "channelConfig"    # I

    .prologue
    .line 294
    iput p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioFormatChannelConfig:I

    .line 295
    return-object p0
.end method

.method public audioSourceInfo(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "info"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->validateDefined(Ljava/lang/Object;)V

    .line 161
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 162
    return-object p0
.end method

.method public autoEndPointingTimeouts(II)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 2
    .param p1, "withSpeech"    # I
    .param p2, "withoutSpeech"    # I

    .prologue
    .line 210
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->validateIntBoundary(III)V

    .line 212
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v1

    invoke-direct {p0, p2, v0, v1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->validateIntBoundary(III)V

    .line 214
    iput p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speechEndpointTimeout:I

    .line 215
    iput p2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->nospeechEndpointTimeout:I

    .line 216
    return-object p0
.end method

.method public autoEndpointing(Z)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 199
    iput-boolean p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoEndpointing:Z

    .line 200
    return-object p0
.end method

.method public autoPunctuation(Z)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "autoPunc"    # Z

    .prologue
    .line 248
    iput-boolean p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoPunctuation:Z

    .line 249
    return-object p0
.end method

.method public build()Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .locals 1

    .prologue
    .line 307
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)V

    return-object v0
.end method

.method public capitalizationMode(Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "mode"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .prologue
    .line 257
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->validateDefined(Ljava/lang/Object;)V

    .line 258
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .line 259
    return-object p0
.end method

.method public currentText(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 267
    if-eqz p1, :cond_0

    .line 268
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->currentText:Ljava/lang/String;

    .line 270
    :cond_0
    return-object p0
.end method

.method public cursorPosition(I)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 278
    if-ltz p1, :cond_0

    .line 279
    iput p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->cursorPosition:I

    .line 281
    :cond_0
    return-object p0
.end method

.method public fieldID(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "fieldID"    # Ljava/lang/String;

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->validateStringNotEmpty(Ljava/lang/String;)V

    .line 171
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->fieldID:Ljava/lang/String;

    .line 172
    return-object p0
.end method

.method public language(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->validateDefined(Ljava/lang/Object;)V

    .line 139
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->language:Ljava/lang/String;

    .line 140
    return-object p0
.end method

.method public maxAudioTime(I)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 2
    .param p1, "time"    # I

    .prologue
    .line 150
    const v0, 0x9c40

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->validateIntBoundary(III)V

    .line 151
    iput p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->maxAudioTime:I

    .line 152
    return-object p0
.end method

.method public profanityFilter(Z)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "profFilter"    # Z

    .prologue
    .line 289
    iput-boolean p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->profanityFilter:Z

    .line 290
    return-object p0
.end method

.method public recoMode(Lcom/vlingo/sdk/recognition/RecognitionMode;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "mode"    # Lcom/vlingo/sdk/recognition/RecognitionMode;

    .prologue
    .line 299
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->recoMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    .line 300
    return-object p0
.end method

.method public speechDetectorParams(FFFF)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "silenceThreshold"    # F
    .param p2, "minVoiceDuration"    # F
    .param p3, "voicePortion"    # F
    .param p4, "minVoiceLevel"    # F

    .prologue
    .line 228
    iput p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->silenceThreshold:F

    .line 229
    iput p2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->minVoiceDuration:F

    .line 230
    iput p3, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->voicePortion:F

    .line 231
    iput p4, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->minVoiceLevel:F

    .line 232
    return-object p0
.end method

.method public speexParams(IIII)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
    .locals 0
    .param p1, "speexComplexity"    # I
    .param p2, "speexQuality"    # I
    .param p3, "speexVariableBitrate"    # I
    .param p4, "speexVoiceActivityDetection"    # I

    .prologue
    .line 236
    iput p1, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexComplexity:I

    .line 237
    iput p2, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexQuality:I

    .line 238
    iput p3, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexVariableBitrate:I

    .line 239
    iput p4, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexVoiceActivityDetection:I

    .line 240
    return-object p0
.end method
