.class public final Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
.super Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;
.source "VLDialogContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private dialogGUID:Ljava/lang/String;

.field private dialogState:[B

.field private dialogTurnNumber:I

.field private dmHeaderKVPairs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private eventList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;",
            ">;"
        }
    .end annotation
.end field

.field private isFromEDM:Z

.field private username:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;-><init>()V

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dialogTurnNumber:I

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->eventList:Ljava/util/List;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dmHeaderKVPairs:Ljava/util/HashMap;

    .line 49
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)[B
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dialogState:[B

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->username:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dialogGUID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .prologue
    .line 33
    iget v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dialogTurnNumber:I

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->eventList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dmHeaderKVPairs:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->isFromEDM:Z

    return v0
.end method


# virtual methods
.method public addDMHeaderKVPair(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 110
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "name cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dmHeaderKVPairs:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    return-object p0
.end method

.method public addEvent(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .locals 1
    .param p1, "event"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->eventList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    return-object p0
.end method

.method public bridge synthetic build()Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;
    .locals 2

    .prologue
    .line 133
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;-><init>(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$1;)V

    return-object v0
.end method

.method public setDialogGUID(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .locals 0
    .param p1, "guid"    # Ljava/lang/String;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dialogGUID:Ljava/lang/String;

    .line 89
    return-object p0
.end method

.method public setFromEDM(Z)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .locals 0
    .param p1, "fromEDM"    # Z

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->isFromEDM:Z

    .line 126
    return-object p0
.end method

.method public setState([B)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .locals 0
    .param p1, "state"    # [B

    .prologue
    .line 67
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dialogState:[B

    .line 68
    return-object p0
.end method

.method public setTurnNumber(I)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .locals 0
    .param p1, "turn"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dialogTurnNumber:I

    .line 100
    return-object p0
.end method

.method public setUsername(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->username:Ljava/lang/String;

    .line 78
    return-object p0
.end method
