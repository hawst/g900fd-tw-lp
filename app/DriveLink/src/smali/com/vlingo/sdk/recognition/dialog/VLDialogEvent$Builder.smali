.class public Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
.super Ljava/lang/Object;
.source "VLDialogEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private fieldGroupList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;",
            ">;"
        }
    .end annotation
.end field

.field private fieldList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->name:Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->fieldList:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->fieldGroupList:Ljava/util/ArrayList;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->fieldList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->fieldGroupList:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;-><init>(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)V

    return-object v0
.end method

.method public eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "fieldValue"    # Ljava/lang/String;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->fieldList:Ljava/util/ArrayList;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    return-object p0
.end method

.method public eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    .locals 1
    .param p1, "fieldGroup"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->fieldGroupList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    return-object p0
.end method
