.class final enum Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;
.super Ljava/lang/Enum;
.source "VLActionEvaluator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/recognition/VLActionEvaluator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Operator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

.field public static final enum AND:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

.field public static final enum OR:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    const-string/jumbo v1, "OR"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->OR:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    .line 23
    new-instance v0, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    const-string/jumbo v1, "AND"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->AND:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    .line 21
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    sget-object v1, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->OR:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->AND:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->$VALUES:[Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->$VALUES:[Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    return-object v0
.end method
