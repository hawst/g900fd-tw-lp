.class public Lcom/vlingo/sdk/recognition/NBestData;
.super Ljava/lang/Object;
.source "NBestData.java"


# static fields
.field private static final WORD_SEPARATORS:Ljava/lang/String; = ". ,;:!?\n()[]*&@{}/<>_+=|\""


# instance fields
.field private mUttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)V
    .locals 0
    .param p1, "uttResults"    # Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/NBestData;->mUttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 17
    return-void
.end method


# virtual methods
.method public getWordChoices(I)Ljava/util/List;
    .locals 5
    .param p1, "wordIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v4, p0, Lcom/vlingo/sdk/recognition/NBestData;->mUttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-virtual {v4, p1}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getWord(I)Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    move-result-object v2

    .line 26
    .local v2, "nbest":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 27
    .local v3, "nbestRes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, v2, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iChoices:[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;

    .line 28
    .local v0, "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    array-length v4, v0

    if-lez v4, :cond_0

    .line 29
    const/4 v1, 0x0

    .local v1, "k":I
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_0

    .line 30
    aget-object v4, v0, v1

    iget-object v4, v4, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->originalChoice:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 33
    .end local v1    # "k":I
    :cond_0
    return-object v3
.end method

.method public getWordChoices(Ljava/lang/String;II)Ljava/util/List;
    .locals 2
    .param p1, "currentString"    # Ljava/lang/String;
    .param p2, "startSelection"    # I
    .param p3, "endSelection"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "selectedText":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/sdk/recognition/NBestData;->mUttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getNBestForWord(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v1

    return-object v1
.end method

.method public getWords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
