.class public interface abstract Lcom/vlingo/sdk/recognition/VLRecognitionListener;
.super Ljava/lang/Object;
.source "VLRecognitionListener.java"


# virtual methods
.method public abstract onASRRecorderClosed()V
.end method

.method public abstract onASRRecorderOpened()V
.end method

.method public abstract onCancelled()V
.end method

.method public abstract onError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
.end method

.method public abstract onRecoToneStarting(Z)J
.end method

.method public abstract onRecoToneStopped(Z)V
.end method

.method public abstract onRecognitionResults(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V
.end method

.method public abstract onRmsChanged(I)V
.end method

.method public abstract onStateChanged(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
.end method

.method public abstract onWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)V
.end method
