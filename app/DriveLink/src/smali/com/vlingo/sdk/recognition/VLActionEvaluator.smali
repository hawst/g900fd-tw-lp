.class public Lcom/vlingo/sdk/recognition/VLActionEvaluator;
.super Ljava/lang/Object;
.source "VLActionEvaluator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;
    }
.end annotation


# instance fields
.field protected variables:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->variables:Ljava/util/HashMap;

    .line 38
    return-void
.end method

.method private getBooleanVariable(Ljava/lang/String;)Z
    .locals 2
    .param p1, "varName"    # Ljava/lang/String;

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->getVariable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 206
    .local v0, "val":Ljava/lang/String;
    const-string/jumbo v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private getVariable(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "varName"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v1, p0, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->variables:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 195
    .local v0, "val":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 196
    const-string/jumbo v0, ""

    .line 197
    :cond_0
    return-object v0
.end method

.method protected static isBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 190
    const-string/jumbo v0, "true"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "false"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setVariable(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "varName"    # Ljava/lang/String;
    .param p2, "varValue"    # Ljava/lang/String;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->variables:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    return-void
.end method


# virtual methods
.method public evaluateAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 48
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->isConditional()Z

    move-result v7

    if-nez v7, :cond_0

    .line 92
    :goto_0
    return v6

    .line 56
    :cond_0
    sget-object v3, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->OR:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    .line 59
    .local v3, "operator":Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getIfCondition()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "&&"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 60
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getIfCondition()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "&&"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "exprList":[Ljava/lang/String;
    sget-object v3, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->AND:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    .line 68
    :goto_1
    const/4 v4, 0x1

    .line 69
    .local v4, "result":Z
    sget-object v7, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->OR:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    if-ne v3, v7, :cond_1

    .line 70
    const/4 v4, 0x0

    .line 74
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    array-length v7, v0

    if-ge v2, v7, :cond_2

    .line 76
    aget-object v7, v0, v2

    invoke-virtual {p0, v7}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->evaluateExpression(Ljava/lang/String;)Z

    move-result v1

    .line 78
    .local v1, "exprResult":Z
    sget-object v7, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->AND:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    if-ne v3, v7, :cond_7

    .line 79
    if-eqz v4, :cond_6

    if-eqz v1, :cond_6

    move v4, v6

    .line 80
    :goto_3
    if-nez v4, :cond_9

    .line 88
    .end local v1    # "exprResult":Z
    :cond_2
    if-nez v4, :cond_3

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getElseStatement()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getElseStatement()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_3

    .line 89
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getElseStatement()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->evaluateExpression(Ljava/lang/String;)Z

    :cond_3
    move v6, v4

    .line 92
    goto :goto_0

    .line 62
    .end local v0    # "exprList":[Ljava/lang/String;
    .end local v2    # "i":I
    .end local v4    # "result":Z
    :cond_4
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getIfCondition()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "||"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 63
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getIfCondition()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "||"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "exprList":[Ljava/lang/String;
    goto :goto_1

    .line 65
    .end local v0    # "exprList":[Ljava/lang/String;
    :cond_5
    new-array v0, v6, [Ljava/lang/String;

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getIfCondition()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v5

    .restart local v0    # "exprList":[Ljava/lang/String;
    goto :goto_1

    .restart local v1    # "exprResult":Z
    .restart local v2    # "i":I
    .restart local v4    # "result":Z
    :cond_6
    move v4, v5

    .line 79
    goto :goto_3

    .line 81
    :cond_7
    sget-object v7, Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;->OR:Lcom/vlingo/sdk/recognition/VLActionEvaluator$Operator;

    if-ne v3, v7, :cond_9

    .line 82
    if-nez v4, :cond_8

    if-eqz v1, :cond_a

    :cond_8
    move v4, v6

    .line 74
    :cond_9
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_a
    move v4, v5

    .line 82
    goto :goto_4
.end method

.method protected evaluateExpression(Ljava/lang/String;)Z
    .locals 9
    .param p1, "expression"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 101
    const/16 v8, 0x28

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 102
    .local v5, "parmStart":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v4, v8, -0x1

    .line 104
    .local v4, "parmEnd":I
    add-int/lit8 v8, v5, 0x1

    invoke-virtual {p1, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 105
    .local v2, "parameters":Ljava/lang/String;
    invoke-virtual {p1, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 108
    const-string/jumbo v8, ","

    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 109
    const-string/jumbo v8, ","

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 115
    .local v3, "params":[Ljava/lang/String;
    :goto_0
    const/4 v1, 0x0

    .line 116
    .local v1, "invert":Z
    const-string/jumbo v8, "!"

    invoke-virtual {p1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 117
    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 118
    const/4 v1, 0x1

    .line 131
    :cond_0
    invoke-virtual {p0, p1, v3}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->evaluateExpression(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    .line 133
    .local v0, "exprResult":Z
    if-eqz v1, :cond_1

    .line 134
    if-nez v0, :cond_3

    move v0, v6

    .line 140
    :cond_1
    :goto_1
    return v0

    .line 111
    .end local v0    # "exprResult":Z
    .end local v1    # "invert":Z
    .end local v3    # "params":[Ljava/lang/String;
    :cond_2
    new-array v3, v6, [Ljava/lang/String;

    aput-object v2, v3, v7

    .restart local v3    # "params":[Ljava/lang/String;
    goto :goto_0

    .restart local v0    # "exprResult":Z
    .restart local v1    # "invert":Z
    :cond_3
    move v0, v7

    .line 134
    goto :goto_1
.end method

.method protected evaluateExpression(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 12
    .param p1, "expression"    # Ljava/lang/String;
    .param p2, "parameters"    # [Ljava/lang/String;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 144
    const-string/jumbo v9, "is-installed"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 145
    aget-object v3, p2, v8

    .line 146
    .local v3, "packageName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 147
    .local v2, "minVersionCode":I
    array-length v9, p2

    if-le v9, v7, :cond_0

    .line 149
    const/4 v7, 0x1

    :try_start_0
    aget-object v7, p2, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 155
    :cond_0
    invoke-static {v3, v2}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v7

    .line 185
    .end local v2    # "minVersionCode":I
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_1
    :goto_0
    return v7

    .line 150
    .restart local v2    # "minVersionCode":I
    .restart local v3    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .local v1, "ex":Ljava/lang/Exception;
    move v7, v8

    .line 152
    goto :goto_0

    .line 157
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "minVersionCode":I
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_2
    const-string/jumbo v9, "can-handle"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 158
    const-string/jumbo v0, ""

    .line 159
    .local v0, "category":Ljava/lang/String;
    const-string/jumbo v4, ""

    .line 160
    .local v4, "uri":Ljava/lang/String;
    const-string/jumbo v3, ""

    .line 161
    .restart local v3    # "packageName":Ljava/lang/String;
    array-length v9, p2

    if-le v9, v7, :cond_4

    .line 162
    aget-object v0, p2, v7

    .line 163
    array-length v7, p2

    if-le v7, v10, :cond_3

    .line 164
    aget-object v4, p2, v10

    .line 166
    :cond_3
    array-length v7, p2

    if-le v7, v11, :cond_4

    .line 167
    aget-object v3, p2, v11

    .line 170
    :cond_4
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    aget-object v8, p2, v8

    invoke-static {v7, v8, v0, v4, v3}, Lcom/vlingo/sdk/internal/util/PackageUtil;->canHandleIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    goto :goto_0

    .line 172
    .end local v0    # "category":Ljava/lang/String;
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "uri":Ljava/lang/String;
    :cond_5
    const-string/jumbo v9, "is-true"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 173
    aget-object v7, p2, v8

    invoke-direct {p0, v7}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->getBooleanVariable(Ljava/lang/String;)Z

    move-result v7

    goto :goto_0

    .line 174
    :cond_6
    const-string/jumbo v9, "is-false"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 175
    aget-object v9, p2, v8

    invoke-direct {p0, v9}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->getBooleanVariable(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    move v7, v8

    goto :goto_0

    .line 176
    :cond_7
    const-string/jumbo v9, "equals"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 177
    aget-object v5, p2, v8

    .line 178
    .local v5, "varName":Ljava/lang/String;
    aget-object v6, p2, v7

    .line 179
    .local v6, "varVal":Ljava/lang/String;
    invoke-direct {p0, v5}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->getVariable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    goto :goto_0

    .line 180
    .end local v5    # "varName":Ljava/lang/String;
    .end local v6    # "varVal":Ljava/lang/String;
    :cond_8
    const-string/jumbo v9, "set"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 181
    aget-object v8, p2, v8

    aget-object v9, p2, v7

    invoke-direct {p0, v8, v9}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->setVariable(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :cond_9
    const-class v8, Lcom/vlingo/sdk/recognition/VLActionEvaluator;

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Unhandled expression: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
