.class public Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;
.super Ljava/lang/Object;
.source "VLSpeechDetectorContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

.field private minVoiceDuration:F

.field private minVoiceLevel:F

.field private silenceThreshold:F

.field private voicePortion:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget-object v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_16KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    invoke-static {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getDataBufferSource(Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 32
    const/high16 v0, 0x41300000    # 11.0f

    iput v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->silenceThreshold:F

    .line 33
    const v0, 0x3da3d70a    # 0.08f

    iput v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->minVoiceDuration:F

    .line 34
    const v0, 0x3ca3d70a    # 0.02f

    iput v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->voicePortion:F

    .line 35
    const/high16 v0, 0x42640000    # 57.0f

    iput v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->minVoiceLevel:F

    .line 41
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;

    .prologue
    .line 30
    iget v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->silenceThreshold:F

    return v0
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;

    .prologue
    .line 30
    iget v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->minVoiceDuration:F

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;

    .prologue
    .line 30
    iget v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->voicePortion:F

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;

    .prologue
    .line 30
    iget v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->minVoiceLevel:F

    return v0
.end method

.method private validateDefined(Ljava/lang/Object;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 88
    if-nez p1, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Value cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    return-void
.end method


# virtual methods
.method public audioSourceInfo(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;
    .locals 4
    .param p1, "info"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->validateDefined(Ljava/lang/Object;)V

    .line 51
    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getType()Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    move-result-object v1

    .line 52
    .local v1, "type":Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;
    sget-object v2, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;->BUFFER:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    if-eq v1, v2, :cond_0

    .line 53
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Unsupported audio source, supported formats are: SourceType.BUFFER"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getFormat()Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    move-result-object v0

    .line 56
    .local v0, "format":Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;
    sget-object v2, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_16KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_8KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    if-eq v0, v2, :cond_1

    .line 58
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Unsupported audio format format, supported formats are: SourceFormat.PCM_16KHZ_16BIT, SourceFormat.PCM_8KHZ_16BIT"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 60
    :cond_1
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 61
    return-object p0
.end method

.method public build()Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;
    .locals 2

    .prologue
    .line 84
    new-instance v0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;-><init>(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$1;)V

    return-object v0
.end method

.method public speechDetectorParams(FFFF)Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;
    .locals 0
    .param p1, "silenceThreshold"    # F
    .param p2, "minVoiceDuration"    # F
    .param p3, "voicePortion"    # F
    .param p4, "minVoiceLevel"    # F

    .prologue
    .line 73
    iput p1, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->silenceThreshold:F

    .line 74
    iput p2, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->minVoiceDuration:F

    .line 75
    iput p3, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->voicePortion:F

    .line 76
    iput p4, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->minVoiceLevel:F

    .line 77
    return-object p0
.end method
