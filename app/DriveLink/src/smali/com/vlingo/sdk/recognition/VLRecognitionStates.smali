.class public final enum Lcom/vlingo/sdk/recognition/VLRecognitionStates;
.super Ljava/lang/Enum;
.source "VLRecognitionStates.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/recognition/VLRecognitionStates;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/recognition/VLRecognitionStates;

.field public static final enum CONNECTING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

.field public static final enum GETTING_READY:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

.field public static final enum LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

.field public static final enum THINKING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    const-string/jumbo v1, "GETTING_READY"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->GETTING_READY:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .line 16
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    const-string/jumbo v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->CONNECTING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .line 19
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    const-string/jumbo v1, "LISTENING"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .line 22
    new-instance v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    const-string/jumbo v1, "THINKING"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->THINKING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .line 11
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->GETTING_READY:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->CONNECTING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->THINKING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->$VALUES:[Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionStates;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/recognition/VLRecognitionStates;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->$VALUES:[Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/recognition/VLRecognitionStates;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    return-object v0
.end method
