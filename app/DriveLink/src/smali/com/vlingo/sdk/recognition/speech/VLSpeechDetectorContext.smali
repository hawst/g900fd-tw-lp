.class public final Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;
.super Ljava/lang/Object;
.source "VLSpeechDetectorContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$1;,
        Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_MIN_VOICE_DURATION:F = 0.08f

.field public static final DEFAULT_MIN_VOICE_LEVEL:F = 57.0f

.field public static final DEFAULT_SILENCE_THRESHOLD:F = 11.0f

.field public static final DEFAULT_VOICE_PORTION:F = 0.02f


# instance fields
.field private audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

.field private minVoiceDuration:F

.field private minVoiceLevel:F

.field private silenceThreshold:F

.field private voicePortion:F


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    # getter for: Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->access$100(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 103
    # getter for: Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->silenceThreshold:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->access$200(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->silenceThreshold:F

    .line 104
    # getter for: Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->minVoiceDuration:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->access$300(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->minVoiceDuration:F

    .line 105
    # getter for: Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->voicePortion:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->access$400(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->voicePortion:F

    .line 106
    # getter for: Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->minVoiceLevel:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->access$500(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->minVoiceLevel:F

    .line 107
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;
    .param p2, "x1"    # Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$1;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;-><init>(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;)V

    return-void
.end method


# virtual methods
.method public getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    return-object v0
.end method

.method public getMinVoiceDuration()F
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->minVoiceDuration:F

    return v0
.end method

.method public getMinVoiceLevel()F
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->minVoiceLevel:F

    return v0
.end method

.method public getSilenceThreshold()F
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->silenceThreshold:F

    return v0
.end method

.method public getVoicePortion()F
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->voicePortion:F

    return v0
.end method
