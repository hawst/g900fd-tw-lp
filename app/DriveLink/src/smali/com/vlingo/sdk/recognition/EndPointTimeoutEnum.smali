.class public final enum Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;
.super Ljava/lang/Enum;
.source "EndPointTimeoutEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

.field public static final enum DEFAULT_ENDPOINT_SILENCE_SPEECH_MEDIUM:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

.field public static final enum DEFAULT_NOSPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

.field public static final enum DEFAULT_SPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

.field public static final enum ENDPOINT_SILENCE_SPEECH_LONG:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

.field public static final enum ENDPOINT_SILENCE_SPEECH_LONG_LONG:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

.field public static final enum ENDPOINT_SILENCE_SPEECH_MEDIUM_LONG:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

.field public static final enum ENDPOINT_SILENCE_SPEECH_SHORT:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

.field public static final enum MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

.field public static final enum MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    const-string/jumbo v1, "MAX_ENDPOINT_TIMEOUT_MS"

    const/16 v2, 0x2710

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    .line 6
    new-instance v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    const-string/jumbo v1, "MIN_ENDPOINT_TIMEOUT_MS"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    .line 7
    new-instance v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    const-string/jumbo v1, "DEFAULT_SPEECH_ENDPOINT_TIMEOUT_MS"

    const/16 v2, 0x5dc

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_SPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    .line 8
    new-instance v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    const-string/jumbo v1, "DEFAULT_NOSPEECH_ENDPOINT_TIMEOUT_MS"

    const/16 v2, 0x1388

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_NOSPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    .line 9
    new-instance v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    const-string/jumbo v1, "ENDPOINT_SILENCE_SPEECH_SHORT"

    invoke-direct {v0, v1, v7, v3}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->ENDPOINT_SILENCE_SPEECH_SHORT:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    .line 10
    new-instance v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    const-string/jumbo v1, "DEFAULT_ENDPOINT_SILENCE_SPEECH_MEDIUM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_ENDPOINT_SILENCE_SPEECH_MEDIUM:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    .line 11
    new-instance v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    const-string/jumbo v1, "ENDPOINT_SILENCE_SPEECH_MEDIUM_LONG"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v5}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->ENDPOINT_SILENCE_SPEECH_MEDIUM_LONG:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    .line 12
    new-instance v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    const-string/jumbo v1, "ENDPOINT_SILENCE_SPEECH_LONG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v6}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->ENDPOINT_SILENCE_SPEECH_LONG:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    .line 13
    new-instance v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    const-string/jumbo v1, "ENDPOINT_SILENCE_SPEECH_LONG_LONG"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v7}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->ENDPOINT_SILENCE_SPEECH_LONG_LONG:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    .line 4
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_SPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_NOSPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->ENDPOINT_SILENCE_SPEECH_SHORT:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_ENDPOINT_SILENCE_SPEECH_MEDIUM:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->ENDPOINT_SILENCE_SPEECH_MEDIUM_LONG:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->ENDPOINT_SILENCE_SPEECH_LONG:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->ENDPOINT_SILENCE_SPEECH_LONG_LONG:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->$VALUES:[Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 16
    iput p3, p0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->value:I

    .line 17
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->$VALUES:[Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->value:I

    return v0
.end method

.method public setValue(Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;)V
    .locals 0
    .param p1, "newvalue"    # Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    .prologue
    .line 25
    return-void
.end method
