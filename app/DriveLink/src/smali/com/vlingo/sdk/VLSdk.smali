.class public final Lcom/vlingo/sdk/VLSdk;
.super Ljava/lang/Object;
.source "VLSdk.java"


# static fields
.field public static final EMBEDDED_RECOGNIZER_SUPPORTED:Ljava/lang/String; = "false"

.field public static final INCLUDE_LTS_ASSERTS:Ljava/lang/String; = "false"

.field public static final INCLUDE_LTS_ASSERTS_FALSE:Ljava/lang/String; = "false"

.field public static final SENSORY:Ljava/lang/String; = "USE_SENSORY_2.0"

.field private static final USE_SENSORY_2:Ljava/lang/String; = "USE_SENSORY_2.0"

.field public static VERSION:Ljava/lang/String;

.field public static smAssetsExtracted:Z

.field private static smInstance:Lcom/vlingo/sdk/VLSdk;


# instance fields
.field private mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

.field private mDebugSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

.field private mIsInvalid:Z

.field private mLocationOn:Z

.field private mLogServerURI:Ljava/lang/String;

.field private mRecoServerURI:Ljava/lang/String;

.field private mSdkHandler:Landroid/os/Handler;

.field private mTTSServerURI:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-string/jumbo v0, "2.0.1017"

    sput-object v0, Lcom/vlingo/sdk/VLSdk;->VERSION:Ljava/lang/String;

    .line 88
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/sdk/VLSdk;->smAssetsExtracted:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/util/SDKDebugSettings;Z)V
    .locals 21
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "appName"    # Ljava/lang/String;
    .param p4, "appVersion"    # Ljava/lang/String;
    .param p5, "salesCode"    # Ljava/lang/String;
    .param p6, "locationOn"    # Z
    .param p7, "recoServerURI"    # Ljava/lang/String;
    .param p8, "ttsServerURI"    # Ljava/lang/String;
    .param p9, "logServerURI"    # Ljava/lang/String;
    .param p10, "helloServerURI"    # Ljava/lang/String;
    .param p11, "lmttServerURI"    # Ljava/lang/String;
    .param p12, "debugSettings"    # Lcom/vlingo/sdk/util/SDKDebugSettings;
    .param p13, "edmEnabled"    # Z

    .prologue
    .line 254
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/vlingo/sdk/VLSdk;->mIsInvalid:Z

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move/from16 v12, p6

    move-object/from16 v13, p12

    move-object/from16 v14, p5

    move/from16 v15, p13

    .line 256
    invoke-direct/range {v2 .. v15}, Lcom/vlingo/sdk/VLSdk;->updateSettings(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/sdk/util/SDKDebugSettings;Ljava/lang/String;Z)V

    .line 264
    sget-boolean v2, Lcom/vlingo/sdk/VLSdk;->smAssetsExtracted:Z

    if-nez v2, :cond_1

    .line 265
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v17

    .line 266
    .local v17, "editor":Landroid/content/SharedPreferences$Editor;
    const/16 v16, 0x0

    .line 268
    .local v16, "doCommit":Z
    const-string/jumbo v2, "vlsdk_lib"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v19

    .line 269
    .local v19, "libDir":Ljava/io/File;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->isSensory2Using()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 270
    const-string/jumbo v2, "vlsdk_lib_s2.0.zip"

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-static {v2, v0, v1}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractAssetZipIfNeeded(Ljava/lang/String;Ljava/io/File;Landroid/content/SharedPreferences$Editor;)Z

    move-result v2

    or-int v16, v16, v2

    .line 275
    :goto_0
    const-string/jumbo v2, "vlsdk_raw"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v20

    .line 276
    .local v20, "rawDir":Ljava/io/File;
    const-string/jumbo v2, "false"

    const-string/jumbo v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 277
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->isSensory2Using()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 278
    const-string/jumbo v2, "vlsdk_acoustic_raw_s2.0.zip"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-static {v2, v0, v1}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractAssetZipIfNeeded(Ljava/lang/String;Ljava/io/File;Landroid/content/SharedPreferences$Editor;)Z

    move-result v2

    or-int v16, v16, v2

    .line 282
    :goto_1
    const-string/jumbo v2, "vlsdk_lts_raw.zip"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-static {v2, v0, v1}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractAssetZipIfNeeded(Ljava/lang/String;Ljava/io/File;Landroid/content/SharedPreferences$Editor;)Z

    move-result v2

    or-int v16, v16, v2

    .line 284
    :cond_0
    const/4 v2, 0x1

    sput-boolean v2, Lcom/vlingo/sdk/VLSdk;->smAssetsExtracted:Z

    .line 285
    if-eqz v16, :cond_1

    .line 286
    invoke-static/range {v17 .. v17}, Lcom/vlingo/sdk/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 290
    .end local v16    # "doCommit":Z
    .end local v17    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v19    # "libDir":Ljava/io/File;
    .end local v20    # "rawDir":Ljava/io/File;
    :cond_1
    new-instance v18, Landroid/os/HandlerThread;

    const-string/jumbo v2, "SDKWorker"

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 291
    .local v18, "ht":Landroid/os/HandlerThread;
    invoke-virtual/range {v18 .. v18}, Landroid/os/HandlerThread;->start()V

    .line 292
    new-instance v2, Landroid/os/Handler;

    invoke-virtual/range {v18 .. v18}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/sdk/VLSdk;->mSdkHandler:Landroid/os/Handler;

    .line 294
    new-instance v2, Lcom/vlingo/sdk/internal/VLComponentManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/VLSdk;->mSdkHandler:Landroid/os/Handler;

    invoke-direct {v2, v3}, Lcom/vlingo/sdk/internal/VLComponentManager;-><init>(Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    .line 295
    return-void

    .line 272
    .end local v18    # "ht":Landroid/os/HandlerThread;
    .restart local v16    # "doCommit":Z
    .restart local v17    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v19    # "libDir":Ljava/io/File;
    :cond_2
    const-string/jumbo v2, "vlsdk_lib.zip"

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-static {v2, v0, v1}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractAssetZipIfNeeded(Ljava/lang/String;Ljava/io/File;Landroid/content/SharedPreferences$Editor;)Z

    move-result v2

    or-int v16, v16, v2

    goto :goto_0

    .line 280
    .restart local v20    # "rawDir":Ljava/io/File;
    :cond_3
    const-string/jumbo v2, "vlsdk_acoustic_raw.zip"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-static {v2, v0, v1}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractAssetZipIfNeeded(Ljava/lang/String;Ljava/io/File;Landroid/content/SharedPreferences$Editor;)Z

    move-result v2

    or-int v16, v16, v2

    goto :goto_1
.end method

.method public static create(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/util/SDKDebugSettings;Z)Lcom/vlingo/sdk/VLSdk;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "appVersion"    # Ljava/lang/String;
    .param p4, "salesCode"    # Ljava/lang/String;
    .param p5, "locationOn"    # Z
    .param p6, "recoServerURI"    # Ljava/lang/String;
    .param p7, "ttsServerURI"    # Ljava/lang/String;
    .param p8, "logServerURI"    # Ljava/lang/String;
    .param p9, "helloServerURI"    # Ljava/lang/String;
    .param p10, "lmttServerURI"    # Ljava/lang/String;
    .param p11, "debugSettings"    # Lcom/vlingo/sdk/util/SDKDebugSettings;
    .param p12, "edmEnabled"    # Z

    .prologue
    .line 122
    sget-object v1, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    if-eqz v1, :cond_0

    .line 123
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "singleton VLSdk already exists!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 125
    :cond_0
    new-instance v1, Lcom/vlingo/sdk/VLSdk;

    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move/from16 v14, p12

    invoke-direct/range {v1 .. v14}, Lcom/vlingo/sdk/VLSdk;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/util/SDKDebugSettings;Z)V

    sput-object v1, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    .line 127
    sget-object v1, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    iget-object v1, v1, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLComponentManager;->getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    move-result-object v1

    move/from16 v0, p12

    invoke-interface {v1, v0}, Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;->setEnabled(Z)V

    .line 128
    if-eqz p12, :cond_1

    .line 129
    sget-object v1, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    iget-object v1, v1, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLComponentManager;->getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;->backgroundInit()V

    .line 131
    :cond_1
    sget-object v1, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    return-object v1
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 305
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    if-eqz v0, :cond_0

    .line 306
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    invoke-direct {v0}, Lcom/vlingo/sdk/VLSdk;->destroyInternal()V

    .line 308
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    .line 309
    return-void
.end method

.method private static destroyAllExceptComponents()V
    .locals 0

    .prologue
    .line 170
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->destroy()V

    .line 171
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->destroy()V

    .line 172
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->destroy()V

    .line 174
    invoke-static {}, Lcom/vlingo/sdk/internal/audio/AudioDevice;->destroy()V

    .line 175
    invoke-static {}, Lcom/vlingo/sdk/internal/http/HttpManager;->destroy()V

    .line 176
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->destroy()V

    .line 177
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->destroy()V

    .line 178
    invoke-static {}, Lcom/vlingo/sdk/internal/util/TimerSingleton;->destroy()V

    .line 179
    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->destroy()V

    .line 180
    invoke-static {}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->cleanup()V

    .line 181
    invoke-static {}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarFactory;->cleanup()V

    .line 182
    return-void
.end method

.method private destroyInternal()V
    .locals 2

    .prologue
    .line 312
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 314
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    iget-object v0, v0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->destroyAll()V

    .line 316
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->destroyAllExceptComponents()V

    .line 318
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    iget-object v0, v0, Lcom/vlingo/sdk/VLSdk;->mSdkHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 319
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/vlingo/sdk/VLSdk;->mSdkHandler:Landroid/os/Handler;

    .line 321
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/vlingo/sdk/VLSdk;->mIsInvalid:Z

    .line 322
    return-void
.end method

.method public static doesEmbeddedRecognizerSupported()Z
    .locals 1

    .prologue
    .line 542
    const-string/jumbo v0, "false"

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getInstance()Lcom/vlingo/sdk/VLSdk;
    .locals 2

    .prologue
    .line 246
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    if-nez v0, :cond_0

    .line 247
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "VLSdk not initialized!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    return-object v0
.end method

.method public static isSensory2Using()Z
    .locals 2

    .prologue
    .line 504
    const-string/jumbo v0, "USE_SENSORY_2.0"

    const-string/jumbo v1, "USE_SENSORY_2.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static partlyDestroyOnReload()V
    .locals 1

    .prologue
    .line 162
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 164
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    iget-object v0, v0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->destroySettingsDependentComponents()V

    .line 166
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->destroyAllExceptComponents()V

    .line 167
    return-void
.end method

.method public static reload(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/util/SDKDebugSettings;Z)Lcom/vlingo/sdk/VLSdk;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "appVersion"    # Ljava/lang/String;
    .param p4, "salesCode"    # Ljava/lang/String;
    .param p5, "locationOn"    # Z
    .param p6, "recoServerURI"    # Ljava/lang/String;
    .param p7, "ttsServerURI"    # Ljava/lang/String;
    .param p8, "logServerURI"    # Ljava/lang/String;
    .param p9, "helloServerURI"    # Ljava/lang/String;
    .param p10, "lmttServerURI"    # Ljava/lang/String;
    .param p11, "debugSettings"    # Lcom/vlingo/sdk/util/SDKDebugSettings;
    .param p12, "edmEnabled"    # Z

    .prologue
    .line 153
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->partlyDestroyOnReload()V

    .line 155
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move/from16 v10, p5

    move-object/from16 v11, p11

    move-object/from16 v12, p4

    move/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/vlingo/sdk/VLSdk;->updateSettings(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/sdk/util/SDKDebugSettings;Ljava/lang/String;Z)V

    .line 158
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    return-object v0
.end method

.method private updateSettings(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/sdk/util/SDKDebugSettings;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "appName"    # Ljava/lang/String;
    .param p4, "appVersion"    # Ljava/lang/String;
    .param p5, "recoServerURI"    # Ljava/lang/String;
    .param p6, "ttsServerURI"    # Ljava/lang/String;
    .param p7, "logServerURI"    # Ljava/lang/String;
    .param p8, "helloServerURI"    # Ljava/lang/String;
    .param p9, "lmttServerURI"    # Ljava/lang/String;
    .param p10, "locationOn"    # Z
    .param p11, "debugSettings"    # Lcom/vlingo/sdk/util/SDKDebugSettings;
    .param p12, "salesCode"    # Ljava/lang/String;
    .param p13, "edmEnabled"    # Z

    .prologue
    .line 188
    if-nez p1, :cond_0

    .line 189
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_0
    invoke-static {p2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "appId must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_1
    invoke-static {p3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 195
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "appName must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_2
    invoke-static {p4}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 198
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "appVersion must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_3
    invoke-static {p5}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 201
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "recoServerURI must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_4
    invoke-static {p6}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 204
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "ttsServerURI must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_5
    invoke-static {p7}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 207
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "logServerURI must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_6
    invoke-static {p8}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 210
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "helloServerURI must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_7
    invoke-static {p9}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 213
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "lmttServerURI must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_8
    iput-object p5, p0, Lcom/vlingo/sdk/VLSdk;->mRecoServerURI:Ljava/lang/String;

    .line 217
    iput-object p6, p0, Lcom/vlingo/sdk/VLSdk;->mTTSServerURI:Ljava/lang/String;

    .line 218
    iput-object p7, p0, Lcom/vlingo/sdk/VLSdk;->mLogServerURI:Ljava/lang/String;

    .line 219
    iput-boolean p10, p0, Lcom/vlingo/sdk/VLSdk;->mLocationOn:Z

    .line 220
    iput-object p11, p0, Lcom/vlingo/sdk/VLSdk;->mDebugSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    .line 222
    invoke-static {p5}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->setServerName(Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mTTSServerURI:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->setTTSServerName(Ljava/lang/String;)V

    .line 224
    invoke-static {p7}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->setLogServerName(Ljava/lang/String;)V

    .line 225
    invoke-static {p8}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->setHelloServerName(Ljava/lang/String;)V

    .line 226
    invoke-static {p9}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->setLMTTServerName(Ljava/lang/String;)V

    .line 227
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->init(Landroid/content/Context;)V

    .line 228
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->setAppId(Ljava/lang/String;)V

    .line 229
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->setAppName(Ljava/lang/String;)V

    .line 230
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->setAppVersion(Ljava/lang/String;)V

    .line 231
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0, p12}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->setSalesCode(Ljava/lang/String;)V

    .line 233
    const-class v0, Lcom/vlingo/sdk/internal/vlservice/AndroidVLServiceCookieManager;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->setCookieJarManagerImpl(Ljava/lang/Class;)V

    .line 234
    const-class v0, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarFactory;->setCookieJarImpl(Ljava/lang/Class;)V

    .line 235
    return-void
.end method

.method private static validateInstance()V
    .locals 2

    .prologue
    .line 546
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    iget-boolean v0, v0, Lcom/vlingo/sdk/VLSdk;->mIsInvalid:Z

    if-eqz v0, :cond_0

    .line 547
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "VLSdk instance is no longer valid!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 549
    :cond_0
    return-void
.end method


# virtual methods
.method public appAllowsLocationSending()Z
    .locals 1

    .prologue
    .line 490
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 491
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->isChinesePhone()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public doesUseEDM()Z
    .locals 1

    .prologue
    .line 538
    invoke-virtual {p0}, Lcom/vlingo/sdk/VLSdk;->getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 522
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getAppId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 526
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 530
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mDebugSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 512
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 513
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getDeviceID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;
    .locals 2

    .prologue
    .line 364
    const/4 v0, 0x0

    .line 365
    .local v0, "result":Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 366
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    if-eqz v1, :cond_0

    .line 367
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLComponentManager;->getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    move-result-object v0

    .line 370
    :cond_0
    return-object v0
.end method

.method public getEmbeddedTrainer()Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;
    .locals 2

    .prologue
    .line 347
    const/4 v0, 0x0

    .line 348
    .local v0, "result":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 349
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    if-eqz v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLComponentManager;->getEmbeddedTrainer()Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;

    move-result-object v0

    .line 353
    :cond_0
    return-object v0
.end method

.method public getLocationOn()Z
    .locals 1

    .prologue
    .line 481
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 482
    iget-boolean v0, p0, Lcom/vlingo/sdk/VLSdk;->mLocationOn:Z

    return v0
.end method

.method public getLogServerURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 468
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 469
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mLogServerURI:Ljava/lang/String;

    return-object v0
.end method

.method public getRecoServerURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 446
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 447
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mRecoServerURI:Ljava/lang/String;

    return-object v0
.end method

.method public getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;
    .locals 2

    .prologue
    .line 331
    const/4 v0, 0x0

    .line 332
    .local v0, "recognizer":Lcom/vlingo/sdk/recognition/VLRecognizer;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 333
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    if-eqz v1, :cond_0

    .line 334
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLComponentManager;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    .line 337
    :cond_0
    return-object v0
.end method

.method public getSpeakerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 517
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 518
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getSpeakerID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpeechDetector()Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;
    .locals 1

    .prologue
    .line 391
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 392
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getSpeechDetector()Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;

    move-result-object v0

    return-object v0
.end method

.method public getSpotter()Lcom/vlingo/sdk/recognition/spotter/VLSpotter;
    .locals 1

    .prologue
    .line 380
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 381
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getSpotter()Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

    move-result-object v0

    return-object v0
.end method

.method public getTTSServerURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 457
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 458
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mTTSServerURI:Ljava/lang/String;

    return-object v0
.end method

.method public getTextToSpeech()Lcom/vlingo/sdk/tts/VLTextToSpeech;
    .locals 1

    .prologue
    .line 413
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 414
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getTextToSpeech()Lcom/vlingo/sdk/tts/VLTextToSpeech;

    move-result-object v0

    return-object v0
.end method

.method public getTrainer()Lcom/vlingo/sdk/training/VLTrainer;
    .locals 1

    .prologue
    .line 402
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 403
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getTrainer()Lcom/vlingo/sdk/training/VLTrainer;

    move-result-object v0

    return-object v0
.end method

.method public getVLDownloadService()Lcom/vlingo/sdk/filedownload/VLDownloadService;
    .locals 1

    .prologue
    .line 435
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 436
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getDownloadService()Lcom/vlingo/sdk/filedownload/VLDownloadService;

    move-result-object v0

    return-object v0
.end method

.method public getVLServices()Lcom/vlingo/sdk/services/VLServices;
    .locals 1

    .prologue
    .line 424
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 425
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getVLServices()Lcom/vlingo/sdk/services/VLServices;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 500
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->VERSION:Ljava/lang/String;

    return-object v0
.end method
