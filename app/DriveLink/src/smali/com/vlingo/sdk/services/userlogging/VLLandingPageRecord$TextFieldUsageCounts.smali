.class public Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;
.super Ljava/lang/Object;
.source "VLLandingPageRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TextFieldUsageCounts"
.end annotation


# instance fields
.field private fieldID:Ljava/lang/String;

.field private usageCounts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 0
    .param p1, "fieldID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 311
    .local p2, "counts":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312
    iput-object p1, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->fieldID:Ljava/lang/String;

    .line 313
    iput-object p2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->usageCounts:Ljava/util/HashMap;

    .line 314
    return-void
.end method

.method private getCount(Ljava/lang/String;)I
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 345
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->usageCounts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 348
    :goto_0
    return v0

    .line 347
    :catch_0
    move-exception v0

    .line 348
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public getCountClears()I
    .locals 1

    .prologue
    .line 325
    const-string/jumbo v0, "CLR"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCount(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getCountDeletes()I
    .locals 2

    .prologue
    .line 331
    const-string/jumbo v0, "CDEL"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCount(Ljava/lang/String;)I

    move-result v0

    const-string/jumbo v1, "WDEL"

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCount(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getCountFixAccept()I
    .locals 1

    .prologue
    .line 322
    const-string/jumbo v0, "NBCOR"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCount(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getCountFixInvoke()I
    .locals 1

    .prologue
    .line 319
    const/4 v0, -0x1

    return v0
.end method

.method public getCountKeys()I
    .locals 2

    .prologue
    .line 328
    const-string/jumbo v0, "KEY"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCount(Ljava/lang/String;)I

    move-result v0

    const-string/jumbo v1, "NAV"

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCount(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getCountRecognitions()I
    .locals 2

    .prologue
    .line 334
    const-string/jumbo v0, "CREC"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCount(Ljava/lang/String;)I

    move-result v0

    const-string/jumbo v1, "WREC"

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCount(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getFieldID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->fieldID:Ljava/lang/String;

    return-object v0
.end method

.method public isEdited()Z
    .locals 1

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountKeys()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountDeletes()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountClears()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountFixAccept()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountRecognitions()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
