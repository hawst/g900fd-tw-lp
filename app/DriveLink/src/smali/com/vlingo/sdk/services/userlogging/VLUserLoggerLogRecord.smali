.class public final Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;
.super Ljava/lang/Object;
.source "VLUserLoggerLogRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$1;,
        Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;
    }
.end annotation


# instance fields
.field private errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;",
            ">;"
        }
    .end annotation
.end field

.field private helpPages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;",
            ">;"
        }
    .end annotation
.end field

.field private landingPages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;",
            ">;"
        }
    .end annotation
.end field

.field private settings:Ljava/lang/String;

.field private setupFinished:Z

.field private setupStarted:Z


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-boolean v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->setupStarted:Z

    .line 72
    iput-boolean v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->setupFinished:Z

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->settings:Ljava/lang/String;

    .line 79
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->setupStarted:Z
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->access$100(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->setupStarted:Z

    .line 80
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->setupFinished:Z
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->access$200(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->setupFinished:Z

    .line 81
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->settings:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->access$300(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->settings:Ljava/lang/String;

    .line 83
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->helpPages:Ljava/util/List;

    .line 84
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->helpPages:Ljava/util/List;

    # getter for: Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->helpPages:Ljava/util/List;
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->access$400(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 85
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->errors:Ljava/util/List;

    .line 86
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->errors:Ljava/util/List;

    # getter for: Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->errors:Ljava/util/List;
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->access$500(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 87
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->landingPages:Ljava/util/List;

    .line 88
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->landingPages:Ljava/util/List;

    # getter for: Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->landingPages:Ljava/util/List;
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->access$600(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 89
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;
    .param p2, "x1"    # Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;-><init>(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)V

    return-void
.end method


# virtual methods
.method public getXML()Ljava/lang/String;
    .locals 7

    .prologue
    .line 92
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    .local v4, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "<user-log>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "<user-id>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getDeviceID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "</user-id>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "<setup started=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->setupStarted:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\" finished=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->setupFinished:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\"/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget-object v5, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->settings:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 99
    const-string/jumbo v5, "<settings values=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    iget-object v5, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->settings:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    const-string/jumbo v5, "\"/>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    :cond_0
    iget-object v5, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->helpPages:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;

    .line 105
    .local v1, "helpPage":Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;
    invoke-virtual {v1}, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;->generateXml()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 108
    .end local v1    # "helpPage":Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;
    :cond_1
    iget-object v5, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->errors:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;

    .line 109
    .local v0, "error":Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;->generateXml()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 112
    .end local v0    # "error":Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;
    :cond_2
    const-string/jumbo v5, "<landing-pages>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    iget-object v5, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->landingPages:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;

    .line 114
    .local v3, "page":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;
    invoke-virtual {v3}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->generateXml()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 116
    .end local v3    # "page":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;
    :cond_3
    const-string/jumbo v5, "</landing-pages>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string/jumbo v5, "</user-log>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
