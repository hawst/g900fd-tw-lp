.class public final enum Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;
.super Ljava/lang/Enum;
.source "VLTextToSpeechRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

.field public static final enum EMAIL:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

.field public static final enum MMS:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

.field public static final enum PLAIN:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

.field public static final enum SMS:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    const-string/jumbo v1, "PLAIN"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->PLAIN:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    .line 31
    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    const-string/jumbo v1, "EMAIL"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->EMAIL:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    .line 33
    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    const-string/jumbo v1, "SMS"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->SMS:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    .line 35
    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    const-string/jumbo v1, "MMS"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->MMS:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    .line 27
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->PLAIN:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->EMAIL:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->SMS:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->MMS:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->$VALUES:[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->$VALUES:[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    return-object v0
.end method
