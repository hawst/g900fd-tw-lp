.class public Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
.super Ljava/lang/Object;
.source "VLTextToSpeechRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private language:Ljava/lang/String;

.field private maxWords:I

.field private msgFrom:Ljava/lang/String;

.field private msgReadBody:Z

.field private msgSubject:Ljava/lang/String;

.field private speechRate:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

.field private text:Ljava/lang/String;

.field private type:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

.field private voice:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-string/jumbo v0, "en-US"

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->language:Ljava/lang/String;

    .line 50
    const-string/jumbo v0, "This is an example of vlingo TTS"

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->text:Ljava/lang/String;

    .line 51
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->DEFAULT_VOICE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->voice:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    .line 52
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->DEFAULT_TYPE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->type:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    .line 53
    const/16 v0, 0x1e

    iput v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->maxWords:I

    .line 54
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->DEFAULT_SPEECH_RATE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->speechRate:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    .line 55
    iput-object v1, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgFrom:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgSubject:Ljava/lang/String;

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgReadBody:Z

    .line 59
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->language:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->text:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->voice:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->type:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .prologue
    .line 48
    iget v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->maxWords:I

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->speechRate:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgFrom:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgSubject:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgReadBody:Z

    return v0
.end method


# virtual methods
.method public build()Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
    .locals 2

    .prologue
    .line 93
    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;-><init>(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$1;)V

    return-object v0
.end method

.method public language(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
    .locals 0
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->language:Ljava/lang/String;

    .line 62
    return-object p0
.end method

.method public maxWords(I)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
    .locals 0
    .param p1, "maxWords"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->maxWords:I

    .line 78
    return-object p0
.end method

.method public msgReadyBody(Z)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
    .locals 0
    .param p1, "msgReadyBody"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgReadBody:Z

    .line 90
    return-object p0
.end method

.method public msgSubject(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
    .locals 0
    .param p1, "msgSubject"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgSubject:Ljava/lang/String;

    .line 86
    return-object p0
.end method

.method public speechRate(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
    .locals 0
    .param p1, "rate"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->speechRate:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    .line 82
    return-object p0
.end method

.method public text(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->text:Ljava/lang/String;

    .line 66
    return-object p0
.end method

.method public type(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
    .locals 0
    .param p1, "type"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->type:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    .line 74
    return-object p0
.end method

.method public voice(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
    .locals 0
    .param p1, "voice"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->voice:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    .line 70
    return-object p0
.end method
