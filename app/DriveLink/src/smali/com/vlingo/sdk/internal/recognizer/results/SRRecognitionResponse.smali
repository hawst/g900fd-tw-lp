.class public Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
.super Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
.source "SRRecognitionResponse.java"


# instance fields
.field private guttid:Ljava/lang/String;

.field private results:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->results:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    .line 25
    return-void
.end method


# virtual methods
.method public getGUttId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->guttid:Ljava/lang/String;

    return-object v0
.end method

.method public getResults()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->results:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    return-object v0
.end method

.method public hasResults()Z
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->results:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->results:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGUttId(Ljava/lang/String;)V
    .locals 0
    .param p1, "guttid"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->guttid:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setTaggedResults(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)V
    .locals 0
    .param p1, "results"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->results:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    .line 37
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 49
    .local v0, "buff":Ljava/lang/StringBuffer;
    invoke-super {p0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 50
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->results:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->results:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 55
    :goto_0
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 56
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 53
    :cond_0
    const-string/jumbo v1, "<no results>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
