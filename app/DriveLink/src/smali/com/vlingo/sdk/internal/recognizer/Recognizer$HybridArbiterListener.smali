.class Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;
.super Ljava/lang/Object;
.source "Recognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/Recognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HybridArbiterListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V
    .locals 0

    .prologue
    .line 549
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;

    .prologue
    .line 549
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;-><init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V

    return-void
.end method


# virtual methods
.method public onNetworkError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V
    .locals 2
    .param p1, "recError"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 565
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    invoke-static {v0, p1, p2, v1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    .line 566
    return-void
.end method

.method public onNetworkResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 1
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 559
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1200(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 560
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->cleanup()V
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1300(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V

    .line 561
    return-void
.end method

.method public onVoconResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 1
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 553
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1200(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 554
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->cleanup()V
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1300(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V

    .line 555
    return-void
.end method
