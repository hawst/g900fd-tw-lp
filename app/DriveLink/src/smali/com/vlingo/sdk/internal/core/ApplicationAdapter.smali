.class public Lcom/vlingo/sdk/internal/core/ApplicationAdapter;
.super Ljava/lang/Object;
.source "ApplicationAdapter.java"


# static fields
.field protected static instance:Lcom/vlingo/sdk/internal/core/ApplicationAdapter;


# instance fields
.field contextApp:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->instance:Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->contextApp:Landroid/content/Context;

    return-void
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->instance:Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    .line 47
    return-void
.end method

.method public static getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->instance:Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->instance:Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    .line 42
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->instance:Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    return-object v0
.end method


# virtual methods
.method public debugErrorLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "errorCode"    # Ljava/lang/String;
    .param p2, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 63
    return-void
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->contextApp:Landroid/content/Context;

    return-object v0
.end method

.method public getConnectionTestFieldID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method public getConnectionTestServerDetails()Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->contextApp:Landroid/content/Context;

    .line 51
    return-void
.end method

.method public isAudioStreamingEnabled()Z
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public showFatalDialog(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 72
    return-void
.end method
