.class public abstract Lcom/vlingo/sdk/internal/util/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# static fields
.field private static final UNZIP_BUFFER_SIZE:I = 0x100

.field private static lastUpdateTime:J

.field private static final log:Lcom/vlingo/sdk/internal/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lcom/vlingo/sdk/internal/util/FileUtils;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/sdk/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/util/FileUtils;->log:Lcom/vlingo/sdk/internal/logging/Logger;

    .line 35
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/vlingo/sdk/internal/util/FileUtils;->lastUpdateTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertHashToString([B)Ljava/lang/String;
    .locals 8
    .param p0, "bytes"    # [B

    .prologue
    .line 292
    const-string/jumbo v4, ""

    .line 293
    .local v4, "returnVal":Ljava/lang/String;
    move-object v0, p0

    .local v0, "arr$":[B
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-byte v1, v0, v2

    .line 294
    .local v1, "b":B
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    and-int/lit16 v6, v1, 0xff

    add-int/lit16 v6, v6, 0x100

    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 293
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 296
    .end local v1    # "b":B
    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 10
    .param p0, "srcFile"    # Ljava/io/File;
    .param p1, "dstFile"    # Ljava/io/File;

    .prologue
    const/4 v6, 0x0

    .line 178
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 179
    .local v3, "in":Ljava/io/InputStream;
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 180
    .local v5, "out":Ljava/io/OutputStream;
    const/16 v7, 0x400

    new-array v0, v7, [B

    .line 182
    .local v0, "buf":[B
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .local v4, "len":I
    if-lez v4, :cond_0

    .line 183
    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 189
    .end local v0    # "buf":[B
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v4    # "len":I
    .end local v5    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v2

    .line 190
    .local v2, "ex":Ljava/io/FileNotFoundException;
    const-string/jumbo v7, "FileUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "FileNotFound: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    .end local v2    # "ex":Ljava/io/FileNotFoundException;
    :goto_1
    return v6

    .line 185
    .restart local v0    # "buf":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "len":I
    .restart local v5    # "out":Ljava/io/OutputStream;
    :cond_0
    :try_start_1
    invoke-virtual {v5}, Ljava/io/OutputStream;->flush()V

    .line 186
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 187
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 188
    const/4 v6, 0x1

    goto :goto_1

    .line 193
    .end local v0    # "buf":[B
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v4    # "len":I
    .end local v5    # "out":Ljava/io/OutputStream;
    :catch_1
    move-exception v1

    .line 194
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v7, "FileUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static extractAssetZip(Ljava/lang/String;Ljava/io/File;)V
    .locals 13
    .param p0, "zipFileName"    # Ljava/lang/String;
    .param p1, "destDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 109
    .local v1, "context":Landroid/content/Context;
    const/4 v8, 0x0

    .line 111
    .local v8, "zip":Ljava/util/zip/ZipInputStream;
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v10

    invoke-virtual {v10, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 118
    .local v4, "is":Ljava/io/InputStream;
    :try_start_1
    new-instance v9, Ljava/util/zip/ZipInputStream;

    invoke-direct {v9, v4}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 122
    .end local v8    # "zip":Ljava/util/zip/ZipInputStream;
    .local v9, "zip":Ljava/util/zip/ZipInputStream;
    const/16 v10, 0x100

    :try_start_2
    new-array v0, v10, [B

    .line 123
    .local v0, "bytes":[B
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v7

    .local v7, "ze":Ljava/util/zip/ZipEntry;
    :goto_0
    if-eqz v7, :cond_4

    .line 125
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 127
    .local v6, "path":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 128
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    .line 130
    .local v5, "parent":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    .line 131
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v10

    if-nez v10, :cond_2

    .line 132
    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Unable to create folder "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 143
    .end local v0    # "bytes":[B
    .end local v5    # "parent":Ljava/io/File;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "ze":Ljava/util/zip/ZipEntry;
    :catchall_0
    move-exception v10

    move-object v8, v9

    .end local v9    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v8    # "zip":Ljava/util/zip/ZipInputStream;
    :goto_1
    if-eqz v8, :cond_0

    .line 145
    :try_start_3
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 150
    :cond_0
    :goto_2
    if-eqz v4, :cond_1

    .line 152
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 143
    :cond_1
    :goto_3
    throw v10

    .line 112
    .end local v4    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v3

    .line 158
    :goto_4
    return-void

    .line 136
    .end local v8    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "bytes":[B
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v6    # "path":Ljava/lang/String;
    .restart local v7    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v9    # "zip":Ljava/util/zip/ZipInputStream;
    :cond_2
    :try_start_5
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_3

    .line 137
    invoke-static {v9, v6, v0}, Lcom/vlingo/sdk/internal/util/FileUtils;->getFileFromZip(Ljava/util/zip/ZipInputStream;Ljava/lang/String;[B)V

    .line 123
    :goto_5
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v7

    goto :goto_0

    .line 139
    :cond_3
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_5

    .line 143
    .end local v6    # "path":Ljava/lang/String;
    :cond_4
    if-eqz v9, :cond_5

    .line 145
    :try_start_6
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 150
    :cond_5
    :goto_6
    if-eqz v4, :cond_6

    .line 152
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_6
    :goto_7
    move-object v8, v9

    .line 158
    .end local v9    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v8    # "zip":Ljava/util/zip/ZipInputStream;
    goto :goto_4

    .line 146
    .end local v0    # "bytes":[B
    .end local v7    # "ze":Ljava/util/zip/ZipEntry;
    :catch_1
    move-exception v2

    .line 147
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 153
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 154
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 146
    .end local v2    # "e":Ljava/io/IOException;
    .end local v8    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "bytes":[B
    .restart local v7    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v9    # "zip":Ljava/util/zip/ZipInputStream;
    :catch_3
    move-exception v2

    .line 147
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 153
    .end local v2    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v2

    .line 154
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 143
    .end local v0    # "bytes":[B
    .end local v2    # "e":Ljava/io/IOException;
    .end local v7    # "ze":Ljava/util/zip/ZipEntry;
    .end local v9    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v8    # "zip":Ljava/util/zip/ZipInputStream;
    :catchall_1
    move-exception v10

    goto :goto_1
.end method

.method public static extractAssetZipIfNeeded(Ljava/lang/String;Ljava/io/File;Landroid/content/SharedPreferences$Editor;)Z
    .locals 11
    .param p0, "zipFileName"    # Ljava/lang/String;
    .param p1, "destDir"    # Ljava/io/File;
    .param p2, "editor"    # Landroid/content/SharedPreferences$Editor;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    const/4 v5, 0x0

    const-wide/16 v9, 0x0

    .line 39
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 41
    .local v0, "context":Landroid/content/Context;
    sget-wide v6, Lcom/vlingo/sdk/internal/util/FileUtils;->lastUpdateTime:J

    cmp-long v6, v6, v9

    if-nez v6, :cond_0

    .line 43
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-wide v6, v6, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    sput-wide v6, Lcom/vlingo/sdk/internal/util/FileUtils;->lastUpdateTime:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :cond_0
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "zip_timestamp_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "settingsName":Ljava/lang/String;
    invoke-static {v2, v9, v10}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentLong(Ljava/lang/String;J)J

    move-result-wide v3

    .line 51
    .local v3, "timestamp":J
    cmp-long v6, v3, v9

    if-eqz v6, :cond_1

    sget-wide v6, Lcom/vlingo/sdk/internal/util/FileUtils;->lastUpdateTime:J

    cmp-long v6, v6, v3

    if-eqz v6, :cond_2

    .line 53
    :cond_1
    :try_start_1
    invoke-static {p0, p1}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractAssetZip(Ljava/lang/String;Ljava/io/File;)V

    .line 54
    sget-wide v6, Lcom/vlingo/sdk/internal/util/FileUtils;->lastUpdateTime:J

    invoke-interface {p2, v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 55
    const/4 v5, 0x1

    .line 62
    :cond_2
    :goto_1
    return v5

    .line 44
    .end local v2    # "settingsName":Ljava/lang/String;
    .end local v3    # "timestamp":J
    :catch_0
    move-exception v1

    .line 45
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 56
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "settingsName":Ljava/lang/String;
    .restart local v3    # "timestamp":J
    :catch_1
    move-exception v1

    .line 59
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static extractFileFromAssetZip(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    .locals 10
    .param p0, "zipFileName"    # Ljava/lang/String;
    .param p1, "destDir"    # Ljava/io/File;
    .param p2, "absolutePathName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 68
    .local v1, "context":Landroid/content/Context;
    const/4 v3, 0x0

    .line 69
    .local v3, "is":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 71
    .local v6, "zip":Ljava/util/zip/ZipInputStream;
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v8

    invoke-virtual {v8, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 72
    new-instance v7, Ljava/util/zip/ZipInputStream;

    invoke-direct {v7, v3}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    .end local v6    # "zip":Ljava/util/zip/ZipInputStream;
    .local v7, "zip":Ljava/util/zip/ZipInputStream;
    const/16 v8, 0x100

    :try_start_1
    new-array v0, v8, [B

    .line 77
    .local v0, "bytes":[B
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v5

    .local v5, "ze":Ljava/util/zip/ZipEntry;
    :goto_0
    if-eqz v5, :cond_3

    .line 79
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 80
    .local v4, "path":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v8

    if-nez v8, :cond_0

    .line 81
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 82
    invoke-static {v7, v4, v0}, Lcom/vlingo/sdk/internal/util/FileUtils;->getFileFromZip(Ljava/util/zip/ZipInputStream;Ljava/lang/String;[B)V

    .line 77
    :cond_0
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    goto :goto_0

    .line 87
    .end local v0    # "bytes":[B
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zip":Ljava/util/zip/ZipInputStream;
    :catchall_0
    move-exception v8

    :goto_1
    if-eqz v6, :cond_1

    .line 89
    :try_start_2
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 94
    :cond_1
    :goto_2
    if-eqz v3, :cond_2

    .line 96
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 87
    :cond_2
    :goto_3
    throw v8

    .end local v6    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "bytes":[B
    .restart local v5    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v7    # "zip":Ljava/util/zip/ZipInputStream;
    :cond_3
    if-eqz v7, :cond_4

    .line 89
    :try_start_4
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 94
    :cond_4
    :goto_4
    if-eqz v3, :cond_5

    .line 96
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 102
    :cond_5
    :goto_5
    return-void

    .line 90
    .end local v0    # "bytes":[B
    .end local v5    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zip":Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v2

    .line 91
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 97
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 98
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 90
    .end local v2    # "e":Ljava/io/IOException;
    .end local v6    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "bytes":[B
    .restart local v5    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v7    # "zip":Ljava/util/zip/ZipInputStream;
    :catch_2
    move-exception v2

    .line 91
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 97
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 98
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 87
    .end local v0    # "bytes":[B
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "ze":Ljava/util/zip/ZipEntry;
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zip":Ljava/util/zip/ZipInputStream;
    goto :goto_1
.end method

.method public static fileToHash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "algorithm"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 255
    const/4 v3, 0x0

    .line 257
    .local v3, "inputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .local v4, "inputStream":Ljava/io/InputStream;
    const/16 v7, 0x100

    :try_start_1
    new-array v0, v7, [B

    .line 259
    .local v0, "buffer":[B
    invoke-static {p1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 260
    .local v1, "digest":Ljava/security/MessageDigest;
    const/4 v6, 0x0

    .line 261
    .local v6, "numRead":I
    :cond_0
    :goto_0
    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 262
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 263
    if-lez v6, :cond_0

    .line 264
    const/4 v7, 0x0

    invoke-virtual {v1, v0, v7, v6}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 271
    .end local v0    # "buffer":[B
    .end local v1    # "digest":Ljava/security/MessageDigest;
    .end local v6    # "numRead":I
    :catch_0
    move-exception v2

    move-object v3, v4

    .line 276
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .local v2, "e":Ljava/io/IOException;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    :goto_1
    const/4 v7, 0x0

    .line 278
    if-eqz v3, :cond_1

    .line 280
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 276
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    return-object v7

    .line 267
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "digest":Ljava/security/MessageDigest;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "numRead":I
    :cond_2
    :try_start_3
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    .line 269
    .local v5, "md5Bytes":[B
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 270
    invoke-static {v5}, Lcom/vlingo/sdk/internal/util/FileUtils;->convertHashToString([B)Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v7

    .line 278
    if-eqz v4, :cond_3

    .line 280
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_3
    :goto_3
    move-object v3, v4

    .line 270
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    goto :goto_2

    .line 278
    .end local v0    # "buffer":[B
    .end local v1    # "digest":Ljava/security/MessageDigest;
    .end local v5    # "md5Bytes":[B
    .end local v6    # "numRead":I
    :catchall_0
    move-exception v7

    :goto_4
    if-eqz v3, :cond_4

    .line 280
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 278
    :cond_4
    :goto_5
    throw v7

    .line 281
    .restart local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v8

    goto :goto_2

    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v8

    goto :goto_5

    .end local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "digest":Ljava/security/MessageDigest;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v5    # "md5Bytes":[B
    .restart local v6    # "numRead":I
    :catch_3
    move-exception v8

    goto :goto_3

    .line 278
    .end local v0    # "buffer":[B
    .end local v1    # "digest":Ljava/security/MessageDigest;
    .end local v5    # "md5Bytes":[B
    .end local v6    # "numRead":I
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    goto :goto_4

    .line 271
    :catch_4
    move-exception v2

    goto :goto_1
.end method

.method private static getFileFromZip(Ljava/util/zip/ZipInputStream;Ljava/lang/String;[B)V
    .locals 6
    .param p0, "zipInputStream"    # Ljava/util/zip/ZipInputStream;
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 162
    .local v1, "fout":Ljava/io/OutputStream;
    const/4 v2, 0x0

    .line 163
    .local v2, "wrote":Z
    invoke-virtual {p0, p2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v0

    .local v0, "c":I
    :goto_0
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 164
    const/4 v3, 0x0

    invoke-virtual {v1, p2, v3, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 165
    const/4 v2, 0x1

    .line 163
    invoke-virtual {p0, p2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v0

    goto :goto_0

    .line 167
    :cond_0
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 168
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 170
    if-nez v2, :cond_1

    .line 171
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 172
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "didn\'t actually write to new file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 174
    :cond_1
    return-void
.end method

.method public static readResource(Ljava/lang/String;)[B
    .locals 7
    .param p0, "resource"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x1f4

    .line 204
    const/4 v4, 0x0

    .line 205
    .local v4, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 206
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .line 207
    .local v3, "data":[B
    new-array v1, v5, [B

    .line 208
    .local v1, "buf":[B
    const/4 v2, 0x0

    .line 212
    .local v2, "bytesRead":I
    :try_start_0
    const-class v5, Ljava/lang/Class;

    invoke-virtual {v5, p0}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 213
    if-nez v4, :cond_1

    .line 214
    const/4 v5, 0x0

    .line 228
    if-eqz v4, :cond_0

    .line 230
    :try_start_1
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 235
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    .line 239
    :goto_1
    return-object v5

    .line 215
    :cond_1
    :goto_2
    :try_start_3
    invoke-virtual {v4, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v5, -0x1

    if-eq v2, v5, :cond_3

    .line 217
    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 222
    :catch_0
    move-exception v5

    .line 228
    if-eqz v4, :cond_2

    .line 230
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 235
    :cond_2
    :goto_3
    :try_start_5
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :goto_4
    move-object v5, v3

    .line 239
    goto :goto_1

    .line 220
    :cond_3
    :try_start_6
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 221
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v3

    .line 228
    if-eqz v4, :cond_4

    .line 230
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    .line 235
    :cond_4
    :goto_5
    :try_start_8
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_4

    .line 236
    :catch_1
    move-exception v5

    goto :goto_4

    .line 228
    :catchall_0
    move-exception v5

    if-eqz v4, :cond_5

    .line 230
    :try_start_9
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 235
    :cond_5
    :goto_6
    :try_start_a
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 228
    :goto_7
    throw v5

    .line 231
    :catch_2
    move-exception v5

    goto :goto_3

    :catch_3
    move-exception v6

    goto :goto_6

    .line 236
    :catch_4
    move-exception v6

    goto :goto_7

    .line 231
    :catch_5
    move-exception v6

    goto :goto_0

    .line 236
    :catch_6
    move-exception v6

    goto :goto_1

    .line 231
    :catch_7
    move-exception v5

    goto :goto_5
.end method
