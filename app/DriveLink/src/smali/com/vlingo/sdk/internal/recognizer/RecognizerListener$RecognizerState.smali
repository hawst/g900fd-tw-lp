.class public final enum Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;
.super Ljava/lang/Enum;
.source "RecognizerListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RecognizerState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

.field public static final enum CONNECTING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

.field public static final enum LISTENING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

.field public static final enum RESULT:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

.field public static final enum RMS_CHANGED:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

.field public static final enum THINKING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const-string/jumbo v1, "CONNECTING"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->CONNECTING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    .line 24
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const-string/jumbo v1, "LISTENING"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->LISTENING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    .line 25
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const-string/jumbo v1, "RMS_CHANGED"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->RMS_CHANGED:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    .line 27
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const-string/jumbo v1, "THINKING"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->THINKING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    .line 29
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const-string/jumbo v1, "RESULT"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->RESULT:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    .line 22
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->CONNECTING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->LISTENING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->RMS_CHANGED:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->THINKING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->RESULT:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    return-object v0
.end method
