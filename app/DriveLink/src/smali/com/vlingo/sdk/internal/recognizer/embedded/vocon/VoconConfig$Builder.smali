.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;
.super Ljava/lang/Object;
.source "VoconConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "modelFileName"    # Ljava/lang/String;
    .param p2, "clcFileName"    # Ljava/lang/String;
    .param p3, "grammarFileName"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mModelFilename:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/lang/String;)Ljava/lang/String;

    .line 39
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mClcFilename:Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/lang/String;)Ljava/lang/String;

    .line 40
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mGrammarFilename:Ljava/lang/String;
    invoke-static {v0, p3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/lang/String;)Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mGrammarInfoList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->access$402(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/util/List;)Ljava/util/List;

    .line 42
    return-void
.end method


# virtual methods
.method public addGrammarInfo(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;
    .locals 1
    .param p1, "config"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mGrammarInfoList:Ljava/util/List;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->access$400(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    return-object p0
.end method

.method public addGrammarInfo(Ljava/lang/String;[Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;
    .locals 2
    .param p1, "grammarName"    # Ljava/lang/String;
    .param p2, "slots"    # [Ljava/lang/String;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mGrammarInfoList:Ljava/util/List;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->access$400(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;

    invoke-direct {v1, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    return-object p0
.end method

.method public build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    return-object v0
.end method
