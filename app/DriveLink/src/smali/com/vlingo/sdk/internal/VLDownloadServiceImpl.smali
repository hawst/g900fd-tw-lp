.class public Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;
.super Lcom/vlingo/sdk/internal/VLComponentImpl;
.source "VLDownloadServiceImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/filedownload/VLDownloadService;


# instance fields
.field private downloadManager:Landroid/app/DownloadManager;

.field private volatile downloadsMap:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Long;",
            "Landroid/util/Pair",
            "<",
            "Lcom/vlingo/sdk/filedownload/VLDownloadData;",
            "Lcom/vlingo/sdk/filedownload/VLDownloadListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private volatile filters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/filedownload/VLDownloadFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 3
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLComponentImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 41
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    .line 43
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->filters:Ljava/util/List;

    .line 56
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 57
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$1;

    invoke-direct {v2, p0}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$1;-><init>(Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;)Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;
    .param p1, "x1"    # Ljava/lang/Long;
    .param p2, "x2"    # Lcom/vlingo/sdk/filedownload/VLDownloadListener;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->processDownloadId(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Z

    move-result v0

    return v0
.end method

.method private checkFileSum(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadData;Ljava/lang/String;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)V
    .locals 9
    .param p1, "downloadId"    # Ljava/lang/Long;
    .param p2, "VLDownloadData"    # Lcom/vlingo/sdk/filedownload/VLDownloadData;
    .param p3, "downloadedFilePath"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/vlingo/sdk/filedownload/VLDownloadListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 215
    const/4 v1, 0x0

    .line 217
    .local v1, "fileHashCode":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p2}, Lcom/vlingo/sdk/filedownload/VLDownloadData;->getHashAlgorithm()Ljava/lang/String;

    move-result-object v4

    invoke-static {p3, v4}, Lcom/vlingo/sdk/internal/util/FileUtils;->fileToHash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 224
    :goto_0
    invoke-virtual {p2}, Lcom/vlingo/sdk/filedownload/VLDownloadData;->getCheckSum()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {p2}, Lcom/vlingo/sdk/filedownload/VLDownloadData;->getCheckSum()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 233
    if-eqz p4, :cond_0

    .line 234
    invoke-interface {p4, p2, p3}, Lcom/vlingo/sdk/filedownload/VLDownloadListener;->onCheckSumFailed(Lcom/vlingo/sdk/filedownload/VLDownloadData;Ljava/lang/String;)V

    .line 236
    :cond_0
    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 237
    .local v0, "download":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;>;"
    if-eqz v0, :cond_2

    .line 238
    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v4, :cond_1

    .line 239
    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/vlingo/sdk/filedownload/VLDownloadListener;

    invoke-interface {v4, p2, p3}, Lcom/vlingo/sdk/filedownload/VLDownloadListener;->onCheckSumFailed(Lcom/vlingo/sdk/filedownload/VLDownloadData;Ljava/lang/String;)V

    .line 241
    :cond_1
    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 242
    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/vlingo/sdk/filedownload/VLDownloadData;

    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getFilters(Lcom/vlingo/sdk/filedownload/VLDownloadData;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/sdk/filedownload/VLDownloadFilter;

    .line 243
    .local v2, "filter":Lcom/vlingo/sdk/filedownload/VLDownloadFilter;
    invoke-interface {v2, p2, p3}, Lcom/vlingo/sdk/filedownload/VLDownloadFilter;->onCheckSumFailed(Lcom/vlingo/sdk/filedownload/VLDownloadData;Ljava/lang/String;)V

    goto :goto_1

    .line 247
    .end local v2    # "filter":Lcom/vlingo/sdk/filedownload/VLDownloadFilter;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getDownloadManager()Landroid/app/DownloadManager;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [J

    const/4 v6, 0x0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    aput-wide v7, v5, v6

    invoke-virtual {v4, v5}, Landroid/app/DownloadManager;->remove([J)I

    .line 249
    .end local v0    # "download":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;>;"
    :cond_3
    return-void

    .line 218
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method private fireOnFailedDownload(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;)V
    .locals 8
    .param p1, "downloadId"    # Ljava/lang/Long;
    .param p2, "listener"    # Lcom/vlingo/sdk/filedownload/VLDownloadListener;
    .param p3, "failReason"    # Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .prologue
    .line 151
    if-eqz p2, :cond_0

    .line 152
    invoke-interface {p2, p3}, Lcom/vlingo/sdk/filedownload/VLDownloadListener;->onDownloadFailed(Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;)V

    .line 154
    :cond_0
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v3, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 155
    .local v0, "download":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;>;"
    if-eqz v0, :cond_2

    .line 156
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v3, :cond_1

    .line 157
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/vlingo/sdk/filedownload/VLDownloadListener;

    invoke-interface {v3, p3}, Lcom/vlingo/sdk/filedownload/VLDownloadListener;->onDownloadFailed(Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;)V

    .line 159
    :cond_1
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v3, :cond_2

    .line 160
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/vlingo/sdk/filedownload/VLDownloadData;

    invoke-direct {p0, v3}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getFilters(Lcom/vlingo/sdk/filedownload/VLDownloadData;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/filedownload/VLDownloadFilter;

    .line 161
    .local v1, "filter":Lcom/vlingo/sdk/filedownload/VLDownloadFilter;
    invoke-interface {v1, p3}, Lcom/vlingo/sdk/filedownload/VLDownloadFilter;->onDownloadFailed(Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;)V

    goto :goto_0

    .line 165
    .end local v1    # "filter":Lcom/vlingo/sdk/filedownload/VLDownloadFilter;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getDownloadManager()Landroid/app/DownloadManager;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v4, v5

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager;->remove([J)I

    .line 166
    return-void
.end method

.method private fireOnSuccessfulDownload(Ljava/lang/Long;Ljava/lang/String;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)V
    .locals 8
    .param p1, "downloadId"    # Ljava/lang/Long;
    .param p2, "downloadedFilePath"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/vlingo/sdk/filedownload/VLDownloadListener;

    .prologue
    .line 180
    if-eqz p3, :cond_0

    .line 181
    invoke-interface {p3, p2}, Lcom/vlingo/sdk/filedownload/VLDownloadListener;->onDownloadSuccessful(Ljava/lang/String;)V

    .line 183
    :cond_0
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v3, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 184
    .local v0, "download":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;>;"
    if-eqz v0, :cond_2

    .line 185
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v3, :cond_1

    .line 186
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/vlingo/sdk/filedownload/VLDownloadListener;

    invoke-interface {v3, p2}, Lcom/vlingo/sdk/filedownload/VLDownloadListener;->onDownloadSuccessful(Ljava/lang/String;)V

    .line 188
    :cond_1
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v3, :cond_2

    .line 189
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/vlingo/sdk/filedownload/VLDownloadData;

    invoke-direct {p0, v3}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getFilters(Lcom/vlingo/sdk/filedownload/VLDownloadData;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/filedownload/VLDownloadFilter;

    .line 190
    .local v1, "filter":Lcom/vlingo/sdk/filedownload/VLDownloadFilter;
    invoke-interface {v1, p2}, Lcom/vlingo/sdk/filedownload/VLDownloadFilter;->onDownloadSuccessful(Ljava/lang/String;)V

    goto :goto_0

    .line 194
    .end local v1    # "filter":Lcom/vlingo/sdk/filedownload/VLDownloadFilter;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getDownloadManager()Landroid/app/DownloadManager;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v4, v5

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager;->remove([J)I

    .line 195
    return-void
.end method

.method private getDownloadIds()[J
    .locals 7

    .prologue
    .line 422
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v5}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 423
    .local v4, "idsSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    new-array v3, v5, [J

    .line 426
    .local v3, "ids":[J
    const/4 v0, 0x0

    .line 427
    .local v0, "i":I
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 428
    .local v2, "id":Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    aput-wide v5, v3, v0

    .line 429
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 431
    .end local v2    # "id":Ljava/lang/Long;
    :cond_0
    return-object v3
.end method

.method private getDownloadInfoById(Ljava/lang/Long;)Landroid/database/Cursor;
    .locals 6
    .param p1, "downloadId"    # Ljava/lang/Long;

    .prologue
    .line 101
    new-instance v1, Landroid/app/DownloadManager$Query;

    invoke-direct {v1}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 102
    .local v1, "query":Landroid/app/DownloadManager$Query;
    const/4 v2, 0x1

    new-array v2, v2, [J

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 103
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getDownloadManager()Landroid/app/DownloadManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 104
    .local v0, "cur":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_1

    .line 105
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v3

    .line 106
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 107
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 122
    const/4 v0, 0x0

    .line 124
    .end local v0    # "cur":Landroid/database/Cursor;
    :cond_1
    return-object v0

    .line 119
    .restart local v0    # "cur":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private getDownloadManager()Landroid/app/DownloadManager;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadManager:Landroid/app/DownloadManager;

    if-nez v0, :cond_0

    .line 47
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadManager:Landroid/app/DownloadManager;

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadManager:Landroid/app/DownloadManager;

    return-object v0
.end method

.method private getFilters(Lcom/vlingo/sdk/filedownload/VLDownloadData;)Ljava/util/List;
    .locals 3
    .param p1, "data"    # Lcom/vlingo/sdk/filedownload/VLDownloadData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/filedownload/VLDownloadData;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/filedownload/VLDownloadFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/filedownload/VLDownloadFilter;>;"
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->filters:Ljava/util/List;

    new-instance v2, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$2;

    invoke-direct {v2, p0, p1}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$2;-><init>(Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;Lcom/vlingo/sdk/filedownload/VLDownloadData;)V

    invoke-static {v1, v2, v0}, Lcom/vlingo/sdk/util/CollectionUtils;->select(Ljava/lang/Iterable;Lcom/vlingo/sdk/util/Predicate;Ljava/util/Collection;)V

    .line 135
    return-object v0
.end method

.method private processDownloadId(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Z
    .locals 10
    .param p1, "downloadId"    # Ljava/lang/Long;
    .param p2, "listener"    # Lcom/vlingo/sdk/filedownload/VLDownloadListener;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 276
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getDownloadInfoById(Ljava/lang/Long;)Landroid/database/Cursor;

    move-result-object v0

    .line 278
    .local v0, "cur":Landroid/database/Cursor;
    if-nez v0, :cond_1

    .line 317
    :cond_0
    :goto_0
    return v7

    .line 282
    :cond_1
    const-string/jumbo v9, "status"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 283
    .local v6, "status":I
    const/4 v4, 0x0

    .line 284
    .local v4, "fileExistError":Z
    const/16 v9, 0x10

    if-ne v6, v9, :cond_3

    .line 285
    const-string/jumbo v9, "reason"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 286
    .local v3, "failReasonIndex":I
    const/16 v9, 0x3f1

    if-ne v3, v9, :cond_2

    move v4, v8

    .line 287
    :goto_1
    if-eqz v4, :cond_3

    .line 292
    invoke-static {v3}, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->getByIndex(I)Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    move-result-object v7

    invoke-direct {p0, p1, p2, v7}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->fireOnFailedDownload(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;)V

    move v7, v8

    .line 294
    goto :goto_0

    :cond_2
    move v4, v7

    .line 286
    goto :goto_1

    .line 297
    .end local v3    # "failReasonIndex":I
    :cond_3
    const/16 v9, 0x8

    if-eq v6, v9, :cond_4

    if-eqz v4, :cond_0

    .line 298
    :cond_4
    iget-object v7, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v7, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 299
    .local v1, "download":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;>;"
    const-string/jumbo v7, "local_filename"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 300
    .local v5, "filePath":Ljava/lang/String;
    if-eqz v1, :cond_5

    iget-object v7, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v7, :cond_5

    iget-object v7, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Lcom/vlingo/sdk/filedownload/VLDownloadData;

    invoke-virtual {v7}, Lcom/vlingo/sdk/filedownload/VLDownloadData;->getCheckSum()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 302
    :try_start_0
    iget-object v7, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Lcom/vlingo/sdk/filedownload/VLDownloadData;

    invoke-direct {p0, p1, v7, v5, p2}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->checkFileSum(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadData;Ljava/lang/String;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    :cond_5
    :goto_2
    invoke-direct {p0, p1, v5, p2}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->fireOnSuccessfulDownload(Ljava/lang/Long;Ljava/lang/String;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)V

    move v7, v8

    .line 315
    goto :goto_0

    .line 303
    :catch_0
    move-exception v2

    .line 306
    .local v2, "e":Ljava/io/FileNotFoundException;
    sget-object v7, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->DOWNLOADED_FILE_NOT_FOUND:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-direct {p0, p1, p2, v7}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->fireOnFailedDownload(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;)V

    goto :goto_2
.end method

.method private searchInRequestedDownloads(Lcom/vlingo/sdk/filedownload/VLDownloadData;)Ljava/lang/Long;
    .locals 5
    .param p1, "VLDownloadData"    # Lcom/vlingo/sdk/filedownload/VLDownloadData;

    .prologue
    .line 73
    new-instance v2, Landroid/app/DownloadManager$Query;

    invoke-direct {v2}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 74
    .local v2, "query":Landroid/app/DownloadManager$Query;
    const/16 v3, 0x1f

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager$Query;->setFilterByStatus(I)Landroid/app/DownloadManager$Query;

    .line 80
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getDownloadManager()Landroid/app/DownloadManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 81
    .local v0, "cur":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_1

    .line 82
    invoke-virtual {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData;->getUri()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "uri"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 83
    const-string/jumbo v3, "_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 84
    .local v1, "id":Ljava/lang/Long;
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 89
    .end local v1    # "id":Ljava/lang/Long;
    :goto_1
    return-object v1

    .line 81
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 88
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 89
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized addListener(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Lcom/vlingo/sdk/filedownload/VLDownloadListener;
    .locals 4
    .param p1, "downloadId"    # Ljava/lang/Long;
    .param p2, "listener"    # Lcom/vlingo/sdk/filedownload/VLDownloadListener;

    .prologue
    .line 409
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 410
    .local v0, "download":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;>;"
    if-eqz v0, :cond_1

    .line 411
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 412
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418
    :goto_0
    monitor-exit p0

    return-object v1

    .line 414
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    new-instance v2, Landroid/util/Pair;

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-direct {v2, v3, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, p1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, p2

    .line 415
    goto :goto_0

    .line 418
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 409
    .end local v0    # "download":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public bridge synthetic destroy()V
    .locals 0

    .prologue
    .line 36
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->destroy()V

    return-void
.end method

.method public doesDependOnSettings()Z
    .locals 1

    .prologue
    .line 448
    const/4 v0, 0x0

    return v0
.end method

.method public download(Lcom/vlingo/sdk/filedownload/VLDownloadData;)Ljava/lang/Long;
    .locals 1
    .param p1, "vlDownloadData"    # Lcom/vlingo/sdk/filedownload/VLDownloadData;

    .prologue
    .line 322
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->download(Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public download(Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Ljava/lang/Long;
    .locals 10
    .param p1, "vlDownloadData"    # Lcom/vlingo/sdk/filedownload/VLDownloadData;
    .param p2, "listener"    # Lcom/vlingo/sdk/filedownload/VLDownloadListener;

    .prologue
    const/4 v6, 0x2

    const/4 v9, 0x0

    .line 327
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->searchInRequestedDownloads(Lcom/vlingo/sdk/filedownload/VLDownloadData;)Ljava/lang/Long;

    move-result-object v1

    .line 328
    .local v1, "downloadId":Ljava/lang/Long;
    if-eqz v1, :cond_1

    .line 330
    invoke-direct {p0, v1, p2}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->processDownloadId(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x0

    .line 362
    :goto_0
    return-object v6

    :cond_0
    move-object v6, v1

    .line 330
    goto :goto_0

    .line 333
    :cond_1
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 335
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData;->getUri()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 336
    .local v5, "uri":Landroid/net/Uri;
    new-instance v4, Landroid/app/DownloadManager$Request;

    invoke-direct {v4, v5}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 337
    .local v4, "request":Landroid/app/DownloadManager$Request;
    invoke-virtual {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData;->isWifiOnly()Z

    move-result v7

    if-eqz v7, :cond_2

    move v2, v6

    .line 339
    .local v2, "networkType":I
    :goto_1
    invoke-virtual {v4, v2}, Landroid/app/DownloadManager$Request;->setAllowedNetworkTypes(I)Landroid/app/DownloadManager$Request;

    .line 340
    invoke-virtual {v4, v9}, Landroid/app/DownloadManager$Request;->setAllowedOverRoaming(Z)Landroid/app/DownloadManager$Request;

    .line 341
    sget-object v7, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData;->getLocalFileName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v0, v7, v8}, Landroid/app/DownloadManager$Request;->setDestinationInExternalFilesDir(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 344
    invoke-virtual {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData;->isVisible()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 345
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "download "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData;->getLocalFileName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " file"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 346
    .local v3, "notification":Ljava/lang/String;
    invoke-virtual {v4, v3}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 347
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 348
    invoke-virtual {v4, v3}, Landroid/app/DownloadManager$Request;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 358
    .end local v3    # "notification":Ljava/lang/String;
    :goto_2
    iget-object v6, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v6, v4}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 359
    iget-object v6, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    new-instance v7, Landroid/util/Pair;

    invoke-direct {v7, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v6, v1, v7}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v6, v1

    .line 362
    goto/16 :goto_0

    .line 337
    .end local v2    # "networkType":I
    :cond_2
    const/4 v2, 0x3

    goto :goto_1

    .line 350
    .restart local v2    # "networkType":I
    :cond_3
    invoke-virtual {v4, v9}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    .line 351
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xb

    if-lt v7, v8, :cond_4

    .line 352
    invoke-virtual {v4, v6}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    goto :goto_2

    .line 354
    :cond_4
    invoke-virtual {v4, v9}, Landroid/app/DownloadManager$Request;->setShowRunningNotification(Z)Landroid/app/DownloadManager$Request;

    goto :goto_2
.end method

.method public getListener(Ljava/lang/Long;)Lcom/vlingo/sdk/filedownload/VLDownloadListener;
    .locals 2
    .param p1, "downloadId"    # Ljava/lang/Long;

    .prologue
    .line 393
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 394
    .local v0, "download":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;>;"
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getStatus(Ljava/lang/Long;)Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;
    .locals 2
    .param p1, "downloadId"    # Ljava/lang/Long;

    .prologue
    .line 367
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getDownloadInfoById(Ljava/lang/Long;)Landroid/database/Cursor;

    move-result-object v0

    .line 368
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 369
    const/4 v1, 0x0

    .line 372
    :goto_0
    return-object v1

    :cond_0
    const-string/jumbo v1, "status"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->getStatusByIndex(I)Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    move-result-object v1

    goto :goto_0
.end method

.method public bridge synthetic isValid()Z
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->isValid()Z

    move-result v0

    return v0
.end method

.method onDestroy()V
    .locals 2

    .prologue
    .line 436
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getDownloadIds()[J

    move-result-object v0

    .line 437
    .local v0, "ids":[J
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 440
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getDownloadManager()Landroid/app/DownloadManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager;->remove([J)I

    .line 442
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 443
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->filters:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 444
    return-void
.end method

.method public registerFilter(Lcom/vlingo/sdk/filedownload/VLDownloadFilter;)V
    .locals 1
    .param p1, "filter"    # Lcom/vlingo/sdk/filedownload/VLDownloadFilter;

    .prologue
    .line 383
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->filters:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    return-void
.end method

.method public remove(Ljava/lang/Long;)Z
    .locals 6
    .param p1, "downloadId"    # Ljava/lang/Long;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 377
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getDownloadManager()Landroid/app/DownloadManager;

    move-result-object v2

    new-array v3, v0, [J

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v3, v1

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager;->remove([J)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public removeFilter(Lcom/vlingo/sdk/filedownload/VLDownloadFilter;)Z
    .locals 1
    .param p1, "filter"    # Lcom/vlingo/sdk/filedownload/VLDownloadFilter;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->filters:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized removeListener(Ljava/lang/Long;)Z
    .locals 5
    .param p1, "downloadId"    # Ljava/lang/Long;

    .prologue
    .line 399
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 400
    .local v0, "download":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;>;"
    if-eqz v0, :cond_0

    .line 401
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;

    new-instance v2, Landroid/util/Pair;

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, p1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    const/4 v1, 0x1

    .line 404
    :goto_0
    monitor-exit p0

    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 399
    .end local v0    # "download":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
