.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;
.source "VoconUpdateSlotsTask.java"

# interfaces
.implements Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;",
        ">;",
        "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;"
    }
.end annotation


# instance fields
.field private volatile cancelled:Z

.field private final grammarVocabularies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;",
            ">;"
        }
    .end annotation
.end field

.field private final listener:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;

.field private final voconSlotsUpdate:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;Landroid/os/Handler;Ljava/util/List;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;
    .param p2, "mainHandler"    # Landroid/os/Handler;
    .param p4, "voconSlotsUpdate"    # Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;
    .param p5, "listener"    # Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;",
            ">;",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p3, "grammarVocabularies":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;>;"
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;-><init>(Ljava/lang/Object;Landroid/os/Handler;)V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->cancelled:Z

    .line 29
    iput-object p5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->listener:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;

    .line 30
    iput-object p4, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->voconSlotsUpdate:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    .line 31
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->grammarVocabularies:Ljava/util/List;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;)Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->listener:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->cancelled:Z

    return v0
.end method

.method private getVocabulariesWithSlot(Ljava/lang/String;)[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    .locals 4
    .param p1, "slotName"    # Ljava/lang/String;

    .prologue
    .line 137
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;>;"
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->grammarVocabularies:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;

    .line 139
    .local v2, "vocabulary":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;->getSlotName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 140
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    .end local v2    # "vocabulary":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;

    return-object v3
.end method


# virtual methods
.method public declared-synchronized cancel()V
    .locals 1

    .prologue
    .line 124
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->cancelled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    monitor-exit p0

    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected execute()Z
    .locals 15

    .prologue
    .line 36
    monitor-enter p0

    .line 37
    :try_start_0
    iget-boolean v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->cancelled:Z

    if-eqz v10, :cond_0

    .line 38
    const/4 v6, 0x0

    monitor-exit p0

    .line 92
    :goto_0
    return v6

    .line 40
    :cond_0
    new-instance v10, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$1;

    invoke-direct {v10, p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$1;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;)V

    invoke-virtual {p0, v10}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->notifyOnCallerHandler(Ljava/lang/Runnable;)V

    .line 46
    const/4 v6, 0x1

    .line 47
    .local v6, "success":Z
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->voconSlotsUpdate:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    invoke-virtual {v10}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->getSlotName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->getVocabulariesWithSlot(Ljava/lang/String;)[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;

    move-result-object v8

    .line 48
    .local v8, "vocabularies":[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    move-object v0, v8

    .local v0, "arr$":[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v3, v2

    .end local v2    # "i$":I
    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_8

    aget-object v9, v0, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    .local v9, "vocabulary":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    const/4 v7, 0x1

    .line 51
    .local v7, "successForGrammar":Z
    :try_start_1
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->voconSlotsUpdate:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    invoke-virtual {v10}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->isStartFromScratch()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 52
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v10

    invoke-interface {v10, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->clearTerminalsFromVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 55
    const/4 v7, 0x0

    .line 58
    :cond_1
    if-eqz v7, :cond_6

    .line 59
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->voconSlotsUpdate:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    invoke-virtual {v10}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->getRemovedTerminals()Ljava/util/Collection;

    move-result-object v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->voconSlotsUpdate:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    invoke-virtual {v10}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->getRemovedTerminals()Ljava/util/Collection;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Collection;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_3

    .line 60
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->voconSlotsUpdate:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    invoke-virtual {v10}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->getRemovedTerminals()Ljava/util/Collection;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .end local v3    # "i$":I
    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;

    .line 63
    .local v5, "slotTerminal":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v10

    new-instance v11, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;

    invoke-virtual {v5}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;->getOrthography()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;->getId()J

    move-result-wide v13

    invoke-direct {v11, v12, v13, v14}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;-><init>(Ljava/lang/String;J)V

    invoke-interface {v10, v9, v11}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->removeTerminalFromVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)Z

    move-result v10

    if-nez v10, :cond_2

    goto :goto_2

    .line 69
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "slotTerminal":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;
    :cond_3
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->voconSlotsUpdate:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    invoke-virtual {v10}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->getAddedTerminals()Ljava/util/Collection;

    move-result-object v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->voconSlotsUpdate:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    invoke-virtual {v10}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->getAddedTerminals()Ljava/util/Collection;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Collection;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    .line 70
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->voconSlotsUpdate:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    invoke-virtual {v10}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->getAddedTerminals()Ljava/util/Collection;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_4
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;

    .line 73
    .restart local v5    # "slotTerminal":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v10

    new-instance v11, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;

    invoke-virtual {v5}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;->getOrthography()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;->getId()J

    move-result-wide v13

    invoke-direct {v11, v12, v13, v14}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;-><init>(Ljava/lang/String;J)V

    invoke-interface {v10, v9, v11}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->addTerminalToVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)Z

    move-result v10

    if-nez v10, :cond_4

    goto :goto_3

    .line 79
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "slotTerminal":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;
    :cond_5
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v10

    invoke-interface {v10, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->commitVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v10

    if-nez v10, :cond_6

    .line 82
    const/4 v7, 0x0

    .line 90
    :cond_6
    :goto_4
    if-eqz v6, :cond_7

    if-eqz v7, :cond_7

    const/4 v6, 0x1

    .line 48
    :goto_5
    add-int/lit8 v2, v3, 0x1

    .local v2, "i$":I
    move v3, v2

    .end local v2    # "i$":I
    .restart local v3    # "i$":I
    goto/16 :goto_1

    .line 85
    .end local v3    # "i$":I
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/Exception;
    const/4 v7, 0x0

    goto :goto_4

    .line 90
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_7
    const/4 v6, 0x0

    goto :goto_5

    .line 92
    .end local v7    # "successForGrammar":Z
    .end local v9    # "vocabulary":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    .restart local v3    # "i$":I
    :cond_8
    :try_start_2
    monitor-exit p0

    goto/16 :goto_0

    .line 93
    .end local v0    # "arr$":[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v6    # "success":Z
    .end local v8    # "vocabularies":[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    :catchall_0
    move-exception v10

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v10
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->cancelled:Z

    return v0
.end method

.method protected onExecutionFailed(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;)V
    .locals 1
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;

    .prologue
    .line 109
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$3;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$3;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->notifyOnCallerHandler(Ljava/lang/Runnable;)V

    .line 119
    invoke-interface {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;->onVoconUpdateSlotsCompleted()V

    .line 120
    return-void
.end method

.method protected bridge synthetic onExecutionFailed(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->onExecutionFailed(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;)V

    return-void
.end method

.method protected onExecutionSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;)V
    .locals 1
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;

    .prologue
    .line 98
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$2;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$2;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->notifyOnCallerHandler(Ljava/lang/Runnable;)V

    .line 104
    invoke-interface {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;->onVoconUpdateSlotsCompleted()V

    .line 105
    return-void
.end method

.method protected bridge synthetic onExecutionSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->onExecutionSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;)V

    return-void
.end method
