.class public Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;
.super Lcom/vlingo/sdk/internal/VLComponentImpl;
.source "VLEmbeddedDialogManagerImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;


# instance fields
.field private final PREFERENCE_CHANGE_LISTENER:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private enabled:Ljava/lang/Boolean;

.field private thread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 2
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLComponentImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 17
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;

    .line 20
    new-instance v0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl$1;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl$1;-><init>(Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->PREFERENCE_CHANGE_LISTENER:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 43
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->PREFERENCE_CHANGE_LISTENER:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;

    return-object p1
.end method


# virtual methods
.method public backgroundInit()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl$2;

    const-string/jumbo v1, "EDM Initialization"

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl$2;-><init>(Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->thread:Ljava/lang/Thread;

    .line 86
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->thread:Ljava/lang/Thread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 92
    :cond_0
    return-void
.end method

.method public bridge synthetic destroy()V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->destroy()V

    return-void
.end method

.method public doesDependOnSettings()Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return v0
.end method

.method public increasePriority()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->thread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->backgroundInit()V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->thread:Ljava/lang/Thread;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 117
    :cond_1
    return-void
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isValid()Z
    .locals 1

    .prologue
    .line 13
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->isValid()Z

    move-result v0

    return v0
.end method

.method onDestroy()V
    .locals 2

    .prologue
    .line 48
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->PREFERENCE_CHANGE_LISTENER:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 49
    invoke-static {}, Lcom/nuance/embeddeddialogmanager/EDM;->getInstance()Lcom/nuance/embeddeddialogmanager/EDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/embeddeddialogmanager/EDM;->destroy()V

    .line 50
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 106
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;

    .line 107
    return-void
.end method

.method public updateDynamicFeaturesConfig()V
    .locals 2

    .prologue
    .line 54
    invoke-static {}, Lcom/nuance/embeddeddialogmanager/EDM;->getInstance()Lcom/nuance/embeddeddialogmanager/EDM;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/embeddeddialogmanager/EDM;->updateDynamicFeatureConfig(Landroid/content/Context;)V

    .line 55
    return-void
.end method
