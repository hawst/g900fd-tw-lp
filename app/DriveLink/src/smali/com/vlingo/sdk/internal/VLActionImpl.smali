.class public Lcom/vlingo/sdk/internal/VLActionImpl;
.super Ljava/lang/Object;
.source "VLActionImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/VLAction;


# instance fields
.field private elseStatement:Ljava/lang/String;

.field private ifCondition:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mParameters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/internal/vlservice/response/Action;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->getName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->mName:Ljava/lang/String;

    .line 25
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->mParameters:Ljava/util/HashMap;

    .line 26
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->getParams()Ljava/util/Enumeration;

    move-result-object v0

    .line 27
    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 28
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 29
    .local v1, "pName":Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->getStringParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 30
    .local v2, "pValue":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->mParameters:Ljava/util/HashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 32
    .end local v1    # "pName":Ljava/lang/String;
    .end local v2    # "pValue":Ljava/lang/String;
    :cond_0
    iget-object v3, p1, Lcom/vlingo/sdk/internal/vlservice/response/Action;->ifCondition:Ljava/lang/String;

    iput-object v3, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->ifCondition:Ljava/lang/String;

    .line 33
    iget-object v3, p1, Lcom/vlingo/sdk/internal/vlservice/response/Action;->elseStatement:Ljava/lang/String;

    iput-object v3, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->elseStatement:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public getElseStatement()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->elseStatement:Ljava/lang/String;

    return-object v0
.end method

.method public getIfCondition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->ifCondition:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getParamValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "paramName"    # Ljava/lang/String;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->mParameters:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->mParameters:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 53
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getParameterNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->mParameters:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public isConditional()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->ifCondition:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLActionImpl;->mParameters:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
