.class public Lcom/vlingo/sdk/internal/deviceinfo/CarrierUtils;
.super Ljava/lang/Object;
.source "CarrierUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCarrier(Ljava/lang/String;)Lcom/vlingo/sdk/internal/deviceinfo/Carrier;
    .locals 1
    .param p0, "networkName"    # Ljava/lang/String;

    .prologue
    .line 11
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/Carriers;->getInstance()Lcom/vlingo/sdk/internal/deviceinfo/Carriers;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/sdk/internal/deviceinfo/Carriers;->getCarrierByName(Ljava/lang/String;)Lcom/vlingo/sdk/internal/deviceinfo/Carrier;

    move-result-object v0

    goto :goto_0
.end method

.method public static getCarrierISO2Country(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "networkName"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-static {p0}, Lcom/vlingo/sdk/internal/deviceinfo/CarrierUtils;->getCarrier(Ljava/lang/String;)Lcom/vlingo/sdk/internal/deviceinfo/Carrier;

    move-result-object v0

    .line 19
    .local v0, "c":Lcom/vlingo/sdk/internal/deviceinfo/Carrier;
    if-eqz v0, :cond_0

    .line 20
    iget-object v1, v0, Lcom/vlingo/sdk/internal/deviceinfo/Carrier;->iso2letterCountry:Ljava/lang/String;

    .line 22
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
