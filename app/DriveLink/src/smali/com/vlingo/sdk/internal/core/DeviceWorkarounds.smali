.class public Lcom/vlingo/sdk/internal/core/DeviceWorkarounds;
.super Ljava/lang/Object;
.source "DeviceWorkarounds.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isDeviceHTCIncredible2()Z
    .locals 2

    .prologue
    .line 30
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "ADR6350"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static useVoiceRecognitionAudioPath()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 16
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x7

    if-ge v1, v2, :cond_1

    .line 26
    :cond_0
    :goto_0
    return v0

    .line 21
    :cond_1
    invoke-static {}, Lcom/vlingo/sdk/internal/core/DeviceWorkarounds;->isDeviceHTCIncredible2()Z

    move-result v1

    if-nez v1, :cond_0

    .line 26
    const/4 v0, 0x1

    goto :goto_0
.end method
