.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;
.super Ljava/lang/Object;
.source "NativeVoconInstance.java"


# static fields
.field private static volatile sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearInstance()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 22
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    if-eqz v0, :cond_0

    .line 24
    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->uninit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    :goto_0
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    .line 30
    :cond_0
    return-void

    .line 25
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    if-nez v0, :cond_0

    .line 8
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    .line 11
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    return-object v0
.end method

.method public static setInstance(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;)V
    .locals 0
    .param p0, "instance"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 16
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->clearInstance()V

    .line 17
    sput-object p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    .line 18
    return-void
.end method
