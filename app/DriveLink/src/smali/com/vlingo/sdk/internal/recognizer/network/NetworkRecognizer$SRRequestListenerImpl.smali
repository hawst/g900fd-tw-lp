.class Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;
.super Ljava/lang/Object;
.source "NetworkRecognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SRRequestListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$1;

    .prologue
    .line 228
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)V

    return-void
.end method


# virtual methods
.method public requestFailed(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 250
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    monitor-enter v1

    .line 251
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequestListener:Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$100(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 252
    monitor-exit v1

    .line 261
    :goto_0
    return-void

    .line 256
    :cond_0
    const/4 v0, -0x3

    if-ne p1, v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v0

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->TIMEOUT:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v3, "Timeout waiting for request"

    invoke-interface {v0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v0

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->FAIL_CONNECT:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v3, "Network error"

    invoke-interface {v0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 260
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resultReceived(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 6
    .param p1, "response"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 232
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    monitor-enter v1

    .line 233
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequestListener:Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$100(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 234
    monitor-exit v1

    .line 246
    :goto_0
    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$300(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;->getTimeSendStart()J

    move-result-wide v2

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartTime:J
    invoke-static {v4}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v2, v2

    # setter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSendStartDelta:I
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$202(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;I)I

    .line 240
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$300(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;->getTimeSendFinish()J

    move-result-wide v2

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartTime:J
    invoke-static {v4}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v2, v2

    # setter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSendFinishDelta:I
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$502(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;I)I

    .line 241
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$300(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;->getTimeGotResult()J

    move-result-wide v2

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartTime:J
    invoke-static {v4}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v2, v2

    # setter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mGotResultDelta:I
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$602(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;I)I

    .line 242
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartTime:J
    invoke-static {v4}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v2, v2

    # setter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mParseResultDelta:I
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$702(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;I)I

    .line 244
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->handleResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 245
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stateChanged(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 265
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    monitor-enter v1

    .line 266
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequestListener:Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$100(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 267
    monitor-exit v1

    .line 286
    :goto_0
    return-void

    .line 272
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 285
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 274
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v0

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->CONNECTING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognizerStateChanged(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 272
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
