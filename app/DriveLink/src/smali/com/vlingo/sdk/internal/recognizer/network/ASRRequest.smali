.class public Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.super Ljava/lang/Object;
.source "ASRRequest.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$NoAudioOutputStream;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$Connection;
    }
.end annotation


# static fields
.field public static final BOUNDRY:Ljava/lang/String; = "-------------------------------1878979834"

.field private static final HTTP_CHUNK_SIZE:I = 0x200

.field private static final MAX_RETRY_COUNT:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isExpectingAudio:Z

.field private isFinished:Z

.field private isTimeoutScheduled:Z

.field private mAudioOutputStream:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

.field private final mAudioQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;",
            ">;"
        }
    .end annotation
.end field

.field private final mCRC32:Ljava/util/zip/CRC32;

.field private mCancelledState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private final mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

.field private mConnectTimeout:I

.field private mConnection:Ljava/net/HttpURLConnection;

.field private mDisconnectedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mFinishedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mHandler:Landroid/os/Handler;

.field private final mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;",
            ">;"
        }
    .end annotation
.end field

.field private mMPOutputStream:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

.field private mNetworkThread:Landroid/os/HandlerThread;

.field private mReadTimeout:I

.field private mReceivedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mReceivingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mRequestId:I

.field private mResponse:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

.field private final mSRContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

.field private final mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

.field private final mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

.field private mState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mStreamingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mTimeGotResults:J

.field private mTimeSendFinish:J

.field private mTimeSendStart:J

.field private final mTimeoutTimer:Ljava/util/Timer;

.field private mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;Z)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "clientMeta"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p3, "softwareMeta"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .param p4, "srManager"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;
    .param p5, "timings"    # Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;
    .param p6, "sendAudio"    # Z

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 91
    iput v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnectTimeout:I

    .line 92
    iput v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReadTimeout:I

    .line 94
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mRequestId:I

    .line 97
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCRC32:Ljava/util/zip/CRC32;

    .line 111
    new-instance v0, Ljava/util/Timer;

    const-string/jumbo v1, "ASRRequest:TimeoutTimer"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeoutTimer:Ljava/util/Timer;

    .line 113
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mAudioQueue:Ljava/util/concurrent/BlockingQueue;

    .line 115
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnection:Ljava/net/HttpURLConnection;

    .line 116
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mMPOutputStream:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .line 117
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mAudioOutputStream:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    .line 118
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mResponse:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .line 121
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mDisconnectedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 122
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mStreamingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 123
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mFinishedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 124
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 125
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 126
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCancelledState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 129
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mDisconnectedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ctor(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p6, :cond_0

    const-string/jumbo v0, "with"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " audio stream"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/String;)V

    .line 135
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSRContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    .line 136
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    .line 137
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .line 138
    iput-object p4, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

    .line 140
    iput-boolean p6, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isExpectingAudio:Z

    .line 143
    iput-object p5, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .line 147
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mDisconnectedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mStreamingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->transitionTo(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 148
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mStreamingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mFinishedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->transitionTo(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 149
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mFinishedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->transitionTo(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 150
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->transitionTo(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 151
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->noTransition()V

    .line 152
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCancelledState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->noTransition()V

    .line 154
    if-eqz p6, :cond_1

    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    :goto_1
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mAudioOutputStream:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    .line 155
    return-void

    .line 133
    :cond_0
    const-string/jumbo v0, "without"

    goto :goto_0

    .line 154
    :cond_1
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$NoAudioOutputStream;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$NoAudioOutputStream;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    goto :goto_1
.end method

.method static synthetic access$1002(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertMetaTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/zip/CRC32;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCRC32:Ljava/util/zip/CRC32;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertCRCTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    return-void
.end method

.method static synthetic access$1502(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;J)J
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # J

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeSendStart:J

    return-wide p1
.end method

.method static synthetic access$1600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onError(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/concurrent/BlockingQueue;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isFinished:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isFinished:Z

    return p1
.end method

.method static synthetic access$2000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->startTimeoutTimer()V

    return-void
.end method

.method static synthetic access$2100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCancelledState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->runCurrentState()V

    return-void
.end method

.method static synthetic access$2300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->changeState(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Ljava/lang/Throwable;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestManager()Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/net/HttpURLConnection;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setConnection(Ljava/net/HttpURLConnection;)V

    return-void
.end method

.method static synthetic access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3108(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mRequestId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mRequestId:I

    return v0
.end method

.method static synthetic access$3200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getClientData()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getSoftwareData()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setOutputStream(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    return-void
.end method

.method static synthetic access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->buildEventElement()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertTextTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    return-void
.end method

.method static synthetic access$3800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isExpectingAudio()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4002(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;J)J
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # J

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeGotResults:J

    return-wide p1
.end method

.method static synthetic access$4100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/io/InputStream;)[B
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->consume(Ljava/io/InputStream;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    return-void
.end method

.method static synthetic access$4300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->stop()V

    return-void
.end method

.method static synthetic access$4400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getResponse()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    return-void
.end method

.method static synthetic access$4600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getTimingString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onTimeout(I)V

    return-void
.end method

.method private buildEventElement()Ljava/lang/String;
    .locals 5

    .prologue
    .line 341
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 343
    .local v3, "sb":Ljava/lang/StringBuffer;
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getEvents()Ljava/util/List;

    move-result-object v1

    .line 344
    .local v1, "eventList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 345
    const-string/jumbo v4, "<Events>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 346
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    .line 347
    .local v0, "event":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->getXML()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 349
    .end local v0    # "event":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    :cond_0
    const-string/jumbo v4, "</Events>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 352
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private buildMetaElement()Ljava/lang/String;
    .locals 10

    .prologue
    .line 357
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCapitalization()Ljava/lang/String;

    move-result-object v1

    .line 358
    .local v1, "capitalize":Ljava/lang/String;
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAutoPunctuation()Z

    move-result v7

    if-eqz v7, :cond_2

    const-string/jumbo v0, "true"

    .line 359
    .local v0, "autoPunctuate":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->isAudioStreamingEnabled()Z

    move-result v7

    if-eqz v7, :cond_3

    const-string/jumbo v6, "true"

    .line 364
    .local v6, "streaming":Ljava/lang/String;
    :goto_1
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCurrentText()Ljava/lang/String;

    move-result-object v2

    .line 366
    .local v2, "curText":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 368
    .local v5, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v7, "<Request "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 369
    const-string/jumbo v7, "FieldID"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldID()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 370
    const-string/jumbo v7, "AppID"

    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getAppId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 371
    const-string/jumbo v7, "FieldType"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldType()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 372
    const-string/jumbo v7, "FieldContext"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldContext()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 374
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 375
    const-string/jumbo v7, "CurrentText"

    invoke-static {v7, v2}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 376
    const-string/jumbo v7, "CursorPosition"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCursorPosition()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 379
    :cond_0
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_2
    const/4 v7, 0x6

    if-gt v4, v7, :cond_4

    .line 380
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Custom"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCustomParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 381
    .local v3, "customValue":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    .line 382
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Custom"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v3}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 379
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 358
    .end local v0    # "autoPunctuate":Ljava/lang/String;
    .end local v2    # "curText":Ljava/lang/String;
    .end local v3    # "customValue":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v5    # "sb":Ljava/lang/StringBuffer;
    .end local v6    # "streaming":Ljava/lang/String;
    :cond_2
    const-string/jumbo v0, "false"

    goto/16 :goto_0

    .line 359
    .restart local v0    # "autoPunctuate":Ljava/lang/String;
    :cond_3
    const-string/jumbo v6, "false"

    goto/16 :goto_1

    .line 386
    .restart local v2    # "curText":Ljava/lang/String;
    .restart local v4    # "i":I
    .restart local v5    # "sb":Ljava/lang/StringBuffer;
    .restart local v6    # "streaming":Ljava/lang/String;
    :cond_4
    const-string/jumbo v7, "StreamingAudio"

    invoke-static {v7, v6}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 387
    const-string/jumbo v7, "Punctuate"

    invoke-static {v7, v0}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 388
    const-string/jumbo v7, "Capitalize"

    invoke-static {v7, v1}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 389
    const-string/jumbo v7, "/>"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 391
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private changeState(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V
    .locals 2
    .param p1, "state"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .prologue
    .line 450
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setState("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    .line 451
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 452
    return-void
.end method

.method private consume(Ljava/io/InputStream;)[B
    .locals 4
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 522
    const/4 v1, 0x0

    .line 523
    .local v1, "bytesRead":I
    const/16 v3, 0x100

    new-array v0, v3, [B

    .line 524
    .local v0, "buffer":[B
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 525
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-ltz v1, :cond_0

    .line 526
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 527
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method

.method private getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mAudioOutputStream:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    return-object v0
.end method

.method private getAudioQueue()Ljava/util/concurrent/BlockingQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 487
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mAudioQueue:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method private getClientData()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    return-object v0
.end method

.method private getConnection()Ljava/net/HttpURLConnection;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnection:Ljava/net/HttpURLConnection;

    return-object v0
.end method

.method private getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mMPOutputStream:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    return-object v0
.end method

.method private getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSRContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    return-object v0
.end method

.method private getRequestManager()Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

    return-object v0
.end method

.method private getResponse()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mResponse:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    return-object v0
.end method

.method private getSoftwareData()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    return-object v0
.end method

.method private getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    return-object v0
.end method

.method private getTimingString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 421
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "HH:mm:ss.SSS"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 423
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 426
    .local v0, "buf":Ljava/lang/StringBuffer;
    :try_start_0
    const-string/jumbo v3, "Timing Data:\n\tSend start:\t\t\t\t"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getTimeSendStart()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n\tSend finish:\t\t\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getTimeSendFinish()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n\tTime got results:\t\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getTimeGotResult()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n\tAudio bytes written:\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->getBytesWritten()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 440
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 436
    :catch_0
    move-exception v1

    .line 437
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->TAG:Ljava/lang/String;

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertCRCTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 5
    .param p1, "out"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    const-string/jumbo v0, "checksum"

    const-string/jumbo v1, "text/crc32"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCRC32:Ljava/util/zip/CRC32;

    invoke-virtual {v3}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 409
    return-void
.end method

.method private insertMetaTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 395
    const-string/jumbo v0, "meta"

    const-string/jumbo v1, "text/xml"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->buildMetaElement()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 397
    const-string/jumbo v0, "META"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method private insertTextTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    const-string/jumbo v0, "text"

    const-string/jumbo v1, "text/plain; charset=utf-8"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 403
    const-string/jumbo v0, "TEXT"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V

    .line 404
    return-void
.end method

.method private isErrorState(I)Z
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 461
    if-gez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isExpectingAudio()Z
    .locals 1

    .prologue
    .line 465
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isExpectingAudio:Z

    return v0
.end method

.method private log(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 600
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 607
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;Ljava/lang/String;)V

    .line 608
    return-void
.end method

.method private log(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 603
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "EXCEPTION: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    .line 604
    return-void
.end method

.method private notifyListeners(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 325
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "notifyListeners("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    .line 329
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .line 330
    .local v1, "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isErrorState(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 331
    invoke-interface {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;->requestFailed(I)V

    goto :goto_0

    .line 333
    :cond_0
    invoke-interface {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;->stateChanged(I)V

    goto :goto_0

    .line 336
    .end local v1    # "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    :cond_1
    return-void
.end method

.method private notifyListeners(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 3
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 315
    const-string/jumbo v2, "notify resultReceived()"

    invoke-direct {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    .line 319
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .line 320
    .local v1, "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    invoke-interface {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;->resultReceived(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    goto :goto_0

    .line 322
    .end local v1    # "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    :cond_0
    return-void
.end method

.method private onError(I)V
    .locals 1
    .param p1, "reason"    # I

    .prologue
    .line 308
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->cancel(Z)V

    .line 309
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(I)V

    .line 310
    return-void
.end method

.method private onTimeout(I)V
    .locals 2
    .param p1, "reason"    # I

    .prologue
    const/4 v1, 0x1

    .line 301
    const-string/jumbo v0, "[LatencyCheck] onTimeout()"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/String;)V

    .line 302
    invoke-static {v1}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->setTimedOut(Z)V

    .line 303
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->cancel(Z)V

    .line 304
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(I)V

    .line 305
    return-void
.end method

.method private recordDetailedTiming(Ljava/lang/String;)V
    .locals 1
    .param p1, "event"    # Ljava/lang/String;

    .prologue
    .line 414
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;->recordAndTimeStampEvent(Ljava/lang/String;)V

    .line 417
    :cond_0
    return-void
.end method

.method private runCurrentState()V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 447
    return-void
.end method

.method private serverCheckLog(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 594
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "][ServerCheck] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    return-void
.end method

.method private setConnection(Ljava/net/HttpURLConnection;)V
    .locals 0
    .param p1, "connection"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 509
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnection:Ljava/net/HttpURLConnection;

    .line 510
    return-void
.end method

.method private setOutputStream(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 0
    .param p1, "out"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .prologue
    .line 513
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mMPOutputStream:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .line 514
    return-void
.end method

.method private setResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 517
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mResponse:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .line 518
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestManager()Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getGUttId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->setLastGuttID(Ljava/lang/String;)V

    .line 519
    return-void
.end method

.method private startTimeoutTimer()V
    .locals 4

    .prologue
    .line 276
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    if-nez v1, :cond_0

    .line 277
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "starting timeout timer ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getReadTimeout()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ms]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    .line 278
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    .line 295
    .local v0, "task":Ljava/util/TimerTask;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeoutTimer:Ljava/util/Timer;

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getReadTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 296
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    .line 298
    .end local v0    # "task":Ljava/util/TimerTask;
    :cond_0
    return-void
.end method

.method private stop()V
    .locals 2

    .prologue
    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[LatencyCheck] stop(): background processing halted (mNetworkThread), isTimeoutScheduled ? = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    .line 268
    :cond_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeoutTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 270
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeoutTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 271
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    .line 273
    :cond_1
    return-void
.end method


# virtual methods
.method public addListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    return-void
.end method

.method public cancel(Z)V
    .locals 1
    .param p1, "timedOut"    # Z

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->cancel(Z)V

    .line 196
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->finish()V

    .line 191
    return-void
.end method

.method public getConnectTimeout()I
    .locals 2

    .prologue
    .line 164
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnectTimeout:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getOptimalConnectTimeout()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnectTimeout:I

    goto :goto_0
.end method

.method public getReadTimeout()I
    .locals 2

    .prologue
    .line 168
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReadTimeout:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getOptimalConnectTimeout()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReadTimeout:I

    goto :goto_0
.end method

.method public getTimeGotResult()J
    .locals 2

    .prologue
    .line 240
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeGotResults:J

    return-wide v0
.end method

.method public getTimeSendFinish()J
    .locals 2

    .prologue
    .line 235
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeSendFinish:J

    return-wide v0
.end method

.method public getTimeSendStart()J
    .locals 2

    .prologue
    .line 230
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeSendStart:J

    return-wide v0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public isResponseReceived()Z
    .locals 1

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->isResponseReceived()Z

    move-result v0

    return v0
.end method

.method public removeListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 224
    return-void
.end method

.method public sendAudio([BII)V
    .locals 1
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->sendAudio([BII)V

    .line 186
    return-void
.end method

.method public setConnectTimeout(I)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .locals 0
    .param p1, "timeout"    # I

    .prologue
    .line 172
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnectTimeout:I

    .line 173
    return-object p0
.end method

.method public setReadTimeout(I)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .locals 0
    .param p1, "timeout"    # I

    .prologue
    .line 177
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReadTimeout:I

    .line 178
    return-object p0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 244
    const-string/jumbo v0, "[LatencyCheck] mNetworkThread.start()"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/String;)V

    .line 246
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ASRRequest:NetworkThread"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    .line 247
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 248
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mHandler:Landroid/os/Handler;

    .line 250
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->runCurrentState()V

    .line 251
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->startTimeoutTimer()V

    .line 252
    return-void
.end method
