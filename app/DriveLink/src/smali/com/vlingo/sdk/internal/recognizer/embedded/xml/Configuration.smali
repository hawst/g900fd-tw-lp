.class public Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;
.super Ljava/lang/Object;
.source "Configuration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;
    }
.end annotation


# static fields
.field public static final CONFIG_TAG_NAME:Ljava/lang/String; = "Configuration"

.field private static final DOMAIN_PARAM_NAME:Ljava/lang/String; = "domain"

.field private static final FCF_PARAM_NAME:Ljava/lang/String; = "fcf"


# instance fields
.field private domain:Ljava/lang/String;

.field private fcf:Ljava/lang/String;

.field private slotsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->slotsList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->fcf:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->domain:Ljava/lang/String;

    return-object p1
.end method

.method public static getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$1;)V

    return-object v0
.end method


# virtual methods
.method public getDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->domain:Ljava/lang/String;

    return-object v0
.end method

.method public getFcf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->fcf:Ljava/lang/String;

    return-object v0
.end method

.method public getSlotsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->slotsList:Ljava/util/List;

    return-object v0
.end method
