.class public Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;
.super Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
.source "MultiplexHttpConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection$MultiplexOutputStream;,
        Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection$MultiplexInputStream;
    }
.end annotation


# instance fields
.field private ivActiveRequest:Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

.field private ivActiveResponse:Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

.field private ivIn:Ljava/io/DataInputStream;

.field private ivOut:Ljava/io/DataOutputStream;

.field private ivRequestQueue:Lcom/vlingo/sdk/internal/recognizer/Queue;

.field private ivResponseQueue:Lcom/vlingo/sdk/internal/recognizer/Queue;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/http/URL;)V
    .locals 1
    .param p1, "url"    # Lcom/vlingo/sdk/internal/http/URL;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;-><init>(Lcom/vlingo/sdk/internal/http/URL;)V

    .line 37
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/Queue;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/Queue;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivRequestQueue:Lcom/vlingo/sdk/internal/recognizer/Queue;

    .line 38
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/Queue;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/Queue;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivResponseQueue:Lcom/vlingo/sdk/internal/recognizer/Queue;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->read(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;Lcom/vlingo/sdk/internal/http/custom/HttpRequest;[BII)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .param p2, "x2"    # [B
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->write(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;[BII)V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;Lcom/vlingo/sdk/internal/http/custom/HttpRequest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .param p2, "x2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->write(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;Lcom/vlingo/sdk/internal/http/custom/HttpRequest;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .param p2, "x2"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->write(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;[B)V

    return-void
.end method

.method private assignReadLock()V
    .locals 3

    .prologue
    .line 120
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivIn:Ljava/io/DataInputStream;

    monitor-enter v2

    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivResponseQueue:Lcom/vlingo/sdk/internal/recognizer/Queue;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/Queue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    .line 122
    .local v0, "nextResponse":Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivActiveResponse:Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    .line 127
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivIn:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 128
    monitor-exit v2

    .line 129
    return-void

    .line 128
    .end local v0    # "nextResponse":Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private assignWriteLock()V
    .locals 3

    .prologue
    .line 150
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    monitor-enter v2

    .line 151
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivRequestQueue:Lcom/vlingo/sdk/internal/recognizer/Queue;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/Queue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    .line 152
    .local v0, "nextRequest":Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivActiveRequest:Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    .line 157
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 158
    monitor-exit v2

    .line 159
    return-void

    .line 158
    .end local v0    # "nextRequest":Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getReadLock(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    .prologue
    .line 132
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivActiveResponse:Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    if-eq v0, p1, :cond_2

    .line 133
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivIn:Ljava/io/DataInputStream;

    monitor-enter v1

    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivActiveResponse:Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    if-nez v0, :cond_0

    .line 136
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->assignReadLock()V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivActiveResponse:Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, p1, :cond_1

    .line 143
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivIn:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 147
    :cond_2
    return-void

    .line 143
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private getWriteLock(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivActiveRequest:Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    if-eq v0, p1, :cond_2

    .line 163
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    monitor-enter v1

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivActiveRequest:Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    if-nez v0, :cond_0

    .line 166
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->assignWriteLock()V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivActiveRequest:Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, p1, :cond_1

    .line 173
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 177
    :cond_2
    return-void

    .line 173
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private read(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)I
    .locals 1
    .param p1, "response"    # Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->getReadLock(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)V

    .line 101
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivIn:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v0

    return v0
.end method

.method private write(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;I)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .param p2, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->getWriteLock(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V

    .line 116
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p2}, Ljava/io/DataOutputStream;->write(I)V

    .line 117
    return-void
.end method

.method private write(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;[B)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .param p2, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->getWriteLock(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V

    .line 111
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p2}, Ljava/io/DataOutputStream;->write([B)V

    .line 112
    return-void
.end method

.method private write(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;[BII)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .param p2, "b"    # [B
    .param p3, "off"    # I
    .param p4, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->getWriteLock(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V

    .line 106
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p2, p3, p4}, Ljava/io/DataOutputStream;->write([BII)V

    .line 107
    return-void
.end method


# virtual methods
.method public getInputStream(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)Ljava/io/DataInputStream;
    .locals 2
    .param p1, "response"    # Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    monitor-enter p0

    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivIn:Ljava/io/DataInputStream;

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;->getInputStream()Ljava/io/DataInputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivIn:Ljava/io/DataInputStream;

    .line 51
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection$MultiplexInputStream;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivIn:Ljava/io/DataInputStream;

    invoke-direct {v0, p0, v1, p1}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection$MultiplexInputStream;-><init>(Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;Ljava/io/InputStream;Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)V

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getOutputStream(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)Ljava/io/DataOutputStream;
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    monitor-enter p0

    .line 57
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;->getOutputStream()Ljava/io/DataOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    .line 60
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection$MultiplexOutputStream;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    invoke-direct {v0, p0, v1, p1}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection$MultiplexOutputStream;-><init>(Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;Ljava/io/OutputStream;Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V

    return-object v0

    .line 60
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method notifyRequestDone(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivOut:Ljava/io/DataOutputStream;

    monitor-enter v1

    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivActiveRequest:Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    if-eq v0, p1, :cond_0

    .line 76
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v2, "HTTP requests finished out of order"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 82
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->assignWriteLock()V

    .line 83
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    return-void
.end method

.method notifyResponseDone(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivIn:Ljava/io/DataInputStream;

    monitor-enter v1

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivActiveResponse:Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    if-eq v0, p1, :cond_0

    .line 89
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v2, "HTTP responses finished out of order"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 95
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->assignReadLock()V

    .line 96
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    return-void
.end method

.method public declared-synchronized openInteraction(Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;-><init>(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;)V

    .line 67
    .local v0, "dhi":Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setPath(Ljava/lang/String;)V

    .line 68
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivRequestQueue:Lcom/vlingo/sdk/internal/recognizer/Queue;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/Queue;->add(Ljava/lang/Object;)V

    .line 69
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;->ivResponseQueue:Lcom/vlingo/sdk/internal/recognizer/Queue;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getResponse()Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/Queue;->add(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    monitor-exit p0

    return-object v0

    .line 66
    .end local v0    # "dhi":Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
