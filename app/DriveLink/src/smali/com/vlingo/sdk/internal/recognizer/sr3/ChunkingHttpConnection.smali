.class public Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;
.super Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;
.source "ChunkingHttpConnection.java"


# instance fields
.field private ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

.field private ivDin:Ljava/io/InputStream;

.field private ivDout:Ljava/io/DataOutputStream;

.field private ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

.field private ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;I)V
    .locals 0
    .param p1, "con"    # Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    .param p2, "interaction"    # Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;
    .param p3, "requestID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p3}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;-><init>(I)V

    .line 48
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    .line 49
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    .line 50
    return-void
.end method

.method private static initConnection(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/util/Hashtable;ILcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;
    .locals 8
    .param p0, "connection"    # Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "client"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p4, "software"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .param p6, "clientRequestId"    # I
    .param p7, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/ClientMeta;",
            "Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/vlingo/sdk/internal/recognizer/SRContext;",
            ")",
            "Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    .local p5, "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0, p2}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->openInteraction(Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    move-result-object v3

    .line 81
    .local v3, "it":Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    move-result-object v5

    .line 82
    .local v5, "request":Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    invoke-virtual {v5, p1}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setMethod(Ljava/lang/String;)V

    .line 84
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->isAsrKeepAliveEnabled()Z

    move-result v6

    if-nez v6, :cond_0

    .line 85
    const-string/jumbo v6, "Connection"

    const-string/jumbo v7, "Close"

    invoke-virtual {v5, v6, v7}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_0
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    .line 90
    .local v2, "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {v2, p3, p4, p7}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->addStandardVlingoHttpHeaders(Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Lcom/vlingo/sdk/internal/recognizer/SRContext;)V

    .line 91
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->getHost()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {p5, v6, v7}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->addVLServiceCookies(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object p5

    .line 93
    invoke-virtual {v2}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 94
    .local v1, "hEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 95
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 96
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 99
    .end local v4    # "name":Ljava/lang/String;
    :cond_1
    invoke-static {p5}, Lcom/vlingo/sdk/internal/http/HttpUtil;->getCookies(Ljava/util/Hashtable;)Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "cookieStr":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 101
    const-string/jumbo v6, "Cookie"

    invoke-virtual {v5, v6, v0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_2
    new-instance v6, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;

    invoke-direct {v6, p0, v3, p6}, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;-><init>(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;I)V

    return-object v6
.end method

.method public static newConnection(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/util/Hashtable;ILcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;
    .locals 1
    .param p0, "connection"    # Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "client"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p4, "software"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .param p6, "clientRequestId"    # I
    .param p7, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/ClientMeta;",
            "Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/vlingo/sdk/internal/recognizer/SRContext;",
            ")",
            "Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    .local p5, "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static/range {p0 .. p7}, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->initConnection(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/util/Hashtable;ILcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;

    move-result-object v0

    return-object v0
.end method

.method public static newConnection(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Ljava/lang/String;Lcom/vlingo/sdk/internal/http/URL;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/util/Hashtable;ILcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;
    .locals 8
    .param p0, "connectionProvider"    # Lcom/vlingo/sdk/internal/net/ConnectionProvider;
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "url"    # Lcom/vlingo/sdk/internal/http/URL;
    .param p3, "client"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p4, "software"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .param p6, "clientRequestId"    # I
    .param p7, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/net/ConnectionProvider;",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/http/URL;",
            "Lcom/vlingo/sdk/internal/recognizer/ClientMeta;",
            "Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/vlingo/sdk/internal/recognizer/SRContext;",
            ")",
            "Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    .local p5, "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    invoke-direct {v0, p2}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;-><init>(Lcom/vlingo/sdk/internal/http/URL;)V

    .line 68
    .local v0, "hc":Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->open()V

    .line 69
    iget-object v2, p2, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object v7, p7

    invoke-static/range {v0 .. v7}, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->initConnection(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/util/Hashtable;ILcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivDin:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivDin:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 159
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 160
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivDout:Ljava/io/DataOutputStream;

    if-eqz v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivDout:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 161
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    if-eqz v0, :cond_3

    :try_start_3
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 162
    :cond_3
    :goto_3
    return-void

    .line 161
    :catch_0
    move-exception v0

    goto :goto_3

    .line 160
    :catch_1
    move-exception v0

    goto :goto_2

    .line 159
    :catch_2
    move-exception v0

    goto :goto_1

    .line 158
    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public finishRequest()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->finish()V

    .line 166
    return-void
.end method

.method public finishResponse()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getResponse()Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->finish()V

    .line 171
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    instance-of v0, v0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;

    if-nez v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->close()V

    .line 174
    :cond_0
    return-void
.end method

.method public getConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    return-object v0
.end method

.method public getIn()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivDin:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getResponse()Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivDin:Ljava/io/InputStream;

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivDin:Ljava/io/InputStream;

    return-object v0
.end method

.method public getOut()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->getOutputStream()Ljava/io/DataOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivDout:Ljava/io/DataOutputStream;

    .line 139
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivDout:Ljava/io/DataOutputStream;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivBoundary:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;-><init>(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    return-object v0
.end method

.method public getResponseHeaderField(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->getResponseHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "key":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 111
    const/4 v1, 0x0

    .line 112
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getResponse()Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getResponseHeaderFieldKey(I)Ljava/lang/String;
    .locals 4
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getResponse()Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->getHeaderNames()Ljava/util/Enumeration;

    move-result-object v1

    .line 118
    .local v1, "hEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    const/4 v0, 0x0

    .line 119
    .local v0, "field":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-gt v2, p1, :cond_0

    .line 120
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-nez v3, :cond_1

    .line 121
    const/4 v0, 0x0

    .line 125
    .end local v0    # "field":Ljava/lang/String;
    :cond_0
    return-object v0

    .line 123
    .restart local v0    # "field":Ljava/lang/String;
    :cond_1
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "field":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 119
    .restart local v0    # "field":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    return-void
.end method
