.class public Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;
.super Ljava/lang/Object;
.source "VLResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;
    }
.end annotation


# static fields
.field private static dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings; = null

.field static final path:Ljava/lang/String; = "/sdcard/vlingo"

.field static reponseCount:I


# instance fields
.field private attributeIndex:B

.field currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

.field currentResponse:Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

.field private elementIndex:B

.field sectionParsers:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;",
            ">;"
        }
    .end annotation
.end field

.field private final xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

.field private final xmlElements:Lcom/vlingo/sdk/internal/util/ToIntHashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    .line 50
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->reponseCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/16 v0, 0x32

    iput-byte v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->elementIndex:B

    .line 44
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->attributeIndex:B

    .line 52
    iput-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    .line 53
    iput-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentResponse:Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    .line 56
    invoke-static {}, Lcom/vlingo/sdk/internal/util/ToIntHashtableFactory;->createNewHashtable()Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->xmlElements:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    .line 57
    invoke-static {}, Lcom/vlingo/sdk/internal/util/ToIntHashtableFactory;->createNewHashtable()Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    .line 58
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->sectionParsers:Ljava/util/Vector;

    .line 59
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->initParsers()V

    .line 60
    invoke-static {}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->initDebugSettings()V

    .line 61
    return-void
.end method

.method static synthetic access$000()Lcom/vlingo/sdk/util/SDKDebugSettings;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;I)Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->getParserForElement(I)Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    move-result-object v0

    return-object v0
.end method

.method private static clearOldRecordings()V
    .locals 9

    .prologue
    .line 83
    sget-object v7, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    invoke-virtual {v7}, Lcom/vlingo/sdk/util/SDKDebugSettings;->getServerResponseLoggingState()Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    move-result-object v7

    sget-object v8, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->SAVE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    if-ne v7, v8, :cond_0

    .line 85
    const/4 v7, 0x0

    sput v7, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->reponseCount:I

    .line 87
    new-instance v1, Ljava/io/File;

    const-string/jumbo v7, "/sdcard/vlingo"

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 90
    .local v1, "dir":Ljava/io/File;
    new-instance v3, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$1;

    invoke-direct {v3}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$1;-><init>()V

    .line 95
    .local v3, "fileFilter":Ljava/io/FilenameFilter;
    invoke-virtual {v1, v3}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v4

    .line 96
    .local v4, "files":[Ljava/io/File;
    move-object v0, v4

    .local v0, "arr$":[Ljava/io/File;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v2, v0, v5

    .line 97
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 96
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 100
    .end local v2    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private getParserForElement(I)Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
    .locals 3
    .param p1, "elementType"    # I

    .prologue
    .line 190
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->sectionParsers:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 191
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->sectionParsers:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    .line 192
    .local v1, "p":Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
    invoke-virtual {v1, p1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;->handlesElement(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 196
    .end local v1    # "p":Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
    :goto_1
    return-object v1

    .line 190
    .restart local v1    # "p":Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    .end local v1    # "p":Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static initDebugSettings()V
    .locals 3

    .prologue
    .line 65
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/VLSdk;->getDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;

    move-result-object v1

    sput-object v1, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    .line 66
    sget-object v1, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    invoke-virtual {v1}, Lcom/vlingo/sdk/util/SDKDebugSettings;->getServerResponseLoggingState()Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->NONE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    if-eq v1, v2, :cond_0

    .line 67
    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, "/sdcard/vlingo"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 69
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    invoke-static {}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->clearOldRecordings()V

    goto :goto_0
.end method

.method private onParseBegin([C)V
    .locals 3
    .param p1, "xml"    # [C

    .prologue
    .line 183
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->sectionParsers:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 184
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->sectionParsers:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    .line 185
    .local v1, "p":Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
    invoke-virtual {v1, p1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;->onParseBegin([C)V

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 187
    .end local v1    # "p":Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
    :cond_0
    return-void
.end method

.method public static resetResponseCount()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->reponseCount:I

    .line 79
    invoke-static {}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->initDebugSettings()V

    .line 80
    return-void
.end method


# virtual methods
.method public addParser(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;)V
    .locals 1
    .param p1, "parser"    # Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->sectionParsers:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 104
    return-void
.end method

.method public getResponse()Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentResponse:Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    return-object v0
.end method

.method public initParsers()V
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;-><init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->addParser(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;)V

    .line 108
    new-instance v0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;-><init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->addParser(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;)V

    .line 109
    new-instance v0, Lcom/vlingo/sdk/internal/vlservice/response/ActionParser;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionParser;-><init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->addParser(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;)V

    .line 110
    return-void
.end method

.method public onSectionComplete()V
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    .line 180
    return-void
.end method

.method public parseResponseXml(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
    .locals 1
    .param p1, "responseXml"    # Ljava/lang/String;

    .prologue
    .line 113
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->parseResponseXml(Ljava/lang/String;Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;)Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method public parseResponseXml(Ljava/lang/String;Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;)Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
    .locals 12
    .param p1, "responseXml"    # Ljava/lang/String;
    .param p2, "initialResponse"    # Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    .prologue
    .line 121
    :try_start_0
    sget-object v2, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    if-eqz v2, :cond_0

    .line 122
    sget-object v2, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    invoke-virtual {v2}, Lcom/vlingo/sdk/util/SDKDebugSettings;->getServerResponseLoggingState()Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    move-result-object v2

    sget-object v3, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->SAVE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    if-ne v2, v3, :cond_1

    .line 124
    new-instance v9, Ljava/io/FileWriter;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "/sdcard/vlingo/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    invoke-virtual {v3}, Lcom/vlingo/sdk/util/SDKDebugSettings;->getmRawServerLogBase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->reponseCount:I

    add-int/lit8 v4, v3, 0x1

    sput v4, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->reponseCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    .line 125
    .local v9, "fstream":Ljava/io/FileWriter;
    new-instance v11, Ljava/io/BufferedWriter;

    invoke-direct {v11, v9}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 126
    .local v11, "out":Ljava/io/BufferedWriter;
    invoke-virtual {v11, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v11}, Ljava/io/BufferedWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    .end local v9    # "fstream":Ljava/io/FileWriter;
    .end local v11    # "out":Ljava/io/BufferedWriter;
    :cond_0
    :goto_0
    iput-object p2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentResponse:Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    .line 140
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    .line 142
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 143
    .local v1, "chars":[C
    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->onParseBegin([C)V

    .line 144
    new-instance v0, Lcom/vlingo/sdk/internal/xml/XmlParser;

    const/4 v2, 0x0

    array-length v3, v1

    new-instance v4, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;

    invoke-direct {v4, p0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;-><init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V

    iget-object v5, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->xmlElements:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    iget-object v6, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/vlingo/sdk/internal/xml/XmlParser;-><init>([CIILcom/vlingo/sdk/internal/xml/XmlHandler;Lcom/vlingo/sdk/internal/util/ToIntHashtable;Lcom/vlingo/sdk/internal/util/ToIntHashtable;ZZ)V

    .line 145
    .local v0, "parser":Lcom/vlingo/sdk/internal/xml/XmlParser;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/xml/XmlParser;->parseXml()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 153
    .end local v0    # "parser":Lcom/vlingo/sdk/internal/xml/XmlParser;
    .end local v1    # "chars":[C
    :goto_1
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentResponse:Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    return-object v2

    .line 128
    :cond_1
    :try_start_2
    sget-object v2, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    invoke-virtual {v2}, Lcom/vlingo/sdk/util/SDKDebugSettings;->getServerResponseLoggingState()Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    move-result-object v2

    sget-object v3, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->REPLAY:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    if-ne v2, v3, :cond_0

    .line 129
    new-instance v9, Ljava/io/FileReader;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "/sdcard/vlingo/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->dbgSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    invoke-virtual {v3}, Lcom/vlingo/sdk/util/SDKDebugSettings;->getmRawServerLogBase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->reponseCount:I

    add-int/lit8 v4, v3, 0x1

    sput v4, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->reponseCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    .line 130
    .local v9, "fstream":Ljava/io/FileReader;
    new-instance v10, Ljava/io/BufferedReader;

    invoke-direct {v10, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 131
    .local v10, "in":Ljava/io/BufferedReader;
    invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p1

    .line 132
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 135
    .end local v9    # "fstream":Ljava/io/FileReader;
    .end local v10    # "in":Ljava/io/BufferedReader;
    :catch_0
    move-exception v2

    goto :goto_0

    .line 147
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public registerAttribute(Ljava/lang/String;)I
    .locals 2
    .param p1, "attrName"    # Ljava/lang/String;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/util/ToIntHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/util/ToIntHashtable;->get(Ljava/lang/Object;)I

    move-result v0

    .line 175
    :goto_0
    return v0

    .line 173
    :cond_0
    iget-byte v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->attributeIndex:B

    add-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->attributeIndex:B

    .line 174
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    iget-byte v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->attributeIndex:B

    invoke-interface {v0, p1, v1}, Lcom/vlingo/sdk/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 175
    iget-byte v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->attributeIndex:B

    goto :goto_0
.end method

.method public registerElement(Ljava/lang/String;)I
    .locals 2
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->xmlElements:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/util/ToIntHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->xmlElements:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/util/ToIntHashtable;->get(Ljava/lang/Object;)I

    move-result v0

    .line 166
    :goto_0
    return v0

    .line 164
    :cond_0
    iget-byte v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->elementIndex:B

    add-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->elementIndex:B

    .line 165
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->xmlElements:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    iget-byte v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->elementIndex:B

    invoke-interface {v0, p1, v1}, Lcom/vlingo/sdk/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 166
    iget-byte v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->elementIndex:B

    goto :goto_0
.end method
