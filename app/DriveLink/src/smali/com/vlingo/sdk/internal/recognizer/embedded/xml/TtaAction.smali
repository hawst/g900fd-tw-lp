.class public Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;
.super Ljava/lang/Object;
.source "TtaAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;
    }
.end annotation


# static fields
.field public static final ACTION_TAG_NAME:Ljava/lang/String; = "Action"


# instance fields
.field private name:Ljava/lang/String;

.field private parameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->name:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;Ljava/util/Map;)Ljava/util/Map;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;
    .param p1, "x1"    # Ljava/util/Map;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->parameters:Ljava/util/Map;

    return-object p1
.end method

.method public static getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$1;)V

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getParamValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "paramName"    # Ljava/lang/String;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->parameters:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getParameterNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->parameters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getParametersCount()I
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->parameters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
