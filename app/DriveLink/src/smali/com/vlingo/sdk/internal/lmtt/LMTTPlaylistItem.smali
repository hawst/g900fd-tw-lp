.class public Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;
.super Lcom/vlingo/sdk/internal/lmtt/LMTTItem;
.source "LMTTPlaylistItem.java"


# instance fields
.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 1
    .param p1, "uid"    # J
    .param p3, "changeType"    # Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;-><init>(Ljava/lang/String;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "uid"    # J
    .param p4, "changeType"    # Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 15
    sget-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;-><init>(Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 17
    if-nez p1, :cond_0

    .line 18
    const-string/jumbo p1, ""

    .line 21
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;->title:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public getDelXML(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 29
    const-string/jumbo v0, "<PD"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    const-string/jumbo v0, " uid=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;->uid:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 32
    const-string/jumbo v0, "\"/>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    return-void
.end method

.method public getInsXML(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 38
    const-string/jumbo v0, "<PU"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    const-string/jumbo v0, " uid=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;->uid:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 41
    const-string/jumbo v0, "\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    const-string/jumbo v0, " ttl=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;->title:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 44
    const-string/jumbo v0, "\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    const-string/jumbo v0, "/>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    return-void
.end method

.method public getUpXML(Ljava/lang/StringBuilder;)V
    .locals 0
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;->getInsXML(Ljava/lang/StringBuilder;)V

    .line 51
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "LMTTPlaylistItem | uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;->uid:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | changeType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;->changeType:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
