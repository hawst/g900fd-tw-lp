.class public Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
.super Ljava/lang/Object;
.source "VHttpConnection.java"


# instance fields
.field private ivChunkSize:I

.field protected ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

.field private m_HttpInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

.field private m_URL:Lcom/vlingo/sdk/internal/http/URL;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/http/URL;)V
    .locals 1
    .param p1, "url"    # Lcom/vlingo/sdk/internal/http/URL;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivChunkSize:I

    .line 37
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->m_URL:Lcom/vlingo/sdk/internal/http/URL;

    .line 38
    return-void
.end method

.method private primeConnection()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;-><init>(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;)V

    .line 98
    .local v0, "dhi":Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    move-result-object v2

    .line 99
    .local v2, "request":Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    const-string/jumbo v4, "/voicepad/sr"

    invoke-virtual {v2, v4}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setPath(Ljava/lang/String;)V

    .line 100
    const-string/jumbo v4, "HEAD"

    invoke-virtual {v2, v4}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setMethod(Ljava/lang/String;)V

    .line 101
    const-string/jumbo v4, "Connection"

    const-string/jumbo v5, "keep-alive"

    invoke-virtual {v2, v4, v5}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->finish()V

    .line 105
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getResponse()Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    move-result-object v3

    .line 106
    .local v3, "response":Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 107
    .local v1, "in":Ljava/io/InputStream;
    :cond_0
    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 108
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getChunkSize()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivChunkSize:I

    return v0
.end method

.method public getConnectionDetails()Lcom/vlingo/sdk/internal/net/ConnectionResult;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;->getConnectionDetails()Lcom/vlingo/sdk/internal/net/ConnectionResult;

    move-result-object v0

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->m_URL:Lcom/vlingo/sdk/internal/http/URL;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    return-object v0
.end method

.method public getHttpInteraction()Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->m_HttpInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    return-object v0
.end method

.method public getInputStream(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)Ljava/io/DataInputStream;
    .locals 1
    .param p1, "response"    # Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;->getInputStream()Ljava/io/DataInputStream;

    move-result-object v0

    return-object v0
.end method

.method public getOutputStream(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)Ljava/io/DataOutputStream;
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;->getOutputStream()Ljava/io/DataOutputStream;

    move-result-object v0

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->m_URL:Lcom/vlingo/sdk/internal/http/URL;

    iget v0, v0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    return v0
.end method

.method public getVStreamConnection()Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    return-object v0
.end method

.method public isOpen()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method notifyRequestDone(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    return-void
.end method

.method notifyResponseDone(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)V
    .locals 0
    .param p1, "response"    # Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    return-void
.end method

.method public open()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->m_URL:Lcom/vlingo/sdk/internal/http/URL;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/custom/VConnectionFactory;->createConnection(Lcom/vlingo/sdk/internal/http/URL;)Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    .line 47
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Connection factory returned null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->getConnectionDetails()Lcom/vlingo/sdk/internal/net/ConnectionResult;

    move-result-object v0

    iget v0, v0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->connectionType:I

    if-eqz v0, :cond_1

    .line 52
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->primeConnection()V

    .line 55
    :cond_1
    return-void
.end method

.method public openInteraction(Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->m_HttpInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    if-eqz v0, :cond_0

    .line 118
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "New HTTP interaction attempted on non-keep-alive connection"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;-><init>(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->m_HttpInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    .line 121
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->m_HttpInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setPath(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->m_HttpInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    move-result-object v0

    const-string/jumbo v1, "Transfer-Encoding"

    const-string/jumbo v2, "chunked"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->m_HttpInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    return-object v0
.end method

.method public setChunkSize(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->ivChunkSize:I

    .line 74
    return-void
.end method
