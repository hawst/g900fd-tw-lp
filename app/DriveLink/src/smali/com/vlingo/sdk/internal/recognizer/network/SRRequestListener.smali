.class public interface abstract Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
.super Ljava/lang/Object;
.source "SRRequestListener.java"


# static fields
.field public static final CONNECTING:I = 0x1

.field public static final CONNECT_FAILURE:I = -0x1

.field public static final RECEIVING:I = 0x3

.field public static final RECORDER_ERROR:I = -0x2

.field public static final SENDING:I = 0x2

.field public static final TIMED_OUT:I = -0x3


# virtual methods
.method public abstract requestFailed(I)V
.end method

.method public abstract resultReceived(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
.end method

.method public abstract stateChanged(I)V
.end method
