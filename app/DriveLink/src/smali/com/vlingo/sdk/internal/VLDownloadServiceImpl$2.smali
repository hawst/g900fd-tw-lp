.class Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$2;
.super Ljava/lang/Object;
.source "VLDownloadServiceImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/util/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->getFilters(Lcom/vlingo/sdk/filedownload/VLDownloadData;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/vlingo/sdk/util/Predicate",
        "<",
        "Lcom/vlingo/sdk/filedownload/VLDownloadFilter;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;

.field final synthetic val$data:Lcom/vlingo/sdk/filedownload/VLDownloadData;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;Lcom/vlingo/sdk/filedownload/VLDownloadData;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$2;->this$0:Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;

    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$2;->val$data:Lcom/vlingo/sdk/filedownload/VLDownloadData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/vlingo/sdk/filedownload/VLDownloadFilter;)Z
    .locals 1
    .param p1, "object"    # Lcom/vlingo/sdk/filedownload/VLDownloadFilter;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$2;->val$data:Lcom/vlingo/sdk/filedownload/VLDownloadData;

    invoke-interface {p1, v0}, Lcom/vlingo/sdk/filedownload/VLDownloadFilter;->apply(Lcom/vlingo/sdk/filedownload/VLDownloadData;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 129
    check-cast p1, Lcom/vlingo/sdk/filedownload/VLDownloadFilter;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$2;->apply(Lcom/vlingo/sdk/filedownload/VLDownloadFilter;)Z

    move-result v0

    return v0
.end method
