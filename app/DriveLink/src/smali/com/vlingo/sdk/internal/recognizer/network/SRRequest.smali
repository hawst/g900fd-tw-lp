.class public interface abstract Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
.super Ljava/lang/Object;
.source "SRRequest.java"


# static fields
.field public static final DEFAULT_TIMEOUT:I = -0x1


# virtual methods
.method public abstract addListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V
.end method

.method public abstract cancel(Z)V
.end method

.method public abstract finish()V
.end method

.method public abstract getTimeGotResult()J
.end method

.method public abstract getTimeSendFinish()J
.end method

.method public abstract getTimeSendStart()J
.end method

.method public abstract isCancelled()Z
.end method

.method public abstract isResponseReceived()Z
.end method

.method public abstract removeListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V
.end method

.method public abstract sendAudio([BII)V
.end method
