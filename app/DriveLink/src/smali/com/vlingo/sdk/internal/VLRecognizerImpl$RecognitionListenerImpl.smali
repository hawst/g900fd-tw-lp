.class Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;
.super Ljava/lang/Object;
.source "VLRecognizerImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/VLRecognizerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecognitionListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;

    .prologue
    .line 418
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;-><init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)V

    return-void
.end method


# virtual methods
.method public onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 6
    .param p1, "response"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 479
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$200(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v3

    if-eq p0, v3, :cond_0

    .line 514
    :goto_0
    return-void

    .line 486
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->isError()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 487
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getFirstMessage()Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    move-result-object v1

    .line 490
    .local v1, "msg":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v3

    sget-object v4, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SERVER:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 494
    .end local v1    # "msg":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :cond_1
    new-instance v2, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;

    invoke-direct {v2, p1}, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;-><init>(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 496
    .local v2, "vlResult":Lcom/vlingo/sdk/recognition/VLRecognitionResult;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/VLSdk;->doesUseEDM()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2}, Lcom/vlingo/sdk/internal/edm/EDMResultsProcessor;->hasNluAction(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 498
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$900(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # invokes: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getSRContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    invoke-static {v4}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$400(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/vlingo/sdk/internal/edm/EDMResultsProcessor;->conformResult(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    move-result-object v2

    .line 501
    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->hasWarnings()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 502
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getFirstMessage()Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    move-result-object v1

    .line 503
    .restart local v1    # "msg":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->getCode()Ljava/lang/String;

    move-result-object v0

    .line 504
    .local v0, "code":Ljava/lang/String;
    const-string/jumbo v3, "NothingRecognized"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getResultString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 505
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v3

    sget-object v4, Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;->WARNING_NOTHING_RECOGNIZED:Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)V

    .line 512
    .end local v0    # "code":Ljava/lang/String;
    .end local v1    # "msg":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyResult(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    goto :goto_0

    .line 509
    .restart local v0    # "code":Ljava/lang/String;
    .restart local v1    # "msg":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :cond_4
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v3

    sget-object v4, Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;->WARNING_SERVER:Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V
    .locals 2
    .param p1, "recError"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$200(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 475
    :goto_0
    return-void

    .line 454
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$7;->$SwitchMap$com$vlingo$sdk$internal$recognizer$RecognizerListener$RecognizerError:[I

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 456
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SERVER:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v0, v1, p2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v0, v1, p2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 462
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NO_MATCH:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v0, v1, p2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 465
    :pswitch_3
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NETWORK:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v0, v1, p2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 468
    :pswitch_4
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_AUDIO:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v0, v1, p2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 471
    :pswitch_5
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SPEECH_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v0, v1, p2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 454
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onRecognizerStateChanged(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V
    .locals 3
    .param p1, "recState"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 421
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$200(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v1

    if-eq p0, v1, :cond_0

    .line 444
    :goto_0
    return-void

    .line 427
    :cond_0
    sget-object v1, Lcom/vlingo/sdk/internal/VLRecognizerImpl$7;->$SwitchMap$com$vlingo$sdk$internal$recognizer$RecognizerListener$RecognizerState:[I

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 429
    :pswitch_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyEvent(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    goto :goto_0

    .line 432
    :pswitch_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->CONNECTING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyEvent(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    goto :goto_0

    .line 435
    :pswitch_2
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->THINKING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyEvent(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    .line 436
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/VLSdk;->getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    move-result-object v0

    .line 437
    .local v0, "edm":Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;
    invoke-interface {v0}, Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;->increasePriority()V

    goto :goto_0

    .line 440
    .end local v0    # "edm":Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;
    :pswitch_3
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyRmsChange(Ljava/lang/Object;)V

    goto :goto_0

    .line 427
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
