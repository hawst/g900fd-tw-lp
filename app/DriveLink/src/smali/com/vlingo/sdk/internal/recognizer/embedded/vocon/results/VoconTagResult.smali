.class public final Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
.source "VoconTagResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    }
.end annotation


# static fields
.field public static final ITEMS:Ljava/lang/String; = "_items"

.field public static final NAME:Ljava/lang/String; = "_name"

.field private static final PHONE_NUMBER_TAG:Ljava/lang/String; = "tizen#PHONENUMBER_TAG"

.field private static final SPECIAL_TAGS_TABLES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private _items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;",
            ">;"
        }
    .end annotation
.end field

.field private _name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 32
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    const-string/jumbo v1, "tizen#ORDINAL_TAG"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "first"

    aput-object v3, v2, v5

    const-string/jumbo v3, "second"

    aput-object v3, v2, v6

    const-string/jumbo v3, "third"

    aput-object v3, v2, v7

    const-string/jumbo v3, "fourth"

    aput-object v3, v2, v8

    const-string/jumbo v3, "fifth"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "sixth"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "seventh"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "eighth"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "ninth"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "tenth"

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const-string/jumbo v1, "tizen#PHONELOCATION_TAG"

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "home"

    aput-object v3, v2, v5

    const-string/jumbo v3, "work"

    aput-object v3, v2, v6

    const-string/jumbo v3, "mobile"

    aput-object v3, v2, v7

    const-string/jumbo v3, "other"

    aput-object v3, v2, v8

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->SPECIAL_TAGS_TABLES:Ljava/util/Map;

    .line 36
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;-><init>()V

    .line 277
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$1;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_name:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_items:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_items:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public addTagNames(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 330
    .local p1, "tagNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_name:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 331
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_items:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 332
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_items:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;

    .line 333
    .local v1, "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    invoke-virtual {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->addTagNames(Ljava/util/List;)V

    goto :goto_0

    .line 336
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    :cond_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_name:Ljava/lang/String;

    return-object v0
.end method

.method public getRecognizedUtterance(Z)Ljava/lang/String;
    .locals 7
    .param p1, "forDisplay"    # Z

    .prologue
    .line 302
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 303
    .local v4, "utt":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getResults()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getResults()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 304
    sget-object v5, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->SPECIAL_TAGS_TABLES:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    .line 305
    .local v3, "specialTagTable":[Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 306
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getResults()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;

    .line 307
    .local v2, "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 308
    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    :cond_0
    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->getUserId()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->intValue()I

    move-result v5

    aget-object v5, v3, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 313
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getResults()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;

    .line 314
    .restart local v2    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_2

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_name:Ljava/lang/String;

    const-string/jumbo v6, "tizen#PHONENUMBER_TAG"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 315
    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    :cond_2
    invoke-virtual {v2, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->getRecognizedUtterance(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 321
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    .end local v3    # "specialTagTable":[Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_name:Ljava/lang/String;

    const-string/jumbo v6, "tizen#PHONENUMBER_TAG"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    if-eqz p1, :cond_4

    .line 322
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/vlingo/sdk/internal/util/StringUtils;->formatPhoneNumber(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 325
    :goto_2
    return-object v0

    :cond_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public getResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_items:Ljava/util/List;

    return-object v0
.end method

.method public getTag(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    .locals 5
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getResults()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;

    .line 346
    .local v1, "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    instance-of v4, v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    if-eqz v4, :cond_0

    move-object v3, v1

    .line 347
    check-cast v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    .line 348
    .local v3, "tagResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 358
    .end local v1    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    .end local v3    # "tagResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    :goto_0
    return-object v3

    .line 351
    .restart local v1    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    .restart local v3    # "tagResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    :cond_1
    invoke-virtual {v3, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getTag(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    move-result-object v2

    .line 352
    .local v2, "tag":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 353
    goto :goto_0

    .line 358
    .end local v1    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    .end local v2    # "tag":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    .end local v3    # "tagResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method
