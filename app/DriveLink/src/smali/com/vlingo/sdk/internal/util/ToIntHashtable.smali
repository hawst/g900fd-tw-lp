.class public interface abstract Lcom/vlingo/sdk/internal/util/ToIntHashtable;
.super Ljava/lang/Object;
.source "ToIntHashtable.java"


# virtual methods
.method public abstract clear()V
.end method

.method public abstract contains(I)Z
.end method

.method public abstract containsKey(Ljava/lang/Object;)Z
.end method

.method public abstract get(Ljava/lang/Object;)I
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract keys()Ljava/util/Enumeration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract put(Ljava/lang/Object;I)I
.end method

.method public abstract remove(Ljava/lang/Object;)I
.end method

.method public abstract size()I
.end method
