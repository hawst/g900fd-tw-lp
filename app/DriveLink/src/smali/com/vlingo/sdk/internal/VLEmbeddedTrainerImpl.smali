.class public Lcom/vlingo/sdk/internal/VLEmbeddedTrainerImpl;
.super Lcom/vlingo/sdk/internal/VLComponentImpl;
.source "VLEmbeddedTrainerImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;


# instance fields
.field private voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 1
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLComponentImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 19
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->bindToInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedTrainerImpl;->voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    .line 20
    return-void
.end method


# virtual methods
.method public bridge synthetic destroy()V
    .locals 0

    .prologue
    .line 11
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->destroy()V

    return-void
.end method

.method public doesDependOnSettings()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic isValid()Z
    .locals 1

    .prologue
    .line 11
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->isValid()Z

    move-result v0

    return v0
.end method

.method onDestroy()V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedTrainerImpl;->voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedTrainerImpl;->voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->unbindFromInstance(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedTrainerImpl;->voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    .line 28
    :cond_0
    return-void
.end method

.method public updateSlots(Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;)Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;
    .locals 1
    .param p1, "callback"    # Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;
    .param p2, "slotsUpdate"    # Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLEmbeddedTrainerImpl;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLEmbeddedTrainerImpl;->voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlots(Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;)Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;

    move-result-object v0

    .line 37
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
