.class public Lcom/vlingo/sdk/internal/audio/SpeexJNI;
.super Ljava/lang/Object;
.source "SpeexJNI.java"


# static fields
.field private static nextID:J


# instance fields
.field private ID:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    sget-wide v0, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->nextID:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    sput-wide v0, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->nextID:J

    iput-wide v0, p0, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->ID:J

    .line 14
    return-void
.end method

.method public static init()V
    .locals 1

    .prologue
    .line 69
    const-string/jumbo v0, "libogg.so"

    invoke-static {v0}, Lcom/vlingo/sdk/util/SystemUtil;->loadLibrary(Ljava/lang/String;)V

    .line 70
    const-string/jumbo v0, "libspeex.so"

    invoke-static {v0}, Lcom/vlingo/sdk/util/SystemUtil;->loadLibrary(Ljava/lang/String;)V

    .line 71
    const-string/jumbo v0, "libSpeexJNI.so"

    invoke-static {v0}, Lcom/vlingo/sdk/util/SystemUtil;->loadLibrary(Ljava/lang/String;)V

    .line 72
    return-void
.end method


# virtual methods
.method public native Encode(J[S[BII)I
.end method

.method public native Initialize(JIIIIIIFFFF)I
.end method

.method public encode([S[BII)I
    .locals 7
    .param p1, "input"    # [S
    .param p2, "output"    # [B
    .param p3, "maxOutputLen"    # I
    .param p4, "isEnd"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 41
    iget-wide v1, p0, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->ID:J

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->Encode(J[S[BII)I

    move-result v0

    return v0
.end method

.method public initialize(IIIIIIFFFF)I
    .locals 13
    .param p1, "wb"    # I
    .param p2, "quality"    # I
    .param p3, "complexity"    # I
    .param p4, "vbr"    # I
    .param p5, "vad"    # I
    .param p6, "useSilenceDetection"    # I
    .param p7, "silenceThreshold"    # F
    .param p8, "minVoiceDuration"    # F
    .param p9, "voicePortion"    # F
    .param p10, "minVoiceLevel"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 30
    iget-wide v1, p0, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->ID:J

    move-object v0, p0

    move v3, p1

    move v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    invoke-virtual/range {v0 .. v12}, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->Initialize(JIIIIIIFFFF)I

    move-result v0

    return v0
.end method
