.class Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;
.super Ljava/lang/Object;
.source "Recognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/Recognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataReaderListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;

    .prologue
    .line 330
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;-><init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V

    return-void
.end method


# virtual methods
.method public onDataAvailable([B[S)V
    .locals 3
    .param p1, "encodedBytes"    # [B
    .param p2, "rawShorts"    # [S

    .prologue
    .line 432
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    monitor-enter v1

    .line 433
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 434
    monitor-exit v1

    .line 460
    :goto_0
    return-void

    .line 439
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 459
    :cond_1
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 441
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 442
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->processAudio([S)V

    goto :goto_1

    .line 446
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->processAudio([B)V

    goto :goto_1

    .line 451
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 452
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->processAudio([B)V

    .line 454
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->processAudio([S)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 439
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onError(Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;Ljava/lang/String;)V
    .locals 4
    .param p1, "code"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 464
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    monitor-enter v1

    .line 465
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 466
    monitor-exit v1

    .line 473
    :goto_0
    return-void

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->READER_ERROR:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const/4 v3, 0x0

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    invoke-static {v0, v2, p2, v3}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    .line 472
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onRMSDataAvailable(I)V
    .locals 3
    .param p1, "energy"    # I

    .prologue
    .line 477
    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->RMS_CHANGED:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleStateChange(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V

    .line 480
    :cond_0
    return-void
.end method

.method public onStarted()V
    .locals 4

    .prologue
    .line 334
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    monitor-enter v1

    .line 335
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 336
    monitor-exit v1

    .line 364
    :goto_0
    return-void

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mIsStoppedDataReader:Z
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$502(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Z)Z

    .line 342
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 362
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->LISTENING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleStateChange(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V

    .line 363
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 344
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 345
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingStarted()V

    goto :goto_1

    .line 349
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 350
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingStarted()V

    goto :goto_1

    .line 354
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 355
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingStarted()V

    .line 357
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingStarted()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 342
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onStopped(IZ)V
    .locals 5
    .param p1, "totalDuration"    # I
    .param p2, "isSpeechDetected"    # Z

    .prologue
    .line 368
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    monitor-enter v1

    .line 369
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 370
    monitor-exit v1

    .line 428
    :goto_0
    return-void

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    const/4 v2, 0x1

    # setter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mIsStoppedDataReader:Z
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$502(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Z)Z

    .line 376
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 397
    :cond_1
    :goto_1
    if-nez p2, :cond_4

    .line 400
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->NO_SPEECH:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v3, "No speech detected"

    const/4 v4, 0x0

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    invoke-static {v0, v2, v3, v4}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    .line 427
    :cond_2
    :goto_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 378
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingFinished()V

    goto :goto_1

    .line 383
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 384
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingFinished()V

    goto :goto_1

    .line 388
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 389
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingFinished()V

    .line 391
    :cond_3
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 392
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingFinished()V

    goto :goto_1

    .line 401
    :cond_4
    const/4 v0, -0x1

    if-le p1, v0, :cond_5

    const/16 v0, 0x1f4

    if-ge p1, v0, :cond_5

    .line 402
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->TOO_SHORT:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v3, "Audio too short. Please try again."

    const/4 v4, 0x0

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    invoke-static {v0, v2, v3, v4}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    goto :goto_2

    .line 404
    :cond_5
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->THINKING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleStateChange(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V

    .line 406
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_2

    .line 408
    :pswitch_3
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 409
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->completeAudioRecognition()V

    goto/16 :goto_2

    .line 413
    :pswitch_4
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 414
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->completeAudioRecognition()V

    goto/16 :goto_2

    .line 418
    :pswitch_5
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 419
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->completeAudioRecognition()V

    .line 421
    :cond_6
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 422
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->completeAudioRecognition()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 406
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
