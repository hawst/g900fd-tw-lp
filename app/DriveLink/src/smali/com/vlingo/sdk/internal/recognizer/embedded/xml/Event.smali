.class public Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;
.super Ljava/lang/Object;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;
    }
.end annotation


# static fields
.field public static final CHILD_TAG_NAME:Ljava/lang/String; = "Child"

.field public static final EVENT_TAG_NAME:Ljava/lang/String; = "Event"


# instance fields
.field private childName:Ljava/lang/String;

.field private childParametersList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->childParametersList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->childName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->name:Ljava/lang/String;

    return-object p1
.end method

.method public static getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$1;)V

    return-object v0
.end method


# virtual methods
.method public getChildName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->childName:Ljava/lang/String;

    return-object v0
.end method

.method public getChildParametersList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->childParametersList:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->name:Ljava/lang/String;

    return-object v0
.end method
