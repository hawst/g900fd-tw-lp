.class public final Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;
.super Ljava/lang/Object;
.source "SRStatisticsCollection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SRStatisticsGroup"
.end annotation


# instance fields
.field private final groupedStatistics:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final guttID:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)V
    .locals 1
    .param p1, "newStatistics"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->groupedStatistics:Ljava/util/Vector;

    .line 86
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->getGuttId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->guttID:Ljava/lang/String;

    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->groupedStatistics:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$1;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)V

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->mergeStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)Z

    move-result v0

    return v0
.end method

.method private getXML()Ljava/lang/String;
    .locals 6

    .prologue
    .line 99
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 100
    .local v1, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v4, "<Stats "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    const-string/jumbo v4, "guttid"

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->guttID:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 102
    const-string/jumbo v4, ">"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 104
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->groupedStatistics:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v2

    .line 105
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 106
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->groupedStatistics:Ljava/util/Vector;

    invoke-virtual {v4, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;

    .line 107
    .local v3, "stats":Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->getXML(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    .end local v3    # "stats":Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;
    :cond_0
    const-string/jumbo v4, "</Stats>"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private mergeStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)Z
    .locals 2
    .param p1, "statsToMerge"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->guttID:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->getGuttId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->groupedStatistics:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 93
    const/4 v0, 0x1

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public schedule(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V
    .locals 6
    .param p1, "serverDetails"    # Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;
    .param p2, "clientMeta"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p3, "softwareMeta"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .prologue
    .line 118
    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->getXML()Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, "xml":Ljava/lang/String;
    const-string/jumbo v2, "SRStats"

    new-instance v3, Lcom/vlingo/sdk/internal/http/BaseHttpCallback;

    invoke-direct {v3}, Lcom/vlingo/sdk/internal/http/BaseHttpCallback;-><init>()V

    invoke-interface {p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;->getStatsURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v4

    invoke-static {v2, v3, v4, v1}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v0

    .line 122
    .local v0, "request":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    invoke-virtual {v0, p2}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->setClientMeta(Lcom/vlingo/sdk/internal/recognizer/ClientMeta;)V

    .line 123
    invoke-virtual {v0, p3}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->setSoftwareMeta(Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V

    .line 124
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->setMaxRetry(I)V

    .line 125
    const-wide/16 v2, 0x2710

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->schedule(JZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    .end local v0    # "request":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    .end local v1    # "xml":Ljava/lang/String;
    :goto_0
    return-void

    .line 130
    :catchall_0
    move-exception v2

    throw v2

    .line 127
    :catch_0
    move-exception v2

    goto :goto_0
.end method
