.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;
.super Ljava/lang/Object;
.source "VoconConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GrammarInfo"
.end annotation


# instance fields
.field private mGrammarName:Ljava/lang/String;

.field private mGrammarSlots:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "grammarName"    # Ljava/lang/String;
    .param p2, "grammarSlots"    # [Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;->mGrammarName:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;->mGrammarSlots:[Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public getGrammarName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;->mGrammarName:Ljava/lang/String;

    return-object v0
.end method

.method public getGrammarSlots()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;->mGrammarSlots:[Ljava/lang/String;

    return-object v0
.end method
