.class public Lcom/vlingo/sdk/internal/AndroidServerDetails;
.super Ljava/lang/Object;
.source "AndroidServerDetails.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;


# static fields
.field public static final DEFAULT_ASR_SERVER_HOST:Ljava/lang/String; = "androidasr.vlingo.com"

.field private static final DEFAULT_PORT:I = 0x50

.field public static final DEFAULT_TTS_SERVER_HOST:Ljava/lang/String; = "tts.vlingo.com"

.field public static final DEFAULT_UTTSCOOP_ASR_SERVER_HOST:Ljava/lang/String; = "uttscoop.vlingo.com"

.field public static final DEFAULT_UTTSCOOP_LIST_SERVER_HOST:Ljava/lang/String; = "downloads.vlingo.com"

.field public static final DEFAULT_UTTSCOOP_LMDC_SERVER_HOST:Ljava/lang/String; = "lmdcapps.vlingo.com"

.field private static final PATH_ACTIVITY_LOG:Ljava/lang/String; = "/voicepad/activitylog"

.field private static final PATH_CANCEL:Ljava/lang/String; = "/voicepad/srcancel"

.field private static final PATH_HELLO:Ljava/lang/String; = "/voicepad/hello"

.field private static final PATH_LMTT:Ljava/lang/String; = "/voicepad/lmtt"

.field public static final PATH_SR:Ljava/lang/String; = "/voicepad/sr"

.field private static final PATH_STATS:Ljava/lang/String; = "/voicepad/stats"

.field private static final PATH_TTS:Ljava/lang/String; = "/tts/tts2"

.field private static final PATH_UTTSCOOP_LISTS:Ljava/lang/String; = "/lmdc/lists"

.field private static final PATH_UTTSCOOP_LMDC:Ljava/lang/String; = "/lmdc/go"

.field private static s_LMTTURL:Lcom/vlingo/sdk/internal/http/URL;

.field private static s_TTSServerURL:Lcom/vlingo/sdk/internal/http/URL;

.field private static s_cancelURL:Lcom/vlingo/sdk/internal/http/URL;

.field private static s_helloURL:Lcom/vlingo/sdk/internal/http/URL;

.field private static s_srURL:Lcom/vlingo/sdk/internal/http/URL;

.field private static s_statsURL:Lcom/vlingo/sdk/internal/http/URL;

.field private static s_userLoggingURL:Lcom/vlingo/sdk/internal/http/URL;

.field private static s_uttscoopLMDCURL:Lcom/vlingo/sdk/internal/http/URL;

.field private static s_uttscoopListURL:Lcom/vlingo/sdk/internal/http/URL;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x50

    .line 38
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    const-string/jumbo v1, "androidasr.vlingo.com"

    const-string/jumbo v2, "/voicepad/sr"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_srURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 39
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    const-string/jumbo v1, "androidasr.vlingo.com"

    const-string/jumbo v2, "/voicepad/srcancel"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_cancelURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 40
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    const-string/jumbo v1, "androidasr.vlingo.com"

    const-string/jumbo v2, "/voicepad/stats"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_statsURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 41
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    const-string/jumbo v1, "androidasr.vlingo.com"

    const-string/jumbo v2, "/voicepad/lmtt"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_LMTTURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 42
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    const-string/jumbo v1, "androidasr.vlingo.com"

    const-string/jumbo v2, "/voicepad/activitylog"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_userLoggingURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 43
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    const-string/jumbo v1, "androidasr.vlingo.com"

    const-string/jumbo v2, "/voicepad/hello"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_helloURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 46
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    const-string/jumbo v1, "tts.vlingo.com"

    const-string/jumbo v2, "/tts/tts2"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_TTSServerURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 47
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    const-string/jumbo v1, "lmdcapps.vlingo.com"

    const-string/jumbo v2, "/lmdc/go"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_uttscoopLMDCURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 48
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    const-string/jumbo v1, "downloads.vlingo.com"

    const-string/jumbo v2, "/lmdc/lists"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_uttscoopListURL:Lcom/vlingo/sdk/internal/http/URL;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHelloURL()Lcom/vlingo/sdk/internal/http/URL;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_helloURL:Lcom/vlingo/sdk/internal/http/URL;

    return-object v0
.end method

.method public static getLMTTURL()Lcom/vlingo/sdk/internal/http/URL;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_LMTTURL:Lcom/vlingo/sdk/internal/http/URL;

    return-object v0
.end method

.method public static getTTSURL()Lcom/vlingo/sdk/internal/http/URL;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_TTSServerURL:Lcom/vlingo/sdk/internal/http/URL;

    return-object v0
.end method

.method public static getUserLoggingURL()Lcom/vlingo/sdk/internal/http/URL;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_userLoggingURL:Lcom/vlingo/sdk/internal/http/URL;

    return-object v0
.end method

.method public static getUttscoopLMDCURL()Lcom/vlingo/sdk/internal/http/URL;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_uttscoopLMDCURL:Lcom/vlingo/sdk/internal/http/URL;

    return-object v0
.end method

.method public static getUttscoopListURL()Lcom/vlingo/sdk/internal/http/URL;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_uttscoopListURL:Lcom/vlingo/sdk/internal/http/URL;

    return-object v0
.end method

.method public static setHelloServerName(Ljava/lang/String;)V
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 113
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 114
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "name cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_1
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/voicepad/hello"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_helloURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 117
    return-void
.end method

.method public static setLMTTServerName(Ljava/lang/String;)V
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 120
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 121
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "name cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_1
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/voicepad/lmtt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_LMTTURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 124
    return-void
.end method

.method public static setLogServerName(Ljava/lang/String;)V
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 94
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 95
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "name cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_1
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/voicepad/activitylog"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_userLoggingURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 98
    return-void
.end method

.method public static setServerName(Ljava/lang/String;)V
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 101
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 102
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "name cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_1
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/voicepad/sr"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_srURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 105
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/voicepad/srcancel"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_cancelURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 106
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/voicepad/stats"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_statsURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 107
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/voicepad/lmtt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_LMTTURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 108
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/voicepad/activitylog"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_userLoggingURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 109
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/voicepad/hello"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_helloURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 110
    return-void
.end method

.method public static setTTSServerName(Ljava/lang/String;)V
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 87
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 88
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "name cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_1
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/tts/tts2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_TTSServerURL:Lcom/vlingo/sdk/internal/http/URL;

    .line 91
    return-void
.end method


# virtual methods
.method public getASRCancelURL()Lcom/vlingo/sdk/internal/http/URL;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_cancelURL:Lcom/vlingo/sdk/internal/http/URL;

    return-object v0
.end method

.method public getASRURL()Lcom/vlingo/sdk/internal/http/URL;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_srURL:Lcom/vlingo/sdk/internal/http/URL;

    return-object v0
.end method

.method public getStatsURL()Lcom/vlingo/sdk/internal/http/URL;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/vlingo/sdk/internal/AndroidServerDetails;->s_statsURL:Lcom/vlingo/sdk/internal/http/URL;

    return-object v0
.end method
