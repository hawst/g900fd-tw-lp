.class public Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
.super Ljava/lang/Object;
.source "ActionList.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field list:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->list:Ljava/util/Vector;

    .line 39
    return-void
.end method

.method public static createActionListFromJSONArray(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    .locals 2
    .param p0, "jsonArray"    # Ljava/lang/String;

    .prologue
    .line 98
    new-instance v0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;-><init>()V

    .line 100
    .local v0, "al":Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->createActionListFromJSONArray(Lorg/json/JSONArray;)Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    .line 102
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static createActionListFromJSONArray(Lorg/json/JSONArray;)Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    .locals 10
    .param p0, "actions"    # Lorg/json/JSONArray;

    .prologue
    .line 109
    new-instance v1, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;-><init>()V

    .line 111
    .local v1, "al":Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_3

    .line 112
    invoke-virtual {p0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 113
    .local v3, "jAction":Lorg/json/JSONObject;
    new-instance v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;

    const-string/jumbo v9, "Name"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v9}, Lcom/vlingo/sdk/internal/vlservice/response/Action;-><init>(Ljava/lang/String;)V

    .line 114
    .local v0, "action":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    const-string/jumbo v9, "If"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 115
    const-string/jumbo v9, "If"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->ifCondition:Ljava/lang/String;

    .line 117
    :cond_0
    const-string/jumbo v9, "Else"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 118
    const-string/jumbo v9, "Else"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->elseStatement:Ljava/lang/String;

    .line 120
    :cond_1
    const-string/jumbo v9, "Params"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 121
    const-string/jumbo v9, "Params"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 122
    .local v7, "params":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v4, v9, :cond_2

    .line 123
    invoke-virtual {v7, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 124
    .local v6, "param":Lorg/json/JSONObject;
    const-string/jumbo v9, "Name"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 125
    .local v5, "name":Ljava/lang/String;
    const-string/jumbo v9, "Value"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 126
    .local v8, "value":Ljava/lang/String;
    const/4 v9, 0x0

    invoke-virtual {v0, v5, v8, v9}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->addParameter(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)V

    .line 122
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 129
    .end local v4    # "k":I
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "param":Lorg/json/JSONObject;
    .end local v7    # "params":Lorg/json/JSONArray;
    .end local v8    # "value":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->addElement(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 132
    .end local v0    # "action":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    .end local v3    # "jAction":Lorg/json/JSONObject;
    :catch_0
    move-exception v9

    .line 135
    :cond_3
    return-object v1
.end method

.method public static createActionListFromURL(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    .locals 18
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 139
    new-instance v6, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-direct {v6}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;-><init>()V

    .line 140
    .local v6, "al":Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    const-string/jumbo v17, "action:"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_0

    const-string/jumbo v17, "vvaction:"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 141
    :cond_0
    const-string/jumbo v16, ""

    .line 143
    .local v16, "queryString":Ljava/lang/String;
    const/16 v17, 0x3a

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 144
    .local v7, "colonIndex":I
    add-int/lit8 v17, v7, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 146
    const/16 v17, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v5

    .line 147
    .local v5, "actions":[Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v5

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v8, v0, :cond_3

    .line 148
    aget-object v4, v5, v8

    .line 152
    .local v4, "actionString":Ljava/lang/String;
    const-string/jumbo v17, "?"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v15

    .line 153
    .local v15, "qPos":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-le v15, v0, :cond_1

    .line 154
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v4, v0, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 155
    .local v3, "actionName":Ljava/lang/String;
    add-int/lit8 v17, v15, 0x1

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 162
    :goto_1
    new-instance v2, Lcom/vlingo/sdk/internal/vlservice/response/Action;

    invoke-direct {v2, v3}, Lcom/vlingo/sdk/internal/vlservice/response/Action;-><init>(Ljava/lang/String;)V

    .line 163
    .local v2, "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v17

    if-lez v17, :cond_2

    .line 164
    const/16 v17, 0x26

    invoke-static/range {v16 .. v17}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v10

    .line 165
    .local v10, "params":[Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_2
    array-length v0, v10

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v9, v0, :cond_2

    .line 166
    aget-object v11, v10, v9

    .line 167
    .local v11, "parm":Ljava/lang/String;
    const/16 v17, 0x3d

    move/from16 v0, v17

    invoke-static {v11, v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v13

    .line 168
    .local v13, "pv":[Ljava/lang/String;
    const/16 v17, 0x0

    aget-object v12, v13, v17

    .line 169
    .local v12, "pname":Ljava/lang/String;
    const/16 v17, 0x1

    aget-object v14, v13, v17

    .line 170
    .local v14, "pval":Ljava/lang/String;
    invoke-static {v14}, Lcom/vlingo/sdk/internal/util/UrlUtils;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 173
    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v2, v12, v14, v0}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->addParameter(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)V

    .line 165
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 158
    .end local v2    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    .end local v3    # "actionName":Ljava/lang/String;
    .end local v9    # "j":I
    .end local v10    # "params":[Ljava/lang/String;
    .end local v11    # "parm":Ljava/lang/String;
    .end local v12    # "pname":Ljava/lang/String;
    .end local v13    # "pv":[Ljava/lang/String;
    .end local v14    # "pval":Ljava/lang/String;
    :cond_1
    move-object v3, v4

    .restart local v3    # "actionName":Ljava/lang/String;
    goto :goto_1

    .line 176
    .restart local v2    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :cond_2
    invoke-virtual {v6, v2}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->addElement(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V

    .line 147
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 179
    .end local v2    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    .end local v3    # "actionName":Ljava/lang/String;
    .end local v4    # "actionString":Ljava/lang/String;
    .end local v5    # "actions":[Ljava/lang/String;
    .end local v7    # "colonIndex":I
    .end local v8    # "i":I
    .end local v15    # "qPos":I
    .end local v16    # "queryString":Ljava/lang/String;
    :cond_3
    return-object v6
.end method

.method public static createActionListFromXML(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    .locals 2
    .param p0, "xml"    # Ljava/lang/String;

    .prologue
    .line 93
    invoke-static {p0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->createFromXml(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v0

    .line 94
    .local v0, "res":Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->getActionList()Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public declared-synchronized addElement(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V
    .locals 1
    .param p1, "arg0"    # Lcom/vlingo/sdk/internal/vlservice/response/Action;

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->list:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    monitor-exit p0

    return-void

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    .line 45
    .local v0, "clone":Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->list:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    iput-object v1, v0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->list:Ljava/util/Vector;

    .line 46
    return-object v0
.end method

.method public containsActionWithName(Ljava/lang/String;)Z
    .locals 3
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 63
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 64
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->elementAt(I)Lcom/vlingo/sdk/internal/vlservice/response/Action;

    move-result-object v0

    .line 65
    .local v0, "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    iget-object v2, v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->name:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 66
    const/4 v2, 0x1

    .line 69
    .end local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :goto_1
    return v2

    .line 63
    .restart local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    .end local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public containsErrorMessage()Z
    .locals 5

    .prologue
    .line 50
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 51
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->elementAt(I)Lcom/vlingo/sdk/internal/vlservice/response/Action;

    move-result-object v0

    .line 52
    .local v0, "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->getName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "ShowMessage"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    const-string/jumbo v3, "Type"

    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->getStringParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "typeStr":Ljava/lang/String;
    const-string/jumbo v3, "error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55
    const/4 v3, 0x1

    .line 59
    .end local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    .end local v2    # "typeStr":Ljava/lang/String;
    :goto_1
    return v3

    .line 50
    .restart local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 59
    .end local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public elementAt(I)Lcom/vlingo/sdk/internal/vlservice/response/Action;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 183
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->list:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;

    return-object v0
.end method

.method public declared-synchronized elements()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->list:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getActionByName(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/Action;
    .locals 3
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 83
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 84
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->elementAt(I)Lcom/vlingo/sdk/internal/vlservice/response/Action;

    move-result-object v0

    .line 85
    .local v0, "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    iget-object v2, v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->name:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    .end local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :goto_1
    return-object v0

    .line 83
    .restart local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    .end local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->list:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public removeActionByName(Ljava/lang/String;)V
    .locals 3
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 73
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 74
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->elementAt(I)Lcom/vlingo/sdk/internal/vlservice/response/Action;

    move-result-object v0

    .line 75
    .local v0, "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    iget-object v2, v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->name:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 76
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->list:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 80
    .end local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :cond_0
    return-void

    .line 73
    .restart local v0    # "a":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public declared-synchronized removeAllElements()V
    .locals 1

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->list:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    monitor-exit p0

    return-void

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->list:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method
