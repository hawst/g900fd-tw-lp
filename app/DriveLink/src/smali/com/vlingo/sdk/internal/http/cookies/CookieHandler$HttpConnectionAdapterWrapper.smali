.class Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionAdapterWrapper;
.super Ljava/lang/Object;
.source "CookieHandler.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HttpConnectionAdapterWrapper"
.end annotation


# instance fields
.field private connection:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;)V
    .locals 2
    .param p1, "connection"    # Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    if-nez p1, :cond_0

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "you MUST provide a non-null \'connection\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionAdapterWrapper;->connection:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    .line 116
    return-void
.end method


# virtual methods
.method public getHeaderField(I)Ljava/lang/String;
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionAdapterWrapper;->connection:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getResponseHeaderField(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionAdapterWrapper;->connection:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getResponseHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    const-string/jumbo v0, ""

    return-object v0
.end method
