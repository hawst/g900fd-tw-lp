.class public Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;
.super Ljava/lang/Object;
.source "PhoneInfo.java"


# static fields
.field private static instance:Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;


# instance fields
.field deviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->deviceId:Ljava/lang/String;

    return-void
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->instance:Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    .line 42
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;
    .locals 2

    .prologue
    .line 34
    const-class v1, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->instance:Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->instance:Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    .line 37
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->instance:Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static hashForPhoneNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 46
    .local v2, "len":I
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 49
    .local v3, "sb":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 50
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 51
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 52
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 49
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 55
    .end local v0    # "c":C
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    const/4 v5, 0x7

    if-le v4, v5, :cond_2

    .line 56
    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x7

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 58
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/vlingo/sdk/internal/crypto/CryptoUtils;->getHash(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private static myModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "HTC Paradise"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string/jumbo v0, "ventura"

    .line 82
    :goto_0
    return-object v0

    .line 80
    :cond_0
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "HTC Liberty"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    const-string/jumbo v0, "intruder"

    goto :goto_0

    .line 82
    :cond_1
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getCarrierCountry()Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getCtx()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getCarrierCountry(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "s":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 89
    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getMCC()Ljava/lang/String;

    move-result-object v0

    .line 90
    const-string/jumbo v1, "000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    const/4 v0, 0x0

    .line 94
    :cond_1
    if-nez v0, :cond_2

    .line 95
    const-string/jumbo v0, ""

    .line 97
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method getCtx()Landroid/content/Context;
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentCarrier()Ljava/lang/String;
    .locals 4

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getManager()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "networkOperatorName":Ljava/lang/String;
    const-string/jumbo v2, "[^A-Za-z0-9]"

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "carrier":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 114
    :cond_0
    return-object v0
.end method

.method public getCurrentNetworkISO3CountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getCarrierCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/sdk/internal/deviceinfo/CountryUtils;->mISO2ToISO3CountryCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getManager()Landroid/telephony/TelephonyManager;
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getCtx()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->myModel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOSVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getPhoneNumberHash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getShortOSVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method public getVendorCarrier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    return-object v0
.end method

.method public getVendorCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVendorID()I
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public getVendorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    return-object v0
.end method

.method getWifiManager()Landroid/net/wifi/WifiManager;
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getCtx()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method public isSimulator()Z
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public isTouchDevice()Z
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x1

    return v0
.end method
