.class public Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
.super Ljava/lang/Object;
.source "MPOutputStream.java"


# static fields
.field private static final ivEndLine:Ljava/lang/String; = "\r\n"


# instance fields
.field private ivBoundary:Ljava/lang/String;

.field private ivDout:Ljava/io/DataOutputStream;


# direct methods
.method public constructor <init>(Ljava/io/DataOutputStream;Ljava/lang/String;)V
    .locals 2
    .param p1, "dout"    # Ljava/io/DataOutputStream;
    .param p2, "boundary"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    if-nez p1, :cond_0

    .line 36
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Output stream is null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->ivDout:Ljava/io/DataOutputStream;

    .line 38
    iput-object p2, p0, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->ivBoundary:Ljava/lang/String;

    .line 39
    return-void
.end method

.method private write(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->ivDout:Ljava/io/DataOutputStream;

    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->convertStringToBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->write([B)V

    .line 45
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->ivDout:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 132
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->ivDout:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 125
    return-void
.end method

.method public write([BII)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->ivDout:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 118
    return-void
.end method

.method public writeBoundary()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->ivBoundary:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->write(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public writeDataField(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeDataField(Ljava/lang/String;Ljava/lang/String;[BZ)V

    .line 85
    return-void
.end method

.method public writeDataField(Ljava/lang/String;Ljava/lang/String;[BZ)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "data"    # [B
    .param p4, "compress"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 89
    if-eqz p4, :cond_0

    .line 90
    const-string/jumbo v1, "deflate"

    invoke-virtual {p0, p1, p2, v1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeFieldHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-static {p3}, Lcom/vlingo/sdk/internal/util/CompressUtils;->deflate([B)[B

    move-result-object v0

    .line 92
    .local v0, "compressedBytes":[B
    array-length v1, v0

    invoke-virtual {p0, v0, v2, v1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->write([BII)V

    .line 99
    .end local v0    # "compressedBytes":[B
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeEndFieldValue()V

    .line 100
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeBoundary()V

    .line 101
    return-void

    .line 95
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeFieldHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    array-length v1, p3

    invoke-virtual {p0, p3, v2, v1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->write([BII)V

    goto :goto_0
.end method

.method public writeEndFieldValue()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    const-string/jumbo v0, "\r\n"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->write(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public writeEndHeader()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    const-string/jumbo v0, "\r\n"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->write(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeFieldHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0, p3}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->write(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeEndFieldValue()V

    .line 79
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeBoundary()V

    .line 80
    return-void
.end method

.method public writeFieldHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "encoding"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    const-string/jumbo v0, "Content-Disposition"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "form-data; name=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string/jumbo v0, "Content-Type"

    invoke-virtual {p0, v0, p2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    if-eqz p3, :cond_0

    .line 69
    const-string/jumbo v0, "Content-Encoding"

    invoke-virtual {p0, v0, p3}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeEndHeader()V

    .line 72
    return-void
.end method

.method public writeFileFieldHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    const-string/jumbo v0, "Content-Disposition"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "form-data; name=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\"; filename=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string/jumbo v0, "Content-Type"

    invoke-virtual {p0, v0, p2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string/jumbo v0, "Content-Transfer-Encoding"

    const-string/jumbo v1, "binary"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeEndHeader()V

    .line 111
    return-void
.end method

.method public writeHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->write(Ljava/lang/String;)V

    .line 53
    return-void
.end method
