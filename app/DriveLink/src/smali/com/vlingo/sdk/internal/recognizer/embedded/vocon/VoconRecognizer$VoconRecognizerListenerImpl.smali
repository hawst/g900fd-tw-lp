.class abstract Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$VoconRecognizerListenerImpl;
.super Ljava/lang/Object;
.source "VoconRecognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "VoconRecognizerListenerImpl"
.end annotation


# instance fields
.field private final recognizerListener:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;


# direct methods
.method protected constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;)V
    .locals 0
    .param p1, "recognizerListener"    # Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$VoconRecognizerListenerImpl;->recognizerListener:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    .line 124
    return-void
.end method


# virtual methods
.method public onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;)V
    .locals 1
    .param p1, "response"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$VoconRecognizerListenerImpl;->recognizerListener:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$VoconRecognizerListenerImpl;->recognizerListener:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;->onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;)V

    .line 138
    :cond_0
    return-void
.end method

.method public onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V
    .locals 1
    .param p1, "recError"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$VoconRecognizerListenerImpl;->recognizerListener:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$VoconRecognizerListenerImpl;->recognizerListener:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;->onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 131
    :cond_0
    return-void
.end method
