.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$2;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "Rule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->addChildrenBuilders(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$2;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addParametersNames(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "paramNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$2;->buildResultObject()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected buildResultObject()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$2;->parametersMap:Ljava/util/Map;

    const-string/jumbo v1, "name"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$2;->parametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 87
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    const-string/jumbo v0, "Tag"

    return-object v0
.end method
