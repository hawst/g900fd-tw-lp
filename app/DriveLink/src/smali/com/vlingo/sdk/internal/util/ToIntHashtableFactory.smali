.class public Lcom/vlingo/sdk/internal/util/ToIntHashtableFactory;
.super Ljava/lang/Object;
.source "ToIntHashtableFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createNewHashtable()Lcom/vlingo/sdk/internal/util/ToIntHashtable;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/vlingo/sdk/internal/util/AndroidToIntHashtable;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/util/AndroidToIntHashtable;-><init>()V

    return-object v0
.end method

.method public static createNewHashtable(I)Lcom/vlingo/sdk/internal/util/ToIntHashtable;
    .locals 1
    .param p0, "initialCapacity"    # I

    .prologue
    .line 24
    new-instance v0, Lcom/vlingo/sdk/internal/util/AndroidToIntHashtable;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/util/AndroidToIntHashtable;-><init>(I)V

    return-object v0
.end method
