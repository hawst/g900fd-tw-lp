.class public Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;
.super Ljava/lang/Object;
.source "RawInputStreamReader.java"


# instance fields
.field mInputBuffer:[B

.field mInputBufferOffset:I

.field mInputStream:Ljava/io/InputStream;

.field mIsBEInputStream:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;ZI)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "isBigEndian"    # Z
    .param p3, "maxReadLength"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputStream:Ljava/io/InputStream;

    .line 20
    iput-boolean p2, p0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mIsBEInputStream:Z

    .line 21
    mul-int/lit8 v0, p3, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBuffer:[B

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBufferOffset:I

    .line 23
    return-void
.end method


# virtual methods
.method public read([SIIZ[I)I
    .locals 26
    .param p1, "data"    # [S
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "isSteroe"    # Z
    .param p5, "rms"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    mul-int/lit8 v5, p3, 0x2

    .line 28
    .local v5, "bytesToRead":I
    if-eqz p4, :cond_0

    .line 29
    mul-int/lit8 v5, v5, 0x2

    .line 30
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBuffer:[B

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-le v5, v0, :cond_1

    .line 31
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBuffer:[B

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v5, v0

    .line 33
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBufferOffset:I

    move/from16 v19, v0

    sub-int v5, v5, v19

    .line 35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputStream:Ljava/io/InputStream;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBuffer:[B

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBufferOffset:I

    move/from16 v21, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    .line 37
    .local v4, "bytesRead":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v4, v0, :cond_2

    .line 38
    const/16 v19, -0x1

    .line 82
    :goto_0
    return v19

    .line 41
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBufferOffset:I

    move/from16 v19, v0

    add-int v3, v4, v19

    .line 42
    .local v3, "bytesInBuffer":I
    rem-int/lit8 v19, v3, 0x2

    sub-int v13, v3, v19

    .line 43
    .local v13, "shortBytesInBuffer":I
    if-eq v13, v3, :cond_3

    const/4 v6, 0x1

    .line 45
    .local v6, "carryOverLastByte":Z
    :goto_1
    const-wide/16 v16, 0x0

    .line 46
    .local v16, "sumSquare":J
    const-wide/16 v14, 0x0

    .line 49
    .local v14, "sum":J
    if-eqz p4, :cond_4

    const/4 v9, 0x4

    .line 50
    .local v9, "incBytes":I
    :goto_2
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_3
    if-ge v8, v13, :cond_6

    .line 52
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mIsBEInputStream:Z

    move/from16 v19, v0

    if-eqz v19, :cond_5

    .line 53
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBuffer:[B

    move-object/from16 v19, v0

    aget-byte v19, v19, v8

    move/from16 v0, v19

    and-int/lit16 v7, v0, 0xff

    .line 54
    .local v7, "hi":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBuffer:[B

    move-object/from16 v19, v0

    add-int/lit8 v20, v8, 0x1

    aget-byte v19, v19, v20

    move/from16 v0, v19

    and-int/lit16 v10, v0, 0xff

    .line 60
    .local v10, "lo":I
    :goto_4
    shl-int/lit8 v19, v7, 0x8

    or-int v19, v19, v10

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v18, v0

    .line 61
    .local v18, "val":S
    aput-short v18, p1, p2

    .line 62
    add-int/lit8 p2, p2, 0x1

    .line 64
    mul-int v19, v18, v18

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    add-long v16, v16, v19

    .line 65
    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v19, v0

    add-long v14, v14, v19

    .line 50
    add-int/2addr v8, v9

    goto :goto_3

    .line 43
    .end local v6    # "carryOverLastByte":Z
    .end local v7    # "hi":I
    .end local v8    # "i":I
    .end local v9    # "incBytes":I
    .end local v10    # "lo":I
    .end local v14    # "sum":J
    .end local v16    # "sumSquare":J
    .end local v18    # "val":S
    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    .line 49
    .restart local v6    # "carryOverLastByte":Z
    .restart local v14    # "sum":J
    .restart local v16    # "sumSquare":J
    :cond_4
    const/4 v9, 0x2

    goto :goto_2

    .line 57
    .restart local v8    # "i":I
    .restart local v9    # "incBytes":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBuffer:[B

    move-object/from16 v19, v0

    aget-byte v19, v19, v8

    move/from16 v0, v19

    and-int/lit16 v10, v0, 0xff

    .line 58
    .restart local v10    # "lo":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBuffer:[B

    move-object/from16 v19, v0

    add-int/lit8 v20, v8, 0x1

    aget-byte v19, v19, v20

    move/from16 v0, v19

    and-int/lit16 v7, v0, 0xff

    .restart local v7    # "hi":I
    goto :goto_4

    .line 68
    .end local v7    # "hi":I
    .end local v10    # "lo":I
    :cond_6
    move/from16 v0, p3

    int-to-long v0, v0

    move-wide/from16 v19, v0

    div-long v19, v14, v19

    move-wide/from16 v0, v19

    long-to-double v11, v0

    .line 69
    .local v11, "mean":D
    const/16 v19, 0x0

    const-wide/high16 v20, 0x4024000000000000L    # 10.0

    move/from16 v0, p3

    int-to-long v0, v0

    move-wide/from16 v22, v0

    div-long v22, v16, v22

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v22, v0

    mul-double v24, v11, v11

    sub-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->log10(D)D

    move-result-wide v22

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v20, v0

    aput v20, p5, v19

    .line 71
    if-eqz v6, :cond_7

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBuffer:[B

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBuffer:[B

    move-object/from16 v21, v0

    add-int/lit8 v22, v3, -0x1

    aget-byte v21, v21, v22

    aput-byte v21, v19, v20

    .line 73
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBufferOffset:I

    .line 79
    :goto_5
    if-eqz p4, :cond_8

    .line 80
    div-int/lit8 v19, v13, 0x4

    goto/16 :goto_0

    .line 76
    :cond_7
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->mInputBufferOffset:I

    goto :goto_5

    .line 82
    :cond_8
    div-int/lit8 v19, v13, 0x2

    goto/16 :goto_0
.end method
