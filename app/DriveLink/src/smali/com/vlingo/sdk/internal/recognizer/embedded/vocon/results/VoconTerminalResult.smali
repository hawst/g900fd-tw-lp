.class public final Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
.source "VoconTerminalResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    }
.end annotation


# static fields
.field public static final ORTHOGRAPHY:Ljava/lang/String; = "_orthography"


# instance fields
.field private _orthography:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;-><init>()V

    .line 204
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$1;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;->_orthography:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public addTagNames(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 224
    .local p1, "tagNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public getOrthography()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;->_orthography:Ljava/lang/String;

    return-object v0
.end method

.method public getRecognizedUtterance(Z)Ljava/lang/String;
    .locals 1
    .param p1, "forDisplay"    # Z

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;->getOrthography()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
