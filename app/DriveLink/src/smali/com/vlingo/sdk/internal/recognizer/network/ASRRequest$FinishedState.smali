.class Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;
.super Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
.source "ASRRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FinishedState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 1

    .prologue
    .line 1029
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;

    .prologue
    .line 1029
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 1039
    const-string/jumbo v0, "finish(): ignored"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;->log(Ljava/lang/String;)V

    .line 1040
    return-void
.end method

.method public onRun()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1044
    const-string/jumbo v0, "[LatencyCheck] FinishedState audio stream complete"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;->log(Ljava/lang/String;)V

    .line 1045
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->close()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    .line 1046
    return-void
.end method

.method public sendAudio([BII)V
    .locals 1
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 1033
    const-string/jumbo v0, "sendAudio(): ignored"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;->log(Ljava/lang/String;)V

    .line 1035
    return-void
.end method
