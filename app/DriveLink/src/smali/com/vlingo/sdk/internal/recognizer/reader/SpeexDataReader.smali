.class public Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;
.super Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;
.source "SpeexDataReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader$1;
    }
.end annotation


# static fields
.field private static final INITIALIZE_FAIL_RETRY_COUNT:I = 0x5

.field private static final MAX_CODEC_OUTPUT_CHUNK:I = 0x3e8

.field private static final MIN_SPEECH_TIME:F = 0.5f

.field private static final RMS_UPDATE_COUNT:I = 0x5

.field private static final SPEEX_NB:I = 0x0

.field private static final SPEEX_WB:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final USE_SILENCE_DETECTION:I = 0x1


# instance fields
.field private mAutoEndpointingEnabled:Z

.field private mAutoEndpointingTimeWithSpeech:F

.field private mAutoEndpointingTimeWithoutSpeech:F

.field private mRawInputStreamReader:Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;

.field private mRmsReadSize:I

.field private mSampleRate:I

.field private mSpeechDetected:Z

.field private mSpeex:Lcom/vlingo/sdk/internal/audio/SpeexJNI;

.field private mSpeexInputBuffer:[S

.field private mSpeexInputBufferSamples:I

.field private mSpeexOutputBuffer:[B

.field private mTotalMillisecProcessed:I

.field private mTotalSamplesProcessed:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V
    .locals 1
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    .param p3, "DataReadyListener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;-><init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V

    .line 41
    const/16 v0, 0x3e8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexOutputBuffer:[B

    .line 56
    return-void
.end method

.method private convertBytesToInt([BI)I
    .locals 4
    .param p1, "input"    # [B
    .param p2, "startIndex"    # I

    .prologue
    .line 306
    const/4 v1, 0x0

    .line 307
    .local v1, "shift":I
    const/4 v2, 0x0

    .line 308
    .local v2, "value":I
    move v0, p2

    .local v0, "i":I
    :goto_0
    add-int/lit8 v3, p2, 0x4

    if-ge v0, v3, :cond_0

    .line 309
    aget-byte v3, p1, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/2addr v3, v1

    or-int/2addr v2, v3

    .line 310
    add-int/lit8 v1, v1, 0x8

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 312
    :cond_0
    return v2
.end method

.method private onInitUnsafe()Z
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->getSRContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v17

    .line 72
    .local v17, "srContext":Lcom/vlingo/sdk/internal/recognizer/SRContext;
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v12

    .line 74
    .local v12, "asi":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAutoEndpointing()Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mAutoEndpointingEnabled:Z

    .line 75
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getSpeechEndpointTimeout()I

    move-result v1

    int-to-float v1, v1

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float/2addr v1, v7

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mAutoEndpointingTimeWithSpeech:F

    .line 76
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getNoSpeechEndpointTimeout()I

    move-result v1

    int-to-float v1, v1

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float/2addr v1, v7

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mAutoEndpointingTimeWithoutSpeech:F

    .line 80
    const/4 v14, 0x0

    .line 82
    .local v14, "isBigEndian":Z
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader$1;->$SwitchMap$com$vlingo$sdk$recognition$AudioSourceInfo$SourceFormat:[I

    invoke-virtual {v12}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getFormat()Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->ordinal()I

    move-result v7

    aget v1, v1, v7

    packed-switch v1, :pswitch_data_0

    .line 94
    const/16 v1, 0x3e80

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSampleRate:I

    .line 98
    :goto_0
    new-instance v1, Lcom/vlingo/sdk/internal/audio/SpeexJNI;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/audio/SpeexJNI;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeex:Lcom/vlingo/sdk/internal/audio/SpeexJNI;

    .line 100
    const/16 v16, -0x1

    .line 101
    .local v16, "speexChunkSize":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSampleRate:I

    const/16 v7, 0x3e80

    if-ne v1, v7, :cond_1

    const/4 v2, 0x1

    .line 102
    .local v2, "speexMode":I
    :goto_1
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getSilenceThreshold()F

    move-result v8

    .line 103
    .local v8, "silenceThreshold":F
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getMinVoiceDuration()F

    move-result v9

    .line 104
    .local v9, "minVoiceDuration":F
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getVoicePortion()F

    move-result v10

    .line 105
    .local v10, "voicePortion":F
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getMinVoiceLevel()F

    move-result v11

    .line 106
    .local v11, "minVoiceLevel":F
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getSpeexQuality()I

    move-result v3

    .line 107
    .local v3, "speexQuality":I
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getSpeexComplexity()I

    move-result v4

    .line 108
    .local v4, "speexComplexity":I
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getSpeexVariableBitrate()I

    move-result v5

    .line 109
    .local v5, "speexVariableBitrate":I
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getSpeexVoiceActivityDetection()I

    move-result v6

    .line 112
    .local v6, "speexVoiceActivityDetection":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeex:Lcom/vlingo/sdk/internal/audio/SpeexJNI;

    const/4 v7, 0x1

    invoke-virtual/range {v1 .. v11}, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->initialize(IIIIIIFFFF)I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v16

    .line 143
    :cond_0
    :goto_2
    if-gtz v16, :cond_2

    .line 146
    const/4 v1, 0x0

    .line 160
    :goto_3
    return v1

    .line 84
    .end local v2    # "speexMode":I
    .end local v3    # "speexQuality":I
    .end local v4    # "speexComplexity":I
    .end local v5    # "speexVariableBitrate":I
    .end local v6    # "speexVoiceActivityDetection":I
    .end local v8    # "silenceThreshold":F
    .end local v9    # "minVoiceDuration":F
    .end local v10    # "voicePortion":F
    .end local v11    # "minVoiceLevel":F
    .end local v16    # "speexChunkSize":I
    :pswitch_0
    const/4 v14, 0x1

    .line 86
    :pswitch_1
    const/16 v1, 0x3e80

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSampleRate:I

    goto :goto_0

    .line 89
    :pswitch_2
    const/4 v14, 0x1

    .line 91
    :pswitch_3
    const/16 v1, 0x1f40

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSampleRate:I

    goto :goto_0

    .line 101
    .restart local v16    # "speexChunkSize":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 114
    .restart local v2    # "speexMode":I
    .restart local v3    # "speexQuality":I
    .restart local v4    # "speexComplexity":I
    .restart local v5    # "speexVariableBitrate":I
    .restart local v6    # "speexVoiceActivityDetection":I
    .restart local v8    # "silenceThreshold":F
    .restart local v9    # "minVoiceDuration":F
    .restart local v10    # "voicePortion":F
    .restart local v11    # "minVoiceLevel":F
    :catch_0
    move-exception v13

    .line 126
    .local v13, "e":Ljava/lang/UnsatisfiedLinkError;
    const/4 v15, 0x0

    .local v15, "retries":I
    :goto_4
    const/4 v1, 0x5

    if-ge v15, v1, :cond_0

    .line 130
    const-wide/16 v18, 0x64

    invoke-static/range {v18 .. v19}, Ljava/lang/Thread;->sleep(J)V

    .line 132
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeex:Lcom/vlingo/sdk/internal/audio/SpeexJNI;

    const/4 v7, 0x1

    invoke-virtual/range {v1 .. v11}, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->initialize(IIIIIIFFFF)I
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_1

    move-result v16

    goto :goto_2

    .line 136
    :catch_1
    move-exception v1

    .line 126
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 150
    .end local v13    # "e":Ljava/lang/UnsatisfiedLinkError;
    .end local v15    # "retries":I
    :cond_2
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "minVoiceLevel= "

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    move/from16 v0, v16

    new-array v1, v0, [S

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    .line 153
    div-int/lit8 v1, v16, 0x5

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mRmsReadSize:I

    .line 155
    const/4 v1, 0x2

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioFormatChannelConfig()I

    move-result v7

    if-ne v1, v7, :cond_3

    .line 156
    new-instance v1, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    move/from16 v0, v16

    invoke-direct {v1, v7, v14, v0}, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;-><init>(Ljava/io/InputStream;ZI)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mRawInputStreamReader:Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;

    .line 160
    :goto_5
    const/4 v1, 0x1

    goto :goto_3

    .line 158
    :cond_3
    new-instance v1, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    mul-int/lit8 v18, v16, 0x2

    move/from16 v0, v18

    invoke-direct {v1, v7, v14, v0}, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;-><init>(Ljava/io/InputStream;ZI)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mRawInputStreamReader:Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;

    goto :goto_5

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected isSpeechDetected()Z
    .locals 1

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeechDetected:Z

    return v0
.end method

.method protected onDeinit()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 164
    const/4 v1, 0x1

    .line 166
    .local v1, "isEnd":I
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeex:Lcom/vlingo/sdk/internal/audio/SpeexJNI;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexOutputBuffer:[B

    const/16 v5, 0x3e8

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->encode([S[BII)I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :goto_0
    iput-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeex:Lcom/vlingo/sdk/internal/audio/SpeexJNI;

    .line 174
    iput-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mRawInputStreamReader:Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;

    .line 175
    return-void

    .line 167
    :catch_0
    move-exception v0

    .line 171
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;->READ_ERROR:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->onError(Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onInit()Z
    .locals 4

    .prologue
    .line 63
    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->onInitUnsafe()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 66
    :goto_0
    return v1

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error initializing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onProcessData()V
    .locals 24

    .prologue
    .line 179
    const/4 v11, 0x0

    .line 182
    .local v11, "isDone":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    array-length v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    sub-int v19, v2, v3

    .line 183
    .local v19, "samplesToRead":I
    const/16 v17, 0x0

    .line 186
    .local v17, "samplesRead":I
    const/4 v2, 0x2

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->getSRContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioFormatChannelConfig()I

    move-result v3

    if-eq v2, v3, :cond_2

    const/4 v6, 0x1

    .line 188
    .local v6, "isSteroe":Z
    :goto_0
    const/16 v18, 0x0

    .line 189
    .local v18, "samplesReadForRMS":I
    const/4 v2, 0x1

    new-array v7, v2, [I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v7, v2

    .line 191
    .local v7, "rms":[I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    const/4 v2, 0x5

    if-ge v10, v2, :cond_0

    .line 192
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mRmsReadSize:I

    move/from16 v0, v19

    if-lt v0, v2, :cond_3

    .line 193
    move-object/from16 v0, p0

    iget v5, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mRmsReadSize:I

    .line 200
    .local v5, "samplesToReadForRMS":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mRawInputStreamReader:Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    invoke-virtual/range {v2 .. v7}, Lcom/vlingo/sdk/internal/audio/RawInputStreamReader;->read([SIIZ[I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v18

    .line 201
    const/4 v2, -0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_4

    .line 205
    const/4 v11, 0x1

    .line 224
    .end local v5    # "samplesToReadForRMS":I
    :cond_0
    if-lez v17, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->getDataReadyListener()Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;->isDataReady()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 225
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mTotalSamplesProcessed:I

    mul-int/lit16 v2, v2, 0x3e8

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSampleRate:I

    div-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mTotalMillisecProcessed:I

    .line 227
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mTotalMillisecProcessed:I

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->getMaxDuration()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 230
    const/4 v11, 0x1

    .line 234
    :cond_1
    if-eqz v11, :cond_6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    if-lez v2, :cond_6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    array-length v3, v3

    if-ge v2, v3, :cond_6

    .line 238
    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    array-length v3, v3

    if-ge v2, v3, :cond_6

    .line 239
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    const/4 v4, 0x0

    aput-short v4, v2, v3

    goto :goto_3

    .line 186
    .end local v6    # "isSteroe":Z
    .end local v7    # "rms":[I
    .end local v10    # "i":I
    .end local v18    # "samplesReadForRMS":I
    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 194
    .restart local v6    # "isSteroe":Z
    .restart local v7    # "rms":[I
    .restart local v10    # "i":I
    .restart local v18    # "samplesReadForRMS":I
    :cond_3
    if-eqz v19, :cond_0

    .line 197
    move/from16 v5, v19

    .restart local v5    # "samplesToReadForRMS":I
    goto :goto_2

    .line 209
    :cond_4
    sub-int v19, v19, v18

    .line 210
    add-int v17, v17, v18

    .line 212
    const/4 v2, 0x0

    :try_start_1
    aget v2, v7, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->onRMSDataAvailable(I)V

    .line 214
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    add-int v2, v2, v18

    move-object/from16 v0, p0

    iput v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    .line 215
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mTotalSamplesProcessed:I

    add-int v2, v2, v18

    move-object/from16 v0, p0

    iput v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mTotalSamplesProcessed:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 191
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 217
    .end local v5    # "samplesToReadForRMS":I
    .end local v6    # "isSteroe":Z
    .end local v7    # "rms":[I
    .end local v10    # "i":I
    .end local v18    # "samplesReadForRMS":I
    :catch_0
    move-exception v9

    .line 218
    .local v9, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception reading from stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;->READ_ERROR:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->onError(Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;Ljava/lang/String;)V

    .line 299
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_4
    return-void

    .line 243
    .restart local v6    # "isSteroe":Z
    .restart local v7    # "rms":[I
    .restart local v10    # "i":I
    .restart local v18    # "samplesReadForRMS":I
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    array-length v3, v3

    if-lt v2, v3, :cond_a

    .line 244
    const/4 v12, 0x0

    .line 245
    .local v12, "isEnd":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    array-length v2, v2

    new-array v0, v2, [S

    move-object/from16 v16, v0

    .line 246
    .local v16, "rawData":[S
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-static {v2, v3, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 249
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeex:Lcom/vlingo/sdk/internal/audio/SpeexJNI;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBuffer:[S

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexOutputBuffer:[B

    const/16 v22, 0x3e8

    const/16 v23, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->encode([S[BII)I
    :try_end_2
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_1

    move-result v15

    .line 257
    .local v15, "outLen":I
    if-lez v15, :cond_c

    .line 258
    const/4 v13, -0x1

    .line 259
    .local v13, "lastSpeechSample":I
    const/16 v2, 0x3e0

    if-ge v15, v2, :cond_8

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexOutputBuffer:[B

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v15}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->convertBytesToInt([BI)I

    move-result v13

    .line 262
    const/4 v2, -0x1

    if-le v13, v2, :cond_7

    .line 263
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeechDetected:Z

    .line 268
    :cond_7
    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "maxRMS= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexOutputBuffer:[B

    add-int/lit8 v22, v15, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v4, v1}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->convertBytesToInt([BI)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_8
    if-nez v11, :cond_9

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mAutoEndpointingEnabled:Z

    if-eqz v2, :cond_9

    .line 273
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mTotalSamplesProcessed:I

    sub-int/2addr v2, v13

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSampleRate:I

    int-to-float v3, v3

    div-float v20, v2, v3

    .line 274
    .local v20, "timeSinceLastSpeechSample":F
    int-to-float v2, v13

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSampleRate:I

    int-to-float v3, v3

    div-float v14, v2, v3

    .line 275
    .local v14, "lastSpeechTime":F
    const/high16 v2, 0x3f000000    # 0.5f

    cmpl-float v2, v14, v2

    if-lez v2, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mAutoEndpointingTimeWithSpeech:F

    move/from16 v21, v0

    .line 276
    .local v21, "timeThresholdToUse":F
    :goto_5
    cmpl-float v2, v20, v21

    if-lez v2, :cond_9

    .line 279
    const/4 v11, 0x1

    .line 283
    .end local v14    # "lastSpeechTime":F
    .end local v20    # "timeSinceLastSpeechSample":F
    .end local v21    # "timeThresholdToUse":F
    :cond_9
    new-array v8, v15, [B

    .line 284
    .local v8, "data":[B
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexOutputBuffer:[B

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v8, v4, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 286
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mTotalMillisecProcessed:I

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v8, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->onDataAvailable([B[SI)V

    .line 293
    .end local v8    # "data":[B
    .end local v13    # "lastSpeechSample":I
    :goto_6
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mSpeexInputBufferSamples:I

    .line 296
    .end local v12    # "isEnd":I
    .end local v15    # "outLen":I
    .end local v16    # "rawData":[S
    :cond_a
    if-eqz v11, :cond_5

    .line 297
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->stop()V

    goto/16 :goto_4

    .line 250
    .restart local v12    # "isEnd":I
    .restart local v16    # "rawData":[S
    :catch_1
    move-exception v9

    .line 254
    .local v9, "e":Ljava/lang/UnsatisfiedLinkError;
    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;->READ_ERROR:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    invoke-virtual {v9}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->onError(Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 275
    .end local v9    # "e":Ljava/lang/UnsatisfiedLinkError;
    .restart local v13    # "lastSpeechSample":I
    .restart local v14    # "lastSpeechTime":F
    .restart local v15    # "outLen":I
    .restart local v20    # "timeSinceLastSpeechSample":F
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;->mAutoEndpointingTimeWithoutSpeech:F

    move/from16 v21, v0

    goto :goto_5

    .line 290
    .end local v13    # "lastSpeechSample":I
    .end local v14    # "lastSpeechTime":F
    .end local v20    # "timeSinceLastSpeechSample":F
    :cond_c
    const/4 v11, 0x1

    goto :goto_6
.end method
