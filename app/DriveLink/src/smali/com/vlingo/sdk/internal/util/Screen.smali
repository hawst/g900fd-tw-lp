.class public Lcom/vlingo/sdk/internal/util/Screen;
.super Ljava/lang/Object;
.source "Screen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static defaultMagnification()F
    .locals 1

    .prologue
    .line 72
    const v0, 0x4089999a    # 4.3f

    return v0
.end method

.method public static defaultWidth()I
    .locals 1

    .prologue
    .line 65
    const/16 v0, 0x500

    return v0
.end method

.method public static getInstance()Lcom/vlingo/sdk/internal/util/Screen;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/vlingo/sdk/internal/util/Screen;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/util/Screen;-><init>()V

    return-object v0
.end method

.method public static getMagnification()F
    .locals 3

    .prologue
    .line 48
    invoke-static {}, Lcom/vlingo/sdk/internal/util/Screen;->defaultMagnification()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "defaultMag":Ljava/lang/String;
    const-string/jumbo v2, "screen.mag"

    invoke-static {v2, v0}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "setting":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    return v2
.end method


# virtual methods
.method public getWidth()I
    .locals 3

    .prologue
    .line 36
    invoke-static {}, Lcom/vlingo/sdk/internal/util/Screen;->defaultWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "defaultWidth":Ljava/lang/String;
    const-string/jumbo v2, "screen.width"

    invoke-static {v2, v0}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "setting":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    return v2
.end method
