.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$FieldIdToGrammarBuilder$1;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;
.source "EmbeddedRecognizerSettingsXmlParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$FieldIdToGrammarBuilder;->addChildrenBuilders(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$FieldIdToGrammarBuilder;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$FieldIdToGrammarBuilder;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$FieldIdToGrammarBuilder$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$FieldIdToGrammarBuilder;

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method protected getChildBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v0

    return-object v0
.end method

.method protected getChildTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    const-string/jumbo v0, "RuleSet"

    return-object v0
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    const-string/jumbo v0, "RuleSets"

    return-object v0
.end method
