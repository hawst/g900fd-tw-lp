.class Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl$1;
.super Ljava/lang/Object;
.source "VLEmbeddedDialogManagerImpl.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;)V
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 25
    if-nez p2, :cond_1

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    const-string/jumbo v1, "USE_EDM"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27
    const-string/jumbo v1, "USE_EDM"

    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->access$000(Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 29
    .local v0, "newEnabledValue":Ljava/lang/Boolean;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->access$000(Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 30
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;

    # setter for: Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;
    invoke-static {v1, v0}, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->access$002(Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 31
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->enabled:Ljava/lang/Boolean;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;->access$000(Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 34
    invoke-static {}, Lcom/nuance/embeddeddialogmanager/EDM;->getInstance()Lcom/nuance/embeddeddialogmanager/EDM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/embeddeddialogmanager/EDM;->destroy()V

    goto :goto_0
.end method
