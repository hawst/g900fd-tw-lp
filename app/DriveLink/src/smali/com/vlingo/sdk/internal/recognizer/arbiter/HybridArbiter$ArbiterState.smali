.class final enum Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;
.super Ljava/lang/Enum;
.source "HybridArbiter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ArbiterState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

.field public static final enum FINISHED:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

.field public static final enum NETWORK_ERROR:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

.field public static final enum NO_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

.field public static final enum VOCON_BAD_QUALITY_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

.field public static final enum VOCON_ERROR:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

.field public static final enum WAITING_FOR_NETWORK_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 171
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    const-string/jumbo v1, "NO_RESULT"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->NO_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    .line 172
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    const-string/jumbo v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->NETWORK_ERROR:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    .line 173
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    const-string/jumbo v1, "VOCON_ERROR"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->VOCON_ERROR:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    .line 174
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    const-string/jumbo v1, "VOCON_BAD_QUALITY_RESULT"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->VOCON_BAD_QUALITY_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    .line 175
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    const-string/jumbo v1, "WAITING_FOR_NETWORK_RESULT"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->WAITING_FOR_NETWORK_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    .line 176
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    const-string/jumbo v1, "FINISHED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->FINISHED:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    .line 170
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->NO_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->NETWORK_ERROR:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->VOCON_ERROR:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->VOCON_BAD_QUALITY_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->WAITING_FOR_NETWORK_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->FINISHED:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 170
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 170
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;
    .locals 1

    .prologue
    .line 170
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    return-object v0
.end method
