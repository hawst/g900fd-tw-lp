.class public abstract Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
.super Ljava/lang/Object;
.source "VoconHypothesisNode.java"


# static fields
.field public static final BEGIN_TIME_MS:Ljava/lang/String; = "_beginTimeMs"

.field public static final CONFIDENCE:Ljava/lang/String; = "_conf"

.field public static final END_TIME_MS:Ljava/lang/String; = "_endTimeMs"

.field public static final SCORE:Ljava/lang/String; = "_score"

.field public static final TYPE:Ljava/lang/String; = "_type"

.field public static final USER_ID_HI:Ljava/lang/String; = "_userID.hi32"

.field public static final USER_ID_LO:Ljava/lang/String; = "_userID.lo32"


# instance fields
.field protected _beginTimeMs:Ljava/lang/Long;

.field protected _conf:Ljava/lang/Long;

.field protected _endTimeMs:Ljava/lang/Long;

.field protected _score:Ljava/lang/Long;

.field protected _userId:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract addTagNames(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public getBeginTimeMs()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->_beginTimeMs:Ljava/lang/Long;

    return-object v0
.end method

.method public getConfidence()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->_conf:Ljava/lang/Long;

    return-object v0
.end method

.method public getEndTimeMs()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->_endTimeMs:Ljava/lang/Long;

    return-object v0
.end method

.method public abstract getRecognizedUtterance(Z)Ljava/lang/String;
.end method

.method public getScore()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->_score:Ljava/lang/Long;

    return-object v0
.end method

.method public getUserId()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->_userId:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 160
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 162
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->_userId:Ljava/lang/Long;

    goto :goto_0
.end method
