.class public Lcom/vlingo/sdk/internal/util/Future;
.super Ljava/lang/Object;
.source "Future.java"


# instance fields
.field private alive:Z

.field private cancelled:Z

.field private complete:Z

.field private final run:Ljava/lang/Runnable;

.field private scheduleTask:Ljava/util/TimerTask;

.field private thread:Ljava/lang/Thread;


# direct methods
.method constructor <init>(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "run"    # Ljava/lang/Runnable;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->complete:Z

    .line 15
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->cancelled:Z

    .line 16
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->alive:Z

    .line 20
    iput-object p1, p0, Lcom/vlingo/sdk/internal/util/Future;->run:Ljava/lang/Runnable;

    .line 21
    return-void
.end method


# virtual methods
.method public declared-synchronized cancel()V
    .locals 2

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->complete:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->cancelled:Z

    if-nez v0, :cond_2

    .line 42
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/Future;->scheduleTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/Future;->scheduleTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/util/Future;->scheduleTask:Ljava/util/TimerTask;

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/Future;->thread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/util/Future;->thread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_1

    .line 47
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/Future;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 49
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->cancelled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    :cond_2
    monitor-exit p0

    return-void

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized complete()V
    .locals 1

    .prologue
    .line 58
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->complete:Z

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->alive:Z

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/util/Future;->thread:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/Future;->run:Ljava/lang/Runnable;

    return-object v0
.end method

.method public declared-synchronized isAlive()Z
    .locals 1

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->alive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized isCancelled()Z
    .locals 1

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->cancelled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized setScheduleTask(Ljava/util/TimerTask;)V
    .locals 1
    .param p1, "scheduleTask"    # Ljava/util/TimerTask;

    .prologue
    .line 28
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/util/Future;->scheduleTask:Ljava/util/TimerTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    monitor-exit p0

    return-void

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized setThread(Ljava/lang/Thread;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Thread;

    .prologue
    .line 32
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/util/Future;->thread:Ljava/lang/Thread;

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/Future;->alive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    monitor-exit p0

    return-void

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
