.class abstract Lcom/vlingo/sdk/internal/VLComponentImpl;
.super Ljava/lang/Object;
.source "VLComponentImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/VLComponent;


# instance fields
.field private mDestroyLock:Ljava/lang/Object;

.field private mHandler:Landroid/os/Handler;

.field private mIsBusy:Z

.field private mIsValid:Z

.field private mManager:Lcom/vlingo/sdk/internal/VLComponentManager;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 1
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    .line 27
    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mHandler:Landroid/os/Handler;

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mIsValid:Z

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mDestroyLock:Ljava/lang/Object;

    .line 30
    return-void
.end method


# virtual methods
.method public declared-synchronized destroy()V
    .locals 3

    .prologue
    .line 34
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mDestroyLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 37
    :try_start_1
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mIsValid:Z

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mIsValid:Z

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mHandler:Landroid/os/Handler;

    .line 40
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/VLComponentManager;->removeComponent(Ljava/lang/Class;)V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    .line 42
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->onDestroy()V

    .line 44
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45
    monitor-exit p0

    return-void

    .line 44
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 34
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract doesDependOnSettings()Z
.end method

.method getDestroyLock()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mDestroyLock:Ljava/lang/Object;

    return-object v0
.end method

.method getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method isBusy()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mIsBusy:Z

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mIsValid:Z

    return v0
.end method

.method abstract onDestroy()V
.end method

.method setBusy(Z)V
    .locals 0
    .param p1, "isBusy"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mIsBusy:Z

    .line 74
    return-void
.end method

.method validateInstance()V
    .locals 3

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLComponentImpl;->mIsValid:Z

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " instance is no longer valid!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    return-void
.end method
