.class public Lcom/vlingo/sdk/internal/NBestManager;
.super Ljava/lang/Object;
.source "NBestManager.java"


# static fields
.field private static final MAX_RESULTS:I = 0x5

.field private static final WORD_SEPARATORS:Ljava/lang/String; = ". ,;:!?\n()[]*&@{}/<>_+=|\""


# instance fields
.field private previousResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/NBestManager;->previousResults:Ljava/util/ArrayList;

    return-void
.end method

.method private getCurrentWord([CI)Ljava/lang/String;
    .locals 5
    .param p1, "phrase"    # [C
    .param p2, "cursorPosition"    # I

    .prologue
    .line 156
    aget-char v3, p1, p2

    invoke-static {v3}, Lcom/vlingo/sdk/internal/NBestManager;->isWordSeparator(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 157
    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x1

    invoke-direct {v3, p1, p2, v4}, Ljava/lang/String;-><init>([CII)V

    .line 176
    :goto_0
    return-object v3

    .line 159
    :cond_0
    const/4 v2, 0x0

    .line 160
    .local v2, "startIndex":I
    array-length v0, p1

    .line 163
    .local v0, "endIndex":I
    move v1, p2

    .local v1, "i":I
    :goto_1
    array-length v3, p1

    if-ge v1, v3, :cond_1

    .line 164
    aget-char v3, p1, v1

    invoke-static {v3}, Lcom/vlingo/sdk/internal/NBestManager;->isWordSeparator(C)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 165
    move v0, v1

    .line 169
    :cond_1
    move v1, p2

    :goto_2
    if-ltz v1, :cond_2

    .line 170
    aget-char v3, p1, v1

    invoke-static {v3}, Lcom/vlingo/sdk/internal/NBestManager;->isWordSeparator(C)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 171
    add-int/lit8 v2, v1, 0x1

    .line 176
    :cond_2
    new-instance v3, Ljava/lang/String;

    sub-int v4, v0, v2

    invoke-direct {v3, p1, v2, v4}, Ljava/lang/String;-><init>([CII)V

    goto :goto_0

    .line 163
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 169
    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_2
.end method

.method private getWordsAfter([Ljava/lang/String;[CI)I
    .locals 8
    .param p1, "wordsAfter"    # [Ljava/lang/String;
    .param p2, "phrase"    # [C
    .param p3, "cursorPosition"    # I

    .prologue
    .line 124
    const/4 v0, 0x0

    .line 125
    .local v0, "count":I
    move v3, p3

    .line 126
    .local v3, "i":I
    array-length v4, p2

    .line 128
    .local v4, "len":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-char v6, p2, v3

    invoke-static {v6}, Lcom/vlingo/sdk/internal/NBestManager;->isWordSeparator(C)Z

    move-result v6

    if-nez v6, :cond_1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 140
    .local v5, "startIndex":I
    :cond_0
    move v2, v3

    .line 142
    .local v2, "endIndex":I
    if-ge v5, v2, :cond_1

    .line 143
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    new-instance v6, Ljava/lang/String;

    sub-int v7, v2, v5

    invoke-direct {v6, p2, v5, v7}, Ljava/lang/String;-><init>([CII)V

    aput-object v6, p1, v0

    move v0, v1

    .line 130
    .end local v1    # "count":I
    .end local v2    # "endIndex":I
    .end local v5    # "startIndex":I
    .restart local v0    # "count":I
    :cond_1
    array-length v6, p1

    if-ge v0, v6, :cond_3

    .line 133
    :goto_1
    if-ge v3, v4, :cond_2

    aget-char v6, p2, v3

    invoke-static {v6}, Lcom/vlingo/sdk/internal/NBestManager;->isWordSeparator(C)Z

    move-result v6

    if-eqz v6, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 134
    :cond_2
    move v5, v3

    .line 136
    .restart local v5    # "startIndex":I
    if-lt v5, v4, :cond_4

    .line 146
    .end local v5    # "startIndex":I
    :cond_3
    return v0

    .line 139
    .restart local v5    # "startIndex":I
    :cond_4
    :goto_2
    if-ge v3, v4, :cond_0

    aget-char v6, p2, v3

    invoke-static {v6}, Lcom/vlingo/sdk/internal/NBestManager;->isWordSeparator(C)Z

    move-result v6

    if-nez v6, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private getWordsBefore([Ljava/lang/String;[CI)I
    .locals 7
    .param p1, "wordsBefore"    # [Ljava/lang/String;
    .param p2, "phrase"    # [C
    .param p3, "cursorPosition"    # I

    .prologue
    .line 99
    const/4 v0, 0x0

    .line 100
    .local v0, "count":I
    move v3, p3

    .line 102
    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_1

    aget-char v5, p2, v3

    invoke-static {v5}, Lcom/vlingo/sdk/internal/NBestManager;->isWordSeparator(C)Z

    move-result v5

    if-nez v5, :cond_1

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 113
    .local v2, "endIndex":I
    :cond_0
    add-int/lit8 v4, v3, 0x1

    .line 115
    .local v4, "startIndex":I
    if-ge v4, v2, :cond_1

    .line 116
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    new-instance v5, Ljava/lang/String;

    sub-int v6, v2, v4

    invoke-direct {v5, p2, v4, v6}, Ljava/lang/String;-><init>([CII)V

    aput-object v5, p1, v0

    move v0, v1

    .line 104
    .end local v1    # "count":I
    .end local v2    # "endIndex":I
    .end local v4    # "startIndex":I
    .restart local v0    # "count":I
    :cond_1
    array-length v5, p1

    if-ge v0, v5, :cond_3

    .line 107
    :goto_1
    if-ltz v3, :cond_2

    aget-char v5, p2, v3

    invoke-static {v5}, Lcom/vlingo/sdk/internal/NBestManager;->isWordSeparator(C)Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 108
    :cond_2
    add-int/lit8 v2, v3, 0x1

    .line 109
    .restart local v2    # "endIndex":I
    if-gtz v2, :cond_4

    .line 119
    .end local v2    # "endIndex":I
    :cond_3
    return v0

    .line 112
    .restart local v2    # "endIndex":I
    :cond_4
    :goto_2
    if-ltz v3, :cond_0

    aget-char v5, p2, v3

    invoke-static {v5}, Lcom/vlingo/sdk/internal/NBestManager;->isWordSeparator(C)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v3, v3, -0x1

    goto :goto_2
.end method

.method private static isWordSeparator(C)Z
    .locals 2
    .param p0, "c"    # C

    .prologue
    .line 150
    const-string/jumbo v0, ". ,;:!?\n()[]*&@{}/<>_+=|\""

    invoke-static {p0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private moveCursorIfInSpace([CI)I
    .locals 2
    .param p1, "phrase"    # [C
    .param p2, "cursorPosition"    # I

    .prologue
    const/4 v0, -0x1

    .line 81
    if-ltz p2, :cond_0

    array-length v1, p1

    if-lt p2, v1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return v0

    .line 82
    :cond_1
    aget-char v1, p1, p2

    invoke-static {v1}, Ljava/lang/Character;->isSpace(C)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 83
    if-lez p2, :cond_2

    add-int/lit8 v1, p2, -0x1

    aget-char v1, p1, v1

    invoke-static {v1}, Ljava/lang/Character;->isSpace(C)Z

    move-result v1

    if-nez v1, :cond_2

    .line 85
    add-int/lit8 v0, p2, -0x1

    goto :goto_0

    .line 87
    :cond_2
    add-int/lit8 p2, p2, 0x1

    .line 89
    :cond_3
    array-length v1, p1

    if-ge p2, v1, :cond_0

    .line 90
    aget-char v1, p1, p2

    invoke-static {v1}, Ljava/lang/Character;->isSpace(C)Z

    move-result v1

    if-nez v1, :cond_0

    move v0, p2

    .line 92
    goto :goto_0
.end method


# virtual methods
.method public getNBestForWord(Ljava/lang/String;I)Ljava/util/List;
    .locals 13
    .param p1, "phrase"    # Ljava/lang/String;
    .param p2, "cursorPosition"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    const/4 v8, 0x3

    .line 44
    .local v8, "MAX_SURROUNDING_WORDS":I
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    .line 46
    .local v10, "charPhrase":[C
    invoke-direct {p0, v10, p2}, Lcom/vlingo/sdk/internal/NBestManager;->moveCursorIfInSpace([CI)I

    move-result v9

    .line 47
    .local v9, "adjustedCursor":I
    if-gez v9, :cond_1

    const/4 v1, 0x0

    .line 77
    :cond_0
    :goto_0
    return-object v1

    .line 49
    :cond_1
    invoke-direct {p0, v10, v9}, Lcom/vlingo/sdk/internal/NBestManager;->getCurrentWord([CI)Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "word":Ljava/lang/String;
    if-nez v2, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    .line 51
    :cond_2
    const/4 v0, 0x3

    new-array v3, v0, [Ljava/lang/String;

    .line 52
    .local v3, "previousWords":[Ljava/lang/String;
    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/String;

    .line 53
    .local v5, "nextWords":[Ljava/lang/String;
    invoke-direct {p0, v3, v10, v9}, Lcom/vlingo/sdk/internal/NBestManager;->getWordsBefore([Ljava/lang/String;[CI)I

    move-result v4

    .line 54
    .local v4, "numPreviousWords":I
    invoke-direct {p0, v5, v10, v9}, Lcom/vlingo/sdk/internal/NBestManager;->getWordsAfter([Ljava/lang/String;[CI)I

    move-result v6

    .line 64
    .local v6, "numNextWords":I
    iget-object v0, p0, Lcom/vlingo/sdk/internal/NBestManager;->previousResults:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 66
    .local v12, "resnum":I
    const/4 v7, 0x0

    .line 67
    .local v7, "numMatchingWords":I
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 68
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v12, :cond_3

    .line 69
    iget-object v0, p0, Lcom/vlingo/sdk/internal/NBestManager;->previousResults:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    invoke-virtual/range {v0 .. v7}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->getNBestForWordIfBetterMatch(Ljava/util/List;Ljava/lang/String;[Ljava/lang/String;I[Ljava/lang/String;II)I

    move-result v7

    .line 68
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 76
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNBestForWord(Ljava/lang/String;)Ljava/util/Vector;
    .locals 4
    .param p1, "word"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v3, p0, Lcom/vlingo/sdk/internal/NBestManager;->previousResults:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 32
    .local v1, "resnum":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 33
    iget-object v3, p0, Lcom/vlingo/sdk/internal/NBestManager;->previousResults:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    invoke-virtual {v3, p1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->getNBestForWord(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v2

    .line 34
    .local v2, "result":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    .line 38
    .end local v2    # "result":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    :goto_1
    return-object v2

    .line 32
    .restart local v2    # "result":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    .end local v2    # "result":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public registerResults(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)V
    .locals 2
    .param p1, "results"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/sdk/internal/NBestManager;->previousResults:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 24
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/NBestManager;->previousResults:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    .line 25
    iget-object v0, p0, Lcom/vlingo/sdk/internal/NBestManager;->previousResults:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/NBestManager;->previousResults:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 27
    :cond_0
    return-void
.end method
