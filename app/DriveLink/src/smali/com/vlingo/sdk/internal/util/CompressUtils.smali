.class public Lcom/vlingo/sdk/internal/util/CompressUtils;
.super Ljava/lang/Object;
.source "CompressUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/vlingo/sdk/internal/util/CompressUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/util/CompressUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deflate([B)[B
    .locals 8
    .param p0, "dataToCompress"    # [B

    .prologue
    .line 28
    const/4 v3, 0x0

    .line 29
    .local v3, "zlos":Ljava/util/zip/DeflaterOutputStream;
    const/4 v2, 0x0

    .line 30
    .local v2, "result":[B
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    array-length v5, p0

    invoke-direct {v0, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 34
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v4, Ljava/util/zip/DeflaterOutputStream;

    invoke-direct {v4, v0}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    .end local v3    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    .local v4, "zlos":Ljava/util/zip/DeflaterOutputStream;
    :try_start_1
    invoke-virtual {v4, p0}, Ljava/util/zip/DeflaterOutputStream;->write([B)V

    .line 36
    invoke-virtual {v4}, Ljava/util/zip/DeflaterOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 37
    const/4 v3, 0x0

    .line 38
    .end local v4    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    .restart local v3    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 47
    if-eqz v3, :cond_0

    :try_start_3
    invoke-virtual {v3}, Ljava/util/zip/DeflaterOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    .line 48
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 50
    :cond_1
    :goto_1
    return-object v2

    .line 44
    :catch_0
    move-exception v1

    .line 45
    .local v1, "ex":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    sget-object v5, Lcom/vlingo/sdk/internal/util/CompressUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Error compressing: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 47
    if-eqz v3, :cond_2

    :try_start_6
    invoke-virtual {v3}, Ljava/util/zip/DeflaterOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 48
    :cond_2
    :goto_3
    if-eqz v0, :cond_1

    :try_start_7
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_1

    .end local v1    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    goto :goto_1

    .line 47
    :catchall_0
    move-exception v5

    :goto_4
    if-eqz v3, :cond_3

    :try_start_8
    invoke-virtual {v3}, Ljava/util/zip/DeflaterOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 48
    :cond_3
    :goto_5
    if-eqz v0, :cond_4

    :try_start_9
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    .line 47
    :cond_4
    :goto_6
    throw v5

    :catch_2
    move-exception v6

    goto :goto_5

    .line 48
    :catch_3
    move-exception v6

    goto :goto_6

    .line 47
    .restart local v1    # "ex":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    goto :goto_3

    .end local v1    # "ex":Ljava/lang/Exception;
    :catch_5
    move-exception v5

    goto :goto_0

    .end local v3    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    .restart local v4    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    .restart local v3    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    goto :goto_4

    .line 44
    .end local v3    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    .restart local v4    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    :catch_6
    move-exception v1

    move-object v3, v4

    .end local v4    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    .restart local v3    # "zlos":Ljava/util/zip/DeflaterOutputStream;
    goto :goto_2
.end method
