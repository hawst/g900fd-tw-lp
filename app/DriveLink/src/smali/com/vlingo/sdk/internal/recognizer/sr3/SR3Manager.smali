.class public Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;
.super Lcom/vlingo/sdk/internal/recognizer/network/SRManager;
.source "SR3Manager.java"


# instance fields
.field private final connectionProvider:Lcom/vlingo/sdk/internal/net/ConnectionProvider;

.field private ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

.field private ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

.field private final ivExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

.field private volatile ivLastGuttID:Ljava/lang/String;

.field private ivRequestId:I

.field private ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

.field private final responseParser:Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

.field serverDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

.field private final timings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;)V
    .locals 2
    .param p1, "connectionProvider"    # Lcom/vlingo/sdk/internal/net/ConnectionProvider;
    .param p2, "timings"    # Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;-><init>()V

    .line 43
    new-instance v0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    const-string/jumbo v1, "SR3Worker"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .line 56
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivRequestId:I

    .line 64
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->connectionProvider:Lcom/vlingo/sdk/internal/net/ConnectionProvider;

    .line 65
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->timings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .line 66
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->responseParser:Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

    .line 67
    return-void
.end method

.method private getChunkingConnection(Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;
    .locals 8
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 139
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->isAsrKeepAliveEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 140
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    const-string/jumbo v1, "POST"

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->serverDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;->getASRURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v2

    iget-object v2, v2, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivRequestId:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivRequestId:I

    move-object v7, p1

    invoke-static/range {v0 .. v7}, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->newConnection(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/util/Hashtable;ILcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    if-eqz v0, :cond_1

    .line 150
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :cond_1
    :goto_1
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->serverDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    invoke-interface {v1}, Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;->getASRURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/custom/MultiplexHttpConnection;-><init>(Lcom/vlingo/sdk/internal/http/URL;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    .line 155
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->open()V

    .line 156
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    const-string/jumbo v1, "POST"

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->serverDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;->getASRURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v2

    iget-object v2, v2, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivRequestId:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivRequestId:I

    move-object v7, p1

    invoke-static/range {v0 .. v7}, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->newConnection(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/util/Hashtable;ILcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;

    move-result-object v0

    goto :goto_0

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->connectionProvider:Lcom/vlingo/sdk/internal/net/ConnectionProvider;

    const-string/jumbo v1, "POST"

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->serverDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;->getASRURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivRequestId:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivRequestId:I

    move-object v7, p1

    invoke-static/range {v0 .. v7}, Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;->newConnection(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Ljava/lang/String;Lcom/vlingo/sdk/internal/http/URL;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/util/Hashtable;ILcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;

    move-result-object v0

    goto :goto_0

    .line 150
    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->shutdown()V

    .line 92
    return-void
.end method

.method public getConnection(Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;
    .locals 8
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->isAudioStreamingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->getChunkingConnection(Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/ChunkingHttpConnection;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->connectionProvider:Lcom/vlingo/sdk/internal/net/ConnectionProvider;

    const-string/jumbo v1, "POST"

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->serverDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;->getASRURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v2

    iget-object v2, v2, Lcom/vlingo/sdk/internal/http/URL;->urlField:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    const/4 v5, 0x0

    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivRequestId:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivRequestId:I

    move-object v7, p1

    invoke-static/range {v0 .. v7}, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->newConnection(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/util/Hashtable;ILcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    move-result-object v0

    goto :goto_0
.end method

.method public getLastGuttID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivLastGuttID:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseParser()Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->responseParser:Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

    return-object v0
.end method

.method public getServerDetails()Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->serverDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    return-object v0
.end method

.method public init(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V
    .locals 2
    .param p1, "serverDetails"    # Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;
    .param p2, "clientMeta"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p3, "softwareMeta"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .prologue
    .line 70
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    .line 71
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .line 73
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->setServer(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;)V

    .line 75
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->setMaxPoolSize(I)V

    .line 76
    return-void
.end method

.method public newRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
    .locals 1
    .param p1, "context"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .prologue
    .line 99
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->newRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;Z)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    move-result-object v0

    return-object v0
.end method

.method public newRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;Z)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
    .locals 7
    .param p1, "context"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    .param p3, "sendAudio"    # Z

    .prologue
    .line 103
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->timings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    move-object v1, p1

    move-object v4, p0

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;-><init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;Z)V

    .line 104
    .local v0, "request":Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;
    invoke-virtual {v0, p2}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->addListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V

    .line 109
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)Lcom/vlingo/sdk/internal/util/Future;

    .line 110
    return-object v0
.end method

.method public readyForRecognition()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)V
    .locals 4
    .param p1, "stats"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->isStatsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;-><init>()V

    .line 173
    .local v0, "collection":Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;
    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->addStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)V

    .line 174
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->serverDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->schedule(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V

    .line 176
    .end local v0    # "collection":Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;
    :cond_0
    return-void
.end method

.method public sendStatsCollection(Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V
    .locals 3
    .param p1, "collection"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->isAcceptedTextEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->serverDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    invoke-virtual {p1, v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->schedule(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V

    .line 184
    :cond_0
    return-void
.end method

.method setLastGuttID(Ljava/lang/String;)V
    .locals 0
    .param p1, "guttID"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->ivLastGuttID:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public setServer(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;)V
    .locals 0
    .param p1, "serverDetails"    # Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->serverDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    .line 84
    return-void
.end method
