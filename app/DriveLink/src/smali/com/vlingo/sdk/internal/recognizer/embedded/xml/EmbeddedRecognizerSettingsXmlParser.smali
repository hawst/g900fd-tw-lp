.class public final Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;
.super Ljava/lang/Object;
.source "EmbeddedRecognizerSettingsXmlParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$FieldIdToGrammarBuilder;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$TagToActionBuilder;
    }
.end annotation


# static fields
.field private static final NAMESPACE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->NAMESPACE:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public static getFieldIdToGrammarBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 154
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$FieldIdToGrammarBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$FieldIdToGrammarBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$1;)V

    return-object v0
.end method

.method public static getTagToActionBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$TagToActionBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$TagToActionBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser$1;)V

    return-object v0
.end method

.method public static parse(Ljava/io/InputStream;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/InputStream;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "builder":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<TT;>;"
    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 41
    .local v0, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const-string/jumbo v1, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V

    .line 42
    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 43
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 44
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->readTags(Lorg/xmlpull/v1/XmlPullParser;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 46
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 44
    return-object v1

    .line 46
    .end local v0    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catchall_0
    move-exception v1

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method private static readTags(Lorg/xmlpull/v1/XmlPullParser;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;)Ljava/lang/Object;
    .locals 8
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "builder":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<TT;>;"
    const/4 v7, 0x3

    const/4 v6, 0x2

    .line 62
    sget-object v4, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->NAMESPACE:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;->getTagName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p0, v6, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;->hasParameters()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 64
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;->getParamNames()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 65
    .local v3, "paramName":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->NAMESPACE:Ljava/lang/String;

    invoke-interface {p0, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 68
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "paramName":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;->hasChildTags()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 69
    :cond_1
    :goto_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    if-eq v4, v7, :cond_4

    .line 70
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    if-ne v4, v6, :cond_1

    .line 74
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "childTagName":Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;->getChildNodeBuilder(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v0

    .line 77
    .local v0, "childBuilder":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;"
    if-eqz v0, :cond_2

    .line 78
    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->readTags(Lorg/xmlpull/v1/XmlPullParser;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v1, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;->addChildTagObject(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 80
    :cond_2
    invoke-static {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_1

    .line 87
    .end local v0    # "childBuilder":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;"
    .end local v1    # "childTagName":Ljava/lang/String;
    :cond_3
    :goto_2
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    if-eq v4, v7, :cond_4

    .line 88
    invoke-static {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_2

    .line 91
    :cond_4
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;->build()Ljava/lang/Object;

    move-result-object v4

    return-object v4
.end method

.method private static skip(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 96
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 98
    :cond_0
    const/4 v0, 0x1

    .line 99
    .local v0, "depth":I
    :goto_0
    if-eqz v0, :cond_1

    .line 100
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 105
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    .line 103
    goto :goto_0

    .line 109
    :cond_1
    return-void

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
