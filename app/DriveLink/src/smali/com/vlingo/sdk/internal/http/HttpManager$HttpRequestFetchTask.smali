.class final Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
.super Ljava/lang/Object;
.source "HttpManager.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/util/ThreadPoolRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/http/HttpManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HttpRequestFetchTask"
.end annotation


# instance fields
.field private final background:Z

.field private finished:Z

.field private future:Lcom/vlingo/sdk/internal/util/Future;

.field private final highPriority:Z

.field private final ordered:Z

.field private final parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

.field final synthetic this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

.field private timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

.field private volatile waitTime:J


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 6
    .param p2, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    const/4 v3, 0x0

    .line 212
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;-><init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;ZZZ)V

    .line 213
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/http/HttpManager;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p3, "x2"    # Lcom/vlingo/sdk/internal/http/HttpManager$1;

    .prologue
    .line 182
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;-><init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    return-void
.end method

.method private constructor <init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;ZZZ)V
    .locals 2
    .param p2, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p3, "background"    # Z
    .param p4, "highPriority"    # Z
    .param p5, "ordered"    # Z

    .prologue
    const/4 v0, 0x0

    .line 215
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->future:Lcom/vlingo/sdk/internal/util/Future;

    .line 196
    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finished:Z

    .line 209
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->waitTime:J

    .line 216
    iput-object p2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 217
    iput-boolean p3, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->background:Z

    .line 218
    iput-boolean p4, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->highPriority:Z

    .line 219
    iput-boolean p5, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->ordered:Z

    .line 220
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;ZZZLcom/vlingo/sdk/internal/http/HttpManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/http/HttpManager;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p3, "x2"    # Z
    .param p4, "x3"    # Z
    .param p5, "x4"    # Z
    .param p6, "x5"    # Lcom/vlingo/sdk/internal/http/HttpManager$1;

    .prologue
    .line 182
    invoke-direct/range {p0 .. p5}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;-><init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;ZZZ)V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->start()V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;J)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    .param p1, "x1"    # J

    .prologue
    .line 182
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->schedule(J)V

    return-void
.end method

.method private declared-synchronized finish()V
    .locals 1

    .prologue
    .line 287
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finished:Z

    .line 288
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;->cancel()Z

    .line 290
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->future:Lcom/vlingo/sdk/internal/util/Future;

    if-eqz v0, :cond_1

    .line 293
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->future:Lcom/vlingo/sdk/internal/util/Future;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/util/Future;->cancel()V

    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->future:Lcom/vlingo/sdk/internal/util/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    :cond_1
    monitor-exit p0

    return-void

    .line 287
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized schedule(J)V
    .locals 4
    .param p1, "delay"    # J

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finished:Z

    if-nez v0, :cond_1

    .line 249
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    .line 250
    iput-wide p1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->waitTime:J

    .line 251
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

    # getter for: Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/HttpManager;->access$400(Lcom/vlingo/sdk/internal/http/HttpManager;)Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->executeLater(Ljava/lang/Runnable;J)Lcom/vlingo/sdk/internal/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->future:Lcom/vlingo/sdk/internal/util/Future;

    .line 257
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;->cancel()Z

    .line 259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTimeout()I

    move-result v0

    if-lez v0, :cond_1

    .line 262
    new-instance v0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;-><init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpManager$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    .line 263
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

    # getter for: Lcom/vlingo/sdk/internal/http/HttpManager;->timeoutTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/HttpManager;->access$700(Lcom/vlingo/sdk/internal/http/HttpManager;)Ljava/util/Timer;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    :cond_1
    monitor-exit p0

    return-void

    .line 254
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

    # getter for: Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/HttpManager;->access$400(Lcom/vlingo/sdk/internal/http/HttpManager;)Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)Lcom/vlingo/sdk/internal/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->future:Lcom/vlingo/sdk/internal/util/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized start()V
    .locals 4

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finished:Z

    if-nez v0, :cond_1

    .line 228
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->background:Z

    if-eqz v0, :cond_2

    .line 229
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

    # getter for: Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/HttpManager;->access$400(Lcom/vlingo/sdk/internal/http/HttpManager;)Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)Lcom/vlingo/sdk/internal/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->future:Lcom/vlingo/sdk/internal/util/Future;

    .line 234
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;->cancel()Z

    .line 236
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTimeout()I

    move-result v0

    if-lez v0, :cond_1

    .line 239
    new-instance v0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;-><init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpManager$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    .line 240
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

    # getter for: Lcom/vlingo/sdk/internal/http/HttpManager;->timeoutTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/HttpManager;->access$700(Lcom/vlingo/sdk/internal/http/HttpManager;)Ljava/util/Timer;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    :cond_1
    monitor-exit p0

    return-void

    .line 231
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

    # getter for: Lcom/vlingo/sdk/internal/http/HttpManager;->onDemandExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/HttpManager;->access$500(Lcom/vlingo/sdk/internal/http/HttpManager;)Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)Lcom/vlingo/sdk/internal/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->future:Lcom/vlingo/sdk/internal/util/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized cancel()V
    .locals 1

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finished:Z

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->notifyCancelled()V

    .line 271
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    :cond_0
    monitor-exit p0

    return-void

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isHighPriority()Z
    .locals 1

    .prologue
    .line 316
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->background:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->highPriority:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOrdered()Z
    .locals 1

    .prologue
    .line 320
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->background:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->ordered:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized isRetry()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 324
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finished:Z

    if-nez v2, :cond_2

    .line 325
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/HttpRequest;->isRetry()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 326
    iget-boolean v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->background:Z

    if-eqz v2, :cond_0

    .line 327
    iget-wide v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->waitTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    .line 341
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 333
    :cond_1
    :try_start_1
    iget-wide v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->waitTime:J

    const-wide/16 v4, 0x2

    mul-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->schedule(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 334
    goto :goto_0

    :cond_2
    move v0, v1

    .line 341
    goto :goto_0

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 300
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->notifyWillExecute()V

    .line 303
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->fetchResponse()Lcom/vlingo/sdk/internal/http/HttpResponse;

    move-result-object v0

    .line 305
    .local v0, "response":Lcom/vlingo/sdk/internal/http/HttpResponse;
    monitor-enter p0

    .line 306
    :try_start_0
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finished:Z

    if-nez v1, :cond_0

    .line 307
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finish()V

    .line 308
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->notifyResponse(Lcom/vlingo/sdk/internal/http/HttpResponse;)V

    .line 310
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->this$0:Lcom/vlingo/sdk/internal/http/HttpManager;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    # invokes: Lcom/vlingo/sdk/internal/http/HttpManager;->requestWasRan(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/http/HttpManager;->access$800(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    .line 313
    return-void

    .line 310
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public declared-synchronized timeout(Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;)Z
    .locals 1
    .param p1, "originatingTask"    # Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeoutTask:Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    if-ne p1, v0, :cond_1

    .line 277
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finished:Z

    if-nez v0, :cond_0

    .line 278
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finish()V

    .line 279
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->parentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->notifyTimeout()Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finished:Z

    .line 281
    :cond_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->finished:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
