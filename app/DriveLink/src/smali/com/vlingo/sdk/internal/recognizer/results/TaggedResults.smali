.class public Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;
.super Ljava/lang/Object;
.source "TaggedResults.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$1;,
        Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;,
        Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    }
.end annotation


# static fields
.field private static final SHOW_DEBUG:Z = true


# instance fields
.field private curParseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

.field private curTag:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

.field private groups:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private guttid:Ljava/lang/String;

.field private uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "guttId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 65
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    .line 75
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->curParseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    .line 76
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->curTag:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    .line 82
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->guttid:Ljava/lang/String;

    .line 83
    return-void
.end method

.method private static final collapseWords(Ljava/lang/StringBuffer;[Ljava/lang/String;)V
    .locals 2
    .param p0, "sb"    # Ljava/lang/StringBuffer;
    .param p1, "words"    # [Ljava/lang/String;

    .prologue
    .line 173
    array-length v1, p1

    if-lez v1, :cond_0

    .line 174
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 175
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 176
    const-string/jumbo v1, " "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 177
    aget-object v1, p1, v0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private static final serializeParseGroups(Ljava/lang/StringBuffer;Ljava/util/Vector;)V
    .locals 13
    .param p0, "sb"    # Ljava/lang/StringBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 259
    .local p1, "groups":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Ljava/util/Vector;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_8

    .line 260
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v7

    .line 261
    .local v7, "size":I
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "<T n=\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 262
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v7, :cond_7

    .line 263
    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    .line 264
    .local v6, "pg":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "<pg t=\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    # getter for: Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->parseType:Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->access$400(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\" c=\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    # getter for: Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->confidence:F
    invoke-static {v6}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->access$500(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;)F

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\" n=\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    # getter for: Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->access$200(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;)Ljava/util/Vector;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Vector;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 266
    # getter for: Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->access$200(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;)Ljava/util/Vector;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v8

    .line 267
    .local v8, "tSize":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    if-ge v3, v8, :cond_6

    .line 268
    # getter for: Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->access$200(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;)Ljava/util/Vector;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    .line 269
    .local v9, "tag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    # getter for: Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    invoke-static {v9}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->access$300(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;)Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-result-object v11

    iget-object v10, v11, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 270
    .local v10, "ul":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    # getter for: Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    invoke-static {v9}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->access$300(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;)Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-result-object v11

    iget-object v0, v11, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListCannonical:[Ljava/lang/String;

    .line 271
    .local v0, "cannonical":[Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "<tag n=\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    array-length v12, v10

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\" "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 272
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "nm=\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    # getter for: Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->name:Ljava/lang/String;
    invoke-static {v9}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->access$600(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 274
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_2
    array-length v11, v10

    if-ge v4, v11, :cond_5

    .line 275
    const-string/jumbo v11, "<tl "

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 276
    if-eqz v0, :cond_0

    array-length v11, v0

    if-le v11, v4, :cond_0

    aget-object v11, v0, v4

    if-eqz v11, :cond_0

    .line 277
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "cf=\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v0, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\" "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 278
    :cond_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "n=\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v10, v4

    array-length v12, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 279
    const/4 v5, 0x0

    .local v5, "l":I
    :goto_3
    aget-object v11, v10, v4

    array-length v11, v11

    if-ge v5, v11, :cond_4

    .line 280
    aget-object v11, v10, v4

    aget-object v11, v11, v5

    iget v2, v11, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->id:I

    .line 281
    .local v2, "id":I
    const/4 v11, -0x1

    if-eq v2, v11, :cond_3

    .line 282
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "<w id=\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 285
    :goto_4
    aget-object v11, v10, v4

    aget-object v11, v11, v5

    iget-boolean v11, v11, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->noSpace:Z

    if-eqz v11, :cond_1

    .line 286
    const-string/jumbo v11, " ns=\"t\""

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 287
    :cond_1
    aget-object v11, v10, v4

    aget-object v11, v11, v5

    iget-boolean v11, v11, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->noSpaceNumber:Z

    if-eqz v11, :cond_2

    .line 288
    const-string/jumbo v11, " nsd=\"t\""

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 289
    :cond_2
    const-string/jumbo v11, ">"

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 290
    aget-object v11, v10, v4

    aget-object v11, v11, v5

    iget-object v11, v11, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 291
    const-string/jumbo v11, "</w>"

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 279
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 284
    :cond_3
    const-string/jumbo v11, "<w"

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 293
    .end local v2    # "id":I
    :cond_4
    const-string/jumbo v11, "</tl>"

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 274
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 295
    .end local v5    # "l":I
    :cond_5
    const-string/jumbo v11, "</tag>"

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 267
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 297
    .end local v0    # "cannonical":[Ljava/lang/String;
    .end local v4    # "k":I
    .end local v9    # "tag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    .end local v10    # "ul":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_6
    const-string/jumbo v11, "</pg>"

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 262
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 299
    .end local v3    # "j":I
    .end local v6    # "pg":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    .end local v8    # "tSize":I
    :cond_7
    const-string/jumbo v11, "</T>"

    invoke-virtual {p0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 301
    .end local v1    # "i":I
    .end local v7    # "size":I
    :cond_8
    return-void
.end method

.method private static serializeUttList(Ljava/lang/StringBuffer;Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)V
    .locals 7
    .param p0, "sb"    # Ljava/lang/StringBuffer;
    .param p1, "results"    # Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .prologue
    .line 207
    iget-object v4, p1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 208
    .local v4, "ul":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget-object v0, p1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListConf:[F

    .line 209
    .local v0, "conf":[F
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "<UL n=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 211
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v4

    if-ge v1, v5, :cond_5

    .line 212
    const-string/jumbo v5, "<c "

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 213
    if-eqz v0, :cond_0

    array-length v5, v0

    if-ge v1, v5, :cond_0

    .line 214
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "c=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v0, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\" "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 215
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "n=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v4, v1

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 217
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    aget-object v5, v4, v1

    array-length v5, v5

    if-ge v3, v5, :cond_4

    .line 218
    aget-object v5, v4, v1

    aget-object v5, v5, v3

    iget v2, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->id:I

    .line 219
    .local v2, "id":I
    const/4 v5, -0x1

    if-eq v2, v5, :cond_3

    .line 220
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "<w id=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 223
    :goto_2
    aget-object v5, v4, v1

    aget-object v5, v5, v3

    iget-boolean v5, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->noSpace:Z

    if-eqz v5, :cond_1

    .line 224
    const-string/jumbo v5, " ns=\"t\""

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 225
    :cond_1
    aget-object v5, v4, v1

    aget-object v5, v5, v3

    iget-boolean v5, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->noSpaceNumber:Z

    if-eqz v5, :cond_2

    .line 226
    const-string/jumbo v5, " nsd=\"t\""

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 227
    :cond_2
    const-string/jumbo v5, ">"

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 228
    aget-object v5, v4, v1

    aget-object v5, v5, v3

    iget-object v5, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 229
    const-string/jumbo v5, "</w>"

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 217
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 222
    :cond_3
    const-string/jumbo v5, "<w"

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 231
    .end local v2    # "id":I
    :cond_4
    const-string/jumbo v5, "</c>"

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 211
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 233
    .end local v3    # "j":I
    :cond_5
    const-string/jumbo v5, "</UL>"

    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 234
    return-void
.end method

.method private static serializeWordList(Ljava/lang/StringBuffer;Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)V
    .locals 8
    .param p0, "sb"    # Ljava/lang/StringBuffer;
    .param p1, "results"    # Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .prologue
    .line 236
    iget-object v5, p1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 237
    .local v5, "wl":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "<WL n=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v5

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 239
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v5

    if-ge v0, v6, :cond_3

    .line 240
    aget-object v2, v5, v0

    .line 241
    .local v2, "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget-object v4, v2, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iChoices:[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;

    .line 242
    .local v4, "wcl":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "<w id=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\" n=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 243
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    array-length v6, v4

    if-ge v1, v6, :cond_2

    .line 244
    aget-object v3, v4, v1

    .line 245
    .local v3, "wc":Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "<c r=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->numAlign:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 246
    iget-boolean v6, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->noSpace:Z

    if-eqz v6, :cond_0

    .line 247
    const-string/jumbo v6, " ns=\"t\""

    invoke-virtual {p0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 248
    :cond_0
    iget-boolean v6, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->noSpaceNumber:Z

    if-eqz v6, :cond_1

    .line 249
    const-string/jumbo v6, " nsd=\"t\""

    invoke-virtual {p0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 250
    :cond_1
    const-string/jumbo v6, ">"

    invoke-virtual {p0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 251
    iget-object v6, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->words:[Ljava/lang/String;

    invoke-static {p0, v6}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->collapseWords(Ljava/lang/StringBuffer;[Ljava/lang/String;)V

    .line 252
    const-string/jumbo v6, "</c>"

    invoke-virtual {p0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 243
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 254
    .end local v3    # "wc":Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    :cond_2
    const-string/jumbo v6, "</w>"

    invoke-virtual {p0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 239
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 256
    .end local v1    # "j":I
    .end local v2    # "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .end local v4    # "wcl":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    :cond_3
    const-string/jumbo v6, "</WL>"

    invoke-virtual {p0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 257
    return-void
.end method


# virtual methods
.method public getGUttID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->guttid:Ljava/lang/String;

    return-object v0
.end method

.method public getNBestForWord(Ljava/lang/String;)Ljava/util/Vector;
    .locals 1
    .param p1, "word"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getNBestForWord(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    return-object v0
.end method

.method public getNBestForWordIfBetterMatch(Ljava/util/List;Ljava/lang/String;[Ljava/lang/String;I[Ljava/lang/String;II)I
    .locals 8
    .param p2, "word"    # Ljava/lang/String;
    .param p3, "previousWords"    # [Ljava/lang/String;
    .param p4, "numPreviousWords"    # I
    .param p5, "nextWords"    # [Ljava/lang/String;
    .param p6, "numNextWords"    # I
    .param p7, "bestNumMatchingWordsSoFar"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "I[",
            "Ljava/lang/String;",
            "II)I"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getNBestForWordIfBetterMatch(Ljava/util/List;Ljava/lang/String;[Ljava/lang/String;I[Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public getNumParseGroups()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized getParseGroup()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    .locals 2

    .prologue
    .line 42
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    :cond_0
    const/4 v0, 0x0

    .line 44
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getParseGroups()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    return-object v0
.end method

.method public declared-synchronized getParseType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->getParseGroup()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->getParseGroup()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->getParseType()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 52
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string/jumbo v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getUttResults()Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isParseGroupsEmpty()Z
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onParseGroup(FLjava/lang/String;I)V
    .locals 2
    .param p1, "conf"    # F
    .param p2, "parseType"    # Ljava/lang/String;
    .param p3, "num"    # I

    .prologue
    .line 150
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;-><init>(FLjava/lang/String;ILcom/vlingo/sdk/internal/recognizer/results/TaggedResults$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->curParseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    .line 151
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->curParseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 152
    return-void
.end method

.method onTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 160
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;-><init>(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->curTag:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    .line 161
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->curParseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->access$200(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;)Ljava/util/Vector;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->curTag:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 162
    return-void
.end method

.method onTagList(I)V
    .locals 1
    .param p1, "num"    # I

    .prologue
    .line 140
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, p1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    .line 141
    return-void
.end method

.method onTagResults(Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)V
    .locals 1
    .param p1, "tagResults"    # Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->curTag:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->access$302(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 170
    return-void
.end method

.method onUttResults(Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)V
    .locals 0
    .param p1, "uttResults"    # Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 133
    return-void
.end method

.method public serialize()Ljava/lang/String;
    .locals 3

    .prologue
    .line 188
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    if-nez v1, :cond_0

    .line 189
    const-string/jumbo v1, ""

    .line 204
    :goto_0
    return-object v1

    .line 190
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 191
    .local v0, "sb":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "<Alternates guttid=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v2, v2, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->guttid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 194
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->serializeWordList(Ljava/lang/StringBuffer;Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)V

    .line 197
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->serializeUttList(Ljava/lang/StringBuffer;Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)V

    .line 200
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->serializeParseGroups(Ljava/lang/StringBuffer;Ljava/util/Vector;)V

    .line 203
    const-string/jumbo v1, "</Alternates>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 204
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 306
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "   TaggedResults: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->guttid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 307
    .local v2, "rs":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\n      UttResults: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 308
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\n         UttPhrase:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 309
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    if-eqz v5, :cond_0

    .line 310
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->uttResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getPhraseLevel()[Ljava/lang/String;

    move-result-object v4

    .line 311
    .local v4, "uttPhrase":[Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 312
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_1

    .line 313
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\n            "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v4, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 312
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317
    .end local v0    # "i":I
    .end local v4    # "uttPhrase":[Ljava/lang/String;
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 320
    :cond_1
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    if-eqz v5, :cond_2

    .line 321
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\n      ParseGroups "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 322
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v3

    .line 323
    .local v3, "size":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v3, :cond_3

    .line 324
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->groups:Ljava/util/Vector;

    invoke-virtual {v5, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    .line 325
    .local v1, "pg":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 323
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 328
    .end local v0    # "i":I
    .end local v1    # "pg":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    .end local v3    # "size":I
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 330
    :cond_3
    return-object v2
.end method
