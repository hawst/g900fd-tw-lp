.class public interface abstract Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
.super Ljava/lang/Object;
.source "DataReaderListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;
    }
.end annotation


# virtual methods
.method public abstract onDataAvailable([B[S)V
.end method

.method public abstract onError(Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;Ljava/lang/String;)V
.end method

.method public abstract onRMSDataAvailable(I)V
.end method

.method public abstract onStarted()V
.end method

.method public abstract onStopped(IZ)V
.end method
