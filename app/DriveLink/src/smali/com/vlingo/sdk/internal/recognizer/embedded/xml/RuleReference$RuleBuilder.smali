.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$RuleBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "RuleReference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RuleBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$1;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$RuleBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method protected addParametersNames(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "paramNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "templateName"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method protected buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;-><init>()V

    .line 32
    .local v0, "ruleReference":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$RuleBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "templateName"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;->templateName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;Ljava/lang/String;)Ljava/lang/String;

    .line 33
    return-object v0
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$RuleBuilder;->buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$RuleBuilder;->parametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 39
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string/jumbo v0, "Reference"

    return-object v0
.end method
