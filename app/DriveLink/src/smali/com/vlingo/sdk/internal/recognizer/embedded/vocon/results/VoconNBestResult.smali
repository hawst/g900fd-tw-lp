.class public final Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
.super Ljava/lang/Object;
.source "VoconNBestResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;
    }
.end annotation


# static fields
.field public static final HYPOTHESES:Ljava/lang/String; = "_hypotheses"

.field public static final IS_IN_GRAMMAR:Ljava/lang/String; = "_isInGrammar"

.field public static final IS_SPEECH:Ljava/lang/String; = "_isSpeech"

.field public static final RESULT_TYPE:Ljava/lang/String; = "_resultType"


# instance fields
.field private _hypotheses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;",
            ">;"
        }
    .end annotation
.end field

.field private _isInGrammar:Z

.field private _isSpeech:Z

.field private _resultType:Ljava/lang/String;

.field private mRawTagResult:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$1;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->mRawTagResult:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_resultType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_isSpeech:Z

    return p1
.end method

.method static synthetic access$402(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_isInGrammar:Z

    return p1
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_hypotheses:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_hypotheses:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public getBest()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .locals 2

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->getHypotheses()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 267
    const/4 v0, 0x0

    .line 270
    :goto_0
    return-object v0

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->getHypotheses()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 270
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->getHypotheses()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    goto :goto_0
.end method

.method public getHypotheses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_hypotheses:Ljava/util/List;

    return-object v0
.end method

.method public getRawTagResult()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->mRawTagResult:Ljava/lang/String;

    return-object v0
.end method

.method public getResultType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_resultType:Ljava/lang/String;

    return-object v0
.end method

.method public isInGrammar()Z
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_isInGrammar:Z

    return v0
.end method

.method public isSpeech()Z
    .locals 1

    .prologue
    .line 232
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_isSpeech:Z

    return v0
.end method
