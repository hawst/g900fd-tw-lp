.class abstract Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;
.super Ljava/lang/Object;
.source "VoconTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final callback:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final callerHandler:Landroid/os/Handler;

.field private final mainHandler:Landroid/os/Handler;


# direct methods
.method protected constructor <init>(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 1
    .param p2, "mainHandler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;, "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask<TT;>;"
    .local p1, "callback":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->callback:Ljava/lang/Object;

    .line 16
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->mainHandler:Landroid/os/Handler;

    .line 17
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->callerHandler:Landroid/os/Handler;

    .line 18
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;

    .prologue
    .line 6
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->callback:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected abstract execute()Z
.end method

.method public getCallback()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 64
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;, "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask<TT;>;"
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->callback:Ljava/lang/Object;

    return-object v0
.end method

.method protected final notifyOnCallerHandler(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 59
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;, "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask<TT;>;"
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->callerHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 60
    return-void
.end method

.method protected abstract onExecutionFailed(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method protected abstract onExecutionSuccess(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method protected onUnexpectedError(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 56
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;, "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask<TT;>;"
    return-void
.end method

.method public final run()V
    .locals 7

    .prologue
    .line 23
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;, "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask<TT;>;"
    const/4 v4, 0x0

    .line 25
    .local v4, "unexpectedException":Ljava/lang/Exception;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->execute()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 30
    .local v3, "success":Z
    :goto_0
    move v1, v3

    .line 31
    .local v1, "finalSuccess":Z
    move-object v2, v4

    .line 32
    .local v2, "finalUnexpectedException":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->mainHandler:Landroid/os/Handler;

    new-instance v6, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;

    invoke-direct {v6, p0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;ZLjava/lang/Exception;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 45
    return-void

    .line 26
    .end local v1    # "finalSuccess":Z
    .end local v2    # "finalUnexpectedException":Ljava/lang/Exception;
    .end local v3    # "success":Z
    :catch_0
    move-exception v0

    .line 27
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    .line 28
    .restart local v3    # "success":Z
    move-object v4, v0

    goto :goto_0
.end method
