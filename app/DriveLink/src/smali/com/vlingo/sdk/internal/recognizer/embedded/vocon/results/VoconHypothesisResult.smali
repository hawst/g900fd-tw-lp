.class public final Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
.super Ljava/lang/Object;
.source "VoconHypothesisResult.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;",
        ">;"
    }
.end annotation


# static fields
.field public static final BEGIN_TIME_MS:Ljava/lang/String; = "_beginTimeMs"

.field public static final CONFIDENCE:Ljava/lang/String; = "_conf"

.field public static final END_TIME_MS:Ljava/lang/String; = "_endTimeMs"

.field public static final ITEMS:Ljava/lang/String; = "_items"

.field public static final LM_SCORE:Ljava/lang/String; = "_lmScore"

.field public static final SCORE:Ljava/lang/String; = "_score"

.field public static final START_RULE:Ljava/lang/String; = "_startRule"


# instance fields
.field private _beginTimeMs:Ljava/lang/Long;

.field private _conf:Ljava/lang/Long;

.field private _endTimeMs:Ljava/lang/Long;

.field private _items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;",
            ">;"
        }
    .end annotation
.end field

.field private _lmScore:Ljava/lang/Long;

.field private _score:Ljava/lang/Long;

.field private _startRule:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$1;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_startRule:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .param p1, "x1"    # Ljava/lang/Long;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_conf:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .param p1, "x1"    # Ljava/lang/Long;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_score:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic access$402(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .param p1, "x1"    # Ljava/lang/Long;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_beginTimeMs:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic access$502(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .param p1, "x1"    # Ljava/lang/Long;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_endTimeMs:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic access$602(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .param p1, "x1"    # Ljava/lang/Long;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_lmScore:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_items:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_items:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public compareTo(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;)I
    .locals 2
    .param p1, "another"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_conf:Ljava/lang/Long;

    if-nez v0, :cond_2

    .line 289
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->getConfidence()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 291
    :goto_0
    return v0

    .line 289
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 291
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->getConfidence()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_conf:Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->getConfidence()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->compareTo(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;)I

    move-result v0

    return v0
.end method

.method public getAllTagNamesList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 434
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 435
    .local v2, "tagsNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;

    .line 436
    .local v1, "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->addTagNames(Ljava/util/List;)V

    goto :goto_0

    .line 438
    .end local v1    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    :cond_0
    return-object v2
.end method

.method public getBeginTimeMs()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_beginTimeMs:Ljava/lang/Long;

    return-object v0
.end method

.method public getConfidence()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_conf:Ljava/lang/Long;

    return-object v0
.end method

.method public getEndTimeMs()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_endTimeMs:Ljava/lang/Long;

    return-object v0
.end method

.method public getLmScore()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_lmScore:Ljava/lang/Long;

    return-object v0
.end method

.method public getRecognizedUtterance(Z)Ljava/lang/String;
    .locals 7
    .param p1, "forDisplay"    # Z

    .prologue
    const/4 v6, 0x1

    .line 306
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 307
    .local v2, "recognizedUtterance":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_items:Ljava/util/List;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 308
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;

    .line 309
    .local v1, "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 310
    const-string/jumbo v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    :cond_0
    invoke-virtual {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;->getRecognizedUtterance(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 317
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 318
    .local v3, "utt":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz p1, :cond_2

    .line 319
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 321
    :cond_2
    return-object v3
.end method

.method public getResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 401
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_items:Ljava/util/List;

    return-object v0
.end method

.method public getScore()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_score:Ljava/lang/Long;

    return-object v0
.end method

.method public getStartRule()Ljava/lang/String;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_startRule:Ljava/lang/String;

    return-object v0
.end method

.method public getTag(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    .locals 5
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 412
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->getResults()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;

    .line 413
    .local v1, "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    instance-of v4, v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    if-eqz v4, :cond_0

    move-object v3, v1

    .line 414
    check-cast v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    .line 415
    .local v3, "tagResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 425
    .end local v1    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    .end local v3    # "tagResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    :goto_0
    return-object v3

    .line 418
    .restart local v1    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    .restart local v3    # "tagResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    :cond_1
    invoke-virtual {v3, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getTag(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    move-result-object v2

    .line 419
    .local v2, "tag":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 420
    goto :goto_0

    .line 425
    .end local v1    # "node":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;
    .end local v2    # "tag":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    .end local v3    # "tagResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method
