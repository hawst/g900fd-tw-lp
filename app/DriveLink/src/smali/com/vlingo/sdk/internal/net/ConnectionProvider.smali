.class public abstract Lcom/vlingo/sdk/internal/net/ConnectionProvider;
.super Ljava/lang/Object;
.source "ConnectionProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getConnection(Ljava/lang/String;IZ)Lcom/vlingo/sdk/internal/net/Connection;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "timeouts"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2, p3}, Lcom/vlingo/sdk/internal/net/ConnectionProvider;->getConnectionWithDetails(Ljava/lang/String;IZ)Lcom/vlingo/sdk/internal/net/ConnectionResult;

    move-result-object v0

    .line 19
    .local v0, "result":Lcom/vlingo/sdk/internal/net/ConnectionResult;
    iget-object v1, v0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->connection:Lcom/vlingo/sdk/internal/net/Connection;

    return-object v1
.end method

.method public abstract getConnectionWithDetails(Ljava/lang/String;IZ)Lcom/vlingo/sdk/internal/net/ConnectionResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
