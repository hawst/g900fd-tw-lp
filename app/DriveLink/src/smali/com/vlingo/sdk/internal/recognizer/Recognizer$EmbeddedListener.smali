.class Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;
.super Ljava/lang/Object;
.source "Recognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/Recognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EmbeddedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V
    .locals 0

    .prologue
    .line 522
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;

    .prologue
    .line 522
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;-><init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V

    return-void
.end method


# virtual methods
.method public onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;)V
    .locals 2
    .param p1, "response"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

    .prologue
    .line 537
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->HYBRID:Lcom/vlingo/sdk/recognition/RecognitionMode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1100(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 538
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1100(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->notifyOfVoconResult(Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;)V

    .line 546
    :cond_0
    :goto_0
    return-void

    .line 539
    :cond_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->EMBEDDED:Lcom/vlingo/sdk/recognition/RecognitionMode;

    if-ne v0, v1, :cond_0

    .line 540
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1200(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->getResult()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 541
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->cleanup()V
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1300(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V

    goto :goto_0
.end method

.method public onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V
    .locals 2
    .param p1, "recError"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 525
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->HYBRID:Lcom/vlingo/sdk/recognition/RecognitionMode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1100(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 526
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1100(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->notifyOfVoconResult(Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;)V

    .line 533
    :cond_0
    :goto_0
    return-void

    .line 527
    :cond_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->EMBEDDED:Lcom/vlingo/sdk/recognition/RecognitionMode;

    if-ne v0, v1, :cond_0

    .line 528
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->EMBEDDED:Lcom/vlingo/sdk/recognition/RecognitionMode;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    invoke-static {v0, p1, p2, v1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    goto :goto_0
.end method
