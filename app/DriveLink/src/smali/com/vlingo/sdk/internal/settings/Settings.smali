.class public abstract Lcom/vlingo/sdk/internal/settings/Settings;
.super Ljava/lang/Object;
.source "Settings.java"


# static fields
.field public static final ACOUSTIC_RAW_ZIP_FILENAME:Ljava/lang/String; = "vlsdk_acoustic_raw.zip"

.field public static final ACOUSTIC_RAW_ZIP_FILENAME_S2:Ljava/lang/String; = "vlsdk_acoustic_raw_s2.0.zip"

.field public static final AUDIO_FILE_LOG_ENABLED:Z = false

.field public static final DEFAULT_LANGUAGE:Ljava/lang/String; = "en-US"

.field public static final DEFAULT_MAX_WIDTH:Ljava/lang/String; = "0"

.field public static final DEFAULT_PLOT_WIDTH:Ljava/lang/String; = "0"

.field public static final DETAILED_TIMINGS:Z = true

.field public static final KEY_ACCEPTEDTEXT_ENABLE:Ljava/lang/String; = "acceptedtext.enable"

.field public static final KEY_ACTIVITYLOG_ENABLE:Ljava/lang/String; = "activitylog.enable"

.field public static final KEY_ASR_CONNECT_TIMEOUT:Ljava/lang/String; = "asr.http.timeout.connect_ms"

.field public static final KEY_ASR_KEEPALIVE:Ljava/lang/String; = "asr.http.keep_alive"

.field public static final KEY_ASR_MANAGER:Ljava/lang/String; = "asr.manager"

.field public static final KEY_ASR_READ_TIMEOUT:Ljava/lang/String; = "asr.http.timeout.read_ms"

.field public static final KEY_COOKIE_DATA:Ljava/lang/String; = "cookie_data"

.field public static final KEY_HELLO_ENABLE:Ljava/lang/String; = "hello.enable"

.field private static final KEY_ISTLSV1DOT2ENABLED:Ljava/lang/String; = "vlsdk.https.tlsv1.2.enabled"

.field public static final KEY_MAX_WIDTH:Ljava/lang/String; = "max.width"

.field public static final KEY_OBEY_DEVICE_LOCATION_SETTINGS:Ljava/lang/String; = "obey_device_location_settings"

.field public static final KEY_PLOT_WIDTH:Ljava/lang/String; = "plot.width"

.field public static final KEY_SCREEN_MAG:Ljava/lang/String; = "screen.mag"

.field public static final KEY_SCREEN_WIDTH:Ljava/lang/String; = "screen.width"

.field public static final KEY_SPEAKERID:Ljava/lang/String; = "speaker_id"

.field public static final KEY_STAT_ENABLE:Ljava/lang/String; = "stats.enable"

.field public static final KEY_TOS_ACCEPTED_DATE:Ljava/lang/String; = "tos_accepted_date"

.field public static final KEY_USE_EDM:Ljava/lang/String; = "USE_EDM"

.field public static final KEY_UUID:Ljava/lang/String; = "uuid"

.field public static final KEY_ZIP_SIZE_PREFIX:Ljava/lang/String; = "zip_timestamp_"

.field public static LANGUAGE:Ljava/lang/String; = null

.field public static final LIB_PATH:Ljava/lang/String; = "vlsdk_lib"

.field public static final LIB_ZIP_FILENAME:Ljava/lang/String; = "vlsdk_lib.zip"

.field public static final LIB_ZIP_FILENAME_S2:Ljava/lang/String; = "vlsdk_lib_s2.0.zip"

.field public static final LTS_RAW_ZIP_FILENAME:Ljava/lang/String; = "vlsdk_lts_raw.zip"

.field public static final RAW_PATH:Ljava/lang/String; = "vlsdk_raw"

.field public static final SERVERLOG_ENABLED:Z

.field public static SUPPORTED_LANGUAGES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 77
    const-string/jumbo v0, "en-US"

    sput-object v0, Lcom/vlingo/sdk/internal/settings/Settings;->LANGUAGE:Ljava/lang/String;

    .line 78
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "en-US"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "en-GB"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "fr-FR"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "it-IT"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "de-DE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "es-ES"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "v-es-LA"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "v-es-NA"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "zh-CN"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "ja-JP"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "ko-KR"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/sdk/internal/settings/Settings;->SUPPORTED_LANGUAGES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V
    .locals 2
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 95
    const-class v0, Lcom/vlingo/sdk/internal/settings/Settings;

    monitor-enter v0

    :try_start_0
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    monitor-exit v0

    return-void

    .line 95
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getAsrConnectTimeout()I
    .locals 2

    .prologue
    .line 153
    const-string/jumbo v0, "asr.http.timeout.connect_ms"

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getAsrReadTimeout()I
    .locals 2

    .prologue
    .line 157
    const-string/jumbo v0, "asr.http.timeout.read_ms"

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getPersistentBoolean(Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 139
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getPersistentFloat(Ljava/lang/String;F)F
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # F

    .prologue
    .line 115
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static getPersistentInt(Ljava/lang/String;I)I
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 103
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getPersistentLong(Ljava/lang/String;J)J
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # J

    .prologue
    .line 127
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getPersistentString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 143
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 144
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    return-object v1
.end method

.method public static isAsrKeepAliveEnabled()Z
    .locals 2

    .prologue
    .line 149
    const-string/jumbo v0, "asr.http.keep_alive"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isTLSv1Dot2Enabled()Z
    .locals 2

    .prologue
    .line 165
    const-string/jumbo v0, "vlsdk.https.tlsv1.2.enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static setLanguage(Ljava/lang/String;)V
    .locals 0
    .param p0, "language"    # Ljava/lang/String;

    .prologue
    .line 161
    sput-object p0, Lcom/vlingo/sdk/internal/settings/Settings;->LANGUAGE:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public static setPersistentInt(Ljava/lang/String;I)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # I

    .prologue
    .line 107
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 108
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 109
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 111
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 112
    return-void
.end method

.method public static setPersistentLong(Ljava/lang/String;J)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # J

    .prologue
    .line 119
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 120
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 121
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 123
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 124
    return-void
.end method

.method public static setPersistentString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 132
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 133
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 135
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 136
    return-void
.end method

.method public static declared-synchronized startBatchEdit()Landroid/content/SharedPreferences$Editor;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 90
    const-class v1, Lcom/vlingo/sdk/internal/settings/Settings;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
