.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder$1;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder;->addChildrenBuilders(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder;)V
    .locals 1

    .prologue
    .line 149
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$1;)V

    return-void
.end method


# virtual methods
.method protected addChildrenBuilders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "builders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;>;"
    const-string/jumbo v0, "Param"

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    return-void
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder;->buildResultObject()Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
