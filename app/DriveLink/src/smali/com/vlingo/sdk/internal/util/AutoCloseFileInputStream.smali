.class public Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;
.super Ljava/io/FileInputStream;
.source "AutoCloseFileInputStream.java"


# instance fields
.field private firstRead:Z

.field private isClosed:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 12
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->isClosed:Z

    .line 13
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->firstRead:Z

    .line 17
    return-void
.end method

.method private waitFirst()V
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->firstRead:Z

    if-nez v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->waitToLoad()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->firstRead:Z

    .line 58
    :cond_0
    return-void
.end method

.method private waitToLoad()V
    .locals 3

    .prologue
    .line 62
    const-wide/16 v1, 0x12c

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->isClosed:Z

    if-eqz v0, :cond_0

    .line 47
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-super {p0}, Ljava/io/FileInputStream;->close()V

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->isClosed:Z

    goto :goto_0
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->waitFirst()V

    .line 22
    invoke-super {p0}, Ljava/io/FileInputStream;->read()I

    move-result v0

    .line 23
    .local v0, "byteRead":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->close()V

    .line 26
    :cond_0
    return v0
.end method

.method public read([BII)I
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "byteOffset"    # I
    .param p3, "byteCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->waitFirst()V

    .line 32
    invoke-super {p0, p1, p2, p3}, Ljava/io/FileInputStream;->read([BII)I

    move-result v0

    .line 33
    .local v0, "bytesRead":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;->close()V

    .line 36
    :cond_0
    return v0
.end method
