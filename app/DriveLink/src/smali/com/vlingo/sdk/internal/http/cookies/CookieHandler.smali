.class public Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;
.super Ljava/lang/Object;
.source "CookieHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;,
        Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionAdapterWrapper;,
        Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;,
        Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    return-void
.end method

.method private static extractCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 8
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;
    .param p1, "cookieJar"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 236
    invoke-interface {p0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "domain":Ljava/lang/String;
    const/4 v3, 0x0

    .line 240
    .local v3, "key":Ljava/lang/String;
    const/4 v2, 0x0

    .line 242
    .local v2, "i":I
    :goto_0
    :try_start_0
    invoke-interface {p0, v2}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 245
    const-string/jumbo v5, "Set-Cookie"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 248
    invoke-interface {p0, v2}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;->getHeaderField(I)Ljava/lang/String;

    move-result-object v4

    .line 251
    .local v4, "value":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 252
    if-nez p1, :cond_0

    .line 253
    invoke-static {}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarFactory;->newInstance()Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object p1

    .line 255
    :cond_0
    invoke-static {v4, v0, p1}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->parseSetCookieString(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    .end local v4    # "value":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 260
    :catch_0
    move-exception v1

    .line 261
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v5, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "COK1 Exception while extracting cookies: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_2
    return-object p1
.end method

.method public static extractCookies(Lcom/vlingo/sdk/internal/net/HttpConnection;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/net/HttpConnection;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/net/HttpConnection;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static extractCookies(Lcom/vlingo/sdk/internal/net/HttpConnection;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/net/HttpConnection;
    .param p1, "cookieJar"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 59
    new-instance v0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;-><init>(Lcom/vlingo/sdk/internal/net/HttpConnection;)V

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static extractCookies(Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static extractCookies(Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;
    .param p1, "cookieJar"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 63
    new-instance v0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionAdapterWrapper;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionAdapterWrapper;-><init>(Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;)V

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static extractCookies(Ljava/net/HttpURLConnection;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Ljava/net/HttpURLConnection;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static extractCookies(Ljava/net/HttpURLConnection;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Ljava/net/HttpURLConnection;
    .param p1, "cookieJar"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 67
    new-instance v0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;-><init>(Ljava/net/HttpURLConnection;)V

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static parseSetCookieString(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V
    .locals 25
    .param p0, "cookieString"    # Ljava/lang/String;
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "cookieJar"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 275
    const/16 v21, 0x2c

    :try_start_0
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v7

    .line 276
    .local v7, "cookies":[Ljava/lang/String;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    array-length v0, v7

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v13, v0, :cond_8

    .line 277
    aget-object v6, v7, v13

    .line 279
    .local v6, "cookie":Ljava/lang/String;
    const-string/jumbo v21, "Expires="

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    const-string/jumbo v21, "expires="

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_1

    .line 281
    :cond_0
    add-int/lit8 v13, v13, 0x1

    .line 282
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, ","

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    aget-object v22, v7, v13

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 285
    :cond_1
    const/16 v21, 0x3d

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    .line 286
    .local v9, "eqIndex":I
    const/4 v5, -0x1

    .line 287
    .local v5, "colonIndex":I
    const/4 v3, 0x0

    .line 288
    .local v3, "c":Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v9, v0, :cond_3

    .line 289
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v6, v0, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 290
    .local v4, "cname":Ljava/lang/String;
    const-string/jumbo v8, ""

    .line 291
    .local v8, "cvalue":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v21

    add-int/lit8 v22, v9, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_2

    .line 292
    const/16 v21, 0x3b

    add-int/lit8 v22, v9, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v5

    .line 293
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v5, v0, :cond_6

    .line 294
    add-int/lit8 v21, v9, 0x1

    move/from16 v0, v21

    invoke-virtual {v6, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 300
    :cond_2
    :goto_1
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/vlingo/sdk/internal/http/cookies/CookieFactory;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/cookies/Cookie;

    move-result-object v3

    .line 301
    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setDomain(Ljava/lang/String;)V

    .line 302
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/vlingo/sdk/internal/http/cookies/CookieJar;->addCookie(Lcom/vlingo/sdk/internal/http/cookies/Cookie;)V

    .line 308
    .end local v4    # "cname":Ljava/lang/String;
    .end local v8    # "cvalue":Ljava/lang/String;
    :cond_3
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v5, v0, :cond_e

    .line 309
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v21

    add-int/lit8 v22, v5, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_e

    .line 310
    add-int/lit8 v21, v5, 0x1

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 311
    .local v16, "paramsStr":Ljava/lang/String;
    const/16 v21, 0x3b

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v15

    .line 312
    .local v15, "params":[Ljava/lang/String;
    const/4 v14, 0x0

    .local v14, "j":I
    :goto_2
    array-length v0, v15

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v14, v0, :cond_e

    .line 313
    aget-object v21, v15, v14

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    .line 314
    .local v19, "pv":Ljava/lang/String;
    const-string/jumbo v21, "="

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v17

    .line 315
    .local v17, "peqIndex":I
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    .line 316
    .local v18, "pname":Ljava/lang/String;
    add-int/lit8 v21, v17, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    .line 318
    .local v20, "pvalue":Ljava/lang/String;
    const-string/jumbo v21, "\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    const-string/jumbo v21, "\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 319
    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 322
    :cond_4
    if-nez v3, :cond_7

    .line 312
    :cond_5
    :goto_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 297
    .end local v14    # "j":I
    .end local v15    # "params":[Ljava/lang/String;
    .end local v16    # "paramsStr":Ljava/lang/String;
    .end local v17    # "peqIndex":I
    .end local v18    # "pname":Ljava/lang/String;
    .end local v19    # "pv":Ljava/lang/String;
    .end local v20    # "pvalue":Ljava/lang/String;
    .restart local v4    # "cname":Ljava/lang/String;
    .restart local v8    # "cvalue":Ljava/lang/String;
    :cond_6
    add-int/lit8 v21, v9, 0x1

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 325
    .end local v4    # "cname":Ljava/lang/String;
    .end local v8    # "cvalue":Ljava/lang/String;
    .restart local v14    # "j":I
    .restart local v15    # "params":[Ljava/lang/String;
    .restart local v16    # "paramsStr":Ljava/lang/String;
    .restart local v17    # "peqIndex":I
    .restart local v18    # "pname":Ljava/lang/String;
    .restart local v19    # "pv":Ljava/lang/String;
    .restart local v20    # "pvalue":Ljava/lang/String;
    :cond_7
    const-string/jumbo v21, "Domain"

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 326
    move-object/from16 v0, v20

    invoke-interface {v3, v0}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setDomain(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 356
    .end local v3    # "c":Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    .end local v5    # "colonIndex":I
    .end local v6    # "cookie":Ljava/lang/String;
    .end local v7    # "cookies":[Ljava/lang/String;
    .end local v9    # "eqIndex":I
    .end local v13    # "i":I
    .end local v14    # "j":I
    .end local v15    # "params":[Ljava/lang/String;
    .end local v16    # "paramsStr":Ljava/lang/String;
    .end local v17    # "peqIndex":I
    .end local v18    # "pname":Ljava/lang/String;
    .end local v19    # "pv":Ljava/lang/String;
    .end local v20    # "pvalue":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 358
    .local v10, "ex":Ljava/lang/Exception;
    sget-object v21, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "COK2 Error parsing cookie: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    sget-object v21, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "Cookie string: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    sget-object v21, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "Domain: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 363
    .end local v10    # "ex":Ljava/lang/Exception;
    :cond_8
    return-void

    .line 328
    .restart local v3    # "c":Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    .restart local v5    # "colonIndex":I
    .restart local v6    # "cookie":Ljava/lang/String;
    .restart local v7    # "cookies":[Ljava/lang/String;
    .restart local v9    # "eqIndex":I
    .restart local v13    # "i":I
    .restart local v14    # "j":I
    .restart local v15    # "params":[Ljava/lang/String;
    .restart local v16    # "paramsStr":Ljava/lang/String;
    .restart local v17    # "peqIndex":I
    .restart local v18    # "pname":Ljava/lang/String;
    .restart local v19    # "pv":Ljava/lang/String;
    .restart local v20    # "pvalue":Ljava/lang/String;
    :cond_9
    :try_start_1
    const-string/jumbo v21, "Path"

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 329
    move-object/from16 v0, v20

    invoke-interface {v3, v0}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setPath(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 331
    :cond_a
    const-string/jumbo v21, "Expires"

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v21

    if-eqz v21, :cond_c

    .line 332
    const-wide/16 v11, 0x0

    .line 333
    .local v11, "expires":J
    :try_start_2
    invoke-static/range {v20 .. v20}, Lcom/vlingo/sdk/internal/http/date/HttpDateParser;->parse(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-wide v11

    .line 334
    :goto_4
    const-wide/16 v21, 0x0

    cmp-long v21, v11, v21

    if-lez v21, :cond_5

    .line 335
    :try_start_3
    invoke-interface {v3}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getExpires()J

    move-result-wide v21

    const-wide/16 v23, 0x0

    cmp-long v21, v21, v23

    if-eqz v21, :cond_b

    invoke-interface {v3}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getExpires()J

    move-result-wide v21

    cmp-long v21, v11, v21

    if-gez v21, :cond_5

    .line 336
    :cond_b
    invoke-interface {v3, v11, v12}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setExpires(J)V

    goto/16 :goto_3

    .line 339
    .end local v11    # "expires":J
    :cond_c
    const-string/jumbo v21, "Max-Age"

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result v21

    if-eqz v21, :cond_5

    .line 340
    const-wide/16 v11, 0x0

    .line 341
    .restart local v11    # "expires":J
    :try_start_4
    invoke-static/range {v20 .. v20}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-wide v21

    const-wide/16 v23, 0x3e8

    mul-long v11, v21, v23

    .line 342
    :goto_5
    const-wide/16 v21, 0x0

    cmp-long v21, v11, v21

    if-lez v21, :cond_5

    .line 343
    :try_start_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    add-long v11, v11, v21

    .line 344
    invoke-interface {v3}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getExpires()J

    move-result-wide v21

    const-wide/16 v23, 0x0

    cmp-long v21, v21, v23

    if-eqz v21, :cond_d

    invoke-interface {v3}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getExpires()J

    move-result-wide v21

    cmp-long v21, v11, v21

    if-gez v21, :cond_5

    .line 345
    :cond_d
    invoke-interface {v3, v11, v12}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setExpires(J)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_3

    .line 276
    .end local v11    # "expires":J
    .end local v14    # "j":I
    .end local v15    # "params":[Ljava/lang/String;
    .end local v16    # "paramsStr":Ljava/lang/String;
    .end local v17    # "peqIndex":I
    .end local v18    # "pname":Ljava/lang/String;
    .end local v19    # "pv":Ljava/lang/String;
    .end local v20    # "pvalue":Ljava/lang/String;
    :cond_e
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_0

    .line 333
    .restart local v11    # "expires":J
    .restart local v14    # "j":I
    .restart local v15    # "params":[Ljava/lang/String;
    .restart local v16    # "paramsStr":Ljava/lang/String;
    .restart local v17    # "peqIndex":I
    .restart local v18    # "pname":Ljava/lang/String;
    .restart local v19    # "pv":Ljava/lang/String;
    .restart local v20    # "pvalue":Ljava/lang/String;
    :catch_1
    move-exception v21

    goto :goto_4

    .line 341
    :catch_2
    move-exception v21

    goto :goto_5
.end method
