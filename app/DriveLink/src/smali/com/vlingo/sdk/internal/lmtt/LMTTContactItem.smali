.class public Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;
.super Lcom/vlingo/sdk/internal/lmtt/LMTTItem;
.source "LMTTContactItem.java"


# instance fields
.field public companyName:Ljava/lang/String;

.field public firstName:Ljava/lang/String;

.field public fullName:Ljava/lang/String;

.field public lastName:Ljava/lang/String;

.field public middleName:Ljava/lang/String;

.field public nickname:Ljava/lang/String;

.field public phoneticFirstName:Ljava/lang/String;

.field public phoneticLastName:Ljava/lang/String;

.field public phoneticMiddleName:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 13
    .param p1, "uid"    # J
    .param p3, "changeType"    # Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 50
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-wide v10, p1

    move-object/from16 v12, p3

    invoke-direct/range {v0 .. v12}, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 1
    .param p1, "firstName"    # Ljava/lang/String;
    .param p2, "middleName"    # Ljava/lang/String;
    .param p3, "lastName"    # Ljava/lang/String;
    .param p4, "nickname"    # Ljava/lang/String;
    .param p5, "fullName"    # Ljava/lang/String;
    .param p6, "companyName"    # Ljava/lang/String;
    .param p7, "phoneticFirstName"    # Ljava/lang/String;
    .param p8, "phoneticMiddleName"    # Ljava/lang/String;
    .param p9, "phoneticLastName"    # Ljava/lang/String;
    .param p10, "uid"    # J
    .param p12, "changeType"    # Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 31
    sget-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_CONTACT:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    invoke-direct {p0, v0, p10, p11, p12}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;-><init>(Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 33
    if-nez p1, :cond_0

    const-string/jumbo p1, ""

    .line 34
    :cond_0
    if-nez p3, :cond_1

    const-string/jumbo p3, ""

    .line 35
    :cond_1
    if-nez p6, :cond_2

    const-string/jumbo p6, ""

    .line 37
    :cond_2
    iput-object p1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->firstName:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->middleName:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->lastName:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->nickname:Ljava/lang/String;

    .line 41
    iput-object p5, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->fullName:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->lastName:Ljava/lang/String;

    .line 43
    iput-object p6, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->companyName:Ljava/lang/String;

    .line 44
    iput-object p7, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->phoneticFirstName:Ljava/lang/String;

    .line 45
    iput-object p8, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->phoneticMiddleName:Ljava/lang/String;

    .line 46
    iput-object p9, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->phoneticLastName:Ljava/lang/String;

    .line 47
    return-void
.end method


# virtual methods
.method public getDelXML(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 54
    const-string/jumbo v0, "<e uid=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->uid:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 56
    const-string/jumbo v0, "\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    const-string/jumbo v0, " t=\"d\">"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    const-string/jumbo v0, "</e>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    return-void
.end method

.method public getInsXML(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 63
    const-string/jumbo v0, "<e uid=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->uid:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 65
    const-string/jumbo v0, "\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    const-string/jumbo v0, "><fn>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->firstName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 68
    const-string/jumbo v0, "</fn><mn>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->middleName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 70
    const-string/jumbo v0, "</mn><ln>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->lastName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 72
    const-string/jumbo v0, "</ln><nn>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->nickname:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 74
    const-string/jumbo v0, "</nn><fln>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->fullName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 76
    const-string/jumbo v0, "</fln><pfn>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->phoneticFirstName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 78
    const-string/jumbo v0, "</pfn><pmn>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->phoneticMiddleName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 80
    const-string/jumbo v0, "</pmn><pln>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->phoneticLastName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 82
    const-string/jumbo v0, "</pln><c>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->companyName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 84
    const-string/jumbo v0, "</c>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    const-string/jumbo v0, "</e>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    return-void
.end method

.method public getUpXML(Ljava/lang/StringBuilder;)V
    .locals 0
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->getInsXML(Ljava/lang/StringBuilder;)V

    .line 91
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "LMTTContact: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->firstName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->lastName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->companyName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->uid:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " changeType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;->changeType:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
