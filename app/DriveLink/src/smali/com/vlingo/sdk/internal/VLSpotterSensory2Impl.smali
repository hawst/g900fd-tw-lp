.class public final Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;
.super Lcom/vlingo/sdk/internal/VLSpotterImpl;
.source "VLSpotterSensory2Impl.java"


# instance fields
.field private mIsBluetoothOn:Z

.field private mIsCarMode:Z

.field private mSensoryCtx:J


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 0
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLSpotterImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 27
    return-void
.end method

.method private getSVoiceAppDirectory()Ljava/lang/String;
    .locals 5

    .prologue
    .line 144
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 145
    .local v0, "ctx":Landroid/content/Context;
    const-string/jumbo v3, "vlsdk_raw"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 146
    .local v1, "rawDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "toReturn":Ljava/lang/String;
    return-object v2
.end method

.method private sensoryFileExists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "fullPathname"    # Ljava/lang/String;

    .prologue
    .line 135
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 136
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 139
    .local v1, "toReturn":Z
    return v1
.end method


# virtual methods
.method protected getAcousticModelFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "audio"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 106
    .local v0, "am":Landroid/media/AudioManager;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v4, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v6}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->sensoryFileExists(Ljava/lang/String;)Z

    move-result v2

    .line 107
    .local v2, "mediaFileExist":Z
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v1

    .line 108
    .local v1, "headsetConnected":Z
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsCarMode:Z

    if-eqz v4, :cond_0

    if-nez v1, :cond_1

    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsBluetoothOn:Z

    if-nez v4, :cond_1

    .line 109
    :cond_0
    sget-object v4, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 115
    .local v3, "toReturn":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 111
    .end local v3    # "toReturn":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .restart local v3    # "toReturn":Ljava/lang/String;
    goto :goto_0
.end method

.method protected getDirectory(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "externalDir"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->getSVoiceAppDirectory()Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "toReturn":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 94
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->getAcousticModelFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "sampleSystemPathName":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->sensoryFileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    move-object v1, p1

    .line 99
    .end local v0    # "sampleSystemPathName":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method protected getGrammarModelFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "audio"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 122
    .local v0, "am":Landroid/media/AudioManager;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v4, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v6}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->sensoryFileExists(Ljava/lang/String;)Z

    move-result v2

    .line 123
    .local v2, "mediaFileExist":Z
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v1

    .line 124
    .local v1, "headsetConnected":Z
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsCarMode:Z

    if-eqz v4, :cond_0

    if-nez v1, :cond_1

    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsBluetoothOn:Z

    if-nez v4, :cond_1

    .line 125
    :cond_0
    sget-object v4, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 131
    .local v3, "toReturn":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 127
    .end local v3    # "toReturn":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .restart local v3    # "toReturn":Ljava/lang/String;
    goto :goto_0
.end method

.method declared-synchronized onDestroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 37
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    iget-wide v1, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->phrasespotClose(J)V

    .line 40
    :cond_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsStarted:Z

    if-eqz v0, :cond_1

    .line 41
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->MakeReady()Z

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsStarted:Z

    .line 44
    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    monitor-exit p0

    return-void

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized phrasespotPipe(Ljava/nio/ByteBuffer;J)Ljava/lang/String;
    .locals 8
    .param p1, "b"    # Ljava/nio/ByteBuffer;
    .param p2, "rate"    # J

    .prologue
    const-wide/16 v2, 0x0

    const/4 v7, 0x0

    .line 154
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->validateInstance()V

    .line 155
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    if-eqz v0, :cond_4

    .line 156
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsStarted:Z

    if-nez v0, :cond_1

    .line 157
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 160
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    iget-wide v1, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J

    move-object v3, p1

    move-wide v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->phrasespotPipe(JLjava/nio/ByteBuffer;J)Ljava/lang/String;

    move-result-object v6

    .line 161
    .local v6, "ps":Ljava/lang/String;
    if-nez v6, :cond_2

    .line 162
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsStarted:Z

    .line 164
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 167
    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-lez v0, :cond_3

    .line 170
    .end local v6    # "ps":Ljava/lang/String;
    :goto_0
    monitor-exit p0

    return-object v6

    .restart local v6    # "ps":Ljava/lang/String;
    :cond_3
    move-object v6, v7

    .line 167
    goto :goto_0

    .end local v6    # "ps":Ljava/lang/String;
    :cond_4
    move-object v6, v7

    .line 170
    goto :goto_0
.end method

.method reinitSpotter(Ljava/lang/String;)Z
    .locals 8
    .param p1, "externalDir"    # Ljava/lang/String;

    .prologue
    const-wide/16 v6, 0x0

    .line 51
    iget-wide v3, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J

    cmp-long v3, v3, v6

    if-eqz v3, :cond_0

    .line 52
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    iget-wide v4, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J

    invoke-virtual {v3, v4, v5}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->phrasespotClose(J)V

    .line 53
    iput-wide v6, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J

    .line 55
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->getDirectory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, "appDir":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->getAcousticModelFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "acousticModelFilename":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->getGrammarModelFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 60
    .local v2, "grammarModelFilename":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    invoke-virtual {v3, v1, v0, v2}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->phrasespotInit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J

    .line 61
    iget-wide v3, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSensoryCtx:J

    cmp-long v3, v3, v6

    if-nez v3, :cond_1

    .line 62
    const-string/jumbo v3, "VLG_EXCEPTION"

    const-string/jumbo v4, "[sensory2.0] mSensoryCtx = 0 IN reinitSpotter"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    const/4 v3, 0x0

    .line 68
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public startSpotter(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;Ljava/lang/String;ZZ)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;
    .param p2, "externalDir"    # Ljava/lang/String;
    .param p3, "isCarMode"    # Z
    .param p4, "isBluetoothOn"    # Z

    .prologue
    .line 75
    iput-boolean p3, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsCarMode:Z

    .line 76
    iput-boolean p4, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsBluetoothOn:Z

    .line 80
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->checkContext(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;)V

    .line 82
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    if-eq v0, p1, :cond_0

    .line 83
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    .line 85
    :cond_0
    invoke-virtual {p0, p2}, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->reinitSpotter(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;->mIsStarted:Z

    .line 86
    const/4 v0, 0x1

    return v0
.end method
