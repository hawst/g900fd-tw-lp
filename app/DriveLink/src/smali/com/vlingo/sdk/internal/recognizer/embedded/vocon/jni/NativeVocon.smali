.class public interface abstract Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;
.super Ljava/lang/Object;
.source "NativeVocon.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException;
    }
.end annotation


# virtual methods
.method public abstract activateAllGrammarRules()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract activateGrammarRule(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract addTerminalToVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract addTerminalsToVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract cancelRecognition()V
.end method

.method public abstract clearMergedGrammars()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract clearTerminalsFromVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract commitVocabularies()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract commitVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract deactivateAllGrammarRules()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract deactivateGrammarRule(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract getGrammarRules()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract getMergedGrammars()[Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract init(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract isGrammarLoaded()Z
.end method

.method public abstract isInit()Z
.end method

.method public abstract isRecognizing()Z
.end method

.method public abstract loadGrammar(Ljava/lang/String;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract mergeGrammar(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract pushAudioFrame([S)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException;
        }
    .end annotation
.end method

.method public abstract removeMergedGrammar(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract removeTerminalFromVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract removeTerminalsFromVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract resetTimeoutAfterSpeech()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract resetTimeoutBeforeSpeech()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract setTimeoutAfterSpeech(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract setTimeoutBeforeSpeech(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract startRecognition()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract uninit()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract unloadGrammar()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method
