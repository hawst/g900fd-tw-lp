.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;
.super Ljava/lang/Object;
.source "VoconNBestResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private nBestJson:Lorg/json/JSONObject;

.field private nBestResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "rawTagResult"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    .line 79
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->mRawTagResult:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Ljava/lang/String;)Ljava/lang/String;

    .line 81
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestJson:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private addHypothese(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;
    .locals 2
    .param p1, "hypothesis"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_hypotheses:Ljava/util/List;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->access$500(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_hypotheses:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->access$502(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Ljava/util/List;)Ljava/util/List;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_hypotheses:Ljava/util/List;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->access$500(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    return-object p0
.end method

.method private isInGrammar(Z)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;
    .locals 1
    .param p1, "isInGrammar"    # Z

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_isInGrammar:Z
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->access$402(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Z)Z

    .line 123
    return-object p0
.end method

.method private isSpeech(Z)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;
    .locals 1
    .param p1, "isSpeech"    # Z

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_isSpeech:Z
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Z)Z

    .line 110
    return-object p0
.end method

.method private resultType(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;
    .locals 1
    .param p1, "resultType"    # Ljava/lang/String;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->_resultType:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Ljava/lang/String;)Ljava/lang/String;

    .line 98
    return-object p0
.end method


# virtual methods
.method public build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 151
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestJson:Lorg/json/JSONObject;

    if-eqz v6, :cond_1

    .line 154
    :try_start_0
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestJson:Lorg/json/JSONObject;

    const-string/jumbo v7, "_resultType"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->resultType(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :try_start_1
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestJson:Lorg/json/JSONObject;

    const-string/jumbo v7, "_isSpeech"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "yes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    invoke-direct {p0, v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->isSpeech(Z)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 170
    :try_start_2
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestJson:Lorg/json/JSONObject;

    const-string/jumbo v7, "_isInGrammar"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "yes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    invoke-direct {p0, v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->isInGrammar(Z)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 178
    :try_start_3
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestJson:Lorg/json/JSONObject;

    const-string/jumbo v7, "_hypotheses"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 180
    .local v1, "hypotheses":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "hypothesisPosn":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v4, v6, :cond_2

    .line 181
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 182
    .local v3, "hypothesisJson":Lorg/json/JSONObject;
    new-instance v6, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;

    invoke-direct {v6, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    move-result-object v2

    .line 184
    .local v2, "hypothesis":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    if-eqz v2, :cond_0

    .line 185
    invoke-direct {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->addHypothese(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    .line 180
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 155
    .end local v1    # "hypotheses":Lorg/json/JSONArray;
    .end local v2    # "hypothesis":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .end local v3    # "hypothesisJson":Lorg/json/JSONObject;
    .end local v4    # "hypothesisPosn":I
    :catch_0
    move-exception v0

    .line 196
    :cond_1
    :goto_1
    return-object v5

    .line 163
    :catch_1
    move-exception v0

    .line 166
    .local v0, "e":Lorg/json/JSONException;
    goto :goto_1

    .line 171
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v0

    .line 174
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 188
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_3
    move-exception v0

    .line 191
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 194
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "hypotheses":Lorg/json/JSONArray;
    .restart local v4    # "hypothesisPosn":I
    :cond_2
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->nBestResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    goto :goto_1
.end method
