.class public Lcom/vlingo/sdk/internal/http/HttpResponse;
.super Ljava/lang/Object;
.source "HttpResponse.java"


# instance fields
.field cookies:Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

.field private final data:[B

.field public final responseCode:I


# direct methods
.method public constructor <init>(I[B)V
    .locals 1
    .param p1, "responseCode"    # I
    .param p2, "data"    # [B

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/sdk/internal/http/HttpResponse;-><init>(I[BLcom/vlingo/sdk/internal/http/cookies/CookieJar;)V

    .line 31
    return-void
.end method

.method public constructor <init>(I[BLcom/vlingo/sdk/internal/http/cookies/CookieJar;)V
    .locals 0
    .param p1, "responseCode"    # I
    .param p2, "data"    # [B
    .param p3, "cookies"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    .line 35
    iput-object p2, p0, Lcom/vlingo/sdk/internal/http/HttpResponse;->data:[B

    .line 36
    iput-object p3, p0, Lcom/vlingo/sdk/internal/http/HttpResponse;->cookies:Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .line 37
    return-void
.end method


# virtual methods
.method public getCookies()Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpResponse;->cookies:Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    return-object v0
.end method

.method public getDataAsBytes()[B
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpResponse;->data:[B

    return-object v0
.end method

.method public getDataAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string/jumbo v0, "UTF-8"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDataAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "encoding"    # Ljava/lang/String;

    .prologue
    .line 63
    const-string/jumbo v0, ""

    .line 65
    .local v0, "result":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpResponse;->data:[B

    if-eqz v2, :cond_0

    .line 66
    new-instance v0, Ljava/lang/String;

    .end local v0    # "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpResponse;->data:[B

    invoke-direct {v0, v2, p1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .restart local v0    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 69
    .end local v0    # "result":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 71
    .local v1, "uee":Ljava/io/UnsupportedEncodingException;
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpResponse;->data:[B

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_0
.end method

.method public getDataLength()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpResponse;->data:[B

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpResponse;->data:[B

    array-length v0, v0

    .line 48
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
