.class public abstract Lcom/vlingo/sdk/internal/VLSpotterImpl;
.super Lcom/vlingo/sdk/internal/VLComponentImpl;
.source "VLSpotterImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/spotter/VLSpotter;


# instance fields
.field protected mIsStarted:Z

.field protected mSensoryCtx:J

.field protected mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

.field protected mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 1
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLComponentImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 34
    invoke-static {}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->init()V

    .line 36
    new-instance v0, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterImpl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    .line 37
    return-void
.end method


# virtual methods
.method protected checkContext(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLSpotterImpl;->validateInstance()V

    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "context must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpotterImpl;->mIsStarted:Z

    if-eqz v0, :cond_1

    .line 47
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Spotter already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_1
    return-void
.end method

.method public bridge synthetic destroy()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->destroy()V

    return-void
.end method

.method public doesDependOnSettings()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized getLastScore()F
    .locals 2

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLSpotterImpl;->validateInstance()V

    .line 86
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpotterImpl;->mIsStarted:Z

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot call getLastScore when Spotter is not started."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 90
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterImpl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->GetLastScore()F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized getSupportedLanguageList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLSpotterImpl;->validateInstance()V

    .line 96
    sget-object v0, Lcom/vlingo/sdk/internal/settings/Settings;->SUPPORTED_LANGUAGES:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic isValid()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->isValid()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized processShortArray([SII)Ljava/lang/String;
    .locals 3
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLSpotterImpl;->validateInstance()V

    .line 68
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/VLSpotterImpl;->mIsStarted:Z

    if-nez v1, :cond_0

    .line 69
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Cannot call processShortArray when Spotter is not started."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 72
    :cond_0
    if-lez p2, :cond_1

    .line 73
    :try_start_1
    invoke-static {p1, p2, p3}, Ljava/util/Arrays;->copyOfRange([SII)[S

    move-result-object p1

    .line 75
    :cond_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLSpotterImpl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    invoke-virtual {v1, p1, p3}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->ProcessShortArray([SI)Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "spottedString":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 79
    .end local v0    # "spottedString":Ljava/lang/String;
    :goto_0
    monitor-exit p0

    return-object v0

    .restart local v0    # "spottedString":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized stopSpotter()V
    .locals 1

    .prologue
    .line 56
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLSpotterImpl;->validateInstance()V

    .line 58
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpotterImpl;->mIsStarted:Z

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterImpl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->MakeReady()Z

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpotterImpl;->mIsStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :cond_0
    monitor-exit p0

    return-void

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
