.class public interface abstract Lcom/vlingo/sdk/internal/http/cookies/Cookie;
.super Ljava/lang/Object;
.source "Cookie.java"


# virtual methods
.method public abstract getDomain()Ljava/lang/String;
.end method

.method public abstract getExpires()J
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPath()Ljava/lang/String;
.end method

.method public abstract getValue()Ljava/lang/String;
.end method

.method public abstract isExpired()Z
.end method

.method public abstract isMatch(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract setDomain(Ljava/lang/String;)V
.end method

.method public abstract setExpires(J)V
.end method

.method public abstract setPath(Ljava/lang/String;)V
.end method

.method public abstract setValue(Ljava/lang/String;)V
.end method
