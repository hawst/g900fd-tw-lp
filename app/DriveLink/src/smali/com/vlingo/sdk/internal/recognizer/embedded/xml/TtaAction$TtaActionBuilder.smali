.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "TtaAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TtaActionBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;",
        ">;"
    }
.end annotation


# instance fields
.field private actionParametersMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;->actionParametersMap:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$1;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addChildTagObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "childName"    # Ljava/lang/String;
    .param p2, "tagObject"    # Ljava/lang/Object;

    .prologue
    .line 70
    const-string/jumbo v1, "Param"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 71
    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;

    .line 72
    .local v0, "parameter":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;->actionParametersMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    .end local v0    # "parameter":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
    :cond_0
    return-void
.end method

.method protected addChildrenBuilders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "builders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;>;"
    const-string/jumbo v0, "Param"

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    return-void
.end method

.method protected addParametersNames(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "paramNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    return-void
.end method

.method protected buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;-><init>()V

    .line 47
    .local v0, "action":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "name"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->name:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;Ljava/lang/String;)Ljava/lang/String;

    .line 48
    new-instance v1, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;->actionParametersMap:Ljava/util/Map;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->parameters:Ljava/util/Map;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;Ljava/util/Map;)Ljava/util/Map;

    .line 49
    return-object v0
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;->buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;->actionParametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 55
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction$TtaActionBuilder;->parametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 56
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string/jumbo v0, "Action"

    return-object v0
.end method
