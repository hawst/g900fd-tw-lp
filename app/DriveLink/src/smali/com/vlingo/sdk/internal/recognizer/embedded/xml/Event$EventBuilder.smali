.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EventBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;",
        ">;"
    }
.end annotation


# instance fields
.field private childParameters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private eventChildName:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;->childParameters:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$1;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addChildTagObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "childName"    # Ljava/lang/String;
    .param p2, "tagObject"    # Ljava/lang/Object;

    .prologue
    .line 70
    const-string/jumbo v2, "Child"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    :try_start_0
    move-object v0, p2

    check-cast v0, Landroid/util/Pair;

    move-object v1, v0

    .line 73
    .local v1, "child":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;->eventChildName:Ljava/lang/String;

    .line 74
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/Collection;

    invoke-static {v2}, Lcom/vlingo/sdk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 75
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;->childParameters:Ljava/util/List;

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    .end local v1    # "child":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_0
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected addChildrenBuilders(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "builders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;>;"
    const-string/jumbo v0, "Child"

    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventChildBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$1;)V

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    return-void
.end method

.method public addParametersNames(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "paramNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    return-void
.end method

.method protected buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;-><init>()V

    .line 49
    .local v0, "result":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;->childParameters:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->childParametersList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;Ljava/util/List;)Ljava/util/List;

    .line 50
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;->eventChildName:Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->childName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;Ljava/lang/String;)Ljava/lang/String;

    .line 51
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "name"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->name:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;Ljava/lang/String;)Ljava/lang/String;

    .line 52
    return-object v0
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;->buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;->eventChildName:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;->childParameters:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 59
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event$EventBuilder;->parametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 60
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    const-string/jumbo v0, "Event"

    return-object v0
.end method
