.class public Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;
.super Ljava/io/InputStream;
.source "InputStreamLogger.java"


# static fields
.field public static final DRIVING_MODE_AUDIO_FILENAME:Ljava/lang/String; = "driving_mode_audio."

.field public static final EXTENSION_AMR:Ljava/lang/String; = "amr"

.field public static final EXTENSION_RAW:Ljava/lang/String; = "raw"


# instance fields
.field private isLogRecoWaveform:Z

.field private mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

.field private mInputStream:Ljava/io/InputStream;

.field private saveAudioFile:Z


# direct methods
.method constructor <init>(Ljava/io/InputStream;ZZ)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "saveAudioFile"    # Z
    .param p3, "isLogRecoWaveform"    # Z

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 19
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->saveAudioFile:Z

    .line 20
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->isLogRecoWaveform:Z

    .line 23
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mInputStream:Ljava/io/InputStream;

    .line 24
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    .line 25
    iput-boolean p2, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->saveAudioFile:Z

    .line 26
    iput-boolean p3, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->isLogRecoWaveform:Z

    .line 27
    return-void
.end method


# virtual methods
.method public declared-synchronized read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 32
    .local v0, "b":I
    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 33
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 35
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit p0

    return v1

    .line 31
    .end local v0    # "b":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized read([B)I
    .locals 3
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v1, p1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .line 50
    .local v0, "bytesRead":I
    if-lez v0, :cond_0

    .line 51
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :cond_0
    monitor-exit p0

    return v0

    .line 49
    .end local v0    # "bytesRead":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized read([BII)I
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 41
    .local v0, "bytesRead":I
    if-lez v0, :cond_0

    .line 42
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    :cond_0
    monitor-exit p0

    return v0

    .line 40
    .end local v0    # "bytesRead":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method declared-synchronized writeLog(Ljava/lang/String;)V
    .locals 8
    .param p1, "guttID"    # Ljava/lang/String;

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-lez v6, :cond_3

    .line 58
    const/4 v2, 0x0

    .line 59
    .local v2, "fos":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .line 61
    .local v4, "inStream":Ljava/io/InputStream;
    if-nez p1, :cond_0

    .line 62
    :try_start_1
    const-string/jumbo p1, "null-ID"

    .line 64
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    .line 65
    .local v5, "root":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->canWrite()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 66
    iget-boolean v6, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->isLogRecoWaveform:Z

    if-eqz v6, :cond_8

    .line 67
    new-instance v0, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "vlingo_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".raw"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 68
    .local v0, "audfile":Ljava/io/File;
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 69
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/FileOutputStream;->write([B)V

    .line 70
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 72
    .end local v0    # "audfile":Ljava/io/File;
    :goto_0
    iget-boolean v6, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->saveAudioFile:Z

    if-eqz v6, :cond_7

    .line 73
    new-instance v0, Ljava/io/File;

    const-string/jumbo v6, "driving_mode_audio.raw"

    invoke-direct {v0, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 74
    .restart local v0    # "audfile":Ljava/io/File;
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 75
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :try_start_3
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/io/FileOutputStream;->write([B)V

    .line 76
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 82
    .end local v0    # "audfile":Ljava/io/File;
    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    .line 84
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 89
    :cond_2
    :goto_2
    if-eqz v4, :cond_3

    .line 91
    :try_start_5
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 98
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .end local v4    # "inStream":Ljava/io/InputStream;
    .end local v5    # "root":Ljava/io/File;
    :cond_3
    :goto_3
    monitor-exit p0

    return-void

    .line 79
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "inStream":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 80
    .local v1, "e":Ljava/io/IOException;
    :goto_4
    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 82
    if-eqz v2, :cond_4

    .line 84
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 89
    :cond_4
    :goto_5
    if-eqz v4, :cond_3

    .line 91
    :try_start_8
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    .line 92
    :catch_1
    move-exception v1

    .line 93
    :try_start_9
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    .line 57
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .end local v4    # "inStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 82
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "inStream":Ljava/io/InputStream;
    :catchall_1
    move-exception v6

    :goto_6
    if-eqz v2, :cond_5

    .line 84
    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 89
    :cond_5
    :goto_7
    if-eqz v4, :cond_6

    .line 91
    :try_start_b
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 82
    :cond_6
    :goto_8
    :try_start_c
    throw v6

    .line 85
    :catch_2
    move-exception v1

    .line 86
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 92
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 93
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 85
    :catch_4
    move-exception v1

    .line 86
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 85
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v5    # "root":Ljava/io/File;
    :catch_5
    move-exception v1

    .line 86
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 92
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 93
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_3

    .line 82
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v6

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_6

    .line 79
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_7
    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    :cond_8
    move-object v3, v2

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0
.end method
