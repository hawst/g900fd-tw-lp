.class public final Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
.super Ljava/lang/Object;
.source "TaggedResults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Tag"
.end annotation


# instance fields
.field private name:Ljava/lang/String;

.field private tagResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 428
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->name:Ljava/lang/String;

    .line 429
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$1;

    .prologue
    .line 422
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;)Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    .prologue
    .line 422
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    return-object v0
.end method

.method static synthetic access$302(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .prologue
    .line 422
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    return-object p1
.end method

.method static synthetic access$600(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    .prologue
    .line 422
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->name:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getRecResults()Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 433
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n            Tag "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 434
    .local v2, "rs":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\n               UttLevel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 435
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "               PhraseLevel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 436
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getPhraseLevel()[Ljava/lang/String;

    move-result-object v1

    .line 437
    .local v1, "phrase":[Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 438
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 439
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "               "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 438
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 442
    .end local v0    # "i":I
    :cond_0
    return-object v2
.end method
