.class public Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
.super Ljava/lang/Object;
.source "RecResults.java"


# static fields
.field static final END_DELIM:C = '}'

.field static final START_DELIM:C = '{'


# instance fields
.field public choiceIndex:I

.field public guttid:Ljava/lang/String;

.field public iMostRecentRecEnd:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

.field public iMostRecentRecStart:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

.field public iNumWords:I

.field private iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

.field private mostRecentString:Ljava/lang/String;

.field private totalStringHash:I

.field public upDownArrowWordIndex:I

.field public uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

.field public uttListCannonical:[Ljava/lang/String;

.field public uttListConf:[F

.field public wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "aMaxWords"    # I

    .prologue
    .line 266
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;-><init>(ILjava/lang/String;)V

    .line 267
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 2
    .param p1, "aMaxWords"    # I
    .param p2, "guttid"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    .line 35
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->mostRecentString:Ljava/lang/String;

    .line 41
    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->totalStringHash:I

    .line 47
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    move-object v0, v1

    .line 50
    check-cast v0, [[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 53
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListConf:[F

    .line 56
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListCannonical:[Ljava/lang/String;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->upDownArrowWordIndex:I

    .line 62
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iMostRecentRecStart:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 65
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iMostRecentRecEnd:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 271
    new-array v0, p1, [Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 272
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->guttid:Ljava/lang/String;

    .line 273
    return-void
.end method

.method private countMatchingContextWords(I[Ljava/lang/String;I[Ljava/lang/String;I)I
    .locals 6
    .param p1, "index"    # I
    .param p2, "previousWords"    # [Ljava/lang/String;
    .param p3, "numPreviousWords"    # I
    .param p4, "nextWords"    # [Ljava/lang/String;
    .param p5, "numNextWords"    # I

    .prologue
    .line 166
    const/4 v2, 0x0

    .line 167
    .local v2, "numFound":I
    const/4 v0, 0x2

    .line 168
    .local v0, "SEARCH_PADDING":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p3, :cond_1

    .line 170
    aget-object v3, p2, v1

    add-int/lit8 v4, p3, 0x2

    sub-int v4, p1, v4

    invoke-direct {p0, v3, v4, p1}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->hasMatchingWord(Ljava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 171
    add-int/lit8 v2, v2, 0x1

    .line 168
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 175
    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-ge v1, p5, :cond_3

    .line 178
    aget-object v3, p4, v1

    add-int/lit8 v4, p1, 0x1

    add-int/lit8 v5, p5, 0x2

    add-int/2addr v5, p1

    invoke-direct {p0, v3, v4, v5}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->hasMatchingWord(Ljava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 179
    add-int/lit8 v2, v2, 0x1

    .line 175
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 183
    :cond_3
    return v2
.end method

.method private growResults(I)V
    .locals 3
    .param p1, "n"    # I

    .prologue
    .line 320
    new-array v1, p1, [Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 322
    .local v1, "newResults":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v0, v2, :cond_0

    .line 323
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 325
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 326
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 327
    return-void
.end method

.method private hasMatchingWord(Ljava/lang/String;II)Z
    .locals 5
    .param p1, "word"    # Ljava/lang/String;
    .param p2, "startIndex"    # I
    .param p3, "endIndex"    # I

    .prologue
    .line 187
    if-gez p2, :cond_0

    const/4 p2, 0x0

    .line 188
    :cond_0
    iget v4, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-le p3, v4, :cond_1

    iget p3, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    .line 190
    :cond_1
    move v2, p2

    .local v2, "i":I
    :goto_0
    if-ge v2, p3, :cond_4

    .line 191
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v4, v4, v2

    iget-object v1, v4, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iChoices:[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;

    .line 192
    .local v1, "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v4, v1

    if-ge v3, v4, :cond_3

    .line 193
    aget-object v4, v1, v3

    iget-object v0, v4, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->originalChoice:Ljava/lang/String;

    .line 194
    .local v0, "choice":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    .line 197
    .end local v0    # "choice":Ljava/lang/String;
    .end local v1    # "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    .end local v3    # "j":I
    :goto_2
    return v4

    .line 192
    .restart local v0    # "choice":Ljava/lang/String;
    .restart local v1    # "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    .restart local v3    # "j":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 190
    .end local v0    # "choice":Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 197
    .end local v1    # "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    .end local v3    # "j":I
    :cond_4
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private moveWords(II)V
    .locals 5
    .param p1, "aWordIndex"    # I
    .param p2, "aNumNewWords"    # I

    .prologue
    .line 305
    iget v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int v1, v2, p2

    .line 306
    .local v1, "newNumWords":I
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    array-length v2, v2

    if-le v1, v2, :cond_0

    .line 307
    mul-int/lit8 v2, v1, 0x2

    invoke-direct {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->growResults(I)V

    .line 312
    :cond_0
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    add-int v2, p1, p2

    if-lt v0, v2, :cond_1

    .line 313
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    sub-int v4, v0, p2

    aget-object v3, v3, v4

    aput-object v3, v2, v0

    .line 312
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 315
    :cond_1
    iget v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/2addr v2, p2

    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    .line 316
    return-void
.end method


# virtual methods
.method public addNBest(ILcom/vlingo/sdk/internal/recognizer/results/RecNBest;)V
    .locals 2
    .param p1, "aNewIndex"    # I
    .param p2, "recNBest"    # Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .prologue
    .line 347
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 348
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->growResults(I)V

    .line 351
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->moveWords(II)V

    .line 353
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aput-object p2, v0, p1

    .line 354
    return-void
.end method

.method public addWord(ILjava/lang/String;)V
    .locals 2
    .param p1, "aNewIndex"    # I
    .param p2, "aNewString"    # Ljava/lang/String;

    .prologue
    .line 334
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 335
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->growResults(I)V

    .line 338
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->moveWords(II)V

    .line 340
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    invoke-direct {v1, p2}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, p1

    .line 341
    return-void
.end method

.method public append(Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;)V
    .locals 3
    .param p1, "w"    # Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .prologue
    .line 372
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    .line 373
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->growResults(I)V

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    aput-object p1, v0, v1

    .line 376
    return-void
.end method

.method public append(Ljava/lang/String;)V
    .locals 3
    .param p1, "aNewString"    # Ljava/lang/String;

    .prologue
    .line 361
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    .line 362
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->growResults(I)V

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    invoke-direct {v2, p1}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 365
    return-void
.end method

.method public clear()V
    .locals 3

    .prologue
    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v0, v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    .line 78
    return-void
.end method

.method public clearMostRecentRecognition()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 81
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iMostRecentRecStart:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 82
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iMostRecentRecEnd:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 83
    return-void
.end method

.method public clearPositions(I)V
    .locals 3
    .param p1, "fromIndex"    # I

    .prologue
    .line 437
    move v0, p1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v0, v2, :cond_1

    .line 439
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 440
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v1, v2, v0

    .line 442
    .local v1, "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->clearPosition()V

    .line 437
    .end local v1    # "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 445
    :cond_1
    return-void
.end method

.method public deleteWord(I)V
    .locals 4
    .param p1, "aWordIndex"    # I

    .prologue
    .line 421
    if-ltz p1, :cond_1

    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge p1, v1, :cond_1

    .line 422
    move v0, p1

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 423
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    aput-object v2, v1, v0

    .line 422
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 425
    :cond_0
    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    .line 427
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public findNumChoice(I)I
    .locals 2
    .param p1, "aWordIndex"    # I

    .prologue
    .line 412
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v0, v1, p1

    .line 413
    .local v0, "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget v1, v0, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iN:I

    return v1
.end method

.method public getChars([C)I
    .locals 9
    .param p1, "data"    # [C

    .prologue
    .line 502
    const/4 v1, 0x0

    .line 504
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v0, v7, :cond_1

    .line 505
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v5, v7, v0

    .line 506
    .local v5, "w1":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget-object v4, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    .line 507
    .local v4, "s1":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 509
    .local v3, "len":I
    const/4 v7, 0x0

    invoke-virtual {v4, v7, v3, p1, v1}, Ljava/lang/String;->getChars(II[CI)V

    .line 510
    add-int/2addr v1, v3

    .line 511
    iget v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/lit8 v7, v7, -0x1

    if-ge v0, v7, :cond_0

    .line 512
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    add-int/lit8 v8, v0, 0x1

    aget-object v6, v7, v8

    .line 514
    .local v6, "w2":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    invoke-virtual {v6, v5}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->needsSpace(Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 515
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    const/16 v7, 0x20

    aput-char v7, p1, v1

    move v1, v2

    .line 504
    .end local v2    # "index":I
    .end local v6    # "w2":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .restart local v1    # "index":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 519
    .end local v3    # "len":I
    .end local v4    # "s1":Ljava/lang/String;
    .end local v5    # "w1":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_1
    return v1
.end method

.method public getNBestForWord(Ljava/lang/String;)Ljava/util/Vector;
    .locals 7
    .param p1, "word"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 92
    .local v5, "nbest":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v2, v6, :cond_2

    .line 93
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v6, v6, v2

    iget-object v1, v6, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iChoices:[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;

    .line 94
    .local v1, "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    array-length v6, v1

    if-lez v6, :cond_1

    .line 95
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v6, v1

    if-ge v3, v6, :cond_1

    .line 96
    aget-object v6, v1, v3

    iget-object v0, v6, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->originalChoice:Ljava/lang/String;

    .line 97
    .local v0, "choice":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 99
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_2
    array-length v6, v1

    if-ge v4, v6, :cond_2

    .line 101
    aget-object v6, v1, v4

    iget-object v6, v6, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->originalChoice:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 99
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 95
    .end local v4    # "k":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 92
    .end local v0    # "choice":Ljava/lang/String;
    .end local v3    # "j":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 109
    .end local v1    # "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    :cond_2
    return-object v5
.end method

.method public getNBestForWordIfBetterMatch(Ljava/util/List;Ljava/lang/String;[Ljava/lang/String;I[Ljava/lang/String;II)I
    .locals 15
    .param p2, "word"    # Ljava/lang/String;
    .param p3, "previousWords"    # [Ljava/lang/String;
    .param p4, "numPreviousWords"    # I
    .param p5, "nextWords"    # [Ljava/lang/String;
    .param p6, "numNextWords"    # I
    .param p7, "bestNumMatchingWordsSoFar"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "I[",
            "Ljava/lang/String;",
            "II)I"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, -0x1

    .line 117
    .local v9, "bestWordIndex":I
    move/from16 v8, p7

    .line 118
    .local v8, "bestNumMatches":I
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v7

    .line 120
    .local v7, "bestMatchingNbest":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v2, v1, :cond_4

    .line 121
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v1, v1, v2

    iget-object v11, v1, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iChoices:[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;

    .line 122
    .local v11, "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_1
    array-length v1, v11

    if-ge v12, v1, :cond_3

    .line 123
    aget-object v1, v11, v12

    iget-object v10, v1, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->originalChoice:Ljava/lang/String;

    .line 125
    .local v10, "choice":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, p0

    move-object/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    .line 126
    invoke-direct/range {v1 .. v6}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->countMatchingContextWords(I[Ljava/lang/String;I[Ljava/lang/String;I)I

    move-result v1

    add-int/lit8 v14, v1, 0x1

    .line 131
    .local v14, "matchingContextCount":I
    if-gt v14, v8, :cond_0

    const/4 v1, 0x2

    if-ge v7, v1, :cond_2

    array-length v1, v11

    const/4 v3, 0x1

    if-le v1, v3, :cond_2

    .line 133
    :cond_0
    array-length v1, v11

    const/4 v3, 0x1

    if-gt v1, v3, :cond_1

    if-nez v8, :cond_2

    .line 135
    :cond_1
    move v8, v14

    .line 136
    move v9, v2

    .line 137
    array-length v7, v11

    .line 122
    .end local v14    # "matchingContextCount":I
    :cond_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 120
    .end local v10    # "choice":Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 144
    .end local v11    # "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    .end local v12    # "j":I
    :cond_4
    if-ltz v9, :cond_5

    .line 146
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->clear()V

    .line 147
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v1, v1, v9

    iget-object v11, v1, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iChoices:[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;

    .line 148
    .restart local v11    # "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    const/4 v13, 0x0

    .local v13, "k":I
    :goto_2
    array-length v1, v11

    if-ge v13, v1, :cond_5

    .line 151
    aget-object v1, v11, v13

    iget-object v1, v1, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->originalChoice:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 154
    .end local v11    # "choices":[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    .end local v13    # "k":I
    :cond_5
    return v8
.end method

.method public getPhraseLevel()[Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 575
    const/4 v2, 0x0

    .line 576
    .local v2, "plChoices":[Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    array-length v6, v6

    if-lez v6, :cond_2

    .line 577
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 578
    .local v5, "sentences":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    array-length v6, v5

    new-array v2, v6, [Ljava/lang/String;

    .line 579
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 581
    .local v3, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v5

    if-ge v0, v6, :cond_3

    .line 582
    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 583
    aget-object v4, v5, v0

    .line 584
    .local v4, "sentence":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    if-nez v4, :cond_0

    .line 585
    const/4 v6, 0x0

    .line 599
    .end local v0    # "i":I
    .end local v3    # "sb":Ljava/lang/StringBuffer;
    .end local v4    # "sentence":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .end local v5    # "sentences":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :goto_1
    return-object v6

    .line 587
    .restart local v0    # "i":I
    .restart local v3    # "sb":Ljava/lang/StringBuffer;
    .restart local v4    # "sentence":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .restart local v5    # "sentences":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_0
    array-length v6, v4

    if-lez v6, :cond_1

    .line 588
    aget-object v6, v4, v7

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->getSelectedWord()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 589
    const/4 v1, 0x1

    .local v1, "j":I
    :goto_2
    array-length v6, v4

    if-ge v1, v6, :cond_1

    .line 590
    const-string/jumbo v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 591
    aget-object v6, v4, v1

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->getSelectedWord()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 589
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 594
    .end local v1    # "j":I
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v0

    .line 581
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 597
    .end local v0    # "i":I
    .end local v3    # "sb":Ljava/lang/StringBuffer;
    .end local v4    # "sentence":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .end local v5    # "sentences":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_2
    const/4 v6, 0x1

    new-array v2, v6, [Ljava/lang/String;

    .end local v2    # "plChoices":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v7

    .restart local v2    # "plChoices":[Ljava/lang/String;
    :cond_3
    move-object v6, v2

    .line 599
    goto :goto_1
.end method

.method public getString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 531
    const/4 v5, 0x0

    .line 536
    .local v5, "totalHash":I
    iget v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-nez v7, :cond_0

    .line 537
    const-string/jumbo v7, ""

    .line 556
    :goto_0
    return-object v7

    .line 540
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v1, v7, :cond_1

    .line 541
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v6, v7, v1

    .line 542
    .local v6, "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget-object v4, v6, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    .line 544
    .local v4, "s":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v7

    add-int/2addr v5, v7

    .line 540
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 546
    .end local v4    # "s":Ljava/lang/String;
    .end local v6    # "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_1
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->mostRecentString:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->totalStringHash:I

    if-ne v5, v7, :cond_2

    .line 547
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->mostRecentString:Ljava/lang/String;

    goto :goto_0

    .line 549
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->size()I

    move-result v2

    .line 550
    .local v2, "len":I
    new-array v0, v2, [C

    .line 552
    .local v0, "data":[C
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getChars([C)I

    move-result v3

    .line 554
    .local v3, "len2":I
    new-instance v7, Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct {v7, v0, v8, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->mostRecentString:Ljava/lang/String;

    .line 555
    iput v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->totalStringHash:I

    .line 556
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->mostRecentString:Ljava/lang/String;

    goto :goto_0
.end method

.method public getWord(I)Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 241
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge p1, v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v0, v0, p1

    .line 247
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWordLength(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 253
    if-ltz p1, :cond_0

    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge p1, v1, :cond_0

    .line 254
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v0, v1, p1

    .line 257
    .local v0, "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->length()I

    move-result v1

    .line 260
    .end local v0    # "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public indexOf(Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;)I
    .locals 3
    .param p1, "word"    # Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .prologue
    .line 563
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v0, v2, :cond_1

    .line 564
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v1, v2, v0

    .line 565
    .local v1, "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    if-ne v1, p1, :cond_0

    .line 568
    .end local v0    # "i":I
    .end local v1    # "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :goto_1
    return v0

    .line 563
    .restart local v0    # "i":I
    .restart local v1    # "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 568
    .end local v1    # "w":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public insert(Lcom/vlingo/sdk/internal/recognizer/results/RecResults;II)I
    .locals 6
    .param p1, "input"    # Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    .param p2, "aCursorWord"    # I
    .param p3, "aCursorChar"    # I

    .prologue
    .line 384
    if-nez p3, :cond_2

    .line 385
    move v2, p2

    .line 389
    .local v2, "wordIndex":I
    :goto_0
    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-le v2, v3, :cond_0

    .line 390
    iget v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    .line 393
    :cond_0
    iget v1, p1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    .line 395
    .local v1, "numNewWords":I
    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    array-length v4, v4

    if-lt v3, v4, :cond_1

    .line 396
    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v1

    invoke-direct {p0, v3}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->growResults(I)V

    .line 400
    :cond_1
    invoke-direct {p0, v2, v1}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->moveWords(II)V

    .line 402
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 403
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    add-int v4, v0, v2

    iget-object v5, p1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v5, v5, v0

    aput-object v5, v3, v4

    .line 402
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 387
    .end local v0    # "i":I
    .end local v1    # "numNewWords":I
    .end local v2    # "wordIndex":I
    :cond_2
    add-int/lit8 v2, p2, 0x1

    .restart local v2    # "wordIndex":I
    goto :goto_0

    .line 405
    .restart local v0    # "i":I
    .restart local v1    # "numNewWords":I
    :cond_3
    return v1
.end method

.method public removeRangeOfWords(Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;I)I
    .locals 8
    .param p1, "start"    # Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .param p2, "end"    # Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .param p3, "cursor"    # I

    .prologue
    .line 203
    const/4 v4, -0x1

    .line 204
    .local v4, "startIndex":I
    const/4 v0, -0x1

    .line 206
    .local v0, "endIndex":I
    move v2, p3

    .line 209
    .local v2, "newCursor":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v1, v5, :cond_1

    .line 210
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v5, v5, v1

    if-ne v5, p1, :cond_0

    .line 211
    move v4, v1

    .line 214
    :cond_0
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v5, v5, v1

    if-ne v5, p2, :cond_2

    .line 215
    move v0, v1

    .line 221
    :cond_1
    if-ltz v4, :cond_5

    if-ltz v0, :cond_5

    .line 223
    add-int/lit8 v5, v0, 0x1

    sub-int v3, v5, v4

    .line 225
    .local v3, "numToDelete":I
    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    sub-int/2addr v5, v3

    iput v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    .line 226
    move v1, v4

    :goto_1
    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v1, v5, :cond_3

    .line 227
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    add-int v7, v1, v3

    aget-object v6, v6, v7

    aput-object v6, v5, v1

    .line 226
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 209
    .end local v3    # "numToDelete":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 229
    .restart local v3    # "numToDelete":I
    :cond_3
    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    :goto_2
    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/2addr v5, v3

    if-ge v1, v5, :cond_4

    .line 230
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    const/4 v6, 0x0

    aput-object v6, v5, v1

    .line 229
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 232
    :cond_4
    move v2, v4

    move v5, v2

    .line 235
    .end local v3    # "numToDelete":I
    :goto_3
    return v5

    :cond_5
    const/4 v5, -0x1

    goto :goto_3
.end method

.method public size()I
    .locals 7

    .prologue
    .line 453
    const/4 v1, 0x0

    .line 455
    .local v1, "num":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v0, v5, :cond_1

    .line 456
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v3, v5, v0

    .line 457
    .local v3, "w1":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget-object v2, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    .line 459
    .local v2, "s1":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v1, v5

    .line 460
    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/lit8 v5, v5, -0x1

    if-ge v0, v5, :cond_0

    .line 461
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    add-int/lit8 v6, v0, 0x1

    aget-object v4, v5, v6

    .line 463
    .local v4, "w2":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    invoke-virtual {v4, v3}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->needsSpace(Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 464
    add-int/lit8 v1, v1, 0x1

    .line 455
    .end local v4    # "w2":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 468
    .end local v2    # "s1":Ljava/lang/String;
    .end local v3    # "w1":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_1
    return v1
.end method

.method public size(II)I
    .locals 7
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    .line 476
    const/4 v1, 0x0

    .line 478
    .local v1, "num":I
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_1

    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v0, v5, :cond_1

    .line 479
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v3, v5, v0

    .line 480
    .local v3, "w1":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget-object v2, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    .line 482
    .local v2, "s1":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v1, v5

    .line 483
    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    add-int/lit8 v5, v5, -0x1

    if-ge v0, v5, :cond_0

    add-int/lit8 v5, p2, -0x1

    if-ge v0, v5, :cond_0

    .line 484
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iResults:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    add-int/lit8 v6, v0, 0x1

    aget-object v4, v5, v6

    .line 486
    .local v4, "w2":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    invoke-virtual {v4, v3}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->needsSpace(Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 487
    add-int/lit8 v1, v1, 0x1

    .line 478
    .end local v4    # "w2":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 491
    .end local v2    # "s1":Ljava/lang/String;
    .end local v3    # "w1":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_1
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 524
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateSentenceCap()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 277
    const/4 v1, 0x0

    .line 278
    .local v1, "lastWasPeriod":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->iNumWords:I

    if-ge v0, v4, :cond_5

    .line 279
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getWord(I)Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    move-result-object v3

    .line 280
    .local v3, "word":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget-object v4, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 278
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 282
    :cond_1
    iget-object v4, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 283
    const/4 v1, 0x1

    .line 284
    goto :goto_1

    .line 286
    :cond_2
    if-eqz v1, :cond_0

    .line 287
    iget-boolean v4, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->capitalized:Z

    if-nez v4, :cond_4

    iget-object v4, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isLowerCase(C)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 288
    iget-object v4, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    invoke-virtual {v4, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 289
    .local v2, "newWord":Ljava/lang/String;
    iget-object v4, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v6, :cond_3

    .line 290
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 291
    :cond_3
    iput-object v2, v3, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iSelectedWord:Ljava/lang/String;

    .line 293
    .end local v2    # "newWord":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 296
    .end local v3    # "word":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_5
    return-void
.end method
