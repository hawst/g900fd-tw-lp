.class public Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
.super Ljava/lang/Object;
.source "ClientMeta.java"


# static fields
.field private static instance:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;


# instance fields
.field deviceID:Ljava/lang/String;

.field deviceModel:Ljava/lang/String;

.field deviceOS:Ljava/lang/String;

.field fakeLatLong:Ljava/lang/Boolean;

.field hardwareID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->instance:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceID:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceModel:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceOS:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->hardwareID:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->fakeLatLong:Ljava/lang/Boolean;

    return-void
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->instance:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    .line 51
    return-void
.end method

.method public static getInstance()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->instance:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->instance:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    .line 46
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->instance:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    return-object v0
.end method


# virtual methods
.method public getCarrier()Ljava/lang/String;
    .locals 3

    .prologue
    .line 54
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/VLSdk;->getDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;

    move-result-object v1

    .line 55
    .local v1, "ds":Lcom/vlingo/sdk/util/SDKDebugSettings;
    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v1}, Lcom/vlingo/sdk/util/SDKDebugSettings;->getCarrier()Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "carrier":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 61
    .end local v0    # "carrier":Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getInstance()Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getCurrentCarrier()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCarrierCountry()Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/VLSdk;->getDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;

    move-result-object v1

    .line 66
    .local v1, "ds":Lcom/vlingo/sdk/util/SDKDebugSettings;
    if-eqz v1, :cond_0

    .line 67
    invoke-virtual {v1}, Lcom/vlingo/sdk/util/SDKDebugSettings;->getCarrierCountry()Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "carrierCountry":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    .end local v0    # "carrierCountry":Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getInstance()Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getCarrierCountry()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 5

    .prologue
    .line 85
    const-string/jumbo v3, "uuid"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 86
    .local v2, "uuid":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 88
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 89
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/nuance/id/NuanceId;

    const/16 v3, 0xe

    invoke-direct {v1, v0, v3}, Lcom/nuance/id/NuanceId;-><init>(Landroid/content/Context;I)V

    .line 90
    .local v1, "nuanceId":Lcom/nuance/id/NuanceId;
    invoke-virtual {v1}, Lcom/nuance/id/NuanceId;->getId()Ljava/lang/String;

    move-result-object v2

    .line 97
    const-string/jumbo v3, "uuid"

    invoke-static {v3, v2}, Lcom/vlingo/sdk/internal/settings/Settings;->setPersistentString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "nuanceId":Lcom/nuance/id/NuanceId;
    :cond_0
    return-object v2
.end method

.method public getDeviceMake()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/VLSdk;->getDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;

    move-result-object v0

    .line 116
    .local v0, "ds":Lcom/vlingo/sdk/util/SDKDebugSettings;
    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0}, Lcom/vlingo/sdk/util/SDKDebugSettings;->getModelNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceModel:Ljava/lang/String;

    .line 118
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceModel:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceModel:Ljava/lang/String;

    .line 127
    :goto_0
    return-object v1

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceModel:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 123
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getInstance()Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getModel()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceModel:Ljava/lang/String;

    .line 124
    :cond_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceModel:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 125
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceModel:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceModel:Ljava/lang/String;

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->deviceModel:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDeviceOS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getInstance()Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getOSVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceOSName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    const-string/jumbo v0, "Android"

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/vlingo/sdk/internal/settings/Settings;->LANGUAGE:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 4

    .prologue
    .line 146
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/VLSdk;->getDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;

    move-result-object v0

    .line 147
    .local v0, "ds":Lcom/vlingo/sdk/util/SDKDebugSettings;
    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {v0}, Lcom/vlingo/sdk/util/SDKDebugSettings;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object v2, v1

    .line 158
    .end local v1    # "location":Ljava/lang/String;
    .local v2, "location":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 154
    .end local v2    # "location":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getCellTowerInfo()Ljava/lang/String;

    move-result-object v1

    .line 155
    .restart local v1    # "location":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 156
    const-string/jumbo v1, ""

    :cond_1
    move-object v2, v1

    .line 158
    .end local v1    # "location":Ljava/lang/String;
    .restart local v2    # "location":Ljava/lang/String;
    goto :goto_0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getSpeakerID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 76
    const-string/jumbo v1, "speaker_id"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "speakerId":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 78
    invoke-static {}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->getSpeakerID()Ljava/lang/String;

    move-result-object v0

    .line 79
    const-string/jumbo v1, "speaker_id"

    invoke-static {v1, v0}, Lcom/vlingo/sdk/internal/settings/Settings;->setPersistentString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    return-object v0
.end method

.method public getVendorID()I
    .locals 1

    .prologue
    .line 103
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getInstance()Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getVendorID()I

    move-result v0

    return v0
.end method
