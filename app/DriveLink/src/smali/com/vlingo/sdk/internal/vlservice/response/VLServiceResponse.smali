.class public Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
.super Ljava/lang/Object;
.source "VLServiceResponse.java"


# instance fields
.field protected actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

.field private dialogGUID:Ljava/lang/String;

.field protected dialogState:[B

.field private dialogTurn:I

.field protected isError:Z

.field protected messages:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->isError:Z

    .line 18
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogTurn:I

    return-void
.end method

.method public static createFromXml(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
    .locals 2
    .param p0, "xml"    # Ljava/lang/String;

    .prologue
    .line 110
    new-instance v1, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;-><init>()V

    .line 111
    .local v1, "responseParser":Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;
    invoke-virtual {v1, p0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->parseResponseXml(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v0

    .line 112
    .local v0, "res":Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
    return-object v0
.end method


# virtual methods
.method public addAction(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/internal/vlservice/response/Action;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->addElement(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V

    .line 79
    return-void
.end method

.method public addMessage(Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;)V
    .locals 1
    .param p1, "message"    # Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 68
    return-void
.end method

.method public getActionList()Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    return-object v0
.end method

.method public getDialogGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogGUID:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogState()[B
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogState:[B

    return-object v0
.end method

.method public getDialogTurn()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogTurn:I

    return v0
.end method

.method public getFirstMessage()Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .line 63
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMessages()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    return-object v0
.end method

.method public hasActions()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDialogState()Z
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogState:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogState:[B

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessageOfType(I)Z
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 32
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 33
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 34
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .line 35
    .local v1, "message":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 36
    const/4 v2, 0x1

    .line 40
    .end local v0    # "i":I
    .end local v1    # "message":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :goto_1
    return v2

    .line 33
    .restart local v0    # "i":I
    .restart local v1    # "message":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    .end local v0    # "i":I
    .end local v1    # "message":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public hasMessages()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWarnings()Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->hasMessageOfType(I)Z

    move-result v0

    return v0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->hasMessageOfType(I)Z

    move-result v0

    return v0
.end method

.method public setActionList(Lcom/vlingo/sdk/internal/vlservice/response/ActionList;)V
    .locals 0
    .param p1, "actionList"    # Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    .line 72
    return-void
.end method

.method public setDialogGuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "guid"    # Ljava/lang/String;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogGUID:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public setDialogState([B)V
    .locals 0
    .param p1, "state"    # [B

    .prologue
    .line 102
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogState:[B

    .line 103
    return-void
.end method

.method public setDialogTurn(I)V
    .locals 0
    .param p1, "turn"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogTurn:I

    .line 95
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 116
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 117
    .local v0, "buff":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->getFirstMessage()Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 118
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 119
    iget-object v3, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .line 120
    .local v2, "msg":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 121
    const-string/jumbo v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 118
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 124
    .end local v1    # "i":I
    .end local v2    # "msg":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :cond_0
    const-string/jumbo v3, "<no message>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    :cond_1
    const-string/jumbo v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    iget-object v3, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    if-eqz v3, :cond_2

    .line 128
    iget-object v3, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 132
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 130
    :cond_2
    const-string/jumbo v3, "<no actions>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method
