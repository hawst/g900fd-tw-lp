.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "Configuration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConfigurationBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;",
        ">;"
    }
.end annotation


# instance fields
.field private slots:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;->slots:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$1;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addChildTagObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "childName"    # Ljava/lang/String;
    .param p2, "tagObject"    # Ljava/lang/Object;

    .prologue
    .line 67
    const-string/jumbo v0, "Slot"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;->slots:Ljava/util/List;

    check-cast p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;

    .end local p2    # "tagObject":Ljava/lang/Object;
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    :cond_0
    return-void
.end method

.method protected addChildrenBuilders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "builders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;>;"
    const-string/jumbo v0, "Slot"

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    return-void
.end method

.method public addParametersNames(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "paramNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "fcf"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    const-string/jumbo v0, "domain"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method protected buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;-><init>()V

    .line 46
    .local v0, "result":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;->slots:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->slotsList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;Ljava/util/List;)Ljava/util/List;

    .line 47
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "fcf"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->fcf:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;Ljava/lang/String;)Ljava/lang/String;

    .line 48
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "domain"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->domain:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;Ljava/lang/String;)Ljava/lang/String;

    .line 49
    return-object v0
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;->buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;->slots:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 55
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration$ConfigurationBuilder;->parametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 56
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    const-string/jumbo v0, "Configuration"

    return-object v0
.end method
