.class Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;
.super Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
.source "ASRRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReceivingState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 1

    .prologue
    .line 1056
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;

    .prologue
    .line 1056
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    return-void
.end method


# virtual methods
.method public onRun()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1067
    const-string/jumbo v3, "[LatencyCheck] ReceivingState receiving ..."

    invoke-virtual {p0, v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->log(Ljava/lang/String;)V

    .line 1069
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v4, 0x3

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(I)V
    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    .line 1071
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v4, "RESP"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 1072
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v4, "RESH"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 1076
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    # setter for: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeGotResults:J
    invoke-static {v3, v4, v5}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4002(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;J)J

    .line 1079
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Ljava/net/HttpURLConnection;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    .line 1080
    .local v0, "cookieJar":Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    if-eqz v0, :cond_0

    .line 1081
    invoke-static {v0}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->handleResponseCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V

    .line 1083
    :cond_0
    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

    invoke-direct {v2}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;-><init>()V

    .line 1094
    .local v2, "parser":Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->consume(Ljava/io/InputStream;)[B
    invoke-static {v5, v6}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/io/InputStream;)[B

    move-result-object v5

    const-string/jumbo v6, "UTF-8"

    invoke-direct {v4, v5, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->parseResponseXml(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v4

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1102
    .end local v0    # "cookieJar":Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .end local v2    # "parser":Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;
    :goto_0
    return-void

    .line 1095
    :catch_0
    move-exception v1

    .line 1096
    .local v1, "e":Ljava/net/SocketTimeoutException;
    const-string/jumbo v3, "response TIMED OUT"

    invoke-virtual {p0, v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->log(Ljava/lang/String;)V

    .line 1097
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v4, -0x3

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onTimeout(I)V
    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    goto :goto_0

    .line 1098
    .end local v1    # "e":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v1

    .line 1099
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->log(Ljava/lang/Throwable;)V

    .line 1100
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v4, -0x2

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onError(I)V
    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    goto :goto_0
.end method

.method public sendAudio([BII)V
    .locals 1
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 1060
    const-string/jumbo v0, "sendAudio()"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;->log(Ljava/lang/String;)V

    .line 1062
    return-void
.end method
