.class public final Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;
.super Ljava/lang/Object;
.source "VoconConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;
    }
.end annotation


# instance fields
.field private mClcFilename:Ljava/lang/String;

.field private mGrammarFilename:Ljava/lang/String;

.field private mGrammarInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mModelFilename:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$1;

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 7
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mModelFilename:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 7
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mClcFilename:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 7
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mGrammarFilename:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    .prologue
    .line 7
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mGrammarInfoList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 7
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mGrammarInfoList:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public getClcFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mClcFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getGrammarFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mGrammarFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getGrammarInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$GrammarInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mGrammarInfoList:Ljava/util/List;

    return-object v0
.end method

.method public getModelFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->mModelFilename:Ljava/lang/String;

    return-object v0
.end method
