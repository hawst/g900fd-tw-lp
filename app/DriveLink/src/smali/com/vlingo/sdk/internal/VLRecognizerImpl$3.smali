.class Lcom/vlingo/sdk/internal/VLRecognizerImpl$3;
.super Ljava/lang/Object;
.source "VLRecognizerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/VLRecognizerImpl;->stopRecognition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$3;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$3;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getDestroyLock()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 290
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$3;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 291
    monitor-exit v1

    .line 295
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$3;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$600(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->stop()V

    .line 294
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
