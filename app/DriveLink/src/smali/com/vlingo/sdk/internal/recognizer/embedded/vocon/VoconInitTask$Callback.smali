.class public interface abstract Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;
.super Ljava/lang/Object;
.source "VoconInitTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onVoconInitFailed()V
.end method

.method public abstract onVoconInitSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;",
            ">;)V"
        }
    .end annotation
.end method
