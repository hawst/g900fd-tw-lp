.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
.super Ljava/lang/Object;
.source "VoconTagResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private tagJson:Lorg/json/JSONObject;

.field private tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "tag"    # Lorg/json/JSONObject;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    .line 71
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagJson:Lorg/json/JSONObject;

    .line 72
    return-void
.end method

.method private addResult(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    .locals 2
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_items:Ljava/util/List;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->access$200(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_items:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;Ljava/util/List;)Ljava/util/List;

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_items:Ljava/util/List;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->access$200(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    return-object p0
.end method

.method private beginTimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    .locals 2
    .param p1, "beginTime"    # J

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_beginTimeMs:Ljava/lang/Long;

    .line 125
    return-object p0
.end method

.method private confidence(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    .locals 2
    .param p1, "confidence"    # J

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_conf:Ljava/lang/Long;

    .line 98
    return-object p0
.end method

.method private endtimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    .locals 2
    .param p1, "endTime"    # J

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_endTimeMs:Ljava/lang/Long;

    .line 138
    return-object p0
.end method

.method private name(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_name:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;Ljava/lang/String;)Ljava/lang/String;

    .line 84
    return-object p0
.end method

.method private score(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    .locals 2
    .param p1, "score"    # J

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_score:Ljava/lang/Long;

    .line 112
    return-object p0
.end method

.method private userId(II)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    .locals 7
    .param p1, "hi32"    # I
    .param p2, "lo32"    # I

    .prologue
    .line 153
    const/4 v0, 0x0

    .line 154
    .local v0, "userId":Ljava/lang/Long;
    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    .line 155
    int-to-long v1, p1

    const/16 v3, 0x20

    shl-long/2addr v1, v3

    int-to-long v3, p2

    const-wide/32 v5, 0xfffffff

    and-long/2addr v3, v5

    or-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->_userId:Ljava/lang/Long;

    .line 158
    return-object p0
.end method


# virtual methods
.method public build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 188
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagJson:Lorg/json/JSONObject;

    if-eqz v8, :cond_0

    .line 191
    :try_start_0
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_name"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->name(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :try_start_1
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_conf"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->confidence(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 207
    :try_start_2
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_score"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->score(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 215
    :try_start_3
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_beginTimeMs"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->beginTimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    .line 223
    :try_start_4
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_endTimeMs"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->endtimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    .line 231
    :try_start_5
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_items"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 233
    .local v3, "nodes":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "nodePosn":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v2, v8, :cond_2

    .line 234
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 235
    .local v1, "nodeJson":Lorg/json/JSONObject;
    const-string/jumbo v8, "_type"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->getFromString(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    move-result-object v6

    .line 237
    .local v6, "type":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    sget-object v8, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$results$HypothesisNodeType:[I

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 271
    .end local v1    # "nodeJson":Lorg/json/JSONObject;
    .end local v2    # "nodePosn":I
    .end local v3    # "nodes":Lorg/json/JSONArray;
    .end local v6    # "type":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    :cond_0
    :goto_1
    return-object v7

    .line 192
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Lorg/json/JSONException;
    goto :goto_1

    .line 200
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 203
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 208
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v0

    .line 211
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 216
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_3
    move-exception v0

    .line 219
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 224
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_4
    move-exception v0

    .line 227
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 242
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "nodeJson":Lorg/json/JSONObject;
    .restart local v2    # "nodePosn":I
    .restart local v3    # "nodes":Lorg/json/JSONArray;
    .restart local v6    # "type":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    :pswitch_0
    new-instance v8, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;

    invoke-direct {v8, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    move-result-object v4

    .line 243
    .local v4, "subTag":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    if-eqz v4, :cond_1

    .line 245
    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->addResult(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;

    .line 233
    .end local v4    # "subTag":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 252
    :pswitch_1
    new-instance v8, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;

    invoke-direct {v8, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    move-result-object v5

    .line 253
    .local v5, "terminal":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;
    if-eqz v5, :cond_1

    .line 255
    invoke-direct {p0, v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->addResult(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_2

    .line 264
    .end local v1    # "nodeJson":Lorg/json/JSONObject;
    .end local v2    # "nodePosn":I
    .end local v3    # "nodes":Lorg/json/JSONArray;
    .end local v5    # "terminal":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;
    .end local v6    # "type":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    :catch_5
    move-exception v0

    .line 267
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 269
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v2    # "nodePosn":I
    .restart local v3    # "nodes":Lorg/json/JSONArray;
    :cond_2
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->tagResult:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    goto :goto_1

    .line 237
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
