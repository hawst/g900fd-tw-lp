.class public abstract Lcom/vlingo/sdk/internal/util/StringUtils;
.super Ljava/lang/Object;
.source "StringUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static arePhoneNumbersTheSame(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "n1"    # Ljava/lang/String;
    .param p1, "n2"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 140
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 150
    :goto_0
    return v0

    .line 142
    :cond_1
    const-string/jumbo v0, "+"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 144
    :cond_2
    const-string/jumbo v0, "+"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 145
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 146
    :cond_3
    const-string/jumbo v0, "1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 147
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 148
    :cond_4
    const-string/jumbo v0, "1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 149
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 150
    :cond_5
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static byteArrayToString([B)Ljava/lang/String;
    .locals 4
    .param p0, "arr"    # [B

    .prologue
    .line 109
    new-instance v2, Ljava/lang/StringBuffer;

    array-length v3, p0

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 110
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_0

    .line 111
    aget-byte v0, p0, v1

    .line 112
    .local v0, "b":B
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    .end local v0    # "b":B
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static cleanPhoneNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    .line 130
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 131
    .local v1, "cleaned":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 132
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 133
    .local v0, "c":C
    const/16 v3, 0x30

    if-lt v0, v3, :cond_0

    const/16 v3, 0x39

    if-gt v0, v3, :cond_0

    .line 134
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 131
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 136
    .end local v0    # "c":C
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static compareVersions(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p0, "version1"    # Ljava/lang/String;
    .param p1, "version2"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x2e

    const/4 v6, 0x0

    .line 305
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 319
    :cond_0
    :goto_0
    return v6

    .line 308
    :cond_1
    invoke-static {p0, v8}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v2

    .line 309
    .local v2, "v1":[Ljava/lang/String;
    invoke-static {p1, v8}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v3

    .line 311
    .local v3, "v2":[Ljava/lang/String;
    array-length v7, v2

    array-length v8, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 312
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 313
    const/4 v4, 0x0

    .local v4, "vc1":I
    const/4 v5, 0x0

    .line 314
    .local v5, "vc2":I
    array-length v7, v2

    if-ge v0, v7, :cond_2

    aget-object v7, v2, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 315
    :cond_2
    array-length v7, v3

    if-ge v0, v7, :cond_3

    aget-object v7, v3, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 316
    :cond_3
    if-le v4, v5, :cond_4

    const/4 v6, 0x1

    goto :goto_0

    .line 317
    :cond_4
    if-ge v4, v5, :cond_5

    const/4 v6, -0x1

    goto :goto_0

    .line 312
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static final convertBytesToString([B)Ljava/lang/String;
    .locals 3
    .param p0, "bytes"    # [B

    .prologue
    .line 38
    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    return-object v1

    .line 39
    :catch_0
    move-exception v0

    .line 40
    .local v0, "ex":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public static final convertStringToBytes(Ljava/lang/String;)[B
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 46
    :try_start_0
    const-string/jumbo v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 48
    :goto_0
    return-object v1

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "ex":Ljava/io/UnsupportedEncodingException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    .line 154
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->formatPhoneNumber(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatPhoneNumber(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 12
    .param p0, "number"    # Ljava/lang/String;
    .param p1, "showParens"    # Z

    .prologue
    const/4 v5, 0x1

    const/16 v11, 0xa

    const/4 v10, 0x6

    const/4 v6, 0x0

    const/4 v9, 0x3

    .line 159
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_2

    :cond_0
    const-string/jumbo p0, ""

    .line 231
    .end local p0    # "number":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p0

    .line 161
    .restart local p0    # "number":Ljava/lang/String;
    :cond_2
    invoke-static {p0}, Lcom/vlingo/sdk/internal/util/StringUtils;->cleanPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "c":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    .line 163
    .local v3, "clen":I
    if-nez v3, :cond_3

    const-string/jumbo p0, ""

    goto :goto_0

    .line 164
    :cond_3
    new-instance v2, Ljava/lang/StringBuffer;

    add-int/lit8 v7, v3, 0x10

    invoke-direct {v2, v7}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 165
    .local v2, "cleaned":Ljava/lang/StringBuffer;
    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x31

    if-ne v7, v8, :cond_5

    move v4, v5

    .line 166
    .local v4, "startsWithOne":Z
    :goto_1
    const-string/jumbo v7, "+"

    invoke-virtual {p0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 167
    const-string/jumbo v7, "+"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 169
    :cond_4
    if-eqz v4, :cond_f

    .line 170
    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 171
    add-int/lit8 v3, v3, -0x1

    .line 173
    if-lt v3, v9, :cond_8

    if-gt v3, v10, :cond_8

    .line 174
    if-eqz p1, :cond_6

    const-string/jumbo v5, "1 ("

    :goto_2
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 175
    invoke-virtual {v0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 176
    if-eqz p1, :cond_7

    const-string/jumbo v5, ") "

    :goto_3
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 177
    invoke-virtual {v0, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 178
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .end local v4    # "startsWithOne":Z
    :cond_5
    move v4, v6

    .line 165
    goto :goto_1

    .line 174
    .restart local v4    # "startsWithOne":Z
    :cond_6
    const-string/jumbo v5, "1-"

    goto :goto_2

    .line 176
    :cond_7
    const-string/jumbo v5, "-"

    goto :goto_3

    .line 180
    :cond_8
    const/4 v5, 0x7

    if-lt v3, v5, :cond_b

    if-gt v3, v11, :cond_b

    .line 181
    if-eqz p1, :cond_9

    const-string/jumbo v5, "1 ("

    :goto_4
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 182
    invoke-virtual {v0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 183
    if-eqz p1, :cond_a

    const-string/jumbo v5, ") "

    :goto_5
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 184
    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 185
    const-string/jumbo v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 186
    invoke-virtual {v0, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 187
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 181
    :cond_9
    const-string/jumbo v5, "1-"

    goto :goto_4

    .line 183
    :cond_a
    const-string/jumbo v5, "-"

    goto :goto_5

    .line 189
    :cond_b
    if-le v3, v11, :cond_1

    .line 190
    add-int/lit8 v1, v3, -0xa

    .line 191
    .local v1, "cidx":I
    if-eqz p1, :cond_c

    const-string/jumbo v5, "1 "

    :goto_6
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 192
    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 193
    if-eqz p1, :cond_d

    const-string/jumbo v5, " ("

    :goto_7
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 194
    add-int/lit8 v5, v1, 0x3

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 195
    if-eqz p1, :cond_e

    const-string/jumbo v5, ") "

    :goto_8
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 196
    add-int/lit8 v5, v1, 0x3

    add-int/lit8 v6, v1, 0x6

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 197
    const-string/jumbo v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 198
    add-int/lit8 v5, v1, 0x6

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 199
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 200
    goto/16 :goto_0

    .line 191
    :cond_c
    const-string/jumbo v5, "1-"

    goto :goto_6

    .line 193
    :cond_d
    const-string/jumbo v5, "-"

    goto :goto_7

    .line 195
    :cond_e
    const-string/jumbo v5, "-"

    goto :goto_8

    .line 203
    .end local v1    # "cidx":I
    :cond_f
    const/4 v5, 0x4

    if-lt v3, v5, :cond_10

    const/4 v5, 0x7

    if-gt v3, v5, :cond_10

    .line 204
    invoke-virtual {v0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 205
    const-string/jumbo v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 206
    invoke-virtual {v0, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 207
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 209
    :cond_10
    const/16 v5, 0x8

    if-lt v3, v5, :cond_13

    if-gt v3, v11, :cond_13

    .line 210
    if-eqz p1, :cond_11

    const-string/jumbo v5, "("

    :goto_9
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 211
    invoke-virtual {v0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 212
    if-eqz p1, :cond_12

    const-string/jumbo v5, ") "

    :goto_a
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 213
    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 214
    const-string/jumbo v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 215
    invoke-virtual {v0, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 216
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 210
    :cond_11
    const-string/jumbo v5, ""

    goto :goto_9

    .line 212
    :cond_12
    const-string/jumbo v5, "-"

    goto :goto_a

    .line 218
    :cond_13
    if-le v3, v11, :cond_1

    .line 219
    add-int/lit8 v1, v3, -0xa

    .line 220
    .restart local v1    # "cidx":I
    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 221
    if-eqz p1, :cond_14

    const-string/jumbo v5, " ("

    :goto_b
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 222
    add-int/lit8 v5, v1, 0x3

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 223
    if-eqz p1, :cond_15

    const-string/jumbo v5, ") "

    :goto_c
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 224
    add-int/lit8 v5, v1, 0x3

    add-int/lit8 v6, v1, 0x6

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 225
    const-string/jumbo v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 226
    add-int/lit8 v5, v1, 0x6

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 227
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 221
    :cond_14
    const-string/jumbo v5, "-"

    goto :goto_b

    .line 223
    :cond_15
    const-string/jumbo v5, "-"

    goto :goto_c
.end method

.method public static getHostname(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 238
    const-string/jumbo v2, "://"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 239
    .local v1, "slashPos":I
    if-ne v1, v4, :cond_0

    .line 240
    const-string/jumbo v2, ""

    .line 247
    :goto_0
    return-object v2

    .line 241
    :cond_0
    const-string/jumbo v2, "/"

    add-int/lit8 v3, v1, 0x3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 242
    .local v0, "endSlashPos":I
    if-ne v0, v4, :cond_1

    .line 243
    const-string/jumbo v2, ";"

    add-int/lit8 v3, v1, 0x3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 244
    if-ne v0, v4, :cond_1

    .line 245
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 247
    :cond_1
    add-int/lit8 v2, v1, 0x3

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getSubstring(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "startDelimiter"    # Ljava/lang/String;
    .param p2, "endDelimiter"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 387
    if-nez p0, :cond_1

    .line 406
    :cond_0
    :goto_0
    return-object v2

    .line 390
    :cond_1
    const/4 v1, 0x0

    .line 391
    .local v1, "startIndex":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 393
    .local v0, "endIndex":I
    if-eqz p1, :cond_2

    .line 394
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 395
    if-ltz v1, :cond_0

    .line 397
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    .line 400
    :cond_2
    if-eqz p2, :cond_3

    .line 401
    invoke-virtual {p0, p2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 402
    if-ltz v0, :cond_0

    .line 406
    :cond_3
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static final getWordAtCursor(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p0, "phrase"    # Ljava/lang/String;
    .param p1, "cursorPosition"    # I

    .prologue
    .line 21
    const-string/jumbo v1, ""

    .line 22
    .local v1, "word":Ljava/lang/String;
    if-ltz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge p1, v4, :cond_1

    .line 23
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x20

    invoke-static {v4, v5}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v3

    .line 24
    .local v3, "words":[Ljava/lang/String;
    const/4 v4, 0x0

    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    .line 25
    .local v0, "pos":I
    const/4 v2, 0x0

    .line 26
    .local v2, "wordindex":I
    :goto_0
    if-le p1, v0, :cond_0

    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_0

    .line 27
    add-int/lit8 v2, v2, 0x1

    aget-object v4, v3, v2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    .line 29
    :cond_0
    array-length v4, v3

    if-gt v2, v4, :cond_1

    .line 30
    aget-object v1, v3, v2

    .line 33
    .end local v0    # "pos":I
    .end local v2    # "wordindex":I
    .end local v3    # "words":[Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method public static isEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "s1"    # Ljava/lang/String;
    .param p1, "s2"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 377
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 378
    const/4 v0, 0x1

    .line 383
    :cond_0
    :goto_0
    return v0

    .line 379
    :cond_1
    if-eqz p0, :cond_2

    if-eqz p1, :cond_0

    .line 381
    :cond_2
    if-nez p0, :cond_3

    if-nez p1, :cond_0

    .line 383
    :cond_3
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isIPAddress(Ljava/lang/String;)Z
    .locals 13
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, -0x1

    const/16 v9, 0x2e

    const/4 v7, 0x0

    .line 331
    invoke-static {p0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ne v8, v10, :cond_1

    .line 373
    :cond_0
    :goto_0
    return v7

    .line 335
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 337
    const/4 v1, 0x0

    .local v1, "dotCount":I
    new-array v2, v12, [I

    .line 340
    .local v2, "dotPos":[I
    aput v10, v2, v7

    .line 341
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    aput v8, v2, v11

    .line 343
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v4, v8, :cond_4

    if-ge v1, v11, :cond_4

    .line 344
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 345
    .local v0, "ch":C
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v8

    if-nez v8, :cond_2

    if-ne v0, v9, :cond_0

    .line 347
    :cond_2
    if-ne v0, v9, :cond_3

    .line 348
    add-int/lit8 v1, v1, 0x1

    aput v4, v2, v1

    .line 343
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 352
    .end local v0    # "ch":C
    :cond_4
    const/4 v8, 0x3

    if-ne v1, v8, :cond_0

    .line 360
    const/4 v1, 0x1

    :goto_2
    if-ge v1, v12, :cond_5

    .line 361
    add-int/lit8 v8, v1, -0x1

    :try_start_0
    aget v8, v2, v8

    add-int/lit8 v4, v8, 0x1

    .line 362
    aget v8, v2, v1

    invoke-virtual {p0, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 363
    .local v5, "num":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 365
    .local v6, "number":I
    if-ltz v6, :cond_0

    const/16 v8, 0xff

    if-gt v6, v8, :cond_0

    .line 360
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 369
    .end local v5    # "num":Ljava/lang/String;
    .end local v6    # "number":I
    :catch_0
    move-exception v3

    .line 370
    .local v3, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 373
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_5
    const/4 v7, 0x1

    goto :goto_0
.end method

.method public static isNullOrWhiteSpace(Ljava/lang/String;)Z
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 64
    if-eqz p0, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    :cond_0
    const/4 v0, 0x1

    .line 67
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPhoneNumber(Ljava/lang/String;)Z
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 118
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 119
    .local v2, "len":I
    if-nez v2, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v3

    .line 121
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_4

    .line 122
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 123
    .local v0, "ch":C
    const/16 v4, 0x30

    if-lt v0, v4, :cond_2

    const/16 v4, 0x39

    if-le v0, v4, :cond_3

    :cond_2
    const/16 v4, 0x20

    if-eq v0, v4, :cond_3

    const/16 v4, 0x2d

    if-eq v0, v4, :cond_3

    const/16 v4, 0x2b

    if-eq v0, v4, :cond_3

    const/16 v4, 0x28

    if-eq v0, v4, :cond_3

    const/16 v4, 0x29

    if-ne v0, v4, :cond_0

    .line 121
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 126
    .end local v0    # "ch":C
    :cond_4
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static isVersionAtLeast(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "version"    # Ljava/lang/String;
    .param p1, "atLeast"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 294
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 297
    :cond_0
    :goto_0
    return v1

    .line 296
    :cond_1
    invoke-static {p0, p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->compareVersions(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 297
    .local v0, "res":I
    if-eqz v0, :cond_2

    if-ne v0, v2, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public static removeFromEnd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "strToRemove"    # Ljava/lang/String;

    .prologue
    .line 323
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 324
    invoke-virtual {p0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 326
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 327
    return-object p0
.end method

.method public static final removeTopChoice([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p0, "choices"    # [Ljava/lang/String;

    .prologue
    .line 53
    if-eqz p0, :cond_0

    array-length v3, p0

    if-nez v3, :cond_2

    .line 54
    :cond_0
    const/4 v1, 0x0

    .line 60
    :cond_1
    return-object v1

    .line 55
    :cond_2
    array-length v3, p0

    add-int/lit8 v2, v3, -0x1

    .line 56
    .local v2, "size":I
    new-array v1, v2, [Ljava/lang/String;

    .line 57
    .local v1, "ret":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 58
    add-int/lit8 v3, v0, 0x1

    aget-object v3, p0, v3

    aput-object v3, v1, v0

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "searchStr"    # Ljava/lang/String;
    .param p2, "replacementStr"    # Ljava/lang/String;

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 90
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 91
    .local v2, "searchStringPos":I
    const/4 v3, 0x0

    .line 92
    .local v3, "startPos":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 95
    .local v1, "searchStringLength":I
    :goto_0
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 96
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    add-int v3, v2, v1

    .line 98
    invoke-virtual {p0, p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    .line 102
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 104
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static replaceHostname(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "newHostname"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    .line 255
    const-string/jumbo v3, "://"

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 256
    .local v2, "slashPos":I
    if-ne v2, v5, :cond_0

    .line 257
    const-string/jumbo v3, ""

    .line 268
    :goto_0
    return-object v3

    .line 258
    :cond_0
    const-string/jumbo v3, "/"

    add-int/lit8 v4, v2, 0x3

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 259
    .local v1, "endSlashPos":I
    if-ne v1, v5, :cond_1

    .line 260
    const-string/jumbo v3, ";"

    add-int/lit8 v4, v2, 0x3

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 261
    if-ne v1, v5, :cond_1

    .line 262
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 265
    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 266
    .local v0, "buff":Ljava/lang/StringBuffer;
    add-int/lit8 v3, v2, 0x3

    invoke-virtual {v0, v3, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 267
    add-int/lit8 v3, v2, 0x3

    invoke-virtual {v0, v3, p1}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 268
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static split(Ljava/lang/String;C)[Ljava/lang/String;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "delimiter"    # C

    .prologue
    .line 275
    if-nez p0, :cond_0

    .line 276
    const/4 v0, 0x0

    .line 290
    :goto_0
    return-object v0

    .line 277
    :cond_0
    const/4 v1, 0x0

    .line 278
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 279
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, p1, :cond_1

    .line 280
    add-int/lit8 v1, v1, 0x1

    .line 278
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 282
    :cond_2
    add-int/lit8 v5, v1, 0x1

    new-array v0, v5, [Ljava/lang/String;

    .line 283
    .local v0, "arr":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 284
    .local v3, "index":I
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v1, :cond_3

    .line 285
    invoke-virtual {p0, p1, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 286
    .local v4, "pos":I
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    .line 287
    add-int/lit8 v3, v4, 0x1

    .line 284
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 289
    .end local v4    # "pos":I
    :cond_3
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v1

    goto :goto_0
.end method
