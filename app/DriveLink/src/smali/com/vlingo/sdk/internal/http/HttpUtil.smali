.class public Lcom/vlingo/sdk/internal/http/HttpUtil;
.super Ljava/lang/Object;
.source "HttpUtil.java"


# static fields
.field public static CR:Ljava/lang/String; = null

.field public static CRLF:Ljava/lang/String; = null

.field public static CRLF_BYTES:[B = null

.field public static CR_CHAR:C = '\u0000'

.field public static final HEADER_CONNECTION:Ljava/lang/String; = "Connection"

.field public static final HEADER_CONTENT_LENGTH:Ljava/lang/String; = "Content-Length"

.field public static final HEADER_CONTENT_TYPE:Ljava/lang/String; = "Content-type"

.field public static final HEADER_COOKIE:Ljava/lang/String; = "Cookie"

.field public static final HEADER_SET_COOKIE:Ljava/lang/String; = "Set-Cookie"

.field public static final HEADER_STATUS:Ljava/lang/String; = "STATUS"

.field public static final HEADER_TRANSFER_ENCODING:Ljava/lang/String; = "Transfer-Encoding"

.field public static final HEADER_USER_AGENT:Ljava/lang/String; = "User-Agent"

.field public static LF:Ljava/lang/String; = null

.field public static LF_CHAR:C = '\u0000'

.field public static final METHOD_GET:Ljava/lang/String; = "GET"

.field public static final METHOD_HEAD:Ljava/lang/String; = "HEAD"

.field public static final METHOD_POST:Ljava/lang/String; = "POST"

.field private static final TAG:Ljava/lang/String;

.field public static final VAL_CHUNKED:Ljava/lang/String; = "chunked"

.field public static final VAL_CLOSE:Ljava/lang/String; = "Close"

.field public static final VAL_KEEP_ALIVE:Ljava/lang/String; = "keep-alive"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const/16 v0, 0xa

    sput-char v0, Lcom/vlingo/sdk/internal/http/HttpUtil;->LF_CHAR:C

    .line 35
    const/16 v0, 0xd

    sput-char v0, Lcom/vlingo/sdk/internal/http/HttpUtil;->CR_CHAR:C

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lcom/vlingo/sdk/internal/http/HttpUtil;->CR_CHAR:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/HttpUtil;->CR:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lcom/vlingo/sdk/internal/http/HttpUtil;->LF_CHAR:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/HttpUtil;->LF:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lcom/vlingo/sdk/internal/http/HttpUtil;->CR_CHAR:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lcom/vlingo/sdk/internal/http/HttpUtil;->LF_CHAR:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/HttpUtil;->CRLF:Ljava/lang/String;

    .line 39
    sget-object v0, Lcom/vlingo/sdk/internal/http/HttpUtil;->CRLF:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/HttpUtil;->CRLF_BYTES:[B

    .line 60
    const-class v0, Lcom/vlingo/sdk/internal/http/HttpUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/HttpUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static byteArrayToString([B)Ljava/lang/String;
    .locals 3
    .param p0, "arr"    # [B

    .prologue
    .line 270
    if-nez p0, :cond_0

    .line 271
    :try_start_0
    const-string/jumbo v1, "null"

    .line 278
    :goto_0
    return-object v1

    .line 272
    :cond_0
    array-length v1, p0

    if-nez v1, :cond_1

    .line 273
    const-string/jumbo v1, ""

    goto :goto_0

    .line 275
    :cond_1
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 277
    :catch_0
    move-exception v0

    .line 278
    .local v0, "uee":Ljava/io/UnsupportedEncodingException;
    const-string/jumbo v1, "unsupported encoding: UTF-8"

    goto :goto_0
.end method

.method public static bytesToString([BII)Ljava/lang/String;
    .locals 5
    .param p0, "buff"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 284
    add-int v0, p1, p2

    .line 285
    .local v0, "end":I
    const-string/jumbo v2, ""

    .line 286
    .local v2, "str":Ljava/lang/String;
    move v1, p1

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 287
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 288
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 290
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-byte v4, p0, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 286
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 292
    :cond_1
    return-object v2
.end method

.method public static genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\" "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCacheControlHeaders(Lcom/vlingo/sdk/internal/net/HttpConnection;)Ljava/util/Hashtable;
    .locals 11
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/net/HttpConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/net/HttpConnection;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 435
    new-instance v5, Ljava/util/Hashtable;

    invoke-direct {v5}, Ljava/util/Hashtable;-><init>()V

    .line 436
    .local v5, "result":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v7, "cache-control"

    invoke-static {v7, p0}, Lcom/vlingo/sdk/internal/http/HttpUtil;->getHttpResponseHeader(Ljava/lang/String;Lcom/vlingo/sdk/internal/net/HttpConnection;)Ljava/lang/String;

    move-result-object v1

    .line 437
    .local v1, "header":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 439
    const-string/jumbo v7, ","

    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-eq v7, v9, :cond_0

    .line 440
    const/16 v7, 0x2c

    invoke-static {v1, v7}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v4

    .line 443
    .local v4, "parts":[Ljava/lang/String;
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v7, v4

    if-ge v2, v7, :cond_2

    .line 446
    const-string/jumbo v7, "="

    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 447
    .local v0, "eqPos":I
    if-eq v0, v9, :cond_1

    .line 448
    invoke-virtual {v1, v10, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 449
    .local v3, "key":Ljava/lang/String;
    add-int/lit8 v7, v0, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v8, v0

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 455
    .local v6, "value":Ljava/lang/String;
    :goto_2
    invoke-virtual {v5, v3, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 442
    .end local v0    # "eqPos":I
    .end local v2    # "i":I
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "parts":[Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    :cond_0
    const/4 v7, 0x1

    new-array v4, v7, [Ljava/lang/String;

    aput-object v1, v4, v10

    .restart local v4    # "parts":[Ljava/lang/String;
    goto :goto_0

    .line 452
    .restart local v0    # "eqPos":I
    .restart local v2    # "i":I
    :cond_1
    move-object v3, v1

    .line 453
    .restart local v3    # "key":Ljava/lang/String;
    const-string/jumbo v6, "1"

    .restart local v6    # "value":Ljava/lang/String;
    goto :goto_2

    .line 458
    .end local v0    # "eqPos":I
    .end local v2    # "i":I
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "parts":[Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    :cond_2
    return-object v5
.end method

.method public static getCookies(Ljava/util/Hashtable;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<**>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 300
    .local p0, "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    invoke-virtual {p0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 301
    .local v1, "iter":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 302
    .local v0, "buffer":Ljava/lang/StringBuffer;
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 303
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 304
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 305
    .local v3, "value":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 306
    const-string/jumbo v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 308
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 310
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static getDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 462
    const/4 v1, 0x0

    .line 464
    .local v1, "theURL":Ljava/net/URL;
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "theURL":Ljava/net/URL;
    .local v2, "theURL":Ljava/net/URL;
    move-object v1, v2

    .line 468
    .end local v2    # "theURL":Ljava/net/URL;
    .restart local v1    # "theURL":Ljava/net/URL;
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    :goto_1
    return-object v3

    .line 465
    :catch_0
    move-exception v0

    .line 466
    .local v0, "e":Ljava/net/MalformedURLException;
    sget-object v3, Lcom/vlingo/sdk/internal/http/HttpUtil;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 468
    .end local v0    # "e":Ljava/net/MalformedURLException;
    :cond_0
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static getHttpResponseHeader(Ljava/lang/String;Lcom/vlingo/sdk/internal/net/HttpConnection;)Ljava/lang/String;
    .locals 4
    .param p0, "headerName"    # Ljava/lang/String;
    .param p1, "conn"    # Lcom/vlingo/sdk/internal/net/HttpConnection;

    .prologue
    .line 417
    const-string/jumbo v1, ""

    .line 418
    .local v1, "key":Ljava/lang/String;
    const/4 v0, 0x0

    .line 420
    .local v0, "i":I
    :goto_0
    :try_start_0
    invoke-interface {p1, v0}, Lcom/vlingo/sdk/internal/net/HttpConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 421
    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 422
    invoke-interface {p1, v0}, Lcom/vlingo/sdk/internal/net/HttpConnection;->getHeaderField(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 423
    .local v2, "value":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 428
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 430
    :catch_0
    move-exception v3

    .line 431
    :cond_1
    return-object v1
.end method

.method public static getPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 472
    const/4 v1, 0x0

    .line 474
    .local v1, "theURL":Ljava/net/URL;
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "theURL":Ljava/net/URL;
    .local v2, "theURL":Ljava/net/URL;
    move-object v1, v2

    .line 478
    .end local v2    # "theURL":Ljava/net/URL;
    .restart local v1    # "theURL":Ljava/net/URL;
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v3

    :goto_1
    return-object v3

    .line 475
    :catch_0
    move-exception v0

    .line 476
    .local v0, "e":Ljava/net/MalformedURLException;
    sget-object v3, Lcom/vlingo/sdk/internal/http/HttpUtil;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 478
    .end local v0    # "e":Ljava/net/MalformedURLException;
    :cond_0
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static httpStatusCodeToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "statusCode"    # I

    .prologue
    .line 372
    sparse-switch p0, :sswitch_data_0

    .line 410
    const-string/jumbo v0, "Unknown"

    .line 413
    .local v0, "statusMessage":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 374
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_0
    const-string/jumbo v0, "OK"

    .line 375
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 377
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_1
    const-string/jumbo v0, "Moved Permanently"

    .line 378
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 380
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_2
    const-string/jumbo v0, "Found"

    .line 381
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 383
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_3
    const-string/jumbo v0, "Not Modified"

    .line 384
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 386
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_4
    const-string/jumbo v0, "Bad Request"

    .line 387
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 389
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_5
    const-string/jumbo v0, "Not Authorized"

    .line 390
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 392
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_6
    const-string/jumbo v0, "Forbidden"

    .line 393
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 395
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_7
    const-string/jumbo v0, "Not Found"

    .line 396
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 398
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_8
    const-string/jumbo v0, "Method Not Allowed"

    .line 399
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 401
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_9
    const-string/jumbo v0, "Internal Server Error"

    .line 402
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 404
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_a
    const-string/jumbo v0, "Bad Gateway"

    .line 405
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 407
    .end local v0    # "statusMessage":Ljava/lang/String;
    :sswitch_b
    const-string/jumbo v0, "Service Unavailable"

    .line 408
    .restart local v0    # "statusMessage":Ljava/lang/String;
    goto :goto_0

    .line 372
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x12d -> :sswitch_1
        0x12e -> :sswitch_2
        0x130 -> :sswitch_3
        0x190 -> :sswitch_4
        0x191 -> :sswitch_5
        0x193 -> :sswitch_6
        0x194 -> :sswitch_7
        0x195 -> :sswitch_8
        0x1f4 -> :sswitch_9
        0x1f6 -> :sswitch_a
        0x1f7 -> :sswitch_b
    .end sparse-switch
.end method

.method public static newHttpConnection(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Hashtable;Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)Lcom/vlingo/sdk/internal/net/HttpConnection;
    .locals 6
    .param p0, "connectionProvider"    # Lcom/vlingo/sdk/internal/net/ConnectionProvider;
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "keepalive"    # Z
    .param p6, "client"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p7, "software"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/net/ConnectionProvider;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Hashtable",
            "<**>;",
            "Ljava/util/Hashtable",
            "<**>;",
            "Lcom/vlingo/sdk/internal/recognizer/ClientMeta;",
            "Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;",
            ")",
            "Lcom/vlingo/sdk/internal/net/HttpConnection;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p4, "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    .local p5, "extraHeaders":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    const/4 v5, 0x1

    .line 327
    const/4 v2, 0x0

    .line 332
    .local v2, "hc":Lcom/vlingo/sdk/internal/net/HttpConnection;
    const-string/jumbo v4, "GET"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 333
    const/4 v4, 0x0

    invoke-virtual {p0, p2, v4, v5}, Lcom/vlingo/sdk/internal/net/ConnectionProvider;->getConnection(Ljava/lang/String;IZ)Lcom/vlingo/sdk/internal/net/Connection;

    move-result-object v2

    .end local v2    # "hc":Lcom/vlingo/sdk/internal/net/HttpConnection;
    check-cast v2, Lcom/vlingo/sdk/internal/net/HttpConnection;

    .line 339
    .restart local v2    # "hc":Lcom/vlingo/sdk/internal/net/HttpConnection;
    :goto_0
    invoke-interface {v2, p1}, Lcom/vlingo/sdk/internal/net/HttpConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 342
    if-eqz p3, :cond_0

    .line 343
    const-string/jumbo v4, "Connection"

    const-string/jumbo v5, "keep-alive"

    invoke-interface {v2, v4, v5}, Lcom/vlingo/sdk/internal/net/HttpConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :cond_0
    if-eqz p5, :cond_2

    .line 349
    invoke-virtual {p5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 350
    .local v1, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 351
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 354
    .local v3, "key":Ljava/lang/String;
    invoke-virtual {p5, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/vlingo/sdk/internal/net/HttpConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 335
    .end local v1    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    .end local v3    # "key":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, p2, v5, v5}, Lcom/vlingo/sdk/internal/net/ConnectionProvider;->getConnection(Ljava/lang/String;IZ)Lcom/vlingo/sdk/internal/net/Connection;

    move-result-object v2

    .end local v2    # "hc":Lcom/vlingo/sdk/internal/net/HttpConnection;
    check-cast v2, Lcom/vlingo/sdk/internal/net/HttpConnection;

    .restart local v2    # "hc":Lcom/vlingo/sdk/internal/net/HttpConnection;
    goto :goto_0

    .line 358
    :cond_2
    if-eqz p4, :cond_3

    .line 359
    invoke-static {p4}, Lcom/vlingo/sdk/internal/http/HttpUtil;->getCookies(Ljava/util/Hashtable;)Ljava/lang/String;

    move-result-object v0

    .line 360
    .local v0, "cookieStr":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 363
    const-string/jumbo v4, "Cookie"

    invoke-interface {v2, v4, v0}, Lcom/vlingo/sdk/internal/net/HttpConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    .end local v0    # "cookieStr":Ljava/lang/String;
    :cond_3
    return-object v2
.end method

.method public static newHttpConnection(Ljava/lang/String;Ljava/lang/String;ZLjava/util/Hashtable;Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)Lcom/vlingo/sdk/internal/net/HttpConnection;
    .locals 8
    .param p0, "method"    # Ljava/lang/String;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "keepalive"    # Z
    .param p5, "client"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p6, "software"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Hashtable",
            "<**>;",
            "Ljava/util/Hashtable",
            "<**>;",
            "Lcom/vlingo/sdk/internal/recognizer/ClientMeta;",
            "Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;",
            ")",
            "Lcom/vlingo/sdk/internal/net/HttpConnection;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 318
    .local p3, "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    .local p4, "extraHeaders":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getInstance()Lcom/vlingo/sdk/internal/net/ConnectionManager;

    move-result-object v0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-static/range {v0 .. v7}, Lcom/vlingo/sdk/internal/http/HttpUtil;->newHttpConnection(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Hashtable;Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)Lcom/vlingo/sdk/internal/net/HttpConnection;

    move-result-object v0

    return-object v0
.end method

.method public static println(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 2
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "line"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    if-nez p0, :cond_0

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "out is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 74
    sget-object v0, Lcom/vlingo/sdk/internal/http/HttpUtil;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 75
    return-void
.end method

.method public static readData(Ljava/io/InputStream;)[B
    .locals 5
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 109
    .local v0, "boas":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    .end local v0    # "boas":Ljava/io/ByteArrayOutputStream;
    .local v1, "boas":Ljava/io/ByteArrayOutputStream;
    :try_start_1
    invoke-static {p0, v1}, Lcom/vlingo/sdk/internal/http/HttpUtil;->transfer(Ljava/io/InputStream;Ljava/io/ByteArrayOutputStream;)V

    .line 111
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 115
    .local v2, "data":[B
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 112
    :cond_0
    :goto_0
    return-object v2

    .line 115
    .end local v1    # "boas":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "data":[B
    .restart local v0    # "boas":Ljava/io/ByteArrayOutputStream;
    :catchall_0
    move-exception v3

    :goto_1
    if-eqz v0, :cond_1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    :goto_2
    throw v3

    :catch_0
    move-exception v4

    goto :goto_2

    .end local v0    # "boas":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "boas":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "data":[B
    :catch_1
    move-exception v3

    goto :goto_0

    .end local v2    # "data":[B
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "boas":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "boas":Ljava/io/ByteArrayOutputStream;
    goto :goto_1
.end method

.method public static readLine(Ljava/io/DataInputStream;)Ljava/lang/String;
    .locals 4
    .param p0, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, Ljava/io/DataInputStream;->read()I

    move-result v1

    .line 88
    .local v1, "it":I
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 89
    .local v0, "bout":Ljava/io/ByteArrayOutputStream;
    :goto_0
    if-ltz v1, :cond_0

    .line 90
    sget-char v3, Lcom/vlingo/sdk/internal/http/HttpUtil;->LF_CHAR:C

    if-ne v1, v3, :cond_1

    .line 96
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 97
    const/4 v2, 0x0

    .line 100
    :goto_1
    return-object v2

    .line 93
    :cond_1
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 94
    invoke-virtual {p0}, Ljava/io/DataInputStream;->read()I

    move-result v1

    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v2

    .line 100
    .local v2, "line":Ljava/lang/String;
    goto :goto_1
.end method

.method public static transfer(Ljava/io/InputStream;Ljava/io/ByteArrayOutputStream;)V
    .locals 7
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "out"    # Ljava/io/ByteArrayOutputStream;

    .prologue
    const/16 v6, 0x400

    .line 135
    const/16 v1, 0x400

    .line 136
    .local v1, "chunkSize":I
    new-array v3, v6, [B

    .line 137
    .local v3, "inBuff":[B
    const/4 v0, -0x1

    .line 141
    .local v0, "bytesRead":I
    const/4 v2, 0x1

    .line 142
    .local v2, "dataRemaining":Z
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v4

    .line 143
    .local v4, "numToRead":I
    :cond_0
    :goto_0
    if-eqz v2, :cond_4

    .line 145
    if-gtz v4, :cond_1

    .line 146
    const/4 v4, 0x1

    .line 148
    :cond_1
    if-le v4, v6, :cond_2

    .line 149
    const/16 v4, 0x400

    .line 152
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {p0, v3, v5, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 153
    if-gez v0, :cond_3

    .line 154
    const/4 v2, 0x0

    goto :goto_0

    .line 157
    :cond_3
    const/4 v5, 0x0

    invoke-virtual {p1, v3, v5, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 158
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v4

    .line 159
    if-nez v4, :cond_0

    .line 160
    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 164
    .end local v4    # "numToRead":I
    :catch_0
    move-exception v5

    .line 165
    :cond_4
    return-void
.end method

.method public static writeCRLF(Ljava/io/OutputStream;)V
    .locals 1
    .param p0, "dout"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    const-string/jumbo v0, ""

    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/http/HttpUtil;->println(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 83
    return-void
.end method
