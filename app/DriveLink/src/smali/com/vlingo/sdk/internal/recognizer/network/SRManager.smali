.class public abstract Lcom/vlingo/sdk/internal/recognizer/network/SRManager;
.super Ljava/lang/Object;
.source "SRManager.java"


# static fields
.field public static final DEFAULT_TIMEOUT:I = -0x1

.field public static final USE_ASR:Z = true


# instance fields
.field private mConnectTimeout:I

.field private mReadTimeout:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->mConnectTimeout:I

    .line 109
    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->mReadTimeout:I

    return-void
.end method

.method public static create(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;)Lcom/vlingo/sdk/internal/recognizer/network/SRManager;
    .locals 3
    .param p0, "connectionProvider"    # Lcom/vlingo/sdk/internal/net/ConnectionProvider;
    .param p1, "timings"    # Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .prologue
    .line 34
    const-string/jumbo v1, "asr.manager"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 37
    .local v0, "useAsrManager":Z
    if-eqz v0, :cond_0

    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;-><init>(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;)V

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;-><init>(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;)V

    goto :goto_0
.end method

.method public static isAsrManager()Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public abstract destroy()V
.end method

.method public getConnectTimeout()I
    .locals 2

    .prologue
    .line 85
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->mConnectTimeout:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getOptimalConnectTimeout()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->mConnectTimeout:I

    goto :goto_0
.end method

.method public abstract getLastGuttID()Ljava/lang/String;
.end method

.method public getReadTimeout()I
    .locals 2

    .prologue
    .line 89
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->mReadTimeout:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getOptimalNetworkTimeout()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->mReadTimeout:I

    goto :goto_0
.end method

.method public abstract init(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V
.end method

.method protected isAcceptedTextEnabled()Z
    .locals 2

    .prologue
    .line 101
    const-string/jumbo v0, "acceptedtext.enable"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected isStatsEnabled()Z
    .locals 2

    .prologue
    .line 105
    const-string/jumbo v0, "stats.enable"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public abstract newRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
.end method

.method public abstract newRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;Z)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
.end method

.method public abstract readyForRecognition()Z
.end method

.method public abstract sendStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)V
.end method

.method public abstract sendStatsCollection(Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V
.end method

.method public setConnectTimeout(I)V
    .locals 0
    .param p1, "timeout"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->mConnectTimeout:I

    .line 94
    return-void
.end method

.method public setReadTimeout(I)V
    .locals 0
    .param p1, "timeout"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->mReadTimeout:I

    .line 98
    return-void
.end method

.method public abstract setServer(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;)V
.end method
