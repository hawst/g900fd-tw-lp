.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$ParameterTagBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "ParameterTag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ParameterTagBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$1;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$ParameterTagBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addParametersNames(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "paramNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "language"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    const-string/jumbo v0, "value"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method protected buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
    .locals 3

    .prologue
    .line 47
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;-><init>()V

    .line 48
    .local v0, "result":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$ParameterTagBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "language"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->language:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;Ljava/lang/String;)Ljava/lang/String;

    .line 49
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$ParameterTagBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "name"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->name:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;Ljava/lang/String;)Ljava/lang/String;

    .line 50
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$ParameterTagBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "value"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->value:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;Ljava/lang/String;)Ljava/lang/String;

    .line 51
    return-object v0
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$ParameterTagBuilder;->buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$ParameterTagBuilder;->parametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 57
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const-string/jumbo v0, "Param"

    return-object v0
.end method
