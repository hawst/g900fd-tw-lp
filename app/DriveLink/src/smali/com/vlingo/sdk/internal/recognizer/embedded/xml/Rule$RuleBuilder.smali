.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "Rule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RuleBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
        ">;"
    }
.end annotation


# instance fields
.field private actions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;",
            ">;"
        }
    .end annotation
.end field

.field private tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->tags:Ljava/util/List;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->actions:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$1;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addChildTagObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p1, "childName"    # Ljava/lang/String;
    .param p2, "tagObject"    # Ljava/lang/Object;

    .prologue
    .line 104
    const-string/jumbo v2, "Tag"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    instance-of v2, p2, Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 105
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->tags:Ljava/util/List;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "tagObject":Ljava/lang/Object;
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 106
    .restart local p2    # "tagObject":Ljava/lang/Object;
    :cond_1
    const-string/jumbo v2, "ActionList"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    :try_start_0
    move-object v0, p2

    check-cast v0, Ljava/util/List;

    move-object v1, v0

    .line 109
    .local v1, "actionListChild":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;>;"
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->actions:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 110
    .end local v1    # "actionListChild":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;>;"
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected addChildrenBuilders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "builders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;>;"
    const-string/jumbo v0, "ActionList"

    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$1;

    invoke-direct {v1, p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$1;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;)V

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    const-string/jumbo v0, "Tag"

    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$2;

    invoke-direct {v1, p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$2;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;)V

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    return-void
.end method

.method protected buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;-><init>()V

    .line 47
    .local v0, "rule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->tags:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->tagNames:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;Ljava/util/List;)Ljava/util/List;

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->actions:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->actionsList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;Ljava/util/List;)Ljava/util/List;

    .line 49
    return-object v0
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->tags:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 55
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->actions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 56
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string/jumbo v0, "ResultBlock"

    return-object v0
.end method
