.class public Lcom/vlingo/sdk/internal/recognizer/Recognizer;
.super Ljava/lang/Object;
.source "Recognizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;,
        Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;,
        Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;,
        Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;,
        Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;
    }
.end annotation


# static fields
.field static final MID_CONFIDENCE_TIMEOUT:I = 0xbb8

.field private static final MIN_AUDIO_DURATION:I = 0x1f4


# instance fields
.field private mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

.field private mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

.field private mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

.field private mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

.field private final mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

.field private mIsStoppedDataReader:Z

.field private mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

.field private mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    sget-object v0, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    .line 73
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;-><init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;)V

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;-><init>(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    .line 74
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->doesEmbeddedRecognizerSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;

    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer$EmbeddedListener;-><init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;)V

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    goto :goto_0
.end method

.method static synthetic access$1000(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->cleanup()V

    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    return-object v0
.end method

.method static synthetic access$502(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mIsStoppedDataReader:Z

    return p1
.end method

.method static synthetic access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/vlingo/sdk/recognition/RecognitionMode;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    return-void
.end method

.method private checkRecognizerInternalState()V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    if-nez v0, :cond_1

    sget-object v0, Lcom/vlingo/sdk/recognition/RecognitionMode;->EMBEDDED:Lcom/vlingo/sdk/recognition/RecognitionMode;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/recognition/RecognitionMode;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/vlingo/sdk/recognition/RecognitionMode;->HYBRID:Lcom/vlingo/sdk/recognition/RecognitionMode;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/recognition/RecognitionMode;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Embedded recognizer is disabled. Could not handle "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " recognition mode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_1
    return-void
.end method

.method private cleanup()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 290
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onCancelled()V

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onCancelled()V

    .line 297
    :cond_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    if-eqz v0, :cond_2

    .line 298
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->stop()V

    .line 299
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->writeLog()V

    .line 300
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    .line 302
    :cond_2
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    .line 303
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .line 304
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    .line 305
    return-void
.end method

.method private handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    .locals 1
    .param p1, "recError"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "mode"    # Lcom/vlingo/sdk/recognition/RecognitionMode;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 324
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->cleanup()V

    .line 325
    return-void
.end method

.method private prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z
    .locals 6
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "sendAudio"    # Z
    .param p3, "listener"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .prologue
    .line 241
    if-nez p3, :cond_0

    .line 242
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "listener is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 243
    :cond_0
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    if-eqz v3, :cond_1

    .line 244
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "Recognizer is busy"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 246
    :cond_1
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .line 249
    const/4 v2, 0x1

    .line 251
    .local v2, "success":Z
    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 283
    :goto_0
    return v2

    .line 253
    :pswitch_0
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    if-eqz v3, :cond_2

    .line 254
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    invoke-interface {v3, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z

    move-result v2

    goto :goto_0

    .line 256
    :cond_2
    const/4 v2, 0x0

    .line 258
    goto :goto_0

    .line 260
    :pswitch_1
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    if-eqz v3, :cond_3

    .line 261
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v3, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z

    move-result v2

    goto :goto_0

    .line 263
    :cond_3
    const/4 v2, 0x0

    .line 265
    goto :goto_0

    .line 267
    :pswitch_2
    new-instance v3, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    new-instance v4, Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/vlingo/sdk/internal/recognizer/Recognizer$HybridArbiterListener;-><init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;)V

    const/16 v5, 0xbb8

    invoke-direct {v3, v4, v5}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;-><init>(Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;I)V

    iput-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    .line 268
    const/4 v1, 0x1

    .line 269
    .local v1, "networkSuccess":Z
    const/4 v0, 0x1

    .line 270
    .local v0, "embeddedSuccess":Z
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    if-eqz v3, :cond_5

    .line 271
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v3, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z

    move-result v1

    .line 275
    :goto_1
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    if-eqz v3, :cond_6

    .line 276
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    invoke-interface {v3, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z

    move-result v0

    .line 280
    :goto_2
    if-nez v1, :cond_4

    if-eqz v0, :cond_7

    :cond_4
    const/4 v2, 0x1

    :goto_3
    goto :goto_0

    .line 273
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 278
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 280
    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    .line 251
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private startAudioRecognition(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V
    .locals 3
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    .param p3, "dataReadyListener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    .prologue
    const/4 v2, 0x0

    .line 104
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    :goto_0
    return-void

    .line 108
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;-><init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    .line 109
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    invoke-static {p1, v0, p3}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->getDataReader(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    .line 110
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->EMBEDDED:Lcom/vlingo/sdk/recognition/RecognitionMode;

    if-eq v0, v1, :cond_1

    .line 111
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v1}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->getTimingRepository()Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->setTimings(Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;)V

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->init()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->start()V

    goto :goto_0

    .line 116
    :cond_2
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->READER_ERROR:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v1, "Error initializing reader"

    invoke-direct {p0, v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    goto :goto_0
.end method

.method private startTextRecognition(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V
    .locals 2
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->EMBEDDED:Lcom/vlingo/sdk/recognition/RecognitionMode;

    if-ne v0, v1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingStarted()V

    .line 130
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->THINKING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleStateChange(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized cancel()V
    .locals 1

    .prologue
    .line 209
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->cleanup()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    monitor-exit p0

    return-void

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized destroy()V
    .locals 1

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->cleanup()V

    .line 224
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->destroy()V

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->destroy()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    :cond_1
    monitor-exit p0

    return-void

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getIsStoppedDataReader()Z
    .locals 1

    .prologue
    .line 570
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mIsStoppedDataReader:Z

    return v0
.end method

.method protected handleStateChange(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V
    .locals 1
    .param p1, "state"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognizerStateChanged(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V

    .line 311
    :cond_0
    return-void
.end method

.method public declared-synchronized sendAcceptedText(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V
    .locals 1
    .param p1, "gUttId"    # Ljava/lang/String;
    .param p2, "collection"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;

    .prologue
    .line 216
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->sendAcceptedText(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    monitor-exit p0

    return-void

    .line 216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startRecognition(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    .locals 1
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    .param p3, "DataReadyListener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    .param p4, "mode"    # Lcom/vlingo/sdk/recognition/RecognitionMode;

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iput-object p4, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    .line 94
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->checkRecognizerInternalState()V

    .line 96
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->isString()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->startTextRecognition(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :goto_0
    monitor-exit p0

    return-void

    .line 99
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->startAudioRecognition(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startSendEvent(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V
    .locals 2
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .prologue
    .line 140
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->checkRecognizerInternalState()V

    .line 141
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 165
    :goto_0
    monitor-exit p0

    return-void

    .line 144
    :cond_0
    :try_start_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 164
    :cond_1
    :goto_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->THINKING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleStateChange(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 146
    :pswitch_0
    :try_start_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingStarted()V

    goto :goto_1

    .line 151
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingStarted()V

    goto :goto_1

    .line 156
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingStarted()V

    .line 159
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingStarted()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized stop()V
    .locals 2

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->checkRecognizerInternalState()V

    .line 176
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 196
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->stop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :cond_1
    monitor-exit p0

    return-void

    .line 178
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingEnpointed()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 183
    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingEnpointed()V

    goto :goto_0

    .line 188
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 189
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingEnpointed()V

    .line 191
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingEnpointed()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 176
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
