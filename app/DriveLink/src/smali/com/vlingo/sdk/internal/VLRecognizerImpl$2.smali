.class Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;
.super Ljava/lang/Object;
.source "VLRecognizerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/VLRecognizerImpl;->sendEvent(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

.field final synthetic val$context:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;)V
    .locals 1

    .prologue
    .line 241
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->val$context:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$200(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getDestroyLock()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 248
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    monitor-exit v1

    .line 258
    :goto_0
    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v0

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->GETTING_READY:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyEvent(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    .line 252
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->doesUseEDM()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->val$context:Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    # invokes: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->sendEventToEDM(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;)V
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$700(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;)V

    .line 257
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 255
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    # invokes: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->sendEventToRecognizer(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$800(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
