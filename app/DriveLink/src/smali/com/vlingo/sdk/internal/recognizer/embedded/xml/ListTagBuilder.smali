.class public abstract Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "ListTagBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder<TT;>;"
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addChildTagObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "childName"    # Ljava/lang/String;
    .param p2, "tagObject"    # Ljava/lang/Object;

    .prologue
    .line 41
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;->getChildTagName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 44
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected addChildrenBuilders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder<TT;>;"
    .local p1, "builders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;>;"
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;->getChildTagName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;->getChildBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    return-void
.end method

.method public bridge synthetic addParameter(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;

    .prologue
    .line 14
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder<TT;>;"
    invoke-super {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;->buildResultObject()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected buildResultObject()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder<TT;>;"
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;->items:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 21
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder<TT;>;"
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 22
    return-void
.end method

.method protected abstract getChildBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected abstract getChildTagName()Ljava/lang/String;
.end method

.method public bridge synthetic getParamNames()Ljava/util/List;
    .locals 1

    .prologue
    .line 14
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;, "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder<TT;>;"
    invoke-super {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;->getParamNames()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
