.class public final enum Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
.super Ljava/lang/Enum;
.source "HypothesisNodeType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

.field public static final enum TAG:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

.field public static final enum TERMINAL:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

.field public static final enum UNKNOWN:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    const-string/jumbo v1, "TERMINAL"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->TERMINAL:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    .line 16
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    const-string/jumbo v1, "TAG"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->TAG:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    .line 17
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    const-string/jumbo v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->UNKNOWN:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->TERMINAL:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->TAG:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->UNKNOWN:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getFromString(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->TERMINAL:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->TERMINAL:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    .line 25
    :goto_0
    return-object v0

    .line 22
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->TAG:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->TAG:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    goto :goto_0

    .line 25
    :cond_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->UNKNOWN:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    return-object v0
.end method
