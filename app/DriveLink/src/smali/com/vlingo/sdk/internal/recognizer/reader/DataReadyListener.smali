.class public Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
.super Ljava/lang/Object;
.source "DataReadyListener.java"


# instance fields
.field private isDataReady:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;->isDataReady:Z

    return-void
.end method


# virtual methods
.method public isDataReady()Z
    .locals 1

    .prologue
    .line 10
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;->isDataReady:Z

    return v0
.end method

.method public onDataNotReady()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;->isDataReady:Z

    return-void
.end method

.method public onDataReady()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;->isDataReady:Z

    return-void
.end method
