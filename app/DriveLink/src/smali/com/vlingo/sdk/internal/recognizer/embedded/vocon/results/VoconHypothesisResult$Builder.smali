.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
.super Ljava/lang/Object;
.source "VoconHypothesisResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private hypothesisJson:Lorg/json/JSONObject;

.field private voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "hypothesis"    # Lorg/json/JSONObject;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    .line 89
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->hypothesisJson:Lorg/json/JSONObject;

    .line 90
    return-void
.end method

.method private addResult(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    .locals 2
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_items:Ljava/util/List;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->access$700(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_items:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->access$702(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/util/List;)Ljava/util/List;

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_items:Ljava/util/List;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->access$700(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    return-object p0
.end method

.method private beginTimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    .locals 2
    .param p1, "beginTime"    # J

    .prologue
    .line 140
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_beginTimeMs:Ljava/lang/Long;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->access$402(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/Long;)Ljava/lang/Long;

    .line 141
    return-object p0
.end method

.method private confidence(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    .locals 2
    .param p1, "confidence"    # J

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_conf:Ljava/lang/Long;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/Long;)Ljava/lang/Long;

    .line 115
    return-object p0
.end method

.method private endTimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    .locals 2
    .param p1, "endTime"    # J

    .prologue
    .line 153
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_endTimeMs:Ljava/lang/Long;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->access$502(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/Long;)Ljava/lang/Long;

    .line 154
    return-object p0
.end method

.method private lmScore(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    .locals 2
    .param p1, "lmScore"    # J

    .prologue
    .line 166
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_lmScore:Ljava/lang/Long;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->access$602(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/Long;)Ljava/lang/Long;

    .line 167
    return-object p0
.end method

.method private score(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    .locals 2
    .param p1, "score"    # J

    .prologue
    .line 127
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_score:Ljava/lang/Long;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/Long;)Ljava/lang/Long;

    .line 128
    return-object p0
.end method

.method private startRule(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    .locals 1
    .param p1, "startRule"    # Ljava/lang/String;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->_startRule:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Ljava/lang/String;)Ljava/lang/String;

    .line 102
    return-object p0
.end method


# virtual methods
.method public build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 196
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->hypothesisJson:Lorg/json/JSONObject;

    if-eqz v8, :cond_0

    .line 199
    :try_start_0
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->hypothesisJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_startRule"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->startRule(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :try_start_1
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->hypothesisJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_conf"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->confidence(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 215
    :try_start_2
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->hypothesisJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_score"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->score(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 223
    :try_start_3
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->hypothesisJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_beginTimeMs"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->beginTimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    .line 231
    :try_start_4
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->hypothesisJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_endTimeMs"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->endTimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    .line 239
    :try_start_5
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->hypothesisJson:Lorg/json/JSONObject;

    const-string/jumbo v9, "_items"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 240
    .local v2, "nodes":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "tagPosn":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v4, v8, :cond_2

    .line 241
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 242
    .local v1, "node":Lorg/json/JSONObject;
    const-string/jumbo v8, "_type"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->getFromString(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;

    move-result-object v6

    .line 244
    .local v6, "type":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    sget-object v8, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$results$HypothesisNodeType:[I

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 277
    .end local v1    # "node":Lorg/json/JSONObject;
    .end local v2    # "nodes":Lorg/json/JSONArray;
    .end local v4    # "tagPosn":I
    .end local v6    # "type":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    :cond_0
    :goto_1
    return-object v7

    .line 200
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Lorg/json/JSONException;
    goto :goto_1

    .line 208
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 211
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 216
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v0

    .line 219
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 224
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_3
    move-exception v0

    .line 227
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 232
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_4
    move-exception v0

    .line 235
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 248
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "node":Lorg/json/JSONObject;
    .restart local v2    # "nodes":Lorg/json/JSONArray;
    .restart local v4    # "tagPosn":I
    .restart local v6    # "type":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    :pswitch_0
    new-instance v8, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;

    invoke-direct {v8, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult$Builder;->build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    move-result-object v3

    .line 249
    .local v3, "tag":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    if-eqz v3, :cond_1

    .line 251
    invoke-direct {p0, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->addResult(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;

    .line 240
    .end local v3    # "tag":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 257
    :pswitch_1
    new-instance v8, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;

    invoke-direct {v8, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    move-result-object v5

    .line 258
    .local v5, "terminal":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;
    if-eqz v5, :cond_1

    .line 260
    invoke-direct {p0, v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->addResult(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisNode;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_2

    .line 269
    .end local v1    # "node":Lorg/json/JSONObject;
    .end local v2    # "nodes":Lorg/json/JSONArray;
    .end local v4    # "tagPosn":I
    .end local v5    # "terminal":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;
    .end local v6    # "type":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/HypothesisNodeType;
    :catch_5
    move-exception v0

    .line 272
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 275
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v2    # "nodes":Lorg/json/JSONArray;
    .restart local v4    # "tagPosn":I
    :cond_2
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult$Builder;->voconHypothesis:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    goto :goto_1

    .line 244
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
