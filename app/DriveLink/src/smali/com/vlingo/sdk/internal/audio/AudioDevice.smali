.class public Lcom/vlingo/sdk/internal/audio/AudioDevice;
.super Ljava/lang/Object;
.source "AudioDevice.java"


# static fields
.field public static final BT_AMR_PADDING_MILLIS:I = 0x4b0

.field private static final CAR_MIC_IN_USE_DEVICE_NAME:Ljava/lang/String; = "MirrorLink"

.field private static final DEFAULT_AUDIO_DEVICE_NAME:Ljava/lang/String; = "Android"

.field private static final DEFAULT_BLUETOOTH_DEVICE_NAME:Ljava/lang/String; = "Unknown"

.field private static s_AudioDevice:Lcom/vlingo/sdk/internal/audio/AudioDevice;


# instance fields
.field private clientAudioValues:Lcom/vlingo/sdk/internal/audio/IClientAudioValues;

.field private volatile currentBluetoothHeadsetName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->s_AudioDevice:Lcom/vlingo/sdk/internal/audio/AudioDevice;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string/jumbo v0, "Unknown"

    iput-object v0, p0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->currentBluetoothHeadsetName:Ljava/lang/String;

    return-void
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->s_AudioDevice:Lcom/vlingo/sdk/internal/audio/AudioDevice;

    .line 45
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/sdk/internal/audio/AudioDevice;
    .locals 2

    .prologue
    .line 38
    const-class v1, Lcom/vlingo/sdk/internal/audio/AudioDevice;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->s_AudioDevice:Lcom/vlingo/sdk/internal/audio/AudioDevice;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/vlingo/sdk/internal/audio/AudioDevice;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/audio/AudioDevice;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->s_AudioDevice:Lcom/vlingo/sdk/internal/audio/AudioDevice;

    .line 40
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->s_AudioDevice:Lcom/vlingo/sdk/internal/audio/AudioDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getAudioDeviceName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->clientAudioValues:Lcom/vlingo/sdk/internal/audio/IClientAudioValues;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->clientAudioValues:Lcom/vlingo/sdk/internal/audio/IClientAudioValues;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/audio/IClientAudioValues;->isInCarMicInUse()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    const-string/jumbo v0, "MirrorLink"

    .line 57
    :goto_0
    return-object v0

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/audio/AudioDevice;->isAudioBluetooth()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Android/BT/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->currentBluetoothHeadsetName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 57
    :cond_1
    const-string/jumbo v0, "Android"

    goto :goto_0
.end method

.method public isAudioBluetooth()Z
    .locals 3

    .prologue
    .line 62
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 63
    .local v1, "context":Landroid/content/Context;
    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 64
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v2

    return v2
.end method

.method public isAudioHeadset()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public resetCurrentBluetoothDeviceName()V
    .locals 1

    .prologue
    .line 81
    const-string/jumbo v0, "Unknown"

    iput-object v0, p0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->currentBluetoothHeadsetName:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setClientAudioValues(Lcom/vlingo/sdk/internal/audio/IClientAudioValues;)V
    .locals 0
    .param p1, "clientAudioParam"    # Lcom/vlingo/sdk/internal/audio/IClientAudioValues;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->clientAudioValues:Lcom/vlingo/sdk/internal/audio/IClientAudioValues;

    .line 86
    return-void
.end method

.method public setCurrentBluetoothDeviceName(Landroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1, "bd"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 72
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/audio/AudioDevice;->resetCurrentBluetoothDeviceName()V

    .line 78
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "name":Ljava/lang/String;
    iput-object v0, p0, Lcom/vlingo/sdk/internal/audio/AudioDevice;->currentBluetoothHeadsetName:Ljava/lang/String;

    goto :goto_0
.end method
