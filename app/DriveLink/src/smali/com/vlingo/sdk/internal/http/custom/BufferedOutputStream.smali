.class public Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;
.super Ljava/io/OutputStream;
.source "BufferedOutputStream.java"


# instance fields
.field private buffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

.field private out:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "bufferSize"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;->out:Ljava/io/OutputStream;

    .line 30
    new-instance v0, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-direct {v0, p2}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;->buffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    .line 31
    return-void
.end method


# virtual methods
.method public declared-synchronized flush()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;->buffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->getByteArray()[B

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;->buffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->size()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 51
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 52
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;->buffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    monitor-exit p0

    return-void

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write(I)V
    .locals 1
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;->buffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->write(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    monitor-exit p0

    return-void

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([B)V
    .locals 3
    .param p1, "buff"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;->buffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    monitor-exit p0

    return-void

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([BII)V
    .locals 1
    .param p1, "buff"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;->buffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    monitor-exit p0

    return-void

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
