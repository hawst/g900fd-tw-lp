.class public Lcom/vlingo/sdk/internal/edm/EDMAction;
.super Ljava/lang/Object;
.source "EDMAction.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/VLAction;


# instance fields
.field private final elseCondition:Ljava/lang/String;

.field private final ifCondition:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final params:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/voicepad/model/VLActionResult;)V
    .locals 1
    .param p1, "from"    # Lcom/vlingo/voicepad/model/VLActionResult;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-interface {p1}, Lcom/vlingo/voicepad/model/VLActionResult;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->name:Ljava/lang/String;

    .line 19
    invoke-interface {p1}, Lcom/vlingo/voicepad/model/VLActionResult;->getIfClause()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->ifCondition:Ljava/lang/String;

    .line 20
    invoke-interface {p1}, Lcom/vlingo/voicepad/model/VLActionResult;->getElseClause()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->elseCondition:Ljava/lang/String;

    .line 21
    invoke-interface {p1}, Lcom/vlingo/voicepad/model/VLActionResult;->getParameters()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->params:Ljava/util/Map;

    .line 22
    return-void
.end method


# virtual methods
.method public getElseStatement()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->elseCondition:Ljava/lang/String;

    return-object v0
.end method

.method public getIfCondition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->ifCondition:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getParamValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "paramName"    # Ljava/lang/String;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->params:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getParameterNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->params:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public isConditional()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->ifCondition:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMAction;->elseCondition:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
