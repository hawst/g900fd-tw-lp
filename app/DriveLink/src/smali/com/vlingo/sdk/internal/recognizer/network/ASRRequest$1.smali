.class Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;
.super Ljava/util/TimerTask;
.source "ASRRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->startTimeoutTimer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, -0x3

    .line 280
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v2, "TimeoutTimer has expired!!"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 281
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    .line 282
    .local v0, "state":Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    instance-of v1, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;

    if-eqz v1, :cond_1

    .line 283
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/sdk/recognition/VLRecognizer;->getIsStoppedDataReader()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 284
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onTimeout(I)V
    invoke-static {v1, v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    .line 292
    :goto_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z
    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1002(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Z)Z

    .line 293
    return-void

    .line 286
    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/sdk/recognition/VLRecognizer;->stopRecognition()V

    goto :goto_0

    .line 289
    :cond_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onTimeout(I)V
    invoke-static {v1, v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    goto :goto_0
.end method
