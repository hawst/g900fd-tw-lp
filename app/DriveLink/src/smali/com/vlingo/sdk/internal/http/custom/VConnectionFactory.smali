.class public Lcom/vlingo/sdk/internal/http/custom/VConnectionFactory;
.super Ljava/lang/Object;
.source "VConnectionFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createConnection(Lcom/vlingo/sdk/internal/http/URL;)Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;
    .locals 1
    .param p0, "url"    # Lcom/vlingo/sdk/internal/http/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;-><init>(Lcom/vlingo/sdk/internal/http/URL;)V

    .line 25
    .local v0, "connection":Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;
    return-object v0
.end method

.method public static createConnection(Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;-><init>(Ljava/lang/String;)V

    .line 34
    .local v0, "connection":Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;
    return-object v0
.end method
