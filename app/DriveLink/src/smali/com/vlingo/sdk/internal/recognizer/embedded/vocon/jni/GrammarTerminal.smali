.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;
.super Ljava/lang/Object;
.source "GrammarTerminal.java"


# instance fields
.field private final _orthography:Ljava/lang/String;

.field private final _userID:J


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2
    .param p1, "orthography"    # Ljava/lang/String;
    .param p2, "userID"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const-wide v0, 0xffffffffL

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    .line 22
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "user ID must be between 0 and 4,294,967,295"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25
    :cond_1
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_orthography:Ljava/lang/String;

    .line 26
    iput-wide p2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_userID:J

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 47
    instance-of v1, p1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 48
    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;

    .line 49
    .local v0, "grammarTerminal":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;
    iget-wide v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_userID:J

    iget-wide v3, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_userID:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_orthography:Ljava/lang/String;

    iget-object v2, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_orthography:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 51
    .end local v0    # "grammarTerminal":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;
    :goto_0
    return v1

    .line 49
    .restart local v0    # "grammarTerminal":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 51
    .end local v0    # "grammarTerminal":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getOrthography()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_orthography:Ljava/lang/String;

    return-object v0
.end method

.method public getUserID()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_userID:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 56
    new-instance v0, Ljava/lang/Long;

    iget-wide v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_userID:J

    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_orthography:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;->_orthography:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
