.class public Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;
.super Ljava/lang/Object;
.source "SRResponseParser.java"


# instance fields
.field parser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->parser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    .line 27
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->parser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;

    invoke-direct {v1, p0}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;-><init>(Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->addParser(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;)V

    .line 28
    return-void
.end method


# virtual methods
.method public getResponse()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->parser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->getResponse()Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    return-object v0
.end method

.method public parseResponseXml(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    .locals 3
    .param p1, "responseXml"    # Ljava/lang/String;

    .prologue
    .line 42
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->parser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    invoke-direct {v2}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;-><init>()V

    invoke-virtual {v1, p1, v2}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->parseResponseXml(Ljava/lang/String;Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;)Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .line 44
    .local v0, "res":Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    return-object v0
.end method

.method public registerAttribute(Ljava/lang/String;)I
    .locals 1
    .param p1, "attrName"    # Ljava/lang/String;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->parser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public registerElement(Ljava/lang/String;)I
    .locals 1
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->parser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
