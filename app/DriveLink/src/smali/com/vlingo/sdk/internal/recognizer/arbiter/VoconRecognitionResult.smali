.class public Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;
.super Ljava/lang/Object;
.source "VoconRecognitionResult.java"


# instance fields
.field private confidenceScore:Ljava/lang/Long;

.field private embeddedActionsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;",
            ">;"
        }
    .end annotation
.end field

.field private result:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

.field private thresholdInfo:Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getConfidenceScore()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->confidenceScore:Ljava/lang/Long;

    return-object v0
.end method

.method public getEmbeddedActionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->embeddedActionsList:Ljava/util/List;

    return-object v0
.end method

.method public getResult()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->result:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    return-object v0
.end method

.method public getThresholdInfo()Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->thresholdInfo:Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    return-object v0
.end method

.method public setConfidenceScore(Ljava/lang/Long;)V
    .locals 0
    .param p1, "score"    # Ljava/lang/Long;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->confidenceScore:Ljava/lang/Long;

    .line 17
    return-void
.end method

.method public setEmbeddedActionsList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "embeddedActionsList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;>;"
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->embeddedActionsList:Ljava/util/List;

    .line 45
    return-void
.end method

.method public setResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 0
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->result:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .line 33
    return-void
.end method

.method public setThresholdInfo(Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->thresholdInfo:Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    .line 25
    return-void
.end method
