.class public final Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;
.super Lcom/vlingo/sdk/internal/VLSpotterImpl;
.source "VLSpotterSensoryImpl.java"


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 0
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLSpotterImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 24
    return-void
.end method


# virtual methods
.method declared-synchronized onDestroy()V
    .locals 1

    .prologue
    .line 34
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mIsStarted:Z

    .line 35
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->Deinitialize()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    monitor-exit p0

    return-void

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public phrasespotPipe(Ljava/nio/ByteBuffer;J)Ljava/lang/String;
    .locals 2
    .param p1, "b"    # Ljava/nio/ByteBuffer;
    .param p2, "rate"    # J

    .prologue
    .line 130
    const-class v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Something went wrong. This method should never be invoked, when using sensory 1.0"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const/4 v0, 0x0

    return-object v0
.end method

.method reinitSpotter(Ljava/lang/String;)Z
    .locals 20
    .param p1, "externalDir"    # Ljava/lang/String;

    .prologue
    .line 44
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->Deinitialize()V

    .line 45
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    .line 46
    .local v15, "context":Landroid/content/Context;
    const-string/jumbo v1, "vlsdk_raw"

    const/4 v4, 0x0

    invoke-virtual {v15, v1, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v18

    .line 48
    .local v18, "rawDir":Ljava/io/File;
    sget-object v1, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 49
    .local v14, "acousticModelFilename":Ljava/lang/String;
    if-nez v14, :cond_1

    .line 50
    const-string/jumbo v1, "VLG_EXCEPTION"

    const-string/jumbo v4, "acousticModelFilename == null"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const/16 v19, 0x0

    .line 104
    :cond_0
    :goto_0
    return v19

    .line 53
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "acousticModelPathname":Ljava/lang/String;
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 55
    .local v16, "file":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 56
    const-string/jumbo v1, "VLG_EXCEPTION"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Could not find acoustic model file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/16 v19, 0x0

    goto :goto_0

    .line 60
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getGrammarSource()Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->isCompiledFileSource()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 62
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getGrammarSource()Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->getCompiledFilepath()Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->GRAMMAR_FORMALITY_DEFAULT:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getBeam()F

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v6}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getAbsBeam()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v7}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getAoffset()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v8}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getDelay()F

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v1 .. v10}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->Initialize(Ljava/lang/String;Ljava/lang/String;IFFFFLjava/lang/String;I)Z

    move-result v19

    .line 70
    .local v19, "ret":Z
    if-nez v19, :cond_0

    goto/16 :goto_0

    .line 76
    .end local v19    # "ret":Z
    :cond_3
    sget-object v1, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 77
    .local v17, "pronunModelFilename":Ljava/lang/String;
    if-nez v17, :cond_4

    .line 78
    const-string/jumbo v1, "VLG_EXCEPTION"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "No pronun model filename defined for language: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 81
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 82
    .local v3, "pronunModelPathname":Ljava/lang/String;
    new-instance v16, Ljava/io/File;

    .end local v16    # "file":Ljava/io/File;
    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 83
    .restart local v16    # "file":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_5

    .line 84
    const-string/jumbo v1, "VLG_EXCEPTION"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Could not find pronun model file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 89
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getGrammarSource()Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->getGrammarSpec()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getGrammarSource()Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->getWordList()[Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v6}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getGrammarSource()Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->getPronunciationList()[Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->GRAMMAR_FORMALITY_DEFAULT:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v8}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getBeam()F

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v9}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getAbsBeam()F

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v10}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getAoffset()F

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v11}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getDelay()F

    move-result v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v1 .. v13}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->InitializePhrases(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IFFFFLjava/lang/String;I)Z

    move-result v19

    .line 100
    .restart local v19    # "ret":Z
    if-nez v19, :cond_0

    goto/16 :goto_0
.end method

.method public startSpotter(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;Ljava/lang/String;ZZ)Z
    .locals 3
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;
    .param p2, "externalDir"    # Ljava/lang/String;
    .param p3, "isCarMode"    # Z
    .param p4, "isBluetoothOn"    # Z

    .prologue
    const/4 v1, 0x1

    .line 114
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->checkContext(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;)V

    .line 116
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    if-eq v2, p1, :cond_0

    .line 117
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    .line 118
    invoke-virtual {p0, p2}, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->reinitSpotter(Ljava/lang/String;)Z

    move-result v0

    .line 119
    .local v0, "success":Z
    if-nez v0, :cond_0

    .line 120
    const/4 v1, 0x0

    .line 125
    .end local v0    # "success":Z
    :goto_0
    return v1

    .line 123
    :cond_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mSensoryJNI:Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->MakeReady()Z

    .line 124
    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/VLSpotterSensoryImpl;->mIsStarted:Z

    goto :goto_0
.end method
