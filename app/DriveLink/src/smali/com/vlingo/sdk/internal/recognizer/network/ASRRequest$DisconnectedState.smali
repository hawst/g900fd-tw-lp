.class Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;
.super Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
.source "ASRRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DisconnectedState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 1

    .prologue
    .line 850
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;

    .prologue
    .line 850
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    return-void
.end method


# virtual methods
.method public onRun()V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 853
    const/4 v12, 0x0

    .line 854
    .local v12, "retry":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v14, "RUN"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 855
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v14, "OPEN"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 856
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v14, 0x1

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(I)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    .line 858
    const/4 v9, 0x1

    .line 860
    .local v9, "hadRecoverableErrors":Z
    :goto_0
    const/4 v13, 0x2

    if-ge v12, v13, :cond_6

    if-eqz v9, :cond_6

    .line 861
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v14, "connecting to asr service"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 864
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestManager()Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;
    invoke-static {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    invoke-static {v15}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->getConnection(Lcom/vlingo/sdk/internal/recognizer/SRContext;)Ljava/net/HttpURLConnection;

    move-result-object v14

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setConnection(Ljava/net/HttpURLConnection;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/net/HttpURLConnection;)V

    .line 879
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 880
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 881
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 883
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    invoke-virtual {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getReadTimeout()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 884
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    invoke-virtual {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnectTimeout()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 885
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v13

    const-string/jumbo v14, "X-vlrequest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "ClientRequestID:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    move-object/from16 v16, v0

    # operator++ for: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mRequestId:I
    invoke-static/range {v16 .. v16}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3108(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->isAsrKeepAliveEnabled()Z

    move-result v13

    if-nez v13, :cond_0

    .line 887
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v13

    const-string/jumbo v14, "Connection"

    const-string/jumbo v15, "CLOSE"

    invoke-virtual {v13, v14, v15}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    :cond_0
    new-instance v10, Ljava/util/Hashtable;

    invoke-direct {v10}, Ljava/util/Hashtable;-><init>()V

    .line 891
    .local v10, "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Ljava/util/Hashtable;

    invoke-direct {v3}, Ljava/util/Hashtable;-><init>()V

    .line 893
    .local v3, "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getClientData()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getSoftwareData()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    invoke-static {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    invoke-static {v15}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v15

    invoke-static {v10, v13, v14, v15}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->addStandardVlingoHttpHeaders(Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Lcom/vlingo/sdk/internal/recognizer/SRContext;)V

    .line 894
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v13

    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v13

    invoke-virtual {v13}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v14

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v14

    invoke-virtual {v14}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v3, v13, v14}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->addVLServiceCookies(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Hashtable;

    .line 896
    invoke-virtual {v10}, Ljava/util/Hashtable;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 897
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v15

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v15, v13, v14}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 900
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    invoke-static {v3}, Lcom/vlingo/sdk/internal/http/HttpUtil;->getCookies(Ljava/util/Hashtable;)Ljava/lang/String;

    move-result-object v2

    .line 901
    .local v2, "cookieStr":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_2

    .line 902
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v13

    const-string/jumbo v14, "Cookie"

    invoke-virtual {v13, v14, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 905
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v13

    const/16 v14, 0x200

    invoke-virtual {v13, v14}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 907
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v14, "HDRS"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 909
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "connecting to "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v14

    invoke-static {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$Connection;->describe(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->log(Ljava/lang/String;)V

    .line 913
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    new-instance v14, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    new-instance v15, Ljava/io/DataOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    move-object/from16 v16, v0

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static/range {v16 .. v16}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const-string/jumbo v16, "-------------------------------1878979834"

    invoke-direct/range {v14 .. v16}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;-><init>(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setOutputStream(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    .line 914
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeBoundary()V

    .line 917
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getEvents()Ljava/util/List;

    move-result-object v8

    .line 918
    .local v8, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;>;"
    if-eqz v8, :cond_3

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v13

    if-lez v13, :cond_3

    .line 919
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->buildEventElement()Ljava/lang/String;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/lang/String;

    move-result-object v7

    .line 921
    .local v7, "event":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v13

    const-string/jumbo v14, "events"

    const-string/jumbo v15, "text/xml"

    invoke-static {v7}, Lcom/vlingo/sdk/internal/util/StringUtils;->convertStringToBytes(Ljava/lang/String;)[B

    move-result-object v16

    const/16 v17, 0x1

    invoke-virtual/range {v13 .. v17}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeDataField(Ljava/lang/String;Ljava/lang/String;[BZ)V

    .line 922
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 923
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v14, "DMEV"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 927
    .end local v7    # "event":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDialogState()[B

    move-result-object v4

    .line 928
    .local v4, "data":[B
    if-eqz v4, :cond_4

    array-length v13, v4

    if-lez v13, :cond_4

    .line 929
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v13

    const-string/jumbo v14, "dialog-data"

    const-string/jumbo v15, "binary"

    invoke-virtual {v13, v14, v15, v4}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeDataField(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 930
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 931
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v14, "DMST"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 934
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v1

    .line 935
    .local v1, "asi":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->isString()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 937
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCRC32:Ljava/util/zip/CRC32;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/zip/CRC32;

    move-result-object v13

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getText()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/zip/CRC32;->update([B)V

    .line 938
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v14

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertMetaTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    .line 939
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v14

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertTextTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    .line 940
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v14

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertCRCTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    .line 948
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v14, "connected"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 949
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 943
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->open(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->setFlushOnWrite(Z)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 963
    .end local v1    # "asi":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .end local v4    # "data":[B
    .end local v8    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;>;"
    :catch_0
    move-exception v5

    .line 964
    .local v5, "e":Ljava/net/SocketTimeoutException;
    const-string/jumbo v13, "connection TIMED OUT"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->log(Ljava/lang/String;)V

    .line 965
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v14, -0x3

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onTimeout(I)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    .line 966
    const/4 v9, 0x0

    .line 972
    goto/16 :goto_0

    .line 967
    .end local v5    # "e":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v5

    .line 968
    .local v5, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "connection FAILED: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 969
    const/4 v9, 0x1

    .line 970
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCRC32:Ljava/util/zip/CRC32;
    invoke-static {v13}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/zip/CRC32;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/zip/CRC32;->reset()V

    .line 971
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 974
    .end local v2    # "cookieStr":Ljava/lang/String;
    .end local v3    # "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "e":Ljava/io/IOException;
    .end local v10    # "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    :cond_6
    if-eqz v9, :cond_7

    .line 975
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v14, -0x1

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onError(I)V
    invoke-static {v13, v14}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    .line 977
    :cond_7
    return-void
.end method
