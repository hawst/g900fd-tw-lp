.class public Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;
.super Ljava/lang/Object;
.source "Slot.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$SlotBuilder;
    }
.end annotation


# static fields
.field private static final CONTEXT_PARAM_NAME:Ljava/lang/String; = "context"

.field private static final SLOT_PARAM_NAME:Ljava/lang/String; = "slot"

.field public static final SLOT_TAG_NAME:Ljava/lang/String; = "Slot"


# instance fields
.field private context:Ljava/lang/String;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 8
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;->name:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 8
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;->context:Ljava/lang/String;

    return-object p1
.end method

.method public static getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$SlotBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$SlotBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$1;)V

    return-object v0
.end method


# virtual methods
.method public getContext()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;->context:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;->name:Ljava/lang/String;

    return-object v0
.end method
