.class Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$2;
.super Ljava/lang/Object;
.source "VoconRecoTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->onExecutionFailed(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$2;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$2;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mListener:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->access$100(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->READER_ERROR:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v2, "Can\'t record.  Please try again."

    invoke-interface {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;->onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 267
    return-void
.end method
