.class public Lcom/vlingo/sdk/internal/vlservice/AndroidVLServiceCookieManager;
.super Ljava/lang/Object;
.source "AndroidVLServiceCookieManager.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;


# instance fields
.field private persistentCookieJar:Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;

    const-string/jumbo v1, "cookie_data"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/AndroidVLServiceCookieManager;->persistentCookieJar:Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;

    .line 32
    return-void
.end method


# virtual methods
.method public addAllCookiesToHashtable(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "cookies"    # Ljava/util/Hashtable;
    .param p2, "domain"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 38
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/AndroidVLServiceCookieManager;->persistentCookieJar:Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;->clearExpired()Z

    .line 39
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/AndroidVLServiceCookieManager;->persistentCookieJar:Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;->getCookies()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 40
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/sdk/internal/http/cookies/Cookie;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 41
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/http/cookies/Cookie;

    .line 44
    .local v0, "c":Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    invoke-interface {v0, p2, p3}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->isMatch(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    invoke-interface {v0}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 54
    .end local v0    # "c":Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    :cond_1
    return-void
.end method

.method public addCookie(Lcom/vlingo/sdk/internal/http/cookies/Cookie;)V
    .locals 2
    .param p1, "c"    # Lcom/vlingo/sdk/internal/http/cookies/Cookie;

    .prologue
    .line 80
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "??? Is this really an un-used method ???"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public createCookie(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 69
    new-instance v0, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;

    invoke-direct {v0, p1, p2}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getCookieValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cookieName"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/AndroidVLServiceCookieManager;->persistentCookieJar:Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;

    invoke-virtual {v1, p1}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;->getCookieByName(Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/cookies/Cookie;

    move-result-object v0

    .line 61
    .local v0, "c":Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    if-eqz v0, :cond_0

    .line 62
    invoke-interface {v0}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 63
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public mergeCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V
    .locals 1
    .param p1, "newCookies"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/AndroidVLServiceCookieManager;->persistentCookieJar:Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;->mergeCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V

    .line 76
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/AndroidVLServiceCookieManager;->persistentCookieJar:Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;->save()V

    .line 77
    return-void
.end method

.method public removeCookie(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 92
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "??? Is this really an un-used method ???"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public resetCookies()V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "??? Is this really an un-used method ???"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public save()V
    .locals 2

    .prologue
    .line 88
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "??? Is this really an un-used method ???"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
