.class public abstract Lcom/vlingo/sdk/internal/util/UrlUtils;
.super Ljava/lang/Object;
.source "UrlUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static extractValueForKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "urlKey"    # Ljava/lang/String;
    .param p1, "urlString"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 26
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 27
    .local v1, "valueIndex":I
    if-eq v1, v4, :cond_1

    .line 28
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    .line 29
    const-string/jumbo v2, "&"

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 30
    .local v0, "valueEndIndex":I
    if-ne v0, v4, :cond_0

    .line 32
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 34
    :cond_0
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/sdk/internal/util/UrlUtils;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 36
    .end local v0    # "valueEndIndex":I
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static urlDecode(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "decodeStr"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x2b

    const/16 v7, 0x20

    .line 53
    if-nez p0, :cond_0

    .line 54
    const/4 v5, 0x0

    .line 82
    :goto_0
    return-object v5

    .line 57
    :cond_0
    const/4 v4, 0x0

    .line 59
    .local v4, "less":I
    const/4 v0, 0x0

    .line 61
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    if-ge v3, v5, :cond_3

    .line 62
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 64
    .local v2, "c":C
    const/16 v5, 0x25

    if-ne v2, v5, :cond_2

    .line 66
    add-int/lit8 v5, v3, 0x1

    add-int/lit8 v6, v3, 0x3

    :try_start_0
    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    int-to-char v2, v5

    .line 68
    if-nez v0, :cond_1

    .line 69
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .end local v0    # "buf":Ljava/lang/StringBuffer;
    .local v1, "buf":Ljava/lang/StringBuffer;
    move-object v0, v1

    .line 71
    .end local v1    # "buf":Ljava/lang/StringBuffer;
    .restart local v0    # "buf":Ljava/lang/StringBuffer;
    :cond_1
    sub-int v5, v3, v4

    sub-int v6, v3, v4

    add-int/lit8 v6, v6, 0x3

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 72
    sub-int v5, v3, v4

    invoke-virtual {v0, v5, v2}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    add-int/lit8 v4, v4, 0x2

    .line 61
    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 79
    .end local v2    # "c":C
    :cond_3
    if-nez v0, :cond_4

    .line 80
    invoke-virtual {p0, v8, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 82
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v8, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 74
    .restart local v2    # "c":C
    :catch_0
    move-exception v5

    goto :goto_2
.end method

.method public static urlEncode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 40
    if-eqz p0, :cond_0

    .line 42
    :try_start_0
    const-string/jumbo v0, "UTF-8"

    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/util/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 48
    :cond_0
    :goto_0
    return-object p0

    .line 44
    :catch_0
    move-exception v0

    goto :goto_0
.end method
