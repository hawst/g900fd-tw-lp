.class public Lcom/vlingo/sdk/internal/recognizer/SRContext;
.super Ljava/lang/Object;
.source "SRContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/SRContext$1;
    }
.end annotation


# instance fields
.field protected volatile audioFormatChannelConfig:I

.field protected volatile audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

.field protected volatile autoEndpointing:Z

.field protected volatile autoPunctuation:Z

.field protected volatile capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

.field protected volatile curText:Ljava/lang/String;

.field protected volatile cursorPos:I

.field protected volatile custom6:Ljava/lang/String;

.field public volatile customFlag:Z

.field protected volatile dialogGUID:Ljava/lang/String;

.field protected volatile dialogState:[B

.field protected volatile dialogTurnNumber:I

.field protected volatile dmHeaderKVPairs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected volatile eventList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;",
            ">;"
        }
    .end annotation
.end field

.field protected volatile fieldContext:Ljava/lang/String;

.field protected volatile fieldID:Ljava/lang/String;

.field protected volatile fieldType:Ljava/lang/String;

.field protected volatile isDMRequest:Z

.field protected volatile maxAudioTime:I

.field protected volatile minVoiceDuration:F

.field protected volatile minVoiceLevel:F

.field protected volatile noSpeechEndpointTimeout:I

.field protected volatile profanityFilter:Z

.field protected volatile silenceThreshold:F

.field protected volatile speechEndpointTimeout:I

.field protected volatile speexComplexity:I

.field protected volatile speexQuality:I

.field protected volatile speexVariableBitrate:I

.field protected volatile speexVoiceActivityDetection:I

.field protected volatile username:Ljava/lang/String;

.field protected volatile voicePortion:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldID:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldType:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->curText:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldContext:Ljava/lang/String;

    .line 43
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->cursorPos:I

    .line 44
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->autoPunctuation:Z

    .line 45
    sget-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;->DEFAULT:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .line 46
    const v0, 0x9c40

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->maxAudioTime:I

    .line 47
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->autoEndpointing:Z

    .line 48
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_SPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speechEndpointTimeout:I

    .line 49
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_NOSPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->noSpeechEndpointTimeout:I

    .line 50
    const/high16 v0, 0x41300000    # 11.0f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->silenceThreshold:F

    .line 51
    const v0, 0x3da3d70a    # 0.08f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->minVoiceDuration:F

    .line 52
    const v0, 0x3ca3d70a    # 0.02f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->voicePortion:F

    .line 53
    const/high16 v0, 0x42640000    # 57.0f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->minVoiceLevel:F

    .line 54
    const/4 v0, 0x3

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexComplexity:I

    .line 55
    const/16 v0, 0x8

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexQuality:I

    .line 56
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexVariableBitrate:I

    .line 57
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexVoiceActivityDetection:I

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->profanityFilter:Z

    .line 60
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->customFlag:Z

    .line 61
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->custom6:Ljava/lang/String;

    .line 62
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogState:[B

    .line 63
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->eventList:Ljava/util/List;

    .line 64
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->isDMRequest:Z

    .line 65
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->username:Ljava/lang/String;

    .line 66
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogGUID:Ljava/lang/String;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogTurnNumber:I

    .line 69
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->audioFormatChannelConfig:I

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "fieldID"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldID:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldType:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->curText:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldContext:Ljava/lang/String;

    .line 43
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->cursorPos:I

    .line 44
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->autoPunctuation:Z

    .line 45
    sget-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;->DEFAULT:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .line 46
    const v0, 0x9c40

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->maxAudioTime:I

    .line 47
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->autoEndpointing:Z

    .line 48
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_SPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speechEndpointTimeout:I

    .line 49
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_NOSPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->noSpeechEndpointTimeout:I

    .line 50
    const/high16 v0, 0x41300000    # 11.0f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->silenceThreshold:F

    .line 51
    const v0, 0x3da3d70a    # 0.08f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->minVoiceDuration:F

    .line 52
    const v0, 0x3ca3d70a    # 0.02f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->voicePortion:F

    .line 53
    const/high16 v0, 0x42640000    # 57.0f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->minVoiceLevel:F

    .line 54
    const/4 v0, 0x3

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexComplexity:I

    .line 55
    const/16 v0, 0x8

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexQuality:I

    .line 56
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexVariableBitrate:I

    .line 57
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexVoiceActivityDetection:I

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->profanityFilter:Z

    .line 60
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->customFlag:Z

    .line 61
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->custom6:Ljava/lang/String;

    .line 62
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogState:[B

    .line 63
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->eventList:Ljava/util/List;

    .line 64
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->isDMRequest:Z

    .line 65
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->username:Ljava/lang/String;

    .line 66
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogGUID:Ljava/lang/String;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogTurnNumber:I

    .line 69
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->audioFormatChannelConfig:I

    .line 78
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldID:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "fieldID"    # Ljava/lang/String;
    .param p2, "fieldType"    # Ljava/lang/String;
    .param p3, "fieldContext"    # Ljava/lang/String;
    .param p4, "curText"    # Ljava/lang/String;
    .param p5, "cursorPos"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldID:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldType:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->curText:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldContext:Ljava/lang/String;

    .line 43
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->cursorPos:I

    .line 44
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->autoPunctuation:Z

    .line 45
    sget-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;->DEFAULT:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .line 46
    const v0, 0x9c40

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->maxAudioTime:I

    .line 47
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->autoEndpointing:Z

    .line 48
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_SPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speechEndpointTimeout:I

    .line 49
    sget-object v0, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->DEFAULT_NOSPEECH_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->noSpeechEndpointTimeout:I

    .line 50
    const/high16 v0, 0x41300000    # 11.0f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->silenceThreshold:F

    .line 51
    const v0, 0x3da3d70a    # 0.08f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->minVoiceDuration:F

    .line 52
    const v0, 0x3ca3d70a    # 0.02f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->voicePortion:F

    .line 53
    const/high16 v0, 0x42640000    # 57.0f

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->minVoiceLevel:F

    .line 54
    const/4 v0, 0x3

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexComplexity:I

    .line 55
    const/16 v0, 0x8

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexQuality:I

    .line 56
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexVariableBitrate:I

    .line 57
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexVoiceActivityDetection:I

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->profanityFilter:Z

    .line 60
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->customFlag:Z

    .line 61
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->custom6:Ljava/lang/String;

    .line 62
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogState:[B

    .line 63
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->eventList:Ljava/util/List;

    .line 64
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->isDMRequest:Z

    .line 65
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->username:Ljava/lang/String;

    .line 66
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogGUID:Ljava/lang/String;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogTurnNumber:I

    .line 69
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->audioFormatChannelConfig:I

    .line 82
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldID:Ljava/lang/String;

    .line 83
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldType:Ljava/lang/String;

    .line 84
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldContext:Ljava/lang/String;

    .line 85
    iput-object p4, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->curText:Ljava/lang/String;

    .line 86
    iput p5, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->cursorPos:I

    .line 87
    return-void
.end method


# virtual methods
.method public getAudioFormatChannelConfig()I
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->audioFormatChannelConfig:I

    return v0
.end method

.method public getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    return-object v0
.end method

.method public getAutoEndpointing()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->autoEndpointing:Z

    return v0
.end method

.method public getAutoPunctuation()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->autoPunctuation:Z

    return v0
.end method

.method public getCapitalization()Ljava/lang/String;
    .locals 2

    .prologue
    .line 134
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/SRContext$1;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionContext$CapitalizationMode:[I

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 138
    const-string/jumbo v0, "Default"

    :goto_0
    return-object v0

    .line 135
    :pswitch_0
    const-string/jumbo v0, "Off"

    goto :goto_0

    .line 136
    :pswitch_1
    const-string/jumbo v0, "Sentences"

    goto :goto_0

    .line 137
    :pswitch_2
    const-string/jumbo v0, "ProperNoun"

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCurrentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->curText:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 115
    const-string/jumbo v0, ""

    .line 116
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->curText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCursorPosition()Ljava/lang/String;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->curText:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 121
    const-string/jumbo v0, "0"

    .line 122
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->cursorPos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCursorPositionInt()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->cursorPos:I

    return v0
.end method

.method public getCustom6()Ljava/lang/String;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->custom6:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomParam(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 237
    const-string/jumbo v1, ""

    .line 238
    .local v1, "result":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getInstance()Lcom/vlingo/sdk/internal/net/ConnectionManager;

    move-result-object v0

    .line 239
    .local v0, "cm":Lcom/vlingo/sdk/internal/net/ConnectionManager;
    const-string/jumbo v2, "Custom1"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 240
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 241
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ConnType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 244
    :cond_0
    const-string/jumbo v2, "Custom2"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 245
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 246
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "wifi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 247
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "WifiLinkSpd="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getWifiLinkSpeed()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 252
    :cond_1
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 253
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "wifi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 254
    const-string/jumbo v2, "Custom3"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 255
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "NetworkType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 257
    :cond_2
    const-string/jumbo v2, "Custom4"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 258
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "CdmaSigLev="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getCdmaSignal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 260
    :cond_3
    const-string/jumbo v2, "Custom5"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 261
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "EvdoSigLev="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getEvdoSignal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 263
    :cond_4
    const-string/jumbo v2, "Custom6"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 264
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "GsmSigLev="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getGsmSignal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 268
    :cond_5
    return-object v1
.end method

.method public getDMHeaderKVPairs()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dmHeaderKVPairs:Ljava/util/HashMap;

    return-object v0
.end method

.method public getDialogGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogGUID:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogState()[B
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogState:[B

    return-object v0
.end method

.method public getDialogTurnNumber()I
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogTurnNumber:I

    return v0
.end method

.method public getEvents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->eventList:Ljava/util/List;

    return-object v0
.end method

.method public getFieldContext()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldContext:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 109
    const-string/jumbo v0, ""

    .line 110
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldContext:Ljava/lang/String;

    goto :goto_0
.end method

.method public getFieldID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldID:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 98
    const-string/jumbo v0, "<xml><taboofilter>"

    .line 99
    .local v0, "fType":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getProfanityFilter()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "on"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 103
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</taboofilter></xml>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    return-object v0

    .line 102
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "off"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMaxAudioTime()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->maxAudioTime:I

    return v0
.end method

.method public getMinVoiceDuration()F
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->minVoiceDuration:F

    return v0
.end method

.method public getMinVoiceLevel()F
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->minVoiceLevel:F

    return v0
.end method

.method public getNoSpeechEndpointTimeout()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->noSpeechEndpointTimeout:I

    return v0
.end method

.method public getProfanityFilter()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->profanityFilter:Z

    return v0
.end method

.method public getSilenceThreshold()F
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->silenceThreshold:F

    return v0
.end method

.method public getSpeechEndpointTimeout()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speechEndpointTimeout:I

    return v0
.end method

.method public getSpeexComplexity()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexComplexity:I

    return v0
.end method

.method public getSpeexQuality()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexQuality:I

    return v0
.end method

.method public getSpeexVariableBitrate()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexVariableBitrate:I

    return v0
.end method

.method public getSpeexVoiceActivityDetection()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexVoiceActivityDetection:I

    return v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->username:Ljava/lang/String;

    return-object v0
.end method

.method public getVoicePortion()F
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->voicePortion:F

    return v0
.end method

.method public isDMRequest()Z
    .locals 1

    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->isDMRequest:Z

    return v0
.end method

.method public logNetworkInfo()V
    .locals 0

    .prologue
    .line 234
    return-void
.end method

.method public setAudioFormatChannelConfig(I)V
    .locals 0
    .param p1, "audioFormatChannelConfig"    # I

    .prologue
    .line 409
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->audioFormatChannelConfig:I

    .line 410
    return-void
.end method

.method public setAudioSourceInfo(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .prologue
    .line 365
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 366
    return-void
.end method

.method public setAutoEndpointing(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 339
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->autoEndpointing:Z

    .line 340
    return-void
.end method

.method public setAutoPunctuation(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 327
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->autoPunctuation:Z

    .line 328
    return-void
.end method

.method public setCapitalizationMode(Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;)V
    .locals 0
    .param p1, "mode"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .line 332
    return-void
.end method

.method public setCurText(Ljava/lang/String;)V
    .locals 0
    .param p1, "curText"    # Ljava/lang/String;

    .prologue
    .line 315
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->curText:Ljava/lang/String;

    .line 316
    return-void
.end method

.method public setCursorPos(I)V
    .locals 0
    .param p1, "cursorPos"    # I

    .prologue
    .line 323
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->cursorPos:I

    .line 324
    return-void
.end method

.method public setCustom6(Ljava/lang/String;)V
    .locals 0
    .param p1, "custom6"    # Ljava/lang/String;

    .prologue
    .line 377
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->custom6:Ljava/lang/String;

    .line 378
    return-void
.end method

.method public setDMHeaderKVPairs(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397
    .local p1, "dmHeaderKVPairs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dmHeaderKVPairs:Ljava/util/HashMap;

    .line 398
    return-void
.end method

.method public setDialogGUID(Ljava/lang/String;)V
    .locals 0
    .param p1, "guid"    # Ljava/lang/String;

    .prologue
    .line 389
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogGUID:Ljava/lang/String;

    .line 390
    return-void
.end method

.method public setDialogState([B)V
    .locals 0
    .param p1, "data"    # [B

    .prologue
    .line 401
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogState:[B

    .line 402
    return-void
.end method

.method public setDialogTurnNumber(I)V
    .locals 0
    .param p1, "turn"    # I

    .prologue
    .line 393
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->dialogTurnNumber:I

    .line 394
    return-void
.end method

.method public setEvents(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 405
    .local p1, "eventList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;>;"
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->eventList:Ljava/util/List;

    .line 406
    return-void
.end method

.method public setFieldContext(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldContext"    # Ljava/lang/String;

    .prologue
    .line 319
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldContext:Ljava/lang/String;

    .line 320
    return-void
.end method

.method public setFieldID(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldID"    # Ljava/lang/String;

    .prologue
    .line 307
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldID:Ljava/lang/String;

    .line 308
    return-void
.end method

.method public setFieldType(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldType"    # Ljava/lang/String;

    .prologue
    .line 311
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->fieldType:Ljava/lang/String;

    .line 312
    return-void
.end method

.method public setIsDMRequest(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 381
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->isDMRequest:Z

    .line 382
    return-void
.end method

.method public setMaxAudioTime(I)V
    .locals 0
    .param p1, "time"    # I

    .prologue
    .line 335
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->maxAudioTime:I

    .line 336
    return-void
.end method

.method public setNoSpeechEndpointTimeout(I)V
    .locals 0
    .param p1, "ms"    # I

    .prologue
    .line 361
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->noSpeechEndpointTimeout:I

    .line 362
    return-void
.end method

.method public setProfanityFilter(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 369
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->profanityFilter:Z

    .line 370
    return-void
.end method

.method public setSilenceDetectionParams(FFFF)V
    .locals 0
    .param p1, "silenceThreshold"    # F
    .param p2, "minVoiceDuration"    # F
    .param p3, "voicePortion"    # F
    .param p4, "minVoiceLevel"    # F

    .prologue
    .line 343
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->silenceThreshold:F

    .line 344
    iput p2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->minVoiceDuration:F

    .line 345
    iput p3, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->voicePortion:F

    .line 346
    iput p4, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->minVoiceLevel:F

    .line 347
    return-void
.end method

.method public setSpeechEndpointTimeout(I)V
    .locals 0
    .param p1, "ms"    # I

    .prologue
    .line 357
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speechEndpointTimeout:I

    .line 358
    return-void
.end method

.method public setSpeexParams(IIII)V
    .locals 0
    .param p1, "speexComplexity"    # I
    .param p2, "speexQuality"    # I
    .param p3, "speexVariableBitrate"    # I
    .param p4, "speexVoiceActivityDetection"    # I

    .prologue
    .line 350
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexComplexity:I

    .line 351
    iput p2, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexQuality:I

    .line 352
    iput p3, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexVariableBitrate:I

    .line 353
    iput p4, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->speexVoiceActivityDetection:I

    .line 354
    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 385
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SRContext;->username:Ljava/lang/String;

    .line 386
    return-void
.end method
