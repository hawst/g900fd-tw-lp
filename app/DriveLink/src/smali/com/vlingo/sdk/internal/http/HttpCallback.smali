.class public interface abstract Lcom/vlingo/sdk/internal/http/HttpCallback;
.super Ljava/lang/Object;
.source "HttpCallback.java"


# virtual methods
.method public abstract onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
.end method

.method public abstract onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
.end method

.method public abstract onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
.end method

.method public abstract onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
.end method

.method public abstract onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
.end method
