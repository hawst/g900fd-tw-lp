.class Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ResultTimer;
.super Landroid/os/CountDownTimer;
.source "HybridArbiter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResultTimer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;JJ)V
    .locals 0
    .param p2, "millisInFuture"    # J
    .param p4, "countDownInterval"    # J

    .prologue
    .line 188
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ResultTimer;->this$0:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    .line 189
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 190
    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ResultTimer;->this$0:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->access$000(Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->FINISHED:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    if-eq v0, v1, :cond_0

    .line 203
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ResultTimer;->this$0:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->sendVoconResult()V
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->access$100(Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;)V

    .line 205
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 196
    return-void
.end method
