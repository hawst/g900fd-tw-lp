.class Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;
.source "VoconReleaseTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;Landroid/os/Handler;)V
    .locals 0
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;
    .param p2, "mainHandler"    # Landroid/os/Handler;

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;-><init>(Ljava/lang/Object;Landroid/os/Handler;)V

    .line 13
    return-void
.end method


# virtual methods
.method protected execute()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 18
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->uninit()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_1

    .line 24
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 19
    :catch_0
    move-exception v0

    .line 20
    .local v0, "e":Ljava/lang/IllegalStateException;
    goto :goto_0

    .line 21
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 22
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    goto :goto_0
.end method

.method protected onExecutionFailed(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;

    .prologue
    .line 34
    invoke-interface {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;->onVoconReleased()V

    .line 35
    return-void
.end method

.method protected bridge synthetic onExecutionFailed(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 7
    check-cast p1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask;->onExecutionFailed(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;)V

    return-void
.end method

.method protected onExecutionSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;

    .prologue
    .line 29
    invoke-interface {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;->onVoconReleased()V

    .line 30
    return-void
.end method

.method protected bridge synthetic onExecutionSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 7
    check-cast p1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask;->onExecutionSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;)V

    return-void
.end method
