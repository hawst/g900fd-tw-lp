.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;
.source "VoconRecoTask.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;",
        ">;",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;"
    }
.end annotation


# static fields
.field private static final GLOBAL_CANCEL_RULE_NAME:Ljava/lang/String; = "Cancel"

.field private static final GLOBAL_GRAMMAR_NAME:Ljava/lang/String; = "tizen"

.field private static final GLOBAL_START_RULE_NAME:Ljava/lang/String; = "Start"


# instance fields
.field private volatile allAudioReceived:Z

.field private final audioQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<[S>;"
        }
    .end annotation
.end field

.field private volatile mCancelRequested:Z

.field private final mListener:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;

.field private recognitionResponse:Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

.field private final srContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;Landroid/os/Handler;Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;)V
    .locals 1
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;
    .param p2, "mainHandler"    # Landroid/os/Handler;
    .param p3, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p4, "listener"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;-><init>(Ljava/lang/Object;Landroid/os/Handler;)V

    .line 44
    iput-object p4, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mListener:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;

    .line 45
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->audioQueue:Ljava/util/Queue;

    .line 46
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->srContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;)Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->recognitionResponse:Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mListener:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;

    return-object v0
.end method

.method private prepareForRecognition()Z
    .locals 15

    .prologue
    .line 148
    :try_start_0
    iget-object v12, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->srContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    if-nez v12, :cond_0

    .line 151
    const/4 v12, 0x0

    .line 237
    :goto_0
    return v12

    .line 154
    :cond_0
    iget-object v12, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->srContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v12}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldID()Ljava/lang/String;

    move-result-object v2

    .line 156
    .local v2, "fieldId":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->initialize()V

    .line 157
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;

    move-result-object v12

    invoke-virtual {v12, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->getGrammarRuleSet(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;

    move-result-object v11

    .line 158
    .local v11, "ruleSet":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;
    if-nez v11, :cond_1

    .line 161
    const/4 v12, 0x0

    goto :goto_0

    .line 164
    :cond_1
    invoke-virtual {v11}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;->getGrammarList()Ljava/util/List;

    move-result-object v5

    .line 165
    .local v5, "grammarRules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;>;"
    if-eqz v5, :cond_a

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_a

    .line 168
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v12

    invoke-interface {v12}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->deactivateAllGrammarRules()Z

    move-result v12

    if-nez v12, :cond_2

    .line 171
    const/4 v12, 0x0

    goto :goto_0

    .line 179
    :cond_2
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v12

    const-string/jumbo v13, "tizen"

    const-string/jumbo v14, "Start"

    invoke-interface {v12, v13, v14}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->activateGrammarRule(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 182
    const/4 v12, 0x0

    goto :goto_0

    .line 189
    :cond_3
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v12

    const-string/jumbo v13, "tizen"

    const-string/jumbo v14, "Cancel"

    invoke-interface {v12, v13, v14}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->activateGrammarRule(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 192
    const/4 v12, 0x0

    goto :goto_0

    .line 195
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v12

    new-array v9, v12, [Ljava/lang/String;

    .line 196
    .local v9, "ruleNames":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "position":I
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v12

    if-ge v7, v12, :cond_5

    .line 197
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;

    invoke-virtual {v12}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->getRuleName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v9, v7

    .line 196
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 199
    :cond_5
    if-eqz v9, :cond_6

    array-length v12, v9

    if-lez v12, :cond_6

    .line 200
    invoke-static {v9}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 203
    :cond_6
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v12

    invoke-interface {v12}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->getGrammarRules()Ljava/util/Set;

    move-result-object v0

    .line 204
    .local v0, "availableGrammarRules":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v12

    if-gtz v12, :cond_8

    .line 207
    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 210
    :cond_8
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/util/Pair;

    .line 211
    .local v10, "rulePair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 212
    .local v3, "grammarName":Ljava/lang/String;
    iget-object v8, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    .line 214
    .local v8, "ruleName":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "#"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 216
    .local v4, "grammarRuleName":Ljava/lang/String;
    invoke-static {v9, v4}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v12

    const/4 v13, -0x1

    if-le v12, v13, :cond_9

    .line 220
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v12

    invoke-interface {v12, v3, v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->activateGrammarRule(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_9

    .line 223
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 228
    .end local v0    # "availableGrammarRules":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v3    # "grammarName":Ljava/lang/String;
    .end local v4    # "grammarRuleName":Ljava/lang/String;
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "position":I
    .end local v8    # "ruleName":Ljava/lang/String;
    .end local v9    # "ruleNames":[Ljava/lang/String;
    .end local v10    # "rulePair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_a
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v12

    invoke-interface {v12}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->startRecognition()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v12

    if-nez v12, :cond_b

    .line 231
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 233
    :cond_b
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 234
    .end local v2    # "fieldId":Ljava/lang/String;
    .end local v5    # "grammarRules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;>;"
    .end local v11    # "ruleSet":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;
    :catch_0
    move-exception v1

    .line 237
    .local v1, "e":Ljava/lang/Exception;
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method private pushAudioSegment([S)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;
    .locals 5
    .param p1, "chunk"    # [S

    .prologue
    const/4 v4, 0x0

    .line 306
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_1

    .line 309
    :cond_0
    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;

    const-string/jumbo v3, ""

    invoke-direct {v2, p0, v4, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;Ljava/lang/String;)V

    .line 323
    :goto_0
    return-object v2

    .line 316
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->pushAudioFrame([S)Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, "result":Ljava/lang/String;
    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 318
    .end local v1    # "result":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 319
    .local v0, "e":Ljava/lang/IllegalStateException;
    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;->ILLEGAL_STATE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    invoke-direct {v2, p0, v3, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;Ljava/lang/String;)V

    goto :goto_0

    .line 320
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 321
    .local v0, "e":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException;
    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;->RECOGNITION_ERROR:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    invoke-direct {v2, p0, v3, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;Ljava/lang/String;)V

    goto :goto_0

    .line 322
    .end local v0    # "e":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException;
    :catch_2
    move-exception v0

    .line 323
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;->ILLEGAL_STATE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    invoke-direct {v2, p0, v3, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private declared-synchronized stopRecognition()V
    .locals 1

    .prologue
    .line 285
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    monitor-exit p0

    return-void

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized cancelRecognition()V
    .locals 1

    .prologue
    .line 278
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    if-nez v0, :cond_0

    .line 279
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    .line 280
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->stopRecognition()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    :cond_0
    monitor-exit p0

    return-void

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized completeRecognition()V
    .locals 1

    .prologue
    .line 296
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->allAudioReceived:Z

    .line 297
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    monitor-exit p0

    return-void

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected execute()Z
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 51
    monitor-enter p0

    .line 52
    :try_start_0
    iget-boolean v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    if-eqz v10, :cond_1

    .line 53
    monitor-exit p0

    .line 139
    :cond_0
    :goto_0
    return v8

    .line 55
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->prepareForRecognition()Z

    move-result v3

    .line 57
    .local v3, "preparedForRecognition":Z
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mListener:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;

    invoke-interface {v10, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;->onPreparedForRecognition(Z)V

    .line 58
    if-nez v3, :cond_2

    .line 59
    monitor-exit p0

    goto :goto_0

    .line 134
    .end local v3    # "preparedForRecognition":Z
    :catchall_0
    move-exception v8

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 62
    .restart local v3    # "preparedForRecognition":Z
    :cond_2
    const/4 v0, 0x0

    .line 63
    .local v0, "allAudioSent":Z
    const/4 v5, 0x0

    .line 64
    .local v5, "recognitionError":Z
    const/4 v6, 0x0

    .line 65
    .local v6, "result":Ljava/lang/String;
    :cond_3
    :goto_1
    if-nez v0, :cond_7

    :try_start_1
    iget-boolean v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    if-nez v10, :cond_7

    if-nez v5, :cond_7

    .line 66
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->audioQueue:Ljava/util/Queue;

    invoke-interface {v10}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [S

    .line 67
    .local v7, "segment":[S
    if-nez v7, :cond_4

    .line 68
    iget-boolean v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    if-nez v10, :cond_4

    .line 69
    iget-boolean v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->allAudioReceived:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v10, :cond_6

    .line 72
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 81
    :cond_4
    :goto_2
    if-eqz v7, :cond_5

    :try_start_3
    iget-boolean v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    if-nez v10, :cond_5

    .line 82
    invoke-direct {p0, v7}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->pushAudioSegment([S)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;

    move-result-object v4

    .line 83
    .local v4, "pushResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;
    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;->getResult()Ljava/lang/String;

    move-result-object v6

    .line 84
    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;->getError()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    move-result-object v10

    if-eqz v10, :cond_5

    .line 85
    const/4 v5, 0x1

    .line 91
    .end local v4    # "pushResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;
    :cond_5
    if-nez v5, :cond_3

    if-eqz v0, :cond_3

    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v10

    if-eqz v10, :cond_3

    .line 93
    :try_start_4
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v10

    invoke-interface {v10, v7}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->pushAudioFrame([S)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v6

    goto :goto_1

    .line 77
    :cond_6
    const/4 v0, 0x1

    goto :goto_2

    .line 94
    :catch_0
    move-exception v1

    .line 95
    .local v1, "e":Ljava/lang/IllegalStateException;
    const/4 v5, 0x1

    .line 100
    goto :goto_1

    .line 96
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v1

    .line 97
    .local v1, "e":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException;
    const/4 v5, 0x1

    .line 100
    goto :goto_1

    .line 98
    .end local v1    # "e":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException;
    :catch_2
    move-exception v1

    .line 99
    .local v1, "e":Ljava/lang/UnsatisfiedLinkError;
    const/4 v5, 0x1

    goto :goto_1

    .line 104
    .end local v1    # "e":Ljava/lang/UnsatisfiedLinkError;
    .end local v7    # "segment":[S
    :cond_7
    :try_start_5
    iget-boolean v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    if-nez v10, :cond_8

    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 107
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 111
    :cond_8
    :try_start_6
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->cancelRecognition()V
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 120
    :goto_3
    :try_start_7
    new-instance v10, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;

    invoke-direct {v10, v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult$Builder;->build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;

    move-result-object v2

    .line 121
    .local v2, "nBestResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    if-nez v2, :cond_9

    .line 124
    monitor-exit p0

    goto/16 :goto_0

    .line 112
    .end local v2    # "nBestResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    :catch_3
    move-exception v1

    .line 113
    .local v1, "e":Ljava/lang/IllegalStateException;
    const/4 v5, 0x1

    .line 116
    goto :goto_3

    .line 114
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_4
    move-exception v1

    .line 115
    .local v1, "e":Ljava/lang/UnsatisfiedLinkError;
    const/4 v5, 0x1

    goto :goto_3

    .line 127
    .end local v1    # "e":Ljava/lang/UnsatisfiedLinkError;
    .restart local v2    # "nBestResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    :cond_9
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->initialize()V

    .line 128
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;

    move-result-object v10

    iget-object v11, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->srContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v11}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldID()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/vlingo/sdk/internal/settings/Settings;->LANGUAGE:Ljava/lang/String;

    invoke-virtual {v10, v2, v11, v12}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->getRecognitionResult(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->recognitionResponse:Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

    .line 129
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->recognitionResponse:Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

    if-nez v10, :cond_a

    .line 132
    monitor-exit p0

    goto/16 :goto_0

    .line 134
    :cond_a
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 135
    iget-boolean v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    if-nez v10, :cond_0

    move v8, v9

    .line 139
    goto/16 :goto_0

    .line 73
    .end local v2    # "nBestResult":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    .restart local v7    # "segment":[S
    :catch_5
    move-exception v10

    goto/16 :goto_2
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    return v0
.end method

.method protected onExecutionFailed(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;)V
    .locals 1
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    if-nez v0, :cond_0

    .line 263
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$2;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$2;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->notifyOnCallerHandler(Ljava/lang/Runnable;)V

    .line 270
    :cond_0
    invoke-interface {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;->onVoconRecognitionCompleted()V

    .line 271
    return-void
.end method

.method protected bridge synthetic onExecutionFailed(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->onExecutionFailed(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;)V

    return-void
.end method

.method protected onExecutionSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;)V
    .locals 1
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->mCancelRequested:Z

    if-nez v0, :cond_0

    .line 247
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$1;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$1;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->notifyOnCallerHandler(Ljava/lang/Runnable;)V

    .line 254
    :cond_0
    invoke-interface {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;->onVoconRecognitionCompleted()V

    .line 255
    return-void
.end method

.method protected bridge synthetic onExecutionSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->onExecutionSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;)V

    return-void
.end method

.method public declared-synchronized processAudio([S)V
    .locals 1
    .param p1, "chunk"    # [S

    .prologue
    .line 290
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->audioQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 291
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    monitor-exit p0

    return-void

    .line 290
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
