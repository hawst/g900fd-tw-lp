.class public Lcom/vlingo/sdk/internal/VLServicesImpl;
.super Lcom/vlingo/sdk/internal/VLComponentImpl;
.source "VLServicesImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;
.implements Lcom/vlingo/sdk/services/VLServices;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;
    }
.end annotation


# static fields
.field private static final HELLO_REQUEST:Ljava/lang/String; = "VVHello"

.field private static final TYPE_HELLO:I = 0x2

.field private static final TYPE_UAL:I = 0x1

.field private static final UAL_REQUEST:Ljava/lang/String; = "ActivityLog"


# instance fields
.field private mHelloListener:Lcom/vlingo/sdk/services/VLServicesListener;

.field private mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

.field private mUserActivityListener:Lcom/vlingo/sdk/services/VLServicesListener;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 1
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLComponentImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 125
    new-instance v0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;-><init>(Lcom/vlingo/sdk/internal/VLServicesImpl;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    .line 126
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/services/VLServicesListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLServicesImpl;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mUserActivityListener:Lcom/vlingo/sdk/services/VLServicesListener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/sdk/internal/VLServicesImpl;Lcom/vlingo/sdk/services/VLServicesListener;)Lcom/vlingo/sdk/services/VLServicesListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLServicesImpl;
    .param p1, "x1"    # Lcom/vlingo/sdk/services/VLServicesListener;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mUserActivityListener:Lcom/vlingo/sdk/services/VLServicesListener;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/services/VLServicesListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLServicesImpl;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mHelloListener:Lcom/vlingo/sdk/services/VLServicesListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/VLServicesImpl;Lcom/vlingo/sdk/services/VLServicesListener;)Lcom/vlingo/sdk/services/VLServicesListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLServicesImpl;
    .param p1, "x1"    # Lcom/vlingo/sdk/services/VLServicesListener;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mHelloListener:Lcom/vlingo/sdk/services/VLServicesListener;

    return-object p1
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLServicesImpl;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    return-object v0
.end method

.method private getType(Ljava/lang/String;)I
    .locals 1
    .param p1, "requestName"    # Ljava/lang/String;

    .prologue
    .line 291
    const-string/jumbo v0, "ActivityLog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const/4 v0, 0x1

    .line 297
    :goto_0
    return v0

    .line 294
    :cond_0
    const-string/jumbo v0, "VVHello"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    const/4 v0, 0x2

    goto :goto_0

    .line 297
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private stopProcessingIfDisabled(Ljava/lang/String;Lcom/vlingo/sdk/services/VLServicesListener;)Z
    .locals 2
    .param p1, "settingName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/sdk/services/VLServicesListener;

    .prologue
    const/4 v0, 0x1

    .line 175
    invoke-static {p1, v0}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 178
    sget-object v0, Lcom/vlingo/sdk/services/VLServicesErrors;->ERROR_CLIENT:Lcom/vlingo/sdk/services/VLServicesErrors;

    const-string/jumbo v1, "Sending messages is disabled."

    invoke-interface {p2, v0, v1}, Lcom/vlingo/sdk/services/VLServicesListener;->onError(Lcom/vlingo/sdk/services/VLServicesErrors;Ljava/lang/String;)V

    .line 179
    const/4 v0, 0x0

    .line 181
    :cond_0
    return v0
.end method


# virtual methods
.method public bridge synthetic destroy()V
    .locals 0

    .prologue
    .line 33
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->destroy()V

    return-void
.end method

.method public doesDependOnSettings()Z
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic isValid()Z
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->isValid()Z

    move-result v0

    return v0
.end method

.method public onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 283
    return-void
.end method

.method onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 136
    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mUserActivityListener:Lcom/vlingo/sdk/services/VLServicesListener;

    .line 137
    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mHelloListener:Lcom/vlingo/sdk/services/VLServicesListener;

    .line 139
    return-void
.end method

.method public onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTaskName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/VLServicesImpl;->getType(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Lcom/vlingo/sdk/services/VLServicesErrors;->ERROR_NETWORK:Lcom/vlingo/sdk/services/VLServicesErrors;

    const-string/jumbo v3, "Network error"

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->notifyError(ILcom/vlingo/sdk/services/VLServicesErrors;Ljava/lang/String;)V

    .line 280
    return-void
.end method

.method public onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 10
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 230
    iget v6, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_2

    .line 236
    const-string/jumbo v6, "ActivityLog"

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTaskName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 240
    iget-object v6, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTaskName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/vlingo/sdk/internal/VLServicesImpl;->getType(Ljava/lang/String;)I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->notifySuccess(ILjava/util/List;)V

    .line 269
    :goto_0
    return-void

    .line 243
    :cond_0
    const-string/jumbo v6, "VVHello"

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTaskName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 247
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->createFromXml(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v5

    .line 248
    .local v5, "vlResponse":Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->isError()Z

    move-result v6

    if-nez v6, :cond_2

    .line 249
    const/4 v4, 0x0

    .line 250
    .local v4, "vlActionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/sdk/recognition/VLAction;>;"
    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->hasActions()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 251
    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->getActionList()Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    move-result-object v1

    .line 252
    .local v1, "actionList":Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->size()I

    move-result v3

    .line 253
    .local v3, "listSize":I
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "vlActionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/sdk/recognition/VLAction;>;"
    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 254
    .restart local v4    # "vlActionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/sdk/recognition/VLAction;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_1

    .line 255
    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->elementAt(I)Lcom/vlingo/sdk/internal/vlservice/response/Action;

    move-result-object v0

    .line 256
    .local v0, "action":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    new-instance v6, Lcom/vlingo/sdk/internal/VLActionImpl;

    invoke-direct {v6, v0}, Lcom/vlingo/sdk/internal/VLActionImpl;-><init>(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 259
    .end local v0    # "action":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    .end local v1    # "actionList":Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    .end local v2    # "i":I
    .end local v3    # "listSize":I
    :cond_1
    iget-object v6, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTaskName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/vlingo/sdk/internal/VLServicesImpl;->getType(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7, v4}, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->notifySuccess(ILjava/util/List;)V

    goto :goto_0

    .line 268
    .end local v4    # "vlActionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/sdk/recognition/VLAction;>;"
    .end local v5    # "vlResponse":Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
    :cond_2
    iget-object v6, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTaskName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/vlingo/sdk/internal/VLServicesImpl;->getType(Ljava/lang/String;)I

    move-result v7

    sget-object v8, Lcom/vlingo/sdk/services/VLServicesErrors;->ERROR_SERVER:Lcom/vlingo/sdk/services/VLServicesErrors;

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v7, v8, v9}, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->notifyError(ILcom/vlingo/sdk/services/VLServicesErrors;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 4
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->getTaskName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/VLServicesImpl;->getType(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Lcom/vlingo/sdk/services/VLServicesErrors;->ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/services/VLServicesErrors;

    const-string/jumbo v3, "Timeout waiting for request"

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->notifyError(ILcom/vlingo/sdk/services/VLServicesErrors;Ljava/lang/String;)V

    .line 274
    const/4 v0, 0x1

    return v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 226
    return-void
.end method

.method public sendActivityLog(Ljava/lang/String;Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;Lcom/vlingo/sdk/services/VLServicesListener;)V
    .locals 5
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "userLog"    # Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;
    .param p3, "listener"    # Lcom/vlingo/sdk/services/VLServicesListener;

    .prologue
    const/4 v4, 0x0

    .line 189
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLServicesImpl;->validateInstance()V

    .line 191
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 192
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "language cannot be null or empty"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 194
    :cond_0
    if-nez p2, :cond_1

    .line 195
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "userLog cannot be null or empty"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 197
    :cond_1
    if-nez p3, :cond_2

    .line 198
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "listener must be specifed"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 200
    :cond_2
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mUserActivityListener:Lcom/vlingo/sdk/services/VLServicesListener;

    if-eqz v2, :cond_3

    .line 202
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "UserActivity request already in progress"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 205
    :cond_3
    const-string/jumbo v2, "activitylog.enable"

    invoke-direct {p0, v2, p3}, Lcom/vlingo/sdk/internal/VLServicesImpl;->stopProcessingIfDisabled(Ljava/lang/String;Lcom/vlingo/sdk/services/VLServicesListener;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 218
    :goto_0
    return-void

    .line 209
    :cond_4
    iput-object p3, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mUserActivityListener:Lcom/vlingo/sdk/services/VLServicesListener;

    .line 214
    invoke-static {}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->getUserLoggingURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v1

    .line 215
    .local v1, "url":Lcom/vlingo/sdk/internal/http/URL;
    const-string/jumbo v2, "ActivityLog"

    invoke-virtual {p2}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;->getXML()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, p0, v1, v3, p1}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v0

    .line 216
    .local v0, "scheduledRequest":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->setGzipPostData(Z)V

    .line 217
    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3, v4, v4}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->schedule(JZZ)V

    goto :goto_0
.end method

.method public sendHello(Ljava/lang/String;Lcom/vlingo/sdk/services/VLServicesListener;)V
    .locals 6
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/sdk/services/VLServicesListener;

    .prologue
    const/4 v5, 0x0

    .line 146
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLServicesImpl;->validateInstance()V

    .line 148
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 149
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "language cannot be null or empty"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 151
    :cond_0
    if-nez p2, :cond_1

    .line 152
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "listener must be specifed"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 154
    :cond_1
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mHelloListener:Lcom/vlingo/sdk/services/VLServicesListener;

    if-eqz v3, :cond_2

    .line 156
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "Hello request already in progress"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 159
    :cond_2
    const-string/jumbo v3, "hello.enable"

    invoke-direct {p0, v3, p2}, Lcom/vlingo/sdk/internal/VLServicesImpl;->stopProcessingIfDisabled(Ljava/lang/String;Lcom/vlingo/sdk/services/VLServicesListener;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 172
    :goto_0
    return-void

    .line 163
    :cond_3
    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLServicesImpl;->mHelloListener:Lcom/vlingo/sdk/services/VLServicesListener;

    .line 168
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "<Hello PhoneHash=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getInstance()Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getPhoneNumberHash()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\"/>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "body":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->getHelloURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v2

    .line 170
    .local v2, "url":Lcom/vlingo/sdk/internal/http/URL;
    const-string/jumbo v3, "VVHello"

    invoke-static {v3, p0, v2, v0, p1}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v1

    .line 171
    .local v1, "helloRequest":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    const-wide/16 v3, 0x32

    invoke-virtual {v1, v3, v4, v5, v5}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->schedule(JZZ)V

    goto :goto_0
.end method
