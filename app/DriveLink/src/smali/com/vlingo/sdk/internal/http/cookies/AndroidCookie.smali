.class public Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;
.super Lorg/apache/http/impl/cookie/BasicClientCookie;
.source "AndroidCookie.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/cookies/Cookie;


# static fields
.field private static final FIELD_COUNT:I = 0x5

.field private static final FIELD_DELIMITER:C = ','

.field private static final FIELD_INDEX_DOMAIN:I = 0x2

.field private static final FIELD_INDEX_EXPIRES:I = 0x4

.field private static final FIELD_INDEX_NAME:I = 0x0

.field private static final FIELD_INDEX_PATH:I = 0x3

.field private static final FIELD_INDEX_VALUE:I = 0x1


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/cookie/BasicClientCookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;
    .locals 7
    .param p0, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 83
    const/16 v2, 0x2c

    invoke-static {p0, v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "fields":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    .line 85
    new-instance v2, Ljava/lang/Exception;

    const-string/jumbo v3, "Badly formatted cookie data"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2

    .line 87
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-direct {v0, v2, v3}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .local v0, "c":Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;
    aget-object v2, v1, v4

    if-eqz v2, :cond_1

    aget-object v2, v1, v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 89
    aget-object v2, v1, v4

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->setDomain(Ljava/lang/String;)V

    .line 91
    :cond_1
    aget-object v2, v1, v5

    if-eqz v2, :cond_2

    aget-object v2, v1, v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 92
    aget-object v2, v1, v5

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->setPath(Ljava/lang/String;)V

    .line 94
    :cond_2
    aget-object v2, v1, v6

    if-eqz v2, :cond_3

    aget-object v2, v1, v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 95
    aget-object v2, v1, v6

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->setExpires(J)V

    .line 97
    :cond_3
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 107
    instance-of v1, p1, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 108
    check-cast v0, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;

    .line 110
    .local v0, "ac":Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getDomain()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 115
    .end local v0    # "ac":Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;
    :goto_0
    return v1

    .line 110
    .restart local v0    # "ac":Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 115
    .end local v0    # "ac":Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getExpires()J
    .locals 3

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getExpiryDate()Ljava/util/Date;

    move-result-object v0

    .line 36
    .local v0, "date":Ljava/util/Date;
    if-nez v0, :cond_0

    const-wide/16 v1, 0x0

    :goto_0
    return-wide v1

    :cond_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 120
    const/16 v0, 0x1f

    .line 121
    .local v0, "hash":I
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/2addr v0, v1

    .line 123
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 124
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/2addr v0, v1

    .line 125
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 126
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/2addr v0, v1

    .line 127
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 128
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/2addr v0, v1

    .line 129
    :cond_3
    return v0
.end method

.method public isExpired()Z
    .locals 1

    .prologue
    .line 41
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->isExpired(Ljava/util/Date;)Z

    move-result v0

    return v0
.end method

.method public isMatch(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 46
    const/4 v0, 0x1

    .line 47
    .local v0, "isMatch":Z
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 48
    if-eqz p1, :cond_2

    invoke-virtual {p1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    and-int/2addr v0, v1

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 51
    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_1
    and-int/2addr v0, v2

    .line 53
    :cond_1
    return v0

    :cond_2
    move v1, v3

    .line 48
    goto :goto_0

    :cond_3
    move v2, v3

    .line 51
    goto :goto_1
.end method

.method public serialize()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x2c

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getExpiryDate()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getExpires()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 77
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public setExpires(J)V
    .locals 1
    .param p1, "expires"    # J

    .prologue
    .line 58
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 59
    .local v0, "expiryDate":Ljava/util/Date;
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->setExpiryDate(Ljava/util/Date;)V

    .line 60
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "AndroidCookie: expires="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getExpires()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " isExpired="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->isExpired()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " domain="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " path="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookie;->getVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
