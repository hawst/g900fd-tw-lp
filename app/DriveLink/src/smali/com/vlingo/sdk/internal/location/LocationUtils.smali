.class public Lcom/vlingo/sdk/internal/location/LocationUtils;
.super Ljava/lang/Object;
.source "LocationUtils.java"


# static fields
.field public static final NO_LAT:D

.field public static final NO_LONG:D

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/vlingo/sdk/internal/location/LocationUtils;


# instance fields
.field private lastLat:D

.field private lastLong:D

.field private lastMCC:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/vlingo/sdk/internal/location/LocationUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/location/LocationUtils;->TAG:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/location/LocationUtils;->instance:Lcom/vlingo/sdk/internal/location/LocationUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastMCC:Ljava/lang/String;

    .line 38
    iput-wide v1, p0, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastLong:D

    .line 39
    iput-wide v1, p0, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastLat:D

    return-void
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/location/LocationUtils;->instance:Lcom/vlingo/sdk/internal/location/LocationUtils;

    .line 53
    return-void
.end method

.method public static getCarrierCountry(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getInstance()Lcom/vlingo/sdk/internal/location/LocationUtils;

    move-result-object v0

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/location/LocationUtils;->mGetCarrierCountry(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCellTowerInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getInstance()Lcom/vlingo/sdk/internal/location/LocationUtils;

    move-result-object v0

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/location/LocationUtils;->mGetCellTowerInfo()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/sdk/internal/location/LocationUtils;
    .locals 2

    .prologue
    .line 45
    const-class v1, Lcom/vlingo/sdk/internal/location/LocationUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/location/LocationUtils;->instance:Lcom/vlingo/sdk/internal/location/LocationUtils;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/vlingo/sdk/internal/location/LocationUtils;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/location/LocationUtils;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/location/LocationUtils;->instance:Lcom/vlingo/sdk/internal/location/LocationUtils;

    .line 48
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/location/LocationUtils;->instance:Lcom/vlingo/sdk/internal/location/LocationUtils;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getLastLat()D
    .locals 2

    .prologue
    .line 68
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getInstance()Lcom/vlingo/sdk/internal/location/LocationUtils;

    move-result-object v0

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/location/LocationUtils;->mGetLastLat()D

    move-result-wide v0

    return-wide v0
.end method

.method public static getLastLong()D
    .locals 2

    .prologue
    .line 72
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getInstance()Lcom/vlingo/sdk/internal/location/LocationUtils;

    move-result-object v0

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/location/LocationUtils;->mGetLastLong()D

    move-result-wide v0

    return-wide v0
.end method

.method public static getMCC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getInstance()Lcom/vlingo/sdk/internal/location/LocationUtils;

    move-result-object v0

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/location/LocationUtils;->mGetMCC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private mGetCarrierCountry(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    const-string/jumbo v1, "phone"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 116
    .local v0, "mgr":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private mGetCellTowerInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/location/LocationUtils;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/location/LocationUtils;->mGetCellTowerInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private mGetCellTowerInfo(Landroid/content/Context;)Ljava/lang/String;
    .locals 21
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    const-string/jumbo v16, ""

    .line 123
    .local v16, "toReturn":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/VLSdk;->getLocationOn()Z

    move-result v17

    if-eqz v17, :cond_7

    .line 124
    if-eqz p1, :cond_7

    .line 125
    const-string/jumbo v17, "location"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/location/LocationManager;

    .line 126
    .local v9, "lm":Landroid/location/LocationManager;
    new-instance v6, Landroid/location/Criteria;

    invoke-direct {v6}, Landroid/location/Criteria;-><init>()V

    .line 127
    .local v6, "cr":Landroid/location/Criteria;
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 128
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/location/Criteria;->setPowerRequirement(I)V

    .line 129
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/location/Criteria;->setAltitudeRequired(Z)V

    .line 130
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/location/Criteria;->setBearingRequired(Z)V

    .line 131
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/location/Criteria;->setCostAllowed(Z)V

    .line 132
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/location/Criteria;->setSpeedRequired(Z)V

    .line 134
    new-instance v12, Ljava/lang/StringBuffer;

    const/16 v17, 0x32

    move/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 136
    .local v12, "locationStringBuffer":Ljava/lang/StringBuffer;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v9, v6, v0}, Landroid/location/LocationManager;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    move-result-object v14

    .line 139
    .local v14, "provider":Ljava/lang/String;
    if-eqz v14, :cond_2

    .line 140
    invoke-virtual {v9, v14}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v10

    .line 141
    .local v10, "loc":Landroid/location/Location;
    sget-object v18, Lcom/vlingo/sdk/internal/location/LocationUtils;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "mGetCellTowerInfo() - BestProvider\'s Location is "

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    if-eqz v10, :cond_8

    const-string/jumbo v17, "not null"

    :goto_0
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    if-eqz v10, :cond_9

    invoke-virtual {v10}, Landroid/location/Location;->getLatitude()D

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->abs(D)D

    move-result-wide v17

    const-wide v19, 0x4066800000000000L    # 180.0

    cmpl-double v17, v17, v19

    if-gtz v17, :cond_0

    invoke-virtual {v10}, Landroid/location/Location;->getLongitude()D

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->abs(D)D

    move-result-wide v17

    const-wide v19, 0x4066800000000000L    # 180.0

    cmpl-double v17, v17, v19

    if-lez v17, :cond_9

    .line 143
    :cond_0
    const/4 v10, 0x0

    .line 154
    :cond_1
    :goto_1
    sget-object v18, Lcom/vlingo/sdk/internal/location/LocationUtils;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "mGetCellTowerInfo() - AnyProvider\'s Location is "

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    if-eqz v10, :cond_b

    const-string/jumbo v17, "not null"

    :goto_2
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    if-eqz v10, :cond_2

    .line 156
    invoke-virtual {v10}, Landroid/location/Location;->getLatitude()D

    move-result-wide v17

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastLat:D

    .line 157
    invoke-virtual {v10}, Landroid/location/Location;->getLongitude()D

    move-result-wide v17

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastLong:D

    .line 159
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lat="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastLat:D

    move-wide/from16 v18, v0

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, ";Long="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastLong:D

    move-wide/from16 v18, v0

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, ";Alt="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v10}, Landroid/location/Location;->getAltitude()D

    move-result-wide v18

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    .end local v10    # "loc":Landroid/location/Location;
    :cond_2
    const-string/jumbo v17, "phone"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/telephony/TelephonyManager;

    .line 168
    .local v15, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v15}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v5

    .line 169
    .local v5, "cloc":Landroid/telephony/CellLocation;
    invoke-virtual {v15}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v13

    .line 170
    .local v13, "networkInfo":Ljava/lang/String;
    if-eqz v13, :cond_3

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_3

    const-string/jumbo v17, "@"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 171
    :cond_3
    const-string/jumbo v13, "0000"

    .line 174
    :cond_4
    instance-of v0, v5, Landroid/telephony/cdma/CdmaCellLocation;

    move/from16 v17, v0

    if-eqz v17, :cond_c

    move-object v4, v5

    .line 175
    check-cast v4, Landroid/telephony/cdma/CdmaCellLocation;

    .line 177
    .local v4, "cdmaloc":Landroid/telephony/cdma/CdmaCellLocation;
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    if-lez v17, :cond_5

    const/16 v17, 0x3b

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 178
    :cond_5
    const/16 v17, 0x0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastMCC:Ljava/lang/String;

    .line 199
    .end local v4    # "cdmaloc":Landroid/telephony/cdma/CdmaCellLocation;
    :cond_6
    :goto_3
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    .line 204
    .local v11, "locationString":Ljava/lang/String;
    move-object/from16 v16, v11

    .line 207
    .end local v5    # "cloc":Landroid/telephony/CellLocation;
    .end local v6    # "cr":Landroid/location/Criteria;
    .end local v9    # "lm":Landroid/location/LocationManager;
    .end local v11    # "locationString":Ljava/lang/String;
    .end local v12    # "locationStringBuffer":Ljava/lang/StringBuffer;
    .end local v13    # "networkInfo":Ljava/lang/String;
    .end local v14    # "provider":Ljava/lang/String;
    .end local v15    # "tm":Landroid/telephony/TelephonyManager;
    :cond_7
    return-object v16

    .line 141
    .restart local v6    # "cr":Landroid/location/Criteria;
    .restart local v9    # "lm":Landroid/location/LocationManager;
    .restart local v10    # "loc":Landroid/location/Location;
    .restart local v12    # "locationStringBuffer":Ljava/lang/StringBuffer;
    .restart local v14    # "provider":Ljava/lang/String;
    :cond_8
    const-string/jumbo v17, "null"

    goto/16 :goto_0

    .line 144
    :cond_9
    if-nez v10, :cond_1

    .line 147
    invoke-virtual {v9}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 148
    .local v3, "anyProvider":Ljava/lang/String;
    invoke-virtual {v9, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v10

    .line 149
    if-eqz v10, :cond_a

    goto/16 :goto_1

    .line 154
    .end local v3    # "anyProvider":Ljava/lang/String;
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_b
    const-string/jumbo v17, "null"

    goto/16 :goto_2

    .line 186
    .end local v10    # "loc":Landroid/location/Location;
    .restart local v5    # "cloc":Landroid/telephony/CellLocation;
    .restart local v13    # "networkInfo":Ljava/lang/String;
    .restart local v15    # "tm":Landroid/telephony/TelephonyManager;
    :cond_c
    instance-of v0, v5, Landroid/telephony/gsm/GsmCellLocation;

    move/from16 v17, v0

    if-eqz v17, :cond_6

    move-object v7, v5

    .line 187
    check-cast v7, Landroid/telephony/gsm/GsmCellLocation;

    .line 189
    .local v7, "gsmloc":Landroid/telephony/gsm/GsmCellLocation;
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    if-lez v17, :cond_d

    const/16 v17, 0x3b

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 190
    :cond_d
    const/16 v17, 0x0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastMCC:Ljava/lang/String;

    goto :goto_3
.end method

.method private mGetLastLat()D
    .locals 3

    .prologue
    .line 97
    const-wide/16 v0, 0x0

    .line 98
    .local v0, "toReturn":D
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/VLSdk;->getLocationOn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 99
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/location/LocationUtils;->mGetCellTowerInfo()Ljava/lang/String;

    .line 100
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastLat:D

    .line 102
    :cond_0
    return-wide v0
.end method

.method private mGetLastLong()D
    .locals 3

    .prologue
    .line 106
    const-wide/16 v0, 0x0

    .line 107
    .local v0, "toReturn":D
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/VLSdk;->getLocationOn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/location/LocationUtils;->mGetCellTowerInfo()Ljava/lang/String;

    .line 109
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastLong:D

    .line 111
    :cond_0
    return-wide v0
.end method

.method private mGetMCC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    const-string/jumbo v0, ""

    .line 85
    .local v0, "toReturn":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/VLSdk;->getLocationOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/location/LocationUtils;->mGetCellTowerInfo()Ljava/lang/String;

    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/internal/location/LocationUtils;->lastMCC:Ljava/lang/String;

    .line 89
    :cond_0
    return-object v0
.end method
