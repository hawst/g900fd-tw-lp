.class public Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;
.super Lcom/vlingo/sdk/training/VLTrainerUpdateList;
.source "VLTrainerUpdateListImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl$1;
    }
.end annotation


# instance fields
.field mContactItems:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem;",
            ">;"
        }
    .end annotation
.end field

.field mPlaylistItems:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem;",
            ">;"
        }
    .end annotation
.end field

.field mSongItems:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/sdk/training/VLTrainerUpdateList;-><init>()V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mContactItems:Ljava/util/HashMap;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mSongItems:Ljava/util/HashMap;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mPlaylistItems:Ljava/util/HashMap;

    .line 29
    return-void
.end method


# virtual methods
.method public addContact(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1, "uid"    # J
    .param p3, "firstName"    # Ljava/lang/String;
    .param p4, "middleName"    # Ljava/lang/String;
    .param p5, "lastName"    # Ljava/lang/String;
    .param p6, "nickname"    # Ljava/lang/String;
    .param p7, "fullName"    # Ljava/lang/String;
    .param p8, "companyName"    # Ljava/lang/String;
    .param p9, "phoneticFirstName"    # Ljava/lang/String;
    .param p10, "phoneticMiddleName"    # Ljava/lang/String;
    .param p11, "phoneticLastName"    # Ljava/lang/String;

    .prologue
    .line 32
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mContactItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Update list already contains an item for contact with uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 35
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;

    sget-object v12, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->INSERT:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    move-wide v10, p1

    invoke-direct/range {v0 .. v12}, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 36
    .local v0, "contact":Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mContactItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    return-void
.end method

.method public addNOOPItem(Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;)V
    .locals 6
    .param p1, "trainerType"    # Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    .prologue
    const-wide/16 v4, -0x1

    .line 97
    sget-object v0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl$1;->$SwitchMap$com$vlingo$sdk$training$VLTrainer$TrainerItemType:[I

    invoke-virtual {p1}, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 102
    :goto_0
    return-void

    .line 98
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mContactItems:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;

    sget-object v3, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->NOCHANGE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v2, v4, v5, v3}, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;-><init>(JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 99
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mSongItems:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;

    sget-object v3, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->NOCHANGE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v2, v4, v5, v3}, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;-><init>(JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 100
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mPlaylistItems:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;

    sget-object v3, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->NOCHANGE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v2, v4, v5, v3}, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;-><init>(JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public addPlaylist(JLjava/lang/String;)V
    .locals 4
    .param p1, "uid"    # J
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mPlaylistItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Update list already contains an item for playlist with uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 67
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;

    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->INSERT:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v0, p3, p1, p2, v1}, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;-><init>(Ljava/lang/String;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 68
    .local v0, "playlist":Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mPlaylistItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-void
.end method

.method public addSong(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 11
    .param p1, "uid"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "artist"    # Ljava/lang/String;
    .param p5, "album"    # Ljava/lang/String;
    .param p6, "composer"    # Ljava/lang/String;
    .param p7, "genre"    # Ljava/lang/String;
    .param p8, "year"    # I
    .param p9, "folder"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mSongItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Update list already contains an item for song with uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 51
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;

    sget-object v10, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->INSERT:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    move-object v1, p3

    move-object v2, p4

    move-object/from16 v3, p6

    move-object/from16 v4, p5

    move-object/from16 v5, p7

    move/from16 v6, p8

    move-object/from16 v7, p9

    move-wide v8, p1

    invoke-direct/range {v0 .. v10}, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 52
    .local v0, "song":Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mSongItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    return-void
.end method

.method public getLmttItems()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 86
    .local v0, "lmttItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/util/Collection<Lcom/vlingo/sdk/internal/lmtt/LMTTItem;>;>;"
    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_CONTACT:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mContactItems:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mSongItems:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mPlaylistItems:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mContactItems:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mSongItems:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mPlaylistItems:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeContact(J)V
    .locals 4
    .param p1, "uid"    # J

    .prologue
    .line 40
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mContactItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Update list already contains an item for contact with uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 43
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;

    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v0, p1, p2, v1}, Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;-><init>(JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 44
    .local v0, "contact":Lcom/vlingo/sdk/internal/lmtt/LMTTContactItem;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mContactItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    return-void
.end method

.method public removePlayList(J)V
    .locals 4
    .param p1, "uid"    # J

    .prologue
    .line 72
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mPlaylistItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Update list already contains an item for playlist with uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 75
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;

    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v0, p1, p2, v1}, Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;-><init>(JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 76
    .local v0, "playlist":Lcom/vlingo/sdk/internal/lmtt/LMTTPlaylistItem;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mPlaylistItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    return-void
.end method

.method public removeSong(J)V
    .locals 4
    .param p1, "uid"    # J

    .prologue
    .line 56
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mSongItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Update list already contains an item for song with uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 59
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;

    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-direct {v0, p1, p2, v1}, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;-><init>(JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 60
    .local v0, "song":Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->mSongItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    return-void
.end method
