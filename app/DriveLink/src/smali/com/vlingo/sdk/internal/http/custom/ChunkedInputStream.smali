.class public Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;
.super Ljava/io/InputStream;
.source "ChunkedInputStream.java"


# static fields
.field private static final READ_BYTE:I = 0x0

.field private static final READ_CR:I = 0x1

.field private static final READ_EOF:I = -0x1

.field private static final READ_LF:I = 0x2


# instance fields
.field private currChunkRead:I

.field private currChunkSize:I

.field private ivIn:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->currChunkSize:I

    .line 23
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->ivIn:Ljava/io/InputStream;

    .line 24
    return-void
.end method

.method private ensureChunkAvailable()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    iget v3, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->currChunkSize:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->currChunkRead:I

    iget v4, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->currChunkSize:I

    if-lt v3, v4, :cond_3

    .line 43
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "sizeStr":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 45
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v2, "Missing chunk header"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 47
    :cond_1
    const/16 v3, 0x10

    invoke-static {v0, v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->currChunkSize:I

    .line 48
    iput v2, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->currChunkRead:I

    .line 50
    iget v3, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->currChunkSize:I

    if-nez v3, :cond_2

    .line 52
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->readLine()Ljava/lang/String;

    .line 54
    :cond_2
    iget v3, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->currChunkSize:I

    if-lez v3, :cond_4

    .line 56
    .end local v0    # "sizeStr":Ljava/lang/String;
    :cond_3
    :goto_0
    return v1

    .restart local v0    # "sizeStr":Ljava/lang/String;
    :cond_4
    move v1, v2

    .line 54
    goto :goto_0
.end method

.method private readLine()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 66
    .local v1, "buff":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 68
    .local v2, "state":I
    :cond_0
    iget-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->ivIn:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 69
    .local v0, "b":I
    sparse-switch v0, :sswitch_data_0

    .line 86
    const/4 v2, 0x0

    .line 87
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 90
    :goto_0
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 92
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 71
    :sswitch_0
    if-nez v2, :cond_2

    .line 72
    const/4 v2, 0x1

    .line 77
    :goto_1
    :sswitch_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 78
    const/4 v2, 0x2

    .line 83
    :goto_2
    :sswitch_2
    const/4 v2, -0x1

    .line 84
    goto :goto_0

    .line 74
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 80
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 69
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_2
        0xa -> :sswitch_1
        0xd -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->ivIn:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 98
    return-void
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->ensureChunkAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 28
    const/4 v0, -0x1

    .line 32
    :goto_0
    return v0

    .line 30
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->ivIn:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 31
    .local v0, "b":I
    iget v1, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->currChunkRead:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;->currChunkRead:I

    goto :goto_0
.end method
