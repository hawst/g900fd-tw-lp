.class public Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;
.super Ljava/lang/Object;
.source "Grammar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;
    }
.end annotation


# static fields
.field public static final GRAMMAR_TAG_NAME:Ljava/lang/String; = "Grammar"

.field private static final RULE_PARAM_NAME:Ljava/lang/String; = "rule"


# instance fields
.field private configurationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;",
            ">;"
        }
    .end annotation
.end field

.field private eventsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;",
            ">;"
        }
    .end annotation
.end field

.field private ruleName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->configurationList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->eventsList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->ruleName:Ljava/lang/String;

    return-object p1
.end method

.method public static getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$1;)V

    return-object v0
.end method


# virtual methods
.method public getConfigurationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->configurationList:Ljava/util/List;

    return-object v0
.end method

.method public getEventsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->eventsList:Ljava/util/List;

    return-object v0
.end method

.method public getRuleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->ruleName:Ljava/lang/String;

    return-object v0
.end method
