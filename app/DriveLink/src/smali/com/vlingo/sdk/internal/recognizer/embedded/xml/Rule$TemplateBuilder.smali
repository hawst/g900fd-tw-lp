.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "Rule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TemplateBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Landroid/util/Pair",
        "<",
        "Ljava/lang/String;",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final TEMPLATE_NAME_PARAM_NAME:Ljava/lang/String; = "name"


# instance fields
.field private rule:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$1;

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addChildTagObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "childName"    # Ljava/lang/String;
    .param p2, "tagObject"    # Ljava/lang/Object;

    .prologue
    .line 199
    :try_start_0
    check-cast p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;

    .end local p2    # "tagObject":Ljava/lang/Object;
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateBuilder;->rule:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :goto_0
    return-void

    .line 200
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected addChildrenBuilders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "builders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;>;"
    const-string/jumbo v0, "ResultBlock"

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    return-void
.end method

.method protected addParametersNames(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 192
    .local p1, "paramNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    return-void
.end method

.method protected buildResultObject()Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "name"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateBuilder;->rule:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateBuilder;->buildResultObject()Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateBuilder;->parametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 183
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    const-string/jumbo v0, "TtaTemplate"

    return-object v0
.end method
