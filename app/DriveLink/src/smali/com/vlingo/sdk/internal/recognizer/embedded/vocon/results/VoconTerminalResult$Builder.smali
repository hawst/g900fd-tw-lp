.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
.super Ljava/lang/Object;
.source "VoconTerminalResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private terminal:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

.field private terminalJson:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "termJson"    # Lorg/json/JSONObject;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminal:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    .line 48
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminalJson:Lorg/json/JSONObject;

    .line 49
    return-void
.end method

.method private beginTimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    .locals 2
    .param p1, "beginTime"    # J

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminal:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;->_beginTimeMs:Ljava/lang/Long;

    .line 102
    return-object p0
.end method

.method private confidence(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    .locals 2
    .param p1, "confidence"    # J

    .prologue
    .line 74
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminal:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;->_conf:Ljava/lang/Long;

    .line 75
    return-object p0
.end method

.method private endTimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    .locals 2
    .param p1, "endTime"    # J

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminal:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;->_endTimeMs:Ljava/lang/Long;

    .line 115
    return-object p0
.end method

.method private orthography(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    .locals 1
    .param p1, "orthography"    # Ljava/lang/String;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminal:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;->_orthography:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;Ljava/lang/String;)Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method private score(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    .locals 2
    .param p1, "score"    # J

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminal:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;->_score:Ljava/lang/Long;

    .line 89
    return-object p0
.end method

.method private userId(II)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    .locals 7
    .param p1, "hi32"    # I
    .param p2, "lo32"    # I

    .prologue
    .line 130
    const/4 v0, 0x0

    .line 131
    .local v0, "userId":Ljava/lang/Long;
    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    .line 132
    int-to-long v1, p1

    const/16 v3, 0x20

    shl-long/2addr v1, v3

    int-to-long v3, p2

    const-wide/32 v5, 0xfffffff

    and-long/2addr v3, v5

    or-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminal:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;->_userId:Ljava/lang/Long;

    .line 135
    return-object p0
.end method


# virtual methods
.method public build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 147
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminalJson:Lorg/json/JSONObject;

    if-eqz v2, :cond_0

    .line 150
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminalJson:Lorg/json/JSONObject;

    const-string/jumbo v3, "_orthography"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->orthography(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :try_start_1
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminalJson:Lorg/json/JSONObject;

    const-string/jumbo v3, "_conf"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->confidence(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 166
    :try_start_2
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminalJson:Lorg/json/JSONObject;

    const-string/jumbo v3, "_score"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->score(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 174
    :try_start_3
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminalJson:Lorg/json/JSONObject;

    const-string/jumbo v3, "_beginTimeMs"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->beginTimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    .line 182
    :try_start_4
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminalJson:Lorg/json/JSONObject;

    const-string/jumbo v3, "_endTimeMs"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->endTimeMs(J)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    .line 190
    :try_start_5
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminalJson:Lorg/json/JSONObject;

    const-string/jumbo v2, "_userID.hi32"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminalJson:Lorg/json/JSONObject;

    const-string/jumbo v3, "_userID.lo32"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->userId(II)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    .line 196
    :goto_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult$Builder;->terminal:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTerminalResult;

    .line 198
    :cond_0
    :goto_1
    return-object v1

    .line 151
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Lorg/json/JSONException;
    goto :goto_1

    .line 159
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 162
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 167
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v0

    .line 170
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 175
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_3
    move-exception v0

    .line 178
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 183
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_4
    move-exception v0

    .line 186
    .restart local v0    # "e":Lorg/json/JSONException;
    goto :goto_1

    .line 191
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_5
    move-exception v1

    goto :goto_0
.end method
