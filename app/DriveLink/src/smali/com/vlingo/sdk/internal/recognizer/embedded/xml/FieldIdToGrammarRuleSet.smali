.class public Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;
.super Ljava/lang/Object;
.source "FieldIdToGrammarRuleSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet$FieldIdToGrammarRuleSetBuilder;
    }
.end annotation


# static fields
.field public static final FITG_RULE_SET_TAG_NAME:Ljava/lang/String; = "RuleSet"


# instance fields
.field private fieldId:Ljava/lang/String;

.field private grammarList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;->grammarList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;->fieldId:Ljava/lang/String;

    return-object p1
.end method

.method public static getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet$FieldIdToGrammarRuleSetBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet$FieldIdToGrammarRuleSetBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet$1;)V

    return-object v0
.end method


# virtual methods
.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;->fieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getGrammarList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;->grammarList:Ljava/util/List;

    return-object v0
.end method
