.class public Lcom/vlingo/sdk/internal/http/date/HttpDateParser;
.super Ljava/lang/Object;
.source "HttpDateParser.java"


# static fields
.field private static final months:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "Jan"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "Feb"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "Mar"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "Apr"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "May"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "Jun"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "Jul"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "Aug"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "Sep"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "Oct"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "Nov"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "Dec"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/sdk/internal/http/date/HttpDateParser;->months:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)J
    .locals 15
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 32
    const-wide/16 v4, 0x0

    .line 34
    .local v4, "expires":J
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v13

    const/16 v14, 0x1a

    if-ge v13, v14, :cond_1

    .line 36
    :cond_0
    const-wide/16 v13, 0x0

    .line 111
    :goto_0
    return-wide v13

    .line 38
    :cond_1
    const/4 v13, 0x5

    const/4 v14, 0x7

    invoke-virtual {p0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "day":Ljava/lang/String;
    const/16 v13, 0x8

    const/16 v14, 0xb

    invoke-virtual {p0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 40
    .local v9, "month":Ljava/lang/String;
    const/16 v13, 0xc

    const/16 v14, 0x10

    invoke-virtual {p0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 41
    .local v12, "year":Ljava/lang/String;
    const/16 v13, 0x11

    const/16 v14, 0x13

    invoke-virtual {p0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 42
    .local v6, "hour":Ljava/lang/String;
    const/16 v13, 0x14

    const/16 v14, 0x16

    invoke-virtual {p0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 43
    .local v8, "minute":Ljava/lang/String;
    const/16 v13, 0x17

    const/16 v14, 0x19

    invoke-virtual {p0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 45
    .local v11, "second":Ljava/lang/String;
    const-string/jumbo v13, "GMT"

    invoke-static {v13}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v13

    invoke-static {v13}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 48
    .local v0, "cal":Ljava/util/Calendar;
    const/4 v13, 0x5

    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v0, v13, v14}, Ljava/util/Calendar;->set(II)V

    .line 49
    const/4 v13, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v0, v13, v14}, Ljava/util/Calendar;->set(II)V

    .line 50
    const/16 v13, 0xb

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v0, v13, v14}, Ljava/util/Calendar;->set(II)V

    .line 51
    const/16 v13, 0xc

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v0, v13, v14}, Ljava/util/Calendar;->set(II)V

    .line 52
    const/16 v13, 0xd

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v0, v13, v14}, Ljava/util/Calendar;->set(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    const/4 v10, -0x1

    .line 59
    .local v10, "numMonth":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    sget-object v13, Lcom/vlingo/sdk/internal/http/date/HttpDateParser;->months:[Ljava/lang/String;

    array-length v13, v13

    if-ge v7, v13, :cond_2

    .line 60
    sget-object v13, Lcom/vlingo/sdk/internal/http/date/HttpDateParser;->months:[Ljava/lang/String;

    aget-object v13, v13, v7

    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 61
    move v10, v7

    .line 66
    :cond_2
    const/4 v1, -0x1

    .line 67
    .local v1, "calMonth":I
    packed-switch v10, :pswitch_data_0

    .line 106
    const-wide/16 v13, 0x0

    goto :goto_0

    .line 53
    .end local v1    # "calMonth":I
    .end local v7    # "i":I
    .end local v10    # "numMonth":I
    :catch_0
    move-exception v3

    .line 55
    .local v3, "ex":Ljava/lang/NumberFormatException;
    const-wide/16 v13, 0x0

    goto/16 :goto_0

    .line 59
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    .restart local v7    # "i":I
    .restart local v10    # "numMonth":I
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 69
    .restart local v1    # "calMonth":I
    :pswitch_0
    const/4 v1, 0x0

    .line 108
    :goto_2
    const/4 v13, 0x2

    invoke-virtual {v0, v13, v1}, Ljava/util/Calendar;->set(II)V

    .line 109
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-wide v13, v4

    .line 111
    goto/16 :goto_0

    .line 72
    :pswitch_1
    const/4 v1, 0x1

    .line 73
    goto :goto_2

    .line 75
    :pswitch_2
    const/4 v1, 0x2

    .line 76
    goto :goto_2

    .line 78
    :pswitch_3
    const/4 v1, 0x3

    .line 79
    goto :goto_2

    .line 81
    :pswitch_4
    const/4 v1, 0x4

    .line 82
    goto :goto_2

    .line 84
    :pswitch_5
    const/4 v1, 0x5

    .line 85
    goto :goto_2

    .line 87
    :pswitch_6
    const/4 v1, 0x6

    .line 88
    goto :goto_2

    .line 90
    :pswitch_7
    const/4 v1, 0x7

    .line 91
    goto :goto_2

    .line 93
    :pswitch_8
    const/16 v1, 0x8

    .line 94
    goto :goto_2

    .line 96
    :pswitch_9
    const/16 v1, 0x9

    .line 97
    goto :goto_2

    .line 99
    :pswitch_a
    const/16 v1, 0xa

    .line 100
    goto :goto_2

    .line 102
    :pswitch_b
    const/16 v1, 0xb

    .line 103
    goto :goto_2

    .line 67
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
