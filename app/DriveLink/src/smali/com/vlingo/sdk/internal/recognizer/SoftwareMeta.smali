.class public Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
.super Ljava/lang/Object;
.source "SoftwareMeta.java"


# static fields
.field private static allowsLocationInstance:Lcom/vlingo/sdk/util/AllowsLocation;

.field private static instance:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;


# instance fields
.field appid:Ljava/lang/String;

.field name:Ljava/lang/String;

.field salesCode:Ljava/lang/String;

.field version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->instance:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .line 20
    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->allowsLocationInstance:Lcom/vlingo/sdk/util/AllowsLocation;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->instance:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .line 36
    return-void
.end method

.method public static getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->instance:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->instance:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .line 31
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->instance:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    return-object v0
.end method

.method public static setAllowsLocationInstance(Lcom/vlingo/sdk/util/AllowsLocation;)V
    .locals 0
    .param p0, "instance"    # Lcom/vlingo/sdk/util/AllowsLocation;

    .prologue
    .line 39
    sput-object p0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->allowsLocationInstance:Lcom/vlingo/sdk/util/AllowsLocation;

    .line 40
    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->appid:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSalesCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->salesCode:Ljava/lang/String;

    return-object v0
.end method

.method public getSdkName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string/jumbo v0, "AndroidSDK"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->VERSION:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->version:Ljava/lang/String;

    return-object v0
.end method

.method public isChinesePhone()Z
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->allowsLocationInstance:Lcom/vlingo/sdk/util/AllowsLocation;

    invoke-interface {v0}, Lcom/vlingo/sdk/util/AllowsLocation;->isChinesePhone()Z

    move-result v0

    return v0
.end method

.method public setAppId(Ljava/lang/String;)V
    .locals 0
    .param p1, "appid"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->appid:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->name:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "appVersion"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->version:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setSalesCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->salesCode:Ljava/lang/String;

    .line 56
    return-void
.end method
