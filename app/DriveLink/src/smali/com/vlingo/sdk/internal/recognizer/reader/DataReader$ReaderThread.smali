.class Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;
.super Ljava/lang/Thread;
.source "DataReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReaderThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)V
    .locals 1

    .prologue
    .line 215
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    .line 216
    const-string/jumbo v0, "ReaderThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 217
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 219
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->access$000(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 220
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->access$000(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    move-result-object v1

    const-string/jumbo v2, "STARTED"

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;->recordAndTimeStampEvent(Ljava/lang/String;)V

    .line 223
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->access$100(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;->onStarted()V

    .line 225
    :goto_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mIsStopped:Z
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->access$200(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 227
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->onProcessData()V

    .line 228
    const-wide/16 v1, 0x1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 229
    :catch_0
    move-exception v1

    goto :goto_0

    .line 231
    :catch_1
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->access$300()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ReaderThread exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;->READ_ERROR:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->onError(Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;Ljava/lang/String;)V

    goto :goto_0

    .line 243
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->access$100(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTotalDuration:I
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->access$400(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)I

    move-result v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->isSpeechDetected()Z

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;->onStopped(IZ)V

    .line 245
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->onDeinit()V

    .line 247
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mInputStream:Ljava/io/InputStream;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->access$500(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Ljava/io/InputStream;

    move-result-object v1

    instance-of v1, v1, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;

    if-eqz v1, :cond_2

    .line 249
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->this$0:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mInputStream:Ljava/io/InputStream;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->access$500(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 255
    :cond_2
    :goto_1
    return-void

    .line 250
    :catch_2
    move-exception v1

    goto :goto_1
.end method
