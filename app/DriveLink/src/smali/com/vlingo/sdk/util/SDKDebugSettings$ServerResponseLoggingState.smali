.class public final enum Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;
.super Ljava/lang/Enum;
.source "SDKDebugSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/util/SDKDebugSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ServerResponseLoggingState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

.field public static final enum NONE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

.field public static final enum REPLAY:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

.field public static final enum SAVE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

.field private static final lookup:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;",
            ">;"
        }
    .end annotation
.end field

.field private static stringValues:[Ljava/lang/String;


# instance fields
.field private code:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 18
    new-instance v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    const-string/jumbo v6, "NONE"

    const-string/jumbo v7, "None"

    invoke-direct {v5, v6, v8, v7}, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->NONE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    .line 19
    new-instance v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    const-string/jumbo v6, "SAVE"

    const-string/jumbo v7, "Save"

    invoke-direct {v5, v6, v9, v7}, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->SAVE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    .line 20
    new-instance v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    const-string/jumbo v6, "REPLAY"

    const-string/jumbo v7, "Replay"

    invoke-direct {v5, v6, v10, v7}, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->REPLAY:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    .line 17
    const/4 v5, 0x3

    new-array v5, v5, [Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    sget-object v6, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->NONE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    aput-object v6, v5, v8

    sget-object v6, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->SAVE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    aput-object v6, v5, v9

    sget-object v6, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->REPLAY:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    aput-object v6, v5, v10

    sput-object v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->$VALUES:[Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    .line 22
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->lookup:Ljava/util/Map;

    .line 29
    const-class v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    invoke-static {v5}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 30
    .local v0, "all":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;>;"
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    sput-object v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->stringValues:[Ljava/lang/String;

    .line 31
    const/4 v1, 0x0

    .line 32
    .local v1, "i":I
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    .line 33
    .local v4, "s":Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;
    sget-object v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->stringValues:[Ljava/lang/String;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-virtual {v4}, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->getCode()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 34
    sget-object v5, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->lookup:Ljava/util/Map;

    invoke-virtual {v4}, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->getCode()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 36
    .end local v4    # "s":Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "code"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput-object p3, p0, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->code:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public static get(Ljava/lang/String;)Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;
    .locals 1
    .param p0, "code"    # Ljava/lang/String;

    .prologue
    .line 45
    sget-object v0, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->lookup:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    return-object v0
.end method

.method public static getStringValues()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->stringValues:[Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->$VALUES:[Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    return-object v0
.end method


# virtual methods
.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->code:Ljava/lang/String;

    return-object v0
.end method
