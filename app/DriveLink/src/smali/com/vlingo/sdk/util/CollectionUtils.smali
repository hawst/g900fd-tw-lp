.class public final Lcom/vlingo/sdk/util/CollectionUtils;
.super Ljava/lang/Object;
.source "CollectionUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static find(Ljava/lang/Iterable;Lcom/vlingo/sdk/util/Predicate;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TT;>;",
            "Lcom/vlingo/sdk/util/Predicate",
            "<-TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "collection":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+TT;>;"
    .local p1, "predicate":Lcom/vlingo/sdk/util/Predicate;, "Lcom/vlingo/sdk/util/Predicate<-TT;>;"
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 26
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 27
    .local v1, "item":Ljava/lang/Object;, "TT;"
    invoke-interface {p1, v1}, Lcom/vlingo/sdk/util/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 32
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isEmpty(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "coll":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static select(Ljava/lang/Iterable;Lcom/vlingo/sdk/util/Predicate;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TT;>;",
            "Lcom/vlingo/sdk/util/Predicate",
            "<-TT;>;",
            "Ljava/util/Collection",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p0, "input":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+TT;>;"
    .local p1, "predicate":Lcom/vlingo/sdk/util/Predicate;, "Lcom/vlingo/sdk/util/Predicate<-TT;>;"
    .local p2, "outputCollection":Ljava/util/Collection;, "Ljava/util/Collection<-TT;>;"
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 61
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 62
    .local v1, "item":Ljava/lang/Object;, "TT;"
    invoke-interface {p1, v1}, Lcom/vlingo/sdk/util/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Ljava/lang/Object;, "TT;"
    :cond_1
    return-void
.end method
