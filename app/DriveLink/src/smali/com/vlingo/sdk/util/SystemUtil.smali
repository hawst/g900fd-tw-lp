.class public final Lcom/vlingo/sdk/util/SystemUtil;
.super Ljava/lang/Object;
.source "SystemUtil.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SystemUtil"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static libraryIsOk(Ljava/lang/String;)Z
    .locals 7
    .param p0, "absolutePath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 55
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 56
    .local v2, "libFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 57
    .local v0, "length":J
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 58
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "libraryIsOk file \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\' does not exist"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 59
    .local v3, "msg":Ljava/lang/String;
    const-string/jumbo v5, "SystemUtil"

    invoke-static {v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    .end local v3    # "msg":Ljava/lang/String;
    :goto_0
    return v4

    .line 61
    :cond_0
    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-nez v5, :cond_1

    .line 62
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "libraryIsOk file \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\' has 0 length"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 63
    .restart local v3    # "msg":Ljava/lang/String;
    const-string/jumbo v5, "SystemUtil"

    invoke-static {v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 69
    .end local v3    # "msg":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public static loadLibrary(Ljava/lang/String;)V
    .locals 7
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 28
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 29
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Ljava/io/File;

    const-string/jumbo v3, "vlsdk_lib"

    invoke-virtual {v0, v3, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 30
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 31
    sget-object v3, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string/jumbo v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 32
    new-instance v1, Ljava/io/File;

    .end local v1    # "dir":Ljava/io/File;
    const-string/jumbo v3, "vlsdk_lib"

    invoke-virtual {v0, v3, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string/jumbo v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v6

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 35
    .restart local v1    # "dir":Ljava/io/File;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 37
    .local v2, "libPath":Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/vlingo/sdk/util/SystemUtil;->verifyLibrary(Landroid/content/Context;Ljava/lang/String;)V

    .line 41
    invoke-static {v2}, Ljava/lang/System;->load(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method private static reExtract(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 75
    const-string/jumbo v2, "vlsdk_lib"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 77
    .local v0, "libDir":Ljava/io/File;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->isSensory2Using()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    const-string/jumbo v1, "vlsdk_lib_s2.0.zip"

    .line 85
    .local v1, "zipFileName":Ljava/lang/String;
    :goto_0
    :try_start_0
    invoke-static {v1, v0, p1}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractFileFromAssetZip(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_1
    return-void

    .line 80
    .end local v1    # "zipFileName":Ljava/lang/String;
    :cond_0
    const-string/jumbo v1, "vlsdk_lib.zip"

    .restart local v1    # "zipFileName":Ljava/lang/String;
    goto :goto_0

    .line 88
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public static verifyLibrary(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {p1}, Lcom/vlingo/sdk/util/SystemUtil;->libraryIsOk(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    invoke-static {p0, p1}, Lcom/vlingo/sdk/util/SystemUtil;->reExtract(Landroid/content/Context;Ljava/lang/String;)V

    .line 50
    invoke-static {p1}, Lcom/vlingo/sdk/util/SystemUtil;->libraryIsOk(Ljava/lang/String;)Z

    .line 52
    :cond_0
    return-void
.end method
