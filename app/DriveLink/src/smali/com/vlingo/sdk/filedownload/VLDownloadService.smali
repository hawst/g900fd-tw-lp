.class public interface abstract Lcom/vlingo/sdk/filedownload/VLDownloadService;
.super Ljava/lang/Object;
.source "VLDownloadService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;
    }
.end annotation


# virtual methods
.method public abstract addListener(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Lcom/vlingo/sdk/filedownload/VLDownloadListener;
.end method

.method public abstract download(Lcom/vlingo/sdk/filedownload/VLDownloadData;)Ljava/lang/Long;
.end method

.method public abstract download(Lcom/vlingo/sdk/filedownload/VLDownloadData;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Ljava/lang/Long;
.end method

.method public abstract getListener(Ljava/lang/Long;)Lcom/vlingo/sdk/filedownload/VLDownloadListener;
.end method

.method public abstract getStatus(Ljava/lang/Long;)Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;
.end method

.method public abstract registerFilter(Lcom/vlingo/sdk/filedownload/VLDownloadFilter;)V
.end method

.method public abstract remove(Ljava/lang/Long;)Z
.end method

.method public abstract removeFilter(Lcom/vlingo/sdk/filedownload/VLDownloadFilter;)Z
.end method

.method public abstract removeListener(Ljava/lang/Long;)Z
.end method
