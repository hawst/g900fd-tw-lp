.class public final Lcom/vlingo/sdk/filedownload/VLDownloadData;
.super Ljava/lang/Object;
.source "VLDownloadData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/filedownload/VLDownloadData$1;,
        Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;
    }
.end annotation


# instance fields
.field private checkSum:Ljava/lang/String;

.field private hashAlgorithm:Ljava/lang/String;

.field private localFileName:Ljava/lang/String;

.field private uri:Ljava/lang/String;

.field private visible:Z

.field private wifiOnly:Z


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    # getter for: Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->uri:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->access$000(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->uri:Ljava/lang/String;

    .line 49
    # getter for: Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->localFileName:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->access$100(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->localFileName:Ljava/lang/String;

    .line 50
    # getter for: Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->checkSum:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->access$200(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->checkSum:Ljava/lang/String;

    .line 51
    # getter for: Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->visible:Z
    invoke-static {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->access$300(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->visible:Z

    .line 52
    # getter for: Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->wifiOnly:Z
    invoke-static {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->access$400(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->wifiOnly:Z

    .line 53
    # getter for: Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->hashAlgorithm:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->access$500(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->hashAlgorithm:Ljava/lang/String;

    .line 54
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;Lcom/vlingo/sdk/filedownload/VLDownloadData$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;
    .param p2, "x1"    # Lcom/vlingo/sdk/filedownload/VLDownloadData$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/filedownload/VLDownloadData;-><init>(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)V

    return-void
.end method

.method public static getDataBuilder()Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;

    invoke-direct {v0}, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getCheckSum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->checkSum:Ljava/lang/String;

    return-object v0
.end method

.method public getHashAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->hashAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->localFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->uri:Ljava/lang/String;

    return-object v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->visible:Z

    return v0
.end method

.method public isWifiOnly()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData;->wifiOnly:Z

    return v0
.end method
