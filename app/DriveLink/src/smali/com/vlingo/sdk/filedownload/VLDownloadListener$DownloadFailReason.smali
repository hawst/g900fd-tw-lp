.class public final enum Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;
.super Ljava/lang/Enum;
.source "VLDownloadListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/filedownload/VLDownloadListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DownloadFailReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

.field public static final enum DOWNLOADED_FILE_NOT_FOUND:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

.field private static final DOWNLOAD_REASONS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ERROR_CANNOT_RESUME:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

.field public static final enum ERROR_DEVICE_NOT_FOUND:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

.field public static final enum ERROR_FILE_ERROR:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

.field public static final enum ERROR_HTTP_DATA_ERROR:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

.field public static final enum ERROR_INSUFFICIENT_SPACE:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

.field public static final enum ERROR_TOO_MANY_REDIRECTS:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

.field public static final enum ERROR_UNHANDLED_HTTP_CODE:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

.field public static final enum ERROR_UNKNOWN:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;


# instance fields
.field private index:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x3f0

    const/16 v5, 0x8

    .line 47
    new-instance v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    const-string/jumbo v2, "ERROR_CANNOT_RESUME"

    invoke-direct {v1, v2, v7, v6}, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_CANNOT_RESUME:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .line 48
    new-instance v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    const-string/jumbo v2, "ERROR_DEVICE_NOT_FOUND"

    invoke-direct {v1, v2, v8, v6}, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_DEVICE_NOT_FOUND:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .line 49
    new-instance v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    const-string/jumbo v2, "ERROR_FILE_ERROR"

    const/16 v3, 0x3e9

    invoke-direct {v1, v2, v9, v3}, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_FILE_ERROR:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .line 50
    new-instance v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    const-string/jumbo v2, "ERROR_HTTP_DATA_ERROR"

    const/4 v3, 0x3

    const/16 v4, 0x3ec

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_HTTP_DATA_ERROR:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .line 51
    new-instance v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    const-string/jumbo v2, "ERROR_INSUFFICIENT_SPACE"

    const/4 v3, 0x4

    const/16 v4, 0x3ee

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_INSUFFICIENT_SPACE:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .line 52
    new-instance v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    const-string/jumbo v2, "ERROR_TOO_MANY_REDIRECTS"

    const/4 v3, 0x5

    const/16 v4, 0x3ed

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_TOO_MANY_REDIRECTS:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .line 53
    new-instance v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    const-string/jumbo v2, "ERROR_UNHANDLED_HTTP_CODE"

    const/4 v3, 0x6

    const/16 v4, 0x3ea

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_UNHANDLED_HTTP_CODE:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .line 54
    new-instance v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    const-string/jumbo v2, "ERROR_UNKNOWN"

    const/4 v3, 0x7

    const/16 v4, 0x3e8

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_UNKNOWN:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .line 55
    new-instance v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    const-string/jumbo v2, "DOWNLOADED_FILE_NOT_FOUND"

    invoke-direct {v1, v2, v5}, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->DOWNLOADED_FILE_NOT_FOUND:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .line 46
    const/16 v1, 0x9

    new-array v1, v1, [Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_CANNOT_RESUME:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    aput-object v2, v1, v7

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_DEVICE_NOT_FOUND:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    aput-object v2, v1, v8

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_FILE_ERROR:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    aput-object v2, v1, v9

    const/4 v2, 0x3

    sget-object v3, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_HTTP_DATA_ERROR:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_INSUFFICIENT_SPACE:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_TOO_MANY_REDIRECTS:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_UNHANDLED_HTTP_CODE:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_UNKNOWN:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    aput-object v3, v1, v2

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->DOWNLOADED_FILE_NOT_FOUND:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    aput-object v2, v1, v5

    sput-object v1, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->$VALUES:[Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v5}, Ljava/util/HashMap;-><init>(I)V

    .line 62
    .local v0, "reasons":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;>;"
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_CANNOT_RESUME:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const/16 v1, 0x3ef

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_DEVICE_NOT_FOUND:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const/16 v1, 0x3e9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_FILE_ERROR:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const/16 v1, 0x3ec

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_HTTP_DATA_ERROR:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const/16 v1, 0x3ee

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_INSUFFICIENT_SPACE:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    const/16 v1, 0x3ed

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_TOO_MANY_REDIRECTS:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    const/16 v1, 0x3ea

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_UNHANDLED_HTTP_CODE:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const/16 v1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->ERROR_UNKNOWN:Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sput-object v0, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->DOWNLOAD_REASONS:Ljava/util/Map;

    .line 71
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 79
    iput p3, p0, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->index:I

    .line 80
    return-void
.end method

.method public static getByIndex(I)Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;
    .locals 2
    .param p0, "index"    # I

    .prologue
    .line 83
    sget-object v0, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->DOWNLOAD_REASONS:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 46
    const-class v0, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->$VALUES:[Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;

    return-object v0
.end method
