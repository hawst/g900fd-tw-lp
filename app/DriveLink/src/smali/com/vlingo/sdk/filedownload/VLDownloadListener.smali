.class public interface abstract Lcom/vlingo/sdk/filedownload/VLDownloadListener;
.super Ljava/lang/Object;
.source "VLDownloadListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;
    }
.end annotation


# virtual methods
.method public abstract onCheckSumFailed(Lcom/vlingo/sdk/filedownload/VLDownloadData;Ljava/lang/String;)V
.end method

.method public abstract onDownloadFailed(Lcom/vlingo/sdk/filedownload/VLDownloadListener$DownloadFailReason;)V
.end method

.method public abstract onDownloadSuccessful(Ljava/lang/String;)V
.end method
