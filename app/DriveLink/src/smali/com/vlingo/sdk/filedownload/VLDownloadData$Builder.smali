.class public Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;
.super Ljava/lang/Object;
.source "VLDownloadData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/filedownload/VLDownloadData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private checkSum:Ljava/lang/String;

.field private hashAlgorithm:Ljava/lang/String;

.field private localFileName:Ljava/lang/String;

.field private uri:Ljava/lang/String;

.field private visible:Z

.field private wifiOnly:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->visible:Z

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->wifiOnly:Z

    .line 93
    const-string/jumbo v0, "MD5"

    iput-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->hashAlgorithm:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->uri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->localFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->checkSum:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->visible:Z

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->wifiOnly:Z

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->hashAlgorithm:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/vlingo/sdk/filedownload/VLDownloadData;
    .locals 3

    .prologue
    .line 126
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->uri:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Download uri should be initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->localFileName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->uri:Ljava/lang/String;

    const-string/jumbo v1, "^(.*[\\\\\\/])"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\\?(.*)$"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->localFileName:Ljava/lang/String;

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->localFileName:Ljava/lang/String;

    const-string/jumbo v1, "^[\\w,\\s-]+(\\.\\d)*\\.[A-Za-z]{3}$"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 137
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "localFileName value isn\'t a file name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_2
    new-instance v0, Lcom/vlingo/sdk/filedownload/VLDownloadData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/filedownload/VLDownloadData;-><init>(Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;Lcom/vlingo/sdk/filedownload/VLDownloadData$1;)V

    return-object v0
.end method

.method public checkSum(Ljava/lang/String;)Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;
    .locals 0
    .param p1, "checkSum"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->checkSum:Ljava/lang/String;

    .line 107
    return-object p0
.end method

.method public hashAlgorithm(Ljava/lang/String;)Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;
    .locals 0
    .param p1, "hashAlgorithm"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->hashAlgorithm:Ljava/lang/String;

    .line 122
    return-object p0
.end method

.method public localFileName(Ljava/lang/String;)Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;
    .locals 0
    .param p1, "localFileName"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->localFileName:Ljava/lang/String;

    .line 102
    return-object p0
.end method

.method public url(Ljava/lang/String;)Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->uri:Ljava/lang/String;

    .line 97
    return-object p0
.end method

.method public visible(Z)Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->visible:Z

    .line 112
    return-object p0
.end method

.method public wifiOnly(Z)Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;
    .locals 0
    .param p1, "wifiOnly"    # Z

    .prologue
    .line 116
    iput-boolean p1, p0, Lcom/vlingo/sdk/filedownload/VLDownloadData$Builder;->wifiOnly:Z

    .line 117
    return-object p0
.end method
