.class public Lcom/vlingo/workflow/util/JXPathUtil;
.super Ljava/lang/Object;
.source "JXPathUtil.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/vlingo/workflow/util/JXPathUtil;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/util/JXPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFromJXPath(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p0, "pathRoot"    # Ljava/lang/Object;
    .param p1, "xpath"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-static {p0}, Lorg/apache/commons/jxpath/JXPathContext;->newContext(Ljava/lang/Object;)Lorg/apache/commons/jxpath/JXPathContext;

    move-result-object v0

    .line 33
    .local v0, "context":Lorg/apache/commons/jxpath/JXPathContext;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/commons/jxpath/JXPathContext;->setLenient(Z)V

    .line 34
    invoke-virtual {v0, p1}, Lorg/apache/commons/jxpath/JXPathContext;->getValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public static setFromJXPathWithException(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Lorg/apache/commons/jxpath/AbstractFactory;)V
    .locals 5
    .param p0, "pathRoot"    # Ljava/lang/Object;
    .param p1, "xpath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;
    .param p3, "factory"    # Lorg/apache/commons/jxpath/AbstractFactory;

    .prologue
    .line 38
    invoke-static {p0}, Lorg/apache/commons/jxpath/JXPathContext;->newContext(Ljava/lang/Object;)Lorg/apache/commons/jxpath/JXPathContext;

    move-result-object v0

    .line 39
    .local v0, "context":Lorg/apache/commons/jxpath/JXPathContext;
    if-eqz p3, :cond_0

    .line 40
    invoke-virtual {v0, p3}, Lorg/apache/commons/jxpath/JXPathContext;->setFactory(Lorg/apache/commons/jxpath/AbstractFactory;)V

    .line 43
    :cond_0
    :try_start_0
    invoke-virtual {v0, p1, p2}, Lorg/apache/commons/jxpath/JXPathContext;->createPathAndSetValue(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/commons/jxpath/Pointer;
    :try_end_0
    .catch Lorg/apache/commons/jxpath/JXPathException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    return-void

    .line 44
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Lorg/apache/commons/jxpath/JXPathException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Error evaluating \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\' on "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
