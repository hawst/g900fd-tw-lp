.class public abstract Lcom/vlingo/workflow/task/WorkflowTask;
.super Lcom/vlingo/common/util/RunnableBase;
.source "WorkflowTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/vlingo/workflow/ServletEnvironmentInterface;",
        ">",
        "Lcom/vlingo/common/util/RunnableBase;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/vlingo/workflow/model/VLResult;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final ivLogger:Lorg/apache/log4j/Logger;


# instance fields
.field private inputParametersMap:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/ParameterMapping;",
            ">;"
        }
    .end annotation
.end field

.field private ivEnv:Lcom/vlingo/workflow/task/TaskEnv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vlingo/workflow/task/TaskEnv",
            "<TT;>;"
        }
    .end annotation
.end field

.field private ivEssential:Z

.field private ivParams:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/TaskParam;",
            ">;"
        }
    .end annotation
.end field

.field private ivResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/workflow/model/VLResult;",
            ">;"
        }
    .end annotation
.end field

.field private referencedTaskIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private referencedTasks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/workflow/task/WorkflowTask;",
            ">;"
        }
    .end annotation
.end field

.field private responsePart:Lcom/vlingo/workflow/response/ResponsePartEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vlingo/workflow/task/WorkflowTask;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/task/WorkflowTask;->ivLogger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/vlingo/workflow/task/WorkflowTask;, "Lcom/vlingo/workflow/task/WorkflowTask<TT;>;"
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Lcom/vlingo/common/util/RunnableBase;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/workflow/task/WorkflowTask;->ivResults:Ljava/util/List;

    .line 25
    iput-object v1, p0, Lcom/vlingo/workflow/task/WorkflowTask;->referencedTasks:Ljava/util/HashMap;

    .line 26
    iput-object v1, p0, Lcom/vlingo/workflow/task/WorkflowTask;->referencedTaskIds:Ljava/util/HashSet;

    .line 27
    iput-object v1, p0, Lcom/vlingo/workflow/task/WorkflowTask;->inputParametersMap:Ljava/util/Set;

    .line 28
    sget-object v0, Lcom/vlingo/workflow/response/ResponsePartEnum;->None:Lcom/vlingo/workflow/response/ResponsePartEnum;

    iput-object v0, p0, Lcom/vlingo/workflow/task/WorkflowTask;->responsePart:Lcom/vlingo/workflow/response/ResponsePartEnum;

    return-void
.end method

.method private setInputParameters()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vlingo/workflow/task/WorkflowTask;, "Lcom/vlingo/workflow/task/WorkflowTask<TT;>;"
    const/4 v12, 0x0

    .line 162
    sget-object v9, Lcom/vlingo/workflow/task/WorkflowTask;->ivLogger:Lorg/apache/log4j/Logger;

    const-string/jumbo v10, "Setting input parameters."

    invoke-virtual {v9, v10}, Lorg/apache/log4j/Logger;->debug(Ljava/lang/Object;)V

    .line 163
    iget-object v9, p0, Lcom/vlingo/workflow/task/WorkflowTask;->inputParametersMap:Ljava/util/Set;

    if-nez v9, :cond_1

    .line 195
    :cond_0
    return-void

    .line 165
    :cond_1
    iget-object v9, p0, Lcom/vlingo/workflow/task/WorkflowTask;->inputParametersMap:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/workflow/model/ParameterMapping;

    .line 166
    .local v3, "parameterMapping":Lcom/vlingo/workflow/model/ParameterMapping;
    invoke-virtual {v3}, Lcom/vlingo/workflow/model/ParameterMapping;->getName()Ljava/lang/String;

    move-result-object v2

    .line 167
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/vlingo/workflow/model/ParameterMapping;->getExpression()Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "expression":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/vlingo/workflow/model/ParameterMapping;->getProperty()Ljava/lang/String;

    move-result-object v5

    .line 169
    .local v5, "property":Ljava/lang/String;
    const/4 v8, 0x0

    .line 170
    .local v8, "value":Ljava/lang/Object;
    if-eqz v0, :cond_7

    .line 171
    const/4 v7, 0x0

    .line 172
    .local v7, "taskId":Ljava/lang/String;
    const/4 v4, 0x0

    .line 173
    .local v4, "path":Ljava/lang/String;
    const-string/jumbo v9, "/"

    invoke-virtual {v0, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 174
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const-string/jumbo v11, "/"

    invoke-virtual {v0, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 175
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "/"

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 178
    :goto_1
    iget-object v9, p0, Lcom/vlingo/workflow/task/WorkflowTask;->referencedTasks:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/workflow/task/WorkflowTask;

    .line 179
    .local v6, "task":Lcom/vlingo/workflow/task/WorkflowTask;
    if-eqz v4, :cond_4

    .line 180
    invoke-virtual {v6}, Lcom/vlingo/workflow/task/WorkflowTask;->isEssential()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 181
    sget-object v9, Lcom/vlingo/workflow/task/WorkflowTask;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Waiting for task: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " to finish."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/apache/log4j/Logger;->debug(Ljava/lang/Object;)V

    .line 182
    invoke-virtual {v6}, Lcom/vlingo/workflow/task/WorkflowTask;->waitToFinish()V

    .line 184
    :cond_3
    invoke-static {v6, v4}, Lcom/vlingo/workflow/util/JXPathUtil;->getFromJXPath(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .line 186
    .end local v8    # "value":Ljava/lang/Object;
    :cond_4
    if-nez v4, :cond_5

    .line 187
    move-object v8, v6

    .line 188
    :cond_5
    invoke-static {p0, v2, v8, v12}, Lcom/vlingo/workflow/util/JXPathUtil;->setFromJXPathWithException(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Lorg/apache/commons/jxpath/AbstractFactory;)V

    goto/16 :goto_0

    .line 177
    .end local v6    # "task":Lcom/vlingo/workflow/task/WorkflowTask;
    .restart local v8    # "value":Ljava/lang/Object;
    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 189
    .end local v4    # "path":Ljava/lang/String;
    .end local v7    # "taskId":Ljava/lang/String;
    :cond_7
    invoke-virtual {v3}, Lcom/vlingo/workflow/model/ParameterMapping;->getValue()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_8

    .line 190
    invoke-virtual {v3}, Lcom/vlingo/workflow/model/ParameterMapping;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v2, v9, v12}, Lcom/vlingo/workflow/util/JXPathUtil;->setFromJXPathWithException(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Lorg/apache/commons/jxpath/AbstractFactory;)V

    goto/16 :goto_0

    .line 191
    :cond_8
    if-eqz v5, :cond_2

    .line 192
    invoke-direct {p0, v2, v5}, Lcom/vlingo/workflow/task/WorkflowTask;->setParameterValueFromProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private setParameterValueFromProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "property"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vlingo/workflow/task/WorkflowTask;, "Lcom/vlingo/workflow/task/WorkflowTask<TT;>;"
    const/4 v7, 0x0

    .line 198
    const/4 v0, 0x0

    .line 199
    .local v0, "klass":Ljava/lang/String;
    const/4 v1, 0x0

    .line 200
    .local v1, "path":Ljava/lang/String;
    const-string/jumbo v4, "/"

    invoke-virtual {p2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 201
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string/jumbo v6, "/"

    invoke-virtual {p2, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 202
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {p2, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 205
    :goto_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 206
    .local v2, "propClass":Ljava/lang/Class;
    if-nez v1, :cond_1

    .line 207
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    invoke-static {p0, p1, v4, v7}, Lcom/vlingo/workflow/util/JXPathUtil;->setFromJXPathWithException(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Lorg/apache/commons/jxpath/AbstractFactory;)V

    .line 212
    :goto_1
    return-void

    .line 204
    .end local v2    # "propClass":Ljava/lang/Class;
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 209
    .restart local v2    # "propClass":Ljava/lang/Class;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/vlingo/workflow/util/JXPathUtil;->getFromJXPath(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 210
    .local v3, "value":Ljava/lang/Object;
    invoke-static {p0, p1, v3, v7}, Lcom/vlingo/workflow/util/JXPathUtil;->setFromJXPathWithException(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Lorg/apache/commons/jxpath/AbstractFactory;)V

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lcom/vlingo/workflow/task/WorkflowTask;, "Lcom/vlingo/workflow/task/WorkflowTask<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/workflow/task/WorkflowTask;->call()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/workflow/model/VLResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "this":Lcom/vlingo/workflow/task/WorkflowTask;, "Lcom/vlingo/workflow/task/WorkflowTask<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/workflow/task/WorkflowTask;->run()V

    .line 76
    invoke-virtual {p0}, Lcom/vlingo/workflow/task/WorkflowTask;->getNumTimesFinished()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 77
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 79
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/workflow/task/WorkflowTask;->getThrowable()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/vlingo/workflow/task/WorkflowTask;->ivResults:Ljava/util/List;

    return-object v0

    .line 82
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/workflow/task/WorkflowTask;->getThrowable()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    throw v0
.end method

.method public isEssential()Z
    .locals 1

    .prologue
    .line 113
    .local p0, "this":Lcom/vlingo/workflow/task/WorkflowTask;, "Lcom/vlingo/workflow/task/WorkflowTask<TT;>;"
    iget-boolean v0, p0, Lcom/vlingo/workflow/task/WorkflowTask;->ivEssential:Z

    return v0
.end method

.method public abstract process(Lcom/vlingo/workflow/task/TaskEnv;Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/workflow/task/TaskEnv",
            "<TT;>;",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/TaskParam;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public runImpl()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    .local p0, "this":Lcom/vlingo/workflow/task/WorkflowTask;, "Lcom/vlingo/workflow/task/WorkflowTask<TT;>;"
    sget-object v0, Lcom/vlingo/workflow/task/WorkflowTask;->ivLogger:Lorg/apache/log4j/Logger;

    const-string/jumbo v1, "Start task"

    invoke-virtual {v0, v1}, Lorg/apache/log4j/Logger;->debug(Ljava/lang/Object;)V

    .line 88
    invoke-direct {p0}, Lcom/vlingo/workflow/task/WorkflowTask;->setInputParameters()V

    .line 89
    iget-object v0, p0, Lcom/vlingo/workflow/task/WorkflowTask;->ivEnv:Lcom/vlingo/workflow/task/TaskEnv;

    iget-object v1, p0, Lcom/vlingo/workflow/task/WorkflowTask;->ivParams:Ljava/util/Set;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/workflow/task/WorkflowTask;->process(Lcom/vlingo/workflow/task/TaskEnv;Ljava/util/Set;)V

    .line 90
    sget-object v0, Lcom/vlingo/workflow/task/WorkflowTask;->ivLogger:Lorg/apache/log4j/Logger;

    const-string/jumbo v1, "End task"

    invoke-virtual {v0, v1}, Lorg/apache/log4j/Logger;->debug(Ljava/lang/Object;)V

    .line 91
    return-void
.end method

.method public waitToFinish()V
    .locals 8

    .prologue
    .line 98
    .local p0, "this":Lcom/vlingo/workflow/task/WorkflowTask;, "Lcom/vlingo/workflow/task/WorkflowTask<TT;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/vlingo/workflow/task/WorkflowTask;->getCreatedTime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 99
    .local v0, "timeAlive":J
    const-wide/32 v2, 0x1d4c0

    .line 100
    .local v2, "timeoutMax":J
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 109
    :goto_0
    return-void

    .line 106
    :cond_0
    sub-long v4, v2, v0

    const/4 v6, 0x1

    invoke-virtual {p0, v4, v5, v6}, Lcom/vlingo/workflow/task/WorkflowTask;->waitToFinish(JI)V

    goto :goto_0
.end method
