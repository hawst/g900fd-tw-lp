.class public Lcom/vlingo/workflow/task/model/TaskDefinition;
.super Lcom/vlingo/workflow/task/model/TaskDefinitionBase;
.source "TaskDefinition.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/vlingo/workflow/task/model/TaskDefinition;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/task/model/TaskDefinition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/workflow/task/model/TaskDefinitionBase;-><init>()V

    return-void
.end method

.method private validateSettersOrGetters(Ljava/util/Set;Ljava/lang/Class;Ljava/lang/String;)Z
    .locals 9
    .param p2, "taskClass"    # Ljava/lang/Class;
    .param p3, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/task/model/Parameter;",
            ">;",
            "Ljava/lang/Class;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "params":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/workflow/task/model/Parameter;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 31
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/workflow/task/model/Parameter;

    .line 32
    .local v2, "p":Lcom/vlingo/workflow/task/model/Parameter;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/vlingo/workflow/task/model/Parameter;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/vlingo/workflow/task/model/Parameter;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v6, :cond_0

    invoke-virtual {v2}, Lcom/vlingo/workflow/task/model/Parameter;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "setget":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v4, "set"

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 35
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/vlingo/workflow/task/model/Parameter;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    aput-object v8, v4, v7

    invoke-virtual {p2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    sget-object v4, Lcom/vlingo/workflow/task/model/TaskDefinition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "TaskDefinition "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " failed input/output parameter validation because a setter/getter does not exist. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v0}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move v4, v5

    .line 46
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .end local v2    # "p":Lcom/vlingo/workflow/task/model/Parameter;
    .end local v3    # "setget":Ljava/lang/String;
    :goto_2
    return v4

    .line 32
    .restart local v2    # "p":Lcom/vlingo/workflow/task/model/Parameter;
    :cond_0
    const-string/jumbo v4, ""

    goto :goto_1

    .line 37
    .restart local v3    # "setget":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    :try_start_1
    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {p2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2}, Lcom/vlingo/workflow/task/model/Parameter;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 41
    :catch_1
    move-exception v0

    .line 42
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    sget-object v4, Lcom/vlingo/workflow/task/model/TaskDefinition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "TaskDefinition "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " failed input/output parameter validation because a referenced class cannot be found. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v0}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move v4, v5

    .line 43
    goto :goto_2

    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    .end local v2    # "p":Lcom/vlingo/workflow/task/model/Parameter;
    .end local v3    # "setget":Ljava/lang/String;
    :cond_2
    move v4, v6

    .line 46
    goto :goto_2
.end method


# virtual methods
.method public validate()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 18
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 19
    .local v1, "klass":Ljava/lang/Class;
    invoke-virtual {p0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getInputs()Ljava/util/Set;

    move-result-object v3

    const-string/jumbo v4, "set"

    invoke-direct {p0, v3, v1, v4}, Lcom/vlingo/workflow/task/model/TaskDefinition;->validateSettersOrGetters(Ljava/util/Set;Ljava/lang/Class;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 27
    .end local v1    # "klass":Ljava/lang/Class;
    :cond_0
    :goto_0
    return v2

    .line 21
    .restart local v1    # "klass":Ljava/lang/Class;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getOutputs()Ljava/util/Set;

    move-result-object v3

    const-string/jumbo v4, "get"

    invoke-direct {p0, v3, v1, v4}, Lcom/vlingo/workflow/task/model/TaskDefinition;->validateSettersOrGetters(Ljava/util/Set;Ljava/lang/Class;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 27
    const/4 v2, 0x1

    goto :goto_0

    .line 23
    .end local v1    # "klass":Ljava/lang/Class;
    :catch_0
    move-exception v0

    .line 24
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    sget-object v3, Lcom/vlingo/workflow/task/model/TaskDefinition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "TaskDefinition "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " failed validation because a referenced class cannot be found. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
