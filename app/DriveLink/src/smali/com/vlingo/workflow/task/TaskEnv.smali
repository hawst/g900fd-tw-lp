.class public Lcom/vlingo/workflow/task/TaskEnv;
.super Ljava/lang/Object;
.source "TaskEnv.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/vlingo/workflow/ServletEnvironmentInterface;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private ivEnv:Lcom/vlingo/workflow/ServletEnvironmentInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private ivReq:Lcom/vlingo/workflow/model/VRequest;


# virtual methods
.method public getEnv()Lcom/vlingo/workflow/ServletEnvironmentInterface;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lcom/vlingo/workflow/task/TaskEnv;, "Lcom/vlingo/workflow/task/TaskEnv<TT;>;"
    iget-object v0, p0, Lcom/vlingo/workflow/task/TaskEnv;->ivEnv:Lcom/vlingo/workflow/ServletEnvironmentInterface;

    return-object v0
.end method

.method public getRequest()Lcom/vlingo/workflow/model/VRequest;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lcom/vlingo/workflow/task/TaskEnv;, "Lcom/vlingo/workflow/task/TaskEnv<TT;>;"
    iget-object v0, p0, Lcom/vlingo/workflow/task/TaskEnv;->ivReq:Lcom/vlingo/workflow/model/VRequest;

    return-object v0
.end method
