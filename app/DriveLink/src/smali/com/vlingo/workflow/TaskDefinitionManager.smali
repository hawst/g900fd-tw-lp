.class public Lcom/vlingo/workflow/TaskDefinitionManager;
.super Ljava/lang/Object;
.source "TaskDefinitionManager.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private additionalPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/mda/model/PackageMeta;",
            ">;"
        }
    .end annotation
.end field

.field private ivTaskDefinitionMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/workflow/task/model/TaskDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private ivTaskDefinitionsFileName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/vlingo/workflow/TaskDefinitionManager;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/workflow/TaskDefinitionManager;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    .line 33
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/util/Set;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/mda/model/PackageMeta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "additionalPackages":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/mda/model/PackageMeta;>;"
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionsFileName:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->additionalPackages:Ljava/util/Set;

    .line 29
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 36
    iput-object p2, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->additionalPackages:Ljava/util/Set;

    .line 37
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    iput-object p1, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionsFileName:Ljava/lang/String;

    .line 40
    invoke-direct {p0}, Lcom/vlingo/workflow/TaskDefinitionManager;->init()V

    goto :goto_0
.end method

.method private init()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/vlingo/workflow/TaskDefinitionManager;->loadDefinitions()V

    .line 45
    return-void
.end method

.method private loadDefinitions()V
    .locals 6

    .prologue
    .line 49
    const/4 v1, 0x0

    .line 50
    .local v1, "definitionCollecton":Lcom/vlingo/workflow/task/model/TaskDefinitionCollection;
    iget-object v3, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->additionalPackages:Ljava/util/Set;

    if-nez v3, :cond_0

    .line 51
    const-class v3, Lcom/vlingo/workflow/task/model/TaskDefinitionCollection;

    iget-object v4, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionsFileName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/vlingo/workflow/util/Util;->loadMdaObject(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v1

    .end local v1    # "definitionCollecton":Lcom/vlingo/workflow/task/model/TaskDefinitionCollection;
    check-cast v1, Lcom/vlingo/workflow/task/model/TaskDefinitionCollection;

    .line 55
    .restart local v1    # "definitionCollecton":Lcom/vlingo/workflow/task/model/TaskDefinitionCollection;
    :goto_0
    invoke-virtual {v1}, Lcom/vlingo/workflow/task/model/TaskDefinitionCollection;->getTaskDefinitions()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/workflow/task/model/TaskDefinition;

    .line 56
    .local v0, "definition":Lcom/vlingo/workflow/task/model/TaskDefinition;
    iget-object v3, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 57
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Duplicate task definition with the name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 53
    .end local v0    # "definition":Lcom/vlingo/workflow/task/model/TaskDefinition;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    const-class v3, Lcom/vlingo/workflow/task/model/TaskDefinitionCollection;

    iget-object v4, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionsFileName:Ljava/lang/String;

    iget-object v5, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->additionalPackages:Ljava/util/Set;

    invoke-static {v3, v4, v5}, Lcom/vlingo/workflow/util/Util;->loadMdaObject(Ljava/lang/Class;Ljava/lang/String;Ljava/util/Set;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v1

    .end local v1    # "definitionCollecton":Lcom/vlingo/workflow/task/model/TaskDefinitionCollection;
    check-cast v1, Lcom/vlingo/workflow/task/model/TaskDefinitionCollection;

    .restart local v1    # "definitionCollecton":Lcom/vlingo/workflow/task/model/TaskDefinitionCollection;
    goto :goto_0

    .line 58
    .restart local v0    # "definition":Lcom/vlingo/workflow/task/model/TaskDefinition;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {v0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->validate()Z

    move-result v3

    if-nez v3, :cond_2

    .line 59
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "TaskDefinition "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " failed validation."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 61
    :cond_2
    iget-object v3, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 65
    .end local v0    # "definition":Lcom/vlingo/workflow/task/model/TaskDefinition;
    :cond_3
    sget-object v3, Lcom/vlingo/workflow/TaskDefinitionManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "loaded "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " task definitions from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionsFileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 66
    return-void
.end method


# virtual methods
.method public getDefinition(Ljava/lang/String;)Lcom/vlingo/workflow/task/model/TaskDefinition;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Task definition with the name "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " does not exist."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/vlingo/workflow/TaskDefinitionManager;->ivTaskDefinitionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/workflow/task/model/TaskDefinition;

    return-object v0
.end method
