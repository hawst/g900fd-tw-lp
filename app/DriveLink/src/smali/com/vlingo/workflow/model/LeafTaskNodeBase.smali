.class public Lcom/vlingo/workflow/model/LeafTaskNodeBase;
.super Lcom/vlingo/workflow/model/TaskNode;
.source "LeafTaskNodeBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# instance fields
.field private Id:Ljava/lang/String;

.field private InputParameterMappings:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/ParameterMapping;",
            ">;"
        }
    .end annotation
.end field

.field private IsEssential:Ljava/lang/Boolean;

.field private IsTimed:Ljava/lang/Boolean;

.field private TaskDefinitionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/workflow/model/TaskNode;-><init>()V

    .line 9
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/workflow/model/LeafTaskNodeBase;->InputParameterMappings:Ljava/util/Set;

    .line 11
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/vlingo/workflow/model/LeafTaskNodeBase;->IsEssential:Ljava/lang/Boolean;

    .line 13
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/vlingo/workflow/model/LeafTaskNodeBase;->IsTimed:Ljava/lang/Boolean;

    .line 19
    return-void
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 63
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/workflow/model/LeafTaskNode;

    const-string/jumbo v2, "/WorkflowModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/workflow/model/LeafTaskNodeBase;->Id:Ljava/lang/String;

    return-object v0
.end method

.method public getInputParameterMappings()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/ParameterMapping;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/workflow/model/LeafTaskNodeBase;->InputParameterMappings:Ljava/util/Set;

    return-object v0
.end method

.method public getIsEssential()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/workflow/model/LeafTaskNodeBase;->IsEssential:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getTaskDefinitionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/workflow/model/LeafTaskNodeBase;->TaskDefinitionName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
