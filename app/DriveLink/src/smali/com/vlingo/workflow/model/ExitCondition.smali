.class public Lcom/vlingo/workflow/model/ExitCondition;
.super Lcom/vlingo/workflow/model/ExitConditionBase;
.source "ExitCondition.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private exitConditionClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/workflow/task/TaskExitConditionInterface;",
            ">;"
        }
    .end annotation
.end field

.field private referencedTasks:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/vlingo/workflow/model/ExitCondition;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/model/ExitCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/workflow/model/ExitConditionBase;-><init>()V

    .line 15
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/workflow/model/ExitCondition;->referencedTasks:Ljava/util/HashSet;

    return-void
.end method


# virtual methods
.method public init()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/ExitCondition;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/workflow/model/ExitCondition;->exitConditionClass:Ljava/lang/Class;

    .line 19
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/ExitCondition;->getReferencedTasks()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/workflow/model/ReferencedTask;

    .line 20
    .local v1, "referencedTask":Lcom/vlingo/workflow/model/ReferencedTask;
    iget-object v2, p0, Lcom/vlingo/workflow/model/ExitCondition;->referencedTasks:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/vlingo/workflow/model/ReferencedTask;->getReferenceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 21
    .end local v1    # "referencedTask":Lcom/vlingo/workflow/model/ReferencedTask;
    :cond_0
    return-void
.end method
