.class public Lcom/vlingo/workflow/model/MinSoftwareVersionEvaluator;
.super Ljava/lang/Object;
.source "MinSoftwareVersionEvaluator.java"

# interfaces
.implements Lcom/vlingo/workflow/model/EvaluatorInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final ivLogger:Lorg/apache/log4j/Logger;


# instance fields
.field private condition:Lcom/vlingo/workflow/model/RequestCondition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/workflow/model/MinSoftwareVersionEvaluator;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/model/MinSoftwareVersionEvaluator;->ivLogger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/workflow/model/RequestCondition;)V
    .locals 0
    .param p1, "condition"    # Lcom/vlingo/workflow/model/RequestCondition;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/vlingo/workflow/model/MinSoftwareVersionEvaluator;->condition:Lcom/vlingo/workflow/model/RequestCondition;

    .line 27
    return-void
.end method


# virtual methods
.method public evaluate(Ljava/lang/String;Lcom/vlingo/workflow/model/VRequest;)Z
    .locals 4
    .param p1, "servletName"    # Ljava/lang/String;
    .param p2, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    .line 30
    iget-object v1, p0, Lcom/vlingo/workflow/model/MinSoftwareVersionEvaluator;->condition:Lcom/vlingo/workflow/model/RequestCondition;

    invoke-virtual {v1}, Lcom/vlingo/workflow/model/RequestCondition;->getMinSoftwareVersion()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 31
    const-string/jumbo v1, "x-vlsoftware"

    const-string/jumbo v2, "Version"

    invoke-interface {p2, v1, v2}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "softwareVersion":Ljava/lang/String;
    sget-object v1, Lcom/vlingo/workflow/model/MinSoftwareVersionEvaluator;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Evaluating MinSoftwareVersion: expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/workflow/model/MinSoftwareVersionEvaluator;->condition:Lcom/vlingo/workflow/model/RequestCondition;

    invoke-virtual {v3}, Lcom/vlingo/workflow/model/RequestCondition;->getMinSoftwareVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " or higher, found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/log4j/Logger;->trace(Ljava/lang/Object;)V

    .line 33
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/vlingo/workflow/model/MinSoftwareVersionEvaluator;->condition:Lcom/vlingo/workflow/model/RequestCondition;

    invoke-virtual {v1}, Lcom/vlingo/workflow/model/RequestCondition;->getMinSoftwareVersion()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/workflow/WorkflowManager;->compareVersions(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_1

    .line 34
    :cond_0
    const/4 v1, 0x0

    .line 36
    .end local v0    # "softwareVersion":Ljava/lang/String;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
