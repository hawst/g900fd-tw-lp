.class public Lcom/vlingo/workflow/model/WorkflowTemplate;
.super Lcom/vlingo/workflow/model/WorkflowTemplateBase;
.source "WorkflowTemplate.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/workflow/model/WorkflowTemplateBase;-><init>()V

    return-void
.end method


# virtual methods
.method public getTaskNodeList(Lcom/vlingo/workflow/WorkflowTemplateManager;)Ljava/util/Set;
    .locals 7
    .param p1, "templateManager"    # Lcom/vlingo/workflow/WorkflowTemplateManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/workflow/WorkflowTemplateManager;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/TaskNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 15
    .local v1, "ret":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/workflow/model/TaskNode;>;"
    invoke-super {p0}, Lcom/vlingo/workflow/model/WorkflowTemplateBase;->getTaskNodes()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/workflow/model/TaskNode;

    .line 16
    .local v2, "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    instance-of v4, v2, Lcom/vlingo/workflow/model/LeafTaskNode;

    if-eqz v4, :cond_1

    .line 17
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 18
    :cond_1
    instance-of v4, v2, Lcom/vlingo/workflow/model/WorkflowTemplateRef;

    if-eqz v4, :cond_3

    move-object v4, v2

    .line 19
    check-cast v4, Lcom/vlingo/workflow/model/WorkflowTemplateRef;

    invoke-virtual {v4}, Lcom/vlingo/workflow/model/WorkflowTemplateRef;->getWorkflowTemplateName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/vlingo/workflow/WorkflowTemplateManager;->getTemplate(Ljava/lang/String;)Lcom/vlingo/workflow/model/WorkflowTemplate;

    move-result-object v3

    .line 20
    .local v3, "template":Lcom/vlingo/workflow/model/WorkflowTemplate;
    if-nez v3, :cond_2

    .line 21
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "WorflowTemplateManager cannot find the workflow template with the name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    check-cast v2, Lcom/vlingo/workflow/model/WorkflowTemplateRef;

    .end local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    invoke-virtual {v2}, Lcom/vlingo/workflow/model/WorkflowTemplateRef;->getWorkflowTemplateName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 23
    .restart local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    :cond_2
    invoke-virtual {v3, p1}, Lcom/vlingo/workflow/model/WorkflowTemplate;->getTaskNodeList(Lcom/vlingo/workflow/WorkflowTemplateManager;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 24
    .end local v3    # "template":Lcom/vlingo/workflow/model/WorkflowTemplate;
    :cond_3
    instance-of v4, v2, Lcom/vlingo/workflow/model/WorkflowTemplate;

    if-eqz v4, :cond_0

    .line 25
    new-instance v4, Ljava/lang/RuntimeException;

    const-string/jumbo v5, "Nested workflow templates are not allowed."

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 28
    .end local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    :cond_4
    return-object v1
.end method
