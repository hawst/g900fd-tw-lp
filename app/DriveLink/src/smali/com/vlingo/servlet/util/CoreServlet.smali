.class public abstract Lcom/vlingo/servlet/util/CoreServlet;
.super Ljavax/servlet/http/HttpServlet;
.source "CoreServlet.java"


# static fields
.field private static final ivInited:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static ivLogger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    const-class v0, Lcom/vlingo/servlet/util/BaseServlet;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/servlet/util/CoreServlet;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 27
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/vlingo/servlet/util/CoreServlet;->ivInited:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljavax/servlet/http/HttpServlet;-><init>()V

    return-void
.end method
