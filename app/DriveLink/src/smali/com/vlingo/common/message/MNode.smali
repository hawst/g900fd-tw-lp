.class public Lcom/vlingo/common/message/MNode;
.super Ljava/lang/Object;
.source "MNode.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private ivAttributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ivChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/common/message/MNode;",
            ">;"
        }
    .end annotation
.end field

.field public ivFormating:Z

.field private ivName:Ljava/lang/String;

.field private ivParent:Lcom/vlingo/common/message/MNode;

.field public ivValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/common/message/MNode;->ivAttributes:Ljava/util/HashMap;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/common/message/MNode;->ivChildren:Ljava/util/ArrayList;

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/common/message/MNode;->ivFormating:Z

    .line 31
    iput-object p1, p0, Lcom/vlingo/common/message/MNode;->ivName:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/common/message/MNode;->ivAttributes:Ljava/util/HashMap;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/common/message/MNode;->ivChildren:Ljava/util/ArrayList;

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/common/message/MNode;->ivFormating:Z

    .line 36
    iput-object p1, p0, Lcom/vlingo/common/message/MNode;->ivName:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lcom/vlingo/common/message/MNode;->ivValue:Ljava/lang/String;

    .line 38
    return-void
.end method

.method private addChildrenRecursive(Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .param p2, "nodeName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/common/message/MNode;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "res":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    iget-object v2, p0, Lcom/vlingo/common/message/MNode;->ivChildren:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/common/message/MNode;

    .line 117
    .local v0, "child":Lcom/vlingo/common/message/MNode;
    iget-object v2, v0, Lcom/vlingo/common/message/MNode;->ivName:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 123
    :cond_0
    invoke-direct {v0, p1, p2}, Lcom/vlingo/common/message/MNode;->addChildrenRecursive(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    .line 127
    .end local v0    # "child":Lcom/vlingo/common/message/MNode;
    :cond_1
    return-void
.end method


# virtual methods
.method public add(Lcom/vlingo/common/message/MNode;)V
    .locals 1
    .param p1, "item"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 152
    invoke-virtual {p1, p0}, Lcom/vlingo/common/message/MNode;->setParent(Lcom/vlingo/common/message/MNode;)V

    .line 153
    iget-object v0, p0, Lcom/vlingo/common/message/MNode;->ivChildren:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    return-void
.end method

.method public findChildren(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "nodeName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/common/message/MNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v2, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/common/message/MNode;>;"
    iget-object v3, p0, Lcom/vlingo/common/message/MNode;->ivChildren:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/common/message/MNode;

    .line 105
    .local v0, "child":Lcom/vlingo/common/message/MNode;
    iget-object v3, v0, Lcom/vlingo/common/message/MNode;->ivName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 107
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    .end local v0    # "child":Lcom/vlingo/common/message/MNode;
    :cond_1
    return-object v2
.end method

.method public findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "nodeName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/common/message/MNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 131
    .local v0, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/common/message/MNode;>;"
    invoke-direct {p0, v0, p1}, Lcom/vlingo/common/message/MNode;->addChildrenRecursive(Ljava/util/List;Ljava/lang/String;)V

    .line 132
    return-object v0
.end method

.method public findFirstChild(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;
    .locals 1
    .param p1, "nodeName"    # Ljava/lang/String;

    .prologue
    .line 137
    invoke-virtual {p0, p1}, Lcom/vlingo/common/message/MNode;->getChildWithName(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v0

    return-object v0
.end method

.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/vlingo/common/message/MNode;->ivAttributes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lcom/vlingo/common/message/MNode;->ivAttributes:Ljava/util/HashMap;

    return-object v0
.end method

.method public getChildWithName(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 208
    iget-object v2, p0, Lcom/vlingo/common/message/MNode;->ivChildren:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/common/message/MNode;

    .line 210
    .local v0, "child":Lcom/vlingo/common/message/MNode;
    iget-object v2, v0, Lcom/vlingo/common/message/MNode;->ivName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 212
    .end local v0    # "child":Lcom/vlingo/common/message/MNode;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/common/message/MNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/vlingo/common/message/MNode;->ivChildren:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFormating()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/vlingo/common/message/MNode;->ivFormating:Z

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/common/message/MNode;->ivName:Ljava/lang/String;

    return-object v0
.end method

.method public getTextChildNodeValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vlingo/common/message/MNode;->ivChildren:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 94
    const/4 v0, 0x0

    .line 96
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/common/message/MNode;->ivChildren:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/common/message/MNode;

    iget-object v0, v0, Lcom/vlingo/common/message/MNode;->ivValue:Ljava/lang/String;

    goto :goto_0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/common/message/MNode;->ivValue:Ljava/lang/String;

    return-object v0
.end method

.method public setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 187
    if-nez p2, :cond_0

    .line 188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "May not set null value for attribute:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/vlingo/common/message/MNode;->ivAttributes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    return-void
.end method

.method setParent(Lcom/vlingo/common/message/MNode;)V
    .locals 0
    .param p1, "parent"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/vlingo/common/message/MNode;->ivParent:Lcom/vlingo/common/message/MNode;

    .line 148
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 323
    invoke-static {}, Lcom/vlingo/common/message/XMLCodec;->getInstance()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/common/message/XMLCodec;->duplicate()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v0

    .line 324
    .local v0, "codec":Lcom/vlingo/common/message/XMLCodec;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/common/message/XMLCodec;->setDisableOutputFormatting(Z)V

    .line 325
    invoke-virtual {v0, p0}, Lcom/vlingo/common/message/XMLCodec;->encode(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
