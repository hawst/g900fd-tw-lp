.class public Lcom/vlingo/common/util/VThreadPoolExecutor;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "VThreadPoolExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/common/util/VThreadPoolExecutor$1;,
        Lcom/vlingo/common/util/VThreadPoolExecutor$PoolParameterGauge;,
        Lcom/vlingo/common/util/VThreadPoolExecutor$LargestPoolSizeGauge;,
        Lcom/vlingo/common/util/VThreadPoolExecutor$CompletedTaskCountGauge;,
        Lcom/vlingo/common/util/VThreadPoolExecutor$TaskCountGauge;,
        Lcom/vlingo/common/util/VThreadPoolExecutor$PoolSizeGauge;,
        Lcom/vlingo/common/util/VThreadPoolExecutor$ActiveCountGauge;,
        Lcom/vlingo/common/util/VThreadPoolExecutor$QueueDepth;,
        Lcom/vlingo/common/util/VThreadPoolExecutor$RejectionHandler;
    }
.end annotation


# static fields
.field private static final ivLogger:Lorg/apache/log4j/Logger;


# instance fields
.field private final ivExecGauge:Lcom/vlingo/common/util/VNumericGaugeMetric;

.field private final ivMaxCapacity:I

.field private final ivName:Ljava/lang/String;

.field private final ivQueueLinger:Lcom/yammer/metrics/core/TimerMetric;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/common/util/VThreadPoolExecutor;->ivLogger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIII)V
    .locals 9
    .param p1, "poolName"    # Ljava/lang/String;
    .param p2, "maxCapacity"    # I
    .param p3, "corePoolSize"    # I
    .param p4, "maxPoolSize"    # I
    .param p5, "keepAliveSeconds"    # I

    .prologue
    const/4 v8, 0x0

    .line 32
    int-to-long v3, p5

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    if-nez p2, :cond_0

    new-instance v6, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v6}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    :goto_0
    new-instance v7, Lcom/vlingo/common/util/VThreadFactory;

    invoke-direct {v7, p1}, Lcom/vlingo/common/util/VThreadFactory;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move v1, p3

    move v2, p4

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 35
    iput-object p1, p0, Lcom/vlingo/common/util/VThreadPoolExecutor;->ivName:Ljava/lang/String;

    .line 36
    iput p2, p0, Lcom/vlingo/common/util/VThreadPoolExecutor;->ivMaxCapacity:I

    .line 37
    new-instance v0, Lcom/vlingo/common/util/VThreadPoolExecutor$RejectionHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/common/util/VThreadPoolExecutor$RejectionHandler;-><init>(Lcom/vlingo/common/util/VThreadPoolExecutor;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/common/util/VThreadPoolExecutor;->setRejectedExecutionHandler(Ljava/util/concurrent/RejectedExecutionHandler;)V

    .line 38
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "execute"

    invoke-static {v0, v1, p1}, Lcom/vlingo/common/util/VMetricFactory;->newNumericGauge(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/util/VNumericGaugeMetric;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/common/util/VThreadPoolExecutor;->ivExecGauge:Lcom/vlingo/common/util/VNumericGaugeMetric;

    .line 39
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "queueDepth"

    new-instance v2, Lcom/vlingo/common/util/VThreadPoolExecutor$QueueDepth;

    invoke-direct {v2, p0, v8}, Lcom/vlingo/common/util/VThreadPoolExecutor$QueueDepth;-><init>(Lcom/vlingo/common/util/VThreadPoolExecutor;Lcom/vlingo/common/util/VThreadPoolExecutor$1;)V

    invoke-static {v0, v1, p1, v2}, Lcom/yammer/metrics/Metrics;->newGauge(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/yammer/metrics/core/GaugeMetric;)Lcom/yammer/metrics/core/GaugeMetric;

    .line 40
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "queueLinger"

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, p1, v2, v3}, Lcom/yammer/metrics/Metrics;->newTimer(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/TimeUnit;Ljava/util/concurrent/TimeUnit;)Lcom/yammer/metrics/core/TimerMetric;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/common/util/VThreadPoolExecutor;->ivQueueLinger:Lcom/yammer/metrics/core/TimerMetric;

    .line 41
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "poolParameters"

    new-instance v2, Lcom/vlingo/common/util/VThreadPoolExecutor$PoolParameterGauge;

    invoke-direct {v2, p0, p0}, Lcom/vlingo/common/util/VThreadPoolExecutor$PoolParameterGauge;-><init>(Lcom/vlingo/common/util/VThreadPoolExecutor;Lcom/vlingo/common/util/VThreadPoolExecutor;)V

    invoke-static {v0, v1, p1, v2}, Lcom/yammer/metrics/Metrics;->newGauge(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/yammer/metrics/core/GaugeMetric;)Lcom/yammer/metrics/core/GaugeMetric;

    .line 42
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "activeThreads"

    new-instance v2, Lcom/vlingo/common/util/VThreadPoolExecutor$ActiveCountGauge;

    invoke-direct {v2, p0, v8}, Lcom/vlingo/common/util/VThreadPoolExecutor$ActiveCountGauge;-><init>(Lcom/vlingo/common/util/VThreadPoolExecutor;Lcom/vlingo/common/util/VThreadPoolExecutor$1;)V

    invoke-static {v0, v1, p1, v2}, Lcom/yammer/metrics/Metrics;->newGauge(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/yammer/metrics/core/GaugeMetric;)Lcom/yammer/metrics/core/GaugeMetric;

    .line 43
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "poolSize"

    new-instance v2, Lcom/vlingo/common/util/VThreadPoolExecutor$PoolSizeGauge;

    invoke-direct {v2, p0, v8}, Lcom/vlingo/common/util/VThreadPoolExecutor$PoolSizeGauge;-><init>(Lcom/vlingo/common/util/VThreadPoolExecutor;Lcom/vlingo/common/util/VThreadPoolExecutor$1;)V

    invoke-static {v0, v1, p1, v2}, Lcom/yammer/metrics/Metrics;->newGauge(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/yammer/metrics/core/GaugeMetric;)Lcom/yammer/metrics/core/GaugeMetric;

    .line 44
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "taskCount"

    new-instance v2, Lcom/vlingo/common/util/VThreadPoolExecutor$TaskCountGauge;

    invoke-direct {v2, p0, v8}, Lcom/vlingo/common/util/VThreadPoolExecutor$TaskCountGauge;-><init>(Lcom/vlingo/common/util/VThreadPoolExecutor;Lcom/vlingo/common/util/VThreadPoolExecutor$1;)V

    invoke-static {v0, v1, p1, v2}, Lcom/yammer/metrics/Metrics;->newGauge(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/yammer/metrics/core/GaugeMetric;)Lcom/yammer/metrics/core/GaugeMetric;

    .line 45
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "completedTaskCount"

    new-instance v2, Lcom/vlingo/common/util/VThreadPoolExecutor$CompletedTaskCountGauge;

    invoke-direct {v2, p0, v8}, Lcom/vlingo/common/util/VThreadPoolExecutor$CompletedTaskCountGauge;-><init>(Lcom/vlingo/common/util/VThreadPoolExecutor;Lcom/vlingo/common/util/VThreadPoolExecutor$1;)V

    invoke-static {v0, v1, p1, v2}, Lcom/yammer/metrics/Metrics;->newGauge(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/yammer/metrics/core/GaugeMetric;)Lcom/yammer/metrics/core/GaugeMetric;

    .line 46
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "largestPoolSize"

    new-instance v2, Lcom/vlingo/common/util/VThreadPoolExecutor$LargestPoolSizeGauge;

    invoke-direct {v2, p0, v8}, Lcom/vlingo/common/util/VThreadPoolExecutor$LargestPoolSizeGauge;-><init>(Lcom/vlingo/common/util/VThreadPoolExecutor;Lcom/vlingo/common/util/VThreadPoolExecutor$1;)V

    invoke-static {v0, v1, p1, v2}, Lcom/yammer/metrics/Metrics;->newGauge(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/yammer/metrics/core/GaugeMetric;)Lcom/yammer/metrics/core/GaugeMetric;

    .line 47
    return-void

    .line 32
    :cond_0
    new-instance v6, Lcom/vlingo/common/util/VRunnableBlockingQueue;

    invoke-direct {v6, p2}, Lcom/vlingo/common/util/VRunnableBlockingQueue;-><init>(I)V

    goto/16 :goto_0
.end method

.method static synthetic access$600(Lcom/vlingo/common/util/VThreadPoolExecutor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/common/util/VThreadPoolExecutor;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/common/util/VThreadPoolExecutor;->ivName:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ThreadPoolExecutor;->afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V

    .line 76
    iget-object v0, p0, Lcom/vlingo/common/util/VThreadPoolExecutor;->ivExecGauge:Lcom/vlingo/common/util/VNumericGaugeMetric;

    invoke-interface {v0}, Lcom/vlingo/common/util/VNumericGaugeMetric;->dec()J

    .line 77
    return-void
.end method

.method protected beforeExecute(Ljava/lang/Thread;Ljava/lang/Runnable;)V
    .locals 6
    .param p1, "t"    # Ljava/lang/Thread;
    .param p2, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 61
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ThreadPoolExecutor;->beforeExecute(Ljava/lang/Thread;Ljava/lang/Runnable;)V

    .line 63
    iget-object v1, p0, Lcom/vlingo/common/util/VThreadPoolExecutor;->ivExecGauge:Lcom/vlingo/common/util/VNumericGaugeMetric;

    invoke-interface {v1}, Lcom/vlingo/common/util/VNumericGaugeMetric;->inc()J

    .line 64
    instance-of v1, p2, Lcom/vlingo/common/util/RunnableBase;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 66
    check-cast v0, Lcom/vlingo/common/util/RunnableBase;

    .line 67
    .local v0, "rb":Lcom/vlingo/common/util/RunnableBase;
    iget-object v1, p0, Lcom/vlingo/common/util/VThreadPoolExecutor;->ivQueueLinger:Lcom/yammer/metrics/core/TimerMetric;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/vlingo/common/util/RunnableBase;->getCreatedTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Lcom/yammer/metrics/core/TimerMetric;->update(JLjava/util/concurrent/TimeUnit;)V

    .line 69
    .end local v0    # "rb":Lcom/vlingo/common/util/RunnableBase;
    :cond_0
    return-void
.end method
