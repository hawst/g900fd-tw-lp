.class public Lcom/vlingo/common/util/VCounterMetricImpl;
.super Ljava/lang/Object;
.source "VCounterMetricImpl.java"

# interfaces
.implements Lcom/vlingo/common/util/VCounterMetric;


# instance fields
.field private final basedOnMe:Lcom/yammer/metrics/core/CounterMetric;


# direct methods
.method public constructor <init>(Lcom/yammer/metrics/core/CounterMetric;)V
    .locals 2
    .param p1, "basedOn"    # Lcom/yammer/metrics/core/CounterMetric;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    if-nez p1, :cond_0

    .line 32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "basedOn may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_0
    iput-object p1, p0, Lcom/vlingo/common/util/VCounterMetricImpl;->basedOnMe:Lcom/yammer/metrics/core/CounterMetric;

    .line 35
    return-void
.end method


# virtual methods
.method public inc()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vlingo/common/util/VCounterMetricImpl;->basedOnMe:Lcom/yammer/metrics/core/CounterMetric;

    invoke-virtual {v0}, Lcom/yammer/metrics/core/CounterMetric;->inc()V

    .line 44
    return-void
.end method
