.class public Lcom/vlingo/common/util/VLocale;
.super Ljava/lang/Object;
.source "VLocale.java"


# static fields
.field public static final ivISOCountryCodes:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final ivISOLanguageCodes:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field ivCountry:Ljava/lang/String;

.field ivLanguage:Ljava/lang/String;

.field ivVlingoDefined:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/vlingo/common/util/VLocale$1;

    invoke-direct {v0}, Lcom/vlingo/common/util/VLocale$1;-><init>()V

    sput-object v0, Lcom/vlingo/common/util/VLocale;->ivISOLanguageCodes:Ljava/util/HashSet;

    .line 281
    new-instance v0, Lcom/vlingo/common/util/VLocale$2;

    invoke-direct {v0}, Lcom/vlingo/common/util/VLocale$2;-><init>()V

    sput-object v0, Lcom/vlingo/common/util/VLocale;->ivISOCountryCodes:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "localeCode"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 25
    const-string/jumbo v1, "-"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "c":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 27
    aget-object v1, v0, v4

    const-string/jumbo v2, "v"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    iput-boolean v3, p0, Lcom/vlingo/common/util/VLocale;->ivVlingoDefined:Z

    .line 29
    aget-object v1, v0, v3

    iput-object v1, p0, Lcom/vlingo/common/util/VLocale;->ivLanguage:Ljava/lang/String;

    .line 30
    aget-object v1, v0, v5

    iput-object v1, p0, Lcom/vlingo/common/util/VLocale;->ivCountry:Ljava/lang/String;

    .line 45
    .end local v0    # "c":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 32
    .restart local v0    # "c":[Ljava/lang/String;
    :cond_1
    array-length v1, v0

    if-ne v1, v5, :cond_2

    .line 33
    aget-object v1, v0, v4

    iput-object v1, p0, Lcom/vlingo/common/util/VLocale;->ivLanguage:Ljava/lang/String;

    .line 34
    aget-object v1, v0, v3

    iput-object v1, p0, Lcom/vlingo/common/util/VLocale;->ivCountry:Ljava/lang/String;

    goto :goto_0

    .line 35
    :cond_2
    const-string/jumbo v1, "en"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 36
    const-string/jumbo v1, "en"

    iput-object v1, p0, Lcom/vlingo/common/util/VLocale;->ivLanguage:Ljava/lang/String;

    .line 37
    const-string/jumbo v1, "US"

    iput-object v1, p0, Lcom/vlingo/common/util/VLocale;->ivCountry:Ljava/lang/String;

    goto :goto_0

    .line 39
    :cond_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Locale must be v-xx-YY or xx-YY, but saw "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 42
    .end local v0    # "c":[Ljava/lang/String;
    :cond_4
    const-string/jumbo v1, "en"

    iput-object v1, p0, Lcom/vlingo/common/util/VLocale;->ivLanguage:Ljava/lang/String;

    .line 43
    const-string/jumbo v1, "US"

    iput-object v1, p0, Lcom/vlingo/common/util/VLocale;->ivCountry:Ljava/lang/String;

    goto :goto_0
.end method

.method private getLocaleCode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 119
    const-string/jumbo v0, ""

    .line 120
    .local v0, "result":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/vlingo/common/util/VLocale;->ivVlingoDefined:Z

    if-eqz v1, :cond_0

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "v-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/common/util/VLocale;->ivLanguage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/common/util/VLocale;->ivCountry:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    return-object v0
.end method


# virtual methods
.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/vlingo/common/util/VLocale;->ivLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/vlingo/common/util/VLocale;->getLocaleCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
