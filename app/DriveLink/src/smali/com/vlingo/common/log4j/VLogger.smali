.class public Lcom/vlingo/common/log4j/VLogger;
.super Lorg/apache/log4j/Logger;
.source "VLogger.java"


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 4
    .param p1, "clazz"    # Ljava/lang/Class;

    .prologue
    .line 12
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "EDM_"

    invoke-static {p1}, Lcom/vlingo/common/log4j/VLogger;->getLevel(Ljava/lang/Class;)Lorg/apache/log4j/Logger$Level;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/apache/log4j/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/log4j/Logger$Level;Z)V

    .line 13
    return-void
.end method

.method public static getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;
    .locals 1
    .param p0, "clazz"    # Ljava/lang/Class;

    .prologue
    .line 16
    new-instance v0, Lcom/vlingo/common/log4j/VLogger;

    invoke-direct {v0, p0}, Lcom/vlingo/common/log4j/VLogger;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public static getLogger(Ljava/lang/String;)Lcom/vlingo/common/log4j/VLogger;
    .locals 3
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 21
    :try_start_0
    new-instance v1, Lcom/vlingo/common/log4j/VLogger;

    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/vlingo/common/log4j/VLogger;-><init>(Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 22
    :catch_0
    move-exception v0

    .line 23
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public error(Lcom/vlingo/common/message/VMessage;)V
    .locals 1
    .param p1, "o"    # Lcom/vlingo/common/message/VMessage;

    .prologue
    .line 28
    invoke-virtual {p1}, Lcom/vlingo/common/message/VMessage;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/common/log4j/VLogger;->error(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public error(Lcom/vlingo/common/message/VMessage;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "o"    # Lcom/vlingo/common/message/VMessage;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 32
    invoke-virtual {p1}, Lcom/vlingo/common/message/VMessage;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/vlingo/common/log4j/VLogger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 33
    return-void
.end method

.method public fatal(Ljava/lang/Object;)V
    .locals 0
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/vlingo/common/log4j/VLogger;->error(Ljava/lang/Object;)V

    .line 37
    return-void
.end method

.method public fatal(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/common/log4j/VLogger;->error(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 41
    return-void
.end method

.method public getAdditivity()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    return v0
.end method

.method public getAllAppenders()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string/jumbo v0, ""

    return-object v0
.end method
