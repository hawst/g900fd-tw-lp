.class public Lcom/vlingo/mda/model/SimplePropertyMeta;
.super Lcom/vlingo/mda/model/ColumnPropertyMeta;
.source "SimplePropertyMeta.java"


# instance fields
.field private ivColumnName:Ljava/lang/String;

.field private ivDBDef:Ljava/lang/String;

.field private ivDefaultValue:Ljava/lang/String;

.field private ivEnums:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/EnumMeta;",
            ">;"
        }
    .end annotation
.end field

.field private ivGenerated:Ljava/lang/String;

.field private ivHibernateType:Ljava/lang/String;

.field private ivIsAutoIncrement:Z

.field private ivLength:Ljava/lang/Integer;

.field private ivSQLType:Ljava/lang/String;

.field private ivUnique:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "cm"    # Lcom/vlingo/mda/model/ClassMeta;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "javaType"    # Ljava/lang/String;
    .param p4, "hibernateType"    # Ljava/lang/String;
    .param p5, "sqlType"    # Ljava/lang/String;
    .param p6, "colName"    # Ljava/lang/String;
    .param p7, "indexName"    # Ljava/lang/String;
    .param p8, "uniqueKey"    # Ljava/lang/String;
    .param p9, "unique"    # Ljava/lang/String;
    .param p10, "notNull"    # Ljava/lang/Boolean;
    .param p11, "length"    # Ljava/lang/Integer;
    .param p12, "defaultValue"    # Ljava/lang/String;
    .param p13, "autoIncrement"    # Z
    .param p14, "generated"    # Ljava/lang/String;
    .param p15, "dbDef"    # Ljava/lang/String;
    .param p16, "doc"    # Ljava/lang/String;
    .param p17, "compactName"    # Ljava/lang/String;

    .prologue
    .line 37
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p10

    move-object/from16 v8, p16

    move-object/from16 v9, p17

    invoke-direct/range {v1 .. v9}, Lcom/vlingo/mda/model/ColumnPropertyMeta;-><init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivEnums:Ljava/util/Map;

    .line 38
    iput-object p5, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivSQLType:Ljava/lang/String;

    .line 39
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivLength:Ljava/lang/Integer;

    .line 40
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivDefaultValue:Ljava/lang/String;

    .line 41
    iput-object p4, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivHibernateType:Ljava/lang/String;

    .line 42
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivUnique:Ljava/lang/String;

    .line 44
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivIsAutoIncrement:Z

    .line 45
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivDBDef:Ljava/lang/String;

    .line 46
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivColumnName:Ljava/lang/String;

    .line 48
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivGenerated:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method public getEnums()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/EnumMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/mda/model/SimplePropertyMeta;->ivEnums:Ljava/util/Map;

    return-object v0
.end method
