.class public Lcom/vlingo/mda/model/PackageMeta;
.super Ljava/lang/Object;
.source "PackageMeta.java"


# instance fields
.field private ivClasses:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/ClassMeta;",
            ">;"
        }
    .end annotation
.end field

.field private ivCompactNameClasses:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/ClassMeta;",
            ">;"
        }
    .end annotation
.end field

.field private ivModelMeta:Lcom/vlingo/mda/model/ModelMeta;

.field private ivName:Ljava/lang/String;

.field private ivOpenClasses:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/vlingo/mda/model/ModelMeta;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "mm"    # Lcom/vlingo/mda/model/ModelMeta;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/PackageMeta;->ivClasses:Ljava/util/HashMap;

    .line 16
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/PackageMeta;->ivCompactNameClasses:Ljava/util/HashMap;

    .line 17
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/PackageMeta;->ivOpenClasses:Ljava/util/HashSet;

    .line 24
    iput-object p1, p0, Lcom/vlingo/mda/model/PackageMeta;->ivName:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/vlingo/mda/model/PackageMeta;->ivModelMeta:Lcom/vlingo/mda/model/ModelMeta;

    .line 26
    return-void
.end method


# virtual methods
.method public getClassMetas()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/ClassMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/mda/model/PackageMeta;->ivClasses:Ljava/util/HashMap;

    return-object v0
.end method

.method public getClassMetasByCompactName()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/ClassMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/mda/model/PackageMeta;->ivCompactNameClasses:Ljava/util/HashMap;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/mda/model/PackageMeta;->ivName:Ljava/lang/String;

    return-object v0
.end method

.method public getOpenClasses()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/mda/model/PackageMeta;->ivOpenClasses:Ljava/util/HashSet;

    return-object v0
.end method
