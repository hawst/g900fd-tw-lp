.class public Lcom/vlingo/mda/model/CollectionPropertyMeta;
.super Lcom/vlingo/mda/model/PropertyMeta;
.source "CollectionPropertyMeta.java"


# static fields
.field public static final COLTYPES:Ljava/util/HashSet;


# instance fields
.field private ivCardinality:Lcom/vlingo/mda/model/Cardinality;

.field private ivCollectionType:Ljava/lang/String;

.field private ivFlat:Z

.field private ivIsOrdered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/mda/model/CollectionPropertyMeta;->COLTYPES:Ljava/util/HashSet;

    .line 25
    sget-object v0, Lcom/vlingo/mda/model/CollectionPropertyMeta;->COLTYPES:Ljava/util/HashSet;

    const-string/jumbo v1, "list"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 26
    sget-object v0, Lcom/vlingo/mda/model/CollectionPropertyMeta;->COLTYPES:Ljava/util/HashSet;

    const-string/jumbo v1, "set"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/mda/model/Cardinality;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1, "cm"    # Lcom/vlingo/mda/model/ClassMeta;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "javaType"    # Ljava/lang/String;
    .param p4, "cardinality"    # Lcom/vlingo/mda/model/Cardinality;
    .param p5, "doc"    # Ljava/lang/String;
    .param p6, "collectionType"    # Ljava/lang/String;
    .param p7, "compactName"    # Ljava/lang/String;
    .param p8, "flat"    # Z

    .prologue
    const/4 v6, 0x0

    .line 32
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/mda/model/PropertyMeta;-><init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    iput-boolean v6, p0, Lcom/vlingo/mda/model/CollectionPropertyMeta;->ivIsOrdered:Z

    .line 21
    iput-boolean v6, p0, Lcom/vlingo/mda/model/CollectionPropertyMeta;->ivFlat:Z

    .line 33
    iput-object p4, p0, Lcom/vlingo/mda/model/CollectionPropertyMeta;->ivCardinality:Lcom/vlingo/mda/model/Cardinality;

    .line 34
    iput-object p6, p0, Lcom/vlingo/mda/model/CollectionPropertyMeta;->ivCollectionType:Ljava/lang/String;

    .line 35
    sget-object v0, Lcom/vlingo/mda/model/CollectionPropertyMeta;->COLTYPES:Ljava/util/HashSet;

    invoke-virtual {v0, p6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unrecognized collection type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " allowed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/vlingo/mda/model/CollectionPropertyMeta;->COLTYPES:Ljava/util/HashSet;

    const-string/jumbo v3, ","

    invoke-static {v2, v3}, Lcom/bzbyte/util/Util;->toString(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iput-boolean p8, p0, Lcom/vlingo/mda/model/CollectionPropertyMeta;->ivFlat:Z

    .line 40
    return-void
.end method


# virtual methods
.method public isFlat()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/vlingo/mda/model/CollectionPropertyMeta;->ivFlat:Z

    return v0
.end method
