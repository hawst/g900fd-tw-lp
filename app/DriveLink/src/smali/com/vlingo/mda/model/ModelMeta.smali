.class public Lcom/vlingo/mda/model/ModelMeta;
.super Ljava/lang/Object;
.source "ModelMeta.java"


# instance fields
.field private ivPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/PackageMeta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/ModelMeta;->ivPackages:Ljava/util/HashMap;

    .line 16
    return-void
.end method

.method private findClassMetaFromSimpleName(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;
    .locals 5
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/vlingo/mda/model/ModelMeta;->getPackageMetas()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/mda/model/PackageMeta;

    .line 62
    .local v3, "packageMeta":Lcom/vlingo/mda/model/PackageMeta;
    invoke-virtual {v3}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetas()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/mda/model/ClassMeta;

    .line 63
    .local v0, "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 67
    .end local v0    # "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "packageMeta":Lcom/vlingo/mda/model/PackageMeta;
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public findClassMeta(Ljava/lang/Class;)Lcom/vlingo/mda/model/ClassMeta;
    .locals 6
    .param p1, "cls"    # Ljava/lang/Class;

    .prologue
    .line 26
    invoke-virtual {p1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/mda/model/ModelMeta;->getPackageMetas()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/mda/model/PackageMeta;

    .line 28
    .local v2, "pm":Lcom/vlingo/mda/model/PackageMeta;
    if-nez v2, :cond_0

    .line 29
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Package meta not found:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 30
    :cond_0
    invoke-virtual {v2}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetas()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/mda/model/ClassMeta;

    .line 31
    .local v0, "cm":Lcom/vlingo/mda/model/ClassMeta;
    if-nez v0, :cond_1

    .line 32
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Class meta not found. package:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " cls:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 34
    :cond_1
    return-object v0
.end method

.method public findClassMeta(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;
    .locals 6
    .param p1, "fullClassName"    # Ljava/lang/String;

    .prologue
    .line 40
    const/4 v1, 0x0

    .line 42
    .local v1, "cm":Lcom/vlingo/mda/model/ClassMeta;
    const/16 v5, 0x2e

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 43
    .local v2, "lastDot":I
    const/4 v5, -0x1

    if-ne v2, v5, :cond_1

    .line 45
    invoke-direct {p0, p1}, Lcom/vlingo/mda/model/ModelMeta;->findClassMetaFromSimpleName(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v1

    .line 57
    :cond_0
    :goto_0
    return-object v1

    .line 49
    :cond_1
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 50
    .local v3, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/mda/model/ModelMeta;->getPackageMetas()Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/mda/model/PackageMeta;

    .line 51
    .local v4, "pm":Lcom/vlingo/mda/model/PackageMeta;
    if-eqz v4, :cond_0

    .line 52
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "className":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetas()Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "cm":Lcom/vlingo/mda/model/ClassMeta;
    check-cast v1, Lcom/vlingo/mda/model/ClassMeta;

    .restart local v1    # "cm":Lcom/vlingo/mda/model/ClassMeta;
    goto :goto_0
.end method

.method public getPackageMetas()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/PackageMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/mda/model/ModelMeta;->ivPackages:Ljava/util/HashMap;

    return-object v0
.end method
