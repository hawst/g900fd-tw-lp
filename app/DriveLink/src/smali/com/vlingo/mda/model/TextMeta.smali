.class public Lcom/vlingo/mda/model/TextMeta;
.super Ljava/lang/Object;
.source "TextMeta.java"


# instance fields
.field private ivBody:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private ivDoc:Ljava/lang/String;

.field private ivErrorEnum:Ljava/lang/String;

.field private ivErrorType:I

.field private ivName:Ljava/lang/String;

.field private ivParamList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/mda/model/ParamMeta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "errorEnum"    # Ljava/lang/String;
    .param p3, "doc"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/mda/model/TextMeta;->ivErrorType:I

    .line 34
    iput-object p1, p0, Lcom/vlingo/mda/model/TextMeta;->ivName:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/vlingo/mda/model/TextMeta;->ivErrorEnum:Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/TextMeta;->ivParamList:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/TextMeta;->ivBody:Ljava/util/ArrayList;

    .line 38
    iput-object p3, p0, Lcom/vlingo/mda/model/TextMeta;->ivDoc:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public addBodyElement(Ljava/lang/Object;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/mda/model/TextMeta;->ivBody:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method

.method public addParameter(Lcom/vlingo/mda/model/ParamMeta;)V
    .locals 1
    .param p1, "pm"    # Lcom/vlingo/mda/model/ParamMeta;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/mda/model/TextMeta;->ivParamList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/vlingo/mda/model/ParamMeta;->setIdx(I)V

    .line 55
    iget-object v0, p0, Lcom/vlingo/mda/model/TextMeta;->ivParamList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/mda/model/TextMeta;->ivName:Ljava/lang/String;

    return-object v0
.end method

.method public getParamMeta(Ljava/lang/String;)Lcom/vlingo/mda/model/ParamMeta;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 99
    iget-object v2, p0, Lcom/vlingo/mda/model/TextMeta;->ivParamList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/mda/model/ParamMeta;

    .line 100
    .local v1, "pm":Lcom/vlingo/mda/model/ParamMeta;
    invoke-virtual {v1}, Lcom/vlingo/mda/model/ParamMeta;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    .end local v1    # "pm":Lcom/vlingo/mda/model/ParamMeta;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
