.class public Lcom/vlingo/mda/model/ClassMeta;
.super Ljava/lang/Object;
.source "ClassMeta.java"


# instance fields
.field private ivAbstractBase:Z

.field private ivAttributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ivCacheStratgey:Ljava/lang/String;

.field private ivCollPropsByItemName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/PropertyMeta;",
            ">;"
        }
    .end annotation
.end field

.field private ivCompactName:Ljava/lang/String;

.field private ivDoc:Ljava/lang/String;

.field private ivExtends:Ljava/lang/String;

.field private ivImplements:Ljava/lang/String;

.field private ivMessageType:Ljava/lang/String;

.field private ivName:Ljava/lang/String;

.field private ivPM:Lcom/vlingo/mda/model/PackageMeta;

.field private ivPropMetas:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/PropertyMeta;",
            ">;"
        }
    .end annotation
.end field

.field private ivPropMetasByCompactName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/PropertyMeta;",
            ">;"
        }
    .end annotation
.end field

.field private ivSchemaVersion:Lcom/vlingo/mda/model/SchemaVersion;

.field private ivSuperclassMeta:Lcom/vlingo/mda/model/ClassMeta;

.field private ivTableName:Ljava/lang/String;

.field private ivTextMetas:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/TextMeta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/vlingo/mda/model/PackageMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "pm"    # Lcom/vlingo/mda/model/PackageMeta;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "extendsStr"    # Ljava/lang/String;
    .param p4, "implementsStr"    # Ljava/lang/String;
    .param p5, "messageType"    # Ljava/lang/String;
    .param p6, "doc"    # Ljava/lang/String;
    .param p7, "cacheStrategy"    # Ljava/lang/String;
    .param p8, "compactName"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPropMetas:Ljava/util/HashMap;

    .line 30
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPropMetasByCompactName:Ljava/util/HashMap;

    .line 31
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivCollPropsByItemName:Ljava/util/HashMap;

    .line 32
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivTextMetas:Ljava/util/HashMap;

    .line 33
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivAttributes:Ljava/util/HashMap;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivSchemaVersion:Lcom/vlingo/mda/model/SchemaVersion;

    .line 42
    iput-object p2, p0, Lcom/vlingo/mda/model/ClassMeta;->ivName:Ljava/lang/String;

    .line 43
    iput-object p1, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPM:Lcom/vlingo/mda/model/PackageMeta;

    .line 44
    iput-object p3, p0, Lcom/vlingo/mda/model/ClassMeta;->ivExtends:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/vlingo/mda/model/ClassMeta;->ivImplements:Ljava/lang/String;

    .line 46
    iput-object p5, p0, Lcom/vlingo/mda/model/ClassMeta;->ivMessageType:Ljava/lang/String;

    .line 47
    iput-object p6, p0, Lcom/vlingo/mda/model/ClassMeta;->ivDoc:Ljava/lang/String;

    .line 48
    iput-object p7, p0, Lcom/vlingo/mda/model/ClassMeta;->ivCacheStratgey:Ljava/lang/String;

    .line 49
    iput-object p8, p0, Lcom/vlingo/mda/model/ClassMeta;->ivCompactName:Ljava/lang/String;

    .line 51
    return-void
.end method

.method private getSimpleName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "javaType"    # Ljava/lang/String;

    .prologue
    .line 163
    if-nez p1, :cond_1

    const/4 p1, 0x0

    .line 169
    .end local p1    # "javaType":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 165
    .restart local p1    # "javaType":Ljava/lang/String;
    :cond_1
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 166
    .local v0, "idx":I
    if-ltz v0, :cond_0

    .line 169
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public addProperty(Lcom/vlingo/mda/model/PropertyMeta;)V
    .locals 6
    .param p1, "pm"    # Lcom/vlingo/mda/model/PropertyMeta;

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getPropertyMetas()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 110
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Object already has property:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getPropertyMetas()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    invoke-virtual {p1}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 117
    iget-object v3, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPropMetasByCompactName:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 119
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Object already has property:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " with compact name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " or compact name conflict exists"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 121
    :cond_1
    iget-object v3, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPropMetasByCompactName:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    :cond_2
    instance-of v3, p1, Lcom/vlingo/mda/model/CollectionPropertyMeta;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 126
    check-cast v0, Lcom/vlingo/mda/model/CollectionPropertyMeta;

    .line 128
    .local v0, "cpm":Lcom/vlingo/mda/model/CollectionPropertyMeta;
    invoke-virtual {v0}, Lcom/vlingo/mda/model/CollectionPropertyMeta;->isFlat()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 131
    invoke-virtual {v0}, Lcom/vlingo/mda/model/CollectionPropertyMeta;->getJavaType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/vlingo/mda/model/ClassMeta;->getSimpleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 132
    .local v2, "simpleName":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/mda/model/ClassMeta;->ivCollPropsByItemName:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 134
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Object already has property:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " with name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " or compact name conflict exists"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 136
    :cond_3
    iget-object v3, p0, Lcom/vlingo/mda/model/ClassMeta;->ivCollPropsByItemName:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v3, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPM:Lcom/vlingo/mda/model/PackageMeta;

    invoke-virtual {v3}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetas()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/mda/model/ClassMeta;

    .line 139
    .local v1, "scm":Lcom/vlingo/mda/model/ClassMeta;
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/vlingo/mda/model/ClassMeta;->getCompactName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 141
    iget-object v3, p0, Lcom/vlingo/mda/model/ClassMeta;->ivCollPropsByItemName:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/vlingo/mda/model/ClassMeta;->getCompactName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 143
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Object already has property:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " with compact name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/mda/model/ClassMeta;->getCompactName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " or compact name conflict exists"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 145
    :cond_4
    iget-object v3, p0, Lcom/vlingo/mda/model/ClassMeta;->ivCollPropsByItemName:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/vlingo/mda/model/ClassMeta;->getCompactName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    .end local v0    # "cpm":Lcom/vlingo/mda/model/CollectionPropertyMeta;
    .end local v1    # "scm":Lcom/vlingo/mda/model/ClassMeta;
    .end local v2    # "simpleName":Ljava/lang/String;
    :cond_5
    :goto_0
    return-void

    .line 149
    .restart local v0    # "cpm":Lcom/vlingo/mda/model/CollectionPropertyMeta;
    .restart local v1    # "scm":Lcom/vlingo/mda/model/ClassMeta;
    .restart local v2    # "simpleName":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPM:Lcom/vlingo/mda/model/PackageMeta;

    invoke-virtual {v3}, Lcom/vlingo/mda/model/PackageMeta;->getOpenClasses()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 152
    iget-object v3, p0, Lcom/vlingo/mda/model/ClassMeta;->ivCollPropsByItemName:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 155
    :cond_7
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Class meta for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " needs to be defined before collection reference in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public addText(Lcom/vlingo/mda/model/TextMeta;)V
    .locals 3
    .param p1, "tm"    # Lcom/vlingo/mda/model/TextMeta;

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getTextMetas()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/mda/model/TextMeta;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Object already has property:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/mda/model/TextMeta;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getTextMetas()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/mda/model/TextMeta;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    return-void
.end method

.method public findPropertyMetaByClass(Ljava/lang/Class;)Ljava/util/Collection;
    .locals 1
    .param p1, "cls"    # Ljava/lang/Class;

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getPropertyMetas()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/vlingo/mda/util/GenUtil;->findByClass(Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public findPropertyMetaByName(Ljava/lang/String;)Lcom/vlingo/mda/model/PropertyMeta;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 210
    iget-object v1, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPropMetas:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/mda/model/PropertyMeta;

    .line 211
    .local v0, "res":Lcom/vlingo/mda/model/PropertyMeta;
    if-nez v0, :cond_0

    .line 213
    iget-object v1, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPropMetasByCompactName:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "res":Lcom/vlingo/mda/model/PropertyMeta;
    check-cast v0, Lcom/vlingo/mda/model/PropertyMeta;

    .line 215
    .restart local v0    # "res":Lcom/vlingo/mda/model/PropertyMeta;
    :cond_0
    if-nez v0, :cond_1

    .line 217
    iget-object v1, p0, Lcom/vlingo/mda/model/ClassMeta;->ivCollPropsByItemName:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "res":Lcom/vlingo/mda/model/PropertyMeta;
    check-cast v0, Lcom/vlingo/mda/model/PropertyMeta;

    .line 219
    .restart local v0    # "res":Lcom/vlingo/mda/model/PropertyMeta;
    :cond_1
    return-object v0
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivAttributes:Ljava/util/HashMap;

    return-object v0
.end method

.method public getCompactName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivCompactName:Ljava/lang/String;

    return-object v0
.end method

.method public getExtends()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivExtends:Ljava/lang/String;

    return-object v0
.end method

.method public getFullName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPM:Lcom/vlingo/mda/model/PackageMeta;

    invoke-virtual {v1}, Lcom/vlingo/mda/model/PackageMeta;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPM:Lcom/vlingo/mda/model/PackageMeta;

    return-object v0
.end method

.method public getPropertyMetas()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/PropertyMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivPropMetas:Ljava/util/HashMap;

    return-object v0
.end method

.method public getSuperclassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 11

    .prologue
    .line 225
    iget-object v8, p0, Lcom/vlingo/mda/model/ClassMeta;->ivSuperclassMeta:Lcom/vlingo/mda/model/ClassMeta;

    if-eqz v8, :cond_0

    .line 226
    iget-object v8, p0, Lcom/vlingo/mda/model/ClassMeta;->ivSuperclassMeta:Lcom/vlingo/mda/model/ClassMeta;

    .line 254
    :goto_0
    return-object v8

    .line 229
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getExtends()Ljava/lang/String;

    move-result-object v7

    .line 230
    .local v7, "superclassName":Ljava/lang/String;
    if-eqz v7, :cond_2

    .line 234
    :try_start_0
    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    .line 235
    .local v6, "superclass":Ljava/lang/Class;
    if-eqz v6, :cond_2

    .line 237
    invoke-virtual {v6}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v5

    .line 238
    .local v5, "methods":[Ljava/lang/reflect/Method;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/reflect/Method;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 240
    .local v4, "method":Ljava/lang/reflect/Method;
    const-string/jumbo v8, "getClassMetaStatic"

    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 243
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/mda/model/ClassMeta;

    iput-object v8, p0, Lcom/vlingo/mda/model/ClassMeta;->ivSuperclassMeta:Lcom/vlingo/mda/model/ClassMeta;

    .line 244
    iget-object v8, p0, Lcom/vlingo/mda/model/ClassMeta;->ivSuperclassMeta:Lcom/vlingo/mda/model/ClassMeta;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 238
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 249
    .end local v0    # "arr$":[Ljava/lang/reflect/Method;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "method":Ljava/lang/reflect/Method;
    .end local v5    # "methods":[Ljava/lang/reflect/Method;
    .end local v6    # "superclass":Ljava/lang/Class;
    :catch_0
    move-exception v1

    .line 251
    .local v1, "e":Ljava/lang/Exception;
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Unable to find class meta for superclass "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ": "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 254
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public getTextMetas()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/TextMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/vlingo/mda/model/ClassMeta;->ivTextMetas:Ljava/util/HashMap;

    return-object v0
.end method

.method public setIsAbstractBase(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/vlingo/mda/model/ClassMeta;->ivAbstractBase:Z

    .line 60
    return-void
.end method

.method public setSuperclassMeta(Lcom/vlingo/mda/model/ClassMeta;)V
    .locals 0
    .param p1, "superclassMeta"    # Lcom/vlingo/mda/model/ClassMeta;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/mda/model/ClassMeta;->ivSuperclassMeta:Lcom/vlingo/mda/model/ClassMeta;

    .line 55
    return-void
.end method

.method public setTableName(Ljava/lang/String;)V
    .locals 0
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vlingo/mda/model/ClassMeta;->ivTableName:Ljava/lang/String;

    .line 70
    return-void
.end method
