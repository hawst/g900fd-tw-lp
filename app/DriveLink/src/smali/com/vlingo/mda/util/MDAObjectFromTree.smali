.class public Lcom/vlingo/mda/util/MDAObjectFromTree;
.super Ljava/lang/Object;
.source "MDAObjectFromTree.java"


# static fields
.field private static final ivLogger:Lorg/apache/log4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/vlingo/mda/util/MDAObjectFromTree;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static append(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "org"    # Ljava/lang/String;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 258
    if-nez p0, :cond_0

    .line 263
    .end local p1    # "str":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "str":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static extractTextBody(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;
    .locals 6
    .param p0, "parent"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 270
    const/4 v2, 0x0

    .line 271
    .local v2, "res":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/common/message/MNode;

    .line 273
    .local v0, "child":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v3, "#text"

    invoke-virtual {v0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 275
    invoke-virtual {v0}, Lcom/vlingo/common/message/MNode;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/vlingo/mda/util/MDAObjectFromTree;->append(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 276
    :cond_0
    const-string/jumbo v3, "#cdata-section"

    invoke-virtual {v0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 278
    invoke-virtual {v0}, Lcom/vlingo/common/message/MNode;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/vlingo/mda/util/MDAObjectFromTree;->append(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 281
    :cond_1
    sget-object v3, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Text node name not recognized:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/log4j/Logger;->error(Ljava/lang/Object;)V

    goto :goto_0

    .line 284
    .end local v0    # "child":Lcom/vlingo/common/message/MNode;
    :cond_2
    return-object v2
.end method

.method private static findFirstChild(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;
    .locals 4
    .param p0, "container"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/common/message/MNode;

    .line 291
    .local v0, "child":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "#text"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "#comment"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 300
    .end local v0    # "child":Lcom/vlingo/common/message/MNode;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static findPropertyMeta(Ljava/lang/String;Lcom/vlingo/mda/model/ClassMeta;)Lcom/vlingo/mda/model/PropertyMeta;
    .locals 2
    .param p0, "propertyName"    # Ljava/lang/String;
    .param p1, "clMeta"    # Lcom/vlingo/mda/model/ClassMeta;

    .prologue
    .line 185
    invoke-virtual {p1, p0}, Lcom/vlingo/mda/model/ClassMeta;->findPropertyMetaByName(Ljava/lang/String;)Lcom/vlingo/mda/model/PropertyMeta;

    move-result-object v0

    .line 186
    .local v0, "propMeta":Lcom/vlingo/mda/model/PropertyMeta;
    if-nez v0, :cond_0

    .line 189
    invoke-virtual {p1}, Lcom/vlingo/mda/model/ClassMeta;->getSuperclassMeta()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v1

    .line 190
    .local v1, "superclassMeta":Lcom/vlingo/mda/model/ClassMeta;
    if-eqz v1, :cond_0

    .line 192
    invoke-static {p0, v1}, Lcom/vlingo/mda/util/MDAObjectFromTree;->findPropertyMeta(Ljava/lang/String;Lcom/vlingo/mda/model/ClassMeta;)Lcom/vlingo/mda/model/PropertyMeta;

    move-result-object v0

    .line 195
    .end local v0    # "propMeta":Lcom/vlingo/mda/model/PropertyMeta;
    .end local v1    # "superclassMeta":Lcom/vlingo/mda/model/ClassMeta;
    :cond_0
    return-object v0
.end method

.method private static getAllowedSubTags(Lcom/vlingo/mda/model/ClassMeta;)Ljava/util/Set;
    .locals 4
    .param p0, "cm"    # Lcom/vlingo/mda/model/ClassMeta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/mda/model/ClassMeta;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 206
    .local v2, "res":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/vlingo/mda/model/ClassMeta;->getPropertyMetas()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/mda/model/PropertyMeta;

    .line 208
    .local v1, "pm":Lcom/vlingo/mda/model/PropertyMeta;
    invoke-static {v1}, Lcom/vlingo/mda/util/MDAObjectFromTree;->isPersistXML(Lcom/vlingo/mda/model/PropertyMeta;)Z

    move-result v3

    if-eqz v3, :cond_0

    instance-of v3, v1, Lcom/vlingo/mda/model/CollectionPropertyMeta;

    if-nez v3, :cond_1

    instance-of v3, v1, Lcom/vlingo/mda/model/ReferencePropertyMeta;

    if-eqz v3, :cond_0

    .line 210
    :cond_1
    invoke-virtual {v1}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 211
    invoke-virtual {v1}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 213
    invoke-virtual {v1}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    .end local v1    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    :cond_2
    return-object v2
.end method

.method static isPersistXML(Lcom/vlingo/mda/model/PropertyMeta;)Z
    .locals 3
    .param p0, "pm"    # Lcom/vlingo/mda/model/PropertyMeta;

    .prologue
    .line 200
    const-string/jumbo v0, "true"

    invoke-virtual {p0}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "xpxml"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static loadCollection(Ljava/util/Collection;Lcom/vlingo/common/message/MNode;ZLcom/vlingo/mda/util/MDAMetaProvider;)V
    .locals 8
    .param p1, "container"    # Lcom/vlingo/common/message/MNode;
    .param p2, "flatCollection"    # Z
    .param p3, "metaProvider"    # Lcom/vlingo/mda/util/MDAMetaProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/mda/util/MDAObject;",
            ">;",
            "Lcom/vlingo/common/message/MNode;",
            "Z",
            "Lcom/vlingo/mda/util/MDAMetaProvider;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 223
    .local p0, "coll":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/mda/util/MDAObject;>;"
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getAttributes()Ljava/util/Map;

    move-result-object v0

    .line 225
    .local v0, "attr":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 229
    :cond_0
    if-eqz p2, :cond_2

    .line 231
    invoke-static {p1, p3}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v5

    invoke-interface {p0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_1
    return-void

    .line 234
    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/common/message/MNode;

    .line 236
    .local v1, "child":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v4

    .line 237
    .local v4, "nodeName":Ljava/lang/String;
    const-string/jumbo v5, "#text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "#comment"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 239
    invoke-interface {p3, v4}, Lcom/vlingo/mda/util/MDAMetaProvider;->findClassMeta(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v2

    .line 240
    .local v2, "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    if-nez v2, :cond_4

    .line 242
    invoke-interface {p3, v4}, Lcom/vlingo/mda/util/MDAMetaProvider;->findClassMetaByCompactName(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v2

    .line 244
    :cond_4
    if-eqz v2, :cond_5

    .line 246
    invoke-static {v1, p3}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v5

    invoke-interface {p0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 249
    :cond_5
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Dont recognize XML node in relation to MDA object model:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public static loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;)Lcom/vlingo/mda/util/MDAObject;
    .locals 24
    .param p0, "objNode"    # Lcom/vlingo/common/message/MNode;
    .param p1, "metaProvider"    # Lcom/vlingo/mda/util/MDAMetaProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 35
    if-nez p1, :cond_0

    .line 36
    new-instance v20, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v21, "metaProvider can not be null"

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 38
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v16

    .line 40
    .local v16, "objName":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/vlingo/mda/util/MDAMetaProvider;->findClassMeta(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v8

    .line 41
    .local v8, "cm":Lcom/vlingo/mda/model/ClassMeta;
    if-nez v8, :cond_1

    .line 43
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/vlingo/mda/util/MDAMetaProvider;->findClassMetaByCompactName(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v8

    .line 45
    :cond_1
    sget-object v20, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    invoke-virtual/range {v20 .. v20}, Lorg/apache/log4j/Logger;->isTraceEnabled()Z

    move-result v20

    if-eqz v20, :cond_2

    sget-object v20, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "Processing node:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " classMeta:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lorg/apache/log4j/Logger;->trace(Ljava/lang/Object;)V

    .line 48
    :cond_2
    if-nez v8, :cond_3

    .line 50
    new-instance v20, Ljava/lang/RuntimeException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "No class meta found for: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 53
    :cond_3
    invoke-virtual {v8}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/vlingo/mda/util/MDAObject;

    .line 55
    .local v18, "result":Lcom/vlingo/mda/util/MDAObject;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/common/message/MNode;->getAttributes()Ljava/util/Map;

    move-result-object v5

    .line 58
    .local v5, "atrs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_8

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 60
    .local v15, "name":Ljava/lang/String;
    invoke-static {v15, v8}, Lcom/vlingo/mda/util/MDAObjectFromTree;->findPropertyMeta(Ljava/lang/String;Lcom/vlingo/mda/model/ClassMeta;)Lcom/vlingo/mda/model/PropertyMeta;

    move-result-object v17

    .line 61
    .local v17, "pm":Lcom/vlingo/mda/model/PropertyMeta;
    if-nez v17, :cond_5

    .line 62
    new-instance v20, Ljava/lang/RuntimeException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "Property meta not found. prop:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " class:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v8}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " for input: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 63
    :cond_5
    invoke-static/range {v17 .. v17}, Lcom/vlingo/mda/util/MDAObjectFromTree;->isPersistXML(Lcom/vlingo/mda/model/PropertyMeta;)Z

    move-result v14

    .line 64
    .local v14, "isPersistXML":Z
    sget-object v20, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    invoke-virtual/range {v20 .. v20}, Lorg/apache/log4j/Logger;->isTraceEnabled()Z

    move-result v20

    if-eqz v20, :cond_6

    .line 65
    sget-object v20, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "Checking atr name:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " propMeta:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " persistXML:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lorg/apache/log4j/Logger;->trace(Ljava/lang/Object;)V

    .line 66
    :cond_6
    if-eqz v17, :cond_7

    if-nez v14, :cond_4

    .line 68
    :cond_7
    new-instance v20, Ljava/lang/RuntimeException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "Dont recognize property in XML:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, "."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 73
    .end local v14    # "isPersistXML":Z
    .end local v15    # "name":Ljava/lang/String;
    .end local v17    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    :cond_8
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_9
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 75
    .local v3, "atr":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-static {v0, v8}, Lcom/vlingo/mda/util/MDAObjectFromTree;->findPropertyMeta(Ljava/lang/String;Lcom/vlingo/mda/model/ClassMeta;)Lcom/vlingo/mda/model/PropertyMeta;

    move-result-object v17

    .line 76
    .restart local v17    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    if-eqz v17, :cond_9

    invoke-static/range {v17 .. v17}, Lcom/vlingo/mda/util/MDAObjectFromTree;->isPersistXML(Lcom/vlingo/mda/model/PropertyMeta;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 78
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 79
    .local v4, "atrValue":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v20

    const-string/jumbo v21, "xformat"

    invoke-interface/range {v20 .. v21}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 80
    .local v12, "format":Ljava/lang/String;
    if-eqz v4, :cond_9

    .line 82
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-static {v0, v1, v12, v4}, Lcom/vlingo/mda/util/MDAUtil;->setPropertyValue(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 89
    .end local v3    # "atr":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "atrValue":Ljava/lang/String;
    .end local v12    # "format":Ljava/lang/String;
    .end local v17    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_b
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_18

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/common/message/MNode;

    .line 91
    .local v6, "child":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v20

    const-string/jumbo v21, "#text"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_b

    .line 92
    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v20

    const-string/jumbo v21, "#comment"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_b

    .line 95
    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v0, v8}, Lcom/vlingo/mda/util/MDAObjectFromTree;->findPropertyMeta(Ljava/lang/String;Lcom/vlingo/mda/model/ClassMeta;)Lcom/vlingo/mda/model/PropertyMeta;

    move-result-object v17

    .line 98
    .restart local v17    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    if-nez v17, :cond_d

    .line 99
    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v15

    .line 102
    .restart local v15    # "name":Ljava/lang/String;
    :cond_c
    const/16 v17, 0x0

    .line 103
    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Lcom/vlingo/mda/util/MDAMetaProvider;->findClassMeta(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v7

    .line 104
    .local v7, "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    if-nez v7, :cond_e

    .line 122
    .end local v7    # "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    .end local v15    # "name":Ljava/lang/String;
    :cond_d
    :goto_2
    if-nez v17, :cond_f

    .line 124
    new-instance v20, Ljava/lang/RuntimeException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "Property meta not found:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " on object:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 107
    .restart local v7    # "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    .restart local v15    # "name":Ljava/lang/String;
    :cond_e
    invoke-virtual {v7}, Lcom/vlingo/mda/model/ClassMeta;->getExtends()Ljava/lang/String;

    move-result-object v15

    .line 108
    if-eqz v15, :cond_d

    .line 109
    invoke-static {v15, v8}, Lcom/vlingo/mda/util/MDAObjectFromTree;->findPropertyMeta(Ljava/lang/String;Lcom/vlingo/mda/model/ClassMeta;)Lcom/vlingo/mda/model/PropertyMeta;

    move-result-object v17

    .line 110
    move-object/from16 v0, v17

    instance-of v0, v0, Lcom/vlingo/mda/model/CollectionPropertyMeta;

    move/from16 v20, v0

    if-eqz v20, :cond_c

    move-object/from16 v9, v17

    .line 112
    check-cast v9, Lcom/vlingo/mda/model/CollectionPropertyMeta;

    .line 113
    .local v9, "cpm":Lcom/vlingo/mda/model/CollectionPropertyMeta;
    invoke-virtual {v9}, Lcom/vlingo/mda/model/CollectionPropertyMeta;->isFlat()Z

    move-result v20

    if-eqz v20, :cond_c

    goto :goto_2

    .line 126
    .end local v7    # "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    .end local v9    # "cpm":Lcom/vlingo/mda/model/CollectionPropertyMeta;
    .end local v15    # "name":Ljava/lang/String;
    :cond_f
    invoke-static/range {v17 .. v17}, Lcom/vlingo/mda/util/MDAObjectFromTree;->isPersistXML(Lcom/vlingo/mda/model/PropertyMeta;)Z

    move-result v14

    .line 128
    .restart local v14    # "isPersistXML":Z
    sget-object v20, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    invoke-virtual/range {v20 .. v20}, Lorg/apache/log4j/Logger;->isTraceEnabled()Z

    move-result v20

    if-eqz v20, :cond_10

    .line 129
    sget-object v20, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "Processing child node:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " propMeta:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " isPersistXML:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lorg/apache/log4j/Logger;->trace(Ljava/lang/Object;)V

    .line 131
    :cond_10
    if-eqz v17, :cond_17

    if-eqz v14, :cond_17

    .line 133
    move-object/from16 v0, v17

    instance-of v0, v0, Lcom/vlingo/mda/model/CollectionPropertyMeta;

    move/from16 v20, v0

    if-eqz v20, :cond_11

    .line 135
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/bzbyte/util/Util;->getPropertyValue(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Collection;

    .line 136
    .local v10, "dest":Ljava/util/Collection;
    check-cast v17, Lcom/vlingo/mda/model/CollectionPropertyMeta;

    .end local v17    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/mda/model/CollectionPropertyMeta;->isFlat()Z

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p1

    invoke-static {v10, v6, v0, v1}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadCollection(Ljava/util/Collection;Lcom/vlingo/common/message/MNode;ZLcom/vlingo/mda/util/MDAMetaProvider;)V

    goto/16 :goto_1

    .line 137
    .end local v10    # "dest":Ljava/util/Collection;
    .restart local v17    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    :cond_11
    move-object/from16 v0, v17

    instance-of v0, v0, Lcom/vlingo/mda/model/SimplePropertyMeta;

    move/from16 v20, v0

    if-eqz v20, :cond_14

    .line 139
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/mda/model/PropertyMeta;->getJavaType()Ljava/lang/String;

    move-result-object v20

    const-class v21, Lcom/vlingo/common/message/MNode;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_13

    .line 142
    invoke-static {v6}, Lcom/vlingo/mda/util/MDAObjectFromTree;->extractTextBody(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v19

    .line 143
    .local v19, "strVal":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v20

    const-string/jumbo v21, "xformat"

    invoke-interface/range {v20 .. v21}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 144
    .restart local v12    # "format":Ljava/lang/String;
    sget-object v20, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    invoke-virtual/range {v20 .. v20}, Lorg/apache/log4j/Logger;->isTraceEnabled()Z

    move-result v20

    if-eqz v20, :cond_12

    .line 145
    sget-object v20, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "TextBody of SimpleProperty. name:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " text:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lorg/apache/log4j/Logger;->trace(Ljava/lang/Object;)V

    .line 146
    :cond_12
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-static {v0, v1, v12, v2}, Lcom/vlingo/mda/util/MDAUtil;->setPropertyValue(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 150
    .end local v12    # "format":Ljava/lang/String;
    .end local v19    # "strVal":Ljava/lang/String;
    :cond_13
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v21

    const/16 v22, 0x0

    invoke-interface/range {v21 .. v22}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/bzbyte/util/Util;->setPropertyValue(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 152
    :cond_14
    move-object/from16 v0, v17

    instance-of v0, v0, Lcom/vlingo/mda/model/ReferencePropertyMeta;

    move/from16 v20, v0

    if-eqz v20, :cond_16

    .line 155
    const/4 v11, 0x1

    .line 156
    .local v11, "firstChild":Z
    const-string/jumbo v20, "true"

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v21

    const-string/jumbo v22, "x-flat"

    invoke-interface/range {v21 .. v22}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_15

    .line 158
    const/4 v11, 0x0

    .line 160
    :cond_15
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-static {v0, v6, v1, v11}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadReference(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;Z)V

    goto/16 :goto_1

    .line 163
    .end local v11    # "firstChild":Z
    :cond_16
    new-instance v20, Ljava/lang/RuntimeException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "Dont know how to handle property meta:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 165
    :cond_17
    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v20

    const-string/jumbo v21, "#comment"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_b

    .line 170
    new-instance v20, Ljava/lang/RuntimeException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "Dont know how to handle tag:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " under:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " recognized:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {v8}, Lcom/vlingo/mda/util/MDAObjectFromTree;->getAllowedSubTags(Lcom/vlingo/mda/model/ClassMeta;)Ljava/util/Set;

    move-result-object v22

    const-string/jumbo v23, ","

    invoke-static/range {v22 .. v23}, Lcom/bzbyte/util/Util;->toString(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 176
    .end local v6    # "child":Lcom/vlingo/common/message/MNode;
    .end local v14    # "isPersistXML":Z
    .end local v17    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    :cond_18
    invoke-static/range {v18 .. v18}, Lcom/vlingo/mda/util/MDAUtil;->ensureRequiredValues(Lcom/vlingo/mda/util/MDAObject;)V

    .line 178
    return-object v18
.end method

.method private static loadReference(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;Z)V
    .locals 6
    .param p0, "parent"    # Lcom/vlingo/mda/util/MDAObject;
    .param p1, "container"    # Lcom/vlingo/common/message/MNode;
    .param p2, "metaProvider"    # Lcom/vlingo/mda/util/MDAMetaProvider;
    .param p3, "useFirstChild"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 307
    move-object v1, p1

    .line 308
    .local v1, "item":Lcom/vlingo/common/message/MNode;
    if-eqz p3, :cond_0

    .line 310
    invoke-static {p1}, Lcom/vlingo/mda/util/MDAObjectFromTree;->findFirstChild(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;

    move-result-object v1

    .line 313
    :cond_0
    if-eqz v1, :cond_2

    .line 315
    invoke-virtual {v1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/vlingo/mda/util/MDAMetaProvider;->findClassMeta(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    .line 316
    .local v0, "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    sget-object v3, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    invoke-virtual {v3}, Lorg/apache/log4j/Logger;->isTraceEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 317
    sget-object v3, Lcom/vlingo/mda/util/MDAObjectFromTree;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Load reference:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " classMeta:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/log4j/Logger;->trace(Ljava/lang/Object;)V

    .line 318
    :cond_1
    if-eqz v0, :cond_3

    .line 320
    invoke-static {v1, p2}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v2

    .line 321
    .local v2, "obj":Lcom/vlingo/mda/util/MDAObject;
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v2}, Lcom/bzbyte/util/Util;->setPropertyValue(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 329
    .end local v0    # "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    .end local v2    # "obj":Lcom/vlingo/mda/util/MDAObject;
    :cond_2
    return-void

    .line 324
    .restart local v0    # "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    :cond_3
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Dont know how to parse node:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " class meta not found:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
