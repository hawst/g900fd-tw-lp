.class public Lcom/vlingo/mda/util/MDAUtil;
.super Ljava/lang/Object;
.source "MDAUtil.java"


# static fields
.field private static final ivLogger:Lorg/apache/log4j/Logger;

.field private static final ivMethodCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/mda/util/MDAUtil;->ivMethodCache:Ljava/util/HashMap;

    .line 26
    const-class v0, Lcom/vlingo/mda/util/MDAUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/String;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/mda/util/MDAUtil;->ivLogger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ensureRequiredValues(Lcom/vlingo/mda/util/MDAObject;)V
    .locals 12
    .param p0, "obj"    # Lcom/vlingo/mda/util/MDAObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 307
    const-string/jumbo v5, ""

    .line 308
    .local v5, "missingValues":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 309
    .local v2, "emptyValues":Ljava/lang/String;
    const-string/jumbo v1, ""

    .line 310
    .local v1, "emptyStringValues":Ljava/lang/String;
    invoke-interface {p0}, Lcom/vlingo/mda/util/MDAObject;->getClassMeta()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    .line 312
    .local v0, "cm":Lcom/vlingo/mda/model/ClassMeta;
    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getPropertyMetas()Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/mda/model/PropertyMeta;

    .line 314
    .local v8, "pm":Lcom/vlingo/mda/model/PropertyMeta;
    invoke-static {v8}, Lcom/vlingo/mda/util/MDAUtil;->needsValue(Lcom/vlingo/mda/model/PropertyMeta;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 316
    invoke-virtual {v8}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/bzbyte/util/Util;->getPropertyValue(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 317
    .local v6, "o":Ljava/lang/Object;
    if-nez v6, :cond_0

    .line 319
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v8}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 322
    .end local v6    # "o":Ljava/lang/Object;
    :cond_1
    invoke-static {v8}, Lcom/vlingo/mda/util/MDAUtil;->isNonEmpty(Lcom/vlingo/mda/model/PropertyMeta;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 324
    invoke-virtual {v8}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/bzbyte/util/Util;->getPropertyValue(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 325
    .restart local v6    # "o":Ljava/lang/Object;
    if-eqz v6, :cond_2

    .line 326
    const-class v9, Ljava/lang/String;

    invoke-static {v9, v6}, Lcom/bzbyte/util/Util;->typeConvert(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 327
    .local v7, "oStr":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_0

    .line 329
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v8}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 334
    .end local v7    # "oStr":Ljava/lang/String;
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v8}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 337
    .end local v6    # "o":Ljava/lang/Object;
    :cond_3
    invoke-static {v8}, Lcom/vlingo/mda/util/MDAUtil;->isNoEmptyString(Lcom/vlingo/mda/model/PropertyMeta;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 339
    invoke-virtual {v8}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/bzbyte/util/Util;->getPropertyValue(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 340
    .restart local v6    # "o":Ljava/lang/Object;
    if-eqz v6, :cond_0

    .line 342
    const-class v9, Ljava/lang/String;

    invoke-static {v9, v6}, Lcom/bzbyte/util/Util;->typeConvert(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 343
    .restart local v7    # "oStr":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_0

    .line 345
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_4

    .line 346
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 347
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v8}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 353
    .end local v6    # "o":Ljava/lang/Object;
    .end local v7    # "oStr":Ljava/lang/String;
    .end local v8    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    :cond_5
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    if-gtz v9, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_7

    .line 355
    :cond_6
    const-string/jumbo v4, ""

    .line 356
    .local v4, "message":Ljava/lang/String;
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "instance of "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " is missing/null values for properties {"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "} or has empty values {"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "}"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 358
    .end local v4    # "message":Ljava/lang/String;
    :cond_7
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_8

    .line 360
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "instance of "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " has properties with empty string values:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 362
    :cond_8
    return-void
.end method

.method private static declared-synchronized findMethodByName(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 5
    .param p0, "cls"    # Ljava/lang/Class;
    .param p1, "methodName"    # Ljava/lang/String;

    .prologue
    .line 44
    const-class v3, Lcom/vlingo/mda/util/MDAUtil;

    monitor-enter v3

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "key":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/mda/util/MDAUtil;->ivMethodCache:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Method;

    .line 46
    .local v1, "method":Ljava/lang/reflect/Method;
    :goto_0
    if-nez v1, :cond_1

    if-eqz p0, :cond_1

    .line 47
    invoke-static {p0, p1}, Lcom/vlingo/mda/util/MDAUtil;->getDeclaredMethodForClass(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 48
    if-eqz v1, :cond_0

    .line 49
    sget-object v2, Lcom/vlingo/mda/util/MDAUtil;->ivMethodCache:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p0

    goto :goto_0

    .line 53
    :cond_1
    monitor-exit v3

    return-object v1

    .line 44
    .end local v0    # "key":Ljava/lang/String;
    .end local v1    # "method":Ljava/lang/reflect/Method;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static getAnySetterMethod(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 2
    .param p0, "cls"    # Ljava/lang/Class;
    .param p1, "propName"    # Ljava/lang/String;

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/vlingo/mda/util/MDAUtil;->findMethodByName(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    return-object v0
.end method

.method private static getDeclaredMethodForClass(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 5
    .param p0, "cls"    # Ljava/lang/Class;
    .param p1, "methodName"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/reflect/Method;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 35
    .local v3, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 39
    .end local v3    # "method":Ljava/lang/reflect/Method;
    :goto_1
    return-object v3

    .line 34
    .restart local v3    # "method":Ljava/lang/reflect/Method;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 39
    .end local v3    # "method":Ljava/lang/reflect/Method;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static getSetterMethod(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 2
    .param p0, "cls"    # Ljava/lang/Class;
    .param p1, "propName"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-static {p0, p1}, Lcom/vlingo/mda/util/MDAUtil;->getAnySetterMethod(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 69
    .local v0, "method":Ljava/lang/reflect/Method;
    invoke-static {v0}, Lcom/vlingo/mda/util/MDAUtil;->isPublicMethod(Ljava/lang/reflect/Method;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    .end local v0    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v0

    .restart local v0    # "method":Ljava/lang/reflect/Method;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isNoEmptyString(Lcom/vlingo/mda/model/PropertyMeta;)Z
    .locals 3
    .param p0, "pm"    # Lcom/vlingo/mda/model/PropertyMeta;

    .prologue
    .line 376
    const-string/jumbo v0, "true"

    invoke-virtual {p0}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "x-no-empty-string"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isNonEmpty(Lcom/vlingo/mda/model/PropertyMeta;)Z
    .locals 3
    .param p0, "pm"    # Lcom/vlingo/mda/model/PropertyMeta;

    .prologue
    .line 371
    const-string/jumbo v0, "true"

    invoke-virtual {p0}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "x-non-empty-value"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isPublicMethod(Ljava/lang/reflect/Method;)Z
    .locals 1
    .param p0, "method"    # Ljava/lang/reflect/Method;

    .prologue
    .line 29
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v0

    return v0
.end method

.method public static isSensitive(Lcom/vlingo/mda/model/PropertyMeta;)Z
    .locals 3
    .param p0, "pm"    # Lcom/vlingo/mda/model/PropertyMeta;

    .prologue
    .line 386
    const-string/jumbo v0, "true"

    invoke-virtual {p0}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "x-sensitive"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static needsValue(Lcom/vlingo/mda/model/PropertyMeta;)Z
    .locals 3
    .param p0, "pm"    # Lcom/vlingo/mda/model/PropertyMeta;

    .prologue
    .line 366
    const-string/jumbo v0, "true"

    invoke-virtual {p0}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "x-required-value"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static parsePropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "argType"    # Ljava/lang/Class;
    .param p2, "strValue"    # Ljava/lang/String;
    .param p3, "format"    # Ljava/lang/String;

    .prologue
    .line 136
    move-object v2, p2

    .line 137
    .local v2, "val":Ljava/lang/String;
    const-class v3, Ljava/io/File;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 139
    if-nez p2, :cond_0

    .line 142
    const/4 v2, 0x0

    .line 279
    .end local v2    # "val":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 145
    .restart local v2    # "val":Ljava/lang/String;
    :cond_0
    new-instance v2, Ljava/io/File;

    .end local v2    # "val":Ljava/lang/String;
    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v2, "val":Ljava/io/File;
    goto :goto_0

    .line 147
    .local v2, "val":Ljava/lang/String;
    :cond_1
    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 149
    if-nez p2, :cond_2

    .line 151
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Property is boolean but string value was null:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/mda/util/MDAUtil;->warn(Ljava/lang/String;)V

    .line 152
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .local v2, "val":Ljava/lang/Boolean;
    goto :goto_0

    .line 156
    .local v2, "val":Ljava/lang/String;
    :cond_2
    invoke-static {p2}, Lcom/vlingo/mda/util/MDAUtil;->toBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .local v2, "val":Ljava/lang/Boolean;
    goto :goto_0

    .line 159
    .local v2, "val":Ljava/lang/String;
    :cond_3
    const-class v3, Ljava/lang/Boolean;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 161
    if-nez p2, :cond_4

    .line 163
    const/4 v2, 0x0

    .local v2, "val":Ljava/lang/Object;
    goto :goto_0

    .line 166
    .local v2, "val":Ljava/lang/String;
    :cond_4
    invoke-static {p2}, Lcom/vlingo/mda/util/MDAUtil;->toBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .local v2, "val":Ljava/lang/Boolean;
    goto :goto_0

    .line 168
    .local v2, "val":Ljava/lang/String;
    :cond_5
    const-class v3, Ljava/lang/Integer;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 170
    if-nez p2, :cond_6

    .line 172
    const/4 v2, 0x0

    .local v2, "val":Ljava/lang/Object;
    goto :goto_0

    .line 175
    .local v2, "val":Ljava/lang/String;
    :cond_6
    invoke-static {p2}, Lcom/bzbyte/util/Util;->toInteger(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v2

    .local v2, "val":Ljava/lang/Integer;
    goto :goto_0

    .line 177
    .local v2, "val":Ljava/lang/String;
    :cond_7
    const-class v3, Ljava/lang/Float;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 179
    if-nez p2, :cond_8

    .line 181
    const/4 v2, 0x0

    .local v2, "val":Ljava/lang/Object;
    goto :goto_0

    .line 184
    .local v2, "val":Ljava/lang/String;
    :cond_8
    invoke-static {p2}, Lcom/bzbyte/util/Util;->toFloat(Ljava/lang/Object;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .local v2, "val":Ljava/lang/Float;
    goto :goto_0

    .line 187
    .local v2, "val":Ljava/lang/String;
    :cond_9
    const-class v3, Ljava/lang/Double;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 189
    if-nez p2, :cond_a

    .line 191
    const/4 v2, 0x0

    .local v2, "val":Ljava/lang/Object;
    goto :goto_0

    .line 194
    .local v2, "val":Ljava/lang/String;
    :cond_a
    invoke-static {p2}, Lcom/bzbyte/util/Util;->toDouble(Ljava/lang/Object;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .local v2, "val":Ljava/lang/Double;
    goto/16 :goto_0

    .line 196
    .local v2, "val":Ljava/lang/String;
    :cond_b
    const-class v3, Ljava/lang/Long;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 198
    if-nez p2, :cond_c

    .line 200
    const/4 v2, 0x0

    .local v2, "val":Ljava/lang/Object;
    goto/16 :goto_0

    .line 203
    .local v2, "val":Ljava/lang/String;
    :cond_c
    invoke-static {p2}, Lcom/bzbyte/util/Util;->toLong(Ljava/lang/Object;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .local v2, "val":Ljava/lang/Long;
    goto/16 :goto_0

    .line 205
    .local v2, "val":Ljava/lang/String;
    :cond_d
    const-class v3, Ljava/lang/Short;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 207
    if-nez p2, :cond_e

    .line 209
    const/4 v2, 0x0

    .local v2, "val":Ljava/lang/Object;
    goto/16 :goto_0

    .line 212
    .local v2, "val":Ljava/lang/String;
    :cond_e
    invoke-static {p2}, Lcom/bzbyte/util/Util;->toShort(Ljava/lang/Object;)Ljava/lang/Short;

    move-result-object v2

    .local v2, "val":Ljava/lang/Short;
    goto/16 :goto_0

    .line 214
    .local v2, "val":Ljava/lang/String;
    :cond_f
    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 216
    if-nez p2, :cond_10

    .line 218
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Property is integer but string value was null:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/mda/util/MDAUtil;->warn(Ljava/lang/String;)V

    .line 219
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .local v2, "val":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 222
    .local v2, "val":Ljava/lang/String;
    :cond_10
    invoke-static {p2}, Lcom/bzbyte/util/Util;->toInteger(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v2

    .local v2, "val":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 224
    .local v2, "val":Ljava/lang/String;
    :cond_11
    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 226
    if-nez p2, :cond_12

    .line 228
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Property is float but string value was null:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/mda/util/MDAUtil;->warn(Ljava/lang/String;)V

    .line 229
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .local v2, "val":Ljava/lang/Float;
    goto/16 :goto_0

    .line 232
    .local v2, "val":Ljava/lang/String;
    :cond_12
    invoke-static {p2}, Lcom/bzbyte/util/Util;->toFloat(Ljava/lang/Object;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .local v2, "val":Ljava/lang/Float;
    goto/16 :goto_0

    .line 234
    .local v2, "val":Ljava/lang/String;
    :cond_13
    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 236
    if-nez p2, :cond_14

    .line 238
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Property is double but string value was null:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/mda/util/MDAUtil;->warn(Ljava/lang/String;)V

    .line 239
    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .local v2, "val":Ljava/lang/Double;
    goto/16 :goto_0

    .line 242
    .local v2, "val":Ljava/lang/String;
    :cond_14
    invoke-static {p2}, Lcom/bzbyte/util/Util;->toDouble(Ljava/lang/Object;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .local v2, "val":Ljava/lang/Double;
    goto/16 :goto_0

    .line 244
    .local v2, "val":Ljava/lang/String;
    :cond_15
    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 246
    if-nez p2, :cond_16

    .line 248
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Property is long but string value was null:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/mda/util/MDAUtil;->warn(Ljava/lang/String;)V

    .line 249
    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .local v2, "val":Ljava/lang/Long;
    goto/16 :goto_0

    .line 252
    .local v2, "val":Ljava/lang/String;
    :cond_16
    invoke-static {p2}, Lcom/bzbyte/util/Util;->toLong(Ljava/lang/Object;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .local v2, "val":Ljava/lang/Long;
    goto/16 :goto_0

    .line 254
    .local v2, "val":Ljava/lang/String;
    :cond_17
    const-class v3, Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 257
    invoke-static {p2}, Lcom/bzbyte/util/Util;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 259
    :cond_18
    const-class v3, Ljava/sql/Timestamp;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 262
    if-nez p3, :cond_19

    .line 263
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "Format must be specified when parsing timestamps"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 265
    :cond_19
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 268
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    :try_start_0
    new-instance v2, Ljava/sql/Timestamp;

    .end local v2    # "val":Ljava/lang/String;
    invoke-virtual {v1, p2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/sql/Timestamp;-><init>(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .local v2, "val":Ljava/sql/Timestamp;
    goto/16 :goto_0

    .line 270
    .end local v2    # "val":Ljava/sql/Timestamp;
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Failed to parse date:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " format:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 277
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "sdf":Ljava/text/SimpleDateFormat;
    .local v2, "val":Ljava/lang/String;
    :cond_1a
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Dont know how to convert:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static parsePropertyValue(Ljava/lang/String;Ljava/lang/reflect/Method;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "setter"    # Ljava/lang/reflect/Method;
    .param p2, "strValue"    # Ljava/lang/String;
    .param p3, "format"    # Ljava/lang/String;

    .prologue
    .line 285
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {p0, v0, p2, p3}, Lcom/vlingo/mda/util/MDAUtil;->parsePropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static setPropertyValue(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "it"    # Ljava/lang/Object;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "strValue"    # Ljava/lang/String;

    .prologue
    .line 292
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/vlingo/mda/util/MDAUtil;->getSetterMethod(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 293
    .local v0, "setterMethod":Ljava/lang/reflect/Method;
    if-nez v0, :cond_0

    .line 294
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Did not find setter for:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 296
    :cond_0
    invoke-static {v0}, Lcom/vlingo/mda/util/MDAUtil;->isPublicMethod(Ljava/lang/reflect/Method;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 297
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Setter method not accessible for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "on: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 300
    :cond_1
    invoke-static {p1, v0, p3, p2}, Lcom/vlingo/mda/util/MDAUtil;->parsePropertyValue(Ljava/lang/String;Ljava/lang/reflect/Method;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 302
    .local v1, "val":Ljava/lang/Object;
    invoke-static {p0, p1, v1}, Lcom/bzbyte/util/Util;->setPropertyValue(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 303
    return-void
.end method

.method private static toBoolean(Ljava/lang/String;)Z
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    const-string/jumbo v3, "true"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "yes"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 114
    :cond_0
    :goto_0
    return v1

    .line 107
    :cond_1
    const-string/jumbo v3, "false"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "no"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move v1, v2

    .line 108
    goto :goto_0

    .line 113
    :cond_3
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 114
    .local v0, "val":I
    if-nez v0, :cond_0

    move v1, v2

    goto :goto_0

    .line 116
    .end local v0    # "val":I
    :catch_0
    move-exception v1

    .line 122
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Dont know how to convert value to boolean:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;
    .locals 6
    .param p0, "obj"    # Lcom/vlingo/mda/util/MDAObject;

    .prologue
    .line 392
    if-nez p0, :cond_0

    .line 393
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "obj may not be null"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 396
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 397
    .local v0, "bout":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Lcom/vlingo/mda/util/MDAObjectOutputStream;

    invoke-direct {v2, v0}, Lcom/vlingo/mda/util/MDAObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 398
    .local v2, "mos":Lcom/vlingo/mda/util/MDAObjectOutputStream;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/vlingo/mda/util/MDAObjectOutputStream;->setGenSensitiveProperties(Z)V

    .line 399
    invoke-virtual {v2, p0}, Lcom/vlingo/mda/util/MDAObjectOutputStream;->writeObject(Lcom/vlingo/mda/util/MDAObject;)V

    .line 400
    const-string/jumbo v3, "utf-8"

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 402
    .end local v0    # "bout":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "mos":Lcom/vlingo/mda/util/MDAObjectOutputStream;
    :catch_0
    move-exception v1

    .line 404
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Failed to toString on object:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method private static warn(Ljava/lang/String;)V
    .locals 1
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 99
    sget-object v0, Lcom/vlingo/mda/util/MDAUtil;->ivLogger:Lorg/apache/log4j/Logger;

    invoke-virtual {v0, p0}, Lorg/apache/log4j/Logger;->warn(Ljava/lang/Object;)V

    .line 100
    return-void
.end method
