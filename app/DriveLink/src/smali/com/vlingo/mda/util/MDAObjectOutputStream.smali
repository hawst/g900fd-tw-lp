.class public Lcom/vlingo/mda/util/MDAObjectOutputStream;
.super Ljava/lang/Object;
.source "MDAObjectOutputStream.java"


# instance fields
.field private final ivCodec:Lcom/vlingo/common/message/Codec;

.field private final ivOut:Ljava/io/OutputStream;

.field private final ivTreeUtil:Lcom/vlingo/mda/util/MDAObjectToTree;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 34
    invoke-static {}, Lcom/vlingo/common/message/XMLCodec;->getInstance()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/mda/util/MDAObjectOutputStream;-><init>(Ljava/io/OutputStream;Lcom/vlingo/common/message/Codec;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Lcom/vlingo/common/message/Codec;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "codec"    # Lcom/vlingo/common/message/Codec;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/vlingo/mda/util/MDAObjectToTree;

    invoke-direct {v0}, Lcom/vlingo/mda/util/MDAObjectToTree;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/util/MDAObjectOutputStream;->ivTreeUtil:Lcom/vlingo/mda/util/MDAObjectToTree;

    .line 28
    iput-object p1, p0, Lcom/vlingo/mda/util/MDAObjectOutputStream;->ivOut:Ljava/io/OutputStream;

    .line 29
    iput-object p2, p0, Lcom/vlingo/mda/util/MDAObjectOutputStream;->ivCodec:Lcom/vlingo/common/message/Codec;

    .line 30
    return-void
.end method

.method private saveSerialized(Lcom/vlingo/mda/util/MDAObject;)V
    .locals 3
    .param p1, "obj"    # Lcom/vlingo/mda/util/MDAObject;

    .prologue
    .line 45
    iget-object v1, p0, Lcom/vlingo/mda/util/MDAObjectOutputStream;->ivTreeUtil:Lcom/vlingo/mda/util/MDAObjectToTree;

    invoke-virtual {v1, p1}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertObjectToMNode(Lcom/vlingo/mda/util/MDAObject;)Lcom/vlingo/common/message/MNode;

    move-result-object v0

    .line 47
    .local v0, "outputNode":Lcom/vlingo/common/message/MNode;
    iget-object v1, p0, Lcom/vlingo/mda/util/MDAObjectOutputStream;->ivCodec:Lcom/vlingo/common/message/Codec;

    iget-object v2, p0, Lcom/vlingo/mda/util/MDAObjectOutputStream;->ivOut:Ljava/io/OutputStream;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/common/message/Codec;->encode(Lcom/vlingo/common/message/MNode;Ljava/io/OutputStream;)V

    .line 48
    return-void
.end method


# virtual methods
.method public setGenSensitiveProperties(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/mda/util/MDAObjectOutputStream;->ivTreeUtil:Lcom/vlingo/mda/util/MDAObjectToTree;

    invoke-virtual {v0, p1}, Lcom/vlingo/mda/util/MDAObjectToTree;->setGenSensitiveProperties(Z)V

    .line 53
    return-void
.end method

.method public writeObject(Lcom/vlingo/mda/util/MDAObject;)V
    .locals 0
    .param p1, "obj"    # Lcom/vlingo/mda/util/MDAObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/vlingo/mda/util/MDAObjectOutputStream;->saveSerialized(Lcom/vlingo/mda/util/MDAObject;)V

    .line 41
    return-void
.end method
