.class public Lcom/vlingo/mda/util/MDAObjectToTree;
.super Ljava/lang/Object;
.source "MDAObjectToTree.java"


# static fields
.field private static final ivInst:Lcom/vlingo/mda/util/MDAObjectToTree;


# instance fields
.field private ivGenSensitiveProperties:Z

.field private ivIgnoreXpxml:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/vlingo/mda/util/MDAObjectToTree;

    invoke-direct {v0}, Lcom/vlingo/mda/util/MDAObjectToTree;-><init>()V

    sput-object v0, Lcom/vlingo/mda/util/MDAObjectToTree;->ivInst:Lcom/vlingo/mda/util/MDAObjectToTree;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/mda/util/MDAObjectToTree;->ivGenSensitiveProperties:Z

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/mda/util/MDAObjectToTree;->ivIgnoreXpxml:Z

    .line 34
    return-void
.end method

.method private convertObjectToMNode(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/mda/util/MDAObjectStreamEnv;)Lcom/vlingo/common/message/MNode;
    .locals 9
    .param p1, "obj"    # Lcom/vlingo/mda/util/MDAObject;
    .param p2, "env"    # Lcom/vlingo/mda/util/MDAObjectStreamEnv;

    .prologue
    .line 74
    if-nez p1, :cond_0

    .line 75
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "obj may not be null"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 77
    :cond_0
    invoke-virtual {p2, p1}, Lcom/vlingo/mda/util/MDAObjectStreamEnv;->get(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/Integer;

    move-result-object v3

    .line 78
    .local v3, "id":Ljava/lang/Integer;
    invoke-interface {p1}, Lcom/vlingo/mda/util/MDAObject;->getClassMeta()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v1

    .line 79
    .local v1, "cm":Lcom/vlingo/mda/model/ClassMeta;
    invoke-virtual {v1}, Lcom/vlingo/mda/model/ClassMeta;->getName()Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "clName":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/vlingo/mda/model/ClassMeta;->getCompactName()Ljava/lang/String;

    move-result-object v2

    .line 81
    .local v2, "compactName":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 83
    move-object v0, v2

    .line 85
    :cond_1
    if-nez v3, :cond_2

    .line 87
    invoke-virtual {p2, p1}, Lcom/vlingo/mda/util/MDAObjectStreamEnv;->put(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/Integer;

    move-result-object v3

    .line 89
    new-instance v5, Lcom/vlingo/common/message/MNode;

    invoke-direct {v5, v0}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;)V

    .line 91
    .local v5, "outputNode":Lcom/vlingo/common/message/MNode;
    invoke-direct {p0, p1, v1, v5, p2}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertPropertiesToMNode(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/mda/model/ClassMeta;Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAObjectStreamEnv;)V

    .line 92
    invoke-direct {p0, p1, v1, v5, p2}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertSuperclassPropertiesToMNode(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/mda/model/ClassMeta;Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAObjectStreamEnv;)V

    .line 100
    .end local v5    # "outputNode":Lcom/vlingo/common/message/MNode;
    :goto_0
    return-object v5

    .line 98
    :cond_2
    new-instance v4, Lcom/vlingo/common/message/MNode;

    invoke-direct {v4, v0}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;)V

    .line 99
    .local v4, "node":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v6, "ref"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/common/message/MNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    .line 100
    goto :goto_0
.end method

.method private convertPropertiesToMNode(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/mda/model/ClassMeta;Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAObjectStreamEnv;)V
    .locals 15
    .param p1, "obj"    # Lcom/vlingo/mda/util/MDAObject;
    .param p2, "cm"    # Lcom/vlingo/mda/model/ClassMeta;
    .param p3, "outputNode"    # Lcom/vlingo/common/message/MNode;
    .param p4, "env"    # Lcom/vlingo/mda/util/MDAObjectStreamEnv;

    .prologue
    .line 133
    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/mda/model/ClassMeta;->getPropertyMetas()Ljava/util/HashMap;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/mda/model/PropertyMeta;

    .line 135
    .local v7, "pm":Lcom/vlingo/mda/model/PropertyMeta;
    iget-boolean v12, p0, Lcom/vlingo/mda/util/MDAObjectToTree;->ivGenSensitiveProperties:Z

    if-nez v12, :cond_4

    invoke-static {v7}, Lcom/vlingo/mda/util/MDAUtil;->isSensitive(Lcom/vlingo/mda/model/PropertyMeta;)Z

    move-result v12

    if-eqz v12, :cond_4

    const/4 v5, 0x1

    .line 136
    .local v5, "isSensitive":Z
    :goto_1
    invoke-static {v7}, Lcom/vlingo/mda/util/MDAObjectToTree;->isPersistXML(Lcom/vlingo/mda/model/PropertyMeta;)Z

    move-result v12

    if-nez v12, :cond_1

    iget-boolean v12, p0, Lcom/vlingo/mda/util/MDAObjectToTree;->ivIgnoreXpxml:Z

    if-eqz v12, :cond_0

    .line 138
    :cond_1
    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lcom/bzbyte/util/Util;->getPropertyValue(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    .line 139
    .local v10, "val":Ljava/lang/Object;
    instance-of v12, v7, Lcom/vlingo/mda/model/SimplePropertyMeta;

    if-nez v12, :cond_2

    instance-of v12, v7, Lcom/vlingo/mda/model/IDProperty;

    if-eqz v12, :cond_7

    .line 142
    :cond_2
    if-eqz v10, :cond_0

    .line 143
    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_5

    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v8

    .line 145
    .local v8, "propName":Ljava/lang/String;
    :goto_2
    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getJavaType()Ljava/lang/String;

    move-result-object v12

    const-class v13, Lcom/vlingo/common/message/MNode;

    invoke-virtual {v13}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_6

    .line 148
    invoke-static {v7, v10}, Lcom/vlingo/mda/util/MDAObjectToTree;->javaPropToString(Lcom/vlingo/mda/model/PropertyMeta;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 149
    .local v11, "valStr":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 151
    invoke-static {v11}, Lcom/vlingo/common/util/VUtil;->mask(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 153
    :cond_3
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/common/message/MNode;->getAttributes()Ljava/util/Map;

    move-result-object v12

    invoke-interface {v12, v8, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 135
    .end local v5    # "isSensitive":Z
    .end local v8    # "propName":Ljava/lang/String;
    .end local v10    # "val":Ljava/lang/Object;
    .end local v11    # "valStr":Ljava/lang/String;
    :cond_4
    const/4 v5, 0x0

    goto :goto_1

    .line 143
    .restart local v5    # "isSensitive":Z
    .restart local v10    # "val":Ljava/lang/Object;
    :cond_5
    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    .line 158
    .restart local v8    # "propName":Ljava/lang/String;
    :cond_6
    new-instance v9, Lcom/vlingo/common/message/MNode;

    invoke-direct {v9, v8}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;)V

    .line 159
    .local v9, "propNode":Lcom/vlingo/common/message/MNode;
    check-cast v10, Lcom/vlingo/common/message/MNode;

    .end local v10    # "val":Ljava/lang/Object;
    invoke-virtual {v9, v10}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    .line 160
    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    goto :goto_0

    .line 164
    .end local v8    # "propName":Ljava/lang/String;
    .end local v9    # "propNode":Lcom/vlingo/common/message/MNode;
    .restart local v10    # "val":Ljava/lang/Object;
    :cond_7
    instance-of v12, v7, Lcom/vlingo/mda/model/CollectionPropertyMeta;

    if-eqz v12, :cond_a

    if-nez v5, :cond_a

    move-object v1, v10

    .line 166
    check-cast v1, Ljava/util/Collection;

    .local v1, "col":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/mda/util/MDAObject;>;"
    move-object v12, v7

    .line 168
    check-cast v12, Lcom/vlingo/mda/model/CollectionPropertyMeta;

    invoke-virtual {v12}, Lcom/vlingo/mda/model/CollectionPropertyMeta;->isFlat()Z

    move-result v12

    if-nez v12, :cond_9

    .line 170
    new-instance v2, Lcom/vlingo/common/message/MNode;

    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_8

    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v12

    :goto_3
    invoke-direct {v2, v12}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;)V

    .line 171
    .local v2, "colNode":Lcom/vlingo/common/message/MNode;
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    .line 177
    :goto_4
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/mda/util/MDAObject;

    .line 179
    .local v6, "o":Lcom/vlingo/mda/util/MDAObject;
    move-object/from16 v0, p4

    invoke-direct {p0, v6, v0}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertObjectToMNode(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/mda/util/MDAObjectStreamEnv;)Lcom/vlingo/common/message/MNode;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    goto :goto_5

    .line 170
    .end local v2    # "colNode":Lcom/vlingo/common/message/MNode;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "o":Lcom/vlingo/mda/util/MDAObject;
    :cond_8
    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v12

    goto :goto_3

    .line 175
    :cond_9
    move-object/from16 v2, p3

    .restart local v2    # "colNode":Lcom/vlingo/common/message/MNode;
    goto :goto_4

    .line 182
    .end local v1    # "col":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/mda/util/MDAObject;>;"
    .end local v2    # "colNode":Lcom/vlingo/common/message/MNode;
    :cond_a
    instance-of v12, v7, Lcom/vlingo/mda/model/ReferencePropertyMeta;

    if-eqz v12, :cond_c

    if-nez v5, :cond_c

    .line 184
    if-eqz v10, :cond_0

    .line 186
    new-instance v2, Lcom/vlingo/common/message/MNode;

    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_b

    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getCompactName()Ljava/lang/String;

    move-result-object v12

    :goto_6
    invoke-direct {v2, v12}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;)V

    .line 187
    .restart local v2    # "colNode":Lcom/vlingo/common/message/MNode;
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    .line 189
    check-cast v10, Lcom/vlingo/mda/util/MDAObject;

    .end local v10    # "val":Ljava/lang/Object;
    move-object/from16 v0, p4

    invoke-direct {p0, v10, v0}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertObjectToMNode(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/mda/util/MDAObjectStreamEnv;)Lcom/vlingo/common/message/MNode;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    goto/16 :goto_0

    .line 186
    .end local v2    # "colNode":Lcom/vlingo/common/message/MNode;
    .restart local v10    # "val":Ljava/lang/Object;
    :cond_b
    invoke-virtual {v7}, Lcom/vlingo/mda/model/PropertyMeta;->getName()Ljava/lang/String;

    move-result-object v12

    goto :goto_6

    .line 195
    :cond_c
    new-instance v12, Ljava/lang/RuntimeException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Dont know how to handle property:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 200
    .end local v5    # "isSensitive":Z
    .end local v7    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    .end local v10    # "val":Ljava/lang/Object;
    :cond_d
    return-void
.end method

.method private convertSuperclassPropertiesToMNode(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/mda/model/ClassMeta;Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAObjectStreamEnv;)V
    .locals 1
    .param p1, "obj"    # Lcom/vlingo/mda/util/MDAObject;
    .param p2, "cm"    # Lcom/vlingo/mda/model/ClassMeta;
    .param p3, "outputNode"    # Lcom/vlingo/common/message/MNode;
    .param p4, "env"    # Lcom/vlingo/mda/util/MDAObjectStreamEnv;

    .prologue
    .line 107
    invoke-virtual {p2}, Lcom/vlingo/mda/model/ClassMeta;->getSuperclassMeta()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    .line 108
    .local v0, "superclassMeta":Lcom/vlingo/mda/model/ClassMeta;
    if-eqz v0, :cond_0

    .line 110
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertPropertiesToMNode(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/mda/model/ClassMeta;Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAObjectStreamEnv;)V

    .line 111
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertSuperclassPropertiesToMNode(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/mda/model/ClassMeta;Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAObjectStreamEnv;)V

    .line 113
    :cond_0
    return-void
.end method

.method public static isPersistXML(Lcom/vlingo/mda/model/PropertyMeta;)Z
    .locals 3
    .param p0, "pm"    # Lcom/vlingo/mda/model/PropertyMeta;

    .prologue
    .line 61
    const-string/jumbo v0, "true"

    invoke-virtual {p0}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "xpxml"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static javaPropToString(Lcom/vlingo/mda/model/PropertyMeta;Ljava/lang/Object;)Ljava/lang/String;
    .locals 5
    .param p0, "pm"    # Lcom/vlingo/mda/model/PropertyMeta;
    .param p1, "javaVal"    # Ljava/lang/Object;

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v3

    const-string/jumbo v4, "xformat"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 118
    .local v2, "xformat":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/mda/model/PropertyMeta;->getJavaType()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "java.sql.Timestamp"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 120
    check-cast v1, Ljava/sql/Timestamp;

    .line 121
    .local v1, "ts":Ljava/sql/Timestamp;
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, "smf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 126
    .end local v0    # "smf":Ljava/text/SimpleDateFormat;
    .end local v1    # "ts":Ljava/sql/Timestamp;
    :goto_0
    return-object v3

    :cond_0
    const-class v3, Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/bzbyte/util/Util;->typeConvert(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public convertObjectToMNode(Lcom/vlingo/mda/util/MDAObject;)Lcom/vlingo/common/message/MNode;
    .locals 2
    .param p1, "obj"    # Lcom/vlingo/mda/util/MDAObject;

    .prologue
    .line 67
    new-instance v0, Lcom/vlingo/mda/util/MDAObjectStreamEnv;

    invoke-direct {v0}, Lcom/vlingo/mda/util/MDAObjectStreamEnv;-><init>()V

    .line 68
    .local v0, "env":Lcom/vlingo/mda/util/MDAObjectStreamEnv;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertObjectToMNode(Lcom/vlingo/mda/util/MDAObject;Lcom/vlingo/mda/util/MDAObjectStreamEnv;)Lcom/vlingo/common/message/MNode;

    move-result-object v1

    return-object v1
.end method

.method public setGenSensitiveProperties(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/vlingo/mda/util/MDAObjectToTree;->ivGenSensitiveProperties:Z

    .line 39
    return-void
.end method
