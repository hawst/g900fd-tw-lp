.class public Lcom/vlingo/mda/util/ModelFactory;
.super Ljava/lang/Object;
.source "ModelFactory.java"


# static fields
.field private static final ivGenUtil:Lcom/vlingo/mda/util/GenUtil;

.field private static ivInst:Lcom/vlingo/mda/util/ModelFactory;

.field private static ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private ivClassMetas:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Lcom/vlingo/mda/model/ClassMeta;",
            ">;"
        }
    .end annotation
.end field

.field private ivResourceToModelMeta:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/mda/model/ModelMeta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/vlingo/mda/util/ModelFactory;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/mda/util/ModelFactory;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 21
    new-instance v0, Lcom/vlingo/mda/util/ModelFactory;

    invoke-direct {v0}, Lcom/vlingo/mda/util/ModelFactory;-><init>()V

    sput-object v0, Lcom/vlingo/mda/util/ModelFactory;->ivInst:Lcom/vlingo/mda/util/ModelFactory;

    .line 25
    new-instance v0, Lcom/vlingo/mda/util/GenUtil;

    invoke-direct {v0}, Lcom/vlingo/mda/util/GenUtil;-><init>()V

    sput-object v0, Lcom/vlingo/mda/util/ModelFactory;->ivGenUtil:Lcom/vlingo/mda/util/GenUtil;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/util/ModelFactory;->ivResourceToModelMeta:Ljava/util/HashMap;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/util/ModelFactory;->ivClassMetas:Ljava/util/Map;

    return-void
.end method

.method public static getFactory()Lcom/vlingo/mda/util/ModelFactory;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/vlingo/mda/util/ModelFactory;->ivInst:Lcom/vlingo/mda/util/ModelFactory;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;
    .locals 5
    .param p1, "cls"    # Ljava/lang/Class;
    .param p2, "resource"    # Ljava/lang/String;

    .prologue
    .line 36
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/vlingo/mda/util/ModelFactory;->ivClassMetas:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/mda/model/ClassMeta;

    .line 38
    .local v0, "clm":Lcom/vlingo/mda/model/ClassMeta;
    if-nez v0, :cond_1

    .line 40
    invoke-virtual {p0, p2}, Lcom/vlingo/mda/util/ModelFactory;->getModelFromResource(Ljava/lang/String;)Lcom/vlingo/mda/model/ModelMeta;

    move-result-object v1

    .line 41
    .local v1, "mm":Lcom/vlingo/mda/model/ModelMeta;
    invoke-virtual {v1, p1}, Lcom/vlingo/mda/model/ModelMeta;->findClassMeta(Ljava/lang/Class;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    .line 42
    if-nez v0, :cond_0

    .line 43
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Class meta not found in model resrouce. cls:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " resource:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    .end local v0    # "clm":Lcom/vlingo/mda/model/ClassMeta;
    .end local v1    # "mm":Lcom/vlingo/mda/model/ModelMeta;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 45
    .restart local v0    # "clm":Lcom/vlingo/mda/model/ClassMeta;
    .restart local v1    # "mm":Lcom/vlingo/mda/model/ModelMeta;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/vlingo/mda/util/ModelFactory;->ivClassMetas:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 48
    .end local v1    # "mm":Lcom/vlingo/mda/model/ModelMeta;
    :cond_1
    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized getModelFromResource(Ljava/lang/String;)Lcom/vlingo/mda/model/ModelMeta;
    .locals 6
    .param p1, "resource"    # Ljava/lang/String;

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/vlingo/mda/util/ModelFactory;->ivResourceToModelMeta:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/mda/model/ModelMeta;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    .local v1, "mm":Lcom/vlingo/mda/model/ModelMeta;
    if-nez v1, :cond_1

    .line 58
    :try_start_1
    const-class v3, Lcom/vlingo/mda/model/ModelMeta;

    invoke-virtual {v3, p1}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v2

    .line 59
    .local v2, "url":Ljava/net/URL;
    sget-object v3, Lcom/vlingo/mda/util/ModelFactory;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v3}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/vlingo/mda/util/ModelFactory;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Loading meta data from resource:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " url:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 60
    :cond_0
    sget-object v3, Lcom/vlingo/mda/util/ModelFactory;->ivGenUtil:Lcom/vlingo/mda/util/GenUtil;

    invoke-static {}, Lcom/bzbyte/util/audit/AppAuditLog;->getAuditLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v4

    invoke-static {p1}, Lcom/bzbyte/util/xml/XMLUtil;->parseDocument(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/vlingo/mda/util/GenUtil;->parseDocument(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;)Lcom/vlingo/mda/model/ModelMeta;

    move-result-object v1

    .line 61
    iget-object v3, p0, Lcom/vlingo/mda/util/ModelFactory;->ivResourceToModelMeta:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    .end local v2    # "url":Ljava/net/URL;
    :cond_1
    monitor-exit p0

    return-object v1

    .line 63
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "Failed to get model"

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 53
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "mm":Lcom/vlingo/mda/model/ModelMeta;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
