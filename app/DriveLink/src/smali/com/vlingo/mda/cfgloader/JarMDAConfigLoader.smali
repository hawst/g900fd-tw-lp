.class public Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;
.super Lcom/vlingo/mda/cfgloader/ZipMDAConfigLoader;
.source "JarMDAConfigLoader.java"


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/Class;)V
    .locals 2
    .param p1, "aFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/mda/util/MDAObject;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "configClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/mda/util/MDAObject;>;"
    new-instance v0, Ljava/util/jar/JarFile;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;Z)V

    invoke-direct {p0, v0, p2}, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;-><init>(Ljava/util/jar/JarFile;Ljava/lang/Class;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/util/jar/JarFile;Ljava/lang/Class;)V
    .locals 0
    .param p1, "jarFile"    # Ljava/util/jar/JarFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/jar/JarFile;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/mda/util/MDAObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p2, "configClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/mda/util/MDAObject;>;"
    invoke-direct {p0, p1, p2}, Lcom/vlingo/mda/cfgloader/ZipMDAConfigLoader;-><init>(Ljava/util/zip/ZipFile;Ljava/lang/Class;)V

    .line 25
    return-void
.end method
