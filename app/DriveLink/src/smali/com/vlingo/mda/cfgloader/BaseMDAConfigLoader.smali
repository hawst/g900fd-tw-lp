.class public abstract Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;
.super Ljava/lang/Object;
.source "BaseMDAConfigLoader.java"

# interfaces
.implements Lcom/vlingo/mda/cfgloader/ConfigLoader;


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private final ivConfigClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/mda/util/MDAObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/mda/util/MDAObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "configClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/mda/util/MDAObject;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;->ivConfigClass:Ljava/lang/Class;

    .line 37
    return-void
.end method

.method private getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;->ivConfigClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/mda/util/MDAObject;

    invoke-interface {v1}, Lcom/vlingo/mda/util/MDAObject;->getClassMeta()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/mda/model/ClassMeta;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v2, "Couldn\'t create new instance of config class"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private getVersion(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "svn"    # Ljava/lang/String;

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/vlingo/common/util/VUtil;->getSVNTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    return-void
.end method

.method protected abstract getStream(Ljava/lang/String;)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getVersion()Ljava/lang/String;
    .locals 5

    .prologue
    .line 77
    const/4 v1, 0x0

    .line 78
    .local v1, "input":Ljava/io/InputStream;
    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    .line 79
    .local v2, "properties":Ljava/util/Properties;
    const-string/jumbo v3, "url"

    const-string/jumbo v4, "unknown"

    invoke-virtual {v2, v3, v4}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 80
    const-string/jumbo v3, "svn"

    const-string/jumbo v4, "unknown"

    invoke-virtual {v2, v3, v4}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 83
    :try_start_0
    const-string/jumbo v3, "version.txt"

    invoke-virtual {p0, v3}, Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;->getStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 84
    invoke-virtual {v2, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 85
    const-string/jumbo v3, "url"

    invoke-virtual {v2, v3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "svn"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;->getVersion(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 90
    if-eqz v1, :cond_0

    .line 92
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 95
    :cond_0
    :goto_0
    return-object v3

    .line 86
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v3, Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v4, "Error loading version.txt"

    invoke-virtual {v3, v4, v0}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 88
    const-string/jumbo v3, "unknown"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 90
    if-eqz v1, :cond_0

    .line 92
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 93
    :catch_1
    move-exception v4

    goto :goto_0

    .line 90
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 92
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 95
    :cond_1
    :goto_1
    throw v3

    .line 93
    :catch_2
    move-exception v4

    goto :goto_0

    :catch_3
    move-exception v4

    goto :goto_1
.end method

.method public load(Ljava/lang/String;)Lcom/vlingo/mda/util/MDAObject;
    .locals 5
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;->getStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 57
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Lcom/vlingo/mda/util/MDAObjectInputStream;

    invoke-direct {p0}, Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/vlingo/mda/util/MDAObjectInputStream;-><init>(Ljava/io/InputStream;Lcom/vlingo/mda/model/PackageMeta;)V

    .line 58
    .local v2, "moos":Lcom/vlingo/mda/util/MDAObjectInputStream;
    invoke-virtual {v2}, Lcom/vlingo/mda/util/MDAObjectInputStream;->readObject()Lcom/vlingo/mda/util/MDAObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 66
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v3

    .line 60
    .end local v2    # "moos":Lcom/vlingo/mda/util/MDAObjectInputStream;
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "Failed to read from MDAObjectInputStream"

    invoke-direct {v3, v4, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v3
.end method

.method public matchingPaths(Ljava/lang/String;)Ljava/util/Collection;
    .locals 1
    .param p1, "pathRegex"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
