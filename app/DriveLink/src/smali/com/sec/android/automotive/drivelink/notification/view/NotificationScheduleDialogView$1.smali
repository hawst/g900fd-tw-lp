.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;
.super Ljava/lang/Object;
.source "NotificationScheduleDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    .line 239
    const-string/jumbo v0, "[NotificationScheduleDialogView]"

    const-string/jumbo v1, "Navigate Button Clicked."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->dismissDirect(Z)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    .line 245
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v9

    .line 244
    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    .line 246
    .local v9, "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    if-eqz v9, :cond_0

    .line 247
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 248
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newNavigationMap()Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 252
    const-class v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mContext:Landroid/content/Context;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->hasGoogleAccount(Landroid/content/Context;)Z
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 255
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 256
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 254
    invoke-static {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 296
    .end local v9    # "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :cond_0
    :goto_0
    return-void

    .line 263
    .restart local v9    # "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;D)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    .line 265
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v1

    .line 264
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$4(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;D)V

    .line 266
    const-string/jumbo v0, "[NotificationScheduleDialogView]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Latitude:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 267
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "=>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 268
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLatitude:D
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$5(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", Longitude:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 269
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "=>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 270
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLongitude:D
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$6(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 266
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 273
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 274
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v7

    .line 276
    .local v7, "direction":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    invoke-static {v0, v7}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$7(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;Ljava/lang/String;)V

    .line 281
    .end local v7    # "direction":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 282
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->isOriginNeeded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 283
    const/4 v8, 0x0

    .line 284
    .local v8, "mRequestGeoLocation":Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
    new-instance v8, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    .end local v8    # "mRequestGeoLocation":Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
    invoke-direct {v8}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;-><init>()V

    .line 285
    .restart local v8    # "mRequestGeoLocation":Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    invoke-virtual {v8, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->setResponseListener(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->execute(Landroid/content/Context;)Z

    goto/16 :goto_0

    .line 288
    .end local v8    # "mRequestGeoLocation":Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLatitude:D
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$5(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)D

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLongitude:D
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$6(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)D

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTts:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->access$8(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startNavigation(Landroid/content/Context;DDLjava/lang/String;)V

    goto/16 :goto_0

    .line 291
    :cond_4
    const-string/jumbo v0, "[NotificationScheduleDialogView]"

    const-string/jumbo v1, "mNavigation is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
