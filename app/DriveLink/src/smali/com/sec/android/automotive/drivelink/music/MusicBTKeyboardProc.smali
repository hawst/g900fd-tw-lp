.class public Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;
.super Ljava/lang/Object;
.source "MusicBTKeyboardProc.java"


# instance fields
.field private final INIT_TIMING:I

.field private final MODE_LEFT:I

.field private final MODE_RIGHT:I

.field private final PROC_INTERVAL:I

.field private final SEEK_VALUE:I

.field private final SEEK_VALUE_PAUSED:I

.field private final TAG:Ljava/lang/String;

.field private mode:I

.field private time:J

.field private timePast:J

.field private value:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x3e8

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->MODE_LEFT:I

    .line 8
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->MODE_RIGHT:I

    .line 10
    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->INIT_TIMING:I

    .line 11
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->PROC_INTERVAL:I

    .line 13
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->SEEK_VALUE:I

    .line 14
    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->SEEK_VALUE_PAUSED:I

    .line 16
    const-string/jumbo v0, "BT KeyBoard"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->TAG:Ljava/lang/String;

    .line 27
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->timePast:J

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    .line 29
    return-void
.end method


# virtual methods
.method public proc(Landroid/widget/SeekBar;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "$seekbar"    # Landroid/widget/SeekBar;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v9, 0x16

    const/16 v8, 0x15

    const/4 v7, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 32
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    .line 33
    .local v0, "isPlaying":Z
    if-eq p2, v9, :cond_1

    .line 34
    if-eq p2, v8, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v1

    .line 36
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->time:J

    .line 37
    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->timePast:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-nez v3, :cond_5

    .line 38
    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->time:J

    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->timePast:J

    .line 46
    :cond_2
    :goto_1
    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->time:J

    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->timePast:J

    .line 49
    if-ne p2, v9, :cond_b

    .line 50
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->mode:I

    if-eq v3, v2, :cond_3

    .line 52
    iput v7, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    .line 55
    :cond_3
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    if-ne v3, v7, :cond_4

    .line 56
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    iput v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    .line 57
    :cond_4
    if-eqz v0, :cond_7

    .line 58
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    add-int/lit16 v3, v3, 0x7d0

    iput v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    .line 62
    :goto_2
    iput v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->mode:I

    .line 64
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v4

    if-lt v3, v4, :cond_8

    .line 66
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 39
    :cond_5
    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->time:J

    iget-wide v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->timePast:J

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x12c

    cmp-long v3, v3, v5

    if-gez v3, :cond_6

    if-eqz v0, :cond_6

    move v1, v2

    .line 41
    goto :goto_0

    .line 42
    :cond_6
    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->time:J

    iget-wide v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->timePast:J

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x3e8

    cmp-long v3, v3, v5

    if-lez v3, :cond_2

    .line 43
    iput v7, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    goto :goto_1

    .line 60
    :cond_7
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    add-int/lit16 v3, v3, 0x3e8

    iput v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    goto :goto_2

    .line 70
    :cond_8
    if-eqz v0, :cond_a

    .line 72
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    :cond_9
    :goto_3
    move v1, v2

    .line 83
    goto :goto_0

    .line 75
    :cond_a
    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 76
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v2, :cond_9

    .line 79
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    goto :goto_3

    .line 87
    :cond_b
    if-ne p2, v8, :cond_0

    .line 88
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->mode:I

    if-eqz v3, :cond_c

    .line 90
    iput v7, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    .line 92
    :cond_c
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    if-ne v3, v7, :cond_d

    .line 93
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    iput v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    .line 94
    :cond_d
    if-eqz v0, :cond_e

    .line 95
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    add-int/lit16 v3, v3, -0x7d0

    iput v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    .line 99
    :goto_4
    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->mode:I

    .line 101
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    if-gtz v3, :cond_f

    .line 103
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    goto/16 :goto_0

    .line 97
    :cond_e
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    add-int/lit16 v3, v3, -0x3e8

    iput v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    goto :goto_4

    .line 106
    :cond_f
    if-eqz v0, :cond_11

    .line 108
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    :cond_10
    :goto_5
    move v1, v2

    .line 119
    goto/16 :goto_0

    .line 111
    :cond_11
    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 112
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v2, :cond_10

    .line 115
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->value:I

    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    goto :goto_5
.end method
