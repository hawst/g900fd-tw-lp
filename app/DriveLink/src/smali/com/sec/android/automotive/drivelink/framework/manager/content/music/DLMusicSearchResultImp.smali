.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
.source "DLMusicSearchResultImp.java"


# instance fields
.field private mAlbumList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private mArtistList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;"
        }
    .end annotation
.end field

.field private mMusicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end field

.field private mReplicatedMusicCount:I

.field private mSearchKeyword:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "searchKeyword"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mSearchKeyword:Ljava/lang/String;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mArtistList:Ljava/util/ArrayList;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mAlbumList:Ljava/util/ArrayList;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mMusicList:Ljava/util/ArrayList;

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mReplicatedMusicCount:I

    .line 23
    return-void
.end method


# virtual methods
.method public getAlbumList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mAlbumList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getArtistList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mArtistList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMusicList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mMusicList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getResultItemCount()I
    .locals 7

    .prologue
    const/16 v4, 0x28

    .line 34
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 35
    .local v1, "artistCount":I
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mAlbumList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 36
    .local v0, "albumCount":I
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 38
    .local v2, "musicCount":I
    add-int v5, v1, v0

    add-int v3, v5, v2

    .line 39
    .local v3, "totalListCount":I
    if-nez v3, :cond_1

    .line 40
    const/4 v4, 0x0

    .line 43
    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mAlbumList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    if-gt v5, v4, :cond_0

    .line 44
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mAlbumList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getSearchKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mSearchKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalMusicCount()I
    .locals 3

    .prologue
    .line 48
    const/4 v1, 0x0

    .line 51
    .local v1, "totalMusicCount":I
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 55
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mAlbumList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 59
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 61
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mReplicatedMusicCount:I

    sub-int v2, v1, v2

    return v2

    .line 52
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getSongCount()I

    move-result v2

    add-int/2addr v1, v2

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mAlbumList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;->getSongCount()I

    move-result v2

    add-int/2addr v1, v2

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public increaseReplicatedCount()V
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mReplicatedMusicCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mReplicatedMusicCount:I

    .line 90
    return-void
.end method

.method public setAlbumList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mAlbumList:Ljava/util/ArrayList;

    .line 78
    return-void
.end method

.method public setArtistList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "artistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mArtistList:Ljava/util/ArrayList;

    .line 70
    return-void
.end method

.method public setMusicList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mMusicList:Ljava/util/ArrayList;

    .line 86
    return-void
.end method

.method public setSearchKeyword(Ljava/lang/String;)V
    .locals 0
    .param p1, "searchKeyword"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->mSearchKeyword:Ljava/lang/String;

    .line 27
    return-void
.end method
