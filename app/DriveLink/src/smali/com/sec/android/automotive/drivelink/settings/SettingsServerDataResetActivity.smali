.class public Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SettingsServerDataResetActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;
    }
.end annotation


# static fields
.field private static final DIALOG_PROGRESS:I = 0x0

.field private static final KEY_RESET_MODE:Ljava/lang/String; = "reset_mode"

.field private static final RESET_MODE_ERROR_SHOW:I = 0x3

.field private static final RESET_MODE_IDLE:I = 0x0

.field private static final RESET_MODE_PREPARE:I = 0x1

.field private static final RESET_MODE_RESET_STARTED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "[SettingsServerDataResetActivity]"


# instance fields
.field private mDeleteCallback:Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;

.field private mDescription1:Landroid/widget/LinearLayout;

.field private mDescription2:Landroid/widget/LinearLayout;

.field private mDialog:Landroid/app/AlertDialog;

.field private mErrorMessage:Ljava/lang/String;

.field private mProgressDialog:Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;

.field private mResetButton:Landroid/widget/Button;

.field private mResetClickListener:Landroid/view/View$OnClickListener;

.field private mResetMode:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 38
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription1:Landroid/widget/LinearLayout;

    .line 39
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription2:Landroid/widget/LinearLayout;

    .line 40
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetButton:Landroid/widget/Button;

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    .line 42
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDialog:Landroid/app/AlertDialog;

    .line 43
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mErrorMessage:Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mProgressDialog:Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;

    .line 164
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetClickListener:Landroid/view/View$OnClickListener;

    .line 174
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDeleteCallback:Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;

    .line 29
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;)V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->changeToNextMode()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;I)V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->doAction(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;I)V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->changeResetMode(I)V

    return-void
.end method

.method private changeResetMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 159
    const-string/jumbo v0, "[SettingsServerDataResetActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "changeResetMode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "=>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iput p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    .line 161
    iget v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->initCtrls(I)V

    .line 162
    return-void
.end method

.method private changeToNextMode()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 150
    const-string/jumbo v0, "[SettingsServerDataResetActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "changeToNextMode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    if-nez v0, :cond_1

    .line 152
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->changeResetMode(I)V

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    if-ne v0, v3, :cond_0

    .line 154
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->changeResetMode(I)V

    goto :goto_0
.end method

.method private doAction(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 183
    iget v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 184
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->resetUserData()V

    .line 186
    :cond_0
    return-void
.end method

.method private initCtrls(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 114
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription1:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription2:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetButton:Landroid/widget/Button;

    if-nez v0, :cond_1

    .line 116
    :cond_0
    const-string/jumbo v0, "[SettingsServerDataResetActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "view widget is null:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription1:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 117
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription2:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 116
    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :goto_0
    return-void

    .line 121
    :cond_1
    if-nez p1, :cond_2

    .line 123
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 146
    :goto_1
    const-string/jumbo v0, "[SettingsServerDataResetActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "initCtrls:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 126
    :cond_2
    if-ne p1, v2, :cond_3

    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    goto :goto_1

    .line 131
    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    .line 133
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    goto :goto_1

    .line 136
    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_5

    .line 138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mErrorMessage:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->showErrorDialog(Ljava/lang/String;)V

    goto :goto_1

    .line 143
    :cond_5
    const-string/jumbo v0, "[SettingsServerDataResetActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Invalid mode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private resetUserData()V
    .locals 2

    .prologue
    .line 177
    const-string/jumbo v0, "[SettingsServerDataResetActivity]"

    const-string/jumbo v1, "resetUserData"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDeleteCallback:Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/VlingoAndroidCore;->deleteAllPersonalData(ZLcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;)V

    .line 179
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->showDialog(I)V

    .line 180
    return-void
.end method

.method private showErrorDialog(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 189
    const-string/jumbo v1, "[SettingsServerDataResetActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "showErrorDialog:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .line 191
    const/4 v1, 0x4

    .line 190
    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 193
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a045d

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 194
    const v1, 0x7f0a0389

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 195
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;)V

    .line 194
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 200
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDialog:Landroid/app/AlertDialog;

    .line 201
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 209
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 211
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 106
    const-string/jumbo v0, "[SettingsServerDataResetActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onBackPressed:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onBackPressed()V

    .line 111
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    const-string/jumbo v2, "[SettingsServerDataResetActivity]"

    const-string/jumbo v3, "onCreate"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const v2, 0x7f030032

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->setContentView(I)V

    .line 54
    const v2, 0x7f09014f

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription1:Landroid/widget/LinearLayout;

    .line 55
    const v2, 0x7f090150

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDescription2:Landroid/widget/LinearLayout;

    .line 56
    const v2, 0x7f090151

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetButton:Landroid/widget/Button;

    .line 57
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v2, 0x7f090005

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 59
    check-cast v0, Landroid/widget/LinearLayout;

    .line 61
    .local v0, "btnNavBack":Landroid/widget/LinearLayout;
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    if-eqz p1, :cond_0

    .line 69
    const-string/jumbo v2, "reset_mode"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 74
    .local v1, "resetMode":I
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->changeResetMode(I)V

    .line 77
    .end local v1    # "resetMode":I
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 81
    const-string/jumbo v0, "[SettingsServerDataResetActivity]"

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 87
    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 88
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    if-eqz p1, :cond_0

    .line 93
    const-string/jumbo v0, "reset_mode"

    iget v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mResetMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 101
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 102
    return-void
.end method

.method public removeDialog()V
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->removeDialog(I)V

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->mProgressDialog:Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;

    .line 216
    return-void
.end method
