.class public Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;
.super Ljava/lang/Object;
.source "LocationMessage.java"


# static fields
.field private static final KEY_CONTACT:Ljava/lang/String; = "C"

.field private static final KEY_LAT:Ljava/lang/String; = "lat"

.field private static final KEY_LNG:Ljava/lang/String; = "lng"

.field private static final KEY_ROOT:Ljava/lang/String; = "CAR"

.field private static final KEY_TYPE:Ljava/lang/String; = "T"

.field private static final TYPE_REQUEST:Ljava/lang/String; = "R"

.field private static final TYPE_SHARE:Ljava/lang/String; = "S"


# instance fields
.field private mContactName:Ljava/lang/String;

.field private mLocation:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

.field private mType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mType:Ljava/lang/String;

    .line 23
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mLocation:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 25
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mContactName:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public static parseLocationMessage(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;
    .locals 6
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 107
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;-><init>()V

    .line 109
    .local v1, "locationMsg":Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 110
    .local v2, "msg":Lorg/json/JSONObject;
    const-string/jumbo v4, "CAR"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 112
    .local v3, "msgContent":Lorg/json/JSONObject;
    const-string/jumbo v4, "T"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->setType(Ljava/lang/String;)V

    .line 113
    const-string/jumbo v4, "C"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->setContactName(Ljava/lang/String;)V

    .line 115
    const-string/jumbo v4, "S"

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 116
    const-string/jumbo v4, "lat"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->setLatitude(D)V

    .line 117
    const-string/jumbo v4, "lng"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->setLongitude(D)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    .end local v2    # "msg":Lorg/json/JSONObject;
    .end local v3    # "msgContent":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-object v1

    .line 119
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getContactName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mContactName:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mLocation:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mLocation:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public setContactName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mContactName:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setLatitude(D)V
    .locals 1
    .param p1, "lat"    # D

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mLocation:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->setLatitude(D)V

    .line 50
    return-void
.end method

.method public setLongitude(D)V
    .locals 1
    .param p1, "lng"    # D

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mLocation:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->setLongitude(D)V

    .line 58
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mType:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public toStringRequestMessage()Ljava/lang/String;
    .locals 5

    .prologue
    .line 89
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 93
    .local v1, "msg":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 94
    .local v2, "msgContent":Lorg/json/JSONObject;
    const-string/jumbo v3, "T"

    const-string/jumbo v4, "R"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 95
    const-string/jumbo v3, "C"

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mContactName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 97
    const-string/jumbo v3, "CAR"

    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    .end local v2    # "msgContent":Lorg/json/JSONObject;
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 98
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public toStringShareMessage()Ljava/lang/String;
    .locals 6

    .prologue
    .line 69
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 73
    .local v1, "msg":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 74
    .local v2, "msgContent":Lorg/json/JSONObject;
    const-string/jumbo v3, "T"

    const-string/jumbo v4, "S"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 75
    const-string/jumbo v3, "C"

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mContactName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 76
    const-string/jumbo v3, "lat"

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mLocation:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 77
    const-string/jumbo v3, "lng"

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/message/LocationMessage;->mLocation:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 79
    const-string/jumbo v3, "CAR"

    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    .end local v2    # "msgContent":Lorg/json/JSONObject;
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 80
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
