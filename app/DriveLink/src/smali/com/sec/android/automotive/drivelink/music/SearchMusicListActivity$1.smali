.class Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$1;
.super Ljava/lang/Object;
.source "SearchMusicListActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestAlbumList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 320
    .local p1, "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    return-void
.end method

.method public onResponseRequestArtistList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 326
    .local p1, "artistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    return-void
.end method

.method public onResponseRequestFolderList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 332
    .local p1, "folderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;>;"
    return-void
.end method

.method public onResponseRequestMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 0
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 384
    return-void
.end method

.method public onResponseRequestMusicList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 279
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByAlbum(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V
    .locals 0
    .param p2, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ")V"
        }
    .end annotation

    .prologue
    .line 293
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByArtist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V
    .locals 0
    .param p2, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ")V"
        }
    .end annotation

    .prologue
    .line 300
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByFolder(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V
    .locals 0
    .param p2, "folder"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 307
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByMusic(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 0
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByPlaylist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V
    .locals 0
    .param p2, "playlist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ")V"
        }
    .end annotation

    .prologue
    .line 314
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListFromSearchResult(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V
    .locals 5
    .param p2, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 353
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-nez v1, :cond_1

    .line 372
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 357
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayList(Ljava/util/ArrayList;ZZ)V

    .line 358
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 359
    const-class v2, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 358
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 360
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 363
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    .line 362
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 364
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$0(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V

    .line 365
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$3(Ljava/lang/String;)V

    .line 366
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$4(Z)V

    goto :goto_0
.end method

.method public onResponseRequestPlaylistList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 339
    .local p1, "playlistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;>;"
    return-void
.end method

.method public onResponseRequestSearchAllMusic(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 378
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestSearchMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
    .locals 2
    .param p1, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .prologue
    .line 345
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$0(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$1()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->initSearchResultList(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$2(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V

    .line 347
    return-void
.end method
