.class Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;
.super Ljava/lang/Object;
.source "PrepareDialActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    .line 723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 728
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;I)V

    .line 729
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 730
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumberSize:I
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$18(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I

    move-result v0

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsFromMsgReader:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$19(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->startNumberModePhoneActivity()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$20(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    .line 733
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$21(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;Z)V

    .line 735
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->finish()V

    .line 736
    return-void
.end method
