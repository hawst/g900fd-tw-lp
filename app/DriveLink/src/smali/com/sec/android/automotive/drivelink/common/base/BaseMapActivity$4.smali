.class Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;
.super Ljava/lang/Object;
.source "BaseMapActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setNewMapInstance()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    .line 477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapCreated()V
    .locals 7

    .prologue
    .line 482
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setZoomControlsEnabled(Z)V

    .line 484
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isShareMyLocation:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$14(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 485
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 486
    const v2, 0x7f0a02f8

    .line 485
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$15(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->TIMEOUT_MY_CURRENT_LOCATION:Ljava/lang/Long;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$16()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->requestGeolocation(J)Z

    .line 513
    :cond_0
    :goto_0
    return-void

    .line 491
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 492
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v0

    .line 493
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v2

    .line 492
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords(DD)Z

    move-result v0

    .line 493
    if-nez v0, :cond_3

    .line 495
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->TIMEOUT_MY_CURRENT_LOCATION:Ljava/lang/Long;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$16()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->requestGeolocation(J)Z

    goto :goto_0

    .line 499
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v3

    .line 500
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$7(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->hasAddress(Ljava/lang/String;)Z
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$17(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$7(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Ljava/lang/String;

    move-result-object v5

    .line 499
    :goto_1
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->createMarks(DDLjava/lang/String;)V

    .line 502
    const-string/jumbo v0, "yaah.lee"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "mPoint : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 503
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 502
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 506
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "mPoint : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 507
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 506
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 507
    const/4 v2, 0x1

    .line 504
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 507
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 509
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->loading:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$18(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->loading:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$18(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 500
    :cond_4
    const-string/jumbo v5, ""

    goto/16 :goto_1
.end method
