.class public final enum Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;
.super Ljava/lang/Enum;
.source "MusicPlayerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MusicDataEject"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

.field public static final enum LOCAL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

.field public static final enum PRIVATE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

.field public static final enum SDCARD:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    const-string/jumbo v1, "LOCAL"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->LOCAL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    const-string/jumbo v1, "SDCARD"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->SDCARD:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    const-string/jumbo v1, "PRIVATE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->PRIVATE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    .line 34
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->LOCAL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->SDCARD:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->PRIVATE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
