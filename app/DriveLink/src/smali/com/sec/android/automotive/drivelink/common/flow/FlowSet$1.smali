.class Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;
.super Ljava/util/HashMap;
.source "FlowSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Class",
        "<*>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 39
    const-string/jumbo v0, "DM_MAIN"

    const-class v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const-string/jumbo v0, "DM_DIAL_CONTACT"

    const-class v1, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string/jumbo v0, "DM_DIAL_CONTACT_SEARCH_LIST"

    .line 43
    const-class v1, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 42
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const-string/jumbo v0, "DM_DIAL_TYPE"

    const-class v1, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const-string/jumbo v0, "DM_DIAL"

    const-class v1, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string/jumbo v0, "DM_SMS_CONTACT"

    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string/jumbo v0, "DM_SMS_CONTACT_SEARCH_LIST"

    .line 49
    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 48
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string/jumbo v0, "DM_SMS_TYPE"

    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string/jumbo v0, "DM_SMS_COMPOSE"

    .line 52
    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    .line 51
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string/jumbo v0, "DM_SMS_COMPOSING"

    .line 54
    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    .line 53
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const-string/jumbo v0, "DM_SMS_COMPLETE"

    .line 56
    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    .line 55
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string/jumbo v0, "DM_SMS_SEND"

    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string/jumbo v0, "DM_SMS_INBOX"

    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const-string/jumbo v0, "DM_SMS_INBOX_SEARCH_LIST"

    .line 61
    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 60
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    const-string/jumbo v0, "DM_SMS_READBACK"

    .line 63
    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    .line 62
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-string/jumbo v0, "DM_LOCATION"

    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE"

    .line 67
    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    .line 66
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE_SEARCH_LIST"

    .line 69
    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    .line 68
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE_TYPE"

    .line 71
    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    .line 70
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST"

    .line 74
    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    .line 73
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST_SEARCH_LIST"

    .line 76
    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    .line 75
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST_TYPE"

    .line 78
    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    .line 77
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    const-string/jumbo v0, "DM_LOCATION_SHARE_CONFIRM"

    .line 81
    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;

    .line 80
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    const-string/jumbo v0, "DM_LOCATION_REQ_CONFIRM"

    .line 83
    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;

    .line 82
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    const-string/jumbo v0, "DM_LOCATION_ADDR_TYPE"

    .line 85
    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 84
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    const-string/jumbo v0, "DM_LOCATION_NAV_SEARCH_CONFIRM"

    .line 87
    const-class v1, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    .line 86
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    const-string/jumbo v0, "DM_MUSIC_PLAYER"

    const-class v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    const-string/jumbo v0, "DM_WHAT_CAN_I_SAY"

    .line 103
    const-class v1, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;

    .line 102
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1
    return-void
.end method
