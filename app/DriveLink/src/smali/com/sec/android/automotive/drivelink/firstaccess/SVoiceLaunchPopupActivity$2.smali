.class Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$2;
.super Ljava/lang/Object;
.source "SVoiceLaunchPopupActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 54
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 55
    const-string/jumbo v2, "car_mode_on"

    .line 54
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    .line 55
    const/4 v2, 0x1

    .line 53
    if-ne v1, v2, :cond_0

    .line 57
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 58
    const-string/jumbo v2, "car_mode_on"

    const/4 v3, 0x0

    .line 56
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->finish()V

    .line 63
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 64
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
