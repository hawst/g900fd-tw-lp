.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$3;
.super Ljava/lang/Object;
.source "NotificationMusicView.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->resumeNotiFlow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setBrightMode()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setDayMode()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->access$4(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    .line 192
    return-void
.end method

.method public setDarkMode()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setNightMode()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    .line 186
    return-void
.end method
