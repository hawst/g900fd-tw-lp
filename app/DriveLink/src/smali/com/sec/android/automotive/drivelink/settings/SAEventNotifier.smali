.class public Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;
.super Ljava/lang/Object;
.source "SAEventNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier$SADataNotifier;
    }
.end annotation


# instance fields
.field private callback:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier$SADataNotifier;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier$SADataNotifier;)V
    .locals 0
    .param p1, "event"    # Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier$SADataNotifier;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;->callback:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier$SADataNotifier;

    .line 15
    return-void
.end method


# virtual methods
.method public doNotify(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;->callback:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier$SADataNotifier;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier$SADataNotifier;->notifyDataReady(Landroid/os/Bundle;)V

    .line 20
    return-void
.end method
