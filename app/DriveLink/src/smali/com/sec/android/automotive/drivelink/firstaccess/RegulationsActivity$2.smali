.class Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$2;
.super Ljava/lang/Object;
.source "RegulationsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 131
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->setUserRegulationAgreement(Z)V

    .line 134
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->setTosAccept()V

    .line 136
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 137
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    .line 138
    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 137
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 140
    const-string/jumbo v1, "EXTRA_NAME_FIRST_ACCESS"

    .line 139
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 142
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->startActivity(Landroid/content/Intent;)V

    .line 143
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->finish()V

    .line 144
    return-void
.end method
