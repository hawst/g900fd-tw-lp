.class Lcom/sec/android/automotive/drivelink/notification/NotificationController$3;
.super Ljava/lang/Object;
.source "NotificationController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/NotificationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 228
    const-string/jumbo v0, "[NotificationController]"

    const-string/jumbo v1, "Animation End..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 235
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$5(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v2

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$6(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 243
    const-string/jumbo v0, "[NotificationController]"

    const-string/jumbo v1, "Animation Start..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    return-void
.end method
