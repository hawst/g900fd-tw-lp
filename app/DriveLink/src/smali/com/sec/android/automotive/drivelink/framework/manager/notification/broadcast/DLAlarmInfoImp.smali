.class public Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
.source "DLAlarmInfoImp.java"


# instance fields
.field private mAlarmId:I

.field private mAlarmName:Ljava/lang/String;

.field private mAlertTime:J

.field private mSNZActive:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 12
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;-><init>()V

    .line 7
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmName:Ljava/lang/String;

    .line 8
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlertTime:J

    .line 9
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mSNZActive:Z

    .line 10
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmId:I

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmName:Ljava/lang/String;

    .line 14
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlertTime:J

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JI)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "time"    # J
    .param p4, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmName:Ljava/lang/String;

    .line 8
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlertTime:J

    .line 9
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mSNZActive:Z

    .line 10
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmId:I

    .line 18
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmName:Ljava/lang/String;

    .line 19
    iput-wide p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlertTime:J

    .line 20
    iput p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmId:I

    .line 21
    return-void
.end method


# virtual methods
.method public getAlarmId()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmId:I

    return v0
.end method

.method public getAlarmName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmName:Ljava/lang/String;

    return-object v0
.end method

.method public getAlertTime()J
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlertTime:J

    return-wide v0
.end method

.method public getSNZActive()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mSNZActive:Z

    return v0
.end method

.method public setAlarmId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmId:I

    .line 64
    return-void
.end method

.method public setAlarmName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlarmName:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setAlertTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 34
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mAlertTime:J

    .line 35
    return-void
.end method

.method public setSNZActive(I)V
    .locals 1
    .param p1, "active"    # I

    .prologue
    .line 48
    if-nez p1, :cond_0

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mSNZActive:Z

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->mSNZActive:Z

    goto :goto_0
.end method
