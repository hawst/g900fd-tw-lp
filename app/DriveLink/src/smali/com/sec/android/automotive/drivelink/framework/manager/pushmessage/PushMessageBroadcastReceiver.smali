.class public Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageBroadcastReceiver;
.super Landroid/support/v4/content/WakefulBroadcastReceiver;
.source "PushMessageBroadcastReceiver.java"


# static fields
.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageBroadcastReceiver;

    .line 14
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 13
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/support/v4/content/WakefulBroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onReceive"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    new-instance v0, Landroid/content/ComponentName;

    .line 26
    const-class v1, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;

    .line 25
    invoke-direct {v0, p1, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 27
    .local v0, "comp":Landroid/content/ComponentName;
    invoke-virtual {p2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 28
    invoke-static {p1, p2}, Landroid/support/v4/content/WakefulBroadcastReceiver;->startWakefulService(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 29
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageBroadcastReceiver;->setResultCode(I)V

    .line 30
    return-void
.end method
