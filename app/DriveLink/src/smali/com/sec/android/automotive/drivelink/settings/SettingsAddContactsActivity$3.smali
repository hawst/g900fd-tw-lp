.class Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;
.super Ljava/lang/Object;
.source "SettingsAddContactsActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestFavoriteContactList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/4 v2, 0x0

    .line 164
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$7(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Ljava/util/ArrayList;)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$8(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mSearchOrAllContactsFetched:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$9(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$8(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$10(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->lvFavorites:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$11(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->adapter:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$12(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 171
    :cond_0
    return-void
.end method
