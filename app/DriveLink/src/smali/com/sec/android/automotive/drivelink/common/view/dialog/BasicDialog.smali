.class public Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;
.super Ljava/lang/Object;
.source "BasicDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BasicDialog"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

.field private mBasicDialogListener:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;

.field private mContext:Landroid/content/Context;

.field private mNegativeButtonText:Ljava/lang/String;

.field private mPositiveButtonText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "positiveText"    # I
    .param p3, "negativeText"    # I

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialog:Landroid/app/AlertDialog;

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mContext:Landroid/content/Context;

    .line 17
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mPositiveButtonText:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mNegativeButtonText:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mContext:Landroid/content/Context;

    .line 38
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mPositiveButtonText:Ljava/lang/String;

    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mNegativeButtonText:Ljava/lang/String;

    .line 40
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mContext:Landroid/content/Context;

    .line 41
    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 40
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 43
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setupListeners()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "positiveText"    # Ljava/lang/String;
    .param p3, "negativeText"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialog:Landroid/app/AlertDialog;

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mContext:Landroid/content/Context;

    .line 17
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mPositiveButtonText:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mNegativeButtonText:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mPositiveButtonText:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mNegativeButtonText:Ljava/lang/String;

    .line 30
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mContext:Landroid/content/Context;

    .line 31
    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 33
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setupListeners()V

    .line 34
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;)Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mBasicDialogListener:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;

    return-object v0
.end method

.method private setupListeners()V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mPositiveButtonText:Ljava/lang/String;

    .line 48
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$1;-><init>(Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;)V

    .line 47
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 58
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mNegativeButtonText:Ljava/lang/String;

    .line 59
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$2;-><init>(Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;)V

    .line 58
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 68
    return-void
.end method


# virtual methods
.method public getDialog()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialog:Landroid/app/AlertDialog;

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public setMessage(I)V
    .locals 1
    .param p1, "titleId"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 96
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 92
    return-void
.end method

.method public setOnClickListener(Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;)V
    .locals 0
    .param p1, "basicDialogListener"    # Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mBasicDialogListener:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;

    .line 80
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "titleId"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 88
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->mAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 84
    return-void
.end method
