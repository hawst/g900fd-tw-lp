.class Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$1;
.super Landroid/content/BroadcastReceiver;
.source "LocationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    .line 1523
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    .line 1526
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$1;->getResultCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1543
    :goto_0
    :pswitch_0
    return-void

    .line 1528
    :pswitch_1
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "smsReceiver RESULT_OK"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1531
    :pswitch_2
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "smsReceiver RESULT_ERROR_GENERIC_FAILURE"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1534
    :pswitch_3
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "smsReceiver RESULT_ERROR_NO_SERVICE"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1537
    :pswitch_4
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "smsReceiver RESULT_ERROR_NULL_PDU"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1540
    :pswitch_5
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "smsReceiver RESULT_ERROR_RADIO_OFF"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1526
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
