.class Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;
.super Ljava/lang/Object;
.source "ConnectivityManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

.field private final synthetic val$btDevice:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    .line 609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 612
    const/4 v3, 0x0

    .line 613
    .local v3, "mTotalTimeHFP":I
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mOwnConnectionPattern:Z
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$16(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 614
    :goto_0
    const/16 v4, 0x1388

    if-le v3, v4, :cond_2

    .line 631
    :cond_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->checkContinueConnection(Landroid/bluetooth/BluetoothDevice;I)Z
    invoke-static {v4, v5, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$17(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Landroid/bluetooth/BluetoothDevice;I)Z

    move-result v4

    .line 632
    if-eqz v4, :cond_4

    .line 634
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 635
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothHeadset;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    .line 636
    .local v0, "connectionResult":Z
    if-eqz v0, :cond_3

    .line 637
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$18(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 639
    new-instance v2, Ljava/lang/Thread;

    .line 640
    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {v4, p0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;Landroid/bluetooth/BluetoothDevice;)V

    .line 639
    invoke-direct {v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 690
    .local v2, "mThread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 711
    .end local v0    # "connectionResult":Z
    .end local v2    # "mThread":Ljava/lang/Thread;
    :cond_1
    :goto_1
    return-void

    .line 615
    :cond_2
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 616
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5, v6}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v5

    .line 615
    iput v5, v4, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->result:I

    .line 617
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    iget v4, v4, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->result:I

    if-eq v4, v9, :cond_0

    .line 622
    const-wide/16 v4, 0x1f4

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 623
    add-int/lit16 v3, v3, 0x1f4

    goto :goto_0

    .line 624
    :catch_0
    move-exception v1

    .line 625
    .local v1, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v4

    .line 626
    const-string/jumbo v5, "connectBluetoothDevice"

    .line 625
    invoke-static {v4, v5, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 693
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "connectionResult":Z
    :cond_3
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$18(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 695
    .end local v0    # "connectionResult":Z
    :catch_1
    move-exception v1

    .line 696
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "connectBluetoothDevice"

    invoke-static {v4, v5, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 700
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 701
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5, v6}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v5

    .line 700
    iput v5, v4, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->result:I

    .line 702
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    iget v4, v4, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->result:I

    if-eq v4, v7, :cond_5

    .line 703
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v4

    iget-object v5, v4, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    monitor-enter v5

    .line 704
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->notify()V

    .line 703
    monitor-exit v5

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    .line 706
    :cond_5
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    iget v4, v4, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->result:I

    if-eq v4, v9, :cond_1

    .line 707
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$18(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    goto/16 :goto_1
.end method
