.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;
.super Landroid/os/Handler;
.source "NotificationMessageDialogView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    .line 729
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 734
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;I)V

    .line 735
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSMode:I
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 757
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 758
    return-void

    .line 738
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isMultiMode:Z

    if-eqz v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;

    .line 740
    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;->setTTSEnable(Z)V

    goto :goto_0

    .line 742
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;

    .line 743
    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->setTTSEnable(Z)V

    goto :goto_0

    .line 747
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isMultiMode:Z

    if-eqz v0, :cond_1

    .line 748
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;

    .line 749
    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;->setTTSEnable(Z)V

    goto :goto_0

    .line 751
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;

    .line 752
    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->setTTSEnable(Z)V

    goto :goto_0

    .line 735
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
