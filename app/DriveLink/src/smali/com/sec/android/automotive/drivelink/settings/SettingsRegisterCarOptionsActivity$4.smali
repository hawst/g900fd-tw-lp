.class Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;
.super Ljava/lang/Object;
.source "SettingsRegisterCarOptionsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->showRenameDialog(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

.field private final synthetic val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;->val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 249
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Landroid/widget/EditText;

    move-result-object v1

    .line 250
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 251
    .local v0, "newDeviceName":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    move-result-object v1

    .line 252
    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->setBtName(Ljava/lang/String;)V

    .line 255
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 256
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    move-result-object v2

    .line 254
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->renameRegisteredCarsPreference(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)V

    .line 257
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;->val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 259
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Landroid/widget/EditText;

    move-result-object v2

    .line 260
    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    .line 258
    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 261
    return-void
.end method
