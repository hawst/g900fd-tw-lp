.class Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;
.super Ljava/lang/Object;
.source "SearchMusicListActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getMusicSipStateListener()Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$music$EditTextForMusicSIP$MusicSIPState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$music$EditTextForMusicSIP$MusicSIPState()[I
    .locals 3

    .prologue
    .line 540
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$music$EditTextForMusicSIP$MusicSIPState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->values()[Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$music$EditTextForMusicSIP$MusicSIPState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    .line 540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnStateChanged(Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;)V
    .locals 4
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 544
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$14(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;)V

    .line 546
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$music$EditTextForMusicSIP$MusicSIPState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 570
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 555
    :pswitch_1
    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$1()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    move-result-object v0

    if-eqz v0, :cond_1

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$1()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    move-result-object v0

    .line 556
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getTotalMusicCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$5(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    move-result v0

    if-nez v0, :cond_0

    .line 558
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$5(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 559
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$6(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 558
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 560
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$4(Z)V

    .line 561
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$0(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V

    .line 562
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$3(Ljava/lang/String;)V

    .line 563
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->finish()V

    goto :goto_0

    .line 546
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
