.class Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "ApplicationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageDeleteObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageDeleted(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 421
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onPackageDeleted:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageDeleted;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageDeleted;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 429
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    iput-object p1, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 430
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    iput p2, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->returncode:I

    .line 431
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->deleteHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 432
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->deleteHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 434
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method
