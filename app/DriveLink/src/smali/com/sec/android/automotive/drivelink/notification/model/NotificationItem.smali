.class public Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.super Ljava/lang/Object;
.source "NotificationItem.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;,
        Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowBarginCmd;,
        Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;,
        Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;,
        Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowSoundCmd;,
        Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;,
        Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowTTSFlowCmd;,
        Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;,
        Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;,
        Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field private static ACTION_TIMEOUT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "[NotificationItem]"


# instance fields
.field private appeared:Z

.field bListenStarted:Z

.field private backupCmd:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

.field private backupCmdQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;",
            ">;"
        }
    .end annotation
.end field

.field bargeInTimeoutHandler:Landroid/os/Handler;

.field private cmdQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;",
            ">;"
        }
    .end annotation
.end field

.field cmdTimeoutHandler:Landroid/os/Handler;

.field context:Landroid/content/Context;

.field private flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

.field mCurrentListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;

.field private mPool:Landroid/media/SoundPool;

.field mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

.field mRecognizerListener:Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;

.field private mRingtoneListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

.field private mSoundAFListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field mSoundID:I

.field mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field nextCmdHandler:Landroid/os/Handler;

.field notiFlowListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;

.field private notiInfo:Ljava/lang/Object;

.field private notiType:I

.field private priority:I

.field ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

.field soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

.field timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ACTION_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(IILjava/lang/Object;Landroid/content/Context;)V
    .locals 3
    .param p1, "notiType"    # I
    .param p2, "priority"    # I
    .param p3, "notiInfo"    # Ljava/lang/Object;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MAX:I

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->priority:I

    .line 54
    iput v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiType:I

    .line 58
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->IDLE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 60
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->appeared:Z

    .line 67
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    .line 68
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    .line 69
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    .line 70
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->context:Landroid/content/Context;

    .line 71
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    .line 73
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmd:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 172
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiFlowListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;

    .line 603
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 671
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mSoundAFListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 722
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRingtoneListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    .line 784
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;

    .line 785
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mSoundID:I

    .line 892
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$3;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdTimeoutHandler:Landroid/os/Handler;

    .line 909
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$4;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    .line 922
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$5;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bargeInTimeoutHandler:Landroid/os/Handler;

    .line 985
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bListenStarted:Z

    .line 989
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 1071
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mCurrentListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;

    .line 1073
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizerListener:Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;

    .line 81
    iput p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiType:I

    .line 82
    iput p2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->priority:I

    .line 83
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiInfo:Ljava/lang/Object;

    .line 84
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->context:Landroid/content/Context;

    .line 85
    return-void
.end method

.method private abandonAudioFocus()Z
    .locals 6

    .prologue
    .line 703
    const/4 v1, 0x0

    .line 704
    .local v1, "isSuccess":Z
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->context:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 705
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 706
    const-string/jumbo v4, "audio"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 705
    check-cast v0, Landroid/media/AudioManager;

    .line 707
    .local v0, "am":Landroid/media/AudioManager;
    if-eqz v0, :cond_0

    .line 708
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mSoundAFListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v2

    .line 709
    .local v2, "result":I
    const-string/jumbo v3, "[NotificationItem]"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Noti Alert Sound stop!! abandon audio focus: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 710
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 709
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 712
    const/4 v1, 0x1

    .line 716
    .end local v0    # "am":Landroid/media/AudioManager;
    .end local v2    # "result":I
    :cond_0
    return v1
.end method

.method static synthetic access$0()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ACTION_TIMEOUT:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V
    .locals 0

    .prologue
    .line 436
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z
    .locals 1

    .prologue
    .line 681
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->requestAudioFocus()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z
    .locals 1

    .prologue
    .line 702
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->abandonAudioFocus()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;
    .locals 1

    .prologue
    .line 784
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Landroid/media/SoundPool;)V
    .locals 0

    .prologue
    .line 784
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRingtoneListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    return-object v0
.end method

.method private clearAlert()V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    if-eqz v0, :cond_1

    .line 445
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->isDaemon()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 446
    :cond_0
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "Interrupt Alert Thread"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->interrupt()V

    .line 449
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    .line 450
    return-void
.end method

.method private clearSound()V
    .locals 2

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    if-eqz v0, :cond_1

    .line 454
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->isDaemon()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455
    :cond_0
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "Interrupt Sound Thread"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->interrupt()V

    .line 458
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    .line 459
    return-void
.end method

.method private clearTimeout()V
    .locals 2

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    if-eqz v0, :cond_1

    .line 463
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->isDaemon()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 464
    :cond_0
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "Interrupt Timeout Thread"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->interrupt()V

    .line 467
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    .line 468
    return-void
.end method

.method private doActionCommand(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V
    .locals 4
    .param p1, "cmd"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .prologue
    const/4 v3, 0x1

    .line 471
    if-nez p1, :cond_1

    .line 518
    :cond_0
    :goto_0
    return-void

    .line 475
    :cond_1
    const-string/jumbo v0, "[NotificationItem]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "DO CMD ACTION: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmd:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 479
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 515
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiFlowListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiFlowListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdType()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;->onNotiCommandFinished(I)V

    goto :goto_0

    .line 481
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doBegin()V

    goto :goto_1

    .line 484
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doAlertSound()V

    goto :goto_1

    .line 487
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doSound(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V

    goto :goto_1

    .line 490
    :pswitch_3
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 491
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doTTS(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V

    goto :goto_1

    .line 494
    :pswitch_4
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 495
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doTTSWithListen(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V

    goto :goto_1

    .line 498
    :pswitch_5
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 499
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doTTSWithFlowId(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V

    goto :goto_1

    .line 502
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doBargeIn(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V

    goto :goto_1

    .line 505
    :pswitch_7
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doListen()V

    goto :goto_1

    .line 508
    :pswitch_8
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doTimeout(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V

    goto :goto_1

    .line 511
    :pswitch_9
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doFinish()V

    goto :goto_1

    .line 479
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private doAlertSound()V
    .locals 2

    .prologue
    .line 530
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "doAlertSound"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->SOUND:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 532
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->clearAlert()V

    .line 533
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    .line 534
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    const-string/jumbo v1, "NotiRingtoneThread"

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->setName(Ljava/lang/String;)V

    .line 535
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRingtoneListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->setOnSoundFinishedListener(Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;)V

    .line 536
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ringtoneThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->start()V

    .line 537
    return-void
.end method

.method private doBargeIn(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V
    .locals 6
    .param p1, "cmd"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .prologue
    .line 606
    const-string/jumbo v3, "[NotificationItem]"

    const-string/jumbo v4, "doBargeIn"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotificationReadout()Z

    move-result v3

    if-nez v3, :cond_0

    .line 608
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V

    .line 636
    :goto_0
    return-void

    .line 611
    :cond_0
    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->LISTENING:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 612
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowBarginCmd;

    .line 613
    .local v0, "command":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowBarginCmd;
    if-eqz v0, :cond_1

    const-class v3, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowBarginCmd;

    invoke-virtual {v3, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 614
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V

    goto :goto_0

    .line 617
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getDialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 618
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-nez v3, :cond_3

    .line 619
    new-instance v3, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-direct {v3}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;-><init>()V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 620
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizerListener:Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->InitBargeInRecognizer(Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;)V

    .line 623
    :cond_3
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v3}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->isBargeInEnabled()Z

    move-result v2

    .line 624
    .local v2, "use":Z
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v3}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getState()I

    move-result v1

    .line 625
    .local v1, "state":I
    const-string/jumbo v3, "BARGE IN"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "USE:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", STATE:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowBarginCmd;->listener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mCurrentListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;

    .line 628
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    iget v4, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowBarginCmd;->cmdType:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->startBargeIn(I)V

    .line 629
    new-instance v3, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;)V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    .line 630
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    const-string/jumbo v4, "TimeoutThreadForBargeIn"

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->setName(Ljava/lang/String;)V

    .line 631
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bargeInTimeoutHandler:Landroid/os/Handler;

    iput-object v4, v3, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->timeoutHandler:Landroid/os/Handler;

    .line 632
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->timeoutSec:Ljava/lang/Integer;

    .line 633
    const-string/jumbo v3, "[NotificationItem]"

    const-string/jumbo v4, "Start Barge in Timeout"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->start()V

    goto/16 :goto_0
.end method

.method private doBegin()V
    .locals 2

    .prologue
    .line 521
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "doBegin"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->IDLE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 523
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->clearAlert()V

    .line 524
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->clearTimeout()V

    .line 525
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getDialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 526
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V

    .line 527
    return-void
.end method

.method private doCmd()V
    .locals 1

    .prologue
    .line 437
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getCommand()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    move-result-object v0

    .line 438
    .local v0, "cmd":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;
    if-eqz v0, :cond_0

    .line 439
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doActionCommand(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V

    .line 441
    :cond_0
    return-void
.end method

.method private doFinish()V
    .locals 2

    .prologue
    .line 667
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "doFinish"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->FINISH:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 669
    return-void
.end method

.method private doListen()V
    .locals 2

    .prologue
    .line 639
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "doListen"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotificationReadout()Z

    move-result v0

    if-nez v0, :cond_1

    .line 641
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V

    .line 652
    :cond_0
    :goto_0
    return-void

    .line 645
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->LISTENING:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 646
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bListenStarted:Z

    .line 647
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "dm flow start"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v0

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    .line 649
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getDialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    goto :goto_0
.end method

.method private doSound(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V
    .locals 3
    .param p1, "cmd"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .prologue
    .line 540
    const-string/jumbo v1, "[NotificationItem]"

    const-string/jumbo v2, "doSound"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->SOUND:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 542
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowSoundCmd;

    .line 543
    .local v0, "command":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowSoundCmd;
    if-eqz v0, :cond_0

    const-class v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowSoundCmd;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 544
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V

    .line 550
    :goto_0
    return-void

    .line 548
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->clearSound()V

    .line 549
    iget v1, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowSoundCmd;->resourceId:I

    iget v2, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowSoundCmd;->hundredmillis:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->playSound(II)V

    goto :goto_0
.end method

.method private doTTS(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V
    .locals 3
    .param p1, "cmd"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .prologue
    .line 553
    const-string/jumbo v1, "[NotificationItem]"

    const-string/jumbo v2, "doTTS"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotificationReadout()Z

    move-result v1

    if-nez v1, :cond_0

    .line 555
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V

    .line 564
    :goto_0
    return-void

    .line 558
    :cond_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->TTS:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 559
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 561
    .local v0, "tts":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 562
    const-string/jumbo v1, "[NotificationItem]"

    const-string/jumbo v2, "dm flow start"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getDialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doTTSWithFlowId(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V
    .locals 4
    .param p1, "cmd"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .prologue
    .line 585
    const-string/jumbo v2, "[NotificationItem]"

    const-string/jumbo v3, "doTTSWithFlowId"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotificationReadout()Z

    move-result v2

    if-nez v2, :cond_1

    .line 587
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V

    .line 601
    :cond_0
    :goto_0
    return-void

    .line 590
    :cond_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->LISTENING:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 591
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowTTSFlowCmd;

    .line 592
    .local v1, "ttsFlow":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowTTSFlowCmd;
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bListenStarted:Z

    .line 594
    const-string/jumbo v2, "[NotificationItem]"

    const-string/jumbo v3, "dm flow start"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v2

    sget-object v3, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v2, v3, :cond_0

    .line 596
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 597
    .local v0, "param":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iget-object v2, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowTTSFlowCmd;->tts:Ljava/lang/String;

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 598
    iget-object v2, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowTTSFlowCmd;->flowId:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0
.end method

.method private doTTSWithListen(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V
    .locals 3
    .param p1, "cmd"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .prologue
    .line 572
    const-string/jumbo v1, "[NotificationItem]"

    const-string/jumbo v2, "doTTSWithListen"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotificationReadout()Z

    move-result v1

    if-nez v1, :cond_0

    .line 574
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V

    .line 582
    :goto_0
    return-void

    .line 577
    :cond_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->LISTENING:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 578
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 579
    .local v0, "tts":Ljava/lang/String;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bListenStarted:Z

    .line 580
    const-string/jumbo v1, "[NotificationItem]"

    const-string/jumbo v2, "dm flow start"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getDialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doTimeout(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V
    .locals 2
    .param p1, "cmd"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .prologue
    .line 655
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "doTimeout"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->clearTimeout()V

    .line 657
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    .line 658
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    const-string/jumbo v1, "NotiTimeoutThread"

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->setName(Ljava/lang/String;)V

    .line 659
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdTimeoutHandler:Landroid/os/Handler;

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->timeoutHandler:Landroid/os/Handler;

    .line 660
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->timeoutSec:Ljava/lang/Integer;

    .line 661
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->timeoutThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->start()V

    .line 662
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->TIMEOUT:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 664
    return-void
.end method

.method private getCommand()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    :cond_0
    const/4 v0, 0x0

    .line 433
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    goto :goto_0
.end method

.method private getDialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;
    .locals 1

    .prologue
    .line 164
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    return-object v0
.end method

.method private requestAudioFocus()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 682
    const/4 v1, 0x0

    .line 683
    .local v1, "isSuccess":Z
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->context:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 684
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 685
    const-string/jumbo v4, "audio"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 684
    check-cast v0, Landroid/media/AudioManager;

    .line 686
    .local v0, "am":Landroid/media/AudioManager;
    if-eqz v0, :cond_0

    .line 687
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mSoundAFListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 688
    const/4 v4, 0x3

    .line 689
    const/4 v5, 0x2

    .line 687
    invoke-virtual {v0, v3, v4, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v2

    .line 690
    .local v2, "result":I
    const-string/jumbo v4, "[NotificationItem]"

    .line 691
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "result of requesting Audio Focus is "

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 692
    if-ne v2, v6, :cond_1

    const-string/jumbo v3, "granted"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 691
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 690
    invoke-static {v4, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    if-ne v2, v6, :cond_0

    .line 695
    const/4 v1, 0x1

    .line 699
    .end local v0    # "am":Landroid/media/AudioManager;
    .end local v2    # "result":I
    :cond_0
    return v1

    .line 693
    .restart local v0    # "am":Landroid/media/AudioManager;
    .restart local v2    # "result":I
    :cond_1
    const-string/jumbo v3, "failed"

    goto :goto_0
.end method


# virtual methods
.method public addAlert()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 284
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "addAlert"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 286
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    .line 287
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    return-void
.end method

.method public addBargeIn(ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;)V
    .locals 4
    .param p1, "commandType"    # I
    .param p2, "listener"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;

    .prologue
    const/4 v3, 0x6

    .line 332
    const-string/jumbo v1, "[NotificationItem]"

    const-string/jumbo v2, "addBargeIn"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowBarginCmd;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowBarginCmd;-><init>(ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;)V

    .line 334
    .local v0, "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowBarginCmd;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    new-instance v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 335
    invoke-direct {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    .line 334
    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 336
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 337
    invoke-direct {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    .line 336
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    return-void
.end method

.method public addBegin()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 276
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "addBegin"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 278
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 279
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    .line 280
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    return-void
.end method

.method public addFinish()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x9

    .line 356
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "addFinish"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 358
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    .line 359
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    return-void
.end method

.method public addListen()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x7

    .line 341
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "addListen"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 343
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    .line 344
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    return-void
.end method

.method public addSound(II)V
    .locals 4
    .param p1, "resourceId"    # I
    .param p2, "hundredMillis"    # I

    .prologue
    const/4 v3, 0x2

    .line 297
    const-string/jumbo v1, "[NotificationItem]"

    const-string/jumbo v2, "addSound"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowSoundCmd;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowSoundCmd;-><init>(II)V

    .line 300
    .local v0, "soundCmd":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowSoundCmd;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    new-instance v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 301
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 302
    invoke-direct {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    .line 301
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    return-void
.end method

.method public addTTS(Ljava/lang/String;)V
    .locals 3
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x3

    .line 306
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "addTTS"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v2, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 308
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v2, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    return-void
.end method

.method public addTTSWithFlowId(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "tts"    # Ljava/lang/String;
    .param p2, "flowId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x5

    .line 323
    const-string/jumbo v1, "[NotificationItem]"

    const-string/jumbo v2, "addTTSWithFlowId"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowTTSFlowCmd;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowTTSFlowCmd;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    .local v0, "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowTTSFlowCmd;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    new-instance v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 326
    invoke-direct {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    .line 325
    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 327
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 328
    invoke-direct {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    .line 327
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    return-void
.end method

.method public addTTSWithListen(Ljava/lang/String;)V
    .locals 3
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x4

    .line 315
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "addTTSWithListen"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 317
    invoke-direct {v1, v2, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    .line 316
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 318
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 319
    invoke-direct {v1, v2, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    .line 318
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    return-void
.end method

.method public addTimeout(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "timeoutSec"    # Ljava/lang/Integer;

    .prologue
    const/16 v3, 0x8

    .line 348
    const-string/jumbo v0, "[NotificationItem]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "addTimeout:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 350
    invoke-direct {v1, v3, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    .line 349
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 351
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 352
    invoke-direct {v1, v3, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(ILjava/lang/Object;)V

    .line 351
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    return-void
.end method

.method public cancelNotiFlow()V
    .locals 3

    .prologue
    .line 363
    const-string/jumbo v0, "[NotificationItem]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "cancelNotiFlow:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bListenStarted:Z

    .line 365
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 370
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->IDLE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_3

    .line 390
    :cond_2
    :goto_0
    return-void

    .line 377
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->SOUND:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_4

    .line 378
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->clearAlert()V

    .line 379
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->clearSound()V

    goto :goto_0

    .line 380
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->TTS:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-eq v0, v1, :cond_5

    .line 381
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->LISTENING:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_6

    .line 383
    :cond_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 384
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getDialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelAudio()V

    .line 385
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getDialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    goto :goto_0

    .line 386
    :cond_6
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->TIMEOUT:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_2

    .line 387
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->clearTimeout()V

    goto :goto_0
.end method

.method public cmdFlowStart()V
    .locals 2

    .prologue
    .line 424
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "cmdFlowStart"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V

    .line 426
    return-void
.end method

.method public getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    return-object v0
.end method

.method public getNotiInfo()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiInfo:Ljava/lang/Object;

    return-object v0
.end method

.method public getNotiType()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiType:I

    return v0
.end method

.method public getNotificationReadout()Z
    .locals 1

    .prologue
    .line 1115
    const/4 v0, 0x1

    return v0
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->priority:I

    return v0
.end method

.method public isAppeared()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->appeared:Z

    return v0
.end method

.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v3, 0x0

    .line 1039
    const-string/jumbo v0, "[NotificationItem]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "MicState:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", bListenStarted:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1040
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bListenStarted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1039
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1041
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1069
    :cond_0
    :goto_0
    return-void

    .line 1043
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->LISTENING:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bListenStarted:Z

    if-eqz v0, :cond_0

    .line 1044
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getDialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1049
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bListenStarted:Z

    .line 1050
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "<<<LISTEN/THINK END"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    const-string/jumbo v0, "[NotificationItem]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Mic state go to idle from think. start next. : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1052
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1051
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1053
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1054
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "call nextCmdHandler <= onMicStateChanged"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1055
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1060
    :pswitch_1
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, ">>>LISTENING"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->bListenStarted:Z

    goto :goto_0

    .line 1064
    :pswitch_2
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, ">>>THINKING"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1041
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public pauseNotiFlow()V
    .locals 3

    .prologue
    .line 410
    const-string/jumbo v0, "[NotificationItem]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "pauseNotiFlow: NotiType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->PAUSE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 412
    return-void
.end method

.method public pauseToRestartNotiFlow()V
    .locals 2

    .prologue
    .line 393
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "pauseToRestartNotiFlow"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->RESTART:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 395
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 396
    return-void
.end method

.method playSound(II)V
    .locals 5
    .param p1, "resourceId"    # I
    .param p2, "hundredmillis"    # I

    .prologue
    const/4 v4, 0x1

    .line 791
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    if-eqz v1, :cond_0

    .line 793
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->interrupt()V

    .line 794
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 800
    :cond_0
    :goto_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    .line 801
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    const-string/jumbo v2, "NotiSoundThread"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->setName(Ljava/lang/String;)V

    .line 802
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    iput p2, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->hundredmillis:I

    .line 803
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;

    if-nez v1, :cond_1

    .line 804
    new-instance v1, Landroid/media/SoundPool;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-direct {v1, v4, v2, v3}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;

    .line 806
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->context:Landroid/content/Context;

    invoke-virtual {v1, v2, p1, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mSoundID:I

    .line 807
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;

    new-instance v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    invoke-virtual {v1, v2}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 827
    return-void

    .line 795
    :catch_0
    move-exception v0

    .line 797
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public restartNotiFlow()V
    .locals 4

    .prologue
    .line 399
    const-string/jumbo v2, "[NotificationItem]"

    const-string/jumbo v3, "restartNotiFlow"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmdQueue:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 404
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->IDLE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 406
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 407
    return-void

    .line 400
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .line 401
    .local v0, "backup":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {v1, v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;-><init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V

    .line 402
    .local v1, "cmd":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdQueue:Ljava/util/Queue;

    invoke-interface {v3, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public resumeNotiFlow()V
    .locals 3

    .prologue
    .line 415
    const-string/jumbo v0, "[NotificationItem]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "resumeNotiFlow: NotiType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", Cmd:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 416
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmd:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 415
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmd:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    if-eqz v0, :cond_0

    .line 418
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "resume: do Backup Cmd"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->backupCmd:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doActionCommand(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V

    .line 421
    :cond_0
    return-void
.end method

.method public setAppeared(Z)V
    .locals 0
    .param p1, "appeared"    # Z

    .prologue
    .line 159
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->appeared:Z

    .line 160
    return-void
.end method

.method public setFlowState(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;)V
    .locals 0
    .param p1, "voiceState"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->flowState:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 145
    return-void
.end method

.method public setNotiFlowListener(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiFlowListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;

    .line 176
    return-void
.end method

.method public setNotiInfo(Ljava/lang/Object;)V
    .locals 0
    .param p1, "notiInfo"    # Ljava/lang/Object;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiInfo:Ljava/lang/Object;

    .line 100
    return-void
.end method

.method public setNotiType(I)V
    .locals 0
    .param p1, "notiType"    # I

    .prologue
    .line 129
    iput p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->notiType:I

    .line 130
    return-void
.end method

.method public setNotificationReadout(Z)V
    .locals 0
    .param p1, "isReadout"    # Z

    .prologue
    .line 1121
    return-void
.end method

.method public setPriority(I)V
    .locals 0
    .param p1, "priority"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->priority:I

    .line 115
    return-void
.end method
