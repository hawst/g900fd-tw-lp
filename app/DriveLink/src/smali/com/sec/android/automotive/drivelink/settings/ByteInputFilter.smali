.class public Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;
.super Ljava/lang/Object;
.source "ByteInputFilter.java"

# interfaces
.implements Landroid/text/InputFilter;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private activity:Landroid/app/Activity;

.field private alertDialog:Landroid/app/AlertDialog;

.field private encoding:Ljava/lang/String;

.field private isToastShowing:Z

.field protected maxByte:I

.field private toastMsgResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "maxbyte"    # I

    .prologue
    .line 43
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;-><init>(Landroid/app/Activity;II)V

    .line 44
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Landroid/app/AlertDialog;)V
    .locals 6
    .param p1, "maxbyte"    # I
    .param p2, "encoding"    # Ljava/lang/String;
    .param p3, "alertDialog"    # Landroid/app/AlertDialog;

    .prologue
    .line 70
    const/4 v1, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;-><init>(Landroid/app/Activity;ILjava/lang/String;ILandroid/app/AlertDialog;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;II)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "maxbyte"    # I
    .param p3, "toastMsgResourceId"    # I

    .prologue
    .line 59
    const-string/jumbo v3, "UTF-8"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;-><init>(Landroid/app/Activity;ILjava/lang/String;ILandroid/app/AlertDialog;)V

    .line 60
    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;ILjava/lang/String;ILandroid/app/AlertDialog;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "maxbyte"    # I
    .param p3, "encoding"    # Ljava/lang/String;
    .param p4, "toastMsgResourceId"    # I
    .param p5, "alertDialog"    # Landroid/app/AlertDialog;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->activity:Landroid/app/Activity;

    .line 84
    iput p2, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->maxByte:I

    .line 85
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->encoding:Ljava/lang/String;

    .line 86
    iput p4, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->toastMsgResourceId:I

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->isToastShowing:Z

    .line 88
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->alertDialog:Landroid/app/AlertDialog;

    .line 89
    return-void
.end method

.method private getByteLength(Ljava/lang/String;)I
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 206
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->encoding:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 207
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    array-length v1, v1

    .line 213
    :goto_0
    return v1

    .line 210
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->encoding:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "encoding error for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->encoding:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 213
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_0
.end method

.method private getToastString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->toastMsgResourceId:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 192
    iget v4, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->maxByte:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 191
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v6, 0x0

    .line 132
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-interface {p4, v6, p5}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 134
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 135
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v3

    invoke-interface {p4, p6, v3}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 137
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "expected":Ljava/lang/String;
    iget v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->maxByte:I

    .line 139
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->getByteLength(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    .line 138
    sub-int/2addr v3, v4

    .line 140
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v4

    sub-int v5, p6, p5

    sub-int/2addr v4, v5

    .line 138
    sub-int v1, v3, v4

    .line 145
    .local v1, "keep":I
    if-gtz v1, :cond_2

    .line 146
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "keep <= 0"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 148
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "source.length() > 0"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->activity:Landroid/app/Activity;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->isToastShowing:Z

    if-nez v3, :cond_0

    .line 151
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "toast"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->activity:Landroid/app/Activity;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->getToastString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    .line 153
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 154
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->isToastShowing:Z

    .line 157
    :cond_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->alertDialog:Landroid/app/AlertDialog;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-nez v3, :cond_1

    .line 158
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "alertDialog"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 163
    :cond_1
    const-string/jumbo v3, ""

    .line 186
    :goto_0
    return-object v3

    .line 164
    :cond_2
    sub-int v3, p3, p2

    if-lt v1, v3, :cond_3

    .line 165
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "keep >= (end - start)"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->isToastShowing:Z

    .line 167
    const/4 v3, 0x0

    goto :goto_0

    .line 169
    :cond_3
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "else"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    sub-int v3, p3, p2

    if-ge v1, v3, :cond_5

    .line 172
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "keep < (end - start)"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->activity:Landroid/app/Activity;

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->isToastShowing:Z

    if-nez v3, :cond_4

    .line 175
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "toast"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->activity:Landroid/app/Activity;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->getToastString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    .line 177
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 180
    :cond_4
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->alertDialog:Landroid/app/AlertDialog;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-nez v3, :cond_5

    .line 181
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "alertDialog"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 186
    :cond_5
    add-int v3, p2, v1

    invoke-interface {p1, p2, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0
.end method
