.class public abstract Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
.super Ljava/lang/Object;
.source "OnDrivelinkLocationBaseListener.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifySmartAlert(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;Ljava/lang/String;)V
    .locals 0
    .param p1, "incidentInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;
    .param p2, "msgErro"    # Ljava/lang/String;

    .prologue
    .line 144
    return-void
.end method

.method public onResponseCreateUserProfile(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 115
    return-void
.end method

.method public onResponseRemoveGroupShared(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 87
    return-void
.end method

.method public onResponseRequestAcceptLocationShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 0
    .param p1, "result"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 53
    return-void
.end method

.method public onResponseRequestAcceptShareMyLocation(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 48
    return-void
.end method

.method public onResponseRequestAddFriendsToGroup(ZLjava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "friendAdded"    # Z
    .param p3, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 65
    .local p2, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    return-void
.end method

.method public onResponseRequestAllParticipantTracking(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    return-void
.end method

.method public onResponseRequestChangeGroupDestination(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "response"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 121
    return-void
.end method

.method public onResponseRequestCreateGroupShare(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "groupCreated"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 59
    return-void
.end method

.method public onResponseRequestFriendLocationShare(Ljava/lang/Exception;Z)V
    .locals 0
    .param p1, "error"    # Ljava/lang/Exception;
    .param p2, "result"    # Z

    .prologue
    .line 42
    return-void
.end method

.method public onResponseRequestGroupSharedUpdateInfo(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "updated"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 71
    return-void
.end method

.method public onResponseRequestQuitFromGroup(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 92
    return-void
.end method

.method public onResponseRequestRecommendedLocationList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    return-void
.end method

.method public onResponseRequestReinviteFriendToGroup(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 0
    .param p1, "reinvited"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "friend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 133
    return-void
.end method

.method public onResponseRequestRestartGroupShared(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "response"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 127
    return-void
.end method

.method public onResponseRequestSetMyLocation(Z)V
    .locals 0
    .param p1, "locationSetted"    # Z

    .prologue
    .line 82
    return-void
.end method

.method public onResponseRequestShareMyLocation(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 37
    return-void
.end method

.method public onResponseRequestUpdateParticipantStatus(Z)V
    .locals 0
    .param p1, "mResult"    # Z

    .prologue
    .line 138
    return-void
.end method
