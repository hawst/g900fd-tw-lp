.class public Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/ISACallbackImp;
.super Lcom/msc/sa/aidl/ISACallback$Stub;
.source "ISACallbackImp.java"


# instance fields
.field private mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/msc/sa/aidl/ISACallback$Stub;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/ISACallbackImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    .line 14
    check-cast p1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    .end local p1    # "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/ISACallbackImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    .line 15
    return-void
.end method


# virtual methods
.method public onReceiveAccessToken(IZLandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/ISACallbackImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->onReceiveSamsungAccountAccessToken(ZLandroid/os/Bundle;)V

    .line 22
    return-void
.end method

.method public onReceiveAuthCode(IZLandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/ISACallbackImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->onReceiveSamsungAccountAuthCode(ZLandroid/os/Bundle;)V

    .line 45
    return-void
.end method

.method public onReceiveChecklistValidation(IZLandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/ISACallbackImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    .line 28
    invoke-virtual {v0, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->onReceiveSamsungAccountChecklistValidation(ZLandroid/os/Bundle;)V

    .line 30
    return-void
.end method

.method public onReceiveDisclaimerAgreement(IZLandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/ISACallbackImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    .line 36
    invoke-virtual {v0, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->onReceiveSamsungAccountDisclaimerAgreement(ZLandroid/os/Bundle;)V

    .line 38
    return-void
.end method

.method public onReceiveSCloudAccessToken(IZLandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/ISACallbackImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    .line 51
    invoke-virtual {v0, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->onReceiveSamsungAccountSCloudAccessToken(ZLandroid/os/Bundle;)V

    .line 52
    return-void
.end method
