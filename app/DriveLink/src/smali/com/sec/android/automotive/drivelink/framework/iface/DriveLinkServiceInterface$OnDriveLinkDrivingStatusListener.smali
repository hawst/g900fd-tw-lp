.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDriveLinkDrivingStatusListener"
.end annotation


# virtual methods
.method public abstract onNotifyCarSpeedStatusChange(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;)V
.end method

.method public abstract onResponseRequestStartDrivingStatusMonitoring(Z)V
.end method

.method public abstract onResponseRequestStopDrivingStatusMonitoring(Z)V
.end method
