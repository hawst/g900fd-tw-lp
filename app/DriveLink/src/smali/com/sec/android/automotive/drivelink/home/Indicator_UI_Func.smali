.class public Lcom/sec/android/automotive/drivelink/home/Indicator_UI_Func;
.super Ljava/lang/Object;
.source "Indicator_UI_Func.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static setAlbum(Landroid/widget/ImageView;Z)V
    .locals 1
    .param p0, "$iv"    # Landroid/widget/ImageView;
    .param p1, "isOn"    # Z

    .prologue
    .line 16
    if-eqz p1, :cond_0

    .line 17
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 20
    :goto_0
    return-void

    .line 19
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method static setEqualizer(Landroid/widget/ImageView;Z)V
    .locals 1
    .param p0, "$iv"    # Landroid/widget/ImageView;
    .param p1, "isOn"    # Z

    .prologue
    .line 9
    if-eqz p1, :cond_0

    .line 10
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 13
    :goto_0
    return-void

    .line 12
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
