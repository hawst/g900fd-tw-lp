.class public Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SoundSettingReceiver.java"


# static fields
.field private static final SOUND_FORCE_SETTING:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static canLaunchToast:Z

.field private static timerManager:Ljava/util/Timer;


# instance fields
.field SoundChangeHandler:Landroid/os/Handler;

.field mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;

    .line 22
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 21
    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->TAG:Ljava/lang/String;

    .line 27
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->timerManager:Ljava/util/Timer;

    .line 28
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->canLaunchToast:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 72
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$1;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->SoundChangeHandler:Landroid/os/Handler;

    .line 20
    return-void
.end method

.method static synthetic access$0()Z
    .locals 1

    .prologue
    .line 28
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->canLaunchToast:Z

    return v0
.end method

.method static synthetic access$1(Z)V
    .locals 0

    .prologue
    .line 28
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->canLaunchToast:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->setTimer()V

    return-void
.end method

.method static synthetic access$3()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private setTimer()V
    .locals 4

    .prologue
    .line 91
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->timerManager:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->timerManager:Ljava/util/Timer;

    .line 95
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->timerManager:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 96
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->timerManager:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$3;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;)V

    .line 101
    const-wide/16 v2, 0x9c4

    .line 96
    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 102
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->mContext:Landroid/content/Context;

    .line 33
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Ringer mode changed"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$2;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 68
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 70
    :cond_0
    return-void
.end method
