.class public Lcom/sec/android/automotive/drivelink/location/model/LocationShareContact;
.super Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;
.source "LocationShareContact.java"


# instance fields
.field private contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 0
    .param p1, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareContact;->contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 24
    return-void
.end method


# virtual methods
.method public getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareContact;->contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareContact;->contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->CONTACT:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    return-object v0
.end method
