.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLPlaceOnResponseImp;
.super Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/iface/DLPlaceOnResponse;
.source "DLPlaceOnResponseImp.java"


# instance fields
.field private mResponseCode:Ljava/lang/String;

.field private mResponseMessage:Ljava/lang/String;

.field private mShareGroupId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "result"    # Lorg/json/JSONObject;

    .prologue
    const/4 v1, 0x0

    .line 13
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/iface/DLPlaceOnResponse;-><init>()V

    .line 9
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLPlaceOnResponseImp;->mShareGroupId:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLPlaceOnResponseImp;->mResponseCode:Ljava/lang/String;

    .line 11
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLPlaceOnResponseImp;->mResponseMessage:Ljava/lang/String;

    .line 16
    :try_start_0
    const-string/jumbo v1, "shareGroupId"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLPlaceOnResponseImp;->mShareGroupId:Ljava/lang/String;

    .line 17
    const-string/jumbo v1, "responseMsg"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLPlaceOnResponseImp;->mResponseMessage:Ljava/lang/String;

    .line 18
    const-string/jumbo v1, "responseCode"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLPlaceOnResponseImp;->mResponseCode:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    :goto_0
    return-void

    .line 19
    :catch_0
    move-exception v0

    .line 20
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getShareGroupId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLPlaceOnResponseImp;->mShareGroupId:Ljava/lang/String;

    return-object v0
.end method

.method public responseCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLPlaceOnResponseImp;->mResponseCode:Ljava/lang/String;

    return-object v0
.end method

.method public responseMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLPlaceOnResponseImp;->mResponseMessage:Ljava/lang/String;

    return-object v0
.end method
