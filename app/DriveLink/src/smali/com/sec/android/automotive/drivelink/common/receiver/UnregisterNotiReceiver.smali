.class public Lcom/sec/android/automotive/drivelink/common/receiver/UnregisterNotiReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UnregisterNotiReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/android/automotive/drivelink/common/receiver/UnregisterNotiReceiver;

    .line 13
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 12
    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/UnregisterNotiReceiver;->TAG:Ljava/lang/String;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 17
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 18
    .local v0, "action":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/UnregisterNotiReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "recieved:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    const-string/jumbo v2, "notification"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 20
    check-cast v1, Landroid/app/NotificationManager;

    .line 22
    .local v1, "mNotificationManager":Landroid/app/NotificationManager;
    const/16 v2, 0x1e3e

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 23
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/UnregisterNotiReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "cancel Notify register"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    return-void
.end method
