.class Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;
.super Ljava/lang/Thread;
.source "NotificationItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NotiRingtoneThread"
.end annotation


# instance fields
.field private mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V
    .locals 1

    .prologue
    .line 740
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 741
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 754
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 758
    const/4 v3, 0x2

    :try_start_0
    invoke-static {v3}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v1

    .line 760
    .local v1, "notification":Landroid/net/Uri;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 759
    invoke-static {v3, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v2

    .line 761
    .local v2, "r":Landroid/media/Ringtone;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->requestAudioFocus()Z
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$2(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z

    .line 762
    if-eqz v2, :cond_0

    .line 763
    invoke-virtual {v2}, Landroid/media/Ringtone;->play()V

    .line 764
    const-string/jumbo v3, "[NotificationItem]"

    const-string/jumbo v4, ">>>Alert Sound Start"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    :goto_0
    invoke-virtual {v2}, Landroid/media/Ringtone;->isPlaying()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-nez v3, :cond_2

    .line 776
    .end local v1    # "notification":Landroid/net/Uri;
    .end local v2    # "r":Landroid/media/Ringtone;
    :cond_0
    :goto_1
    const-string/jumbo v3, "[NotificationItem]"

    const-string/jumbo v4, "<<<Alert Sound End"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    if-eqz v3, :cond_1

    .line 778
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->abandonAudioFocus()Z
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$3(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z

    .line 779
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    invoke-interface {v3}, Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;->onNotiSoundFinished()V

    .line 781
    :cond_1
    return-void

    .line 766
    .restart local v1    # "notification":Landroid/net/Uri;
    .restart local v2    # "r":Landroid/media/Ringtone;
    :cond_2
    const-wide/16 v3, 0x64

    :try_start_1
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 769
    .end local v1    # "notification":Landroid/net/Uri;
    .end local v2    # "r":Landroid/media/Ringtone;
    :catch_0
    move-exception v0

    .line 770
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 771
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    goto :goto_1

    .line 772
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 773
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public setOnSoundFinishedListener(Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    .prologue
    .line 744
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiRingtoneThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    .line 745
    return-void
.end method
