.class final enum Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;
.super Ljava/lang/Enum;
.source "SetupWizardNavigationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MapType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BAIDU:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

.field public static final enum CMCC_NAVI:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

.field public static final enum DAUM:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

.field public static final enum GOOGLE:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

.field public static final enum OLLEH:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

.field public static final enum TMAP:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

.field public static final enum UPLUS:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    const-string/jumbo v1, "GOOGLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->GOOGLE:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    const-string/jumbo v1, "DAUM"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->DAUM:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    const-string/jumbo v1, "TMAP"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->TMAP:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    const-string/jumbo v1, "UPLUS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->UPLUS:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    const-string/jumbo v1, "OLLEH"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->OLLEH:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    const-string/jumbo v1, "BAIDU"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->BAIDU:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    const-string/jumbo v1, "CMCC_NAVI"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->CMCC_NAVI:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    .line 54
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->GOOGLE:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->DAUM:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->TMAP:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->UPLUS:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->OLLEH:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->BAIDU:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->CMCC_NAVI:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
