.class public Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;
.super Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
.source "OllehNavigationAdapter.java"


# static fields
.field private static final KEY_CALLER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.automotive.drivelink"

.field public static final KEY_COORDINATE_WGS84:Ljava/lang/String; = "COORDINATE_WGS84"

.field private static final OLLEH_NAVI_ACTION_NAME:Ljava/lang/String; = "kt.navi.OLLEH_NAVIGATION"

.field private static final OLLEH_NAVI_PACKAGE_NAME:Ljava/lang/String; = "kt.navi"

.field private static final TAG:Ljava/lang/String; = "OllehNavigationAdapter"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public getNaviPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    const-string/jumbo v0, "kt.navi"

    return-object v0
.end method

.method public getNavigationIntent(DDLjava/lang/String;)Landroid/content/Intent;
    .locals 9
    .param p1, "lat"    # D
    .param p3, "lng"    # D
    .param p5, "address"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    .line 43
    .local v0, "mOrigin":Lcom/google/android/gms/maps/model/LatLng;
    if-eqz v0, :cond_4

    .line 44
    const/4 v3, 0x4

    new-array v2, v3, [D

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    aput-wide v3, v2, v8

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    aput-wide v3, v2, v5

    const/4 v3, 0x2

    aput-wide p1, v2, v3

    const/4 v3, 0x3

    .line 45
    aput-wide p3, v2, v3

    .line 47
    .local v2, "pt":[D
    const/4 v1, 0x0

    .line 48
    .local v1, "navigationIntent":Landroid/content/Intent;
    cmpl-double v3, p1, v6

    if-eqz v3, :cond_1

    cmpl-double v3, p3, v6

    if-eqz v3, :cond_1

    .line 49
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v3, "kt.navi.OLLEH_NAVIGATION"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 50
    .restart local v1    # "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v3, "CALLER_PACKAGE_NAME"

    .line 51
    const-string/jumbo v4, "com.sec.android.automotive.drivelink"

    .line 50
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string/jumbo v3, "COORDINATE_WGS84"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 53
    const-string/jumbo v3, "ROUTE_WGS84_POINT_NAME"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[D)Landroid/content/Intent;

    .line 73
    :cond_0
    :goto_0
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 75
    const/high16 v3, 0x200000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 76
    const v3, 0x8000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 80
    .end local v1    # "navigationIntent":Landroid/content/Intent;
    .end local v2    # "pt":[D
    :goto_1
    return-object v1

    .line 54
    .restart local v1    # "navigationIntent":Landroid/content/Intent;
    .restart local v2    # "pt":[D
    :cond_1
    if-eqz p5, :cond_3

    invoke-virtual {p5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 55
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v3, "kt.navi.OLLEH_NAVIGATION"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 56
    .restart local v1    # "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v3, "CALLER_PACKAGE_NAME"

    .line 57
    const-string/jumbo v4, "com.sec.android.automotive.drivelink"

    .line 56
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    const-string/jumbo v3, "EXTERN_LINK_TYPE"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 59
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x64

    if-lt v3, v4, :cond_2

    .line 60
    const-string/jumbo v3, "LINK_SEARCH_WORD"

    .line 61
    const/16 v4, 0x63

    invoke-virtual {p5, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 60
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 63
    :cond_2
    const-string/jumbo v3, "LINK_SEARCH_WORD"

    invoke-virtual {v1, v3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 66
    :cond_3
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 67
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 68
    const-string/jumbo v4, "kt.navi"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 69
    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 79
    .end local v1    # "navigationIntent":Landroid/content/Intent;
    .end local v2    # "pt":[D
    :cond_4
    const-string/jumbo v3, "OllehNavigationAdapter"

    const-string/jumbo v4, "GPS LatLng is null"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public startMap(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startMap(Landroid/content/Context;)V

    .line 88
    if-eqz p1, :cond_0

    .line 89
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    .line 103
    :goto_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "kt.navi"

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 104
    const-string/jumbo v4, "OllehNavigationAdapter"

    const-string/jumbo v5, "app installed"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    .line 107
    const v5, 0x7f0a061c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "mTts":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    .line 110
    const-string/jumbo v5, "kt.navi.OLLEH_NAVIGATION"

    const-string/jumbo v6, ""

    .line 109
    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .local v3, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5, v1, v3}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    .line 132
    .end local v1    # "mTts":Ljava/lang/String;
    .end local v3    # "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    :goto_1
    return-void

    .line 91
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 120
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 119
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v4

    .line 122
    const-string/jumbo v5, "PREF_SETTINGS_MY_NAVIGATION"

    .line 123
    const/4 v6, 0x0

    .line 121
    invoke-virtual {v4, v5, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v2

    .line 124
    .local v2, "mapID":I
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v0

    .line 125
    .local v0, "goToMarket":Landroid/content/Intent;
    if-eqz v0, :cond_2

    .line 126
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 128
    :cond_2
    const-string/jumbo v4, "Error Olleh Navi"

    .line 129
    const-string/jumbo v5, "Error on starting Olleh Navigation Intent"

    .line 128
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public startMultiNavi(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pacel"    # Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .prologue
    .line 136
    const/4 v9, 0x1

    .line 138
    .local v9, "ret":Z
    if-eqz p1, :cond_1

    .line 139
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    .line 145
    :goto_0
    iget v0, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    if-nez v0, :cond_2

    .line 146
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->startMultiWindowsActivity(Landroid/content/Context;)V

    .line 177
    :cond_0
    :goto_1
    return v9

    .line 141
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->startMultiWindowsActivity(Landroid/content/Context;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "kt.navi"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 150
    iget-wide v1, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 151
    iget-wide v3, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    iget-object v5, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    move-object v0, p0

    .line 150
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->getNavigationIntent(DDLjava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    .line 154
    .local v8, "navigationIntent":Landroid/content/Intent;
    if-eqz v8, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 159
    .end local v8    # "navigationIntent":Landroid/content/Intent;
    :cond_3
    const/4 v9, 0x0

    .line 162
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 161
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 165
    const-string/jumbo v1, "PREF_SETTINGS_MY_NAVIGATION"

    .line 166
    const/4 v2, 0x0

    .line 164
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v7

    .line 167
    .local v7, "mapID":I
    invoke-virtual {p0, v7}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v6

    .line 168
    .local v6, "goToMarket":Landroid/content/Intent;
    if-eqz v6, :cond_4

    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 171
    :cond_4
    const-string/jumbo v0, "Error Olleh Navi"

    .line 172
    const-string/jumbo v1, "Error on starting Olleh Navigation Intent"

    .line 171
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public startMultiWindowsActivity(Landroid/content/Context;)V
    .locals 3
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 181
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 182
    const-string/jumbo v2, "kt.navi"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 183
    .local v0, "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 185
    const/high16 v1, 0x200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 186
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 187
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 190
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 191
    return-void
.end method

.method public startNavigation(Landroid/content/Context;DDLjava/lang/String;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "tts"    # Ljava/lang/String;

    .prologue
    .line 196
    if-eqz p1, :cond_0

    .line 197
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    .line 202
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "kt.navi"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 203
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;DDLjava/lang/String;)V

    .line 205
    .local v0, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, p6, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    .line 223
    .end local v0    # "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    :goto_1
    return-void

    .line 199
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 211
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 210
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 213
    const-string/jumbo v2, "PREF_SETTINGS_MY_NAVIGATION"

    .line 214
    const/4 v3, 0x0

    .line 212
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v8

    .line 215
    .local v8, "mapID":I
    invoke-virtual {p0, v8}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v7

    .line 216
    .local v7, "goToMarket":Landroid/content/Intent;
    if-eqz v7, :cond_2

    .line 217
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 219
    :cond_2
    const-string/jumbo v1, "Error Olleh Navi"

    .line 220
    const-string/jumbo v2, "Error on starting Olleh Navigation Intent"

    .line 219
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
