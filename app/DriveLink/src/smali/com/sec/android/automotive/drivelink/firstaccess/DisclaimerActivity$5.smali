.class Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$5;
.super Landroid/os/Handler;
.source "DisclaimerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 221
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 224
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mCheckStartHomeActivity:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$7(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 225
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$8(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;Z)V

    .line 226
    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "runHandler : start HomeActivity"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 228
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    const-class v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 229
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->startActivity(Landroid/content/Intent;)V

    .line 230
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->finish()V

    .line 232
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
