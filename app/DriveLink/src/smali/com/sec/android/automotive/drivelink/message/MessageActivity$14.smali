.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->showInboxList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 1514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1538
    const-string/jumbo v0, "MessageActivity"

    const-string/jumbo v1, "showInboxList - setAutoShrink : false n true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1539
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 1540
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 1541
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    .line 1526
    if-nez p1, :cond_0

    .line 1527
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$60(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$58(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    .line 1528
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getInitialPosition()I

    move-result v1

    .line 1527
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1529
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$59(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1534
    :goto_0
    return-void

    .line 1531
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$59(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 1532
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$58(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 1531
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$58(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1519
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$59(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 1520
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$58(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 1519
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1522
    :cond_0
    return-void
.end method
