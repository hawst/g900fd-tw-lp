.class Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/home/HomeActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS()[I
    .locals 3

    .prologue
    .line 273
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->values()[Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_CHANGE_NONE:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_NOTIFY_CAR_SPEED_STATUS_CHANGE:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_REQUEST_START_DRIVING:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_REQUEST_STOP_DRIVING:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDriveStatusChanged(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;)V
    .locals 6
    .param p1, "Type"    # Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 277
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS()[I

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 319
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 283
    :pswitch_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v0

    .line 284
    .local v0, "dlspeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    if-eqz v0, :cond_1

    .line 285
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v2

    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-eq v2, v3, :cond_1

    .line 286
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$9(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V

    .line 291
    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$10(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 292
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$10(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 293
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$1()Ljava/lang/String;

    move-result-object v2

    .line 294
    const-string/jumbo v3, "update drving status_TTS isShowing _ START"

    .line 293
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$10(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mIsNowDriving:Z
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$11(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->updateList(Z)V

    goto :goto_0

    .line 288
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$9(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V

    goto :goto_1

    .line 301
    .end local v0    # "dlspeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    :pswitch_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->responseCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v1

    .line 303
    .local v1, "speedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    if-eqz v1, :cond_2

    .line 304
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v2

    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_FAST:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-ne v2, v3, :cond_2

    .line 305
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$9(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V

    .line 310
    :goto_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$10(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 311
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$10(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 312
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$1()Ljava/lang/String;

    move-result-object v2

    .line 313
    const-string/jumbo v3, "update drving status_TTS isShowing _ state change"

    .line 312
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$10(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mIsNowDriving:Z
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$11(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->updateList(Z)V

    goto/16 :goto_0

    .line 307
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$9(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V

    goto :goto_2

    .line 277
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
