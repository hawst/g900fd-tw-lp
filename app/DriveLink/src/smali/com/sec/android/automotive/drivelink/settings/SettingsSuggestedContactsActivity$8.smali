.class Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;
.super Ljava/lang/Object;
.source "SettingsSuggestedContactsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->loadListContents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    iput p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;->val$position:I

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 240
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->mList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$9(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;->val$position:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 242
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 243
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 242
    invoke-interface {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->removeFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 244
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 245
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestFavoriteContactList(Landroid/content/Context;)V

    .line 247
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_0
    return-void
.end method
