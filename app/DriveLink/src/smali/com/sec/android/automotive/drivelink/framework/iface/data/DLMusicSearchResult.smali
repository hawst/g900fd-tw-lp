.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLMusicSearchResult.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 11
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getResultItemCount()I

    move-result v5

    if-lt p1, v5, :cond_1

    .line 12
    :cond_0
    const/4 v4, 0x0

    .line 34
    :goto_0
    return-object v4

    .line 15
    :cond_1
    const/4 v4, 0x0

    .line 16
    .local v4, "musicObject":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getArtistList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 17
    .local v2, "artistListCount":I
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getAlbumList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 19
    .local v0, "albumListCount":I
    add-int/lit8 v3, v2, -0x1

    .line 20
    .local v3, "artistListLastIndex":I
    add-int v5, v2, v0

    add-int/lit8 v1, v5, -0x1

    .line 22
    .local v1, "albumListLastIndex":I
    if-ltz p1, :cond_2

    if-gt p1, v3, :cond_2

    .line 23
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getArtistList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "musicObject":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .line 24
    .restart local v4    # "musicObject":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    goto :goto_0

    :cond_2
    if-gt p1, v1, :cond_3

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getAlbumList()Ljava/util/ArrayList;

    move-result-object v5

    sub-int v6, p1, v2

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "musicObject":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .line 26
    .restart local v4    # "musicObject":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    goto :goto_0

    .line 27
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getMusicList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getMusicList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "musicObject":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .line 29
    .restart local v4    # "musicObject":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    goto :goto_0

    .line 30
    :cond_4
    const-string/jumbo v5, "None"

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceDataFactory;->createDLMusic(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v4

    goto :goto_0
.end method

.method public abstract getAlbumList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getArtistList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMusicList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResultItemCount()I
.end method

.method public abstract getSearchKeyword()Ljava/lang/String;
.end method

.method public abstract getTotalMusicCount()I
.end method
