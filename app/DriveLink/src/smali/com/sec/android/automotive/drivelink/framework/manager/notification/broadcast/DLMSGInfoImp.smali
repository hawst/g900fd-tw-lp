.class public Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
.source "DLMSGInfoImp.java"


# instance fields
.field private mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

.field private mImage:Landroid/graphics/Bitmap;

.field private mMSGType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

.field private mMessage:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mPhoneNumber:Ljava/lang/String;

.field private mTime:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;-><init>()V

    .line 10
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mName:Ljava/lang/String;

    .line 11
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 12
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMessage:Ljava/lang/String;

    .line 13
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mTime:J

    .line 14
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mImage:Landroid/graphics/Bitmap;

    .line 15
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMSGType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    .line 16
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 19
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mName:Ljava/lang/String;

    .line 20
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 21
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMessage:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "Number"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;-><init>()V

    .line 10
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mName:Ljava/lang/String;

    .line 11
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 12
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMessage:Ljava/lang/String;

    .line 13
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mTime:J

    .line 14
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mImage:Landroid/graphics/Bitmap;

    .line 15
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMSGType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    .line 16
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 25
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p1, "Number"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "time"    # J

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;-><init>()V

    .line 10
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mName:Ljava/lang/String;

    .line 11
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 12
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMessage:Ljava/lang/String;

    .line 13
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mTime:J

    .line 14
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mImage:Landroid/graphics/Bitmap;

    .line 15
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMSGType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    .line 16
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 29
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMessage:Ljava/lang/String;

    .line 31
    iput-wide p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mTime:J

    .line 32
    return-void
.end method


# virtual methods
.method public getDLMessage()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    return-object v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getMSGType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMSGType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMessage:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mTime:J

    return-wide v0
.end method

.method public setDLMessage(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V
    .locals 0
    .param p1, "inMessage"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 95
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mImage:Landroid/graphics/Bitmap;

    .line 86
    return-void
.end method

.method public setMSGType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;)V
    .locals 0
    .param p1, "type"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mMSGType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    .line 77
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mName:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 67
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->mTime:J

    .line 68
    return-void
.end method
