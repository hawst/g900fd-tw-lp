.class public Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;
.super Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
.source "SandUpperView.java"


# instance fields
.field private FROM_ANCHOR:[[F

.field private FROM_CONTROL1:[[F

.field private FROM_CONTROL2:[[F

.field private TO_ANCHOR:[[F

.field private TO_CONTROL1:[[F

.field private TO_CONTROL2:[[F


# direct methods
.method public constructor <init>(Landroid/content/Context;FF)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentWidth"    # F
    .param p3, "contentHeight"    # F

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;-><init>(Landroid/content/Context;FF)V

    .line 27
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->init()V

    .line 28
    return-void
.end method

.method private getPaint(I)Landroid/graphics/Paint;
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 36
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 37
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 38
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 39
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 41
    return-object v0
.end method

.method private init()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method private makePathInfo()Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 102
    new-array v2, v8, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_0

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_1

    aput-object v3, v2, v5

    .line 103
    new-array v3, v4, [F

    fill-array-data v3, :array_2

    aput-object v3, v2, v4

    new-array v3, v4, [F

    fill-array-data v3, :array_3

    aput-object v3, v2, v7

    .line 102
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_ANCHOR:[[F

    .line 104
    new-array v2, v8, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_4

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_5

    aput-object v3, v2, v5

    .line 105
    new-array v3, v4, [F

    fill-array-data v3, :array_6

    aput-object v3, v2, v4

    new-array v3, v4, [F

    fill-array-data v3, :array_7

    aput-object v3, v2, v7

    .line 104
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_CONTROL1:[[F

    .line 106
    new-array v2, v8, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_8

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_9

    aput-object v3, v2, v5

    .line 107
    new-array v3, v4, [F

    fill-array-data v3, :array_a

    aput-object v3, v2, v4

    new-array v3, v4, [F

    fill-array-data v3, :array_b

    aput-object v3, v2, v7

    .line 106
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_CONTROL2:[[F

    .line 109
    new-array v2, v8, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_c

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_d

    aput-object v3, v2, v5

    .line 110
    new-array v3, v4, [F

    fill-array-data v3, :array_e

    aput-object v3, v2, v4

    new-array v3, v4, [F

    fill-array-data v3, :array_f

    aput-object v3, v2, v7

    .line 109
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_ANCHOR:[[F

    .line 111
    new-array v2, v8, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_10

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_11

    aput-object v3, v2, v5

    .line 112
    new-array v3, v4, [F

    fill-array-data v3, :array_12

    aput-object v3, v2, v4

    new-array v3, v4, [F

    fill-array-data v3, :array_13

    aput-object v3, v2, v7

    .line 111
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_CONTROL1:[[F

    .line 113
    new-array v2, v8, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_14

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_15

    aput-object v3, v2, v5

    .line 114
    new-array v3, v4, [F

    fill-array-data v3, :array_16

    aput-object v3, v2, v4

    new-array v3, v4, [F

    fill-array-data v3, :array_17

    aput-object v3, v2, v7

    .line 113
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_CONTROL2:[[F

    .line 116
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 117
    .local v1, "path":Landroid/graphics/Path;
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;

    const/16 v2, 0xfe

    const/16 v3, 0xfe

    const/16 v4, 0xfe

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v2

    invoke-direct {v0, v1, v2, v5}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;-><init>(Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    .line 119
    .local v0, "info":Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->modifyPathInfo(Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;F)V

    .line 121
    return-object v0

    .line 102
    nop

    :array_0
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    :array_1
    .array-data 4
        0x41c80000    # 25.0f
        0x40c00000    # 6.0f
    .end array-data

    .line 103
    :array_2
    .array-data 4
        0x41100000    # 9.0f
        0x40c00000    # 6.0f
    .end array-data

    :array_3
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    .line 104
    :array_4
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
    .end array-data

    :array_5
    .array-data 4
        0x41a80000    # 21.0f
        0x41800000    # 16.0f
    .end array-data

    .line 105
    :array_6
    .array-data 4
        0x41880000    # 17.0f
        0x40c00000    # 6.0f
    .end array-data

    :array_7
    .array-data 4
        0x41100000    # 9.0f
        0x40c00000    # 6.0f
    .end array-data

    .line 106
    :array_8
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
    .end array-data

    :array_9
    .array-data 4
        0x41c80000    # 25.0f
        0x40c00000    # 6.0f
    .end array-data

    .line 107
    :array_a
    .array-data 4
        0x41880000    # 17.0f
        0x40c00000    # 6.0f
    .end array-data

    :array_b
    .array-data 4
        0x41500000    # 13.0f
        0x41800000    # 16.0f
    .end array-data

    .line 109
    :array_c
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    :array_d
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    .line 110
    :array_e
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    :array_f
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    .line 111
    :array_10
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
    .end array-data

    :array_11
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    .line 112
    :array_12
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    :array_13
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    .line 113
    :array_14
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
    .end array-data

    :array_15
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    .line 114
    :array_16
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data

    :array_17
    .array-data 4
        0x41880000    # 17.0f
        0x41800000    # 16.0f
    .end array-data
.end method


# virtual methods
.method protected createPathInfos()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 61
    .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;>;"
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->makePathInfo()Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    return-object v0
.end method

.method protected modifyPathInfo(Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;F)V
    .locals 13
    .param p1, "info"    # Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;
    .param p2, "ratio"    # F

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 69
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;->PATH:Landroid/graphics/Path;

    .line 70
    .local v0, "path":Landroid/graphics/Path;
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 72
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_ANCHOR:[[F

    array-length v8, v9

    .line 73
    .local v8, "length":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-lt v7, v8, :cond_0

    .line 97
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->transform(Landroid/graphics/Path;)V

    .line 98
    return-void

    .line 74
    :cond_0
    if-nez v7, :cond_1

    .line 76
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_ANCHOR:[[F

    aget-object v9, v9, v7

    aget v9, v9, v11

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_ANCHOR:[[F

    aget-object v10, v10, v7

    aget v10, v10, v11

    invoke-virtual {p0, v9, v10, p2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->calculate(FFF)F

    move-result v5

    .line 77
    .local v5, "ax":F
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_ANCHOR:[[F

    aget-object v9, v9, v7

    aget v9, v9, v12

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_ANCHOR:[[F

    aget-object v10, v10, v7

    aget v10, v10, v12

    invoke-virtual {p0, v9, v10, p2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->calculate(FFF)F

    move-result v6

    .line 79
    .local v6, "ay":F
    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 73
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 82
    .end local v5    # "ax":F
    .end local v6    # "ay":F
    :cond_1
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_CONTROL1:[[F

    aget-object v9, v9, v7

    aget v9, v9, v11

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_CONTROL1:[[F

    aget-object v10, v10, v7

    aget v10, v10, v11

    invoke-virtual {p0, v9, v10, p2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->calculate(FFF)F

    move-result v1

    .line 84
    .local v1, "c1x":F
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_CONTROL1:[[F

    aget-object v9, v9, v7

    aget v9, v9, v12

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_CONTROL1:[[F

    aget-object v10, v10, v7

    aget v10, v10, v12

    invoke-virtual {p0, v9, v10, p2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->calculate(FFF)F

    move-result v2

    .line 86
    .local v2, "c1y":F
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_CONTROL2:[[F

    aget-object v9, v9, v7

    aget v9, v9, v11

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_CONTROL2:[[F

    aget-object v10, v10, v7

    aget v10, v10, v11

    invoke-virtual {p0, v9, v10, p2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->calculate(FFF)F

    move-result v3

    .line 88
    .local v3, "c2x":F
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_CONTROL2:[[F

    aget-object v9, v9, v7

    aget v9, v9, v12

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_CONTROL2:[[F

    aget-object v10, v10, v7

    aget v10, v10, v12

    invoke-virtual {p0, v9, v10, p2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->calculate(FFF)F

    move-result v4

    .line 90
    .local v4, "c2y":F
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_ANCHOR:[[F

    aget-object v9, v9, v7

    aget v9, v9, v11

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_ANCHOR:[[F

    aget-object v10, v10, v7

    aget v10, v10, v11

    invoke-virtual {p0, v9, v10, p2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->calculate(FFF)F

    move-result v5

    .line 91
    .restart local v5    # "ax":F
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->FROM_ANCHOR:[[F

    aget-object v9, v9, v7

    aget v9, v9, v12

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->TO_ANCHOR:[[F

    aget-object v10, v10, v7

    aget v10, v10, v12

    invoke-virtual {p0, v9, v10, p2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->calculate(FFF)F

    move-result v6

    .line 93
    .restart local v6    # "ay":F
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto :goto_1
.end method

.method public render(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 46
    invoke-virtual {p0, p1, p1, p1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;->render(FFF)V

    .line 47
    return-void
.end method

.method public render(FFF)V
    .locals 1
    .param p1, "ratio"    # F
    .param p2, "from"    # F
    .param p3, "to"    # F

    .prologue
    .line 51
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->render(FFF)V

    goto :goto_0
.end method
