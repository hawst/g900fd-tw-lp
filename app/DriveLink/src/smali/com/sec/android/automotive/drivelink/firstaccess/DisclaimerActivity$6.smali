.class Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;
.super Ljava/lang/Object;
.source "DisclaimerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

.field private final synthetic val$progressHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;->val$progressHandler:Landroid/os/Handler;

    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 238
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$9(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;Z)V

    .line 239
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mProgress:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$2(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)I

    move-result v1

    const/16 v2, 0x64

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->isAlive:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$10(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 251
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->isAlive:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$10(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 252
    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Warning_progress_run"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$11(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$11(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 255
    :cond_1
    return-void

    .line 241
    :cond_2
    const-wide/16 v1, 0x28

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 242
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;->val$progressHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;->val$progressHandler:Landroid/os/Handler;

    .line 243
    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 242
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 244
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 247
    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Warning_progress_error"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
