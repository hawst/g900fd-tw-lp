.class Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$1;
.super Ljava/lang/Object;
.source "DrivingManager.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->initialize(Landroid/content/Context;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstLocationUpdateEvent:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;Z)V

    .line 199
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)Landroid/location/LocationManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->access$3(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)Landroid/hardware/SensorManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 192
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    monitor-enter v1

    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->processLocationChangeEvent(Landroid/location/Location;)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->access$4(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;Landroid/location/Location;)V

    .line 192
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 196
    :cond_1
    const-string/jumbo v0, "DrivingManager.onLocationChanged()"

    .line 197
    const-string/jumbo v1, "Main Application is probably down!"

    .line 196
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 172
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 167
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 162
    return-void
.end method
