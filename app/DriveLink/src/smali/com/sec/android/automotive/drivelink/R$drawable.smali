.class public final Lcom/sec/android/automotive/drivelink/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_bg_alone:I = 0x7f020000

.field public static final ab_bg_dialog:I = 0x7f020001

.field public static final ab_bg_dialog_night:I = 0x7f020002

.field public static final ab_bg_register:I = 0x7f020003

.field public static final ab_textfield_clear_d:I = 0x7f020004

.field public static final ab_textfield_clear_n:I = 0x7f020005

.field public static final ab_textfield_cursor:I = 0x7f020006

.field public static final ab_textfield_search_f:I = 0x7f020007

.field public static final ab_textfield_search_n:I = 0x7f020008

.field public static final action_bar_btn:I = 0x7f020009

.field public static final action_bar_btn_bg:I = 0x7f02000a

.field public static final action_bar_btn_bg_commute:I = 0x7f02000b

.field public static final action_bar_btn_bg_night:I = 0x7f02000c

.field public static final action_bar_btn_f:I = 0x7f02000d

.field public static final action_bar_btn_p:I = 0x7f02000e

.field public static final action_bar_btn_p_night:I = 0x7f02000f

.field public static final action_bar_btn_s:I = 0x7f020010

.field public static final action_bar_btn_s_night:I = 0x7f020011

.field public static final action_bar_devider:I = 0x7f020012

.field public static final action_bar_search_btn:I = 0x7f020013

.field public static final action_bar_search_night_btn:I = 0x7f020014

.field public static final action_bar_sip_btn:I = 0x7f020015

.field public static final action_map_btn_bg:I = 0x7f020016

.field public static final action_map_btn_p:I = 0x7f020017

.field public static final action_map_btn_s:I = 0x7f020018

.field public static final ani_count_num_00:I = 0x7f020019

.field public static final ani_count_num_01:I = 0x7f02001a

.field public static final ani_count_num_02:I = 0x7f02001b

.field public static final ani_count_num_03:I = 0x7f02001c

.field public static final ani_count_ring:I = 0x7f02001d

.field public static final ani_dial_glow_01:I = 0x7f02001e

.field public static final ani_dial_glow_02:I = 0x7f02001f

.field public static final ani_dial_glow_03:I = 0x7f020020

.field public static final ani_dial_glow_04:I = 0x7f020021

.field public static final ani_dial_phone:I = 0x7f020022

.field public static final arrow_tutorial_01:I = 0x7f020023

.field public static final arrow_tutorial_02:I = 0x7f020024

.field public static final arrow_tutorial_down:I = 0x7f020025

.field public static final arrow_tutorial_up:I = 0x7f020026

.field public static final badge_home:I = 0x7f020027

.field public static final badge_new:I = 0x7f020028

.field public static final basic_list_btn:I = 0x7f020029

.field public static final basic_list_btn_n:I = 0x7f02002a

.field public static final basic_list_btn_p:I = 0x7f02002b

.field public static final bg_black_trasparent:I = 0x7f02002c

.field public static final bg_btn_2depth_dialog:I = 0x7f02002d

.field public static final bg_btn_2depth_dialog_f:I = 0x7f02002e

.field public static final bg_btn_2depth_dialog_p:I = 0x7f02002f

.field public static final bg_bubble_joined:I = 0x7f020030

.field public static final bg_bubble_p:I = 0x7f020031

.field public static final bg_bubble_s:I = 0x7f020032

.field public static final bg_home_dialog:I = 0x7f020033

.field public static final bg_home_menu_popup:I = 0x7f020034

.field public static final bg_home_music_dialog:I = 0x7f020035

.field public static final bg_indi_bubble:I = 0x7f020036

.field public static final bg_indi_bubble_point:I = 0x7f020037

.field public static final bg_info:I = 0x7f020038

.field public static final bg_loading:I = 0x7f020039

.field public static final bg_location_group_info:I = 0x7f02003a

.field public static final bg_multi_menu:I = 0x7f02003b

.field public static final bg_multi_music_full:I = 0x7f02003c

.field public static final bg_multi_music_full_night:I = 0x7f02003d

.field public static final bg_music_full:I = 0x7f02003e

.field public static final bg_music_full_dialog:I = 0x7f02003f

.field public static final bg_music_full_light_default:I = 0x7f020040

.field public static final bg_music_full_night:I = 0x7f020041

.field public static final bg_tutorial_dashboard:I = 0x7f020042

.field public static final bg_welcome:I = 0x7f020043

.field public static final btn_ab_back:I = 0x7f020044

.field public static final btn_ab_back_night:I = 0x7f020045

.field public static final btn_ab_drawer_menu:I = 0x7f020046

.field public static final btn_ab_drawer_menu_night:I = 0x7f020047

.field public static final btn_arrow_close:I = 0x7f020048

.field public static final btn_arrow_close_d:I = 0x7f020049

.field public static final btn_arrow_close_f:I = 0x7f02004a

.field public static final btn_arrow_close_n:I = 0x7f02004b

.field public static final btn_arrow_close_p:I = 0x7f02004c

.field public static final btn_arrow_open:I = 0x7f02004d

.field public static final btn_arrow_open_d:I = 0x7f02004e

.field public static final btn_arrow_open_f:I = 0x7f02004f

.field public static final btn_arrow_open_n:I = 0x7f020050

.field public static final btn_arrow_open_p:I = 0x7f020051

.field public static final btn_basic_d:I = 0x7f020052

.field public static final btn_basic_df:I = 0x7f020053

.field public static final btn_basic_f:I = 0x7f020054

.field public static final btn_basic_n:I = 0x7f020055

.field public static final btn_basic_p:I = 0x7f020056

.field public static final btn_basic_s:I = 0x7f020057

.field public static final btn_basic_selector:I = 0x7f020058

.field public static final btn_basic_text_black_color:I = 0x7f020059

.field public static final btn_basic_text_white_color:I = 0x7f02005a

.field public static final btn_bg_basic:I = 0x7f02005b

.field public static final btn_bg_basic_d:I = 0x7f02005c

.field public static final btn_bg_basic_f:I = 0x7f02005d

.field public static final btn_bg_basic_n:I = 0x7f02005e

.field public static final btn_bg_basic_nf:I = 0x7f02005f

.field public static final btn_bg_basic_p:I = 0x7f020060

.field public static final btn_bg_basic_s:I = 0x7f020061

.field public static final btn_bg_default:I = 0x7f020062

.field public static final btn_bg_default_02_d:I = 0x7f020063

.field public static final btn_bg_default_02_f:I = 0x7f020064

.field public static final btn_bg_default_02_n:I = 0x7f020065

.field public static final btn_bg_default_02_p:I = 0x7f020066

.field public static final btn_bg_default_02_s:I = 0x7f020067

.field public static final btn_bg_default_d:I = 0x7f020068

.field public static final btn_bg_default_f:I = 0x7f020069

.field public static final btn_bg_default_n:I = 0x7f02006a

.field public static final btn_bg_default_p:I = 0x7f02006b

.field public static final btn_bg_default_s:I = 0x7f02006c

.field public static final btn_bg_group_sharing:I = 0x7f02006d

.field public static final btn_bg_group_sharing_big:I = 0x7f02006e

.field public static final btn_bg_group_sharing_d:I = 0x7f02006f

.field public static final btn_bg_group_sharing_dest_port:I = 0x7f020070

.field public static final btn_bg_group_sharing_destination:I = 0x7f020071

.field public static final btn_bg_group_sharing_duration:I = 0x7f020072

.field public static final btn_bg_group_sharing_duration_d:I = 0x7f020073

.field public static final btn_bg_group_sharing_duration_f:I = 0x7f020074

.field public static final btn_bg_group_sharing_duration_n:I = 0x7f020075

.field public static final btn_bg_group_sharing_duration_p:I = 0x7f020076

.field public static final btn_bg_group_sharing_duration_s:I = 0x7f020077

.field public static final btn_bg_group_sharing_f:I = 0x7f020078

.field public static final btn_bg_group_sharing_n:I = 0x7f020079

.field public static final btn_bg_group_sharing_p:I = 0x7f02007a

.field public static final btn_bg_group_sharing_port:I = 0x7f02007b

.field public static final btn_bg_group_sharing_s:I = 0x7f02007c

.field public static final btn_bg_group_under_d:I = 0x7f02007d

.field public static final btn_bg_group_under_f:I = 0x7f02007e

.field public static final btn_bg_group_under_left_d:I = 0x7f02007f

.field public static final btn_bg_group_under_left_f:I = 0x7f020080

.field public static final btn_bg_group_under_left_n:I = 0x7f020081

.field public static final btn_bg_group_under_left_p:I = 0x7f020082

.field public static final btn_bg_group_under_n:I = 0x7f020083

.field public static final btn_bg_group_under_p:I = 0x7f020084

.field public static final btn_bg_group_under_right_d:I = 0x7f020085

.field public static final btn_bg_group_under_right_f:I = 0x7f020086

.field public static final btn_bg_group_under_right_n:I = 0x7f020087

.field public static final btn_bg_group_under_right_p:I = 0x7f020088

.field public static final btn_bg_home_menu:I = 0x7f020089

.field public static final btn_bg_sub_d:I = 0x7f02008a

.field public static final btn_bg_sub_f:I = 0x7f02008b

.field public static final btn_bg_sub_n:I = 0x7f02008c

.field public static final btn_bg_sub_p:I = 0x7f02008d

.field public static final btn_bg_tutorial:I = 0x7f02008e

.field public static final btn_bg_what_can_i_say:I = 0x7f02008f

.field public static final btn_call_common_f:I = 0x7f020090

.field public static final btn_call_common_n:I = 0x7f020091

.field public static final btn_call_common_p:I = 0x7f020092

.field public static final btn_call_common_s:I = 0x7f020093

.field public static final btn_close_edit:I = 0x7f020094

.field public static final btn_close_edit_d:I = 0x7f020095

.field public static final btn_close_edit_df:I = 0x7f020096

.field public static final btn_close_edit_f:I = 0x7f020097

.field public static final btn_close_edit_n:I = 0x7f020098

.field public static final btn_close_edit_p:I = 0x7f020099

.field public static final btn_divider:I = 0x7f02009a

.field public static final btn_home:I = 0x7f02009b

.field public static final btn_icon_call_n:I = 0x7f02009c

.field public static final btn_icon_cancel_d:I = 0x7f02009d

.field public static final btn_icon_cancel_n:I = 0x7f02009e

.field public static final btn_icon_group_desti_d:I = 0x7f02009f

.field public static final btn_icon_group_desti_n:I = 0x7f0200a0

.field public static final btn_icon_group_duration_d:I = 0x7f0200a1

.field public static final btn_icon_group_duration_n:I = 0x7f0200a2

.field public static final btn_icon_group_location_n:I = 0x7f0200a3

.field public static final btn_icon_group_location_p:I = 0x7f0200a4

.field public static final btn_icon_group_parti_d:I = 0x7f0200a5

.field public static final btn_icon_group_parti_n:I = 0x7f0200a6

.field public static final btn_icon_group_sharing_desti:I = 0x7f0200a7

.field public static final btn_icon_group_sharing_duration:I = 0x7f0200a8

.field public static final btn_icon_group_sharing_parti:I = 0x7f0200a9

.field public static final btn_icon_list_d:I = 0x7f0200aa

.field public static final btn_icon_list_n:I = 0x7f0200ab

.field public static final btn_icon_location_request:I = 0x7f0200ac

.field public static final btn_icon_location_request_d:I = 0x7f0200ad

.field public static final btn_icon_location_request_n:I = 0x7f0200ae

.field public static final btn_icon_location_request_night:I = 0x7f0200af

.field public static final btn_icon_location_request_night_d:I = 0x7f0200b0

.field public static final btn_icon_location_request_night_n:I = 0x7f0200b1

.field public static final btn_icon_location_send:I = 0x7f0200b2

.field public static final btn_icon_location_send_d:I = 0x7f0200b3

.field public static final btn_icon_location_send_n:I = 0x7f0200b4

.field public static final btn_icon_location_send_night:I = 0x7f0200b5

.field public static final btn_icon_location_send_night_d:I = 0x7f0200b6

.field public static final btn_icon_location_send_night_n:I = 0x7f0200b7

.field public static final btn_icon_map_d:I = 0x7f0200b8

.field public static final btn_icon_map_n:I = 0x7f0200b9

.field public static final btn_icon_message_inbox_n:I = 0x7f0200ba

.field public static final btn_icon_message_inbox_p:I = 0x7f0200bb

.field public static final btn_icon_more:I = 0x7f0200bc

.field public static final btn_icon_more_d:I = 0x7f0200bd

.field public static final btn_icon_more_n:I = 0x7f0200be

.field public static final btn_icon_more_night_d:I = 0x7f0200bf

.field public static final btn_icon_more_night_n:I = 0x7f0200c0

.field public static final btn_icon_next:I = 0x7f0200c1

.field public static final btn_icon_next_d:I = 0x7f0200c2

.field public static final btn_icon_next_n:I = 0x7f0200c3

.field public static final btn_icon_phone_logs_n:I = 0x7f0200c4

.field public static final btn_icon_phone_logs_p:I = 0x7f0200c5

.field public static final btn_icon_previous_d:I = 0x7f0200c6

.field public static final btn_icon_previous_n:I = 0x7f0200c7

.field public static final btn_icon_recom_n:I = 0x7f0200c8

.field public static final btn_icon_recom_p:I = 0x7f0200c9

.field public static final btn_icon_reply_n:I = 0x7f0200ca

.field public static final btn_icon_search_d:I = 0x7f0200cb

.field public static final btn_icon_search_n:I = 0x7f0200cc

.field public static final btn_icon_search_night_d:I = 0x7f0200cd

.field public static final btn_icon_search_night_n:I = 0x7f0200ce

.field public static final btn_icon_sip_d:I = 0x7f0200cf

.field public static final btn_icon_sip_n:I = 0x7f0200d0

.field public static final btn_location_back_f:I = 0x7f0200d1

.field public static final btn_location_back_n:I = 0x7f0200d2

.field public static final btn_location_back_p:I = 0x7f0200d3

.field public static final btn_location_default_d:I = 0x7f0200d4

.field public static final btn_location_default_df:I = 0x7f0200d5

.field public static final btn_location_default_f:I = 0x7f0200d6

.field public static final btn_location_default_p:I = 0x7f0200d7

.field public static final btn_location_default_s:I = 0x7f0200d8

.field public static final btn_location_info_next_d:I = 0x7f0200d9

.field public static final btn_location_info_next_n:I = 0x7f0200da

.field public static final btn_location_info_pre_d:I = 0x7f0200db

.field public static final btn_location_info_pre_n:I = 0x7f0200dc

.field public static final btn_location_tracing_f:I = 0x7f0200dd

.field public static final btn_location_tracing_n:I = 0x7f0200de

.field public static final btn_location_tracing_p:I = 0x7f0200df

.field public static final btn_message_read_next_d:I = 0x7f0200e0

.field public static final btn_message_read_next_f:I = 0x7f0200e1

.field public static final btn_message_read_next_n:I = 0x7f0200e2

.field public static final btn_message_read_next_p:I = 0x7f0200e3

.field public static final btn_message_read_pre_d:I = 0x7f0200e4

.field public static final btn_message_read_pre_f:I = 0x7f0200e5

.field public static final btn_message_read_pre_n:I = 0x7f0200e6

.field public static final btn_message_read_pre_p:I = 0x7f0200e7

.field public static final btn_message_tts_off_f:I = 0x7f0200e8

.field public static final btn_message_tts_off_n:I = 0x7f0200e9

.field public static final btn_message_tts_off_p:I = 0x7f0200ea

.field public static final btn_message_tts_on_d:I = 0x7f0200eb

.field public static final btn_message_tts_on_f:I = 0x7f0200ec

.field public static final btn_message_tts_on_n:I = 0x7f0200ed

.field public static final btn_message_tts_on_p:I = 0x7f0200ee

.field public static final btn_next_first_access_bg:I = 0x7f0200ef

.field public static final btn_part_f:I = 0x7f0200f0

.field public static final btn_part_invete_f:I = 0x7f0200f1

.field public static final btn_part_invete_n:I = 0x7f0200f2

.field public static final btn_part_me_f:I = 0x7f0200f3

.field public static final btn_part_me_n:I = 0x7f0200f4

.field public static final btn_part_me_selector:I = 0x7f0200f5

.field public static final btn_part_n:I = 0x7f0200f6

.field public static final btn_part_p:I = 0x7f0200f7

.field public static final btn_part_selector:I = 0x7f0200f8

.field public static final btn_popup_bg:I = 0x7f0200f9

.field public static final btn_previous_next_focus:I = 0x7f0200fa

.field public static final btn_previous_next_press:I = 0x7f0200fb

.field public static final btn_register_default_d:I = 0x7f0200fc

.field public static final btn_register_default_df:I = 0x7f0200fd

.field public static final btn_register_default_f:I = 0x7f0200fe

.field public static final btn_register_default_n:I = 0x7f0200ff

.field public static final btn_register_default_p:I = 0x7f020100

.field public static final btn_register_default_s:I = 0x7f020101

.field public static final btn_register_df:I = 0x7f020102

.field public static final btn_register_f:I = 0x7f020103

.field public static final btn_register_next:I = 0x7f020104

.field public static final btn_register_p:I = 0x7f020105

.field public static final btn_register_s:I = 0x7f020106

.field public static final btn_reinvite_f:I = 0x7f020107

.field public static final btn_reinvite_n:I = 0x7f020108

.field public static final btn_reinvite_p:I = 0x7f020109

.field public static final btn_reinvite_selector:I = 0x7f02010a

.field public static final btn_reinvite_user_background:I = 0x7f02010b

.field public static final btn_save_mycar:I = 0x7f02010c

.field public static final btn_send_textcolor:I = 0x7f02010d

.field public static final btn_sub_tts_off_f:I = 0x7f02010e

.field public static final btn_sub_tts_off_n:I = 0x7f02010f

.field public static final btn_sub_tts_off_p:I = 0x7f020110

.field public static final btn_sub_tts_on_d:I = 0x7f020111

.field public static final btn_sub_tts_on_f:I = 0x7f020112

.field public static final btn_sub_tts_on_n:I = 0x7f020113

.field public static final btn_sub_tts_on_p:I = 0x7f020114

.field public static final btn_text_color_selector:I = 0x7f020115

.field public static final btn_title_01_f:I = 0x7f020116

.field public static final btn_title_01_n:I = 0x7f020117

.field public static final btn_title_01_p:I = 0x7f020118

.field public static final btn_title_btm_f:I = 0x7f020119

.field public static final btn_title_btm_n:I = 0x7f02011a

.field public static final btn_title_btm_p:I = 0x7f02011b

.field public static final btn_title_center_f:I = 0x7f02011c

.field public static final btn_title_center_n:I = 0x7f02011d

.field public static final btn_title_center_p:I = 0x7f02011e

.field public static final btn_title_top_f:I = 0x7f02011f

.field public static final btn_title_top_n:I = 0x7f020120

.field public static final btn_title_top_p:I = 0x7f020121

.field public static final btn_voice_2depth_btn_text_color:I = 0x7f020122

.field public static final bubble_bg:I = 0x7f020123

.field public static final bubble_bg_information:I = 0x7f020124

.field public static final bubble_list_bottom_f:I = 0x7f020125

.field public static final bubble_list_bottom_p:I = 0x7f020126

.field public static final bubble_list_bottom_s:I = 0x7f020127

.field public static final bubble_list_center_f:I = 0x7f020128

.field public static final bubble_list_center_p:I = 0x7f020129

.field public static final bubble_list_center_s:I = 0x7f02012a

.field public static final bubble_list_top_f:I = 0x7f02012b

.field public static final bubble_list_top_p:I = 0x7f02012c

.field public static final bubble_list_top_s:I = 0x7f02012d

.field public static final call_info_icon_home_w:I = 0x7f02012e

.field public static final call_info_icon_office_w:I = 0x7f02012f

.field public static final call_info_icon_phone_w:I = 0x7f020130

.field public static final car_opt:I = 0x7f020131

.field public static final car_parking_icon:I = 0x7f020132

.field public static final cark_fuel_icon:I = 0x7f020133

.field public static final carmode_sub_call_02_ic_01:I = 0x7f020134

.field public static final carmode_sub_launch_01_ic_01:I = 0x7f020135

.field public static final carmode_sub_launch_02_ic_01:I = 0x7f020136

.field public static final carmode_sub_set_01_ic_01:I = 0x7f020137

.field public static final checkbox_off_d:I = 0x7f020138

.field public static final checkbox_off_df:I = 0x7f020139

.field public static final checkbox_off_f:I = 0x7f02013a

.field public static final checkbox_off_n:I = 0x7f02013b

.field public static final checkbox_off_p:I = 0x7f02013c

.field public static final checkbox_on_d:I = 0x7f02013d

.field public static final checkbox_on_df:I = 0x7f02013e

.field public static final checkbox_on_f:I = 0x7f02013f

.field public static final checkbox_on_n:I = 0x7f020140

.field public static final checkbox_on_p:I = 0x7f020141

.field public static final common_btn_basic:I = 0x7f020142

.field public static final common_signin_btn_icon_dark:I = 0x7f020143

.field public static final common_signin_btn_icon_disabled_dark:I = 0x7f020144

.field public static final common_signin_btn_icon_disabled_focus_dark:I = 0x7f020145

.field public static final common_signin_btn_icon_disabled_focus_light:I = 0x7f020146

.field public static final common_signin_btn_icon_disabled_light:I = 0x7f020147

.field public static final common_signin_btn_icon_focus_dark:I = 0x7f020148

.field public static final common_signin_btn_icon_focus_light:I = 0x7f020149

.field public static final common_signin_btn_icon_light:I = 0x7f02014a

.field public static final common_signin_btn_icon_normal_dark:I = 0x7f02014b

.field public static final common_signin_btn_icon_normal_light:I = 0x7f02014c

.field public static final common_signin_btn_icon_pressed_dark:I = 0x7f02014d

.field public static final common_signin_btn_icon_pressed_light:I = 0x7f02014e

.field public static final common_signin_btn_text_dark:I = 0x7f02014f

.field public static final common_signin_btn_text_disabled_dark:I = 0x7f020150

.field public static final common_signin_btn_text_disabled_focus_dark:I = 0x7f020151

.field public static final common_signin_btn_text_disabled_focus_light:I = 0x7f020152

.field public static final common_signin_btn_text_disabled_light:I = 0x7f020153

.field public static final common_signin_btn_text_focus_dark:I = 0x7f020154

.field public static final common_signin_btn_text_focus_light:I = 0x7f020155

.field public static final common_signin_btn_text_light:I = 0x7f020156

.field public static final common_signin_btn_text_normal_dark:I = 0x7f020157

.field public static final common_signin_btn_text_normal_light:I = 0x7f020158

.field public static final common_signin_btn_text_pressed_dark:I = 0x7f020159

.field public static final common_signin_btn_text_pressed_light:I = 0x7f02015a

.field public static final divider_ab:I = 0x7f02015b

.field public static final dot_indicator_selector:I = 0x7f02015c

.field public static final drawer_list_icon_behavior_d:I = 0x7f02015d

.field public static final drawer_list_icon_behavior_n:I = 0x7f02015e

.field public static final drawer_list_icon_consumable_d:I = 0x7f02015f

.field public static final drawer_list_icon_consumable_n:I = 0x7f020160

.field public static final drawer_list_icon_diagnostic_d:I = 0x7f020161

.field public static final drawer_list_icon_diagnostic_n:I = 0x7f020162

.field public static final drawer_list_icon_home:I = 0x7f020163

.field public static final drawer_list_icon_parked_d:I = 0x7f020164

.field public static final drawer_list_icon_parked_n:I = 0x7f020165

.field public static final drawer_list_icon_tohome_d:I = 0x7f020166

.field public static final drawer_list_icon_tohome_n:I = 0x7f020167

.field public static final drawer_list_icon_tooffice_d:I = 0x7f020168

.field public static final drawer_list_icon_tooffice_n:I = 0x7f020169

.field public static final drawer_menu_behavior:I = 0x7f02016a

.field public static final drawer_menu_consumable:I = 0x7f02016b

.field public static final drawer_menu_diagnostic:I = 0x7f02016c

.field public static final drawer_menu_list_bg:I = 0x7f02016d

.field public static final drawer_menu_parked:I = 0x7f02016e

.field public static final drawer_menu_sub_title_text_color:I = 0x7f02016f

.field public static final drawer_menu_text_color:I = 0x7f020170

.field public static final drawer_menu_tohome:I = 0x7f020171

.field public static final drawer_menu_tooffice:I = 0x7f020172

.field public static final drawer_subtitle_bg:I = 0x7f020173

.field public static final drawer_subtitle_bg_disable:I = 0x7f020174

.field public static final dummy_list_icon_mask_n:I = 0x7f020175

.field public static final dummy_list_mask_f:I = 0x7f020176

.field public static final dummy_list_mask_n:I = 0x7f020177

.field public static final dummy_list_mask_night_f:I = 0x7f020178

.field public static final dummy_list_mask_night_n:I = 0x7f020179

.field public static final dummy_list_mask_night_p:I = 0x7f02017a

.field public static final dummy_list_mask_night_s:I = 0x7f02017b

.field public static final dummy_list_mask_p:I = 0x7f02017c

.field public static final dummy_list_mask_s:I = 0x7f02017d

.field public static final expander_close:I = 0x7f02017e

.field public static final expander_open:I = 0x7f02017f

.field public static final find_info_time:I = 0x7f020180

.field public static final find_pin_car:I = 0x7f020181

.field public static final find_pin_me:I = 0x7f020182

.field public static final full_album:I = 0x7f020183

.field public static final full_album_mask:I = 0x7f020184

.field public static final full_album_shadow:I = 0x7f020185

.field public static final full_hide_f:I = 0x7f020186

.field public static final full_hide_n:I = 0x7f020187

.field public static final full_hide_p:I = 0x7f020188

.field public static final full_next_02_f:I = 0x7f020189

.field public static final full_next_02_n:I = 0x7f02018a

.field public static final full_next_02_p:I = 0x7f02018b

.field public static final full_next_f:I = 0x7f02018c

.field public static final full_next_n:I = 0x7f02018d

.field public static final full_next_p:I = 0x7f02018e

.field public static final full_pause_f:I = 0x7f02018f

.field public static final full_pause_n:I = 0x7f020190

.field public static final full_pause_p:I = 0x7f020191

.field public static final full_play_f:I = 0x7f020192

.field public static final full_play_n:I = 0x7f020193

.field public static final full_play_p:I = 0x7f020194

.field public static final full_prev_02_f:I = 0x7f020195

.field public static final full_prev_02_n:I = 0x7f020196

.field public static final full_prev_02_p:I = 0x7f020197

.field public static final full_prev_f:I = 0x7f020198

.field public static final full_prev_n:I = 0x7f020199

.field public static final full_prev_p:I = 0x7f02019a

.field public static final full_vol_f:I = 0x7f02019b

.field public static final full_vol_mute_f:I = 0x7f02019c

.field public static final full_vol_mute_n:I = 0x7f02019d

.field public static final full_vol_mute_p:I = 0x7f02019e

.field public static final full_vol_n:I = 0x7f02019f

.field public static final full_vol_p:I = 0x7f0201a0

.field public static final group_bar_letter_bg:I = 0x7f0201a1

.field public static final group_bar_mask:I = 0x7f0201a2

.field public static final group_bar_unknown:I = 0x7f0201a3

.field public static final group_info_btn_call:I = 0x7f0201a4

.field public static final group_info_text_line:I = 0x7f0201a5

.field public static final group_pin_bg_black:I = 0x7f0201a6

.field public static final group_pin_bg_blue:I = 0x7f0201a7

.field public static final group_pin_bg_gray:I = 0x7f0201a8

.field public static final group_pin_bg_name:I = 0x7f0201a9

.field public static final group_pin_bg_red:I = 0x7f0201aa

.field public static final group_pin_img:I = 0x7f0201ab

.field public static final group_pin_mask:I = 0x7f0201ac

.field public static final handle_f:I = 0x7f0201ad

.field public static final handle_n:I = 0x7f0201ae

.field public static final handle_p:I = 0x7f0201af

.field public static final home_btn_f:I = 0x7f0201b0

.field public static final home_btn_night_p:I = 0x7f0201b1

.field public static final home_btn_night_s:I = 0x7f0201b2

.field public static final home_btn_outline:I = 0x7f0201b3

.field public static final home_btn_outline_night:I = 0x7f0201b4

.field public static final home_btn_p:I = 0x7f0201b5

.field public static final home_btn_s:I = 0x7f0201b6

.field public static final home_message:I = 0x7f0201b7

.field public static final home_message_night:I = 0x7f0201b8

.field public static final home_music:I = 0x7f0201b9

.field public static final home_music_night:I = 0x7f0201ba

.field public static final home_phone:I = 0x7f0201bb

.field public static final home_phone_night:I = 0x7f0201bc

.field public static final home_routes:I = 0x7f0201bd

.field public static final home_routes_night:I = 0x7f0201be

.field public static final ic_launcher:I = 0x7f0201bf

.field public static final ic_plusone_medium_off_client:I = 0x7f0201c0

.field public static final ic_plusone_small_off_client:I = 0x7f0201c1

.field public static final ic_plusone_standard_off_client:I = 0x7f0201c2

.field public static final ic_plusone_tall_off_client:I = 0x7f0201c3

.field public static final icon_access_warning:I = 0x7f0201c4

.field public static final icon_connected:I = 0x7f0201c5

.field public static final icon_connected_d:I = 0x7f0201c6

.field public static final icon_connected_df:I = 0x7f0201c7

.field public static final icon_connected_f:I = 0x7f0201c8

.field public static final icon_connected_p:I = 0x7f0201c9

.field public static final icon_group_popup_close:I = 0x7f0201ca

.field public static final icon_nearby_no_search_gas:I = 0x7f0201cb

.field public static final icon_nearby_no_search_park:I = 0x7f0201cc

.field public static final icon_next:I = 0x7f0201cd

.field public static final icon_next_d:I = 0x7f0201ce

.field public static final icon_next_f:I = 0x7f0201cf

.field public static final icon_next_fp:I = 0x7f0201d0

.field public static final icon_next_n:I = 0x7f0201d1

.field public static final icon_next_p:I = 0x7f0201d2

.field public static final icon_part_host:I = 0x7f0201d3

.field public static final icon_part_web:I = 0x7f0201d4

.field public static final icon_register_car:I = 0x7f0201d5

.field public static final icon_register_car_p:I = 0x7f0201d6

.field public static final icon_register_phone:I = 0x7f0201d7

.field public static final icon_register_phone_p:I = 0x7f0201d8

.field public static final icon_register_welcome:I = 0x7f0201d9

.field public static final icon_setting_create:I = 0x7f0201da

.field public static final icon_title_arrived:I = 0x7f0201db

.field public static final icon_title_close:I = 0x7f0201dc

.field public static final icon_title_open:I = 0x7f0201dd

.field public static final image_register_account:I = 0x7f0201de

.field public static final image_tutorial_mic:I = 0x7f0201df

.field public static final image_tutorial_speak:I = 0x7f0201e0

.field public static final image_tutorial_touch:I = 0x7f0201e1

.field public static final image_welcome_p:I = 0x7f0201e2

.field public static final indi_equalizer_01:I = 0x7f0201e3

.field public static final indi_equalizer_02:I = 0x7f0201e4

.field public static final indi_equalizer_03:I = 0x7f0201e5

.field public static final indi_equalizer_04:I = 0x7f0201e6

.field public static final indi_equalizer_05:I = 0x7f0201e7

.field public static final indicator_car_mode:I = 0x7f0201e8

.field public static final info_count_bg_0:I = 0x7f0201e9

.field public static final info_count_bg_1:I = 0x7f0201ea

.field public static final info_count_bg_2:I = 0x7f0201eb

.field public static final info_count_bg_3:I = 0x7f0201ec

.field public static final input_field:I = 0x7f0201ed

.field public static final input_field_a:I = 0x7f0201ee

.field public static final input_field_cancel_dim:I = 0x7f0201ef

.field public static final input_field_cancel_nor:I = 0x7f0201f0

.field public static final input_field_d:I = 0x7f0201f1

.field public static final input_field_df:I = 0x7f0201f2

.field public static final input_field_f:I = 0x7f0201f3

.field public static final input_field_message:I = 0x7f0201f4

.field public static final input_field_n:I = 0x7f0201f5

.field public static final input_field_p:I = 0x7f0201f6

.field public static final input_field_s:I = 0x7f0201f7

.field public static final input_field_search_nor:I = 0x7f0201f8

.field public static final input_field_white_d:I = 0x7f0201f9

.field public static final input_field_white_f:I = 0x7f0201fa

.field public static final input_field_white_n:I = 0x7f0201fb

.field public static final input_field_white_p:I = 0x7f0201fc

.field public static final input_search_icon:I = 0x7f0201fd

.field public static final input_searchfiled_f:I = 0x7f0201fe

.field public static final input_searchfiled_n:I = 0x7f0201ff

.field public static final line_2depth_dialog:I = 0x7f020200

.field public static final line_box_register:I = 0x7f020201

.field public static final line_register:I = 0x7f020202

.field public static final linearroute_text_color:I = 0x7f020203

.field public static final list_3_mask:I = 0x7f020204

.field public static final list_3_thumb_bg:I = 0x7f020205

.field public static final list_3_thumb_group:I = 0x7f020206

.field public static final list_3_thumb_location_request:I = 0x7f020207

.field public static final list_3_thumb_send_location:I = 0x7f020208

.field public static final list_4_dummy_mask:I = 0x7f020209

.field public static final list_4_dummy_mask_night:I = 0x7f02020a

.field public static final list_4_dummy_mask_phonenumber:I = 0x7f02020b

.field public static final list_4_dummy_night_mask:I = 0x7f02020c

.field public static final list_4_location_thumb_gas:I = 0x7f02020d

.field public static final list_4_location_thumb_gas_night:I = 0x7f02020e

.field public static final list_4_location_thumb_meeting:I = 0x7f02020f

.field public static final list_4_location_thumb_meeting_night:I = 0x7f020210

.field public static final list_4_location_thumb_myplace:I = 0x7f020211

.field public static final list_4_location_thumb_myplace_add:I = 0x7f020212

.field public static final list_4_location_thumb_myplace_add_night:I = 0x7f020213

.field public static final list_4_location_thumb_myplace_night:I = 0x7f020214

.field public static final list_4_location_thumb_parking:I = 0x7f020215

.field public static final list_4_location_thumb_parking_night:I = 0x7f020216

.field public static final list_4_location_thumb_place:I = 0x7f020217

.field public static final list_4_location_thumb_place_dumap:I = 0x7f020218

.field public static final list_4_location_thumb_place_dumap_night:I = 0x7f020219

.field public static final list_4_location_thumb_place_night:I = 0x7f02021a

.field public static final list_4_location_thumb_place_olleh:I = 0x7f02021b

.field public static final list_4_location_thumb_place_olleh_night:I = 0x7f02021c

.field public static final list_4_location_thumb_place_tmap:I = 0x7f02021d

.field public static final list_4_location_thumb_place_tmap_night:I = 0x7f02021e

.field public static final list_4_location_thumb_place_unavi:I = 0x7f02021f

.field public static final list_4_location_thumb_place_unavi_night:I = 0x7f020220

.field public static final list_4_location_thumb_request_night:I = 0x7f020221

.field public static final list_4_location_thumb_share_night:I = 0x7f020222

.field public static final list_4_music_thumb_default:I = 0x7f020223

.field public static final list_4_music_thumb_song:I = 0x7f020224

.field public static final list_4_phone_thumb_fax:I = 0x7f020225

.field public static final list_4_phone_thumb_fax_night:I = 0x7f020226

.field public static final list_4_phone_thumb_home:I = 0x7f020227

.field public static final list_4_phone_thumb_home_add:I = 0x7f020228

.field public static final list_4_phone_thumb_home_add_night:I = 0x7f020229

.field public static final list_4_phone_thumb_home_night:I = 0x7f02022a

.field public static final list_4_phone_thumb_mobile:I = 0x7f02022b

.field public static final list_4_phone_thumb_mobile_night:I = 0x7f02022c

.field public static final list_4_phone_thumb_office:I = 0x7f02022d

.field public static final list_4_phone_thumb_office_add:I = 0x7f02022e

.field public static final list_4_phone_thumb_office_add_night:I = 0x7f02022f

.field public static final list_4_phone_thumb_office_night:I = 0x7f020230

.field public static final list_4_phone_thumb_phone:I = 0x7f020231

.field public static final list_4_phone_thumb_phone_night:I = 0x7f020232

.field public static final list_6_large_dummy_mask:I = 0x7f020233

.field public static final list_6_large_dummy_mask_n:I = 0x7f020234

.field public static final list_6_large_dummy_mask_p:I = 0x7f020235

.field public static final list_6_large_location_thumb_group:I = 0x7f020236

.field public static final list_6_large_location_thumb_request:I = 0x7f020237

.field public static final list_6_large_location_thumb_share:I = 0x7f020238

.field public static final list_6_large_location_thumb_sign:I = 0x7f020239

.field public static final list_6_large_management_thumb_service:I = 0x7f02023a

.field public static final list_6_large_mask:I = 0x7f02023b

.field public static final list_6_large_thumb_bg:I = 0x7f02023c

.field public static final list_6_large_unknown:I = 0x7f02023d

.field public static final list_6_small_dummy_mask:I = 0x7f02023e

.field public static final list_6_small_dummy_mask_n:I = 0x7f02023f

.field public static final list_6_small_dummy_mask_p:I = 0x7f020240

.field public static final list_6_small_location_thumb_gas:I = 0x7f020241

.field public static final list_6_small_location_thumb_group:I = 0x7f020242

.field public static final list_6_small_location_thumb_meeting:I = 0x7f020243

.field public static final list_6_small_location_thumb_parking:I = 0x7f020244

.field public static final list_6_small_location_thumb_place:I = 0x7f020245

.field public static final list_6_small_location_thumb_request:I = 0x7f020246

.field public static final list_6_small_location_thumb_share:I = 0x7f020247

.field public static final list_6_small_management_thumb_service:I = 0x7f020248

.field public static final list_6_small_mask:I = 0x7f020249

.field public static final list_6_small_phone_thumb_home:I = 0x7f02024a

.field public static final list_6_small_phone_thumb_mobile:I = 0x7f02024b

.field public static final list_6_small_phone_thumb_office:I = 0x7f02024c

.field public static final list_6_small_thumb_bg:I = 0x7f02024d

.field public static final list_6_small_unknown:I = 0x7f02024e

.field public static final list_bg:I = 0x7f02024f

.field public static final list_bg_d:I = 0x7f020250

.field public static final list_bg_df:I = 0x7f020251

.field public static final list_bg_f:I = 0x7f020252

.field public static final list_bg_n:I = 0x7f020253

.field public static final list_bg_p:I = 0x7f020254

.field public static final list_bg_s:I = 0x7f020255

.field public static final list_home_more_item:I = 0x7f020256

.field public static final list_icon_add_f:I = 0x7f020257

.field public static final list_icon_add_n:I = 0x7f020258

.field public static final list_icon_add_p:I = 0x7f020259

.field public static final list_icon_call_auto_rejected:I = 0x7f02025a

.field public static final list_icon_call_incoming:I = 0x7f02025b

.field public static final list_icon_call_missed:I = 0x7f02025c

.field public static final list_icon_call_outgoing:I = 0x7f02025d

.field public static final list_icon_call_rejected:I = 0x7f02025e

.field public static final list_icon_call_voice:I = 0x7f02025f

.field public static final list_icon_log_call:I = 0x7f020260

.field public static final list_icon_log_mms:I = 0x7f020261

.field public static final list_icon_log_sms:I = 0x7f020262

.field public static final list_icon_log_video:I = 0x7f020263

.field public static final list_item_main_text_color:I = 0x7f020264

.field public static final list_item_sub2_text_color:I = 0x7f020265

.field public static final list_item_sub_text_color:I = 0x7f020266

.field public static final list_location_bottom_selector:I = 0x7f020267

.field public static final list_location_center_selector:I = 0x7f020268

.field public static final list_location_title_no_participants_selector:I = 0x7f020269

.field public static final list_location_title_selector:I = 0x7f02026a

.field public static final list_new_badge:I = 0x7f02026b

.field public static final list_no_icon_home:I = 0x7f02026c

.field public static final list_no_icon_message:I = 0x7f02026d

.field public static final list_no_icon_music:I = 0x7f02026e

.field public static final list_no_icon_phone:I = 0x7f02026f

.field public static final list_no_icon_route:I = 0x7f020270

.field public static final list_section_divider:I = 0x7f020271

.field public static final location_icon_arrival:I = 0x7f020272

.field public static final location_icon_distance:I = 0x7f020273

.field public static final location_icon_speed:I = 0x7f020274

.field public static final location_info_tracking:I = 0x7f020275

.field public static final location_share_bg_transparent:I = 0x7f020276

.field public static final mini_expend_f:I = 0x7f020277

.field public static final mini_expend_n:I = 0x7f020278

.field public static final mini_expend_p:I = 0x7f020279

.field public static final mini_music_icon:I = 0x7f02027a

.field public static final more_popup_bg:I = 0x7f02027b

.field public static final more_popup_f:I = 0x7f02027c

.field public static final more_popup_p:I = 0x7f02027d

.field public static final multi_album_default:I = 0x7f02027e

.field public static final multi_album_mask:I = 0x7f02027f

.field public static final multi_album_shadow:I = 0x7f020280

.field public static final multi_close_f:I = 0x7f020281

.field public static final multi_close_n:I = 0x7f020282

.field public static final multi_close_p:I = 0x7f020283

.field public static final multi_dialog_album_default:I = 0x7f020284

.field public static final multi_dialog_album_mask:I = 0x7f020285

.field public static final multi_home_btn_f:I = 0x7f020286

.field public static final multi_home_btn_p:I = 0x7f020287

.field public static final multi_home_btn_s:I = 0x7f020288

.field public static final multi_home_home:I = 0x7f020289

.field public static final multi_home_message:I = 0x7f02028a

.field public static final multi_home_music:I = 0x7f02028b

.field public static final multi_home_phone:I = 0x7f02028c

.field public static final multi_home_routes:I = 0x7f02028d

.field public static final multi_next_f:I = 0x7f02028e

.field public static final multi_next_n:I = 0x7f02028f

.field public static final multi_next_p:I = 0x7f020290

.field public static final multi_pause_f:I = 0x7f020291

.field public static final multi_pause_n:I = 0x7f020292

.field public static final multi_pause_p:I = 0x7f020293

.field public static final multi_play_f:I = 0x7f020294

.field public static final multi_play_n:I = 0x7f020295

.field public static final multi_play_p:I = 0x7f020296

.field public static final multi_prev_f:I = 0x7f020297

.field public static final multi_prev_n:I = 0x7f020298

.field public static final multi_prev_p:I = 0x7f020299

.field public static final music_bar:I = 0x7f02029a

.field public static final music_bar_bg:I = 0x7f02029b

.field public static final music_streaming:I = 0x7f02029c

.field public static final navigate_appgar_black_bg:I = 0x7f02029d

.field public static final nearby_list_bar:I = 0x7f02029e

.field public static final nearby_pin_blue:I = 0x7f02029f

.field public static final nearby_pin_f_destination:I = 0x7f0202a0

.field public static final nearby_pin_f_gas:I = 0x7f0202a1

.field public static final nearby_pin_not_f:I = 0x7f0202a2

.field public static final nearby_pin_not_f_gas:I = 0x7f0202a3

.field public static final nearby_pin_red:I = 0x7f0202a4

.field public static final nearby_search_bar:I = 0x7f0202a5

.field public static final no_search_results_bg:I = 0x7f0202a6

.field public static final noti_alarm_icon_repeat_01:I = 0x7f0202a7

.field public static final noti_alarm_icon_repeat_02:I = 0x7f0202a8

.field public static final noti_btn_cancel:I = 0x7f0202a9

.field public static final noti_btn_cancel_f:I = 0x7f0202aa

.field public static final noti_btn_cancel_n:I = 0x7f0202ab

.field public static final noti_btn_cancel_p:I = 0x7f0202ac

.field public static final noti_btn_icon_call:I = 0x7f0202ad

.field public static final noti_btn_icon_reply:I = 0x7f0202ae

.field public static final noti_commute_arrival:I = 0x7f0202af

.field public static final noti_commute_buration:I = 0x7f0202b0

.field public static final noti_daily_bar_pin:I = 0x7f0202b1

.field public static final noti_daily_bg:I = 0x7f0202b2

.field public static final noti_daily_info:I = 0x7f0202b3

.field public static final noti_daily_title_bg:I = 0x7f0202b4

.field public static final noti_daily_title_cancel:I = 0x7f0202b5

.field public static final noti_event_icon_bg:I = 0x7f0202b6

.field public static final noti_event_icon_large_alarm_01:I = 0x7f0202b7

.field public static final noti_event_icon_large_alarm_02:I = 0x7f0202b8

.field public static final noti_event_icon_large_bt_01:I = 0x7f0202b9

.field public static final noti_event_icon_large_bt_02:I = 0x7f0202ba

.field public static final noti_event_icon_large_charge_01:I = 0x7f0202bb

.field public static final noti_event_icon_large_charge_02:I = 0x7f0202bc

.field public static final noti_event_icon_large_message_01:I = 0x7f0202bd

.field public static final noti_event_icon_large_message_02:I = 0x7f0202be

.field public static final noti_event_icon_large_mms_01:I = 0x7f0202bf

.field public static final noti_event_icon_large_mms_02:I = 0x7f0202c0

.field public static final noti_event_icon_large_schedule_01:I = 0x7f0202c1

.field public static final noti_event_icon_large_schedule_02:I = 0x7f0202c2

.field public static final noti_event_icon_large_smart_01:I = 0x7f0202c3

.field public static final noti_event_icon_large_smart_01_a:I = 0x7f0202c4

.field public static final noti_event_icon_large_smart_01_c:I = 0x7f0202c5

.field public static final noti_event_icon_large_smart_01_e:I = 0x7f0202c6

.field public static final noti_event_icon_large_smart_01_f:I = 0x7f0202c7

.field public static final noti_event_icon_large_smart_01_r:I = 0x7f0202c8

.field public static final noti_event_icon_large_smart_02:I = 0x7f0202c9

.field public static final noti_event_icon_large_smart_02_r:I = 0x7f0202ca

.field public static final noti_map_pin_arrival:I = 0x7f0202cb

.field public static final noti_map_pin_flag:I = 0x7f0202cc

.field public static final noti_map_pin_start:I = 0x7f0202cd

.field public static final noti_multicall_cancel_f:I = 0x7f0202ce

.field public static final noti_multicall_cancel_n:I = 0x7f0202cf

.field public static final noti_multicall_cancel_p:I = 0x7f0202d0

.field public static final noti_popup_bar:I = 0x7f0202d1

.field public static final noti_popup_bg_shadow:I = 0x7f0202d2

.field public static final noti_popup_bg_shadow_02:I = 0x7f0202d3

.field public static final noti_sending:I = 0x7f0202d4

.field public static final noti_sending_w:I = 0x7f0202d5

.field public static final page_navigation_n:I = 0x7f0202d6

.field public static final page_navigation_s:I = 0x7f0202d7

.field public static final phone_btn_call_touch:I = 0x7f0202d8

.field public static final phone_call_mask:I = 0x7f0202d9

.field public static final phone_call_thumb_bg:I = 0x7f0202da

.field public static final phone_call_unknown:I = 0x7f0202db

.field public static final pin_bg:I = 0x7f0202dc

.field public static final pin_icon_home:I = 0x7f0202dd

.field public static final pin_icon_office:I = 0x7f0202de

.field public static final pin_icon_schedule:I = 0x7f0202df

.field public static final pin_img:I = 0x7f0202e0

.field public static final pin_mask:I = 0x7f0202e1

.field public static final pin_setting_location:I = 0x7f0202e2

.field public static final pioneer_host_bg:I = 0x7f0202e3

.field public static final pioneer_notjoin_bg:I = 0x7f0202e4

.field public static final poi_icon_baidu:I = 0x7f0202e5

.field public static final poi_icon_daummaps:I = 0x7f0202e6

.field public static final poi_icon_googlenavi:I = 0x7f0202e7

.field public static final poi_icon_ic_launcher:I = 0x7f0202e8

.field public static final poi_icon_navermaps:I = 0x7f0202e9

.field public static final poi_icon_olleh:I = 0x7f0202ea

.field public static final poi_icon_tmaps:I = 0x7f0202eb

.field public static final poi_icon_unavi:I = 0x7f0202ec

.field public static final popup_bg_shadow:I = 0x7f0202ed

.field public static final popup_btn_d:I = 0x7f0202ee

.field public static final popup_btn_default_line:I = 0x7f0202ef

.field public static final popup_btn_df:I = 0x7f0202f0

.field public static final popup_btn_f:I = 0x7f0202f1

.field public static final popup_btn_n:I = 0x7f0202f2

.field public static final popup_btn_p:I = 0x7f0202f3

.field public static final popup_btn_s:I = 0x7f0202f4

.field public static final popup_list_scroll:I = 0x7f0202f5

.field public static final por_bg_multi_menu:I = 0x7f0202f6

.field public static final por_bg_multi_music_full:I = 0x7f0202f7

.field public static final por_bg_multi_music_full_night:I = 0x7f0202f8

.field public static final por_bg_music_full:I = 0x7f0202f9

.field public static final por_bg_music_full_dialog:I = 0x7f0202fa

.field public static final por_bg_music_full_light:I = 0x7f0202fb

.field public static final por_bg_music_full_light_default:I = 0x7f0202fc

.field public static final por_bg_music_full_night:I = 0x7f0202fd

.field public static final por_btn_bg_group_sharing_d:I = 0x7f0202fe

.field public static final por_btn_bg_group_sharing_desti_d:I = 0x7f0202ff

.field public static final por_btn_bg_group_sharing_desti_f:I = 0x7f020300

.field public static final por_btn_bg_group_sharing_desti_n:I = 0x7f020301

.field public static final por_btn_bg_group_sharing_desti_p:I = 0x7f020302

.field public static final por_btn_bg_group_sharing_desti_s:I = 0x7f020303

.field public static final por_btn_bg_group_sharing_f:I = 0x7f020304

.field public static final por_btn_bg_group_sharing_n:I = 0x7f020305

.field public static final por_btn_bg_group_sharing_p:I = 0x7f020306

.field public static final por_btn_bg_group_sharing_s:I = 0x7f020307

.field public static final por_btn_icon_call_n:I = 0x7f020308

.field public static final por_btn_icon_reply_n:I = 0x7f020309

.field public static final por_bubble_bg:I = 0x7f02030a

.field public static final por_full_album:I = 0x7f02030b

.field public static final por_full_album_mask:I = 0x7f02030c

.field public static final por_full_album_shadow:I = 0x7f02030d

.field public static final por_full_next_f:I = 0x7f02030e

.field public static final por_full_next_n:I = 0x7f02030f

.field public static final por_full_next_p:I = 0x7f020310

.field public static final por_full_pause_f:I = 0x7f020311

.field public static final por_full_pause_n:I = 0x7f020312

.field public static final por_full_pause_p:I = 0x7f020313

.field public static final por_full_play_f:I = 0x7f020314

.field public static final por_full_play_n:I = 0x7f020315

.field public static final por_full_play_p:I = 0x7f020316

.field public static final por_full_prev_f:I = 0x7f020317

.field public static final por_full_prev_n:I = 0x7f020318

.field public static final por_full_prev_p:I = 0x7f020319

.field public static final por_full_vol_f:I = 0x7f02031a

.field public static final por_full_vol_mute_f:I = 0x7f02031b

.field public static final por_full_vol_mute_n:I = 0x7f02031c

.field public static final por_full_vol_mute_p:I = 0x7f02031d

.field public static final por_full_vol_n:I = 0x7f02031e

.field public static final por_full_vol_p:I = 0x7f02031f

.field public static final por_icon_share_n:I = 0x7f020320

.field public static final por_image_tutorial_speak:I = 0x7f020321

.field public static final por_multi_album_default:I = 0x7f020322

.field public static final por_multi_album_mask:I = 0x7f020323

.field public static final por_multi_album_shadow:I = 0x7f020324

.field public static final por_multi_close_f:I = 0x7f020325

.field public static final por_multi_close_n:I = 0x7f020326

.field public static final por_multi_close_p:I = 0x7f020327

.field public static final por_nearby_search_bar:I = 0x7f020328

.field public static final por_noti_icon_call_d:I = 0x7f020329

.field public static final por_noti_icon_call_n:I = 0x7f02032a

.field public static final por_noti_icon_reply_d:I = 0x7f02032b

.field public static final por_noti_icon_reply_n:I = 0x7f02032c

.field public static final por_s_voice_multi_eq_bg_01:I = 0x7f02032d

.field public static final por_s_voice_multi_eq_bg_02:I = 0x7f02032e

.field public static final por_s_voice_multi_eq_bg_03:I = 0x7f02032f

.field public static final por_s_voice_multi_mic:I = 0x7f020330

.field public static final por_s_voice_multi_process_bg:I = 0x7f020331

.field public static final por_s_voice_multi_process_timer:I = 0x7f020332

.field public static final por_s_voice_multi_que:I = 0x7f020333

.field public static final por_s_voice_multi_tts_bg:I = 0x7f020334

.field public static final por_shuffle_off_f:I = 0x7f020335

.field public static final por_shuffle_off_n:I = 0x7f020336

.field public static final por_shuffle_off_p:I = 0x7f020337

.field public static final por_shuffle_on_f:I = 0x7f020338

.field public static final por_shuffle_on_n:I = 0x7f020339

.field public static final por_shuffle_on_p:I = 0x7f02033a

.field public static final progress_bar:I = 0x7f02033b

.field public static final progress_bar_update:I = 0x7f02033c

.field public static final progress_bar_warning:I = 0x7f02033d

.field public static final progress_bg:I = 0x7f02033e

.field public static final progress_bg_update:I = 0x7f02033f

.field public static final progress_bg_update_modify:I = 0x7f020340

.field public static final progress_bg_warning:I = 0x7f020341

.field public static final progress_pointer_n:I = 0x7f020342

.field public static final progress_pointer_p:I = 0x7f020343

.field public static final progress_pointer_s:I = 0x7f020344

.field public static final progressbar_poi:I = 0x7f020345

.field public static final quick_panel_music_ff:I = 0x7f020346

.field public static final quick_panel_music_ff_focus:I = 0x7f020347

.field public static final quick_panel_music_ff_press:I = 0x7f020348

.field public static final quick_panel_music_pause:I = 0x7f020349

.field public static final quick_panel_music_pause_focus:I = 0x7f02034a

.field public static final quick_panel_music_pause_press:I = 0x7f02034b

.field public static final quick_panel_music_play:I = 0x7f02034c

.field public static final quick_panel_music_play_focus:I = 0x7f02034d

.field public static final quick_panel_music_play_press:I = 0x7f02034e

.field public static final quick_panel_music_rew:I = 0x7f02034f

.field public static final quick_panel_music_rew_focus:I = 0x7f020350

.field public static final quick_panel_music_rew_press:I = 0x7f020351

.field public static final quick_panel_player_cancel:I = 0x7f020352

.field public static final quick_panel_player_cancel_focus:I = 0x7f020353

.field public static final quick_panel_player_cancel_press:I = 0x7f020354

.field public static final quotes_btn_end:I = 0x7f020355

.field public static final quotes_btn_start:I = 0x7f020356

.field public static final quotes_ex_end:I = 0x7f020357

.field public static final quotes_ex_start:I = 0x7f020358

.field public static final quotes_hi_home_end:I = 0x7f020359

.field public static final quotes_hi_home_end_night:I = 0x7f02035a

.field public static final quotes_hi_home_start:I = 0x7f02035b

.field public static final quotes_hi_home_start_night:I = 0x7f02035c

.field public static final quotes_hi_list_end_night:I = 0x7f02035d

.field public static final quotes_hi_list_start_night:I = 0x7f02035e

.field public static final quotes_home_sub_end:I = 0x7f02035f

.field public static final quotes_home_sub_end_night:I = 0x7f020360

.field public static final quotes_home_sub_start:I = 0x7f020361

.field public static final quotes_home_sub_start_night:I = 0x7f020362

.field public static final quotes_list_big_end:I = 0x7f020363

.field public static final quotes_list_big_end_night:I = 0x7f020364

.field public static final quotes_list_big_start:I = 0x7f020365

.field public static final quotes_list_big_start_night:I = 0x7f020366

.field public static final quotes_list_small_end:I = 0x7f020367

.field public static final quotes_list_small_end_night:I = 0x7f020368

.field public static final quotes_list_small_start:I = 0x7f020369

.field public static final quotes_list_small_start_night:I = 0x7f02036a

.field public static final radio_off_d:I = 0x7f02036b

.field public static final radio_off_df:I = 0x7f02036c

.field public static final radio_off_f:I = 0x7f02036d

.field public static final radio_off_n:I = 0x7f02036e

.field public static final radio_off_p:I = 0x7f02036f

.field public static final radio_on_d:I = 0x7f020370

.field public static final radio_on_df:I = 0x7f020371

.field public static final radio_on_f:I = 0x7f020372

.field public static final radio_on_n:I = 0x7f020373

.field public static final radio_on_off:I = 0x7f020374

.field public static final radio_on_p:I = 0x7f020375

.field public static final rounded_cell:I = 0x7f020376

.field public static final rounded_cell_focused:I = 0x7f020377

.field public static final s_voice_eq_bg_01:I = 0x7f020378

.field public static final s_voice_eq_bg_02:I = 0x7f020379

.field public static final s_voice_mic:I = 0x7f02037a

.field public static final s_voice_multi_eq_bg_01:I = 0x7f02037b

.field public static final s_voice_multi_eq_bg_02:I = 0x7f02037c

.field public static final s_voice_multi_eq_bg_03:I = 0x7f02037d

.field public static final s_voice_multi_mic:I = 0x7f02037e

.field public static final s_voice_multi_process_bg:I = 0x7f02037f

.field public static final s_voice_multi_process_timer:I = 0x7f020380

.field public static final s_voice_multi_que:I = 0x7f020381

.field public static final s_voice_multi_tts_bg:I = 0x7f020382

.field public static final s_voice_process_bg:I = 0x7f020383

.field public static final s_voice_process_timer:I = 0x7f020384

.field public static final s_voice_que:I = 0x7f020385

.field public static final s_voice_que_bg_01:I = 0x7f020386

.field public static final s_voice_que_bg_02:I = 0x7f020387

.field public static final s_voice_que_bg_03:I = 0x7f020388

.field public static final s_voice_tts_bar:I = 0x7f020389

.field public static final s_voice_tts_bar_bg:I = 0x7f02038a

.field public static final s_voice_tts_bar_mask:I = 0x7f02038b

.field public static final s_voice_tts_bg:I = 0x7f02038c

.field public static final scrollbar_register:I = 0x7f02038d

.field public static final searchfiled_bg:I = 0x7f02038e

.field public static final seekbar_progress:I = 0x7f02038f

.field public static final seekbar_thumb:I = 0x7f020390

.field public static final seekbar_volume_progres:I = 0x7f020391

.field public static final selector_action_bar_text:I = 0x7f020392

.field public static final selector_btn_basic_quotation:I = 0x7f020393

.field public static final selector_btn_bg_basic:I = 0x7f020394

.field public static final selector_btn_bg_default_02:I = 0x7f020395

.field public static final selector_btn_bg_group_under:I = 0x7f020396

.field public static final selector_btn_bg_group_under_left:I = 0x7f020397

.field public static final selector_btn_bg_group_under_right:I = 0x7f020398

.field public static final selector_btn_bg_noti:I = 0x7f020399

.field public static final selector_btn_bg_sub:I = 0x7f02039a

.field public static final selector_btn_icon_group_desti:I = 0x7f02039b

.field public static final selector_btn_icon_group_list_location:I = 0x7f02039c

.field public static final selector_btn_icon_group_location:I = 0x7f02039d

.field public static final selector_btn_icon_group_map_location:I = 0x7f02039e

.field public static final selector_btn_location_back:I = 0x7f02039f

.field public static final selector_btn_location_info_next:I = 0x7f0203a0

.field public static final selector_btn_location_info_pre:I = 0x7f0203a1

.field public static final selector_btn_message_read_next:I = 0x7f0203a2

.field public static final selector_btn_message_read_pre:I = 0x7f0203a3

.field public static final selector_btn_my_location_map_button:I = 0x7f0203a4

.field public static final selector_btn_noti_tts:I = 0x7f0203a5

.field public static final selector_btn_noti_tts_off:I = 0x7f0203a6

.field public static final selector_btn_por_bubble_bg:I = 0x7f0203a7

.field public static final selector_btn_register_default:I = 0x7f0203a8

.field public static final selector_btn_reinvite:I = 0x7f0203a9

.field public static final selector_btn_route_default:I = 0x7f0203aa

.field public static final selector_btn_sub_icon_tts:I = 0x7f0203ab

.field public static final selector_btn_text_noti:I = 0x7f0203ac

.field public static final selector_btn_tts_icon:I = 0x7f0203ad

.field public static final selector_checkbox:I = 0x7f0203ae

.field public static final selector_color_comnotif_day:I = 0x7f0203af

.field public static final selector_color_comnotif_time:I = 0x7f0203b0

.field public static final selector_color_register_button:I = 0x7f0203b1

.field public static final selector_color_rejectmessage:I = 0x7f0203b2

.field public static final selector_drawer_subtitle_bg:I = 0x7f0203b3

.field public static final selector_full_vol_mute:I = 0x7f0203b4

.field public static final selector_full_vol_mute_por:I = 0x7f0203b5

.field public static final selector_full_vol_popup_mute:I = 0x7f0203b6

.field public static final selector_group_location_sharing_button_text:I = 0x7f0203b7

.field public static final selector_help_list_group_indicator:I = 0x7f0203b8

.field public static final selector_icon_next:I = 0x7f0203b9

.field public static final selector_inputfield:I = 0x7f0203ba

.field public static final selector_inputfield_cancel:I = 0x7f0203bb

.field public static final selector_inputfield_cancel_bg:I = 0x7f0203bc

.field public static final selector_inputfield_white:I = 0x7f0203bd

.field public static final selector_list_mark_botton_cluster:I = 0x7f0203be

.field public static final selector_list_mark_center_cluster:I = 0x7f0203bf

.field public static final selector_list_mark_top_cluster:I = 0x7f0203c0

.field public static final selector_mini_expand:I = 0x7f0203c1

.field public static final selector_multi_close:I = 0x7f0203c2

.field public static final selector_multi_home_btn:I = 0x7f0203c3

.field public static final selector_multi_next:I = 0x7f0203c4

.field public static final selector_multi_pause:I = 0x7f0203c5

.field public static final selector_multi_play:I = 0x7f0203c6

.field public static final selector_multi_prev:I = 0x7f0203c7

.field public static final selector_next_btn_msg_read:I = 0x7f0203c8

.field public static final selector_next_btn_player:I = 0x7f0203c9

.field public static final selector_next_btn_player_night:I = 0x7f0203ca

.field public static final selector_next_btn_player_por:I = 0x7f0203cb

.field public static final selector_noti_btn_cancel:I = 0x7f0203cc

.field public static final selector_noti_multicall_cancel:I = 0x7f0203cd

.field public static final selector_pause_btn_player:I = 0x7f0203ce

.field public static final selector_play_btn_player:I = 0x7f0203cf

.field public static final selector_play_btn_player_por:I = 0x7f0203d0

.field public static final selector_popup_btn:I = 0x7f0203d1

.field public static final selector_por_multi_close:I = 0x7f0203d2

.field public static final selector_por_multi_next:I = 0x7f0203d3

.field public static final selector_por_multi_play:I = 0x7f0203d4

.field public static final selector_por_multi_prev:I = 0x7f0203d5

.field public static final selector_pref_radio_button:I = 0x7f0203d6

.field public static final selector_prev_btn_msg_read:I = 0x7f0203d7

.field public static final selector_prev_btn_player:I = 0x7f0203d8

.field public static final selector_prev_btn_player_night:I = 0x7f0203d9

.field public static final selector_prev_btn_player_por:I = 0x7f0203da

.field public static final selector_quick_cancel_btn_player:I = 0x7f0203db

.field public static final selector_quick_next_btn_player:I = 0x7f0203dc

.field public static final selector_quick_pause_btn_player:I = 0x7f0203dd

.field public static final selector_quick_play_btn_player:I = 0x7f0203de

.field public static final selector_quick_prev_btn_player:I = 0x7f0203df

.field public static final selector_searchfield:I = 0x7f0203e0

.field public static final selector_seekbar_ball:I = 0x7f0203e1

.field public static final selector_seekbar_volume_ball:I = 0x7f0203e2

.field public static final selector_settings_language_item:I = 0x7f0203e3

.field public static final selector_settings_list_bg:I = 0x7f0203e4

.field public static final selector_settings_list_textcolor_summary:I = 0x7f0203e5

.field public static final selector_settings_list_textcolor_title:I = 0x7f0203e6

.field public static final selector_settings_next_textcolor:I = 0x7f0203e7

.field public static final selector_shrink_above_btn_player:I = 0x7f0203e8

.field public static final selector_shrink_below_btn_player:I = 0x7f0203e9

.field public static final selector_shuffle_btn_player:I = 0x7f0203ea

.field public static final selector_shuffle_btn_player_por:I = 0x7f0203eb

.field public static final selector_switch_bg:I = 0x7f0203ec

.field public static final selector_switch_off_bg:I = 0x7f0203ed

.field public static final selector_switch_off_icon:I = 0x7f0203ee

.field public static final selector_switch_on_bg:I = 0x7f0203ef

.field public static final selector_switch_on_icon:I = 0x7f0203f0

.field public static final selector_switch_thumb:I = 0x7f0203f1

.field public static final selector_text_add:I = 0x7f0203f2

.field public static final selector_tts_bg_btn_msg_read:I = 0x7f0203f3

.field public static final selector_volume_btn_player:I = 0x7f0203f4

.field public static final selector_volume_btn_player_por:I = 0x7f0203f5

.field public static final setting_input_alarm:I = 0x7f0203f6

.field public static final setup_btn_arrow:I = 0x7f0203f7

.field public static final setup_btn_arrow_dim:I = 0x7f0203f8

.field public static final setup_btn_arrow_press:I = 0x7f0203f9

.field public static final shuffle_off_f:I = 0x7f0203fa

.field public static final shuffle_off_n:I = 0x7f0203fb

.field public static final shuffle_off_p:I = 0x7f0203fc

.field public static final shuffle_on_f:I = 0x7f0203fd

.field public static final shuffle_on_n:I = 0x7f0203fe

.field public static final shuffle_on_p:I = 0x7f0203ff

.field public static final switch_bg_d:I = 0x7f020400

.field public static final switch_bg_f:I = 0x7f020401

.field public static final switch_bg_n:I = 0x7f020402

.field public static final switch_off_bg_d:I = 0x7f020403

.field public static final switch_off_bg_n:I = 0x7f020404

.field public static final switch_off_bg_p:I = 0x7f020405

.field public static final switch_off_d:I = 0x7f020406

.field public static final switch_off_icon_d:I = 0x7f020407

.field public static final switch_off_icon_n:I = 0x7f020408

.field public static final switch_off_icon_p:I = 0x7f020409

.field public static final switch_off_n:I = 0x7f02040a

.field public static final switch_off_p:I = 0x7f02040b

.field public static final switch_on_bg_d:I = 0x7f02040c

.field public static final switch_on_bg_n:I = 0x7f02040d

.field public static final switch_on_bg_p:I = 0x7f02040e

.field public static final switch_on_d:I = 0x7f02040f

.field public static final switch_on_icon_d:I = 0x7f020410

.field public static final switch_on_icon_n:I = 0x7f020411

.field public static final switch_on_icon_p:I = 0x7f020412

.field public static final switch_on_n:I = 0x7f020413

.field public static final switch_on_p:I = 0x7f020414

.field public static final switch_thumb:I = 0x7f020415

.field public static final title_bg:I = 0x7f020416

.field public static final title_bg_shadow:I = 0x7f020417

.field public static final tos_text_field:I = 0x7f020418

.field public static final tw_ab_bottom_transparent_dark:I = 0x7f020419

.field public static final tw_background_light:I = 0x7f02041a

.field public static final tw_btn_next_default_holo_dark:I = 0x7f02041b

.field public static final tw_btn_previous_default_holo_dark:I = 0x7f02041c

.field public static final tw_btn_radio_off_holo_dark:I = 0x7f02041d

.field public static final tw_btn_radio_on_holo_dark:I = 0x7f02041e

.field public static final tw_buttonbarbutton_selector_default_holo_dark:I = 0x7f02041f

.field public static final tw_buttonbarbutton_selector_default_line:I = 0x7f020420

.field public static final tw_dialog_bottom_holo_dark:I = 0x7f020421

.field public static final tw_dialog_bottom_shadow_holo_dark:I = 0x7f020422

.field public static final tw_dialog_middle_holo_dark:I = 0x7f020423

.field public static final tw_dialog_top_holo_dark:I = 0x7f020424

.field public static final tw_divider_ab_holo_dark:I = 0x7f020425

.field public static final tw_divider_option_popup_holo_dark:I = 0x7f020426

.field public static final tw_drawer_bg_holo_dark:I = 0x7f020427

.field public static final tw_drawer_list_line_holo_dark:I = 0x7f020428

.field public static final tw_expander_list_bg_holo_light:I = 0x7f020429

.field public static final tw_ic_ab_back_holo_dark:I = 0x7f02042a

.field public static final tw_list_disabled_focused_holo_dark:I = 0x7f02042b

.field public static final tw_list_disabled_holo_dark:I = 0x7f02042c

.field public static final tw_list_divider_holo_dark:I = 0x7f02042d

.field public static final tw_list_focused_holo_dark:I = 0x7f02042e

.field public static final tw_list_icon_create_disabled_focused_holo_dark:I = 0x7f02042f

.field public static final tw_list_icon_create_disabled_holo_dark:I = 0x7f020430

.field public static final tw_list_icon_create_focused_holo_dark:I = 0x7f020431

.field public static final tw_list_icon_create_holo_dark:I = 0x7f020432

.field public static final tw_list_icon_create_pressed_holo_dark:I = 0x7f020433

.field public static final tw_list_icon_minus_disabled_focused_holo_dark:I = 0x7f020434

.field public static final tw_list_icon_minus_disabled_holo_dark:I = 0x7f020435

.field public static final tw_list_icon_minus_focused_holo_dark:I = 0x7f020436

.field public static final tw_list_icon_minus_holo_dark:I = 0x7f020437

.field public static final tw_list_icon_minus_pressed_holo_dark:I = 0x7f020438

.field public static final tw_list_pressed_holo_dark:I = 0x7f020439

.field public static final tw_list_section_divider_holo_dark:I = 0x7f02043a

.field public static final tw_list_selected_holo_dark:I = 0x7f02043b

.field public static final tw_menu_popup_panel_holo_dark:I = 0x7f02043c

.field public static final tw_overscroll_glow_holo:I = 0x7f02043d

.field public static final tw_progress_bg_holo_dark:I = 0x7f02043e

.field public static final tw_progress_primary_holo_dark:I = 0x7f02043f

.field public static final tw_progress_secondary_holo_dark:I = 0x7f020440

.field public static final tw_searchfiled_background_holo_dark:I = 0x7f020441

.field public static final tw_textfield_search_default_holo_dark:I = 0x7f020442

.field public static final tw_textfield_search_selected_holo_dark:I = 0x7f020443

.field public static final tw_toast_frame_holo_dark:I = 0x7f020444

.field public static final tw_widget_progressbar_holo_dark:I = 0x7f020445

.field public static final tw_widget_progressbar_large_holo_dark:I = 0x7f020446

.field public static final update_progress:I = 0x7f020447

.field public static final volume_bg:I = 0x7f020448

.field public static final volume_line:I = 0x7f020449

.field public static final warning_progress:I = 0x7f02044a

.field public static final winset_next_d:I = 0x7f02044b

.field public static final winset_next_f:I = 0x7f02044c

.field public static final winset_next_n:I = 0x7f02044d

.field public static final winset_next_p:I = 0x7f02044e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
