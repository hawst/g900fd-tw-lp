.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->initNumberLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 1618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 1645
    const-string/jumbo v0, "PhoneActivity"

    const-string/jumbo v1, "NumberLayout - setAutoShrink : false n true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1646
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$24(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 1647
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$24(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 1648
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    const/4 v2, 0x0

    .line 1630
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberIsLoaded:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$39(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1631
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$40(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 1632
    if-nez p1, :cond_1

    .line 1633
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$41(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$38(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    move-result-object v1

    .line 1634
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->getInitialPosition()I

    move-result v1

    .line 1633
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1635
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$37(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1641
    :cond_0
    :goto_0
    return-void

    .line 1637
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$37(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 1638
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$38(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 1637
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1622
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$37(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 1623
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$38(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 1622
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1625
    return-void
.end method
