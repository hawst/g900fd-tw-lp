.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getNumberListItemOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 1662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 1667
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1668
    const-string/jumbo v1, "PhoneActivity"

    const-string/jumbo v2, "Activity is finishing now!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1681
    :goto_0
    return-void

    .line 1672
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$37(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 1673
    mul-int/lit8 v2, v2, 0x4

    .line 1672
    add-int/2addr v1, v2

    add-int/lit8 v7, v1, -0x1

    .line 1675
    .local v7, "currentItem":I
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberContactId:J
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$42(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)J

    move-result-wide v1

    .line 1676
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserDisplayName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$43(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/lang/String;

    move-result-object v3

    .line 1677
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$44(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/ArrayList;

    move-result-object v6

    move-object v5, v4

    .line 1675
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1679
    .local v0, "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserIndex:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$45(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v2

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->startPrepareDialActivity(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;I)V
    invoke-static {v1, v2, v0, v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$46(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;I)V

    goto :goto_0
.end method
