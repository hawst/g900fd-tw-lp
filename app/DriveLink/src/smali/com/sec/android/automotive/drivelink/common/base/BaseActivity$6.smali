.class Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$6;
.super Landroid/content/BroadcastReceiver;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setCarModeChangeToOffReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 581
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 584
    .line 585
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 586
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.carmodeoff.finish"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "setCarModeChangeToOffReceiver 1"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->CarAppFinishFunc()V

    .line 591
    :cond_0
    return-void
.end method
