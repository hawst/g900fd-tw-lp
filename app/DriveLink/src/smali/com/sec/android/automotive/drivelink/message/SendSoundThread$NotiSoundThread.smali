.class Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;
.super Ljava/lang/Thread;
.source "SendSoundThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/SendSoundThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NotiSoundThread"
.end annotation


# instance fields
.field public hundredmillis:I

.field private mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

.field public sampleId:I

.field public soundPool:Landroid/media/SoundPool;

.field public status:I

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

.field private timeoutCnt:I


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 57
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 58
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    .line 59
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->timeoutCnt:I

    .line 60
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    .line 61
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->sampleId:I

    .line 62
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->status:I

    .line 63
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->hundredmillis:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 76
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->sampleId:I

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v8

    .line 80
    .local v8, "result":I
    :goto_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->timeoutCnt:I

    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->hundredmillis:I

    if-lt v0, v1, :cond_1

    .line 85
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    # getter for: Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->access$0(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;)Landroid/media/SoundPool;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    # getter for: Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->access$0(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;)Landroid/media/SoundPool;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    iget v1, v1, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mSoundID:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->unload(I)Z

    .line 87
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    # getter for: Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->access$0(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;)Landroid/media/SoundPool;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    # getter for: Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->access$0(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;)Landroid/media/SoundPool;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->access$1(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;Landroid/media/SoundPool;)V

    .line 95
    .end local v8    # "result":I
    :cond_0
    :goto_1
    return-void

    .line 81
    .restart local v8    # "result":I
    :cond_1
    const-wide/16 v0, 0x64

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->sleep(J)V

    .line 82
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->timeoutCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->timeoutCnt:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    .end local v8    # "result":I
    :catch_0
    move-exception v7

    .line 93
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public setOnSoundFinishedListener(Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    .line 67
    return-void
.end method
