.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestSearchAllMusic.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMusicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchKeyword:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "searchKeyword"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mContext:Landroid/content/Context;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mSearchKeyword:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mMusicList:Ljava/util/ArrayList;

    .line 22
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mContext:Landroid/content/Context;

    .line 23
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mSearchKeyword:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mContext:Landroid/content/Context;

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mSearchKeyword:Ljava/lang/String;

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mMusicList:Ljava/util/ArrayList;

    .line 48
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 30
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mContext:Landroid/content/Context;

    .line 31
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mSearchKeyword:Ljava/lang/String;

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getAllMusicSearchList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mMusicList:Ljava/util/ArrayList;

    .line 32
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    move-result-object v0

    .line 38
    .local v0, "musicListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
    if-eqz v0, :cond_0

    .line 39
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;->mMusicList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestSearchAllMusic(Ljava/util/ArrayList;)V

    .line 41
    :cond_0
    return-void
.end method
