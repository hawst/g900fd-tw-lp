.class public Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "MultiWindowActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$VoiceResumeHandler;
    }
.end annotation


# static fields
.field public static final ACTION_EXTRA_STARTNAVI:Ljava/lang/String; = "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI"

.field public static final ACTION_EXTRA_STARTNAVI_DATA:Ljava/lang/String; = "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI_DATA"

.field public static final ACTION_EXTRA_TTS:Ljava/lang/String; = "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_TTS"

.field public static final ACTION_START_MAP:Ljava/lang/String; = "com.sec.android.automotive.drivelink.multiwindow.ACTION_START_MAP"

.field public static final ACTION_START_NAVI:Ljava/lang/String; = "com.sec.android.automotive.drivelink.multiwindow.ACTION_START_NAVI"

.field public static final LAT_ORIGIN:Ljava/lang/String; = "lat"

.field public static final LNG_ORIGIN:Ljava/lang/String; = "lng"

.field private static final MULTIWINDOW_STATE_PAUSE_DELAY:I = 0x12c

.field private static final MULTI_INDICATOR_TAG:Ljava/lang/String; = "multi_indicator"

.field private static final TAG:Ljava/lang/String; = "[MultiWindowActivity]"

.field private static bPerformanceTest:Z = false

.field private static flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener; = null

.field private static isMultiWindowRunning:Z = false

.field private static isMultiWindowVisible:Z = false

.field public static final isSupportFixedSplit:Z = true

.field private static volatile resumeStarted:Z

.field private static sFlowId:Ljava/lang/String;

.field private static volatile setStateStarted:Z


# instance fields
.field private indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

.field private isCarAppFinishing:Z

.field private mActivityStateChangeListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;

.field private mContext:Landroid/content/Context;

.field private mDesNaviTTS:Ljava/lang/String;

.field mFlowIdIf:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

.field mMultiHomeListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 111
    sput-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->bPerformanceTest:Z

    .line 121
    sput-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowRunning:Z

    .line 127
    sput-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->sFlowId:Ljava/lang/String;

    .line 134
    sput-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    .line 1004
    sput-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->resumeStarted:Z

    .line 1032
    sput-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setStateStarted:Z

    .line 1058
    sput-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowVisible:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 145
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    .line 149
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mContext:Landroid/content/Context;

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isCarAppFinishing:Z

    .line 158
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mDesNaviTTS:Ljava/lang/String;

    .line 608
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiHomeListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    .line 735
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mFlowIdIf:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

    .line 937
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mActivityStateChangeListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;

    .line 60
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Z
    .locals 1

    .prologue
    .line 503
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isVisibleActivity()Z

    move-result v0

    return v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;)V
    .locals 0

    .prologue
    .line 134
    sput-object p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    return-void
.end method

.method static synthetic access$11(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 127
    sput-object p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->sFlowId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$12()Z
    .locals 1

    .prologue
    .line 1004
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->resumeStarted:Z

    return v0
.end method

.method static synthetic access$13(Z)V
    .locals 0

    .prologue
    .line 1004
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->resumeStarted:Z

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V
    .locals 0

    .prologue
    .line 548
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->dismissNotiHomeMenu()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V
    .locals 0

    .prologue
    .line 561
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->clickPhone()V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V
    .locals 0

    .prologue
    .line 570
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->clickMessage()V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V
    .locals 0

    .prologue
    .line 579
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->clickLocation()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V
    .locals 0

    .prologue
    .line 588
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->clickMusic()V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 729
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setVoiceFlowId(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$8()Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setActivityFlowID(Ljava/lang/String;)V

    return-void
.end method

.method private addIndicatorView()V
    .locals 5

    .prologue
    .line 510
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "addIndicatorView is called."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    if-nez v2, :cond_1

    .line 512
    new-instance v2, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    .line 513
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "indicator is null. create new indicator"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getInstance()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiHomeListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->registerListener(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;)V

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 525
    .local v1, "manager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 526
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    const-string/jumbo v2, "multi_indicator"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_2

    .line 527
    const v2, 0x7f0900c8

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    const-string/jumbo v4, "multi_indicator"

    invoke-virtual {v0, v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 531
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 532
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "IndicatorView is added."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    return-void

    .line 515
    .end local v0    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .end local v1    # "manager":Landroid/support/v4/app/FragmentManager;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 516
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "indicator is not null and has parent."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->removeIndicatorView()V

    .line 518
    new-instance v2, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    goto :goto_0

    .line 529
    .restart local v0    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .restart local v1    # "manager":Landroid/support/v4/app/FragmentManager;
    :cond_2
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "indicator view is already added!!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private clickLocation()V
    .locals 2

    .prologue
    .line 580
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->moveOutOfHomeOverlayService(Landroid/content/Context;)V

    .line 581
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 582
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 583
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 584
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "DM_LOCATION"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlowForMulti(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 586
    return-void
.end method

.method private clickMessage()V
    .locals 2

    .prologue
    .line 571
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->moveOutOfHomeOverlayService(Landroid/content/Context;)V

    .line 572
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 573
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 574
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 575
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "DM_SMS_CONTACT"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlowForMulti(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 577
    return-void
.end method

.method private clickMusic()V
    .locals 5

    .prologue
    .line 589
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-nez v2, :cond_1

    .line 606
    :cond_0
    :goto_0
    return-void

    .line 592
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 593
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 594
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setCheckListSettingMusicList()Z

    move-result v1

    .line 595
    .local v1, "value":Z
    const-string/jumbo v2, "[MultiWindowActivity]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setCheckMusicList value + "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    if-eqz v1, :cond_0

    .line 599
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->moveOutOfHomeOverlayService(Landroid/content/Context;)V

    .line 600
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setCallMultiwindow(Z)V

    .line 601
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 602
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 603
    const v3, 0x7f0a0032

    .line 602
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 604
    const-string/jumbo v2, "DM_MUSIC_PLAYER"

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlowForMulti(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0
.end method

.method private clickPhone()V
    .locals 2

    .prologue
    .line 562
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->moveOutOfHomeOverlayService(Landroid/content/Context;)V

    .line 563
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 564
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 565
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 566
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "DM_DIAL_CONTACT"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlowForMulti(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 568
    return-void
.end method

.method private dismissNotiHomeMenu()V
    .locals 2

    .prologue
    .line 549
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "dismissNotiHomeMenu"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->requestDismissNotification(I)V

    .line 551
    return-void
.end method

.method private dismissNotiMusic()V
    .locals 2

    .prologue
    .line 554
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "dismissNotiMusic"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->requestDismissNotification(I)V

    .line 556
    return-void
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v6, 0x0

    .line 228
    if-nez p1, :cond_1

    .line 229
    const-string/jumbo v3, "[MultiWindowActivity]"

    const-string/jumbo v4, "intent is null"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    const-string/jumbo v3, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 235
    .local v0, "naviBundle":Landroid/os/Bundle;
    if-eqz v0, :cond_4

    .line 236
    const-string/jumbo v3, "[MultiWindowActivity]"

    const-string/jumbo v4, "Navibundle != null"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    const-string/jumbo v3, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI_DATA"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 240
    .local v2, "nextNaviData":Landroid/os/Parcelable;
    if-eqz v2, :cond_3

    .line 241
    const-string/jumbo v3, "[MultiWindowActivity]"

    const-string/jumbo v4, "nextNaviData != null"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newNavigationMap()Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v1

    .line 244
    .local v1, "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    if-eqz v1, :cond_3

    .line 246
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->isOriginNeeded()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 247
    const-string/jumbo v3, "lat"

    invoke-virtual {p1, v3, v6, v7}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v3

    cmpl-double v3, v3, v6

    if-eqz v3, :cond_2

    .line 248
    const-string/jumbo v3, "lng"

    invoke-virtual {p1, v3, v6, v7}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v3

    cmpl-double v3, v3, v6

    if-eqz v3, :cond_2

    .line 250
    const-string/jumbo v3, "lat"

    invoke-virtual {p1, v3, v6, v7}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v3

    .line 251
    const-string/jumbo v5, "lng"

    invoke-virtual {p1, v5, v6, v7}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v5

    .line 249
    invoke-virtual {v1, v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->setOrigin(DD)V

    .line 254
    :cond_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mContext:Landroid/content/Context;

    .line 255
    check-cast v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .line 254
    .end local v2    # "nextNaviData":Landroid/os/Parcelable;
    invoke-virtual {v1, v3, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startMultiNavi(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)Z

    .line 256
    const-string/jumbo v3, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 261
    .end local v1    # "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    :cond_3
    const/4 v0, 0x0

    .line 264
    :cond_4
    const-string/jumbo v3, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_TTS"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 265
    const-string/jumbo v3, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_TTS"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mDesNaviTTS:Ljava/lang/String;

    .line 266
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mDesNaviTTS:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mDesNaviTTS:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 267
    const-string/jumbo v3, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_TTS"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static isMultiWindowRunning()Z
    .locals 3

    .prologue
    .line 1078
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "isMultiWindowRunning:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1079
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowRunning:Z

    return v0
.end method

.method public static isMultiWindowVisible()Z
    .locals 3

    .prologue
    .line 1066
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "isMultiWindowVisible:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowVisible:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1067
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowVisible:Z

    return v0
.end method

.method private isVisibleActivity()Z
    .locals 4

    .prologue
    .line 504
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isResumed()Z

    move-result v0

    .line 505
    .local v0, "isVisible":Z
    const-string/jumbo v1, "[MultiWindowActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "is visible activity:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    return v0
.end method

.method private pauseHomeOverlayService()V
    .locals 2

    .prologue
    .line 917
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isCarAppFinishing:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->isAppInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 925
    :cond_0
    :goto_0
    return-void

    .line 921
    :cond_1
    new-instance v0, Landroid/content/Intent;

    .line 922
    const-class v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    .line 921
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 923
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_PAUSE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 924
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private playTTS(Ljava/lang/String;)V
    .locals 3
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 278
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279
    :cond_0
    const-string/jumbo v0, "[MultiWindowActivity]"

    .line 280
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "WARNING!! TTS String is NULL or empty. return this method. : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 281
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 280
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 279
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :goto_0
    return-void

    .line 284
    :cond_1
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "playTTS :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private removeIndicatorView()V
    .locals 4

    .prologue
    .line 536
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "removeIndicatorView is called."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    if-eqz v2, :cond_0

    .line 538
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 540
    .local v1, "manager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 541
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 542
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 543
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    .line 544
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "IndicatorView is removed."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    .end local v0    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .end local v1    # "manager":Landroid/support/v4/app/FragmentManager;
    :cond_0
    return-void
.end method

.method private requestDismissNotification(I)V
    .locals 4
    .param p1, "notiType"    # I

    .prologue
    .line 893
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isCarAppFinishing:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->isAppInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 903
    :cond_0
    :goto_0
    return-void

    .line 896
    :cond_1
    const-string/jumbo v1, "[MultiWindowActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "requestDismissNotification:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    new-instance v0, Landroid/content/Intent;

    .line 898
    const-class v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    .line 897
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 899
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_DISMISS_VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 900
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_EXTRA_DISMISS_VIEW_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 902
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private requestRemoveView()V
    .locals 3

    .prologue
    .line 906
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isCarAppFinishing:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->isAppInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 914
    :cond_0
    :goto_0
    return-void

    .line 909
    :cond_1
    const-string/jumbo v1, "[MultiWindowActivity]"

    const-string/jumbo v2, "requestRemoveView"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    new-instance v0, Landroid/content/Intent;

    .line 911
    const-class v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    .line 910
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 912
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_REMOVE_VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 913
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private resumeHomeOverlayService()V
    .locals 2

    .prologue
    .line 928
    new-instance v0, Landroid/content/Intent;

    .line 929
    const-class v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    .line 928
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 930
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_RESUME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 931
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 932
    return-void
.end method

.method public static setFlowCommandListener(Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;)V
    .locals 0
    .param p0, "listener"    # Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    .prologue
    .line 760
    sput-object p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    .line 761
    return-void
.end method

.method private setHomeOverlayServiceFlowIdIf(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;)V
    .locals 0
    .param p1, "flowIf"    # Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

    .prologue
    .line 888
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->setFlowIdInterface(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;)V

    .line 890
    return-void
.end method

.method private static setMultiWindowVisible(Z)V
    .locals 3
    .param p0, "visible"    # Z

    .prologue
    .line 1061
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setMultiWindowVisible:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1062
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowVisible:Z

    .line 1063
    return-void
.end method

.method private setVoiceFlowId(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowId"    # Ljava/lang/String;

    .prologue
    .line 730
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setVoiceFlowId :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    sput-object p1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->sFlowId:Ljava/lang/String;

    .line 732
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setActivityFlowID(Ljava/lang/String;)V

    .line 733
    return-void
.end method

.method private startHomeOverlayService()V
    .locals 2

    .prologue
    .line 876
    new-instance v0, Landroid/content/Intent;

    .line 877
    const-class v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    .line 876
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 878
    .local v0, "service":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 879
    return-void
.end method

.method private stopHomeOverlayService()V
    .locals 2

    .prologue
    .line 882
    new-instance v0, Landroid/content/Intent;

    .line 883
    const-class v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    .line 882
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 884
    .local v0, "service":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->stopService(Landroid/content/Intent;)Z

    .line 885
    return-void
.end method


# virtual methods
.method protected CarAppFinishFunc()V
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isCarAppFinishing:Z

    .line 294
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->CarAppFinishFunc()V

    .line 295
    return-void
.end method

.method protected autoMultiModeSize()Z
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 488
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onBackPressed:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoMain(Z)V

    .line 490
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onBackPressed()V

    .line 491
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 162
    const-string/jumbo v1, "[MultiWindowActivity]"

    const-string/jumbo v2, "onCreate"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 164
    const v1, 0x7f03001d

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setContentView(I)V

    .line 165
    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mContext:Landroid/content/Context;

    .line 172
    sget-boolean v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->bPerformanceTest:Z

    if-nez v1, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 173
    invoke-static {p0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setCurrentActiviy(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;Landroid/content/Context;)V

    .line 175
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/automotive/drivelink/DLApplication;->setCurrentActivity(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V

    .line 176
    const-string/jumbo v1, "DM_RETURN_MAIN"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 180
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v1, :cond_1

    .line 181
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 182
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mActivityStateChangeListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 186
    :cond_1
    sget-boolean v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowRunning:Z

    if-nez v1, :cond_2

    .line 187
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->startHomeOverlayService()V

    .line 188
    sput-boolean v3, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowRunning:Z

    .line 189
    const-string/jumbo v1, "[MultiWindowActivity]"

    const-string/jumbo v2, "Add Voice Listener"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :cond_2
    if-nez p1, :cond_3

    .line 194
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->addIndicatorView()V

    .line 195
    const-string/jumbo v1, "[MultiWindowActivity]"

    const-string/jumbo v2, "SET RESUME :: when Create"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-static {p0, v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiWindowState(Landroid/content/Context;I)V

    .line 215
    :cond_3
    const-string/jumbo v1, "DM_MAIN"

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setActivityFlowID(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 219
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_4

    .line 220
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->handleIntent(Landroid/content/Intent;)V

    .line 222
    :cond_4
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 451
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 454
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "SET PAUSE :: when Destroy"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    invoke-static {p0, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiWindowState(Landroid/content/Context;I)V

    .line 459
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->bPerformanceTest:Z

    if-eqz v0, :cond_0

    .line 461
    const-string/jumbo v0, "DM_RETURN_MAIN"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 463
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->stopHomeOverlayService()V

    .line 464
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_1

    .line 465
    const-string/jumbo v0, "[MultiWindowActivity]"

    .line 466
    const-string/jumbo v1, "call normalWindow() change from multi-window to nomarlWindow"

    .line 465
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->normalWindow()V

    .line 469
    :cond_1
    sput-boolean v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowRunning:Z

    .line 471
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 472
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setCallMultiwindow(Z)V

    .line 475
    :cond_2
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->sFlowId:Ljava/lang/String;

    .line 483
    :cond_3
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 484
    return-void
.end method

.method public onFlowCommandCall(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 851
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 852
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandCall(Ljava/lang/String;)V

    .line 854
    :cond_0
    return-void
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 770
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 771
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandCancel(Ljava/lang/String;)V

    .line 772
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoMain(Z)V

    .line 773
    const-string/jumbo v0, "DM_MAIN"

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 784
    :goto_0
    return-void

    .line 777
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getIsCanceledtoPauseMusic()Z

    move-result v0

    if-nez v0, :cond_1

    .line 778
    const v0, 0x7f0a01f8

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->playTTS(Ljava/lang/String;)V

    goto :goto_0

    .line 780
    :cond_1
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoMain(Z)V

    .line 781
    const-string/jumbo v0, "DM_MAIN"

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0
.end method

.method public onFlowCommandExcute(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 809
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 810
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandExcute(Ljava/lang/String;)V

    .line 812
    :cond_0
    return-void
.end method

.method public onFlowCommandIgnore(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 816
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 817
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandIgnore(Ljava/lang/String;)V

    .line 819
    :cond_0
    return-void
.end method

.method public onFlowCommandLookup(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 823
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 824
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandLookup(Ljava/lang/String;)V

    .line 826
    :cond_0
    return-void
.end method

.method public onFlowCommandNext(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 866
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 867
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandNext(Ljava/lang/String;)V

    .line 869
    :cond_0
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 795
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 796
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandNo(Ljava/lang/String;)V

    .line 798
    :cond_0
    return-void
.end method

.method public onFlowCommandRead(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 858
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onFlowCommandRead:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 860
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandRead(Ljava/lang/String;)V

    .line 862
    :cond_0
    return-void
.end method

.method public onFlowCommandReply(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 844
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 845
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandReply(Ljava/lang/String;)V

    .line 847
    :cond_0
    return-void
.end method

.method public onFlowCommandReset(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 830
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 831
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandReset(Ljava/lang/String;)V

    .line 833
    :cond_0
    return-void
.end method

.method public onFlowCommandReturnMain()V
    .locals 2

    .prologue
    .line 721
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onFlowCommandReturnMain is ignored."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 837
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 838
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandRoute(Ljava/lang/String;)V

    .line 840
    :cond_0
    return-void
.end method

.method public onFlowCommandSend(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 802
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 803
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandSend(Ljava/lang/String;)V

    .line 805
    :cond_0
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 788
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    if-eqz v0, :cond_0

    .line 789
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;->onFlowCommandYes(Ljava/lang/String;)V

    .line 791
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 688
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 689
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onNewIntent action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " flags = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 690
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 689
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->handleIntent(Landroid/content/Intent;)V

    .line 707
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 401
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 402
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getInstance()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->unregisterListener()V

    .line 405
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "Configuration Changing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->requestRemoveView()V

    .line 424
    :goto_0
    return-void

    .line 417
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setMultiWindowVisible(Z)V

    .line 418
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->dismissNotiHomeMenu()V

    .line 419
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->dismissNotiMusic()V

    .line 420
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->pauseHomeOverlayService()V

    .line 421
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setHomeOverlayServiceFlowIdIf(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;)V

    .line 422
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->pause(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)V

    goto :goto_0
.end method

.method public onPrepareFlowChange(Ljava/lang/String;)V
    .locals 3
    .param p1, "nextFlowID"    # Ljava/lang/String;

    .prologue
    .line 711
    const-string/jumbo v0, "DM_MAIN"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 712
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onPrepareFlowChange NextFlowID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->dismissNotiHomeMenu()V

    .line 717
    :goto_0
    return-void

    .line 715
    :cond_0
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onPrepareFlowChange = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 320
    const-string/jumbo v1, "[MultiWindowActivity]"

    const-string/jumbo v2, "onResume"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setMultiWindowVisible(Z)V

    .line 323
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->isMultiMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 324
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setMultiMode(Z)V

    .line 327
    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 330
    invoke-static {p0, v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiWindowState(Landroid/content/Context;I)V

    .line 333
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setMultiModeSize()Z

    .line 341
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 342
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 356
    const-string/jumbo v1, "HOME_MODE_MULTIWINDOW"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->resetMainFlow(Ljava/lang/String;)V

    .line 357
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getInstance()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiHomeListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->registerListener(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;)V

    .line 358
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->sFlowId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 359
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->sFlowId:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setVoiceFlowId(Ljava/lang/String;)V

    .line 368
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mFlowIdIf:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setHomeOverlayServiceFlowIdIf(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;)V

    .line 369
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->resumeHomeOverlayService()V

    .line 372
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasNotification()Z

    move-result v1

    if-nez v1, :cond_3

    .line 375
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getIsWakeup()Z

    move-result v1

    if-nez v1, :cond_2

    .line 376
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getIsCanceledtoMain()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 377
    :cond_2
    const-string/jumbo v1, "[MultiWindowActivity]"

    const-string/jumbo v2, "Wake up"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const-string/jumbo v1, "DM_MAIN"

    invoke-static {v1, v5}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 390
    :cond_3
    :goto_0
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mDesNaviTTS:Ljava/lang/String;

    .line 392
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mDesNaviTTS:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 393
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mDesNaviTTS:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->playTTS(Ljava/lang/String;)V

    .line 394
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mDesNaviTTS:Ljava/lang/String;

    .line 397
    :cond_4
    return-void

    .line 380
    :cond_5
    sget-boolean v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->bPerformanceTest:Z

    if-eqz v1, :cond_6

    .line 381
    new-instance v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$VoiceResumeHandler;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$VoiceResumeHandler;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$VoiceResumeHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 384
    :cond_6
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/nuance/drivelink/DLAppUiUpdater;->resume(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)Z

    goto :goto_0
.end method

.method protected onResumeFragments()V
    .locals 2

    .prologue
    .line 313
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onResumeFragments"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResumeFragments()V

    .line 316
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 299
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onSaveInstanceState"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 302
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 306
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onStart"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onStart()V

    .line 308
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getInstance()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiHomeListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->registerListener(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;)V

    .line 309
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 429
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onStop"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onStop()V

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 432
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "Finishing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setExitNavi()V

    .line 446
    :cond_0
    return-void
.end method

.method protected reloadLayout()Z
    .locals 1

    .prologue
    .line 495
    const/4 v0, 0x0

    return v0
.end method

.method public setMultiModeSize()Z
    .locals 7

    .prologue
    const v6, 0x7f0d0020

    const v5, 0x7f0d001f

    .line 967
    const/4 v1, 0x1

    .line 969
    .local v1, "rtn":Z
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v2, :cond_0

    .line 970
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 971
    const-string/jumbo v2, "[MultiWindowActivity]"

    .line 972
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setMultiModeSize:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 973
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 975
    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 976
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 972
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 971
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 979
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 980
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 978
    invoke-direct {v0, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 982
    .local v0, "point":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setIsolatedCenterPoint(Landroid/graphics/Point;)V

    .line 986
    .end local v0    # "point":Landroid/graphics/Point;
    :goto_0
    return v1

    .line 984
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
