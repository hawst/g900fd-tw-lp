.class public Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;
.super Ljava/lang/Object;
.source "LocationRequestManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LocationRequestManager"


# instance fields
.field private mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mInternalListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

.field private mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;",
            ">;"
        }
    .end annotation
.end field

.field mLocationShareManager:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 35
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 36
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mLocationShareManager:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    .line 68
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;-><init>(Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mInternalListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 39
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->registerListener()V

    .line 40
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->getInstance()Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mLocationShareManager:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    .line 41
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method private registerListener()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    if-nez v0, :cond_0

    .line 62
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 61
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mInternalListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkLocationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 66
    return-void
.end method


# virtual methods
.method public addListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .prologue
    .line 44
    if-nez p1, :cond_0

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->registerListener()V

    .line 49
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public removeListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .prologue
    .line 53
    if-nez p1, :cond_0

    .line 57
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
