.class public Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;
.super Ljava/lang/Object;
.source "LocationsTableHandler.java"


# static fields
.field private static final TABLE_LOCATIONS:Ljava/lang/String; = "tb_locations"

.field private static final TAG:Ljava/lang/String; = "LocationsTableHandler"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method private getLocationFromCursor(Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 182
    .local v0, "i":I
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 183
    .local v2, "location":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationID(Ljava/lang/String;)V

    .line 184
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLatitude(D)V

    .line 185
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLongitude(D)V

    .line 186
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationAddress(Ljava/lang/String;)V

    .line 187
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setCount(I)V

    .line 188
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationName(Ljava/lang/String;)V

    .line 190
    return-object v2
.end method

.method public static getNewLocationsTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    const-string/jumbo v0, "tb_locations"

    return-object v0
.end method

.method private insertLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 85
    const-wide/16 v1, -0x1

    .line 87
    .local v1, "result":J
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 88
    .local v3, "values":Landroid/content/ContentValues;
    const-string/jumbo v4, "location_id"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string/jumbo v4, "latitude"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 90
    const-string/jumbo v4, "longitude"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 91
    const-string/jumbo v4, "address"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string/jumbo v4, "count"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 93
    const-string/jumbo v4, "name"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string/jumbo v4, "tb_locations"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 100
    .end local v3    # "values":Landroid/content/ContentValues;
    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    :goto_1
    return v4

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 100
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_0
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private isLocationInTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v3, 0x0

    .line 64
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "select * from tb_locations where location_id = \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 64
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 66
    .local v2, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 68
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 81
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "sql":Ljava/lang/String;
    :goto_0
    return v3

    .line 71
    .restart local v0    # "cursor":Landroid/database/Cursor;
    .restart local v2    # "sql":Ljava/lang/String;
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 72
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 73
    const/4 v3, 0x1

    goto :goto_0

    .line 76
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 77
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "sql":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 104
    const-wide/16 v1, 0x0

    .line 106
    .local v1, "result":J
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 107
    .local v3, "values":Landroid/content/ContentValues;
    const-string/jumbo v8, "latitude"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 108
    const-string/jumbo v8, "longitude"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 109
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 110
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 111
    const-string/jumbo v8, "address"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_0
    const-string/jumbo v8, "count"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 113
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 114
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 115
    const-string/jumbo v8, "name"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_1
    const-string/jumbo v5, "location_id = ?"

    .line 118
    .local v5, "whereClause":Ljava/lang/String;
    const/4 v8, 0x1

    new-array v4, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    .line 119
    .local v4, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v8, "tb_locations"

    invoke-virtual {p1, v8, v3, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    int-to-long v1, v8

    .line 124
    .end local v3    # "values":Landroid/content/ContentValues;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v5    # "whereClause":Ljava/lang/String;
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-nez v8, :cond_2

    :goto_1
    return v6

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_2
    move v6, v7

    .line 124
    goto :goto_1
.end method


# virtual methods
.method public createLocationsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 28
    :try_start_0
    const-string/jumbo v1, "CREATE TABLE IF NOT EXISTS tb_locations (location_id VARCHAR PRIMARY KEY, latitude REAL, longitude REAL, address VARCHAR, count INTEGER, name VARCHAR);"

    .line 32
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public dropLocationsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 41
    :try_start_0
    const-string/jumbo v1, "DROP TABLE IF EXISTS tb_locations;"

    .line 42
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public genLocationID(DD)Ljava/lang/String;
    .locals 7
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 198
    const-string/jumbo v2, "%.6f"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "strLat":Ljava/lang/String;
    const-string/jumbo v2, "%.6f"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "strLng":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public genLocationID(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Ljava/lang/String;
    .locals 4
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 194
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->genLocationID(DD)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAllLocations(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v2, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    const-string/jumbo v3, "select * from tb_locations"

    .line 165
    .local v3, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 166
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 168
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->getLocationFromCursor(Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    .line 169
    .local v1, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    if-eqz v1, :cond_1

    .line 170
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 173
    .end local v1    # "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    :cond_2
    if-eqz v0, :cond_3

    .line 174
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 176
    :cond_3
    return-object v2
.end method

.method public getLocation(Landroid/database/sqlite/SQLiteDatabase;DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "lat"    # D
    .param p4, "lng"    # D

    .prologue
    .line 139
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->genLocationID(DD)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->getLocation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    return-object v0
.end method

.method public getLocation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "locationId"    # Ljava/lang/String;

    .prologue
    .line 143
    const/4 v2, 0x0

    .line 145
    .local v2, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "select * from tb_locations where location_id = \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 146
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 145
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 147
    .local v3, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 149
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 150
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->getLocationFromCursor(Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    .line 153
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v3    # "sql":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 154
    :catch_0
    move-exception v1

    .line 155
    .local v1, "e":Landroid/database/SQLException;
    invoke-virtual {v1}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public removeLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 130
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "delete from tb_locations where location_id = \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 130
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 132
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public saveLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 49
    if-nez p2, :cond_0

    .line 50
    const-string/jumbo v0, "LocationsTableHandler"

    const-string/jumbo v1, "Invalid location to save on data base"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const/4 v0, 0x0

    .line 59
    :goto_0
    return v0

    .line 54
    :cond_0
    invoke-virtual {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->genLocationID(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLocationID(Ljava/lang/String;)V

    .line 56
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->isLocationInTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->updateLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    goto :goto_0

    .line 59
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->insertLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    goto :goto_0
.end method
