.class Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;
.super Landroid/widget/Filter;
.source "PlaceListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->getFilter()Landroid/widget/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;

    .line 78
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 5
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 82
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 84
    .local v0, "filterResults":Landroid/widget/Filter$FilterResults;
    if-eqz p1, :cond_0

    .line 86
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeService:Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    .line 87
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;->autocomplete(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 89
    .local v2, "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 90
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->access$0(Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;Ljava/util/List;)V

    .line 92
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 100
    :goto_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->access$1(Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 101
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->access$1(Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;)Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_3

    const/4 v3, 0x0

    :goto_2
    iput v3, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 105
    .end local v2    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_0
    return-object v0

    .line 92
    .restart local v2    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    .line 93
    .local v1, "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->access$1(Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 97
    .end local v1    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->access$0(Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;Ljava/util/List;)V

    goto :goto_1

    .line 101
    :cond_3
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->access$1(Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;)Ljava/util/List;

    move-result-object v3

    .line 102
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_2
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "results"    # Landroid/widget/Filter$FilterResults;

    .prologue
    .line 111
    if-eqz p2, :cond_0

    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->notifyDataSetChanged()V

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
