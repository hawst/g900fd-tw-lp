.class Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;
.super Ljava/lang/Object;
.source "SettingsServerDataResetActivity.java"

# interfaces
.implements Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserDataDeleteCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;)V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;)V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;)V
    .locals 3
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 222
    const-string/jumbo v0, "[SettingsServerDataResetActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onFailure:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;

    const/4 v1, 0x3

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->changeResetMode(I)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;I)V

    .line 224
    return-void
.end method

.method public onSuccess(Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;)V
    .locals 4
    .param p1, "arg0"    # Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;

    .prologue
    .line 228
    const-string/jumbo v1, "[SettingsServerDataResetActivity]"

    const-string/jumbo v2, "onSuccess"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "car_mode_on"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 235
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->finish()V

    .line 236
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 237
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity$UserDataDeleteCallback;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;->removeDialog()V

    .line 238
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->deleteLocalApplicationData()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :goto_0
    return-void

    .line 239
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
