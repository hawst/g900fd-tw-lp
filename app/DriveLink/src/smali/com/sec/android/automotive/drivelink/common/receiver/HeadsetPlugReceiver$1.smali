.class Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$1;
.super Ljava/lang/Object;
.source "HeadsetPlugReceiver.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHeadsetPlugChange(Z)V
    .locals 3
    .param p1, "isPlugged"    # Z

    .prologue
    .line 36
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->mListenerCollection:Ljava/util/AbstractCollection;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->access$0(Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;)Ljava/util/AbstractCollection;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 39
    return-void

    .line 36
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;

    .line 37
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;->onHeadsetPlugChange(Z)V

    goto :goto_0
.end method
